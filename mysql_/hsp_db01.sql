#演示数据库的操作
#创建一个名称为hsp_db01的数据库

#使用指令创建数据库
#create database hsp_db01;

#删除数据库
#drop database hsp_db01;

#创建一个使用utf8字符集的hsp_db03的数据库
#create database hsp_db03 character set utf8;

#创建一个使用utf8字符集,并带校对规则的hsp_db04的数据库
#create database hsp_db04 character set utf8 Collate utf8_bin;

#创建一个使用utf8字符集,不区分大小写
#create database hsp_db05 character set utf8 Collate utf8_general_ci;

#下面是一条查询语句 ， select 表示查询   * 表示所有字段  from 从哪个表 where 从哪个字段 name='刘皓' 查询的名字是刘皓
#select * from hsp_user where  `name` = '刘皓';


#查看,删除数据库
#show databases;

#显示数据库的创建语句 
#老师说明 在创建数据库,表的时候,为了规避关键字,可以使用反引号解决
#show create database `hsp_db02`;

#删除数据库的语句 [一定要慎用]
#drop database hsp_db05;

#drop database hsp_db02;

#备份数据库(注意：在DOS执行)命令行
#mysqldump -u用户名 -p -B 数据库1 数据库2 > 文件名.sql



