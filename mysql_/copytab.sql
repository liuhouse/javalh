#表复制
#自我复制数据(蠕虫复制)
#有时,为了对某个sql语句进行效率测试,我们需要海量数据的时候,可以使用此法为表创建海量数据


CREATE TABLE my_tab01(
	`id` INT,
	`name` VARCHAR(32),
	sal DOUBLE,
	job VARCHAR(32),
	deptno INT
);
DESC my_tab01;
SELECT * FROM my_tab01;

-- 演示如何自我复制
-- 1，先把emp表的记录复制到my_tab01
-- 老刘解读：查询出emp表中的全部数据[只是里面所显示的字段的数据]，将此表对应的字段插入到my_tab01表对应的字段
INSERT INTO my_tab01 
	(id , `name` , sal , job , deptno)
	SELECT empno , ename , sal , job , deptno FROM emp;
	
-- 2.自我复制
-- 老刘解读：查询my_tab01表中的所有数据插入到my_tab01数据中，所有的字段都是一模一样,一一对应的
INSERT INTO my_tab01
	SELECT * FROM my_tab01;

SELECT * FROM my_tab01;
SELECT COUNT(*) FROM my_tab01;


-- 如何删除掉一张表的重复记录
-- 1.先创建一张表my_tab02;
-- 2.让my_tab02有重复的记录
-- 老刘解读，创建一张表，像emp      就是把emp表的结构全部进行复制
CREATE TABLE my_tab02 LIKE emp; -- 这个语句 把emp的表结构(列),复制到my_tab02
DESC my_tab02;
-- 给my_tab02插入数据
-- 让my_tab02有重复的记录
INSERT INTO my_tab02 SELECT * FROM emp;

-- 考虑去重 my_tab02的记录
/*
	思路
	(1) 先创建一张临时表my_tmp,该表的结构和my_tab02一样
	(2)把my_tab02的记录通过distinct关键字处理后 把记录复制到my_tmp表中
	(3)清除掉my_tab02记录
	(4)把my_tmp表的记录复制到my_tab02
	(5)删除掉临时表my_tmp
*/
-- (1) 先创建一张临时表my_tmp,该表的结构和my_tab02一样
CREATE TABLE my_tmp LIKE my_tab02;
-- (2)把my_tab02的记录通过distinct关键字处理后 把记录复制到my_tmp表中
INSERT INTO my_tmp SELECT DISTINCT * FROM my_tab02;
-- (3)清除掉my_tab02记录
DELETE FROM my_tab02;
-- (4)把my_tmp表的记录复制到my_tab02
INSERT INTO my_tab02 SELECT DISTINCT * FROM my_tmp;
-- (5)删除掉临时表my_tmp
DROP my_tmp;
DESC my_tmp;
SELECT * FROM my_tmp;
SELECT * FROM my_tab02;

