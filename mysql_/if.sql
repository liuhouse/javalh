#流程控制函数
# 演示流程控制语句
#IF(exp1 , exp2 , exp3) 如果exp1为true , 则返回exp2 , 否则返回exp3
SELECT IF(TRUE , '北京' , '上海') FROM DUAL;

#IFNULL(expr1 , expr2)如果expr1不为空NULL，返回expr1 , 否则返回expr2
#IFNULL(expr1 , expr2)如果expr1为空,则返回expr2 , 否则返回expr1
SELECT IFNULL('ss' , '韩顺平教育') FROM DUAL;

#SELECT CASE WHEN expr1 THEN expr2 WHEN expr3 THEN expr4 ELSE expr5 END;[类似多重分支]
#如果expr1为TRUE的话则返回expr2 , 如果expr2为TRUE,则返回expr4，否则返回expr5

-- 老刘说明：这里的sql语句跟switch case 流程控制语句类似
-- 只要WHEN后面是TRUE的时候就会直接进行返回,如果是FALSE,就会继续查找下一个WHEN，如果WHEN后面全部是FALSE,最后就会使用ELSE的值
SELECT CASE
	WHEN FALSE THEN 'jack'
	WHEN FALSE THEN 'tom'
	WHEN TRUE THEN 'think'
	ELSE 'mary' END;


-- 1.查询emp表,如果comm是null，则显示0.0
-- 老韩说明，判断是否为null,要使用is null , 判断不是为空,要使用 is not null
SELECT ename , IF(comm IS NULL , 0.0 , comm) AS comm FROM emp;
SELECT ename , IF(comm IS NOT NULL , comm , 0.0 ) AS comm FROM emp;

-- 2.如果emp表的job是CLERK则显示职员,如果MANAGER则显示经理,如果SALEMAN则显示 销售人员，其他的显示正常
SELECT ename,job,(
	SELECT CASE 
	WHEN job='CLERK' THEN '职员'
	WHEN job='MANAGER' THEN '经理'
	WHEN job='SALESMAN' THEN '销售人员'
	ELSE job END
) AS job_name FROM emp;

SELECT * FROM dept;

SELECT * FROM salgrade;



	