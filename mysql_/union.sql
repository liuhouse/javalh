#合并查询
#介绍
-- 有时候在实际的应用中,为了合并多个select语句的结果,可以使用集合操作符号 union , union all
-- 1.union all
-- 该操作符用于取得两个结果集的并集,当使用该操作符的时候,不会取消重复行
SELECT ename , sal , job FROM emp WHERE sal > 2500 -- 5
UNION ALL
SELECT ename , sal , job FROM emp WHERE job = 'MANAGER'; -- 3

-- 8

-- 2.union
-- 该操作符与union all 相似,但是会自动去掉结果集中的重复行
SELECT ename , sal , job FROM emp WHERE sal > 2500
UNION
SELECT ename , sal , job FROM emp WHERE job = 'MANAGER';

