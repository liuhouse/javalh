SHOW ENGINES;
/*
   1.MyISAM不支持事务,也不支持外键,但是访问速度快,对事务完整性没有要求
   2.InnoDB存储引擎提供了具有提交,回滚和崩溃恢复能力的事务安全,但是比起
	MYISAM存储引擎,Innodb写的处理效率差一些并且会占用更多的磁盘空间以保留数据和索引
   3.MEMORY存储引擎使用存在内存中的内容来创建表,每个MEMORY表实际上对应一个磁盘文件
	MEMORY类型的表访问非常的快,因为它的数据是存放在内存中的，并且默认使用HASH索引
	但是MYSQL服务一旦关闭,表中的数据就会丢失掉,表的结构还在
*/
-- 三种存储引擎的使用案例
-- 对前面我们提到的三种存储引擎,我们举例说明
-- 查看所有的存储引擎
SHOW ENGINES;

-- innodb存储引擎,是前面使用过的
-- 1.支持事务 2.支持外键 3.支持行级锁

-- myisam存储引擎
CREATE TABLE t29(
	id INT,
	`name` VARCHAR(32)
)ENGINE MYISAM;
-- 添加速度快 2.不支持外键和事务 3.支持表级锁

-- 测试mysiam是不是支持事务  经过测试 Mysiam是不支持事务的
START TRANSACTION;
SAVEPOINT t1
INSERT INTO t29 VALUES(1,'jack');

ROLLBACK;
SELECT * FROM t29;


-- memory 存储引擎
-- 1.数据存储在内存中[关闭了mysql服务,数据丢失,但是表结构还在]
-- 2.执行速度很快(没有IO读写)
-- 3.默认支持索引(hash表)
CREATE TABLE t30(
	`id` INT,
	`name` VARCHAR(32)
) ENGINE MEMORY;
DESC t30;
INSERT INTO t30 VALUES(1,'tom'),(2,'jack'),(3,'hsp');
SELECT * FROM t30;

-- 指令修改表的存储引擎
ALTER TABLE t30 ENGINE=INNODB;


-- 如何选择表的存储引擎
-- 1.如果你的应用不需要事务,处理的只是基本的CURD操作,那么MyISAM是不二选择
-- 2.如果需要支持事务,选择Innodb
-- 3.Memory存储引擎就是将数据存储在内存中,由于没有磁盘I/O读写的等待,所以速度非常快
-- 但是因为是内存的存储引擎,所以所做的任何修改在服务器重新启动之后都将消失.(经典用法 用户在线状态)

-- 修改表的引擎
-- alter table 表名 engine = 引擎名称




