#创建一张数据表 `emp`
-- CREATE TABLE `emp`(
-- 	`id` INT,
-- 	`name` VARCHAR(32),
-- 	`sex` CHAR(1),
-- 	`birthday` DATE,
-- 	`entry_date` DATETIME,
-- 	`job` VARCHAR(32),
-- 	`salary` DOUBLE,
-- 	`resume` TEXT
-- ) CHARSET utf8 COLLATE utf8_bin ENGINE INNODB;

#修改表的基本介绍
#使用alter table 语句追加,修改,或者删除列的语法
-- 应用实例 altertab.sql
-- 员工表emp上增加一个image列,varchar类型(要求在resume后面)
ALTER TABLE `emp`
	ADD image VARCHAR(32) NOT NULL DEFAULT '' AFTER `resume`;
-- 修改job列,使其长度为60
ALTER TABLE `emp`
	MODIFY job VARCHAR(60) NOT NULL DEFAULT ''
-- 删除sex列
ALTER TABLE `emp`
	DROP sex;
-- 表明修改为employee
RENAME TABLE `emp` TO `employee`;
-- 修改表的字符集为utf8
ALTER TABLE employee CHARSET utf8;
ALTER TABLE employee CHARACTER SET utf8;
-- 列名name修改为user_name
ALTER TABLE `employee`
	CHANGE `name` `user_name` VARCHAR(64) NOT NULL DEFAULT ''
-- 可以查看表的列 -- 
DESC `employee`;





