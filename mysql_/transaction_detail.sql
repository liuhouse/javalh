#事务的细节讨论
-- 1:如果不开启事务,默认情况下,dml操作是自动提交的,不能回滚
-- 这里就自动提交了 commit
INSERT INTO t28 VALUES(3,'鲁肃');
ROLLBACK;

-- 2.如果开始一个事务,你没有创建保存点,也可以执行rollback操作,
-- 默认就是回退到你的事务开始的状态
START TRANSACTION
INSERT INTO t28 VALUES(6,'king1');
INSERT INTO t28 VALUES(7 ,'sock1');
-- 表示回滚到事务开启的地方
ROLLBACK;
COMMIT;

-- 3.你可以在这个事务中(还没有提交的时候),创建多个保存点,比如savepoint aaa
-- 指定dml操作
-- savepoint bbb

-- 4.你可以在事务没有提交前,选择退回到哪个保存点
-- 5.Innodb存储引擎支持事务,MyISAM不支持
-- 6.开始一个事务 start transaction , set autocommit=off

SET autocommit=off
INSERT INTO t28 VALUES(8,'king2');
INSERT INTO t28 VALUES(9 ,'sock2');
ROLLBACK;
COMMIT

SELECT * FROM t28;