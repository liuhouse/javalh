#创建表
#创建一个员工表emp(课堂练习),选用适当的数据类型
CREATE TABLE `emp`(
	`id` INT,
	`name` VARCHAR(32),
	`sex` CHAR(1),
	`birthday` DATE,
	`entry_date` DATETIME,
	`job` VARCHAR(32),
	`salary` DOUBLE,
	`resume` TEXT
)CHARSET utf8 COLLATE utf8_bin ENGINE INNODB;

#添加一条数据
INSERT INTO `emp` VALUES(1,'小钻风','男','1995-12-06','2013-12-05:13:00:00','程序员',10000.25,'大王叫我来巡山');

#创建一个用户表
CREATE TABLE `users`(
	`id` INT,
	`name` VARCHAR(50),
	`age` SMALLINT(3),
	`introduce` TEXT
)CHARSET utf8 COLLATE utf8_bin ENGINE INNODB;

#给用户表添加一条数据
INSERT INTO users VALUES(1,'峨眉老祖',120,'我是一个老神仙');