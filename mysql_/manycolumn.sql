#多列子查询
-- 多列子查询则是指查询返回多个列数据的子查询语句
-- 请思考如何查询与ALLEN的部门的岗位完全相同的所有的雇员(并且不包含ALLEN本人)
-- 查询结构 (字段1 ， 字段2...) = (select 字段1,字段2... from table)   需要说明的是,字段必须要保持统一
-- 分析1 . 得到ALLEN的部门和岗位

-- 也就是查询出ALLEN所在部门的相同岗位的同事
-- 分析2，把上面的查询当做子查询来使用,并且使用多列子查询的语法进行匹配
SELECT
	deptno , job , ename 
	FROM emp 
	WHERE(deptno , job) = (SELECT deptno , job FROM emp WHERE `ename` = 'ALLEN')
	AND ename != 'ALLEN';
	
SELECT deptno , job FROM emp WHERE `ename` = 'ALLEN';

SELECT * FROM emp;

-- 一个需求：请查询出和宋江的语文数学英语成绩完全一样的人 ， 这样就可以知道哪些人是作弊了
-- 分析,先查询出宋江的语文英语数学的成绩
-- 将查询后的结果作为子查询然后查询其他的学生的各科成绩

SELECT chinese , english , math FROM student WHERE `name` = '宋江';
SELECT 
	`name` , `chinese` , `english` , `math`
	FROM student WHERE (chinese,english,math) = (SELECT chinese , english , math FROM student WHERE `name` = '宋江'); 
SELECT * FROM student;
