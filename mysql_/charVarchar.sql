#字符串的基本使用
-- char(size)
-- 固定长度的字符串,最大是255个字符
-- varchar(size) 0-65535 个字节
-- 可变长度的字符串 最大65532字节 [utf8编码最大是21844个字符 1-3个字节用于记录大小]

-- 演示字符串类型使用 char varchar
-- 注释的快捷键 shift_ctrl+c , 注销注释 shift+ctrl+r

-- char(size)
-- 规定长度的字符串 最大是255个字符
-- varchar(size) 0-65535个字节
-- 可变长度字符串 最大65535个字节 [utf8编码最大是21844字符,1-3个字符用于记录字符串的大小]
-- 如果表的编码是utf8 varchar(size) size = (65535 - 3) / 3 = 21844
-- 如果表的编码是gbk varchar(size) size = (65535-3) / 2 = 32766

-- create table t09(
-- 	`name` char(255)
-- );

-- gbk最大是32766
-- create table t10(
-- 	`name` varchar(32766) charset gbk
-- );

-- select * from bd_order where business_id = 306 and supplier_type = 3 and order_status = 2 and delete_time = 0 ;
-- select * from bd_order_detail where order_id in(131173,144819);
