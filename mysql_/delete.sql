#delete语句
#使用delete语句删除表中的数据

#快速入门案例
#删除表中名称为 '格力手机' 的记录
DELETE FROM goods WHERE goods_name = '格力手机';
#删除表中所有的记录 ， 老师提醒,一定要小心
DELETE FROM goods;

-- delete语句不能删除某一列的值(可以使用update语句设置成null或者'')
UPDATE goods SET price = NULL WHERE goods_name = '地狱火';

#删除一张表
DROP TABLE goods;

#使用细节
-- 1.如果不使用where子句,将删除表中的所有的数据
-- 2.delete子句不能删除某一列的值(可以使用update设置为null或者'')
-- 3.使用delete语句仅仅能删除数据表中的记录,不能删除表本身,如果要删除表,需要使用drop table 语句 .  drop table 表名

SELECT * FROM goods;