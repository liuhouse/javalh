#create database hsp_db03;
#日期类型的基本使用
#演示时间的相关类型
#创建一张表,date ,datetime,timestamp
CREATE TABLE t14(
	birthday DATE,-- 生日
	job_time DATETIME, -- 记录年月日,时分秒
	login_time TIMESTAMP 
	NOT NULL DEFAULT CURRENT_TIMESTAMP
	ON UPDATE CURRENT_TIMESTAMP -- 登录时间,如果希望log_time自动更新,需要配置
);

#插入数据
#最后一个是有默认时间的,所以不用填充
INSERT INTO t14(birthday , job_time) VALUES('2021-10-10' , '2020-12-25');

SELECT * FROM t14;