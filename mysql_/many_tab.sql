#mysql的多表查询
#问题的引出(重点,难点)
#多表查询是指基于两个和两个以上的表的查询,在实际的应用中,查询单个表可能不能满足你的需求
#如下面的的课堂练习,需要使用用到(dept表和emp表)
-- 多表查询
-- ?显示雇员名,雇员工资以及所在的部门的名字【笛卡尔集】
/**
	老韩分析
	1.雇员名,雇员工资来自emp表
	2.部门的名字,来自dept表
	3.需要对emp和dept查询 ename , sal , dname , deptno
	4.当我们需要指定显示某个表的列时,需要 表.列表
*/
-- 这里查询的结果是,第一张表的每一行数据 * 第二张表的全部数据    然后相加
-- 就相当于 第一张表的行数 * 第二张表的行数 = 全部数据
-- 相当于是把第一张表的每一行数据作为主表，跟第二张表的每一行的数据进行拼接然后进行显示
-- 相当于第一张表的每一行数据要显示四次   最后就是 13 * 4 = 52
-- 13*(1*4)
-- 笛卡尔集
SELECT * FROM emp,dept;

-- ?显示雇员名,雇员工资以及所在的部门的名字【笛卡尔集】
-- 多表查询的时候,写字段的时候,如果有重复的字段,就要写清楚 表名.字段,如果字段是唯一的,可以不用写,但是最好写上

-- 老韩小技巧：多表查询的条件不能少于表的个数 -1 ， 意思是,如果是两个表进行联合查询,那么至少需要一个查询条件
-- 如果是三个表进行查询,那么至少需要两个查询条件，否则会出现笛卡尔集
SELECT ename , sal , emp.deptno ,dept.dname FROM emp , dept WHERE emp.deptno = dept.deptno;

-- ?如何显示部门号为10的部门名,员工名和工资
SELECT ename , sal , dname , emp.deptno FROM emp , dept  WHERE emp.deptno = dept.deptno AND emp.deptno = 10;

-- ?显示各个员工的姓名,工资,以及工资的级别
-- 思路 姓名,工资,来自于emp 员工表
-- 工资级别 来自于salgrade级别表
-- 写sql,先写一个简单的,然后加入过滤条件
SELECT ename ,sal ,grade,losal, hisal FROM emp , salgrade;
SELECT ename , sal , grade FROM emp , salgrade WHERE sal BETWEEN losal AND hisal;

-- 技巧，先编写完整的数据,查询出所有的数据,也就是笛卡尔集数据,然后加上对应的过滤条件即可
-- 其实完整的写法如下
SELECT 
	emp.ename ,
	emp.sal ,
	salgrade.grade 
	FROM emp , salgrade 
	WHERE emp.sal BETWEEN salgrade.losal AND salgrade.hisal;
	
	
-- 学员练习：显示雇员名称,雇员工资以及所在的部门名字,并按照部门排序(降序排)
SELECT ename , sal , dname FROM emp , dept WHERE emp.deptno =dept.deptno;




-- 查询所有的雇员
SELECT * FROM emp;
-- 查询所有的部门
SELECT * FROM dept;
-- 查询工资区间
SELECT * FROM salgrade;


-- 查询供应商的管理员的名字
SELECT bd_supplier.id,bd_supplier.phone,`level`,bd_supplier_personal.real_name FROM bd_supplier,bd_supplier_personal 
WHERE bd_supplier.phone = bd_supplier_personal.phone 
AND bd_supplier.level = 1 
AND bd_supplier.phone != ''
AND bd_supplier_personal.is_manage = 2;
SELECT * FROM bd_supplier WHERE id = 162;
SELECT * FROM bd_supplier_personal WHERE phone = '15102961207' AND supplier_id = 162 AND is_manage = 2;


