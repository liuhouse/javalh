#用户定义主表和从表之间的关系:外键约束要定义在从表上，主表则必须有主键约束或者是unique约束，当定义了外键约束后
#要求外键列数据必须在主表的主键中存在或者是为null(学生/班级/图示)
-- FOREIGN KEY (本表字段名) REFERENCES 主表名(主键名或unique字段名)
-- 学生表(从表) 			班级表(主表) foreign.sql
--  id name class_id			id class_name

-- foreign key(外键) -- 细节说明(创建小表演示)
/*
	1.外键指向的表的字段,要求是primary key 或者是unique
	2.表的类型是innodb,这样才支持外键
	3.外键字段的类型要和主键字段的类型一致(长度可以不同)
	4.外键字段的值,必须在主键的字段中出现过,或者为null[前提是外键字段允许为null]
	5.一旦建立了主外键的关系,数据就不能随意删除了,这样做的业务性对数据的要求比较严格,这样就不能手动在数据库删除数据了
*/

-- 外键演示

-- 创建 主表 my_class
CREATE TABLE my_class(
	id INT PRIMARY KEY, -- 班级编号
	`name` VARCHAR(32) NOT NULL DEFAULT ''
);

-- 创建从表 my_stu
CREATE TABLE my_stu(
	`id` INT PRIMARY KEY , -- 学生编号
	`name` VARCHAR(32) NOT NULL DEFAULT '',
	`class_id` INT, -- 学生所在的班级
	-- 下面指定外键的关系
	FOREIGN KEY(class_id) REFERENCES my_class(`id`)
);

-- 测试数据
-- 添加班级数据
INSERT INTO my_class VALUES(100 , 'java'),(200,'web');

-- 添加学生数据
-- 可以添加进去,因为外键class_id = 100  而在班级表 my_class 中是存在100这个编号的班级的
INSERT INTO my_stu VALUES(1,'tom',100);
-- 可以添加进去,因为外键class_id = 200 而在班级表 my_class中是存在200这个编号的班级的
INSERT INTO my_stu VALUES(2,'jack',200);
-- 不可以添加进去,因为外键class_id=300，而在班级表my_class中不存在300这个编号的班级的
INSERT INTO my_stu VALUES(3,'hsp',300);

INSERT INTO my_stu VALUES(5 , 'king' ,NULL) -- 可以,外键 没有写 not null;

-- 一旦建立主外键的关系,数据不能随意的删除了
DELETE FROM my_class WHERE id = 100;


SELECT * FROM my_stu;

SELECT * FROM my_class;