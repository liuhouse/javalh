#mysql用户管理细节说明
-- 1.在创建用户的时候,如果不指定Host,则为%,%表示所有的IP都有连接权限
CREATE USER fzd;

-- 2.你也可以这样指定
-- 表示xxx用户在192.168.1.* 在这个ip段中的ip都可以登录mysql
CREATE USER 'ml'@'192.168.1.%';

-- 3.在删除用户的时候,如果host不是%，需要明确指定 '用户'@host的值
DROP USER 'm1'@'192.168.1.%';

SELECT * FROM mysql.user;