#not null
-- 如果在列上定义了not null , 那么当插入数据的时候,必须为该列提供数据
-- 字段名  字段类型 not null
-- unique(唯一)
-- 当定义了唯一约束后,该列的值是不能重复的
-- 字段名 字段类型 unique
-- unique 细节(注意)
-- 1.如果没有指定not null ,则unique字段可以有多个null
-- 2.一张表可以有多个unique字段

-- unique的使用
CREATE TABLE t22(
	`id` INT UNIQUE, -- 表示id列是不可以重复的
	`name` VARCHAR(32),
	`email` VARCHAR(32)
);
INSERT INTO t22 VALUES(1,'jack','jack@sohu.com');
-- 这里是添加不进去的 因为id是唯一的
INSERT INTO t22 VALUES(1,'tom','tom@sohu.com');


-- unique 使用细节
-- 1.如果没有指定not null, 则 unique字段可以有多个null
-- 2.如果一个列(字段),是unique not null 使用效果类似 primary key
INSERT INTO t22 VALUES(NULL , 'tom' , 'tom@suhu.com');


SELECT * FROM t22;

-- 一张表可以有多个unique字段
CREATE TABLE t24(
	`id` INT UNIQUE, -- 表示id列是不可以重复的
	`name` VARCHAR(32) NOT NULL UNIQUE, -- 表示name不能为null,并且不能重复
	`email` VARCHAR(32),
	PRIMARY KEY(`id`)
);

#只要设置了
INSERT INTO t23 VALUES(2 , 'hsp1','hsp@qq.com');


DESC t24;

DESC t23;






