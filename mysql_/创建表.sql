#创建表
#CREATE TABLE table_name(
#	field1 datatype，
#      field2 datatype,
#	field3 datatype
#)character set 字符集 collate 校对规则 engine 存储引擎
#field : 指定列名   datatype : 指定列类型(字段类型)
#character set : 如不指定则为所在数据库的字符集
#collate:如果不指定则为所在数据库的校对规则
#engine:引擎(这个涉及的内容比较多,后面单独讲解)

#题目要求
#注意 : hsp_db02创建表的时候,要根据需要保存的数据创建相应的列,并根据数据的类型定义相应的列类型.例如user表(快速入门案例)
#id 	整形
#name	字符串
#password	字符串
#birthday	日期

#使用命令行的方式创建表
#create table `user`(
#	`id` int,
#	`name` varchar(255),
#	`password` varchar(255),
#	`birthday` date
#) character set utf8 collate utf8_bin engine innodb;





