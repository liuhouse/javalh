#数值型(小数)的基本使用
#1.float/double[unsigned]
#float 单精度精度   double 双精度精度

# 2.DECIMAL[M,D][UNSIGNED]
#可以支持更加精确的小数位,M是小数位数(精度)的总数[这个小数有多少位数字],D是小数点(标度)后面的位数
#如果D是0,则值没有小数点或者分数部分,M最大是65,D最大是30,如果D被省略,默认是0,如果M被省略,默认是10
#建议：如果希望小数的精度高,推荐使用decimal
#案例演示

#创建表
CREATE TABLE t06(
	num1 FLOAT,
	num2 DOUBLE,
	num3 DECIMAL(30,20)
);

#添加数据
#从下面插入数据的结果来看
#1.float只是保存了小数点后四位   精度不够
#2.double保存了全部的数据,一般使用double双精度来定义数据类型
#3.decimal不仅保存了全部的数据,而且还自动在小数点后面补充0,补够20位,精度更高,存放的数据量更大
INSERT INTO t06 VALUES(88.12163543543546,88.12163543543546,88.12163543543546);

#decimal可以存放很大的数，DECIMAL(65)，最大的数据长度是65,第二个参数是0,表示没有小数
CREATE TABLE t07(
	num DECIMAL(65)
);

INSERT INTO t07 VALUES(45555555555555568789797777777777777777777777777);

#创建没有符号的数据,证明只能是正数
#看看整形bigint是不是能存放这么大的数据
CREATE TABLE t08(
	num BIGINT UNSIGNED
);

#插入不进去,因为数据已经超出了bigint的范围
INSERT INTO t08 VALUES(456666666666666666666666666666666666666666878787);
 
SELECT * FROM t06;

