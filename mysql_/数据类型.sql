Mysql常用的数据类型(列类型)
 分类			数据类型								说明
				BIT(M)									位类型.M指定位数,默认为1,范围1-64
				TINYINT[UNSIGNED]占1个字节				带符号的范围是负的-128 - 127之间,无符号是0-255，默认是有符号的
数值类型		SMALLINT[UNSIGNED]占两个字节			带符号是 负的2^15到2^15-1,无符号的是0-2^16-1
				MEDIUMINT[UNSIGNED]三个字节				带符号的是负2^23到2^23-1,无符号的是0-2^24-1
				INT[UNSIGNED]四个字节					带符号的是负2^31到2^31-1,无符号的是0-2^32-1
				BIGINT[UNSIGNED]八个字节					带符号的是负2^63到2^63-1,无符号的是0-2^64-1
				
				FLOAT[UNSIGNED]							占用空间4个字节
				DOUBLE[UNSIGNED]						表示比float精度更大的小数,占用空间8个字节
				DECIMAL(M,D)[UNSIGNED]					定点数 M指的是长度,D表示小数点的位数
				
				
				CHAR(size) CHAR(20)						固定长度字符串 最大255
				VARCHAR(size) VARCHAR(20)				可变长度的字符串0-65535[即:2^16-1]
	文本
二进制类型		BLOB LONGBLOB							二进制数据BLOB 0-2^16-1 LONGBLOB 0-2^32-1
				TEXT LONGTEXT							文本 TEXT 0-2^16 LONGTEXT 0-2^32
				
				
时间日期		DATE/DATETIME/TIMESTAMP				日期类型(YYYY-MM-DD)(YYYY-MM-DD HH:MM:SS),
														TimeStamp表示时间戳,他用于记录insert,update的操作时间


				










