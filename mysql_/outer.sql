#mysql表外连接
-- 提出一个问题
-- 1.前面我们学习的查询,是利于where子句对两张表或者多张表,形成笛卡尔积进行筛选,根据关联条件，显示的是所有匹配的记录
-- 匹配不上的,不显示
-- 2.比如：列出部门名称和这些部门的员工名称和工作，同时要求,显示出哪些没有员工的部门
-- 3.使用我们学习过的多表查询的sql,看看效果如何

SELECT dname ,dept.deptno, ename , job FROM emp,dept WHERE emp.deptno = dept.deptno;

SELECT * FROM dept;

-- 创建stu
/*
	id 	name
	1	jack
	2	tom
	3	Kity
	4	nono
*/
CREATE TABLE stu(
	id INT,
	`name` VARCHAR(32)		
);
INSERT INTO stu VALUES(1,'jack'),(2,'tom'),(3,'Kity'),(4,'nono');



-- 创建exam
/*
	id	grade
	1	56
	2	76
	11	8
*/
CREATE TABLE exam(
	id INT,
	grade INT
);

INSERT INTO exam VALUES(1,56),(2,76),(11,8);
SELECT * FROM exam;
SELECT * FROM stu;

-- 使用左连接
-- （显示所有人的成绩,如果没有成绩,也要显示该人的姓名和id号,成绩显示为空）
-- 先使用传统的方法看看最后的结果是什么样子的 这样只能显示出有成绩的同学,没有成绩的同学是显示不出来的,不符合要求
SELECT stu.id,`name`,grade FROM stu,exam WHERE stu.id = exam.id;

-- 改成左外连接
-- 左外连接的意思是  左边的表示主表,主表的内容全部显示,右表的辅助表,根据on条件符合的显示,不符合为null显示
SELECT stu.id,`name`,grade FROM stu LEFT JOIN exam ON stu.id = exam.id;

-- 使用右外连接 (显示所有成绩,如果没有名字匹配则显示为空)
-- 右外连接的意思是  右边的表的数据全部显示,左边的根据on后面的条件符合显示,不符合显示为空
SELECT grade , `name` , exam.id FROM stu RIGHT JOIN exam ON stu.id = exam.id;

-- 基础还是笛卡尔积
-- 列出部门名称和这些部门的员工信息(名字和工作)
-- 同时列出哪些没有员工的部门名
-- 使用左外连接
SELECT dname,ename,job FROM dept LEFT JOIN emp ON dept.deptno = emp.deptno;

-- 使用右外连接
SELECT dname,ename,job FROM emp RIGHT JOIN dept ON dept.deptno = emp.deptno;

-- 一个需求,查询出所有的供应商对应的管理员的名称 ， 不管有没有管理员 ， 供应商必须显示
SELECT 
	bd_supplier.id,bd_supplier.phone,company_name,real_name,bd_supplier_personal.nickname 
	FROM 
	bd_supplier
	LEFT JOIN 
	bd_supplier_personal
	ON 
	bd_supplier.id = bd_supplier_personal.supplier_id
	WHERE bd_supplier.`level` = 4 
	AND bd_supplier.phone != '' 
	AND bd_supplier_personal.is_manage = 2
	LIMIT 20;
	
-- 一个需求,查询出所有供应商的人员所对应的供应商,包括管理员和员工
SELECT 
	person.id,
	person.nickname,
	person.real_name,
	supplier.company_name
	FROM
	bd_supplier_personal AS person
	LEFT JOIN
	bd_supplier AS supplier
	ON person.supplier_id = supplier.id
	ORDER BY person.id DESC
	LIMIT 50
	;
	

-- 查询出所有的员工的信息  以及所属的供应商名称信息
SELECT
	p.id,
	p.real_name,
	p.nickname,
	s.company_name
	FROM bd_supplier  s
	RIGHT JOIN bd_supplier_personal  p
	ON p.supplier_id = s.id
	ORDER BY p.id DESC
	LIMIT 50
	;
	
	-- 老师小结，在实际的开发中,我们绝大多数情况下是使用之前学过的连接
	-- 都满足条件才会进行显示
	
	
	
	SELECT * FROM bd_stock WHERE supplier_id = 241 AND good_id IN(1153282,1156816);
	
	SELECT * FROM bd_stock_tmp WHERE business_id = 241;



