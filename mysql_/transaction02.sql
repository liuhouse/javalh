-- 创建一张数据表
CREATE TABLE t28(
	`id` INT,
	`name` VARCHAR(32)
) CHARSET=utf8 ENGINE=INNODB;

-- 开启事务
START TRANSACTION

-- 设置一个保存点
SAVEPOINT a

-- 开始dml操作
INSERT INTO t28 VALUES(1,'貂蝉')

-- 设置第二个保存点
SAVEPOINT b

-- 开始第二次的dml操作
INSERT INTO t28 VALUES(2,'小乔');

-- 这个时候发现关羽是错的,需要退回到保存点b，也就是设置保存点之后的操作全部回滚
ROLLBACK TO b

-- 这个时候发现原来不是他们两个,人家要的三国女英雄,需要全部回退，回滚
ROLLBACK

-- 这个时候发现都是对的,已经确定了就是这两个人,那么就真正的提交,提交了之后,就不能再返回了,生米已成熟饭
COMMIT


SELECT * FROM t28;

