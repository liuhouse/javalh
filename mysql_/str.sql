#字符串相关的函数
-- 演示字符串相关函数的使用,使用emp表来演示
-- CHARSET(str) 返回字符串的字符集
SELECT CHARSET(ename) FROM emp;
-- CONCAT(string1,string2)连接字符,将多个列拼接成一列
SELECT CONCAT(ename,' 工作是 ',job) FROM emp;

-- INSTR(string,substring) 返回substring在string 中出现的位置,没有返回0
-- dual 亚元表,系统表,可以做为测试表使用
SELECT INSTR('hanshunping' , 'ping') FROM DUAL;

-- UCASE(string2 )转换成大写
SELECT UCASE(ename) FROM emp;

-- LCASE(string2)转换成小写
SELECT LCASE(ename) FROM emp;

-- LEFT(string2 , length) 从string2中的左边起取length个字符
SELECT LEFT(ename , 2) FROM emp;
-- RIGHE(string2，length)从string2中的右边起取length个字符
SELECT RIGHT(ename , 2) FROM emp;

-- LENGTH(string) string 长度【按照字节】
SELECT LENGTH(ename) FROM emp;

-- REPLACE(str,search_str,replace_str)
-- 在str字符串中查找search_str替换成replace_str
-- 如果是manager 就 替换成经理
SELECT ename , REPLACE(job , 'MANAGER' , '经理') FROM emp;

-- STRCMP(string1 , string2) 逐字符比较两个子串大小  等于返回0 ， 大于返回1 ， 小于返回-1
SELECT STRCMP('hsp' , 'asp') FROM DUAL;

-- SUBSTRING(str , position , length)    就是截取字符串
-- 从str的position位置开始【从1开始计算】,取length个字符
-- 从ename列的第一个位置开始取出2个字符
SELECT SUBSTRING(ename , 1 , 2) FROM emp;

-- LTRIM(string2) RTRIM(string2) TRIM(string2)
-- 去除前端空格 或者 后端空格
SELECT LTRIM('      韩顺平教育   ') FROM DUAL;
SELECT RTRIM('      韩顺平教育   ') FROM DUAL;
SELECT TRIM('       韩顺平教育    ') FROM DUAL;

-- 练习：以首字母小写的方式显示所有的员工的姓名
-- 1:先查询出所有员工的姓名
-- 2:将所有员工的姓名的首字母变化为小写
SELECT CONCAT(LCASE(SUBSTRING(ename,1,1)) , SUBSTRING(ename , 2)) FROM emp;

SELECT CONCAT(LCASE(LEFT(ename,1)) , SUBSTRING(ename , 2)) FROM emp;

-- 练习：以首字母大写的方式显示所有员工的姓名 , 但是其他的字母都是小写的
-- 第一种方式
SELECT CONCAT(UCASE(LEFT(ename,1)) , LCASE(SUBSTRING(ename , 2))) FROM emp;
-- 第二种方式 -- 化繁为简   各个击破
SELECT CONCAT(UCASE(SUBSTRING(ename,1,1)) , LCASE(SUBSTRING(ename,2)))  FROM emp;



