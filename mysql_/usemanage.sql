#mysql管理
#mysql用户
-- mysql中的用户,都存储在系统数据库mysql中的user表中
SELECT `host`,`user`,`authentication_string` FROM mysql.user;
-- 其中user表的重要字段说明
-- 1.host：允许登录的"位置",localhost表示该用户只允许本机登录,也可以指定ip地址,如：192.168.1.100
-- 2.user:用户名
-- 3.authentication_string ： 密码,是通过mysql的password()函数加密之后的密码

-- 创建用户
-- create user '用户名'@'允许登录的位置' identified by '密码'
-- 说明：创建用户,同时指定密码
CREATE USER 'lh_1'@'localhost' IDENTIFIED BY '123456';

CREATE USER 'hu_1'@'localhost' IDENTIFIED BY '123456';

-- 删除一个用户
-- drop user '用户名'@'允许登录的位置';
DROP USER 'hu_1'@'localhost';

-- 修改用户的密码
ALTER USER 'lh_1'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';

SELECT * FROM mysql.user;


