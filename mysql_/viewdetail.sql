#视图的细节讨论
/*
  1.创建视图后,到数据库去看,对应视图只有一个视图结构文件(形式：视图名.frm)
  2.视图的数据变化会影响到基表,基表的数据变化也会影响到视图(insert update delete)
  
  视图的最佳实践
  1.安全.一些数据表有着重要的信息,有些字段是保密的,不能让用户直接看到,这个时候就可以创建一个视图
    在这张视图上只保留一部分的字段,这样,用户就可以查询自己需要的字段,不能查看保密的字段
  2.性能,关系数据库的数据常常会分表存储,使用外键建立这些表之间的关系,这个时候,数据库查询通常会用到连接(JOIN)
    这样做不但麻烦,效率相对较低,如果建立一个视图,将相关的表和字段组合在一起,就可以避免使用join查询数据
  3.灵活.如果系统中有一张旧的表,这张表由于设计的问题,即将被废弃,然而,很多应用都是基于这张表，不易修改,这个时候
    就可以建立一张视图,视图中的数据直接映射到新建的表,这样,就可以少做很多改动,也达到了升级数据表的目的
   
*/

-- 视图课堂练习
-- 针对emp,dept和salgrade 三张表,创建一个视图emp_view3,可以显示 雇员编号,雇员名,雇员部门名称和薪水级别[即使用三张表,构建一个视图]
-- 思路分析 得到三张表联合查询,得到结果,将得到的结果,构建成视图

-- 笛卡尔积 三个表的数据相乘
-- 三张表至少要有两个查询条件
CREATE VIEW emp_view03 
AS
SELECT empno , ename ,dname ,grade  FROM 
	emp , dept ,salgrade
	WHERE emp.deptno = dept.deptno AND emp.sal BETWEEN salgrade.losal AND salgrade.hisal;
SELECT * FROM emp_view03;
	
DESC salgrade;