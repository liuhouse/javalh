CREATE TABLE actor( -- 演员表
	id INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(32) NOT NULL DEFAULT '',
	sex CHAR(1) NOT NULL DEFAULT '女',
	borndate DATETIME,
	phone VARCHAR(12)
) ENGINE=INNODB;

INSERT INTO actor VALUES(NULL , '马德华' , '男' , '1995-12-06','1332555');

SELECT * FROM actor;


CREATE TABLE news(
	id INT PRIMARY KEY AUTO_INCREMENT,
	`content` TEXT,
	`date_time` DATE
) ENGINE=INNODB;

SELECT * FROM news;




-- 演示sql注入问题
-- 创建一张表
CREATE TABLE admins(
	`name` VARCHAR(32) NOT NULL UNIQUE,
	`pwd` VARCHAR(32) NOT NULL DEFAULT ''
) CHARSET utf8;

-- 添加数据
INSERT INTO admins VALUES('tom' , '123');

-- 查找某个管理员是否存在
SELECT * FROM admins WHERE `name` = 'tom' AND pwd = '123';

-- SQL
-- 输入用户名为 1'or
-- 输入万能密码为 or '1'='1
-- 无论什么时候都能查询出来
SELECT * FROM admins WHERE `name` = '1' OR' and pwd='OR'1'='1';

SELECT * FROM admins;