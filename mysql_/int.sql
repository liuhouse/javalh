#演示整形
#老韩使用tinyint来演示范围有符号的 -128 ~ 127 之间  如果没有符号 0-255
#说明：表的字符集,效验规则,存储引擎,这里使用数据库默认的
#1.如果没有指定unsinged,则TINYINT默认就是有符号的 
#2.如果指定unsinged，则TINYINT就是无符号的0-255
#create table t3(
#	`id` tinyint
#);
#这是非常简单的sql语句 , 超出返回就会报错
#insert into t3(id)values(-127);
#select * from t3;

#创建无符号的数据类型 0-255
#超过这个返回的数据是添加不进去,会报错的,所以在设计数据库的时候就要特别小心了
#create table `t4`(
#	`id` tinyint unsigned
#);
#insert into t4(`id`)values(254);
#select * from t4;

#如何定义一个无符号的整数
#create table t10(id tinyint)//默认是有符号的
#create table t11(id tinyint unsigned);无符号的

