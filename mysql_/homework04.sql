#根据:emp员工表 写出正确的sql语句
-- 13：显示不带有'R'的员工姓名
SELECT ename FROM emp WHERE ename NOT LIKE '%R%';
-- 14:显示所有员工姓名的前三个字符
SELECT LEFT(ename,3) FROM emp;
SELECT SUBSTRING(ename,1,3) FROM emp;
-- 15.显示所有员工的姓名,用a替换所有的A
SELECT REPLACE(ename,'A','a') FROM emp;
-- 16.显示满10年服务年限的员工的姓名和受雇日期
SELECT ename , hiredate FROM emp WHERE DATE_ADD(hiredate , INTERVAL 10 YEAR) <= DATE(NOW());
-- 17.显示员工的详细资料,按照姓名排序
SELECT * FROM emp ORDER BY ename DESC;
-- 18.显示员工的姓名和受雇日期,根据其服务年限,将最老的员工排在最前面
SELECT ename , hiredate FROM emp ORDER BY hiredate ASC;
-- 19.显示所有员工的姓名,工作和薪资,按照工作降序排序,若工作相同则按照薪金排序
SELECT ename , job , sal FROM emp ORDER BY job DESC , sal DESC;
-- 20.显示所有员工的姓名,加入公司的年份和月份,按受雇日期所在的月排序,若月份相同则将最早年份的员工排在最前面
SELECT ename , YEAR(hiredate) AS _year , MONTH(hiredate) AS _month FROM emp ORDER BY _month ASC , _year ASC;
-- 21.显示在一个月为30天的情况所有的员工的日薪金,忽略余数
SELECT ename , FLOOR((sal / 30)) AS avg_sal FROM emp;
SELECT ename , FLOOR(7800/30) AS avg_sal FROM emp;
-- 22.找出在(任何年份的)2月受聘的所有员工
SELECT ename , hiredate , MONTH(hiredate) AS _month FROM emp HAVING _month = 2;
SELECT ename , hiredate , MONTH(hiredate) AS _month FROM emp WHERE  MONTH(hiredate) = 2;
-- 23.对于每个员工,显示其加入公司的天数
SELECT ename , hiredate , DATEDIFF(NOW() , hiredate) AS day_nums FROM emp;
-- 24.显示姓名字段的任何位置包含A的所有的员工的姓名
SELECT ename FROM emp WHERE ename LIKE '%A%';
-- 25.以年月日的方式显示所有员工的服务年限
SELECT 
	ename , 
	hiredate , 
	CONCAT(
	FLOOR(TIMESTAMPDIFF(DAY,hiredate,DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%S')) / 365 ),'年',
	FLOOR((TIMESTAMPDIFF(DAY,hiredate,DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%S')) - 
	FLOOR(TIMESTAMPDIFF(DAY,hiredate,DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%S')) / 365 ) * 365) / 30),'月',
	FLOOR((TIMESTAMPDIFF(DAY,hiredate,DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%S')) - 
	FLOOR(TIMESTAMPDIFF(DAY,hiredate,DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%S')) / 365 ) * 365) % 30),'日') AS times
	FROM emp;