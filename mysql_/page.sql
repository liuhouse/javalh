#分页查询
-- 分页查询
-- 1.按照雇员的id号升序取出,每页显示3条记录,请分别显示第一页,第二页,第三页
-- 2.基本语法：select ... limit start , rows
-- 表示从start+1行开始取,取出rows行,start从0开始计算
-- 3.公式：
-- select * from emp order by empno limit 每页显示的记录数*(第几页 - 1) ， 每页显示的记录数

-- 第一页
SELECT * FROM emp ORDER BY empno LIMIT 0 , 3;
-- 第二页
SELECT * FROM emp ORDER BY empno LIMIT 3 , 3;
-- 第三页
SELECT * FROM emp ORDER BY empno LIMIT 6 , 3;

-- 推导出一个公式
SELECT * FROM emp ORDER BY empno LIMIT 每页显示的记录数 * (第几页 - 1) , 每页显示的记录数


