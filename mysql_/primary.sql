#约束 - 主键
-- 基本介绍
-- 约束用于确保数据库的数据满足特定的商业规则,在mysql中,约束包括,not null , unique , primary key , foreign key , 和check五中
-- primary key(主键) - 基本使用
-- 字段 字段类型 primary key
-- 用于唯一标识的表的行的数据,当定义主键约束后,该列不能重复


-- 主键使用
--	 id		name	email
CREATE TABLE t17(
	`id` INT PRIMARY KEY, -- 标识id列的主键
	`name` VARCHAR(32),
	`email`VARCHAR(32)
);

-- 主键列的值是不可以重复的
INSERT INTO t17 VALUES
	(1,'jack','jack@sohu.com');
INSERT INTO t17 VALUES
	(2,'tom','tom@sohu.com')
-- 插入失败,因为主键不能重复
INSERT INTO t17 VALUES
	(1,'hsp','hsp@suhu,com')

-- 主键的使用细节讨论
-- primary key 不能重复且不能为null  Column 'id' cannot be null
INSERT INTO t17 VALUES(NULL,'hsp','hsp@suhu.com');

-- 一张表最多只能有一个主键,但是可以是复合主键(比如 id+name)
CREATE TABLE t18(
	`id` INT PRIMARY KEY, -- 标识id列是主键
	`name` VARCHAR(32) PRIMARY KEY, -- PRIMARY KEY 是错误的,因为此表已经有一个主键了
	`email` VARCHAR(32)
);

-- 演示符合主键(id 和 name 做成复合主键)
CREATE TABLE t19(
	`id` INT,
	`name` VARCHAR(32),
	`email` VARCHAR(32),
	PRIMARY KEY (`id`,`name`) -- 这里就表示的是复合主键,id相同并且name相同的时候不能插入数据
);

-- 复合主键
INSERT INTO t19
	VALUES(1,'tom','tom@suhu.com');

INSERT INTO t19
	VALUES(2,'jack','jack@suhu.com');
	
-- 这个添加不进去  因为复合主键重复
INSERT INTO t19
	VALUES(1,'tom','_tom1@suhu.com');
	
-- 主键的指定方式 有两种
-- 1.直接在字段名后面指定：字段名 primary key
-- 2.在表定义最后写primary key(列名);

#第一种，在字段后面定义主键
CREATE TABLE t20(
	`id` INT PRIMARY KEY,
	`name` VARCHAR(32),
	`email` VARCHAR(32)
);

-- 第二种,在表的后面定义主键
CREATE TABLE t21(
	`id` INT,
	`name` VARCHAR(32),
	`email` VARCHAR(32),
	PRIMARY KEY(`id`) -- 在表定义的最后写 primary key(列表)
);

SELECT * FROM t21;

DESC t21;

SELECT * FROM t19;
	
	
SELECT * FROM t17;