#视图
-- 基本概念
-- 1.视图是一个虚拟表,其内容由查询定义,同真实的表一样,视图包含列,其数据来自于对应的真实表(基表)
-- 2.视图和基表的关系示意图
SELECT * FROM emp;

-- 视图的基本使用
/*
1.create view 视图名 as select 语句
2.alter view 视图名 as select 语句 -- 更新成新的视图
3.show create view 视图名
4.drop view 视图名1,视图名2
*/
-- 完成前面提出的需求
-- 创建一个视图 emp_view01,只能查询emp表的(empno , ename , job 和 deptno)信息

-- 视图的使用
-- 创建一个视图
CREATE VIEW emp_view01 AS SELECT empno , ename , job , deptno FROM emp;

-- 查看视图
DESC emp_view01;

SELECT * FROM emp_view01;

-- 查看创建视图的指令
SHOW CREATE VIEW emp_view01;

-- 删除视图
DROP VIEW emp_view01;


-- 视图的细节
-- 1 创建视图后,到数据库去看,对应视图只有一个视图结构文件(形式:视图名.frm)
-- 2 视图的数据变化会影响到基表,基表的数据变化也会影响到视图【insert update delete】

-- 修改视图 会影响到基表
UPDATE emp_view01 SET job='MANAGER' WHERE empno = 7369;

-- 查询基表
SELECT * FROM emp;

SELECT * FROM emp_view01;

-- 修改基表,会影响视图
UPDATE emp SET job='SALEMAN' WHERE empno=7369;

-- 3.视图中可以再使用视图,比如从emp_view01 视图中,选择出empno,和ename做出新视图
DESC emp_view01;

CREATE VIEW emp_view02 AS SELECT empno , ename FROM emp_view01;
DESC emp_view02;
SELECT * FROM emp_view02;





