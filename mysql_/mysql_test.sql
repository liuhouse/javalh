-- 商店售货系统表设计案例
/*
现有一个商店的数据库shop_db,记录客户以及其购物情况，由下面得到三个表组成：
商品goods（商品号,goods_id , 商品名goods_name ,单价unitprice,商品了别category,供应商provider）
客户customer(客户号customer_id , 姓名name,住址address,电邮email性别sex,身份证card_id)
购买purchase(购买订单号order_id,客户号customer_id,商品号goods_id,购买数量nums)
1:建表，在定义中要求声明[进行合理设计]：
(1) - 每个表的主外键
(2) - 客户的姓名不能为空值
(3) - 电邮不能重复
(4) - 客户的性别[男|女] check枚举
(5) - 单价unitprice在1.0 - 9999.99之间check
        
*/
CREATE DATABASE shop_db;

-- 创建商品表
-- 商品goods（商品号,goods_id , 商品名goods_name ,单价unitprice,商品类别category,供应商provider）
CREATE TABLE goods(
	goods_id INT PRIMARY KEY,
	goods_name VARCHAR(32)  UNIQUE,
	unit_price DOUBLE CHECK(unit_price >= 1.0 AND unit_price <= 9999.99),
	category INT,
	provider VARCHAR(32)
) CHARSET utf8 ENGINE=INNODB;
SELECT * FROM goods;

-- 创建客户表
-- 客户customer(客户号customer_id , 姓名name,住址address,电邮email性别sex,身份证card_id)
CREATE TABLE customer(
	customer_id INT PRIMARY KEY,
	`name` VARCHAR(20) NOT NULL DEFAULT '',
	address VARCHAR(50),
	email  VARCHAR(32) UNIQUE,
	sex CHAR(1) CHECK(sex IN('男','女')),
	cart_id VARCHAR(50) UNIQUE NOT NULL DEFAULT '' 
) ;
SELECT * FROM customer;

-- 创建订单表
-- 购买purchase(购买订单号order_id,客户号customer_id,商品号goods_id,购买数量nums)
CREATE TABLE purchase(
	order_id INT PRIMARY KEY,
	customer_id INT,
	goods_id INT,
	nums DOUBLE NOT NULL DEFAULT 0.00,
	-- 下面指定外键的关系
	FOREIGN KEY(`customer_id`) REFERENCES customer(`customer_id`),
	FOREIGN KEY(`goods_id`) REFERENCES goods(`goods_id`)
);
SELECT * FROM purchase;


DESC goods;
DESC customer;
DESC purchase;
