#数值型(bit)的使用
#细节说明 
#bit字段显示的时候,按照位的方式显示
#查询的时候仍然可以使用添加的数值进行查询
#如果一个值有0,1可以考虑使用bit(1),可以节约空间
#位类型,M指定位数,默认值为1，范围是1-64
#使用不多
#create table t05(num bit(8));
#insert into t05 values(0);
#select * from t05 where num = 1;