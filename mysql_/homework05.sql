#设学校环境如下：一个系有若干个专业,每个专业一年只招一个班,每个班有若干个学生,
#现在要建立关于系,学生,班级的数据库,关系模式为:
#班CLASS (班号classid , 专业名 subject , 系名deptname , 入学年份enrolltime,人数num)
#学生STUDENT (学号 studentid , 姓名name , 年龄age , 班号 classid)
#系DEPARTMENT(系号departmentid , 系名deptname)
#试用SQL语言完成下面的功能
#(1)建表,在定义中要求声明
# -- (1):每个表的主外码
# -- (2):deptname是唯一约束
# -- (3):学生姓名不能为空

#创建班级表
CREATE TABLE class(
	`classid` VARCHAR(32) PRIMARY KEY,
	`subject` VARCHAR(32) NOT NULL DEFAULT '',
	`deptname` VARCHAR(32) NOT NULL DEFAULT '',
	`enrolltime` VARCHAR(32),
	`num` INT NOT NULL DEFAULT 0,
	FOREIGN KEY(deptname) REFERENCES department(`deptname`)
) ENGINE = INNODB;
-- alter table class modify enrolltime varchar(20)
#给班级加入数据
INSERT INTO class VALUES
	(101 , '软件','计算机',1995,20),
	(102 ,'微电子','计算机',1996,30),
	(111 ,'无机化学','化学',1995,29),
	(112 , '高分子化学' , '化学' , 1995 , 25),
	(121 , '统计数学' , '数学' , 1995 , 20),
	(131 , '现代语言' , '中文' , 1996 , 20),
	(141 , '国际贸易' , '经济' , 1997 , 30),
	(142 , '国际金融' , '经济' , 1996 , 14)
DESC class;
DROP TABLE class;
SELECT * FROM class;

#创建系表
CREATE TABLE department(
	`departmentid` VARCHAR(32) PRIMARY KEY,
	`deptname` VARCHAR(32) UNIQUE NOT NULL DEFAULT ''
) ENGINE = INNODB;
DROP TABLE department;
#系 插入数据
INSERT INTO department VALUES
	(001 ,'数学' ),
	(002 , '计算机'),
	(003 , '化学'),
	(004 , '中文'),
	(005 , '经济')
	DROP TABLE department;
ALTER TABLE department MODIFY deptname VARCHAR(32) NOT NULL DEFAULT '';
DESC department;
SELECT * FROM department;

#创建学生表
#学生STUDENT (学号 studentid , 姓名name , 年龄age , 班号 classid)

CREATE TABLE student(
	`studentid` VARCHAR(32) PRIMARY KEY , 
	`name` VARCHAR(50) NOT NULL DEFAULT '',
	`age` INT NOT NULL DEFAULT 0 ,
	 `class_id` VARCHAR(32),
	 FOREIGN KEY(class_id) REFERENCES class(`classid`)
) ENGINE = INNODB;
DROP TABLE student;
-- 给学生添加数据
INSERT INTO student VALUES
	(8101 , '张三', 18  , 101),
	(8102 , '钱四' , 16 , 121),
	(8103 , '王玲' , 17 , 131),
	(8105 , '李飞' , 19 , 102),
	(8109 , '赵四' , 18 , 141),
	(8110 , '李可' , 20 , 142),
	(8201 , '张飞' , 18 , 111),
	(8302 , '周瑜' , 16 , 112),
	(8203 , '王亮' , 17 , 111),
	(8305 , '董庆' , 19 , 102),
	(8409 , '赵龙' , 18 , 101),
	(8510 , '李丽' , 20 , 142)
SELECT * FROM student;
DESC student;



-- 练习题
-- 完成以下查询功能
-- 1.找出所有姓李的学生
SELECT * FROM student WHERE `name` LIKE '李%';
-- 2.列出所有开设超过一个专业的系的名字
-- 系是主要的信息 , 1-查询出所有的系  2-查询出系中有专业的系
SELECT department.deptname,COUNT(class.subject) AS subject_num FROM department,class WHERE department.deptname = class.deptname GROUP BY department.deptname HAVING subject_num > 1;
DESC department;
-- 3.列出人数大于等于30的系的编号和名字
-- 分析，查询出所有的系, 查找出人数大于30的系作为子查询
SELECT departmentid AS '编号' , deptname AS '名称'  FROM department WHERE deptname IN(SELECT deptname FROM class WHERE num >= 30);
DESC department;

SELECT department.departmentid , tmp.* FROM department,(SELECT SUM(num) AS nums , deptname FROM class GROUP BY  deptname HAVING nums >=30) AS tmp
WHERE department.deptname = tmp.deptname;

-- 4.学校又新增加了一个物理系,编号为006
INSERT INTO department VALUES('008' , '牛逼系s');

SELECT * FROM department;

-- 学生张三退学,请更新相关的表
DELETE FROM student WHERE `name` = '张三';
UPDATE class SET num = num - 1 WHERE classid = 101;

-- 学生钱四退学
-- 思路
-- 1.班级数的人数 - 1
-- 2.删除钱四这个人

-- 开启事务
START TRANSACTION;

UPDATE class SET num = num - 1 WHERE classid = (SELECT class_id FROM student WHERE `name` = '钱四');
DELETE FROM student WHERE `name` = '钱四';

COMMIT;


SELECT * FROM student;
SELECT * FROM class;

