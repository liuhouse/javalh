SELECT
		o_detail.id AS '详情id',
		o_detail.order_id AS '所属订单id',
		client.client_abbre AS '门店名称',
		o_detail.pr_name AS '产品名称',
		o_detail.single_num AS '数量',
		o_detail.single_price AS '单价',
		o_detail.single_total AS '小计',
		FROM_UNIXTIME(o_detail.create_time, '%Y-%m-%d %H:%i:%s') AS '创建时间'
	FROM 
		bd_order_detail AS o_detail,
		bd_client AS CLIENT
	WHERE  
		o_detail.client_id = client.id 
	AND 
		o_detail.supplier_id = 705 
	AND 
		o_detail.create_time >= 1633017600
	ORDER BY o_detail.create_time DESC
		;