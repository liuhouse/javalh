-- 给用户授权
/*
  基本语法
  grant 权限列表 on 库.对象名 to '用户名'@'登录位置'[identified by '密码']
  说明：
  1,权限列表，多个权限用逗号分开
  grant select on ...
  grant select,delete,create on ...
  grant all [privilinges] on ... --表示赋予该用户在该对象上的所有权限
  
  2.特别说明
  *.*:表示本系统中所有的数据库的所有对象(表,视图,存储过程等)
  库.*:表示某个数据库中的所有数据对象(表,视图,存储过程等)
  3.identified by 可以省略，也可以写出
  (1).如果该用户存在,就是修改该用户的密码
  (2).如果该用户不存在,就是创建用户
  
  回收用户权限
  基本语法
  revoke 权限列表 on 库.对象名 from '用户名'@'登录位置'
  
  权限生效指令
  如果权限没有生效,可以执行下面的命令
  基本语法
  FLUSH PRIVILEGES
  
  
*/

-- 用户管理练习题
-- 1:创建一个用户(你的名字,拼音), 密码 123,并且只可以从本地登录,不让远程登录mysql
CREATE USER 'liuhao'@'localhost' IDENTIFIED BY '123';
-- 2:创建库和表testdb下的news表,要求,使用root用户创建
CREATE DATABASE testdb;
CREATE TABLE `news`(
	`id` INT,
	`name` VARCHAR(32)
) ENGINE=INNODB;
INSERT INTO news VALUES(1,'周杰伦'),(2,'王力宏');

SELECT * FROM news;
-- 3.给用户分配查看news表和添加数据的权限
GRANT SELECT,INSERT ON testdb.news TO 'liuhao'@'localhost';
-- 给用户分配修改的权限
GRANT UPDATE ON testdb.news TO 'liuhao'@'localhost';
-- 4.测试看看用户是否有这几个权限

-- 5.修改密码为abc，要求：使用root用户完成
ALTER USER 'liuhao'@'localhost' IDENTIFIED WITH mysql_native_password BY 'abc';
-- 6.重新登录
-- 7.使用root用户删除你的用户 
-- 8.演示回收权限
REVOKE SELECT , INSERT , UPDATE ON testdb.news FROM 'liuhao'@'localhost';

SELECT * FROM mysql.user;