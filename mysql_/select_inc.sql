#在前面我们讲过mysql表的基本查询,但是都是对一张表进行查询,在实际的软件开发中,还远远不够
#下面我们在讲解的过程中,将使用前面创建的三张表(emp , dept , salgrade) 为大家演示如何进行多表查询
-- 加强查询
-- 使用where子句
-- ?如何查找1992.1.1后入职的员工
-- 老师说明,在mysql中,日期类型可以直接比较,但是需要注意格式
SELECT * FROM emp WHERE hiredate > '1992-01-01';
-- 如何使用like操作符(模糊)
-- %:表示0到多个任意字符 _:表示单个任意字符
-- ? 如何显示首字母为S的员工的姓名和工资
SELECT * FROM emp WHERE ename LIKE 'S%';
-- ? 如何显示第三个字符大写O的所有的员工姓名和工资
SELECT ename,sal FROM emp WHERE ename LIKE '__O%';
-- 如何显示没有上级雇员的情况 -- 当判断一个值是null 的时候   要使用 字段 is null
SELECT * FROM emp WHERE mgr IS NULL;
-- 使用order by 子句
-- 如何按照工资的从低到高的顺序(升序)，显示雇员的信息
SELECT * FROM emp ORDER BY sal;
-- 按照部门号升序而雇员的工资降序排列,显示雇员信息
-- 按照两种排序,写在前面的先排,写在后面的后排
SELECT  * FROM emp ORDER BY deptno ASC,sal DESC; 
-- 查询表结构
DESC emp;
