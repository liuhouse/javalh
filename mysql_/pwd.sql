#加密和系统函数
 -- 演示加密函数和系统函数
 
 -- USER() 查询用户
 -- 可以看到登录到mysql的有哪些用户,以及登录的ip地址
 SELECT USER() FROM DUAL; -- 用户@IP地址
 -- DATABASE() 查询当前使用的数据库名称
 SELECT DATABASE() FROM DUAL;
 
 -- MD5(str)为字符串算出一个 MD5 32位的字符串,常用(用户密码)的加密
 -- root的密码是root -> 加密md5 -> 在数据库中存放的是加密后的密码
 SELECT MD5('root') FROM DUAL;
 -- 查询加密后的密码是不是32位的
 SELECT LENGTH(MD5('root')) FROM DUAL;
 
 -- 演示用户表,存放密码的时候是md5
 CREATE TABLE t16(
	`id` INT,
	`name` VARCHAR(32) NOT NULL DEFAULT '',
	`pwd` CHAR(32) NOT NULL DEFAULT ''
 );
 
 -- 给用户表插入数据  插入密码的时候是使用md5加密的
 INSERT INTO t16 VALUES(1,'降央卓玛',MD5('123456'));
 INSERT INTO t16 VALUES(2,'德德玛',MD5('987654'));
 
-- 查询数据  根据精准的条件查询数据   md5加密后的密码 在查询的时候同样要使用md5加密后进行查询
SELECT * FROM t16 WHERE `name` = '德德玛' AND pwd = MD5('987654'); 

-- PASSWORD(str) -- 加密函数,MYSQL数据库的用户密码就是PASSWORD函数加密的
SELECT SHA1('root') FROM DUAL;

-- mysql.user 表示 数据库.表   这样就不用专门的选择数据库了
SELECT * FROM mysql.user; -- 从原文密码的str计算并返回密码的字符串
-- 通常用于对mysql数据库的加密解密

 SELECT * FROM t16;
 
 
 
 