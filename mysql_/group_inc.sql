#使用分组函数和分组子句 group by
-- 增强group by 的使用
-- (1)显示每种岗位的雇员总数,平均工资
SELECT job,COUNT(*) , AVG(sal) emp_count,comm FROM emp GROUP BY job;
-- (2)显示雇员总数，以及获得补助的雇员数
-- 思路：获得补助的雇员数,就是comm列为非null,就是count(列),如果该值为null，是不会进行统计的
-- sql非常的灵活,需要我们动脑子
SELECT COUNT(*) , COUNT(comm) FROM emp;

-- 老师的扩展要求：统计没有获得补助的雇员数
-- 这里是相当的灵活了,判断如果当前的补助是NULL的话,就显示1,否则显示NULL ，因为按照列统计的话,只会统计
-- 不是NULL的,所以这里只会统计为1的,而这里为1的就是为NULL的,逆向思维
SELECT COUNT(*),COUNT(IF(comm IS NULL , 1,NULL)) FROM emp;
SELECT COUNT(*),(COUNT(*) - COUNT(comm)) AS comm_not FROM emp;

-- (3)显示管理者的总人数,小技巧：尝试写->修改->尝试[正确的]
-- DISTINCT 对字段进行去重操作
SELECT COUNT(DISTINCT mgr) FROM emp;

-- (4) 显示雇员工资的最大差额
-- 思路： max(sal) - min(sal)
SELECT MAX(sal) - MIN(sal) FROM emp;

SELECT * FROM emp;
SELECT * FROM dept;

-- 应用案例：请统计各个部门 group by 的平均工资avg
-- 并且是大于2000的having , 并按照平均工资从高到低进行排序,order by
-- 取出前两个记录 limit 0 , 2
SELECT `deptno`,AVG(sal) AS avg_sal 
	FROM emp 
	GROUP BY deptno 
	HAVING avg_sal >2000 
	ORDER BY avg_sal DESC 
	LIMIT 0,2;




SELECT * FROM emp;
DESC emp;