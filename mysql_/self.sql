#自连接
-- 自连接是指在同一张表的连接查询【将同一张表看做成两张表】
-- 多表查询  自连接
-- 思考题：显示公司员工的名字和他上级的名字
-- 老韩分析 ： 员工名字是在emp , 上级领导的名字是在emp
-- 员工和上级是通过emp的mgr列关联
-- 这里老师小结
-- 自连接的特点：
-- 1.把一张表当做两张表使用
-- 2.需要给表取别名 表明 as 表别名
-- 3.列名不明确,可以指定 列的别名,列名 as 列的别名

-- 关键点
-- 两张表的查询,必须起别名
-- 相同的字段必须起别名
-- 条件是最重要的

SELECT worker_emp.ename AS 'worker_ename' , boss_emp.ename AS 'boss_ename'  
	FROM 
	emp AS worker_emp ,
	emp AS boss_emp
	WHERE
	worker_emp.mgr = boss_emp.empno;

SELECT * FROM emp;


SELECT 
	one_private_cate.private_class_name AS one_class_name,
	two_private_cate.private_class_name AS two_class_name
	FROM 
	bd_private_cate AS two_private_cate,
	bd_private_cate AS one_private_cate 
	WHERE
	two_private_cate.private_class_belong = one_private_cate.private_one_class 
	AND
	two_private_cate.supplier_id = 65  AND one_private_cate.supplier_id = 65
	AND two_private_cate.private_class_belong != 0
	;
	
	
	
	
SELECT 
two.id,
two.private_class_name,
two.private_class_belong,
ones.private_class_name,
ones.private_one_class,
ones.id
FROM 
bd_private_cate AS two ,
bd_private_cate AS ones
WHERE
two.private_class_belong=ones.private_one_class
AND
 two.supplier_id = 65 
AND ones.supplier_id =65
AND two.private_class_belong != 0
;

#-----------------------------------------正确的查询方式---------------------------------

SELECT
 _two.private_class_name AS '二级分类名称',
 _one.private_class_name AS '一级分类名称'
  FROM 
  bd_private_cate AS _two,
  bd_private_cate AS _one
  WHERE _two.private_class_belong = _one.private_one_class
  AND _two.supplier_id = 65 AND _one.supplier_id=65 AND _two.private_class_belong != 0;
  
  
SELECT * FROM bd_private_cate WHERE  supplier_id=65 AND private_class_belong != 0;
