#6根据：emp员工表,写出正确的sql语句
-- 1：选择部门30中的所有员工
SELECT ename FROM emp WHERE deptno = 30;
-- 2:列出所有办事员(CLERK)的姓名,编号,和部门编号
SELECT ename , empno , deptno FROM emp WHERE job = 'CLERK';
-- 3:找出佣金高于薪金的员工
SELECT ename , sal ,comm FROM emp WHERE comm IS NOT NULL AND comm > sal;
-- 4:找出佣金高于薪金60%的员工
SELECT ename , sal , comm FROM emp WHERE comm IS NOT NULL AND comm > (sal * 0.6); 
-- 5.找出部门10中所有经理(MANAGER)和部门20中所有的办事员(CLERK)的详细信息
SELECT * FROM emp WHERE (deptno = 10 AND job = 'MANAGER') OR (deptno = 20 AND job = 'CLERK');

-- 6.找出部门10中的所有经理(MANAGER),部门20中的所有办事员(CLERK),还有既不是经理又不是办事员
-- 但是薪金大于或者等于2000的所有员工的详细资料
SELECT * FROM emp
	WHERE (deptno = 10 AND job = 'MANAGER')
	OR(deptno = 20 AND job = 'CLERK')
	OR((job != 'MANAGER' AND job != 'CLERK') AND (sal >= 2000));
-- 7:找出收取佣金的员工的不同工作
SELECT job FROM emp WHERE comm IS NOT NULL GROUP BY job;
SELECT DISTINCT job FROM emp WHERE comm IS NOT NULL;
-- 8.找出不收取佣金或者收取佣金低于100的员工
SELECT ename,comm FROM emp WHERE (comm IS NULL) OR ((comm IS NOT NULL) AND (comm < 100)); 
-- 9.找出各月倒数第三天受雇的所有员工  [百度解决]
-- 思路分析：查询出每个月的最后一天 - 2 = 每个月的倒数第三天 -- 虽然我自己没有解决,但是通过百度解决了,ok
SELECT ename FROM emp WHERE hiredate = LAST_DAY(hiredate) - 2;
SELECT ename , LAST_DAY(hiredate) AS hiredate_last FROM emp;

-- 10.找出早于12年前受雇的员工
SELECT ename , hiredate , DATE(DATE_SUB(NOW() , INTERVAL 12 YEAR)) AS time_last30 FROM emp WHERE hiredate < DATE(DATE_SUB(NOW() , INTERVAL 12 YEAR));
SELECT * FROM emp WHERE DATE_ADD(hiredate , INTERVAL 12 YEAR) < NOW()  
-- 11.以首字母小写的方式显示所有员工的姓名
SELECT CONCAT(LCASE(LEFT(ename,1)),SUBSTRING(ename , 2)) AS ename_  FROM emp;
SELECT REPLACE(ename,LEFT(ename,1),LCASE(LEFT(ename,1))) AS ename_ FROM emp;

-- 12.显示正好为5个字符的员工的姓名
SELECT ename FROM emp WHERE LENGTH(ename) = 5;
DESC emp;


