-- 辅助单位表的创建
CREATE TABLE bd_auxiliary_unit(
	`id` INT PRIMARY KEY AUTO_INCREMENT COMMENT '自增id',
	`good_supplier_id` INT NOT NULL DEFAULT 0 COMMENT '产品对应供应商的id',
	`sub_unit` SMALLINT(4) NOT NULL DEFAULT 0 COMMENT '副单位',
	`sub_unit_num` VARCHAR(50) NOT NULL DEFAULT '1' COMMENT '副单位的数量',
	`main_unit` SMALLINT(4) NOT NULL DEFAULT 0 COMMENT '主单位|最小单位',
	`main_unit_num` VARCHAR(50) NOT NULL DEFAULT '0',
	`is_min_unit` SMALLINT(1) NOT NULL DEFAULT 0 COMMENT '是否是最小单位 0：否 1：是   最小单位也当做辅助单位,但是只能添加一次',
	`is_cai_unit` SMALLINT(1) NOT NULL DEFAULT 0 COMMENT '是否是采购单位,0:否  1:是',
	`business_id` INT NOT NULL DEFAULT 0 COMMENT '所属的企业id',
	FOREIGN KEY(`good_supplier_id`) REFERENCES bd_goods_supplier_price(`id`)
) CHARSET=utf8 ENGINE=INNODB COMMENT='辅助单位的设置';

ALTER TABLE bd_auxiliary_unit ADD sub_price VARCHAR(32) NOT NULL DEFAULT 0;
DELETE FROM bd_auxiliary_unit;

DROP TABLE bd_auxiliary_unit;

SELECT * FROM bd_auxiliary_unit;
SELECT * FROM bd_goods_supplier_price WHERE id = 21282;



SHOW TABLES;
DESC bd_goods_supplier_price;
ALTER TABLE bd_goods_supplier_price ADD is_set_aux SMALLINT(1) NOT NULL DEFAULT 0 COMMENT '是否设置过单位,设置过的想再次设置,必须初始化  0：未设置过  1：设置过';
ALTER TABLE bd_goods_supplier_price ADD order_unit SMALLINT(4) NOT NULL DEFAULT 0 COMMENT '下单单位'


SELECT * FROM bd_goods_supplier_price WHERE id = 6188;


ALTER TABLE bd_supplier ADD is_class_stock SMALLINT (1) NOT NULL DEFAULT 0 COMMENT '0 关闭 1开启';
DESC bd_supplier;


-- 仓库表的设计
 
CREATE TABLE bd_supplier_warehouse(
	`id` INT PRIMARY KEY AUTO_INCREMENT COMMENT '仓库id',
	`business_id` INT NOT NULL DEFAULT 0 COMMENT '所属企业id',
	`warehouse_name` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '仓库名称',
	`status` SMALLINT(1) NOT NULL DEFAULT 1 COMMENT '仓库状态 0:暂停 1:正常' 
) CHARSET = utf8 ENGINE=INNODB;

-- 给仓库设置索引
CREATE INDEX business_id_index ON bd_supplier_warehouse(business_id);
DESC bd_supplier_warehouse;


SELECT * FROM bd_supplier_warehouse;


-- 添加所属仓库的商品
CREATE TABLE bd_belong_warehouse_goods(
	`good_id` VARCHAR(100) NOT NULL DEFAULT 0 COMMENT '商品id',
	`warehouse_id` INT NOT NULL DEFAULT 0 COMMENT '仓库id',
	`business_id` INT NOT NULL DEFAULT 0 COMMENT '所属企业的id'
) CHARSET=utf8 ENGINE=INNODB;
CREATE INDEX warehouse_id_index ON bd_belong_warehouse_goods(`warehouse_id`);
CREATE INDEX business_id_index ON bd_belong_warehouse_goods(`business_id`);

SELECT * FROM bd_belong_warehouse_goods;

DESC bd_belong_warehouse_goods;



-- 操作模板
SELECT template_id FROM bd_supplier ORDER BY id DESC LIMIT 10;

SELECT * FROM bd_template ORDER BY id DESC LIMIT 10;







