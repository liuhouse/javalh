  -- mysql的事务隔离级别 -- 案例

  /*
	Mysql隔离级别(4种)		脏读		不可重复		幻读		加锁读
	读未提交(Read uncommitted)	 √		   √			  √   		不加锁
	读已提交(Read committed)	 x		   √			  √		不加锁
	可重复读(Repeatable read)	 x		   x			  x       	不加锁
	可串行化(Serializable) 		 x		   x			  x		加锁
	
  */
  
   -- 我们举例一个案例来说明mysql的事务隔离级别,以对account表操作为例(id , name , money)
   -- 1:打开两个mysql的控制台
   -- 2:查看当前会话隔离级别
       -- 方式1
       SELECT @@transaction_isolation
       -- 方式2
       SHOW VARIABLES LIKE 'transaction_isolation';
       
   -- 3.把其中一个控制台的隔离级别设置为READ-UNCOMMITTED
   SET transaction_isolation='READ-UNCOMMITTED';
   -- 4.创建表
   CREATE TABLE `account`(
	id INT,
	`name` VARCHAR(32),
	`money` INT
   ) ENGINE=INNODB;
   
   
   
   -- 
   
   -- 4.创建表
   
   -- https://blog.csdn.net/hhc9_9/article/details/109394221   对于隔离级别的理解