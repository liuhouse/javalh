-- 用户修改密码
-- 修改自己的密码
ALTER USER 'lh_1'@'localhost' IDENTIFIED WITH mysql_native_password BY 'abcdefgg';

-- 修改他人的密码

-- Access denied; you need (at least one of) the CREATE USER privilege(s) for this operation

-- 权限不够
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '123456';
