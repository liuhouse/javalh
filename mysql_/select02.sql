#使用表达式对查询的列进行运算
#1.统计每个学生的总分
SELECT `name`,(chinese+english+math) FROM student;
#2.给所有的学生总分加10分的情况
SELECT `name`,(chinese+english+math+10) FROM student;
#3.使用别名表示学生分数
SELECT `name`,(chinese+english+math+10) AS total_score FROM student;


#课后练习
#1.在赵云的总分上增加60%
SELECT `name`,(chinese+english+math) * 1.6 AS total_score FROM student WHERE `name` = '赵云'; 
#2.统计关羽的总分
SELECT  `name`,(chinese+math+english) AS total_score FROM student WHERE `name` = '关羽';
#3.使用别名表示学生的数学分数
SELECT `name`,`chinese`,`math` AS `myMath`,`english` FROM student;