#mysql表子查询
-- 什么是子查询
-- 子查询是指嵌入在其他sql语句的select语句,也叫嵌套查询
-- 单行子查询
-- 单行子查询是指只返回一行数据的子查询语句
-- 请思考,如果显示与SMITH同一部门的所有员工

/*
	1.先查询到SMITH的部门号
	2.把上面的select语句当做一个子查询来使用
*/

-- 查询出SMITH 的部门号
SELECT deptno FROM emp WHERE ename = 'SMITH';
-- 将上面的sql语句当做条件的值使用   子查询
SELECT * FROM emp WHERE deptno = (SELECT deptno FROM emp WHERE ename = 'SMITH');

SELECT * FROM emp WHERE deptno = 20;

-- 课堂练习
-- 如何查询和部门10的工作相同的雇员的  名字,岗位,工资,部门号,但是不包含10号部门自己的雇员

/*
	思路分析
	1.查询出10号部门有哪些工作
	2.把上面的查询结果当做子查询来进行使用
*/
--  1.查询出10号部门有哪些工作
SELECT DISTINCT job FROM emp WHERE deptno = 10;

-- 2.把上面的查询结果当做子查询来进行使用
SELECT ename , deptno , job , sal FROM emp WHERE job IN(
	SELECT DISTINCT job FROM emp WHERE deptno = 10
) AND deptno <> 10;

