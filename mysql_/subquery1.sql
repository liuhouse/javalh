-- 子查询练习
-- 请思考：查找每个部门工资高于本部门的平均工资的人的资料
-- 这里要用到数据查询的小技巧,把一个子查询当做一个临时表来使用
-- 分析
-- 先查询出各个部门的部门号和平均工资
-- 将上面的查询当做一个临时表来使用,然后查询出人的工资高于部门的平均工资   条件是两个    人的部门= 部门id AND 人的sal > 部门平均工资
SELECT deptno , AVG(sal) AS avg_sal FROM emp GROUP BY deptno;
SELECT 
	emp.ename,
	emp.sal,
	avg_sal,
	emp.deptno
	FROM
	(SELECT deptno , AVG(sal) AS avg_sal FROM emp GROUP BY deptno) AS emp_tmp , emp
	WHERE emp_tmp.deptno = emp.deptno AND emp.sal > emp_tmp.avg_sal;
	


