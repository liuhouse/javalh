#合计/统计函数
#count  返回行的总数
#演示mysql的统计函数的使用
-- 统计,一个班级一共有多少学生
SELECT COUNT(*) FROM student;
-- 统计数学成绩大于90的学生有多少个
SELECT COUNT(*) FROM student WHERE math > 90;
-- 统计总分大于250的人数有多少个
SELECT COUNT(*) FROM student WHERE (english + math + english) > 250;

-- count(*) 和 count(列)的区别
-- 解释:count(*) 返回满足条件的记录行数
-- count(列)：统计满足条件的某列有多少个,但是会排除为Null的情况
CREATE TABLE t15(
	`name` VARCHAR(20)
);


-- delete from t15;

INSERT INTO t15 VALUES('tom');
INSERT INTO t15 VALUES('jack');
INSERT INTO t15 VALUES('marry');
INSERT INTO t15 VALUES(NULL);

SELECT * FROM t15;

#使用*进行统计
#查询t15表一共有多少条记录  查询出了所有的
SELECT COUNT(*) FROM t15; -- 4
#使用列(字段)进行统计  -- 排除了值为null的列,其他的都统计到了
SELECT COUNT(`name`) FROM t15; -- 3

-- 演示sum的使用
-- 统计一个班级数学的总成绩
SELECT SUM(math) FROM student;
-- 统计一个班级语文,英语,数学各科的总成绩
SELECT SUM(math) total_math_score, SUM(english) total_english_score , SUM(chinese) total_chinese_score FROM student; 

-- 统计一个班级语文,英语,数学的成绩总和
SELECT SUM(chinese + english + math) FROM student;

-- 统计一个班级语文成绩的平均分 ， 统计都是相对于数值类型的数据进行统计的
SELECT SUM(chinese)/COUNT(*) AS avg_score FROM student;

-- 演示avg的使用
-- 练习：
-- 求一个班级的数学的平均分?
SELECT AVG(math) AS avg_math_score FROM student;
-- 求一个班级总分的平均分
SELECT AVG(math+english+chinese) AS avg_total_score FROM student;

-- 演示max 和 min 的使用
-- 求班级的最高分和最低分(数值范围在统计中特别的有用)
SELECT MAX(math+english+chinese) , MIN(math+english+chinese) FROM student;

-- 求出班级的数学成绩的最高分和最低分
SELECT MAX(math) AS math_max , MIN(math) AS math_min FROM student; 

#sum
-- sum函数返回满足where条件的行的和,一般使用在数值列
-- 演示sum函数的使用
-- 统计一个班级数学的总成绩
SELECT SUM(math) FROM student;
-- 统计一个班级语文的总成绩
SELECT SUM(chinese) FROM student;

-- 统计一个班级,语文,英语,数学各科的总成绩
SELECT SUM(chinese) AS chinese_score , SUM(math) AS math_score , SUM(english) AS  english_score FROM student;

-- 统计一个班级语文,英语,数学成绩的总和
SELECT SUM(math+english+chinese) AS total_score FROM student;

-- 统计一个班级语文成绩的平均分
SELECT SUM(chinese)/COUNT(*) AS chinese_avg FROM student;

# avg
-- avg函数返回满足where条件的一列的平均值






