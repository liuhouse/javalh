#7.根据：emp表，dept部门表,工资 = 薪金sal + 佣金comm写出正确的sql语句
-- (1)列出至少有一个员工的所有部门
SELECT DISTINCT dept.deptno,dname FROM dept , emp WHERE dept.deptno = emp.deptno ;
SELECT * FROM dept;
-- (2).列出薪金比"SMITH"多的员工
SELECT ename,sal FROM emp WHERE sal > (SELECT sal FROM emp WHERE ename = 'SMITH') AND ename != 'SMITH';
-- (3).列出受雇日期晚于其上级的所有员工
-- 这里需要使用两张临时表来进行查询 1.所有的员工表,2，所有的上级表
SELECT emp_yuan.ename , emp_yuan.hiredate yuan_hiredate , emp_shang.hiredate shang_hiredate  FROM emp emp_yuan , emp emp_shang WHERE  emp_yuan.mgr = emp_shang.empno AND emp_yuan.hiredate > emp_shang.hiredate;
 SELECT * FROM emp;
DESC emp;
-- (4)列出部门名称和这些部门的员工信息,同时列出哪些没有员工的部门
-- 分析,部门是主导的.所以部门是必须显示的,这里使用left join 将部门作为主导
SELECT dname,ename FROM dept LEFT JOIN emp ON dept.deptno = emp.deptno;

-- (5)列出所有"CLERK"(办事员)的姓名以及其部门的名称
-- 分析,先列出所有的办事员的姓名,这里是以员工为主导的 ， 然后left join 部门 查询出部门信息
SELECT ename,dname FROM emp LEFT JOIN dept ON emp.deptno = dept.deptno  WHERE emp.job = 'CLERK';

-- (6)列出最低薪金大于1500的各种工作
SELECT  job , MIN((sal + IF(comm IS NULL , 0 , comm))) AS min_sal FROM emp GROUP BY job HAVING min_sal > 1500;

-- (7).列出部门"SALES"(销售部)工作的员工的姓名
SELECT ename,deptno FROM emp WHERE deptno = (SELECT deptno FROM dept WHERE dname = 'SALES');
SELECT * FROM dept WHERE deptno = 30;

-- (8).列出薪金高于公司平均薪资的所有员工
-- 化繁为简  各个击破
SELECT ename , sal FROM emp WHERE sal > (SELECT AVG(sal) FROM emp);

-- (9).列出与"SCOTT"从事相同工作的所有员工
SELECT * FROM emp WHERE job = (SELECT job FROM emp WHERE ename = 'SCOTT');
-- (10).列出薪金高于在部门30工作的所有的员工的薪金的工作姓名和薪金
-- 1.先得到部门30 工作的所有员工的最高薪金
-- 2.再列出所有的比 结果1 得到的薪金高的  就是符合要求的
SELECT * FROM emp WHERE sal > (SELECT MAX(sal) FROM emp  WHERE deptno = 30);
-- (11)列出在每个部门工作的员工数量,平均工资和平均服务期限
SELECT COUNT(deptno) AS deptno_num , FLOOR(AVG(sal)) AS sal_avg, FLOOR(AVG(DATEDIFF(NOW() , hiredate)) / 365) FROM emp GROUP BY deptno;
-- (12)列出所有员工的姓名,部门名称和工资
SELECT ename, emp.deptno,sal,dname FROM emp LEFT JOIN dept ON emp.deptno = dept.deptno;
-- (13)列出所有部门的详细信息和部门人数
SELECT dept.*,COUNT(emp.deptno) AS deptno_num FROM dept LEFT JOIN emp ON dept.deptno = emp.deptno GROUP BY dept.deptno ORDER BY dept.deptno ASC;
-- (14).列出各种工作的最低工资
SELECT job , MIN(sal) AS min_sal FROM emp GROUP BY job;
-- (15).列出MANAGER(经理)的最低薪金
SELECT ename,MIN(sal) AS min_sal FROM emp WHERE job = 'MANAGER';
-- (16).列出所有员工的年工资,按年薪从低到高排序
SELECT ename , (sal + IF(comm IS NULL , 0 , comm)) * 12 AS year_sal FROM emp ORDER BY year_sal DESC;
SELECT ename , (sal + IFNULL(comm,0)) * 12 AS year_sal FROM emp ORDER BY year_sal DESC;
DESC emp;

SELECT * FROM bd_goods ORDER BY id DESC LIMIT 10;


