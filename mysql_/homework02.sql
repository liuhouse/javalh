#2.写出查看dept表和emp表的结构
DESC dept;
DESC emp;

SHOW INDEXES FROM dept;

SHOW INDEX FROM dept;

#3.使用简单的查询语句完成
-- (1)显示所有的部门名称
SELECT dname FROM dept;
-- (2)显示所有雇员名以及全年收入 13月(工资+补助),并指定列名的别名为"年收入"
SELECT ename , (sal + IF(comm IS NULL , 0.0 , comm))* 13   AS '年收入' FROM emp;

#4.限制查询条件
-- (1)显示工资超过2850的雇员姓名和工资
SELECT ename , sal FROM emp WHERE sal > 2850;
-- (2)显示工资不在1500到2850之间的所有雇员名以及工资
SELECT ename , sal FROM emp WHERE sal NOT BETWEEN 1500 AND 2850;
-- (3)显示编号为7566的雇员姓名以及所在的部门编号
SELECT ename , deptno FROM emp WHERE empno = 7566;
-- (4)显示部门10和30中的工资超过1500的雇员姓名以及工资
SELECT deptno ,ename , sal FROM emp WHERE deptno IN(10,30) AND sal > 1500;
-- (5)显示无管理者的雇员名以及岗位
SELECT ename , job FROM emp WHERE mgr IS NULL;

#5排序数据
-- (1)显示在1991年2月1日到1991年5月1日之间所有的雇员名,岗位以及雇佣日期,以及雇佣日期进行排序
SELECT ename , hiredate , job FROM emp WHERE hiredate BETWEEN '1991-2-1' AND '1991-5-1' ORDER BY hiredate DESC;
-- (2)显示获得补助的所有的雇员名,工资以及补助,并按照工资降序排列
SELECT ename , sal , comm FROM emp WHERE comm IS NOT NULL ORDER BY sal DESC;



SELECT * FROM emp;

