#时间日期相关函数
CREATE TABLE mes(
	id INT,
	content VARCHAR(30),
	send_time DATETIME
);

#添加数据
INSERT INTO mes VALUES(1 , '北京新闻' , CURRENT_TIMESTAMP());
INSERT INTO mes VALUES(2 , '上海新闻' , NOW());
INSERT INTO mes VALUES(3 , '广州新闻' , NOW());

-- 当前日期 【年月日】 current_date()
SELECT CURRENT_DATE() FROM DUAL;
-- 当前时间 【时分秒】current_time()
SELECT CURRENT_TIME() FROM DUAL;
-- CURRENT_TIMESTAMP() 当前时间戳
SELECT CURRENT_TIMESTAMP() FROM DUAL;
-- 查询当前的时间戳 年月日时分秒
SELECT NOW() FROM DUAL;
SELECT * FROM mes;

-- 上应用实例
-- 显示所有新闻信息,发布日期只能显示日期,不用显示时间
-- date() 函数  只显示日期  不显示时间
SELECT id , content , DATE(send_time) FROM mes;
-- 请查询10分钟内发布的新闻,思路梳理
-- 加10分钟大于等于现在的时候  就证明是十分钟之内的   加10分钟如果还是小于当前的时间,就证明消息超过十分钟了
-- 下面的两种方式是一样的
-- 大于等于现在的时间  证明是现在发布的  1分钟之内   大于等于现在的时间 - 10   证明是十分钟之内
SELECT * FROM mes WHERE DATE_ADD(send_time , INTERVAL 10 MINUTE) >= NOW();
SELECT * FROM mes WHERE send_time >= DATE_SUB(NOW() , INTERVAL 10 MINUTE);

-- 请在mysql的sql语句中求出2011-11-11 和 1990-1-1相差多少天
SELECT DATEDIFF('2011-11-11' , '1990-1-1') FROM DUAL;

-- 请用mysql的sql语句求出你活了多少天
SELECT DATEDIFF(NOW() , '1995-12-06') FROM DUAL;

-- 如果能活80岁，求出你还能活多少天  
-- 先求出活80岁,是什么日期x
-- 然后在datediff(x , now()); 1986-11-11 -> datetime
-- INTERVAL 80 YEAR :YEAR可以是年月日,时分秒
-- ‘1995-12-06’ 可以date , datetime , timestamp

-- 活80岁 是个什么日期 2075 - 12 -06
SELECT DATE_ADD('1995-12-06',INTERVAL 80 YEAR) FROM DUAL;
-- 计算跟现在的差值
SELECT DATEDIFF(DATE_ADD('1995-12-06',INTERVAL 80 YEAR) , NOW()) FROM DUAL;

-- 计算两个时间之间的差值
SELECT TIMEDIFF('10:11:11' , '06:10:10') FROM DUAL;

-- YEAR|Month|DAY|DATE(datetime)

-- 获取年
SELECT YEAR(NOW()) FROM DUAL;
-- 获取月
SELECT MONTH(NOW()) FROM DUAL;
-- 获取日
SELECT DAY(NOW()) FROM DUAL;

-- 根据固定的时间格式获取
SELECT MONTH('2021-10-13') FROM DUAL;

-- 获取时间戳的毫秒数 
-- unix_timestamp() : 返回的是 1970-1-1 到现在的秒数
SELECT UNIX_TIMESTAMP() FROM DUAL;

-- FROM_UNIXTIME() : 可以把一个unix_timestamp 秒数(时间戳),转成指定格式的日期
-- %Y-%m-%d 格式是规定好的,表示年月日
SELECT FROM_UNIXTIME(1634108043 , '%Y-%m-%d') FROM DUAL;
# 将时间戳格式化 年月日   时分秒
SELECT FROM_UNIXTIME(1634108043 , '%Y-%m-%d %H:%i:%s') FROM DUAL;

SELECT * FROM mysql.user \G;






