#在多行子查询中使用all操作
-- 请思考：显示工资比部门30的所有员工工资高的员工姓名,工资和部门号
-- 思路
-- 1.先查询出30号部门所有员工的工资
-- 2.将上面的语句当做子查询来进行处理
-- ALL的使用
SELECT ename,sal,deptno FROM emp WHERE sal > ALL(SELECT sal FROM emp WHERE deptno = 30);
-- 扩展要求：大家想想还有没有别的查询方法
-- 思路.想要查询比30号部门所有的员工的工资都高,那么只需要查询出30号部门的最高工资, 然后当做条件,只要是大于 30号员工工资最大的,那么就比30号里面的
-- 员工工资都要大

SELECT ename, sal , deptno FROM emp WHERE sal > (SELECT MAX(sal) FROM emp WHERE deptno = 30);

-- 请思考：如何显示工资比部门30的其中一个员工工资高的员工的姓名,工资和部门号
-- 思路
-- 1.先查询出部门30的所有员工的工资
-- 2.再使用any查询出任意一个比这些员工高的
SELECT ename,sal,deptno FROM emp WHERE sal > ANY(SELECT sal FROM emp WHERE deptno = 30);
-- 第二种写法 -- 只要大于30号部门最小的就符合条件
SELECT ename , sal , deptno FROM emp WHERE sal > (SELECT MIN(sal) FROM emp WHERE deptno = 30);
