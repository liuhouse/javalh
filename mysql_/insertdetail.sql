#添加的细节说明 insertdetail.sql
#说明 insert语句的细节
-- 1：插入的数据应该与字段的数据类型相同
	-- 比如 把'abc'添加到int类型错误
INSERT INTO `goods` (id,goods_name,price)VALUES('5','小米手机',2000);

-- 2：数据的长度应该在列的规定范围内,例如：不能将一个长度为80的字符串加入到长度为40的列中
	-- 下面的数据是不能添加成功的,因为goods_name的字段的值太长,超出了范围
INSERT INTO `goods`(id , goods_name,price)VALUES(6,'vivo手机vivo手机vivo手机vivo手机',3000);

-- 3：在values中列出的数据位置必须与被加入的列的排列位置相对应
INSERT INTO `goods`(id,goods_name,price) VALUES('vivo手机',6,2000); -- 不对的

-- 4：字符和日期型的数据应该包含在单引号中
INSERT INTO `goods`(id , goods_name,price) VALUES(7,vovo手机,3000); -- 这里是错误的, vivo手机   应该是  'vivo手机'

-- 5：列可以插入空值【前提是该字段允许为空】,insert into table value(null)
INSERT INTO `goods`(id,goods_name,price) VALUES(40,'vivo手机',NULL);

-- 6:insert into tab_name(列名..) values(),(),() 添加多条记录  批量添加数据
INSERT INTO `goods`(id,goods_name,price)
	VALUES
	(41,'三星手机第一部',1000),
	(42,'三星手机第二部',2000),
	(43,'三星手机第三部',3000);
	
-- 7.如果是给表中的所有字段添加数据,可以不写前面的字段的名称，但是也必须跟数据库中的字段是一一对应的
INSERT INTO `goods` VALUES(44,'IBM手机',4000);

-- 8.默认值的使用,当不给某个字段值的时候,如果有默认值就会添加默认值,否则会报错
	-- 如果某个列,没有指定not null , 那么再添加数据的时候,没有给定值的时候,则会默认给null
	-- 如果我们希望指定某个列的默认值,可以在创建表的时候指定
INSERT INTO `goods` (id,goods_name)VALUES(45,'格力手机');

-- 创建一张带有默认值的表2
CREATE TABLE `goods2`(
	`id` INT,
	`goods_name` VARCHAR(32),
	`price` DOUBLE NOT NULL DEFAULT 100
) CHARACTER SET utf8 COLLATE utf8_bin ENGINE INNODB;

INSERT INTO `goods2` (id , goods_name) VALUES(1,'格力手机');

SELECT * FROM goods2;


