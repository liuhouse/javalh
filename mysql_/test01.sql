
CREATE TABLE test_dept LIKE dept;
INSERT INTO test_dept SELECT * FROM dept;
SELECT * FROM test_dept;

CREATE TABLE test_emp LIKE emp;
INSERT INTO test_emp SELECT * FROM test_emp;


CREATE TABLE test_salgrade LIKE salgrade; 
INSERT INTO test_salgrade SELECT * FROM salgrade;

-- 典型的空间换时间  加上索引之后表的数据会非常的大   但是查询速度确实很快 
 -- 如果想查询速度非常快的话,加上索引是最好的选择
 -- CREATE INDEX index_name ON table(table_field);

-- 没有创建索引的时候 查询出来的时间是 28 秒
SELECT * FROM test_emp WHERE ename = 'SMITH';
SELECT * FROM test_emp WHERE ename = 'SMITHS';

-- 没有创建sal对应的索引的时候,查询时间是13s
SELECT * FROM test_emp WHERE sal = 801.00;
-- 创建sal索引
-- 创建索引之后查询时间是0
CREATE INDEX sal_index ON test_emp(sal);

-- 使用索引来优化一下,体验索引的牛逼
-- 在没有创建索引前,test_emp.ibd 文件大小是 1110M  加上索引后是140M 增加了300M
-- 创建索引后test_emp.ibd 文件大小是1400M,【索引本身也是会占用空间的】
-- 

CREATE INDEX ename_index ON test_emp(ename);
