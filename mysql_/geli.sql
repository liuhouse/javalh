#mysql事务隔离级别
-- 事务隔离级别介绍
/*
   1.多个连接开启各自事务操作数据库中的数据的时候,数据库系统要负责隔离,以保证各个连接在获取数据时候的准确性
   2.如果不考虑隔离性,可能会引发如下的问题
   脏读
   不可重复读
   幻读
   
   最正确的是,自己查询自己的事务,在此期间别的事务操作跟我的事务操作没有任何的影响
   
   查看事务隔离级别
   脏读(dirty read):当一个事务读取另一个事务尚未提交的改变(update,insert,delete)的时候,产生脏读
	老刘解读：
	当一个事务读取到另外一个事务还没有提交(commit)的数据,属于脏读
	
   
   不可重复读(nonrepeatable read):同一查询在同一事务中多次进行,由于其他提交事务所做的修改或者删除,每次返回不同的结果集
   ,此时发生不可重复读
	老刘解读：
	两个事务进行查询,当一个事务能够查询到另外一个事务修改或者删除后的的数据的时候,属于不可重复读
   
   幻读(phantom read):同一查询在同一事务中多次进行,由于其他提交事务所做的插入操作,每次返回不同的结果集,此时发生幻读
	老刘解读：
	同一查询在同一个事务中多次进行,由于其他的事务所做的插入操作,每次返回不同的结果集,会发生幻读
   
   老韩说明:我们待会会举例一个案例来说明mysql的事务隔离级别,以对account表的操作来说明上面的几种情况
   
   
   -- mysql的事务隔离级别 -- 案例
   -- 我们举例一个案例来说明mysql的事务隔离级别,以对account表操作为例(id , name , money)
   
   
   
   
*/