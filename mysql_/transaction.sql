#mysql事务
-- 什么是事务
-- 事务用于保证数据的一致性，它由一组相关的dml【增加,修改,删除】语句组成,该组的dml语句要么全部成功,要么全部失败
-- 例如,转账的时候就要使用事务来处理,用以保证数据的一致性

-- 当执行事务操作的时候(dml语句),mysql会在表上加锁,防止其他用户改表的数据,这对用户来讲是非常重要的

/*
	mysql操作数据库控制台事务的几个重要的操作
	1.start transaction -- 开启一个事务
	2.savepoint 保存点名 -- 设置保存点
	3.rollback to 保存点名   --回退事务
	4.rollback -- 回退全部事务
	5.commit -- 提交事务,所有的操作生效,不能回退
*/

-- 事务的一个重点的概念和具体操作
-- 演示
-- 1.创建一张测试表

CREATE TABLE t27(
	`id` INT,
	`name` VARCHAR(32)
);

-- 2.开始事务
START TRANSACTION;

-- 3.设置保存点
SAVEPOINT a

-- 指定dml操作
INSERT INTO t27 VALUES(100,'tom');

SAVEPOINT b

-- 执行dml操作
INSERT INTO t27 VALUES(200 , 'jack');

-- 回退到b
ROLLBACK TO b;

-- 继续回退 a
ROLLBACK TO a;

-- rollback,如果是这样,表示直接退回到事务开始的状态
ROLLBACK;

-- 这样就不能再回退了,一旦事务使用commit提交了，就真正提交了数据,就不能再进行回退了
COMMIT;

-- 回退事务
-- 在介绍回退事务前,先介绍一下保存点(savepoint),保存点是事务中的点,用于取消部分事务
-- 当结束事务时(commit),会自动删除该事务定义的所有的保存点,当执行事务回退的时候,通过指定
-- 保存点可以退回到指定的点 

-- 提交事务
-- 使用commit语句可以提交事务,当执行了commit语句后,会确认事务的变化,结束事务,删除保存点
-- 释放锁,数据生效,当使用commit语句结束事务后,其他会话[其他连接]将可以查看到事务变化后的新数据
-- 所以的新数据就正式生效


SELECT * FROM t27;
DELETE FROM t27;
