#自增长
 -- 自增长的基本介绍  一个问题
 -- 在某张表中,存在一个列id(整数)，我们希望在添加记录的时候，该列从1开始，自动的增长,怎么处理
 -- 字段名 整形 primary key auto_increment
 -- 添加自增长的字段方式
 -- insert into xxx(字段1，字段2...) values(null,值);  字段值的第一个为id,但是为Null,会自动增长的
 -- insert into xxx(字段2 ...) values('值1','值2',...); 字段中没有id ， 直接从其他字段开始的,值也不用写id字段对应的值, id是会自动增长的
 -- insert into xxx values(null,值1,值2); 不用写字段,默认就是数据表的全部字段,所以id对应的字段应该为null，就会自动增长了
 
 -- 演示自增长的使用
 -- 创建表
 CREATE TABLE t26(
	id INT PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(32) NOT NULL DEFAULT '',
	`name` VARCHAR(32) NOT NULL DEFAULT ''
 )
 
 -- 测试自增长的使用
 -- 第一种方式
 -- 指明要添加id,但是id使用null表示, id这里会自动增长    如果这里id不使用null，而是写了固定的值,那么就会使用当前固定的值
 -- 再次添加数据之后就会从最新的id开始
 INSERT INTO t26(id , email , `name`) VALUES(NULL , 'smith@qq.com' , 'smith');
 -- 第二种方式
 -- 不用指明添加哪些字段的数据,id使用null表示，会自动增长
 INSERT INTO t26 VALUES(NULL , 'jack@qq.com' , 'jack');
 -- 第三种方式
 INSERT INTO t26(email , `name` )VALUES('rose@qq.com','rose');
 
 -- 修改默认的自增长的开始值
 ALTER TABLE t26 AUTO_INCREMENT = 100;
 
 SELECT * FROM t26;
 DESC t26;