-- 字符串的使用细节
-- 
-- 细节1
-- char(4)  这个4表示字符数(最大255),不是字节数,不管是中文还是字母都是放4个,按照字符计算
-- varchar(4) 这个4表示字符数,不管是字母还是中文都以定义好的表的编码来存放数据
-- 不管是中文还是英文字母,都是最多存放4个,是按照字符来存放的
-- 
-- 细节2
-- char(4)是定长(固定的大小),就是说,即使你插入了'aa',也会占用分配的4个字符的空间,其他的两个会填充默认值
-- varchar(4)是变长(变化的大小),也就是说,如果插入了'aa',实际占用空间大小并不是4个字符,而是按照实际占用空间来分配(老韩说明
-- varchar本身还需要占用1-3个字节来记录存放内容的长度) L(实际数据大小) + (1-3)个字节
-- 
-- 细节3
-- 什么时候使用char，什么时候使用varchar
-- 1.如果数据是定长,推荐使用char，比如md5的密码,邮编,手机号,身份证账号,char(32)
-- 2.如果一个字段的长度是不确定的,我们使用varchar，比如留言,文章
-- 查询速度  char > varchar
-- 
-- 细节4
-- 在存放文本的时候，也可以使用Text数据类型,可以将Text视为VARCHAR列，注意Text不能有默认值,大小为0-2^16个字节
-- 如果希望存放更多的字符,可以选择 mediumText 0-2^24  或者 longtext 0-2^32

#演示字符串类型的使用细节
#char(4)和varchar(4)这个4表示的是字符,而不是字节,不区分字符串是汉字还是字母
CREATE TABLE t11(
	`name` CHAR(4)
);

#因为是char(4)，只允许插入四个字符的数据,超过会报错
INSERT INTO t11(`name`)VALUES('js');
SELECT * FROM t11;

CREATE TABLE t12(
	`name` VARCHAR(4)
);

#插入数据
#超过4个字符就不能插入了,因为最大长度设置的是4
INSERT INTO t12 VALUES('韩顺平好');
INSERT INTO t12 VALUES('北京bj');
SELECT * FROM t12;


#如果varchar不够用,可以考虑使用mediumtext或者longtext
#如果想简单点,可以直接使用text

CREATE TABLE t13(
	`content` TEXT,
	`content2` MEDIUMTEXT,
	`content3` LONGTEXT
);

#添加数据的时候.需要注意的是,表名后面可以不用写字段,在写value的时候,就默认的从第一个字段开始插入
INSERT INTO t13 VALUES('韩顺平教育','韩顺平教育100','韩顺平教育1000----');

SELECT * FROM t13;






#drop table t11;



