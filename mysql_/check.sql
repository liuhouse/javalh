#check
#用于强制数据必须满足的条件,假定在sal列上定义了check约束,并要求sal列值在1000 - 2000 之间，如果不在1000 - 2000
#之间就会提示出错
-- 老师提示：oracle 和 sql server 均支持check.但是mysal5.7目前还不支持check,只做语法效验,但是不会生效   mysql8 也不支持
/*
	基本语法：列名 类型  check (check条件)
	user表
	id , name , sex(man , woman) , sal(大于100 小于900)
	在mysql中实现check的功能,一般在程序中控制,或者通过触发器完成
	学习 orcle , sql server , 这两个数据库是真的生效
*/ 
-- 测试
CREATE TABLE t25(
	id INT PRIMARY KEY,
	`name` VARCHAR(32),
	`sex` VARCHAR(6) CHECK(sex IN('man' , 'woman')),
	`sal` DOUBLE CHECK(sal > 1000 AND sal < 2000)
);

-- 添加数据
INSERT INTO t25 VALUES(1,'jack','mid',1);

SELECT * FROM t25;