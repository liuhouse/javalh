#insert语句
#使用insert语句向表中插入数据
#快速入门案例
#1.创建一张商品表goods(id int , goods_name varchar(10) , price double);
#2.添加两条记录

#创建表
CREATE TABLE `goods`(
	`id` INT,
	`goods_name` VARCHAR(10) NOT NULL DEFAULT '',
	`price` DOUBLE
) CHARSET utf8 COLLATE utf8_bin ENGINE INNODB;

#添加两条记录
INSERT INTO `goods`(`id`,`goods_name`,`price`) VALUES(1,'行者悟空',655);
INSERT INTO `goods`(`id` , `goods_name` , `price`) VALUES(2,'地狱火',888);

SELECT * FROM goods;

#学员练习,使用insert语句向表employee中插入两个员工的信息
INSERT INTO `employee`(`id`,`user_name`,`birthday`,`entry_date`,`job`,`salary`,`resume`,`image`)
		VALUES(1,'孙悟空','1995-11-11','2001-12-06 11:12:50','取经人',10000,'唐僧的大徒弟','image/wukong.jpg');
INSERT INTO `employee`(`id`,`user_name`,`birthday`,`entry_date`,`job`,`salary`,`resume`,`image`)
		VALUES(2,'猪八戒','1996-11-11','2002-12-06 11:12:50','取经人二号',9000,'唐僧的二徒弟','image/wukong.jpg');
SELECT * FROM `employee`;
