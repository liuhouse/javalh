#select语句
#基本语法
#select 语句   [重点 难点]
CREATE TABLE student(
	`id` INT NOT NULL DEFAULT 1,
	`name` VARCHAR(20) NOT NULL DEFAULT '',
	`chinese` FLOAT NOT NULL DEFAULT 0.0,
	`english` FLOAT NOT NULL DEFAULT 0.0,
	`math` FLOAT NOT NULL DEFAULT 0.0
) CHARSET utf8 COLLATE utf8_bin ENGINE INNODB;

#给表中添加数据 -- 批量添加
INSERT INTO student(id,`name`,chinese,english,math)VALUES
(1,'韩顺平',89,78,90),
(2,'张飞',67,98,56),
(3,'宋江',87,78,77),
(4,'关羽',88,98,90),
(5,'赵云',82,84,67),
(6,'欧阳锋',55,85,45),
(7,'黄蓉',75,65,30),
(8,'韩信',45,65,99);

#查询表中所有学生的信息
SELECT * FROM student;

#查询表中所有学生的姓名和对应的英语成绩
SELECT `name`,`english` FROM student;

#过滤表中的重复数据 distinct
#要查询的记录，每个字段都想相同
SELECT DISTINCT `name`,english FROM student;





