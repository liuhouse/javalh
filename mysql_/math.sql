-- 演示数学相关函数
-- ABS(num) 绝对值
SELECT ABS(-10) FROM DUAL;
-- BIN(decimal_number) 十进制转二进制
SELECT BIN(10) FROM DUAL; -- 1010
-- CEILING(number2) 向上取整，得到比number2大的最小整数
SELECT CEILING(2.2) FROM DUAL;
SELECT CEILING(-2.2) FROM DUAL;
-- CONV(number2 , from_base , to_base) 进制转换
-- 下面的含义是8是十进制的8,转化成2进制进行输出
SELECT CONV(8,10,2) FROM DUAL;
-- 下面的含义是16是16进制的16,转换成10进制进行输出
SELECT CONV(16,16,10) FROM DUAL;
-- FLOOR(number2) 向下取整,得到比number2小的最大整数
SELECT FLOOR(2.1) FROM DUAL;
-- FORMAT(number , decimal_places) 保留小数位数
SELECT FORMAT(78.15455844,2) FROM DUAL;
-- HEX(DecimalNumber) 转十六进制
SELECT HEX(11) FROM DUAL;
-- LEAST(number1,number2,number3) 求最小值
SELECT LEAST(0,140,1,10) FROM DUAL;
-- MOD(numeractor , denominator) 求余
SELECT MOD(10 , 3) FROM DUAL;
-- RAND([seed]) RAND([seed]) 返回随机数 其范围为0<=v<=1.0
-- 老韩说明
-- 1.如果使用rand() 每次都返回不同的随机数,在0<=v<=1之间
-- 2.如果使用rand(seed)返回随机数,范围是0<=v<=1.0，如果seed不变,该随机数就不变了
SELECT RAND() FROM DUAL; -- 随即变的
SELECT RAND(20) FROM DUAL; -- 不会再发生变化了
SELECT CURRENT_TIMESTAMP() FROM DUAL; -- 获取当前的时间
