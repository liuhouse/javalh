#在from
-- 请思考：查找每个部门工资高于本部门平均工资的人的资料
-- 这里要用到数据查询的小技巧.把一个子查询当做一个临时表来使用
-- 请思考：查找每个部门工资最高的人的详细资料
-- 1.先查询本部门的平均工资，和部门编号
-- 2.查询出本部门人的工资高于平均工资的信息
SELECT emp.ename , emp.deptno , emp.sal,avg_sal FROM 
	emp ,
	(SELECT deptno , AVG(sal) avg_sal  FROM emp GROUP BY deptno) AS tmp_emp
	WHERE 
	emp.deptno = tmp_emp.deptno
	AND 
	emp.sal > tmp_emp.avg_sal;
	
-- 查找每个部门工资最高的人的详细资料
-- 先查询出各个部门最高的工资
-- 将上面的查询当做一张临时表,查询出本部门中工资=最高工资的人的信息   两个条件    emp.部门 = emp_tmp.部门 and emp.sal = emp_tmp.max_sal
SELECT deptno,MAX(sal) AS max_sal FROM emp GROUP BY deptno;
SELECT emp.ename , emp.deptno , emp.sal , max_sal FROM
	emp,
	(SELECT deptno,MAX(sal) AS max_sal FROM emp GROUP BY deptno) AS tmp_emp
	WHERE
	emp.deptno = tmp_emp.deptno
	AND
	emp.sal = tmp_emp.max_sal;

-- 查找每个部门的信息(包括：部门名,编号,地址)和人员数量,我们一起完成
-- 1：部门名称,编号,地址 来自dept表
-- 2: 各个部门的人员数量 -> 构建一个临时表  来进行联合查询
-- 解决思路，先查询出各个部门的总人数,作为临时表,然后用部门表联合查询    部门表的部门编号= 人员表的编号

SELECT COUNT(*) AS num , deptno FROM emp GROUP BY deptno;

SELECT
	emp_tmp.deptno , dname , loc,num
	FROM 
	dept ,(SELECT COUNT(*) AS num , deptno FROM emp GROUP BY deptno) AS emp_tmp
	WHERE  dept.deptno = emp_tmp.deptno;
	
-- 还有一种写法 表.*表示将该表的所有的列都显示出来,可以简化sal语句
-- 在多表查询中,当多个表的列不重复的时候,可以直接写列名
-- emp_tmp.*表示临时表中的所有,这里指的数 num , deptno
SELECT
	emp_tmp.* , dname , loc
	FROM 
	dept ,(SELECT COUNT(*) AS num , deptno FROM emp GROUP BY deptno) AS emp_tmp
	WHERE  dept.deptno = emp_tmp.deptno;

SELECT * FROM dept;

