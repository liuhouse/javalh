/*
	1.主键索引,主键自动为主索引(类型Primary key)
	2.唯一索引(UNIQUE)
	3.普通索引(INDEX)
	4.全文索引(FULLTEXT)[使用于MYSAM]
	   一般开发,不使用mysql自带的全文索引,而是使用：全文搜索 Solr 和 ElasticSearch(ES)
	   -- 
*/
CREATE TABLE s1(
	id INT PRIMARY KEY, -- 主键,同时也是索引,称为主键索引
	`name` VARCHAR(32)
);

CREATE TABLE s2(
	id INT UNIQUE -- id是唯一的,同时也是索引,称为unique索引
);

SHOW INDEX FROM s1;
SHOW INDEX FROM s2;

-- 演示mysql的索引的使用
-- 创建索引
CREATE TABLE t27(
	`id` INT,
	`name` VARCHAR(32)
);

-- 添加索引
-- 添加唯一索引
CREATE UNIQUE INDEX id_index ON t27(id);

-- 如何选择
-- 1.如果某列的值,是不重复的,则优先考虑使用unique索引,否则使用普通索引


-- 添加普通索引方式1
CREATE INDEX id_index ON t27(id);

-- 添加普通索引方式2
ALTER TABLE t27 ADD INDEX id_index(id)

-- 查询表是否有索引
SHOW INDEX FROM t27;

-- 添加主键索引
CREATE TABLE t28(
	id INT,
	`name` VARCHAR(32)
);

ALTER TABLE t28 ADD PRIMARY KEY (id);

SHOW INDEX FROM t28;




-- 创建一个没有索引的表
CREATE TABLE s3(
	id INT,
	`name` VARCHAR(32)
);

-- 添加主键索引
ALTER TABLE s3 ADD PRIMARY KEY(id);
-- 添加一个普通索引
CREATE INDEX name_index ON s3(`name`);

-- 删除索引
DROP INDEX name_index ON s3;

-- 删除主键索引
ALTER TABLE s3 DROP PRIMARY KEY;

-- 修改索引,先删除索引,再添加新的索引

SHOW INDEX FROM s3;

-- 查询索引
-- 1.方式
SHOW INDEX FROM s3;
-- 2.方式
SHOW INDEXES FROM s3;
-- 3.方式
SHOW KEYS FROM s3;
-- 4.方式
DESC s3;











