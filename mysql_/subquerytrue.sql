#子查询当做临时表来使用
-- 查询ecshop 中各个类别中价格最高的商品
-- 查询 商品表
-- 先得到各个类别中价格最高的商品 , max + group by cat_id , 当做临时表
-- 把子查询当做一张临时表可以解决很多复杂的查询

-- 下面的这句sql语句只是查询出了商品表中各个分类中的最高价格,并不能确定是哪个产品
-- 可以将这个查询当做一个临时表
SELECT cat_id,MAX(shop_price) FROM ecs_goods GROUP BY cat_id;

-- 查询出分类中价格最高的对应的商品
-- 解析
-- 1.查询出商品表中各个分类的最高价格 tmp_table
-- 2.将tmp_table当做临时表做为关联查询 , 条件就是 临时表的.cat_id = 商品表.cat_id AND 临时表.价格 = 商品表.最高价格
SELECT goods_id,goods_name,ecs_goods.cat_id,shop_price
	FROM  
	(SELECT cat_id,MAX(shop_price) AS max_shop_price FROM ecs_goods GROUP BY cat_id) AS emp_goods ,ecs_goods
	 WHERE emp_goods.cat_id = ecs_goods.cat_id AND emp_goods.max_shop_price = ecs_goods.shop_price;
	 
	
SELECT cat_id,MAX(shop_price) FROM ecs_goods GROUP BY cat_id;
SELECT goods_name,shop_price,ecs_goods.cat_id FROM 
	(SELECT cat_id,MAX(shop_price)  AS max_shop_price FROM ecs_goods GROUP BY cat_id) AS tmp_goods,ecs_goods
	WHERE tmp_goods.cat_id = ecs_goods.cat_id AND tmp_goods.max_shop_price = ecs_goods.shop_price;

DESC ecs_goods;

