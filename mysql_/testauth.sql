-- 测试此用户是否有查看数据的权限
SELECT * FROM news;
-- 测试此用户是否有添加数据的权限
INSERT INTO news VALUES(3,'樊振东');
-- 测试用户是否有修改数据的权限
-- UPDATE command denied to user 'liuhao'@'localhost' for table 'news'  失败的  在没有赋予更新 update权限之前
UPDATE news SET `name`='樊振东1号' WHERE id = 3;