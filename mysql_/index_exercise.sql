#索引练习
-- 建立索引(主键)
-- 要求
-- 1.创建一张订单表order(id号,商品名,订购人,数量)，要求id为主键,请使用两种方式来创建主键(提示:为了练习方便,可以使用order1,order2)
-- 方式1
CREATE TABLE order1(
	`id` INT PRIMARY KEY,
	`goods_name` VARCHAR(32),
	`person_name` VARCHAR(32),
	`num` INT
);
SHOW INDEXES FROM order1;

-- 方式2
CREATE TABLE order2(
	`id` INT,
	`goods_name` VARCHAR(32),
	`person_name` VARCHAR(32),
	`num` INT
);
-- 添加主键索引
ALTER TABLE order2 ADD PRIMARY KEY(id);
SHOW INDEX FROM order2;



-- 建立唯一索引（课后练习）
-- 要求：
-- 1.创建一张特价菜谱表menu(id号,菜谱名,厨师,点餐人身份证,价格).要求id号为主键,点餐人的身份证是unique，请使用两种方式来创建unique
-- 2.(提示：为了练习方便,可以是menu1 , menu2)
CREATE TABLE menu(
	`id` INT PRIMARY KEY,
	`menu_name` VARCHAR(50) NOT NULL DEFAULT '',
	`cook` VARCHAR(30) NOT NULL DEFAULT '',
	`id_card` VARCHAR(32) UNIQUE,
	`price` DOUBLE NOT NULL DEFAULT 0.00
);

-- 第二种方式
CREATE TABLE menu2(
	`id` INT PRIMARY KEY,
	`menu_name` VARCHAR(50) NOT NULL DEFAULT '',
	`cook` VARCHAR(30) NOT NULL DEFAULT '',
	`id_card` VARCHAR(32),
	`price` DOUBLE NOT NULL DEFAULT 0.00
);

-- 添加唯一索引
CREATE UNIQUE INDEX id_card_index ON menu2(id_card);

SHOW KEYS FROM  menu2;




-- 建立索引(普通)课堂练习
-- 要求
-- 1.创建一张运动员表sportman(id号,名字,特长).要求id号位主键,
-- 名字为普通索引,请使用2种方式来创建索引(提示:为了练习方便,可以是不同表名 sportman1 , sportman2)
CREATE TABLE sportman1(
	`id` INT PRIMARY KEY,
	`name` VARCHAR(32),
	`specialty` VARCHAR(50)
);

CREATE INDEX name_index ON sportman1(`name`);


-- 第二种创建普通索引的方式
CREATE TABLE sportman2(
	`id` INT PRIMARY KEY,
	`name` VARCHAR(32),
	`specialty` VARCHAR(50)
);

ALTER TABLE sportman2 ADD INDEX name_index(`name`);

SHOW KEYS FROM sportman2;


#哪些列上适合使用索引
-- 1.较频繁的作为查询条件字段的应该创建索引
SELECT * FROM emp WHERE empno = 1;
-- 2.唯一性太差的字段不适合单独的创建索引,即使频繁作为查询条件
SELECT * FROM emp WHERE sex = '男';
-- 3.更新非常频繁的字段不适合创建索引
SELECT * FROM emp WHERE logincount = 1
-- 4.不会出现在WHERE子句中字段不该创建索引



