#update语句
#使用update语句修改表中的数据

#基本使用
#要求：在上面创建的employee表中修改表中的记录
-- 1：将所有员工的薪水修改为5000元.[如果没有带where条件,会修改所有的记录,因此要特别的小心]
UPDATE employee SET salary = 5000;

-- 2:将孙悟空的薪水修改为8000元
UPDATE employee SET salary = 8000 WHERE user_name = '孙悟空';

-- 3：将猪八戒的薪水在原有的基础上增加1000元
UPDATE employee SET salary = salary + 1000 WHERE user_name = '猪八戒';

-- 4：可以修改多个列的值
#将猪八戒的说明修改成 净坛使者,工资直接涨到1万2
UPDATE employee SET salary = 12000 , `resume` = '净坛使者' WHERE user_name = '猪八戒';

#使用细节
#1.update语法可以用于新值的更新原有表行中的各列
#2.set子句指示要修改哪些列和要给与哪些值
#3.where子句指定应该更新哪些行,如果没有where子句,则更新所有的行(记录)，因此老师提醒一定要小心使用
#4.如果需要修改多个字段,可以通过set 字段1=值1 ， 字段2 = 值2 where condition

SELECT * FROM employee;