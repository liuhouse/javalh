package chapter07;

public class Method01_1 {
	public static void main(String[] args) {
		/*
		 * 	需求
		 * 	1:定义speak方法,输出我喜欢java
		 * 	2:定义cal01,输出1-100的和
		 * 	3:定义cal02，传递参数n,输出1-n之间的和
		 * 	4:定义方法sumAdd,传递两个参数,求出和
		 * */
		Dog d1 = new Dog();
		d1.speak();
		d1.cal01();
		d1.cal02(10);
		int sumRes = d1.sumAdd(10, 10);
		System.out.println(sumRes);
		
	}
}


class Dog{
	String name;
	int age;
	
	//定义speak方法
	public void speak() {
		System.out.println("我喜欢java");
	}
	
	
	//定义cal01 计算1..100的和并输出
	public void cal01() {
		int res = 0;
		for(int i = 1 ; i <= 100 ; i ++) {
			res += i;
		}
		System.out.println(res);
	}
	
	
	//定义cal02，传递参数n,输出1-n之间的和
	public void cal02(int n) {
		int res = 0;
		for(int i = 1 ; i <= n ; i++) {
			res += i;
		}
		System.out.println(res);
	}
	
	
	//定义方法sumAdd,传递两个参数,求出和
	public int sumAdd(int sum1,int sum2) {
		int res = sum1 + sum2;
		return res;
	}
	
	
	
	
	
	
	
	
}
