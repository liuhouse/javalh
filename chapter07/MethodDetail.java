package chapter07;

public class MethodDetail {
	public static void main(String[] args) {
		/*
		 * 	成员方法的使用细节
		 * 	(1):一个方法最多有一个返回值【思考.如何返回多个结果, 返回数组】
		 * 	(2):返回类型可以是任意类型,包含基本类型或使用引用类型(数组,对象)
		 * 	(3):如果方法要求有返回数据类型，则方法体中最后执行的语句必须是return值,而且要求返回值得类型必须和return的值得类型一致
		 * 	(4):如果方法是void,则方法体中可以没有return语句,或者只写return
		 * 	(5):方法名，遵循驼峰命名,最好见名知意,表达出该功能的意思即可,比如得到两个数的和 getSum()，开发中按照规范
		 * */
		AA a = new AA();
		//因为a.getInt(n)的返回值是int  , 所以也必须使用int来进行接收
		int resInt = a.getInt(10);
		System.out.println(resInt);
		
		//一个方法返回多个返回值,使用数组
		int resArr[] = a.getSumAndSub(10, 20);
		System.out.println("两个数之和是 " + resArr[0]);
		System.out.println("两个数之差是 " + resArr[1]);
		
		double d1 = a.f1();
		System.out.println("double最终的结果是" + d1);
		
		//调用void 没有返回值得  方法
		a.f2();
		
		//方法要求是两个int 所以是对的
		//int c1 = 1;
		//int c2 = 2;
		
		//传递兼容类型也是可以的   byte -> int
		//int c1 = 1;
		//byte c2 = 2;
		
		//必须是和参数列表的相同类型或者兼容类型
		int c1 = 1;
		String c2 = "2";
		
		//这样就可以，因为定义第二个参数是字符串
		
		//这样就是不对的,因为参数类型的传递顺序是错误的
		//int resInt1 = a.cdCanshu(c2, c1);
		//System.out.println(resInt1);
		
		
		a.f1_1();
		
		
	}
}




class AA{
	
	//形参列表
	//1:一个方法可以有0个参数,也可以有多个参数,中间用逗号隔开  比如  getSum()    getSum(int n1 , int n2)
	//2:参数类型可以为任意类型，包含基本类型或者引用类型 ,比如   基本类型 ： printArr(int n1)  引用类型： printArr(int[][] map)
	//3:调用带参数的方法的时候,一定要对应着参数列表传入相同的类型或者兼容类型的参数
	//4:方法定义的时候的参数称作为形式参数,简称形参,方法调用的时候传入的参数称作实际参数,简称实参,实参和形参的类型要一只或者兼容,个数,顺序必须一致
	//5:方法体,里面写完成功能的具体的语句,可以输入,输出,变量,运算,分支,循环,方法调用,但是不能在方法里面继续写方法，不可以进行方法嵌套定义
	
	
	//5
	public void f1_1() {
		//这是错误的,不能套娃定义
//		public f1_2() {
//			
//		}
	}
	
	
	
	//3:调用带参数的方法的时候,一定要对应着参数列表传入相同的类型或者兼容类型的参数
	public int cdCanshu(int n1,String n2) {
		int res = n1 + Integer.parseInt(n2);
		return res;
	}
	
	
	
	//一方法最多只有一个返回值
	public int getInt(int n1) {
		int res = 10 + n1;
		return res;
	}
	
	
	//如何返回多个结果,使用数组
	//获取两个数之和和之差
	public int[] getSumAndSub(int num1,int num2) {
		//创建数组
		int resArr[] = new int[2];
		//填充数组的第一个元素,两个数之和
		resArr[0] = num1 + num2;
		//填充数组的第二个元素,两个数之差
		resArr[1] = num1 - num2;
		
		return resArr;
	}
	
	//返回值可以是任意类型,包含基本类型或者引用类型(数组,对象)
	//基本数据类型:getInt()    引用类型：getSumAndSub()
	
	
	
	//如果方法中要求有返回的数据类型,则方法体中最后执行的语句必须为return
	//而且要求返回值的类型必须和return 的值得类型一致或者兼容
	public double f1() {
		double d1 = 1.1 * 3;
		int n = 3;
		//return n;//int -> double 类型   低精度向高精度转换 这是允许的   也称作为兼容
		return d1;// double -> int 类型  高精度向低精度转化  这是不允许的
	}
	
	
	
	//如果方法是void,则方法体中可以没有return语句,或者,只写return
	//仅仅只能写return   不能写一个具体的值  否则就会报错
	public void f2() {
		System.out.println("hello1");
		System.out.println("hello2");
		return;
	}
	
	
	
	
	
}