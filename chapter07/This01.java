package javalh.chapter07;

public class This01 {
    public static void main(String[] args){

        //谁调用,this就指向谁
        //举例说明:小明说 我的手   -> 这里"我的"指的就是小明的手      小红说我的手 -> 这里"我的" 指的就是小红
        //每个对象在jvm虚拟机上都有一个内存的地址,这里可以用hashCode来表示显示出来
        //当前打印出来的hashCode()值和构造方法里面this打印出来的hashCode()值是一样的,所以这里的this就是指向的dog
        ThisDog dog = new ThisDog("阿黄",5);
        System.out.println(dog.hashCode());
        dog.info();

        ThisDog dog2 = new ThisDog("大傻",6);
        System.out.println(dog2.hashCode());
        dog2.info();
    }
}

class ThisDog{
    public String name;
    public int age;
//    public ThisDog(String DName,int DAge){
//        name = DName;
//        age = DAge;
//    }

    //构造方法的输入参数名不是非常的好,如果能够将dName改成name就好了,但是我们会发现按照变量的作用域原则,name的值就是null，
    //里面的name 其实就是一个新的name  而 后面的name的值  就是成员属性   这个时候进行实例化初始化赋值的时候 跟成员属性已经没有关系了
    //怎么解决   this

    //什么是this
    /*
    * java虚拟机会给每个对象分配this，代表的是当前对象,哪个实例化的对象调用他,this就指向的是当前对象
    * */
    public ThisDog(String name,int age){
        //当前调用的对象是名为dog的对象   所有this就指向dog
        //System.out.print(this);
        //因为采用的是就近原则
        this.name = name;//后面的name是成员属性name  null
        this.age = age;//后面的age是成员属性age 0
        System.out.println(this.hashCode());
    }

    public void info(){
        System.out.println(this.name + "\t" + this.age + "\t");
    }

}


