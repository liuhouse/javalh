package chapter07;

public class Object02 {
	
	public static void main(String[] args) {
		/**
		 * (1)类是抽象的,概念的,代表一类事物,比如人类,猫类,狗类,就是一种数据类型
		 * (2)对象是具体的,实际的,代表一个具体的事物,就是实例
		 * (3)类是对象的模板,对象是类的一个个体,对应一个实例
		 *  
		 * */
		
		
	}
}


class Car{
	String name;//属性，成员变量,字段,field
	double price;
	String Color;
	String[] master;//属性可以是基本的数据类型,也可以是引用数据类型(对象,数组)
}
