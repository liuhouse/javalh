package javalh.chapter07;

public class HomeWork06 {
    public static void main(String[] args){
        Cale c1 = new Cale(10,0);
        //求和
        System.out.println("两个数相加 = " + c1.sum());
        //求差
        System.out.println("两个数的差 = " + c1.difference());
        //求乘
        System.out.println("两个数相乘 = " + c1.ride());
        //求除
        Double exc_num = c1.except();
        if(exc_num != null){
            System.out.println("两个数相除 = " + c1.except());
        }else{
            System.out.println("除数不能为0");
        }
    }
}

class Cale{
    //定义两个操作数
    double num1;
    double num2;

    //定义构造方法
    public Cale(double num1,double num2){
        this.num1 = num1;
        this.num2 = num2;
    }

    //定义求和的方法
    public double sum(){
        return this.num1 + this.num2;
    }

    //定义求差的方法
    public double difference(){
        return this.num1 - this.num2;
    }

    //定义求乘的方法
    public double ride(){
        return this.num1 * this.num2;
    }

    //定义求除的方法
    //-10010代表的是错误码
    public Double except(){
        if(this.num2 == 0.0){
            return null;
        }else{
            return this.num1 / this.num2;
        }
    }

}
