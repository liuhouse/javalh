package javalh.chapter07;

public class ConstructorExercise {
    public static void main(String[] args){
        /*
        * 需求
        * 在前面定义的Person类中添加两个构造器
        * 第一个是无参构造器：利用构造器设置所有人的age属性初始值都是18
        * 第二个带pName和pAge两个参数的构造器：使得每次创建Person对象的同时初始化对象的age属性值和name属性值
        * 分别使用不同的构造器，创建对象
        * */

        //无参构造器实例化对象
        PersonE pe = new PersonE();
        PersonE pe1 = new PersonE();
        System.out.println(pe.age);
        System.out.println(pe1.age);

        //使用有参构造器实例化对象
        PersonE pe2 = new PersonE("鬼剑士",88);
        System.out.println(pe2.age);
        System.out.println(pe2.name);
    }
}


class PersonE{
    String name;
    int age;

    //无参构造器
    PersonE(){
        age = 18;
    }


    //有参构造器
    public PersonE(String pName,int pAge){
        name = pName;
        age = pAge;
    }
}
