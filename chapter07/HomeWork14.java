package javalh.chapter07;
import java.lang.Math;

public class HomeWork14 {
    public static void main(String[] args){
        /*
        * 有个人Tom设计他的成员变量,成员方法,可以电脑猜拳,电脑每次都会随机生成 0 1 2
        * 0表示石头  1 表示剪刀  2表示布
        * 并要可以显示 Tom的输赢次数 （清单）
        *
        * 思路分析
        * (1):先定义一个类 Tom, 定义两个成员变量  num1  和  num2  用来表示每次的双方出拳的结果
        * (2):定义一个方法 表示比较的结果，方法里面使用随机数
        * (3):定义两个变量,用来记录Tom的输赢的次数
        *
        * */

        Tom t = new Tom();
        t.RockPaperScissors(10);
        System.out.println("赢的次数为" + t.win);
        System.out.println("输的次数为" + t.loser);
    }
}

class Tom{
    //甲方出拳
    int num1;
    //乙方出拳
    int num2;

    //赢了的次数
    int win = 0;
    //输了的次数
    int loser = 0;

    int compare_num = 0;

    //定义结果数
    int[] res_num = {0,1,2,0,1,2,0,1,2,0};

    //石头剪刀布的方法
    public void RockPaperScissors(int compare_num){
        int i = compare_num;
        for ( ; i > 0 ;){
            this.num1 = res_num[(int)(Math.random() * 10)];
            this.num2 = res_num[(int)(Math.random() * 10)];
            //相等的话 就不记录了
            if(this.num1 != this.num2){
                System.out.println("甲方 " + num1 + " 乙方 " + num2);
                if(this.num1 > this.num2){
                    this.win++;
                }else{
                    this.loser++;
                }
                i--;
            }
        }
    }
}
