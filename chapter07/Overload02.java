package chapter07;

public class Overload02 {
    public static void main(String[] args){
        MyCalculator1 cal = new MyCalculator1();
        int cal1 = cal.calculator(1, 2);
        System.out.println(cal1);
        double cal2 = cal.calculator(3.2, 1);
        System.out.println(cal2);
        int cal3 = cal.calculator(1, 2,3);
        System.out.println(cal3);;
        double cal4 = cal.calculator(1.1, 1.0);
        System.out.println(cal4);
        
    }
}


class MyCalculator1{
	//返回int类型
    public int calculator(int n1 , int n2){
        return n1 + n2;
    }
    
    //返回double类型
   public double calculator(double n1 , int n2) {
	   return n1 + n2;
   }
   
   
   //返回int类型
   public int calculator(int n1 , int n2 , int n3) {
	   return n1 + n2 + n3;
   }
   
   
   //返回double
   public double calculator(double n1 , double n2) {
	   return n1 + n2;
   }
   
   
    
}