package javalh.chapter07;

public class HomeWork12 {
    public static void main(String[] args){
        /*
        * 创建一个Employee类,属性有(名字,性别,年龄,职位,薪水),提供3个构造方法,可以初始化
        * (1) (名字,性别,年龄,职位,薪水)
        * (2) (名字,性别,年龄)
        * (3) (职位,薪水)
        * 要求充分复用构造器
        * */

        //构造方法1
        Employee e1 = new Employee("刘备",'男',55,"老大",5000);

        //构造方法2
        Employee e2 = new Employee("关羽",'男',54);

        //构造方法3
        Employee e3 = new Employee("马弓手",10000);

    }
}

class Employee{

    //名字
    String name;
    //性别
    char sex;
    //年龄
    int age;
    //职位
    String position;
    //薪水
    double sal;

    //构造方法3
    //(职位,薪水)
    public Employee(String position , double sal){
        this.position = position;
        this.sal = sal;
        //System.out.println("我是构造方法3 - 薪水为" + this.sal + "职位为" + this.position);
    }

    //构造方法2
    //(名字,性别,年龄)
    public Employee(String name , char sex , int age){
        this.name = name;
        this.sex = sex;
        this.age = age;
        //System.out.println("我是构造方法2 - 姓名为 " + this.name + "性别为" + this.sex + "年龄为" + this.age);
    }


    //构造方法1
    //(名字,性别,年龄,职位,薪水)
    public Employee(String name,char sex,int age,String position,double sal){
        //进行复用
        this(name,sex,age);
        //this调用必须要在第一条   因为这个已经在第二条了    所以是错误的
//        this(position,sal);
        this.position = position;
        this.sal = sal;
        //System.out.println("我是构造方法1 - 姓名为" + this.name + "性别为" + this.sex
                //+ "年龄为" + this.age + "职位为" + this.position + "薪水为" + this.sal);
    }


}
