package javalh.chapter07;
import java.lang.Math;

public class HomeWork05 {
    public static void main(String[] args){
        /*
        * 需求：
        * 定义一个圆类Circle,定义属性：半径,提供显示圆周长功能的方法,提供显示圆面积的方法
        * */
        double radius = 3;
        Circle c = new Circle(radius);
        //得到圆的周长
        System.out.println("半径为" + radius + "的圆,对应的周长为" + c.girth());

        //得到圆的面积
        System.out.println("半径为" + radius + "的圆,对应的面积为" + c.area());

    }
}

class Circle{
    //定义 PI
    double PI = Math.PI;
    //定义半径
    double radius;

    //初始化的时候初始半径
    public Circle(double radius){
        this.radius = radius;
    }

    //计算圆的周长  圆的周长 = 2 * π * r;
    public double girth(){
        return 2 * this.PI * this.radius;
    }

    //计算圆的面积  圆的面积 = π * r * r
    public double area(){
        return this.PI * this.radius * this.radius;
    }
}
