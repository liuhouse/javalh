package javalh.chapter07;

public class TestPerson {
    public static void main(String[] args){
        /*
        * 定义 Person 类，里面有 name、age 属性，并提供 compareTo 比较方法，
        * 用于判断是否和另一个人相等，提供测试类 TestPerson 用于测试, 名字和年龄完全一样，就返回 true, 否则返回 false
        * */
        PersonT1 p1 = new PersonT1("小明1",18);
        PersonT1 p2 = new PersonT1("小明",18);

        PersonT1 pDo = new PersonT1();
        boolean res = pDo.compareTo(p1,p2);
        if(res){
            System.out.println("名字和年龄完全是一样的");
        }else{
            System.out.println("不一样");
        }
    }
}

class PersonT1{
    String name;
    int age;

    //显示构造
    PersonT1(){

    }

    //构造方法
    public PersonT1(String name , int age){
        this.name = name;
        this.age = age;
    }

    //用于比较两个人是不是相同
    public boolean compareTo(PersonT1 p1 , PersonT1 p2){
        return (p1.age == p2.age && p1.name.equals(p2.name));
    }

}
