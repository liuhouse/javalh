package javalh.chapter07;

public class HomeWork08 {
    //主类自定义属性
    int count = 9;
    public static void main(String[] args){
        new HomeWork08().count1();//10
        HomeWork08 hm = new HomeWork08();
        hm.count2();//9
        hm.count2();//10

    }

    public void count1(){
        //就近原则
        count = 10;
        System.out.println("count1 = " + count);
    }

    public void count2(){
        System.out.println("count1 = " + count++);
    }

}
