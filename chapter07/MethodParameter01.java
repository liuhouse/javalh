package chapter07;

public class MethodParameter01 {
	public static void main(String[] args) {
		
		//结论
		//基本数据类型,传递的是值(值拷贝),形参的任何改变不会影响实参
		
		int a = 10;
		int b = 20;
		AAM01 a_obj = new AAM01();
		a_obj.swap(a, b);
		
		System.out.println("main方法 a = " + a + "b = " + b);//a = 10 b = 20
	}
}


class AAM01{
	//进行两个数的交换
	public void swap(int a , int b) {
		System.out.println("\na和b交换之前的值\na=" + a + "\tb=" + b);//a = 10  b = 20
		//完成了a和b的交换
		int tmp = a;
		a = b;
		b = tmp;
		System.out.println("\na和b交换之后的值\na=" + a + "\tb=" + b);//a = 20 b = 10
	}
}
