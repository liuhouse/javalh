package chapter07;

public class MethodExercise01_1 {
	public static void main(String[] args) {
		MeT m = new MeT();
		int num = 11;
		//判断num是不是奇数
//		if(m.is_odd(num)) {
//			System.out.println(num + "是奇数");
//		}else {
//			System.out.println(num + "是偶数");
//		}
		
		//判断num是不是偶数
//		if(m.is_ou(num)) {
//			System.out.println(num + "是偶数");
//		}else {
//			System.out.println(num + "是奇数");
//		}
		
		//打印出10行10列的*
		m.printRes(10, 10, '*');
		
	}
}


//定义一个类
class MeT {
	//判断一个数是不是奇数
	public boolean is_odd(int num) {
		return num % 2 != 0;
	}
	
	//判断一个数是不是偶数
	public boolean is_ou(int num) {
		return num % 2 == 0;
	}
	
	//根据行,列,字符 打印出  多少行对应的多少列的字符
	public void printRes(int num1,int num2,char c) {
		//打印的行数
		for(int i = 0 ; i < num1 ; i++) {
			//没行打印的列数
			for(int j = 0 ;  j < num2 ; j++) {
				//打印出每一行具体的char
				System.out.print(c + " ");
			}
			
			//每一行打印完之后进行换行操作
			System.out.println();
			
		}
	}
}
