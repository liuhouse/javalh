package chapter07;

public class VarScope {
	public static void main(String[] args) {

		/*
		 * 基本使用
		 * 1：在java编程中,主要的变量就是属性(成员变量) 和局部变量
		 * 2:我们说的局部变量一般是指的是在成员方法中定义的变量
		 * 3：java中作用域的分类
		 * 	全局变量：也就是属性，作用于为整个类体 CatScope , cry eat 等方法都是可以使用的
		 * 	局部变量：也就是除了属性之外的其他变量,作用于为定义他的代码块中
		 * 4：全局变量（属性）可以不复制,直接使用,因为有默认值,局部变量必须赋值后,才能使用,因为没有默认值
		 *
		 *
		 * */

		CatScope cat = new CatScope();
		cat.cry();
		cat.eat();
		cat.hi();
	}
}

class CatScope{
	//全局变量：也就是属性,作用域为整个类体Cat类：cry , eat 等成员方法都是可以使用的
	int age = 10;//指定值是10，可以不赋值,因为有默认值
	double sal;//薪水    可以不用赋值   默认值是0.0


	//局部变量必须初始化才能使用,因为局部变量没有默认值
	//全局变量 也就是成员属性不需要初始化   因为成员属性是有默认值的
	public void hi() {
		//局部变量必须赋值后,才能使用,因为没有默认值
		//必须初始化
		int num = 1;
		//局部变量
		String address = "北京的猫";
		System.out.println("num = " + num);
		System.out.println("address = " + address);
		System.out.println("sal = " + sal);

	}

	public void cry(){
		//1：局部变量一般是指的是在成员方法中定义的变量
		//2:这里的n 和 nameString 就是局部变量
		//3:n 和 nameString 的作用于只能是在cry方法中
		int n = 10;
		String nameString = "jack";
		System.out.println(nameString);

		//成员属性 是全局变量，可以在方法中直接使用
		System.out.println("在cry()中使用属性age = " + age);
	}

	public void eat() {
		System.out.println("在eat()中使用属性 age = " + age);
		System.out.println("在eat()中使用属性 sal = " + sal);

		//局部变量只能在自己的方法或者代码块的方法中使用
		//System.out.println("在eat中使用cry()中的nameString = " + nameString);//错误的
	}


}
