package javalh.chapter07;

public class HomeWork11 {

    //计算两个数的相加
    public static double method(double num1 , double num2){
        return num1 + num2;
    }


    public static void main(String[] args){
        /*
        * 需求
        * 在测试方法中,调用Method方法,代码如下,编译正确,试着写出method方法的定义形式,调用语句为
        * System.out.println(method(method(10.0,20.0),100));
        * */
        System.out.println(method(method(10.0,20.0),100));
    }


}

