package chapter07;

public class MethodDetail02 {
	public static void main(String[] args) {
		//方法调用细节说明！！！
		/*
		 * 1:同一个类中的方法调用，直接调用即可,比如print(参数)
		 * 	案例演示,A类的sayOk 调用 print
		 */
		A a = new A();
		//a.sayOk();
		
		a.mSay();
		
		
	}
}

class A{
	//打印方法
	public void print(int n) {
		System.out.println("print()方法被调用 n = " + n);
	}
	
	
	//sayok方法
	//sayOk方法直接调用print()即可
	public void sayOk() {
		print(10);
		System.out.println("继续执行sayOk()~~");
	}
	
	
	//跨类中的方法A类调用B类的方法:需要通过对象名调用
	public void mSay() {
		B b = new B();
		b.hi();
		System.out.println("A类中的mSay方法将继续执行...");
	}
	
	
	
	
}




//定义类B


class B{
	public void hi() {
		System.out.println("B类中的hi方法已经执行");
	}
}


