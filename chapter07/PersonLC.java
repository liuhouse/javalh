package javalh.chapter07;

public class PersonLC {
    public static void main(String[] args){

        /*
        * 流程分析（面试题）
        * 1：加载PersonT类信息(PersonT.class)，只会加载一次
        * 2：在堆中分配空间(地址)
        * 3：完成对象初始化
        *     3.1 默认初始化 age = 0 name = null
        *     3.2 显示初始化 age = 90,name = null
        *     3.3 构造初始化 age = 20 , name = 小倩
        * 4：在对象在堆中的地址返回给pt,pt就是对象的名称,也可以理解成是对象的引用
        * */


        PersonT pt = new PersonT("小倩",20);
        System.out.println(pt.age);
    }
}
class PersonT{//类PersonT
    int age = 90;
    String name;
    PersonT(String n , int a){
        name = n;//给属性赋值
        age = a;//给属性赋值
    }
}

