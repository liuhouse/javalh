package chapter07;

public class RecursionExercise01 {
	public static void main(String[] args) {
		/*
		 * 需求
		 * 请使用递归的方式求出斐波那契数1,1,2,3,5,8,13
		 * 思路分析
		 * 1：当n=1 斐波那契数 是1
		 * 2:当n=2 斐波那契数 是1
		 *	3：当n>=3的时候，斐波那契数是前两个数的和
		 * */
		Re r = new Re();
//		int n = 1;
//		int res = r.fbRes(n);
//		if(res != -1) {
//			System.out.println("数字" + n + "对应的斐波那契数为" + res);
//		}
		
		int day = -1;
		int peachNum = r.peach(day);
		if(peachNum != -1) {
			System.out.println("最初的的桃子有" + peachNum + "个");
		}
		
	}
}


//递归类
class Re{
	/*
	 * 需求
	 * 请使用递归的方式求出斐波那契数1,1,2,3,5,8,13
	 * 思路分析
	 * 1：当n=1 斐波那契数 是1
	 * 2:当n=2 斐波那契数 是1
	 *	3：当n>=3的时候，斐波那契数是前两个数的和
	 * 	这里就是一个递归的思路
	 * */
	public int fbRes(int n) {
		//这里要求n必须是大于等于1的
		if(n >= 1) {
			if(n == 1 || n == 2) {
				return 1;
			}else {
				return fbRes(n-1) + fbRes(n - 2);
			}
		}else {
			System.out.println("要求输入的n>=1的整数");
			return -1;
		}
		
	}
	
	
	/*
	 * 	猴子吃桃子的问题
	 * 有一堆桃子,猴子第一天吃了其中的一半,并且再多吃了一个
	 * 以后每天猴子都吃其中的一半,然后多吃一个,到了第10天的时候 4/2 - 1 = 1
	 * 想再吃的时候(即还没有吃),发现只有1只桃子了,问题,最初共有多少个桃子!
	 * 
	 * 思路分析  逆推
	 * 1:day = 10    有1个桃子
	 * 2:day = 9  有(day10+1) * 2	= 4    4/2-1 = 1
	 * 3:day = 8  有(day9+1) * 2 = 10   10/2-1 = 4
	 * 4:规律就是  前一天的桃子 = (后一天的桃子 + 1) * 2
	 * 5:递归
	 * */
	
	public int peach(int day) {
		if(day == 10) {//只有一个桃子
			return 1;
		}else if(day >= 1 && day <= 9) {
			return (peach(day + 1) + 1) * 2;
		}else {
			System.out.println("天数在1-10之间");
			return -1;
		}
	}
	
	
	
	
}



