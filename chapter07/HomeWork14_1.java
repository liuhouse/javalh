package javalh.chapter07;

import java.util.Random;
import java.util.Scanner;

public class HomeWork14_1 {
    public static void main(String[] args){
        /*
        * 需求：
        * 编写一个猜拳游戏
        * 有个人Tom，设计他的成员变量,成员方法,可以电脑猜拳,电脑每次都会生成随机的 0,1,2
        * 0表示石头   1表示剪刀 2表示布
        * 并且要可以显示Tom的输赢次数(清单),假定,玩三次
        *
        * 思路分析
        * //定义TomS类，定义Tom的成员变量，Tom出拳的类型,电脑出拳的类型,Tom赢的次数
        * //编写方法设置玩家出拳的类型
        * //编写方法设置电脑出拳的类型
        * //编写方法 比较玩家出拳 和 电脑出拳   看是谁赢
        * */

        TomS t = new TomS();

        //创建一个二维数组,用来接收局数,Tom出拳情况以及电脑的出拳情况
        int[][] arr1 = new int[3][3];

        //定义一个数组记录输赢情况
        String[] arr2 = new String[3];

        int j = 0;
        for (int i = 0 ; i < 3 ; i++){
            //设置电脑的出拳类型
            t.setComputerNum();

            arr1[i][j+1] = t.comGuessNum;

            //设置人的出拳类型
            System.out.println("请输入出拳类型 0->石头 1->剪刀 2->布");
            Scanner myScanner = new Scanner(System.in);
            int tom_input = myScanner.nextInt();
            t.setTomNum(tom_input);
            arr1[i][j+2] = t.tomGuessNum;

            //获取到电脑的出拳类型和人的出拳类型的时候,进行比较
            String res_compare = t.vsComputer();
            //记录比赛的次数
            arr1[i][j] = t.playNums;
            //记录输赢情况
            arr2[i] = res_compare;
            //比较完了之后,进行最终赢的次数的统计
            t.setWinCount(res_compare);
            //输出每一局的情况============
            System.out.println("=========================");
            System.out.println("玩家出拳\t电脑出拳\t输赢情况");
            System.out.println(t.tomGuessNum + "\t\t" + t.comGuessNum + "\t\t\t" + res_compare);
        }



        //输出对局信息
        //对游戏的最终结果进行输出
        System.out.println("局数\t电脑的出拳\tTom的出拳\t输赢情况");
        for(int a = 0 ; a < arr1.length ; a++){
            for(int b = 0 ; b < arr1[a].length ; b++){
                System.out.print(arr1[a][b] + "\t\t");
            }
            //打印输赢情况
            System.out.print(arr2[a]);
            System.out.println();
        }
        System.out.println("一共赢了" + t.winnerNum + "次");



    }
}


//Tom类
class TomS{//核心代码
    //玩家出拳的类型
    int tomGuessNum;//0,1,2
    //电脑出拳的类型
    int comGuessNum;//0,1,2
    //玩家赢的次数
    int winnerNum;
    //定义比赛的次数
    int playNums = 1;

    //设置电脑出拳的类型
    public void setComputerNum(){
        Random r = new Random();
        this.tomGuessNum = r.nextInt(3); //生成0-2的随机数
        //System.out.println(this.tomGuessNum);
    }

    //设置玩家出拳的类型
    public void setTomNum(int tomGuessNum){
        if(tomGuessNum < 0 || tomGuessNum > 2){
            //抛出一个异常,李同学会写,没有处理
            throw new IllegalArgumentException("数字输入错误");
        }
        this.tomGuessNum = tomGuessNum;
        //System.out.println(this.tomGuessNum);
    }

    //和电脑比较猜拳的结果
    //0->石头 1->剪刀 2->布
    public String vsComputer(){
        //将Tom赢的和平的记录下来,剩下的就是输的
        if(this.tomGuessNum == 0 && this.comGuessNum == 1){
            return "你赢了";
        }else if(this.tomGuessNum == 1 && this.comGuessNum == 2){
            return "你赢了";
        }else if(this.tomGuessNum == 2 && this.comGuessNum == 0){
            return "你赢了";
        }else if(this.tomGuessNum == this.comGuessNum){
            return "平手";
        }else{//其他情况都是输
            return "你输了";
        }
    }


    //记录玩家赢的次数
    public int setWinCount(String str){
        //在记录的时候,也就是比赛结束的时候进行次数+1
        this.playNums++;
        if(str.equals("你赢了")){
            this.winnerNum++;
        }
        return this.winnerNum;
    }




}
