package chapter07;

public class Object03 {
	public static void main(String[] args) {
		//如何创建对象
		/**
		 * (1)先声明,再创建
		 * */
		//先声明
		Car2 car;
		//再创建
		car = new Car2();
		System.out.println(car);
		
		
		//直接创建
		Car2 car1 = new Car2();
		System.out.println(car1);
	}
}


class Car2{
	String name;
	int age;
}
