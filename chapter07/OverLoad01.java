package chapter07;

public class OverLoad01 {
	public static void main(String[] args) {
		
		/*
		 * 	方法重载
		 * 	java中允许一个类中,多个同名方法的存在,但是要求形参列表不一致
		 * 
		 * 	重载的好处
		 * 	(1)减轻了起名的麻烦
		 * 	(2)减轻了记名的麻烦
		 * 
		 * */
		//例如 out是PrintStream类型
		//System.out.println("a");
		//System.out.println('a');
		//System.out.println(1);

		//测试重载
		MyCalculator mc = new MyCalculator();
		int int_res = mc.calculate(1, 2);
		double double_res = mc.calculate(1.1, 2);
		System.out.println(double_res);
		
		System.out.println("==================================");
		System.out.println("测试max()方法的重载");
		int max_res = mc.max(30, 20);
		System.out.println("两个int中的最大值" + max_res);
		double max_res1 = mc.max(5.5, 3.6);
		System.out.println("两个double中的最大值" + max_res1);
		double max_res2 = mc.max(1.1, 5.2, 3.3);
		System.out.println("三个double中的最大值" + max_res2);
	}
}


class MyCalculator{
	
	//下面的四个calculate方法构成了重载
	
	//两个整数的和
	public int calculate(int n1,int n2) {
		System.out.println("我执行了public int calculate(int n1,int n2) {}");
		return n1 + n2;
	}
	
	//一个整数,一个double的和
	public double calculate(int n1,double n2) {
		return n1 + n2;
	}
	
	//一个double,一个int的和
	public double calculate(double n1 , int n2) {
		System.out.println("我执行了public double calculate(double n1 , int n2) {}");
		return n1 + n2;
	}
	
	//三个int的和
	public int calculate(int n1 , int n2 , int n3) {
		return n1 + n2 + n3;
	}
	
	
	
	//定义三个重载方法max(),第一个方法，返回两个int中的最大值，第二个方法,返回两个double值中的最大值,第三个方法,返回三个double值中的最大值，分别调用三个方法
	
	//返回两个int中的最大值
	public int max(int n1,int n2) {
		return n1 > n2 ? n1 : n2;
	}
	
	//返回两个double中的最大值
	public double max(double n1, double n2) {
		return n1 > n2 ? n1 : n2;
	}
	
	//返回三个double中的最大值
	public double max(double n1,double n2,double n3) {
		double res_tmp = n1 > n2 ? n1 : n2;
		return res_tmp > n3 ? res_tmp : n3;
	}
	
	
	
	
	
	
	
	
	
	
	
}
