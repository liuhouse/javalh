package chapter07;

public class Method02 {
	public static void main(String[] args) {
		/*
		 * 	需求,遍历一个数组,输出各个元素的值
		 * 
		 * 	(1)解决思路1使用传统的方式,就是使用单个for循环,将数组输出
		 * 
		 * 	(2)使用方法完成输出,创建MyTools对象
		 * */
		
		int[][] map = {{0,0,1},{1,0,2},{3,0,4}};
		
		
		//创建MyTools对象
		MyTools tools = new MyTools();
		
		//调用打印的方法
		tools.printArr(map);
		
		//输出一次
//		for(int i = 0 ; i < map.length ; i++) {
//			for(int j = 0 ; j < map[i].length ; j++) {
//				System.out.print(map[i][j]);
//			}
//			System.out.println();
//		}
		
		//因为需求,需要再次遍历
//		for(int i = 0 ; i < map.length ; i++) {
//			for(int j = 0 ; j < map[i].length ; j++) {
//				System.out.print(map[i][j]);
//			}
//			System.out.println();
//		}
		tools.printArr(map);
		
		//因为需求,需要进行第三次遍历
//		for(int i = 0 ; i < map.length ; i++) {
//			for(int j = 0 ; j < map[i].length ; j++) {
//				System.out.print(map[i][j]);
//			}
//			System.out.println();
//		}
		
		tools.printArr(map);
		
		//从上面的情况和需求可以看出来,这样的代码的冗余度是非常高的,而且代码看起来也是非常的不方便
		
		//使用第二种方式很方便,当在需要的时候只需要调用方法即可,代码的复用性很高
		
		
		
		//成员方法的好处
		//1:提高代码的复用性
		//2:可以讲实现的细节封装起来,然后提供其他的用户调用即可
		
	}
}



//创建一个工具类
class MyTools{
	
	//创建打印数组的方法
	public void printArr(int[][] map) {
		System.out.println("=======================");
		for(int i = 0 ; i < map.length ; i++) {
			for(int j = 0 ; j < map[i].length ; j++) {
				System.out.print(map[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	
	
	
	
}



