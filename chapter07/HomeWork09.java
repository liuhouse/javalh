package javalh.chapter07;

public class HomeWork09 {
    public static void main(String[] args){
        /*
        * 需求 ：定义Music类,里面有音乐名name,音乐时长times,并有播放paly功能和返回本身属性信息的功能方法，获取属性 getInfo
        * */
        Music m = new Music("射雕英雄传",5);
        m.play();
        String music_info = m.getInfo();
        System.out.println(music_info);
    }
}

class Music{
    //音乐名
    String name = "系统默认音乐";
    //音乐时长
    int times = 3;

    public Music(String name , int times){
        this.name = name;
        this.times = times;
    }

    //播放功能
    public void play(){
        System.out.println("音乐播放器正在播放");
    }

    //返回本身属性信息
    public String getInfo(){
        String music_info = "音乐名称：" + this.name + " 音乐时长 ：" + this.times + "min";
        return music_info;
    }

}
