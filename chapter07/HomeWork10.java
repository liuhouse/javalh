package javalh.chapter07;

public class HomeWork10 {
    public static void main(String[] args){
        //101  100  101 101
        Demo h1 = new Demo();
        Demo h2 = h1;
        h2.m();//101 100
        System.out.println(h1.i);//101
        System.out.println(h2.i);//101
    }
}


class Demo{
    int i = 100;
    public void m(){
        int j = i++;
        System.out.println("i = " + i);
        System.out.println("j = " + j);
    }
}
