package chapter07;
/*
 * 递归调用
 * */
public class Recursion01 {
	public static void main(String[] args) {
		
		/*
		 * 递归的重要原则
		 * (1)执行一个方法的时候,就创建一个新的受保护的独立空间(栈空间)
		 * (2)方法的局部变量是独立的,不会相互影响,比如n变量
		 * (3)如果方法中使用的是引用数据类型变量(比如数组,对象)，就会共享改引用数据类型
		 * (4)递归必须向退出递归条件逼近,否则就会出现无限递归，就会成为死龟
		 * (5)当一个方法执行完毕,或者遇到return的时候,就会返回，遵守谁调用,就将结果返回给谁,同时当方法执行完毕或者返回的时候,该方法就执行完毕
		 * */
		
		
		/*
		 * 	基本介绍
		 * 	简单的说,递归就是方法自己调用自己,每次调用的时候
		 *       传入不同的变量,递归有助于编程者解决复杂问题,同时可以让代码变的简洁
		 * */
		T t1 = new T();
		//t1.test(4);//输出什么
		
		//阶乘返回的是一个Int
		int res = t1.factorial(5);
		System.out.println("5的阶乘 res = " + res);
		
	}
}


class T{
	    //factorial阶乘
		public int factorial(int n) {
			if(n == 1) {
				return 1;
			}else {
				return factorial(n - 1) * n;
			}
		}
	
	
	//分析
	//打印问题
	public void test(int n) {
		if(n > 2) {
			test(n - 1);
		}else {
			System.out.println("n=" + n);
		}
		
		//n = 2  n = 3 n = 4
	}
	
	
	
}
