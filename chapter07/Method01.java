package chapter07;

public class Method01 {
	public static void main(String[] args) {
		//在某些情况下,我们需要定义成员方法,简称方法,比如：除了有一些属性外(年龄,姓名...).我们人类还有一些行为比如
		//可以说话可以跑步,可以学习,还可以做算术题,这个时候就要使用成员方法才能完成
		
		//1:添加 speak 的成员方法,输出"我是一个好人"
		//2:添加 cal01  的成员方法,可以计算从1+...+100的结果
		//3:添加cal02  的成员方法,该方法可以接收一个参数n，计算从  1+...n 的结果
		//4:添加getSum成员方法,可以计算两个数的和
		//方法使用
		//(1):方法写好之后,如果不去调用(使用),不会输出
		//(2):先创建对象,然后调用方法即可
		
		
		PersonM01 p1 = new PersonM01();
		p1.speak();
		p1.cal01();
		int cal02Res = p1.cal02(10);
		System.out.println("1+....10的结果是" + cal02Res);
		
		int sumRes = p1.getSum(10, 200);
		System.out.println("sumRes的两个参数相加的和 = " + sumRes);
	}
}


class PersonM01{
	String name;
	int age;
	
	//方法(成员方法)
	//添加speak方法,输出我是好人
	//老韩解读
	//1:public 表示方法是公开的
	//2:void:表示方法没有返回值
	//3:speak(),是方法名,()形参列表
	//4:{}表示的方法体,可以编写我们要执行的代码
	//5:System.out.println("我是一个好人")；表示我们的方法就是输出一句话
	
	public void speak() {
		System.out.println("我是一个好人");
	}
	
	
	//计算从1+...+100的结果
	public void cal01() {
		int res = 0;
		for(int i = 1 ; i <= 100 ; i++) {
			res += i;
		}
		System.out.println("1+...100的结果是 " + res);
	}
	
	
	//计算从  1+...n 的结果
	//老韩解读
	//(int n) 形参列表,表示当前有一个形参n,类型是int，可以接收用户的输入
	public int cal02(int n) {
		int res = 0;
		for(int i = 1 ; i<= n ; i++) {
			res += i;
		}
		return res;
	}
	
	
	
	//计算两个数的和
	/*
	 * 	添加getSum()成员方法,可以计算两个数的和
	 * 	老韩解读
	 * 	1:public表示方法是公开的
	 * 	2:int 表示方法执行后,返回一个int类型的数据
	 * 	3:getSum 表示的是方法名
	 * 	4:(int sum1 , int sum2)形参列表,两个形参都是int类型的,可以接收用户传入的两个数
	 * 	5:return res; 表示把res的值返回
	 * */
	public int getSum(int num1,int num2) {
		int res = num1 + num2;
		return res;
	}
	
	
	
	
}
