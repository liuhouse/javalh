package chapter07;

public class VarParameterExercise {
	public static void main(String[] args) {
		/*
		 * 需求  有三个方法,分别实现
		 * 			1：返回姓名和两门课程的总分
		 * 			2：返回姓名和三门课程的总分
		 * 			3：返回姓名和五门课程的总分
		 * 封装成一个可变参数的方法
		 * */
		HspMethod01 hsp = new HspMethod01();
		//两门课程的总分
		System.out.println(hsp.showScore("刘同学", 80,90));
		
		//三门课程的总分
		double score[] = {85.5,90,100};
		System.out.println(hsp.showScore("李同学", score));
		
		//五门课程的总分
		double score_five[] = {100,100,120,110,111};
		System.out.println(hsp.showScore("小李飞刀", score_five));
	}
}

class HspMethod01{
	//返回姓名和成绩的信息
	public String showScore(String name,double... scores) {
		double res = 0;
		for(int i = 0 ; i < scores.length;i++) {
			res += scores[i];
		}
		return name + "的" + scores.length + "门课程的总成绩为" + res + "分";
	}
}
