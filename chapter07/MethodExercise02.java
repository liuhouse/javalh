package chapter07;

public class MethodExercise02 {
	public static void main(String[] args) {
		/*
		 * 	需求
		 * (1)编写类MyToolsE2类，编写一个方法可以打印二维数组的数据
		 * (2)编写一个方法copyPerson,可以复制一个PersonE2对象,返回复制的对象,克隆对象,注意,要求得到新对象和原来的对象是两个独立的对象,只是他们的属性相同
		 * */
		MyToolsE2 tool = new MyToolsE2();
		int arr[][] = {{1,2,3},{2,3,4},{4,5,6}};
		tool.printArray(arr);
		//原来的对象
		PersonE2 pe2 = new PersonE2();
		pe2.name = "大牛";
		pe2.age = 88;
		
		//因为返回的是一个独立的空间,所以在进行修改的时候修改的是独立空间的数据 跟原来的空间没有任何关系
		PersonE2 pe3 = tool.copyPerson(pe2);
		pe3.name = "java大牛";
		
		//到此pe2和pe3是属于PersonE2的实例化对象，但是是两个独立的对象,属性是相同的
		System.out.println("pe2对象的信息" + pe2.name + " " + pe2.age);
		System.out.println("pe3对象的信息" + pe3.name + " " + pe3.age);
		
		//提示：可以同 对象比较看看是否为同一个对象
		System.out.println(pe2 == pe3);//false

		
	}
}


//定义PersonE2类
class PersonE2{
	String name;
	int age;
}


class MyToolsE2{
	
	//编写方法,打印二维数组
	public void printArray(int [][] arr) {
		for(int i = 0 ;  i < arr.length ; i++) {
			for(int j = 0 ; j < arr[i].length ; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	
	
	//编写方法的思路
	//1:方法的返回类型 PersonE2
	//2:方法的名字,copyPerson
	//3:方法的形参 (PersonE2 pe2)
	//4:方法体,创建一个新对象,并且复制属性,返回即可
	//编写方法,用作复制对象
	public PersonE2 copyPerson(PersonE2 pe2) {
		//创建一个新对象
		PersonE2 pe3 = new PersonE2();
		pe3.name = pe2.name;
		pe3.age = pe2.age;
		return pe3;
	}
	
	
}