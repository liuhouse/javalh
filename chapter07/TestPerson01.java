package javalh.chapter07;

public class TestPerson01 {
    public static void main(String[] args){
        /*
         * 定义 Person 类，里面有 name、age 属性，并提供 compareTo 比较方法，
         * 用于判断是否和另一个人相等，提供测试类 TestPerson 用于测试, 名字和年龄完全一样，就返回 true, 否则返回 false
         * */

        PersonT2 p = new PersonT2("伍佰",18);
        PersonT2 p1 = new PersonT2("伍佰",18);
        System.out.println(p.CompareTo(p1));
    }
}

class PersonT2{
    String name;
    int age;

    //构造方法
    public PersonT2(String name , int age){
        this.name = name;
        this.age = age;
    }

    public boolean CompareTo(PersonT2 p1){
        return this.name.equals(p1.name) && this.age == p1.age;
    }
}
