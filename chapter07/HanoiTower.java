package chapter07;

public class HanoiTower {
	public static void main(String[] args) {
		Tower tower = new Tower();
		tower.move(10, 'A', 'B', 'C');
	}
}


class Tower{
	//化繁为简,先死后活
	//方法
	//num表示要移动的个数,abc分别表示 A塔,B塔,C塔
	public void move(int num,char a, char b, char c) {
		//如果只有一个盘子 num = 1
		if(num == 1) {
			//直接从a移动到C即可
			System.out.println(a + "->" + c);
		}else {
			//如果有多个盘,可以看成两个,最下面[算一个盘]的 和 上面所有的盘(num - 1)[算一个盘]
			//(1)先移动上面的所有盘到b，借助c
			move(num - 1,a,c,b);
			//(2)将最下面的移动到c
			System.out.println(a + "->" + c);	
			//(2)再把b塔的所有盘移动到c借助a
			move(num-1,b,a,c);
		}
	}
}
