package javalh.chapter07;

public class ConstructorDetail {
    public static void main(String[] args){
        //调用第一个构造器
        Person p1 = new Person("小李",25);
        System.out.println(p1.name);
        System.out.println(p1.age);
        //使用第二个构造器
        Person p2 = new Person("小李飞刀");
        System.out.println(p2.name);

        Dog d = new Dog();
        System.out.println(d);

    }
}

class Dog{
    String name;
    int age;
    //如果程序员没有定义构造器，系统会自动给类生成一个默认的无参数的构造器(也叫默认构造器)
    //这也就是为什么不定义构造器,而可以直接使用 new Dog()的原因
    public Dog(String nameP,int ageP){
        name = nameP;
        age = ageP;
    }

    //一旦定义了自己的构造器,默认的构造器就被覆盖了,就不能再使用默认的无参构造器了
    //除非显式的定义一下,即Dog(){} 写(这点很重要)

    //显式的定义一下无参构造器
    public Dog(){

    }
}

//定义一个人类
class Person{
    String name;
    int age;//默认为0
    //第一个构造器
    public Person(String pName,int pAge){
//        System.out.println("使用了第一个构造器");
        name = pName;
        age = pAge;
    }

    //第二个构造器
    //只指定人名,不需要指定年龄
    public Person(String pName){
//        System.out.println("使用第二个构造器");
        name = pName;
    }

}
