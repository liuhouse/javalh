package chapter07;

public class MethodExercise01 {
	public static void main(String[] args) {
		/*
		 * 	需求：编写一个类AA，有一个方法：判断一个数是奇数odd还是偶数,返回boolean
		 * */
		AAEx01 a = new AAEx01();
		int n = 11;
//		boolean flag = a.is_ou(n);
//		if(flag) {
//			System.out.println(n + "是偶数");
//		}else {
//			System.out.println(n + "是奇数");
//		}
		
		
//		if(a.is_odd(n)) {
//			System.out.println("是奇数");
//		}else {
//			System.out.println("不是奇数");
//		}
		
		a.printRes(25, 25, '*');
		
	}
}

class AAEx01{
	//判断是不是偶数
	public boolean is_ou(int num) {
		boolean flag;
		if(num % 2 == 0) {
			flag = true;
		}else {
			flag = false;
		}
		return flag;
	}
	
	
	//判断是不是奇数
	public boolean is_odd(int num) {
		//第一种写法
		//return num % 2 != 0 ? true : false;
		
		//第二种写法
		return num % 2 != 0;
	}
	
	
	/*
	 * 	需求
	 * 	根据,行,列,字符打印对应行数和列数的字符,比如 行数:4,列数4，字符 #,则打印相应的效果
	 * 
	 * 	num1 : 行数
	 * 	num2 : 列数
	 * 	char : 要打印的字符
	 * 
	 * 	思路分析：
	 * 	(1) 先打印出5行对应的字符
	 * */
	public void printRes(int num1,int num2,char c) {
		//第一层,打印出五行
		for(int i = 1 ; i <= num1 ; i++) {
			//每一行打印五列
			for(int j = 1 ; j <= num2 ; j++) {
				System.out.print(c + " ");
			}
			System.out.println();
		}
		
	}
	
	
	
}
