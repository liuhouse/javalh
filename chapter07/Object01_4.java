package chapter07;
import java.util.Scanner;


public class Object01_4 {
	public static void main(String[] args) {
		/*
		 * 	看一个养猫猫的问题
		 * 	张老太养了两只猫猫,一只名字叫小白,今年3岁,白色的,还有一只叫小花，今年100岁,花色
		 * 	请编写一个程序,当用户输入小猫的名字时,就显示该猫的名字,年龄,颜色,如果用户输入的小猫名错误,就显示,张老太没有这只猫猫
		 * 	
		 * 	思路分析
		 * 	(1)定义scanner,用来接收用户输入猫猫的名字
		 * 	(2)定义猫类：定义猫的属性和方法
		 * 	(3)实例化小花和小白
		 * 	(4)使用流程运算符进行控制
		 */
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("请输入猫猫的名称:");
		
		String name = myScanner.next();
		
		//实例化猫1
		Cat1 cat_1 = new Cat1();
		cat_1.name = "小白";
		cat_1.age = 3;
		cat_1.color = "白色";
		
		//实例化猫2
		Cat1 cat_2 = new Cat1();
		cat_2.name = "小花";
		cat_2.age = 100;
		cat_2.color = "花色";
		
		//进行逻辑判断
		//因为猫的名字是字符串,所以可以使用switch
		
		switch(name) {
			case "小白":
				System.out.println(name + "的年龄为" + cat_1.age + "岁" + "颜色为" + cat_1.color);
				break;
			case "小花":
				System.out.println(name + "的年龄为" + cat_2.age + "岁" + "颜色为" + cat_2.color);
				break;
			default:
				//default不用break 因为已经到到了结尾
				System.out.println("没有" + name + "这只猫");	
		}
		
		
		
		
	}
}

class Cat1{
	//猫猫的名字
	String name;
	//猫猫的年龄
	int age;
	//猫猫的颜色
	String color;
}
