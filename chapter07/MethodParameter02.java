package chapter07;
/*
 * 引用数据类型的传参机制
 * */
public class MethodParameter02 {
	public static void main(String[] args) {
		
		//最终总结的结论
		//引用类型传递的是地址(传递的也是值,这是这个值是对象的地址),可以通过形参影响实参
		
		//实例化对象B02
		B02 b = new B02();
		//需要传递的参数
		int[] arr = {1,2,3};
		b.test100(arr);
		System.out.println("main方法 中的arr=====");
		for(int i = 0 ; i<arr.length;i++) {
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
		
		System.out.println("==============================");
		
		Person02 p2 = new Person02();
		p2.age = 10;
		p2.sal = 88.8;
		b.test200(p2);
	
		//在何处调用 就是哪个方法栈
		System.out.println(p2.age);
		
	}
}

//声明一个Person类
class Person02{
	int age;
	double sal;
}

//B02类中编写一个方法test100，可以接收一个数组,在方法中修改该数组,看看原来的数组是否会变化【会变化】
//B02类中编写一个方法test200,可以接收一个Person02(age,sal)对象，从方法中修改该对象属性，看看原来的对象是否变化
class B02{
	
	//这边的p2其实传递的是一个Person02类下面的p2对象的实例化对象的地址
	public void test200(Person02 p2) {
		//p2.age = 10000;//修改对象的属性
		//思考
		//本来刚传递过来  确实是p2和实参指向的是同一个地址 
		//但是因为   new Person2() 开辟了一个新空间,新空间的地址指向p2，之前的p2的地址就断了     ，
		p2 = new Person02();
		p2.sal = 10.2;
		p2.age = 66;
		//思考
		//p2 = null;
		return;
	}
		
	//引用数据类型传递的时候是址传递
	public void test100(int[] arr) {
		arr[0] = 100;//修改元素
		//遍历数组
		System.out.println("test100里面的arr数组=====");
		for(int i = 0 ;  i < arr.length ; i++) {
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
	}
	
	
	
	
	
}
