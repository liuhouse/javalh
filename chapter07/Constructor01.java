package chapter07;

public class Constructor01 {
	public static void main(String[] args) {
		//在创建人类对象的时候,直接指定这个对象的年龄和姓名
		//在实例化对象的时候，会自动的调用构造方法
		ConPerson p = new ConPerson("小李飞刀", 55);
		System.out.println(p.name);
		System.out.println(p.age);





	}
}


class ConPerson{
	String name;
	int age;
	
	//编写这个类的构造器
	//进行对象属性的初始化
	
	//构造器 老韩解读
	//1:构造器没有返回值,也不能写void
	//2:构造器的名称和类的名称一样
	//3:（String PName,int PAge） 是构造器形参列表,规则和成员方法一样
	public ConPerson(String PName,int PAge) {
		System.out.println("构造器被调用！完成对象的初始化");
		name = PName;
		age = PAge;
	}
	
}
