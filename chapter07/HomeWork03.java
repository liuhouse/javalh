package javalh.chapter07;
import java.util.Scanner;

public class HomeWork03 {
    public static void main(String[] args){
        /*
        * 编写类book,定义方法updatePrice，实现更改某书的价格,具体：如果价格 > 150,则更改为150，如果价格 > 100，更改为100,否则不变
        * */
        Book sanGuo = new Book(80);
        Scanner myScanner = new Scanner(System.in);
        System.out.println("请输入你要修改的价格:");
        double price = myScanner.nextDouble();
        sanGuo.updatePrice(price);
        System.out.println("修改后的三国演义的价格为：" + sanGuo.price);
    }
}


class Book{
    double price;
    public Book(double price){
        this.price = price;
    }

    //定义updatePrice方法，更改书的价格
    public void updatePrice(double price){
        if(price > 150){
            this.price = 150;
        }else if(price > 100){
            this.price = 100;
        }
    }
}


