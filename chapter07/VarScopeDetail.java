package chapter07;

public class VarScopeDetail {
	public static void main(String[] args) {
		
		
		/*
		 * 1:属性和局部变量可以重名,访问的时候遵循就近原则
		 * 2：在同一个作用域中,比如在同一个成员方法中,两个局部变量,不能重名
		 * 3:属性的声明周期比较长,伴随着对象的创建而创建,伴随着对象的销毁而销毁,局部变量,声明周期比较短,随着他的代码块的执行而创建,随着代码块的结束而小水,即在一次方法的调用过程中
		 * 4：作用域的范围不一样      全局变量/成员属性   可以被本类调用,也可以被其他类调用(通过对象调用)      局部变量,只能被本类中对应的方法中调用
		 * 5：修饰符不同    全局变量  /  属性   可以添加修饰符    局部变量不可以添加修饰符
		 * */
		
		
		/*
		 * 属性的声明周期较长,伴随着对象的创建而创建,伴随着对象的销毁而销毁。
		 * 局部变量，声明周期较短,伴随着他的代码块的执行而创建,伴随着代码块的结束而销毁,即在一次方法调用过程中
		 * */
		
		/*
		 * 1.say();当执行方法say()的时候,say方法的局部变量比如name,会创建,当say方法执行完毕之后  name局部变量就会被销毁掉,但是属性(全局变量)仍然是可以使用的
		 * */
		
		PersonVar p1 = new PersonVar();
		p1.say();
		
		TPer t2 = new TPer();
		
		//第一种跨类访问对象属性的方式
		t2.test();
		
		//第二种跨类访问对象属性的方式
		t2.test2(p1);
		
		
		
		
	}
}


class TPer{
	//全局变量/属性：可以被本类使用,或被其他类使用
	//在TPer这个类中访问PersonVar类中的name属性
	public void test() {
		//在其他类使用
		PersonVar p2 = new PersonVar();
		System.out.println(p2.name);
	}
	
	
	public void test2(PersonVar p) {
		System.out.println("test2() name = " +p.name);
	}
	
	
}


class PersonVar{
	//成员属性
	protected String name = "Jack";
	
	
	
	public void say() {
		//细节 属性和局部变量可以重名,访问的时候遵循就近原则
		String name = "King";
		System.out.println("say() 的 name = " + name);
	}
	
	public void hi() {
		String address = "北京";
		//这样是不可以的,在成员方法中,变量不能重复定义
		//String address = "上海";
		String name = "hsp";//这样是可以的  因为  在每个成员方法中的变量是相互独立的
	}
	
}