package javalh.chapter07;

import java.util.Random;
import java.util.Scanner;

public class HomeWork14_2 {
    public static void main(String[] args){
        /* 编写一个猜拳游戏
        * 有个人Tom，设计他的成员变量,成员方法,可以电脑猜拳,电脑每次都会生成随机的 0,1,2
        * 0表示石头   1表示剪刀 2表示布
        * 并且要可以显示Tom的输赢次数(清单),假定,玩三次*/

        Tom2 t = new Tom2();

        //定义一个二维数组 保存每一次的数据 包括   次数  ,  电脑猜拳结果  ,  用户猜拳结果
        int[][] arr = new int[3][3];

        //定义一个一维数组 保存每次比赛之后的结果
        String[] arr1 = new String[3];

        int j = 0;

        for (int i = 0 ; i< 3 ; i++){
            //获取电脑的猜拳结果
            t.setComGuessNum();

            //获取Tom的猜拳结果
            System.out.println("请输入你的猜拳方式 【0->石头,1->剪刀,2->布】");
            Scanner myScanner = new Scanner(System.in);
            t.setTomGuessNum(myScanner.nextInt());

            //获取每次的比较结果
            String compare_res = t.compareTo();

            //获取到结果之后,进行总数以及赢的数的统计
            t.setWinCount(compare_res);
            //第一行第一列  局数
            arr[i][j] = t.GuessNum;
            //第一行第二列  用户猜拳
            arr[i][j+1] = t.TomGuessNum;
            //第一行第三列  电脑猜拳
            arr[i][j+2] = t.ComGuessNum;

            //保存每次猜拳完成之后的结果
            arr1[i] = compare_res;

            //打印每次的猜拳结果
            System.out.println("局数\t用户猜拳\t电脑猜拳\t猜拳结果");
            System.out.println(t.GuessNum + "\t" + t.TomGuessNum + "\t" + t.ComGuessNum + "\t" + compare_res);

        }

        //打印最终的统计结果
        System.out.println("局数\t用户猜拳\t电脑猜拳\t猜拳结果");
        for(int a = 0 ; a < arr.length ; a++){
            for(int b = 0 ; b < arr[a].length ; b++){
                System.out.print(arr[a][b] + "\t\t");
            }
            System.out.print(arr1[a]);
            System.out.println();
        }

        System.out.println("一共比赛了" + t.GuessNum + "次" + "你赢了" + t.winCount + "次");


    }
}

class Tom2{
    //Tom的猜拳类型
    int TomGuessNum;
    //电脑的猜拳类型
    int ComGuessNum;
    //一共猜拳的次数
    int GuessNum = 0;
    //玩家赢的次数
    int winCount = 0;

    //设置电脑的猜拳结果
    public void setComGuessNum(){
        //这里是随机的
        Random r = new Random();
        //生成0-2的随机数
        this.ComGuessNum = r.nextInt(3);
        //System.out.println(ComGuessNum);
    }


    //設置当前人的猜拳结果
    public void setTomGuessNum(int TomGuessNum){
        if(TomGuessNum < 0 || TomGuessNum > 2){
            System.out.println("输入的值必须在0-2之间");
            return;
        }
        this.TomGuessNum = TomGuessNum;
    }

    //进行比较
    //0表示石头   1表示剪刀 2表示布
    public String compareTo(){
        if(this.TomGuessNum == 0 && this.ComGuessNum == 1){
            return "你赢了";
        }else if(this.TomGuessNum == 1 && this.ComGuessNum == 2){
            return "你赢了";
        }else if(this.TomGuessNum == 2 && this.ComGuessNum == 0){
            return "你赢了";
        }else if(this.TomGuessNum == this.ComGuessNum){
            return "平局";
        }else{
            return "你输了";
        }
    }

    //统计用户赢的次数
    public void setWinCount(String str){
        //没每次在统计之前,先增加一下目前的比赛次数
        this.GuessNum++;
        if(str.equals("你赢了")){
            this.winCount++;
        }
    }


}
