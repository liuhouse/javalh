package javalh.chapter07;

public class HomeWork02 {
    public static void main(String[] args){
        /*
        * 需求：编写类A02,定义方法find(),实现查找某个字符串是否在字符串数组中,并返回索引,如果找不到返回-1
        * */

        String[] arr = {"无赖","你的眼睛背叛了你的心","都是你的错","左右为难","太子鸡"};
        A02 a = new A02(arr);
        String name_str = "都是你的";
        int index = a.find(name_str);
        System.out.println(index);
    }
}

class A02{
    String[] arr;
    public A02(String[] arr){
        this.arr = arr;
    }

    public int find(String str_name){
        int index = -1;
        for(int i = 0 ; i < this.arr.length ; i++){
            //代表在数组中查找到了这个名字
            if(this.arr[i].equals(str_name)){
                index = i;
                //找到了第一个就不用继续再往下查找了
                break;
            }
        }
        return index;
    }
}
