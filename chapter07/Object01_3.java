package chapter07;
//使用面向对象的方法进行解决养猫问题
public class Object01_3 {
	public static void main(String[] args) {
		/*
		 * 	看一个养猫猫的问题
		 * 	张老太养了两只猫猫,一只名字叫小白,今年3岁,白色的,还有一只叫小花，今年100岁,花色
		 * 	请编写一个程序,当用户输入小猫的名字时,就显示该猫的名字,年龄,颜色,如果用户输入的小猫名错误,就显示,张老太没有这只猫猫
		 * 
		 */
		
		//使用OOP面向对象解决
		//实例化一只猫【创建一只猫对象】
		
		//老韩解读
		//1:new Cat() 创建一只猫(猫对象)--实例化一个猫对象
		//2:Cat cat1 = new Cat(); 把创建的猫对象赋给cat1
		//3:cat1就是一个对象
		
		//实例化第一只猫
		Cat cat1 = new Cat();
		cat1.name = "小白";
		cat1.age = 3;
		cat1.color = "白色";
		cat1.weight = 25.5;
		
		//老刘解读
		//1：new Cat() 创建一只猫(猫对象),实例化一个猫对象
		//2 Cat cat2 = new Cat() 把创建的猫对象赋给cat2
		//3 cat2就是一个对象
		
		//实例化第二只猫
		Cat cat2 = new Cat();
		cat2.name = "小花";
		cat2.age = 100;
		cat2.color = "花色";
		cat2.weight = 55.5;
		
		//怎么样访问对象的属性
		System.out.println("第一只猫的名字是" + cat1.name + "年龄是" + cat1.age + "颜色是" + cat1.color + "体重是" + cat1.weight);
		System.out.println("第二只猫的名字时" + cat2.name + "年龄是" + cat2.age + "颜色是" + cat2.color + "体重是" + cat2.weight);
		
		//使用面向对象【OOP】解决了什么问题
		//1:语义化的问题，cat1.name 就代表猫1的名字   .age   代表年龄  
		//2:解决了数据类型的问题  (age : int    name :string  weight : double)
		//一句话,数据比较好管理
		
	}
	
}


//使用面向对象解决养猫问题
	//定义一个猫类 Cat -> 自定义数据类型
	
class Cat{
	//属性,成员变量
	String name;//名字
	int age;//年龄
	String color;//颜色
	//因为需求发生了变化，需要增加猫的体重
	double weight;
	
	
	//行为【方法】
	
}
