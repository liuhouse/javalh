package javalh.chapter07;

public class HomeWork04 {
    public static void main(String[] args){
        /*
        * 需求 ： 编写类A03,实现数组的赋值功能,copyArr，输入旧数组,返回一个新数组,元素和旧数组一样
        * */
        A03 a = new A03();
        int[] oldArr = {1,5,6,3};
        //实现数组的复制
        int[] newArr = a.copyArr(oldArr);
        //输出旧数组的数据
        System.out.println("旧数组的值是====================");
        for(int i = 0 ; i < oldArr.length ; i++){
            System.out.print(oldArr[i] + " ");
        }
        System.out.println();

        System.out.println("新数组的值是=====================");
        for (int i = 0 ; i < newArr.length ; i++){
            System.out.print(newArr[i] + " ");
        }
    }
}

class A03{
    public int[] copyArr(int[] oldArr){
        int[] newArr = new int[oldArr.length];
        for(int i = 0 ; i < oldArr.length ; i++){
            newArr[i] = oldArr[i];
        }
        return newArr;
    }
}
