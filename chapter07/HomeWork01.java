package javalh.chapter07;

public class HomeWork01 {
    public static void main(String[] args){
        /*
        * (1):需求
        * 编写类A01,定义方法max,实现求某个double数组的最大值,并返回
        * 500 , 1.1 , 2 , 2.2 ,110, 10.8
        * */
        double[] arr = null;
        A01 a1 = new A01();
        //因为函数的返回类型是Double对象  所以必须使用Double对象接收
        Double max_num = a1.max(arr);
        if(max_num != null){
            System.out.println("数组中的最大值是" + max_num);
        }else{
            System.out.println("传入的参数不能为空{} 也不能为null");
        }

    }
}


class A01{
    //获取数组中的最大值
    //第一步 完成基本需求   第二部  使得程序更加的健壮
    public Double max(double[] arr){
        if(arr != null && arr.length > 0){
            double max_num = arr[0];
            for(int i = 1 ; i < arr.length ; i++){
                if(arr[i] > max_num){
                    max_num = arr[i];
                }
            }
            return max_num;
        }else{
            return null;
        }

    }
}
