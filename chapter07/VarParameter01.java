package chapter07;

public class VarParameter01 {
	public static void main(String[] args) {
		//一个案例 类HspMethod，方法sum 【可以计算2个数的和,3个数的和,4,5.....个数的和】
		HspMethod hsp = new HspMethod();
		System.out.println(hsp.sum(1,2,3,10));
	}
}


class HspMethod{
	//一个案例 类HspMethod，方法sum 【可以计算2个数的和,3个数的和,4,5.....个数的和】
	//可以使用方法重载
//	public int sum(int num1 , int num2) {
//		return num1 + num2;
//	}
	
	//计算3个数的和
//	public int sum(int num1,int num2,int num3) {
//		return num1 + num2 + num3;
//	}
	
	//计算4个数的和
//	public int sum(int num1,int num2,int num3,int num4) {
//		return num1 + num2 + num3 + num4;
//	}
	
	
	//上面三个方法名称相同,功能相同,参数个数不通  -》 使用可变参数进行优化
	//1:int... 表示接受的是可变参数,类型是Int,即可以接收多个int(0-多个)
	//2:使用可变参数的时候,可以当做数组来使用,即nums可以当做数组
	//3:编辑nums求和即可
	public int sum(int... nums) {
		//System.out.println("接收的参数个数=" + nums.length);
		int res = 0;
		for(int i = 0 ; i < nums.length ; i++) {
			res += nums[i];
		}
		return res;
	}
	
	
}
