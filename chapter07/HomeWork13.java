package javalh.chapter07;
import java.lang.Math;

public class HomeWork13 {
    public static void main(String[] args){
        PassObject p = new PassObject();
        //圆的实例化对象
        Circle13 c = new Circle13();
        p.printAreas(c,5);
    }
}

class Circle13{
    //圆的半径
    double radius;
    //定义π
    double PI = Math.PI;

    //返回圆的面积  π * r * r
    public double findArea(){
        return this.PI * this.radius * this.radius;
    }

    //设置半径
    public void setRadius(double radius){
        this.radius = radius;
    }
}


class PassObject{
    //打印圆的面积
    public void printAreas(Circle13 c,int times){
        System.out.println("Radius \t Areas");
        for(int i = 1 ; i <= times ; i++){
            //给半径属性赋值
            //第一种方式  这种方式很简洁
            //c.radius = i * 1.0;
            c.setRadius(i);
            System.out.println(c.radius + " \t\t " + c.findArea());
        }
    }
}
