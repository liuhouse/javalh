package chapter07;

public class OverloadExercise01 {
	public static void main(String[] args) {
		/*
		 * 	1：编写程序,类，Methods中定义三个重载方法并调用,方法名为m，三个方法分别接收一个int参数,两个int参数,一个字符串参数
		 * 	分别执行平方运算并输出结果,相乘并输出结果,输出字符串信息
		 * 	在主类的main()方法中分别用参数区分调用三个方法
		 * */
		Methods methods = new Methods();
		//一个int参数
		methods.m(5);
		//两个int参数
		methods.m(10, 20);
		//字符串参数
		methods.m("坚持到底学java");
		
	}
}


class Methods{
	//下面的三个m方法就组成一个重载 
	//1:当调用的时候,先看参数个数,进而缩短查找范围 2，根据传入实参的数据类型确定是哪个方法匹配,这个方法只能有一个,因为相同参数的方法不能定义
	//一个int参数
	public void m(int n) {
		System.out.println(n + "的平方是" + (n * n));
	}
	
	//两个int参数
	public void m(int n , int m) {
		System.out.println(n + "和" + m + "两个数相乘的结果是" + (n*m));
	}
	
	//一个字符串
	public void m(String str) {
		System.out.println(str);
	}
}
