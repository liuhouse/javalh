package javalh.chapter07;

public class HomeWork02_1 {
    public static void main(String[] args){
        /*
         * 需求：编写类A04,定义方法find(),实现查找某个字符串是否在字符串数组中,并返回索引,如果找不到返回-1
         * */
        A04 a = new A04();
        String[] arr = {"无赖","你的眼睛背叛了你的心","都是你的错","左右为难","太子鸡"};
        String[] arr1 = null;
        System.out.println(a.find("你的眼睛背叛了你的心",arr));
    }
}

/*
* 类名:A04
* 方法:find()
* 参数 字符串,字符串数组
* 返回:int
* */
class A04{
    public int find(String findStr , String[] strS){
        if(strS != null && strS.length > 0){
            for(int i = 0 ; i < strS.length ; i++){
                //如果在这个数组中查找到了
                if(findStr.equals(strS[i])){
                    return i;
                }
            }
        }

        return -1;
    }
}
