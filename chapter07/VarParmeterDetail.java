package chapter07;

public class VarParmeterDetail {
	public static void main(String[] args) {
		/*
		 * 可变参数的注意事项和使用细节
		 * (1)可变参数的实参可以为0个或者任意多个
		 * (2)可变参数的实参可以为数组
		 * (3)可变参数的本质就是数组
		 * (4)可变参数可以和普通类型的参数一起放在形参列表,但是必须保证可变参数在最后
		 * (5)一个形参列表中只能出现一个可变参数
		 * */
		
		TP1 tp1 = new TP1();
		//tp1.f1(1,2);
		//细节：可变参数实参可以为数组,当可变参数的实参为数组的时候,就不能传递别的参数了,数据应该全部写在数组中
		//可变参数 可以直接传递多个整数    两个只能选择一个
		int[] arr = {1,2,3};
		double arr1[] = {1.1,2,3.1};
		tp1.f1(arr);
		tp1.f2("java study",1,2,3);
		
		
	}
}


class TP1{
	public void f1(int... nums) {
		System.out.println("长度=" + nums.length);
	}
	
	
	//细节：可变参数可以和普通的类型的参数一起放在形参列表,但是必须保证形参列表在最后面
	public void f2(String str,double... nums) {
		System.out.print(str);
		double res = 0;
		for(int i = 0 ; i < nums.length; i++) {
			res += nums[i];
		}
		System.out.print(res);
	}
	
	
	//细节,一个形参列表只能出现一个可变参数
	//下面的写法是错误的
//	public void f3(int... nums1,double...ds nums2) {
//		
//	}
	
	
	
	
}