package javalh.chapter07;

public class ThisDetail {
    public static void main(String[] args){
        //this的注意事项和使用细节
        //1:this关键字可以用来访问本类的属性,方法,构造器
        //2:this用于区分当前类的属性和局部变量
        //3:访问成员方法的语法,this.方法名(参数列表)
        //4:访问构造器语法：this(参数列表);注意只能在构造器中使用(即只能在构造器中访问另外一个构造器,必须放在第一条语句)
        //5:this不能再类定义的外部使用,只能在类定义的方法中使用
        PersonD p1 = new PersonD("roose",80);
        p1.f1();
    }
}


class PersonD{
    String name = "jack";
    int age = 18;

    //无参构造器
    PersonD(){
        System.out.println("无参构造器");
    }

    //有参构造器
    public PersonD(String name, int age){
        //调用构造器,必须放在首位
        this();
        this.name = name;
        this.age = age;
    }

    public void f1(){
        System.out.println("================输出属性的两种方式=============================");
        //因为变量的访问是就近原则,如果有局部变量,就访问局部变量,如果没有就会去找成员属性
        String name = "我是局部变量";
        //第一种输出方式
        System.out.println("name = " + name + " age =" + age);
        //第二种输出方式
        //使用this关键字的话,就是直达，准确定位
        System.out.println("name = " + this.name + " age = " + this.age);

        System.out.println("================输出方法的两种方式==============================");
        //第一种方式
        f2();
        //第二种方式
        this.f2();
    }

    public void f2(){
        System.out.println("f2()方法");
    }
}
