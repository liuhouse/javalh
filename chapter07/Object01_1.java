package chapter07;
import java.util.Scanner;
/*
 * 类与对象
 * 使用单独的定义变量解决
 * */
public class Object01_1 {
	public static void main(String[] args) {
		/*
		 * 	看一个养猫猫的问题
		 * 	张老太养了两只猫猫,一只名字叫小白,今年3岁,白色的,还有一只叫小花，今年100岁,花色
		 * 	请编写一个程序,当用户输入小猫的名字时,就显示该猫的名字,年龄,颜色,如果用户输入的小猫名错误,就显示,张老太没有这只猫猫
		 * 
		 * 	使用现有的技术解决
		 * 	单独定义变量解决
		 * 	
		 * 	思路分析
		 * 	(1)定义第一只猫的名字，年龄，颜色
		 * 	(2)定义第二只猫的名字,年龄,颜色
		 * 	(3)使用Scanner保存输入的名字
		 * 	(4)使用控制语句，进行判断
		 * 
		 * 	缺点
		 * 	虽然这样也可以达到需求,但是有缺点
		 * 	1：现在是只有两只猫，定义六个变量，但是如果是10只猫,200只猫,这样的定义方式就很麻烦了，会定义无数的变量,不利于数据的管理
		 * 	2：当维护数据的时候,如果再增加别的属性,比如猫的大小,那么就要给每只猫单独增加属性   不利于数据维护
		 * */
		
		Scanner myScanner = new Scanner(System.in);
		
		
		//定义第一只猫的信息
		String cat1_name = "小白";
		int cat1_age = 3;
		String cat1_color = "白色";
		
		//定义第二只猫的信息
		String cat2_name = "小花";
		int cat2_age = 100;
		String cat2_color = "花色";
		
		System.out.println("请输入猫的名字...");
		//定义输入的名字
		String cat_name = myScanner.next();
		
		if(cat_name.equals(cat1_name)) {
			System.out.println(cat_name + "的年龄是" + cat1_age + "岁" + "毛色是" + cat1_color + "的");
		}else if(cat_name.equals(cat2_name)) {
			System.out.println(cat_name + "的年龄是" + cat2_age + "岁" + "毛色是" + cat2_color + "的");
		}else {
			System.out.println("没有这只猫");
		}
		
		
		
		
		
		
	}
}
