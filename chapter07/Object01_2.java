package chapter07;
import java.util.Scanner;
/*
 * 类与对象
 * 使用数组的方式解决
 * */
public class Object01_2 {
	public static void main(String[] args) {
		/*
		 * 	看一个养猫猫的问题
		 * 	张老太养了两只猫猫,一只名字叫小白,今年3岁,白色的,还有一只叫小花，今年100岁,花色
		 * 	请编写一个程序,当用户输入小猫的名字时,就显示该猫的名字,年龄,颜色,如果用户输入的小猫名错误,就显示,张老太没有这只猫猫
		 * 
		 * 	思路分析
		 * 	(1):使用数组定义第一只猫的信息
		 * 	(2):使用数组定义第二只猫的信息
		 * 	(3):实例化Scanner类,保存输入的信息
		 * 	(4):使用控制语句完成对应的需求
		 * 
		 * 
		 * 	缺点
		 * 	虽然使用数组也可以达到目的,但是也有缺点
		 * 	(1):下标只能是数字,0,1,2  没有语义,很难确定下标代表的是什么,所以开发难度很大,而且如果要给猫猫增加多余的属性,比如重量，那么每一个猫猫都要增加，难度很大
		 * 	(2):数组的数据类型是固定的,本来年龄是整数的,但是在这里只能是字符串,所以不符合规定，很难处理,后期处理需要转化
		 * 
		 * 	很不方便
		 * 	
		 * 	所以，引入了对象来解决这些问题
		 * 
		 * 	主要缺点是不利于数据的管理，效率低下
		 * 
		 * 	
		 * */
		
		Scanner myScanner = new Scanner(System.in);
		
		//定义第一只猫的信息
		String cat1[] = {"小白","3","白色的"};
		//定义第二只猫的信息
		String[] cat2 = {"小花","100","花色的"};
		
		System.out.println("请输入猫的名字查看猫的信息...");
		
		String cat_name = myScanner.next();
		
		if(cat_name.equals(cat1[0])) {
			System.out.println(cat_name + "的年龄是" + cat1[1] + "毛色是" + cat1[2]);
		}else if(cat_name.equals(cat2[0])) {
			System.out.println(cat_name + "的年龄是" + cat2[1] + "毛色是" + cat2[2]);
		}else {
			System.out.println("没有找到" + cat_name + "这个猫");
		}
		
	}
}
