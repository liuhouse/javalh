package chapter07;

public class ObjectInfo {
	public static void main(String[] args) {
		/**
		 * 类和对象的内存分配机制
		 * (1)栈：一般存放基本数据类型(局部变量)
		 * (2)堆:存放对象(Cat cat,数组等)
		 * (3)方法区：常量池(常量,比如字符串)，类的加载信息
		 * 
		 * Java创建对象的流程简单分析
		 * */
		
		PersonInfo p = new PersonInfo();
		p.name = "Jack";
		p.age = 10;
		
		
		/*
		 * (1)先加载PersonInfo类的信息(属性和方法信息,只会加载一次)
		 * (2)在堆中分配空间,进行默认初始化(看规则)
		 * (3)把地址赋给p,p就指向这个对象
		 * (4)进行指定初始化,比如p.name="jack" p.age=10
		 * */
		
	}
}


class PersonInfo{
	//定义名字
	String name;
	//定义年龄
	int age;
}
