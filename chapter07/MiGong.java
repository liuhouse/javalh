package chapter07;
/*
 * 老鼠出迷宫
 * */
public class MiGong {
	public static void main(String[] args) {
		//思路
		//i表示行(8行)   j表示列(7列)
		//化繁为简,先死后活
		//1：先创建迷宫地图,使用二维数组表示 int[][] map = new int[8][7];
		//2:先规定map数组的元素值:0 表示可以走 1 表示障碍物
		
		
		//创建迷宫地图
		int[][] map = new int[8][7];
		
		//设置障碍物
		//(1)设置第一行全部都是障碍物（即第一行元素全部设置为1）,设置最后一行全部都是障碍物(即最后一行的全部元素设置为1)
		for(int j = 0 ; j < 7 ; j++) {
			map[0][j] = 1;
			map[7][j] = 1;
		}
		
		//(2)设置第一列的全部都是障碍物(即第一列的元素全部设置为1),设置最后一列全部都是障碍物(即最后一列的元素全部设置为1)
		for(int i = 0 ; i < 8 ; i++) {
			map[i][0] = 1;
			map[i][6] = 1;
		}
		map[3][1] = 1;
		map[3][2] = 1;
		
		map[2][2] = 1;//测试回溯
		
//		map[1][2] = 1;
//		map[2][1] = 1;
//		map[2][2] = 1;
		
		//输出当前地图
		System.out.println("======当前地图的情况======");
		for(int i = 0 ; i < map.length ; i++) {
			for(int j = 0 ; j < map[i].length ; j++) {
				System.out.print(map[i][j] + " ");
			}
			System.out.println();
		}
		
		
		
		//使用findWay给老鼠找出路
		TIM t1 = new TIM();
		//下右上左
		//t1.findWay(map, 1, 1);
		
		//上右下左
		t1.findWay2(map, 1, 1);
		
		System.out.println("\n=======找路的情况如下=====");
		
		for(int i = 0 ; i < map.length ; i++) {
			for(int j = 0 ; j < map[i].length ; j++) {
				System.out.print(map[i][j] + " ");
			}
			System.out.println();
		}
		
		System.out.println("第二条走路的步数是" + t1.way_two_count);
		
	}
}


//声明一个老鼠找迷宫的行为类
class TIM{
	
	//第一条走路的步数
	int way_one_count = 0;
	
	//第二条走路的步数
	int way_two_count = 0;
	
	
	
	//使用递归回溯的思想来解决老鼠出迷宫
	
	//老韩解读
	//1:findWay 方法就是专门来找出迷宫的路径
	//2:如果找到,就返回true，否则就返回false
	//3:map就是二维数组,表示迷宫
	//4:i,j就是老鼠的位置,初始化的位置就是(1,1)
	//5:因为我们是递归的找路,所以我们先规定map数组的各个值得含义
		// 0：表示可以走   1：表示障碍物 2：表示可以走,已经走过了 3：表示走过了，但是路走不通
	//6:当map[6][5] = 2 就说明找到了通路,就可以结束,否则就继续找
	//7:先确定老鼠找路的策略   下->右->上->左
	
	/*
	 * 	map 表示当前的地图 
	 * 	i 表示x轴的位置
	 * 	j 表示y轴的位置
	 * 	递归关键  先定好递归结束的条件
	 * 	内部找到符合递归的条件
	 * 	一个都不符合的时候退出递归
	 * */
	public boolean findWay(int[][] map,int i,int j) {
		way_one_count++;
		if(map[6][5] == 2) {//说明已经找到了
			//递归结束的条件
			return true;
		}else {
			if(map[i][j] == 0) {//当前这个位置是0，说明是可以走的
				//我们假设可以走通,但是不一定能走的通
				map[i][j] = 2;
				//使用找路策略,来确定该位置是否真的可以走通
				//下->右->上->左
				if(findWay(map,i+1,j)) {//下
					return true;
				}else if(findWay(map,i,j+1)) {//右
					return true;
				}else if(findWay(map,i-1,j)) {//上
					return true;
				}else if(findWay(map,i,j-1)) {//左
					return true;
				}else {
					map[i][j] = 3;
					return false;
				}
			}else {//map[i][j] = 1,2,3  证明已经无路可走
				return false;
			}
		}
	}
	
	
	/*
	 * 	map 表示当前的地图 
	 * 	i 表示x轴的位置
	 * 	j 表示y轴的位置
	 * 	递归关键  先定好递归结束的条件
	 * 	内部找到符合递归的条件
	 * 	一个都不符合的时候退出递归
	 * 	上 右 下 左
	 * */
	public boolean findWay2(int[][] map,int i,int j) {
		way_two_count++;
		if(map[6][5] == 2) {//说明已经找到了
			//递归结束的条件
			return true;
		}else {
			if(map[i][j] == 0) {//当前这个位置是0，说明是可以走的
				//我们假设可以走通,但是不一定能走的通
				map[i][j] = 2;
				//使用找路策略,来确定该位置是否真的可以走通
				//下->右->上->左
				if(findWay2(map,i-1,j)) {//上
					return true;
				}else if(findWay2(map,i,j+1)) {//右
					return true;
				}else if(findWay2(map,i+1,j)) {//下
					return true;
				}else if(findWay2(map,i,j-1)) {//左
					return true;
				}else {
					map[i][j] = 3;
					return false;
				}
			}else {//map[i][j] = 1,2,3  证明已经无路可走
				return false;
			}
		}
	}
	
	
	
	
}


