package javalh.chapter07;

public class HomeWork07 {
    public static void main(String[] args){
        Dog7 d1 = new Dog7("小黄","黄色",18);
        d1.show();
    }
}



class Dog7{
    String name;
    String color;
    int age;

    //构造方法
    public Dog7(String name , String color , int age){
        this.name = name;
        this.color = color;
        this.age = age;
    }

    public void show(){
        System.out.println("这只狗的信息如下:名字:" + this.name +"颜色:" + this.color + "年龄：" + this.age);
    }
}
