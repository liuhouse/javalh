package javalh.chapter08;

import javalh.chapter08.com.xbc.smalldish.model.Order;
import javalh.chapter08.xiaoming.Dog;

public class Test {
    public static void main(String[] args) {
        //小明类下面的dog
        Dog xiaoming_dog = new Dog();
        xiaoming_dog.speak();
        //小强类下面的dog
        javalh.chapter08.xiaoqiang.Dog dog = new javalh.chapter08.xiaoqiang.Dog();
        dog.speak();

        //包名 com.xiaobangcai.smalldish.order

        //获取订单列表
        Order order = new Order();
        order.getOrderList();


    }
}
