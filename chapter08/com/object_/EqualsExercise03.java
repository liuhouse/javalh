package javalh.chapter08.com.object_;

public class EqualsExercise03 {
    public static void main(String[] args) {
        Person_ p1 = new Person_();
        p1.name = "hsp";
        Person_ p2 = new Person_();
        p2.name = "hsp";
        System.out.println(p1 == p2);//false [两个独立的对象,地址是不一样的]
        System.out.println(p1.name.equals(p2.name));//true [判断两个字符串是不是相等,已经重写了equals方法]
        System.out.println(p1.equals(p2));//false [这里的对象并没有对equals方法进行重写]

        String s1 = new String("asdf");
        String s2 = new String("asdf");
        System.out.println(s1.equals(s2));//true 字符串重写了equals方法
        System.out.println(s1 == s2);//false  这是两个对象  地址不一样
    }
}

class Person_{
    public String name;
}