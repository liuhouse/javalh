package javalh.chapter08.com.object_;


public class ToString_ {
    public static void main(String[] args) {
        /*
            Object的toString()源码
            (1)getClass().getName() 类的全包名(包名+类名)
            (2)Integer.toHexString(hashCode())将对象的hashCode值转成16进制字符串
            =====================================
            public String toString() {
                return getClass().getName() + "@" + Integer.toHexString(hashCode());
            }
        * */

        Monster monster = new Monster("小钻风", "巡山的", 1000);
        //这边默认调用的是Object里面的toString方法
        //javalh.chapter08.com.object_.Monster@119d7047
        System.out.println(monster.toString() + "hashCode = " + monster.hashCode());

        System.out.println("===当直接输出一个对象的时候,toString方法会被默认的调用===");
        System.out.println(monster);//等价于 monster.toString()

    }
}

//妖怪类
class Monster{
    private String name;
    private String job;
    private double sal;

    public Monster(String name , String job , double sal){
        this.name = name;
        this.job = job;
        this.sal = sal;
    }



    //重写toString方法
    //使用快捷键  alt + insert -> toString()
    @Override
    //重写后,一般是把对象的属性值输出,当然程序员也是可以自己进行定制的
    public String toString() {
        return "Monster{" +
                "name='" + name + '\'' +
                ", job='" + job + '\'' +
                ", sal=" + sal +
                '}';
    }
}
