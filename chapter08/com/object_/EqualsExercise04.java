package javalh.chapter08.com.object_;

public class EqualsExercise04 {
    public static void main(String[] args) {
        int it = 65;
        float f1 = 65.0f;
        System.out.println("65和65.0f是否相等?" + (it == f1));//true

        char ch1 = 'A';
        char ch2 = 12;
        System.out.println("65和A是否相等?" + (it == ch1));//true 字符的本质就是int  A = 65
        System.out.println("12和ch2是否相等" + (12 == ch2));//true char的本质就是整数  所以 12 == 12

        String str1 = new String("hello");
        String str2 = new String("hello");
        System.out.println("str1和str2是否相等" + (str1 == str2));//false

        System.out.println("str1是否equals str2?" + (str1.equals(str2)));//true
        //System.out.println("hello" == new java.sql.Date());//编译错误


        //射弩设
    }
}
