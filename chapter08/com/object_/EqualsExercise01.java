package javalh.chapter08.com.object_;

public class EqualsExercise01 {
    public static void main(String[] args) {
        /*
        * 应用实例：
        * 判断两个Person对象的内容是否相等，如果两个Person对象的各个属性值都一样,则返回true,否则返回false
        * */
        Person person1 = new Person("李光地", 50, '男');
        Person person2 = new Person("李光地",50,'男');
        System.out.println(person1.equals(person2));
    }


}

//判断两个Person对象的内容是否相等
//如果两个Person对象的各个属性值都一样,则返回true，否则返回false
class Person{//默认是继承Objects  extends Objects
    //姓名
    private String name;
    //年龄
    private int age;
    //性别
    private char gender;


    public Person(String name, int age, char gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    //重写Object里面的equals方法
    public boolean equals(Object obj){
        //判断如果比较的两个对象是同一个对象,则返回true
        //这里判断的是两个对象的地址 如果地址相等的话  就是同一个对象
        if(this == obj){
            return true;
        }
        //进行类型判断
        //如果传递过来的对象是属于Person对象才进行比较
        if(obj instanceof Person){
            //目前这个obj对象的编译类型是Object
            //进行向下转型,因为我需要得到obj的各个属性
            Person p = (Person) obj;
            //如果当前对象 和 传递过来的对象的  姓名 年龄  性别 都一样 ， 就证明对象的内容相等
            return this.name.equals(p.name) && this.age == p.age && this.gender == p.gender;
        }
        return false;
    }
}
