package javalh.chapter08.com.object_;

public class EqualsExercise02 {
    //重写equlas方法,判断两个Dog对象的是否相等
    //如果两个对象中的各个属性值都相同的话,就代表这两个对象相等
    public static void main(String[] args) {
        Dog dog1 = new Dog("小黄", 3, "黄色的");
        Dog dog2 = new Dog("小黄", 3, "黄色的");
        System.out.println(dog1.equals(dog2));
    }
}
class Dog{
    private String name;
    private int age;
    private String color;

    public Dog(String name , int age , String color){
        this.name = name;
        this.age = age;
        this.color = color;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAge(int age){
        this.age = age;
    }

    public void setColor(String color){
        this.color = color;
    }

    public String getName(){
        return this.name;
    }

    public int getAge(){
        return this.age;
    }

    public String getColor(){
        return this.color;
    }

    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj instanceof Dog){
            Dog dog = (Dog)obj;
            return this.name.equals(dog.name) && this.age == dog.age && this.color.equals(dog.color);
        }
        return false;
    }

}
