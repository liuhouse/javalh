package javalh.chapter08.com.object_;

public class HashCode_ {
    public static void main(String[] args) {

        /*
        * 老韩的6个小结
        * (1)提高具有哈希结构的容器的效率
        * (2)两个引用,如果指向的是同一个对象,则哈希值肯定是一样的
        * (3)两个引用,如果指向的是不同的对象,则哈希值是不一样的
        * (4)哈希值主要根据地址号来的！不能完全将哈希值等价于地址
        * (5)
        * */

        AA aa = new AA();
        AA aa2 = new AA();
        AA aa3 = aa;
        
        //不同对象的HashCode的值是不一样的
        //相同对象 也就是指向同一个地址 的 hashCode值是一样的
        
        System.out.println("aa.hashCode() = " + aa.hashCode());
        System.out.println("aa2.hashCode() = " + aa2.hashCode());
        System.out.println("aa3.hashCode() = " + aa3.hashCode());

    }
}

class AA{}