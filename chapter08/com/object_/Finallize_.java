package javalh.chapter08.com.object_;

//演示Finalize的用法
public class Finallize_ {
    public static void main(String[] args) {

        /*
        * (1)当对象被回收的时候,系统会自动的调用该对象的finalize方法,子类可以重写该方法,做一些释放资源的操作
        * (2)什么时候被回收,当某个对象没有被引用的时候,则jvm就认为这个对象是一个垃圾对象,就会使用垃圾回收机制来销毁对象,在销毁对象前,会先调用finalize方法
        * (3)垃圾回收机制的调用,是由系统来决定的额(即有自己的GC算法),也可以通过System.gc()主动触发垃圾回收机制
        * 注意一点
        * 在我们的实际开发中,几乎不会运用finlize方法,所以更多的时候就会为了应付面试
        * */




        Car bmw = new Car("宝马");
        //当执行下面的操作的时候   其实bmw其实在系统中已经成为垃圾了


        /*
        * 这个时候的car对象就是一个垃圾,垃圾回收器就会回收(销毁)对象,在销毁对象前,会调用该对象的finalsize方法
        * 程序员就可以在finalize中,写自己的业务逻辑代码(比如说释放资源：数据库链接,或者是打开文件)
        * 如果程序员不重写finalsize,那么就默认调用Object类的finalsize方法   就是默认处理
        *
        * */


        bmw = null;
        //不会自动调用垃圾回收机制  因为垃圾回收机制有自己的算法

        //主动调用垃圾回收机制  但是不是说调用了立马就会执行   他还是有自己的算法的   在自己的空闲下来的时候才会去调用
        //比如说一般情况下 下午六点下班的时候才需要保洁阿姨去打扫卫生   但是因为现在是下午五点  办公室实在是太脏了
        //所以我现在立马给保洁阿姨打电话让过来    大概率保洁阿姨是闲着的   所以就会直接过来   但是此时保洁阿姨可能
        //正在做一些自己的工作  所以不会马上过来   当保洁阿姨  完成自己的工作之后   就会过来对我们办公室的垃圾进行处理
        System.gc();

        System.out.println("程序退出了");
    }
}


class Car{
    private String name;
    //属性  资源

    public Car(String name){
        this.name = name;
    }

    //重写finalize

    @Override
    protected void finalize() throws Throwable {
        //默认的finalize方法啥都没干，需要程序员自己去指定要做哪些事情
        System.out.println("我们销毁了汽车" + name);
        System.out.println("释放了某些 资源");
    }
}