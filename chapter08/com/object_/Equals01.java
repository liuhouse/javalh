package javalh.chapter08.com.object_;

public class Equals01 {
    public static void main(String[] args) {
        /*
        *  == 是一个比较运算符
        * 1 ： == 既可以判断基本类型,又可以判断引用类型
        * 2 :  == 如果判断的是基本类型,判断的是值是否相等  实例  int i = 10 , double b = 10.0
        * 3 :  == 如果判断引用类型,判断地址是否相等,即判断是不是同一个对象
        * */

        //基本数据类型,判断值是否相等
        int i = 10;
        double j = 10.0;
        System.out.println(i == j);//true

        //实例化一个对象A
        //三个变量指向同一个地址
        A a = new A();
        A b = a;
        A c = b;

        System.out.println(a == c);//true
        System.out.println(b == c);//true

        //编译类型是B   但是  运行类型是a
        //所以最终的地址还是 new A()的地址
        B bObj = a;
        System.out.println(bObj == c);//true

//        "hello".equals("abc");

        //new Integer(10);

        String hsp = new String("hsp");
        String hsp1 = new String("hsp");

        //两个字符串new 了两个对象   所有地址不相等
        System.out.println(hsp == hsp1);//false

        System.out.println(hsp.equals(hsp1));//true

        System.out.println("hello".equals("abc"));//false


        //看看Object类的equals
        /*
        * 即Object的equals方法默认就是比较对象的地址是否相同
        * 也就是判断两个对象是不是同一个对象
        * */

        //new Object();
        /*
         public boolean equals(Object obj) {
            if (obj instanceof Integer) {
                return value == ((Integer)obj).intValue();
            }
            return false;
         }
         f t f t f
         t t t f t 编译报错
        * */

        //整形的源码判断
        Integer integer1 = new Integer(1000);
        Integer integer = new Integer(1000);
        System.out.println(integer == integer1);//false
        System.out.println(integer.equals(integer1));


    }
}


class B{}
class A extends B{}
