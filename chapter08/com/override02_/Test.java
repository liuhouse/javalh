package javalh.chapter08.com.override02_;

public class Test {
    public static void main(String[] args) {
        System.out.println("Person类的信息如下...");
        Person jack = new Person("jack", 100);
        System.out.println(jack.say());

        System.out.println("Student类的信息如下....");
        Student smith = new Student("smith", 100, 1, 99.9);
        System.out.println(smith.say());
    }
}
