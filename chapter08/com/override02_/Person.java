package javalh.chapter08.com.override02_;

public class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //返回一个自我介绍的方法
    public String say(){
        return "name : " + name + "age : " + age;
    }
}
