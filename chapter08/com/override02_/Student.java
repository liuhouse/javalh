package javalh.chapter08.com.override02_;

public class Student extends Person{
    //student类的特有属性
    private int id;
    private double score;

    public Student(String name, int age, int id, double score) {
        super(name, age);
        this.id = id;
        this.score = score;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public double getScore() {
        return score;
    }


    //重写父类的say方法
    public String say(){
        //可以调用父类的方法  进行  属性的读取
        return super.say() + "id : " + id + "score : " + score;
    }


}
