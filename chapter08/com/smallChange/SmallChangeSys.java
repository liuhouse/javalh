package javalh.chapter08.com.smallChange;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/*
* 零钱通项目
* */
public class SmallChangeSys {
    public static void main(String[] args) {

        /*
        * 拿到一个需求的时候应该怎么做？
        * 核心思想  化繁为简  - 先死后活
        * 先把需要输出的输出来   将复杂的需求  分成很多的小的需求   一个一个的去实现   多写多练即可
        * */

        /*
        * 需求 使用 java开发零钱通项目,可以完成收益入账,消费,查看明细,退出系统等功能
        * 化繁为简  先死后活
        * (1)先完成显示菜单,并且可以选择菜单,给出对应的提示
        * (2)完成零钱通明细
        * (3)完成入账
        * (4)完成消费
        * (5)完成退出
        * (6)用户输入4退出的时候,给出提示:"你确定要退出吗?y/n,必须输入正确的y/n，否则循环输入指令,直到输入y/n"
        * (7)在收益入账和消费的时候，判断金额是否合理,并给出相应的提示
        * */

        //定义相关的变量
        //是否继续使用项目
        boolean loop = true;
        //因为这里要进行项目类型的选择,所以要进行输入,所以要使用Scanner
        Scanner scanner = new Scanner(System.in);

        //保存输入的值
        String key;

        //(2)零钱通明细
        //老韩思路 (1)可以把收益入账和消费,保存到数组 (2)可以使用对象 (3)简单的话可以使用String拼接
        String details = "----------------零钱通明细--------------------";

        //3.完成收益入账，不可能一次直接就把变量定义好,完成具体的功能驱动程序员增加新的变化和代码
        //老韩思路,定义新的变量

        //收益入账
        double money = 0;
        //余额
        double balance = 0;
        //入账时间  date 是 java.util.Date 类型, 表示日期
        Date date = null;
        //对日期进行格式化
        SimpleDateFormat simp = new SimpleDateFormat("yyyy-MM-dd HH:mm");


        //4.消费
        //定义新变量,保存消费的原因
        String note = "";

        //这里至少要走一次
        do{
            System.out.println("\n----------------零钱通菜单-----------------");
            System.out.println("\t\t\t 1 零钱通明细");
            System.out.println("\t\t\t 2 收益入账");
            System.out.println("\t\t\t 3 消费");
            System.out.println("\t\t\t 4 退  出");

            System.out.print("请选择(1-4)");
            key = scanner.next();

            switch (key){
                case "1":
                    System.out.println(details);
                    break;
                case "2":
                    System.out.println("收益入账金额:");
                    //接收入账金额
                    money = scanner.nextDouble();
                    //这里需要一个范围的检查======>
                    if(money <= 0){
                        System.out.println("收益的入账金额需要大于0");
                        break;
                    }
                    //计算接收金额之后的余额
                    balance += money;
                    //计算现在入账的时间
                    date = new Date();
                    //进行收益详情的字符串拼接
                    details += "\n收益入账\t +" + money + "\t" + simp.format(date) + "\t余额" + balance;
                    break;
                case "3":
                    System.out.print("消费金额:");
                    money = scanner.nextDouble();
                    //这里需要进行检验
                    if(money <= 0 || money > balance){
                        System.out.println("你的消费金额应该在0-" + balance);
                        break;
                    }
                    System.out.println("消费说明:");
                    //编写消费说明
                    note = scanner.next();
                    //计算余额
                    balance -= money;
                    //编写时间
                    date = new Date();//获取当前日期
                    //拼接消费详情信息
                    details += "\n" + note + "\t-" + money + "\t" + simp.format(date) + "\t余额" + balance;
                    break;
                case "4":
                    //用户输入4退出的时候,给出提示"你确定要退出吗?y/n,必须输入正确的y/n"
                    //否则循环输出指令,知道输入y或者n
                    //老韩思路分析
                    //(1)先定一个一个choose，用来接收用户的输入
                    //(2)使用while + break,来处理接收到的输入是y或者n
                    //(3)退出while循环之后,再判断choice是y还是n，就可以决定是否退出
                    //(4)建议一段代码,完成一个小功能,尽量不要混在一起
                    String choice = "";
                    //无限循环
                    while (true){
                        System.out.println("确定要退出吗?y/n");
                        //这里需要注意的是  判断两个字符串是否相等的时候 使用  equals
                        choice = scanner.next();
                        if(choice.equals("y") || choice.equals("n")){
                            break;
                        }
                    }
                    if(choice.equals("y")){
                        loop = false;
                    }
                    break;
                default:
                    System.out.println("您的输入有误，请输入(1-4)");
            }
        }while (loop == true);

        System.out.println("------------已经退出了零钱通项目---------------");

    }
}
