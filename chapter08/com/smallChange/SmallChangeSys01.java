package javalh.chapter08.com.smallChange;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class SmallChangeSys01 {
    public static void main(String[] args) {
        /*
        * 需求  使用java开发零钱通项目,可以完成收益入账,消费,查看明细,退出系统功能
        * 化繁为简   先死后活
        * (1)先完成菜单的显示并且可以选择  完成
        * (2)完成零钱通明细 -- 整个页面就完成了  完成
        * ==========完成功能【填充数据】===========
        * 因为这里有四个功能   是根据不同的按键做不同的事情   所以 这里使用switch分支语句来进处理
        * (3)完成收益入账 ， 这里现在使用字符串拼接的方式 完成
        * (4)完成消费    这里现在使用字符串拼接的方式  完成
        * (5)完成退出
        *       在完成退出的时候 ,进行提示  【是否要退出系统y/n】只有符合条件再判断是不是退出系统
        *
        * */

        //因为这里的消费和入账是一直都会进行的动作 ,因为一直都会有入账和消费  ,并不是一次消费过去了就过去了.而且,至少会有一次动作


        //系统是否在线   如果loop == true的话,就代表系统持续在线,就可以进行系统的入账消费和退出查看明细功能
        boolean loop = true;

        //定义一个输出的值
        String key = "";

        //定义零钱通初始化明细
        String details = "---------------------零钱通明细-------------------------";

        //开始定义需要的变量(就和数据库中定义字段一样)
        //收益入账--所需字段
        //金额
        double money = 0;
        //时间
        Date date = null;
        //需要一个格式化时间的方法,因此默认的而时间不是我们想要的
        SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //余额
        double balance = 0;

        //消费出账定义字段

        //消费说明
        String info = "";

        Scanner scanner = new Scanner(System.in);
        do {
            //打印菜单
            System.out.println("\n-------------零钱通菜单---------------");
            System.out.println("\t\t 1：零钱通明细");
            System.out.println("\t\t 2：收益入账");
            System.out.println("\t\t 3：消费出账");
            System.out.println("\t\t 4：退出系统");
            //打印选择
            System.out.print("请选择一个功能(1-4)：");
            key = scanner.next();

            switch (key){
                case "1":
                    //打印出零钱通明细
                    System.out.println(details);
                    break;
                case "2":
                    //定义入账金额所需的字段
                    System.out.print("请输入入账金额:");
                    money = scanner.nextDouble();
                    //这里使用过关斩将的方式  先判断错的,如果发现错误,就不需要再往下执行了
                    //如果钱数小于或者等于0,就代表错误,让重新输入
                    if(money <= 0){
                        break;
                    }
                    //计算入账时间
                    date = new Date();
                    //计算余额
                    balance += money;
                    //进行字符串的拼接
                    details += "\n收益入账\t +" + money + "\t" + simple.format(date) + "\t余额：" + balance ;
                    break;
                case "3":
                    System.out.print("请输入消费金额：");
                    money = scanner.nextDouble();
                    //这里要对消费的钱数进行判断,如果钱数小于等于0大于现在的余额的话,就代表是错误的,就退出重新输入
                    if(money <= 0 || money > balance){
                        System.out.println("您输入的消费金额不合理---");
                        break;
                    }
                    //消费说明
                    System.out.print("请输入消费说明：");
                    info = scanner.next();
                    //计算消费时间
                    date = new Date();
                    //计算余额
                    balance -= money;
                    //进行字符串的拼接
                    details += "\n"+info+"\t-" + money + "\t" + simple.format(date) + "\t余额：" + balance ;
                    break;
                case "4":
                    //这里的逻辑是,判断用户输入的是不是y/n，如果不是,那么就一直让选择
                    String breakSystem = "";
                    while(true){
                        System.out.println("您确定要退出零钱通系统吗？【y：是 / n：否】");
                        breakSystem = scanner.next();
                        //这里就得到了用户的输入
                        if("y".equals(breakSystem) || "n".equals(breakSystem)){
                            break;
                        }
                    }
                    //输入了y就代表确实是要退出系统了
                    if("y".equals(breakSystem)){
                        loop = false;//核心思想是,如果loop == false的话,就要退出系统
                    }
                    break;
                default:
                    System.out.println("您的输入有误，必须在(1-4)之间");
            }
        }while(loop == true);

        System.out.println("您已经退出了零钱通系统的登录---");

    }
}
