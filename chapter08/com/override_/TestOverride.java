package javalh.chapter08.com.override_;

public class TestOverride {
    public static void main(String[] args) {
        Dog dog = new Dog();
        //会标准调用,此时调用的是父类中的cry方法
        //dog.cry();

        //如果传递一个参数,那么久调用的是带参数的,如果本类中有,就调用,否则就去父类调用
        dog.cry("阿黄");
    }
}
