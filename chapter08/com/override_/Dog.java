package javalh.chapter08.com.override_;

public class Dog extends Animal{//Dog属于Animal的子类

    /*
    * 注意事项和使用细节
    * 方法重写也叫做方法覆盖，需要满足下面的条件
    * 1:子类的方法的形参列表,方法名称,要和父类方法的形参列表,方法名称完全一样
    * 2：子类方法的返回类型要和父类方法的返回类型要一致,或者是父类返回类型的子类
    * 3：子类方法不能缩小父类方法的访问权限 演示   public -> portected ->默认  -> private
    * */


    //重写父类的cry方法
    //老韩解读
    //1.因为Dog是Animal的子类
    //2.Dog的cry方法和Animal父类定义的cry方法形式一样(名称,返回类型,参数)
    //3.这个时候我们就说Dog的cry方法重写了父类Animal的cry方法
    public void cry(String name){
        System.out.println("小狗汪汪叫....." + name);
    }

    //父类有一个Object类，子类中的Dog也有一个相同的名字，相同的返回类型 ,相同的参数
    //所以也就是子类的m1对父类的m1方法进行了重写

    /*
    * 细节：子类方法的返回类型和父类方法的返回类型一样 【构成重写】
    * 或者返回类型是父类返回类型的子类
    * 比如 父类 返回类型是Object   那么子类的返回类型是String 就构成重写  或者是int也构成重写
    * */
    public String m1(){
        return null;
    }

    //父类是String    子类是String 这是正确的
    //父类是String  子类是Object 这是错误的   因为Object不是String的子类
//    public Object m2(){
//        return null;
//    }

    // AAA 返回类型相同  名字相同  参数相同   所以重写是对的
    //返回BBB也是对的  因为返回的是AAA的子类
    public BBB m3(){
        return null;
    }


    //重写父类的eat的方法
    //细节 ： 子类方法不能缩小父类方法的访问权限
    //总结一点：子类方法在重写父类方法的时候  不能降低父类的访问权限
    //权限大小    private -> 默认 -> protected -> public
     void eat(){

    }



}



class AAA{

}
class BBB extends AAA{

}
