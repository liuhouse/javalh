package javalh.chapter08.com.override_;

public class Animal {
    /*
     * 方法重写，方法覆盖
     * 简单的说：方法覆盖(重写)就是子类有一个方法,和父类的某个方法的名称,返回类型
     * 参数一样,那么我们就说子类的这个方法覆盖了父类的方法
     * */

    public void cry() {
        System.out.println("动物叫唤...");
    }


    public Object m1() {
        return null;
    }




    public String m2() {
        return null;
    }

    public AAA m3() {
        return null;
    }


    private void eat() {

    }

}
