package javalh.chapter08.com.extend_;

public class Extends01 {
    public static void main(String[] args) {
        Pupil pupil = new Pupil();
        pupil.name = "银角大王";
        pupil.age = 15;
        pupil.setScore(20);
        pupil.testing();
        System.out.println(pupil.name + "的信息如下============");
        System.out.println(pupil.getUserInfo());

        System.out.println();
        Graduate graduate = new Graduate();
        graduate.name = "金角大王";
        graduate.age = 150;
        graduate.setScore(145);
        graduate.testing();
        System.out.println(graduate.name + "的信息如下===========");
        System.out.println(graduate.getUserInfo());
    }
}
