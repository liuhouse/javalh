package javalh.chapter08.com.extend_.computer01;

public class PC extends Computer{
    //默认继承会报错  是因为父类中的无参构造器已经被覆盖了
    //特有属性  brand  品牌
    private String brand;

    //构造方法   第一句话  实现了  父类的构造方法的实例化   后面的特有的方法   自己来进行赋值
    //有参数的构造方法
    public PC(String CPU, int ORM, int disk, String brand) {
        super(CPU, ORM, disk);
        //特有的属性特殊赋值
        this.brand = brand;
    }

    //特殊的方法需要特殊来写
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    //获取PC的电脑的信息
    public String getPCDetail(){
        return getDetails() + getBrand();
    }
}
