package javalh.chapter08.com.extend_.computer01;

public class Computer {
    //CPU信息
    private String CPU;
    //内存信息
    private int ORM;
    //硬盘信息
    private int disk;

    public Computer(String CPU, int ORM, int disk) {
        this.CPU = CPU;
        this.ORM = ORM;
        this.disk = disk;
    }

    public void setCPU(String CPU) {
        this.CPU = CPU;
    }

    public void setORM(int ORM) {
        this.ORM = ORM;
    }

    public void setDisk(int disk) {
        this.disk = disk;
    }

    public String getCPU() {
        return CPU;
    }

    public int getORM() {
        return ORM;
    }

    public int getDisk() {
        return disk;
    }

    //获取电脑的详细信息
    public String getDetails(){
        return "cpu : " + CPU + "orm : " + ORM + "disk : " + disk;
    }
}
