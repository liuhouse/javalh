package javalh.chapter08.com.extend_.computer01;

public class NetBook extends Computer{
    private String color;
    public NetBook(String CPU , int ORM , int disk, String color){
        super(CPU,ORM,disk);
        //单独属性单独赋值
        this.color = color;
    }

    //单独的属性有单独的设置方法
    public void setColor(String color){
        this.color = color;
    }

    //单独的属性有单独的获取方法
    public String getColor(){
        return this.color;
    }

    //获取笔记本电脑的详细信息
    public String getNetBookDetail(){
        return getDetails() + getColor();
    }

}
