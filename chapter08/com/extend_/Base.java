package javalh.chapter08.com.extend_;

//父类 也称作基类
public class Base extends TopBase{
    //四个属性
    public int n1 = 100;
    protected int n2 = 200;
    int n3 = 300;
    private int n4 = 400;

    public int getN4(){
        return this.n4;
    }

    //无参构造器
    public Base(){
        System.out.println("父类Base()构造器被调用...");
    }

    //有参构造器
    public Base(String name){
        super();
        System.out.println("父类的Base(String name)构造器被调用...");
    }

    //有参构造器 两个参数
    public Base(String name , int age){
        System.out.println("父类的Base(String name , int age)构造器被调用...");
    }

    public void test100(){
        System.out.println("test100");
    }

    protected void test200(){
        System.out.println("test200");
    }

    void test300(){
        System.out.println("test300");
    }

    private void test400(){
        System.out.println("test400");
    }

    public void callTest400(){
        this.test400();
    }


}
