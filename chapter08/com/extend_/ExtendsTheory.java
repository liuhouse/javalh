package javalh.chapter08.com.extend_;
/*
* 继承的本质分析(非常重要),不仅java中有继承，php也有,js也有   所有的原理都是一样的
* */
/*
 * 案例
 * 我们看一个案例来分析当子类继承父类,创建子类对象的时候,内存中到底发生了什么事情,老韩提示
 * 当子类对象创建好之后,建立查找关系
 * */

/*
 * 讲解继承的本质
 * */
public class ExtendsTheory {
    public static void main(String[] args) {
        Son son = new Son();//内存的布局
        //这个时候请大家注意，要按照查找关系来返回信息
        /*
        * (1):首先查看子类是否有该属性
        * (2):如果子类有这个属性,并且是可以访问的,则返回这个信息
        * (3):如果子类没有这个属性,就看父类中有没有这个属性(如果父类中有这个属性,
        *     并且是可以访问的,就返回信息)
        * (4):如果父类中没有就按照规则(3)，继续查找上级的父类,直到Object
        * */
        System.out.println(son.name);//大头儿子
        //因为此时的age属性是private的，所以不能访问
        //只要在子类中或者父类中任意一级找到了,那么就不会向上进行访问了,不管是什么修饰符,都视为已经找到了,不管能不能用
        //System.out.println(son.age);//39
        //但是可以在此类中定义公共的方法来进行私有属性的访问
        System.out.println(son.getAge());
        System.out.println(son.hobby);//旅游

    }
}
//下面三个类的关系  Son(儿子类) extends  Father(父亲类)
// extends GrandPa(爷爷类)

class GrandPa{//爷爷类
    String name = "大头爷爷";
    String hobby = "旅游";
    int age = 99;
}

//a   b name    b

class Father extends GrandPa{//父类
    String name = "大头爸爸";
    private int age = 39;
    public int getAge(){
        return this.age;
    }
}

class Son extends Father{//子类
    String name = "大头儿子";
}
