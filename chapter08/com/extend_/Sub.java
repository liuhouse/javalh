package javalh.chapter08.com.extend_;

public class Sub extends Base{//子类

    //会首先调用父类的无参构造器,如果父类没有无参构造器,需要使用super()来进行指定对应的父类构造器
    public Sub(){
        //当继承了父类的话,就相当于是执行了super()来完成对父类的初始化(对父类的无参构造器进行初始化)
        //默认super();
        super("Smith",10);
        System.out.println("Sub()构造器被调用");
    }


    //当定义了有参构造器,那么无参构造器就会被替换掉    换言之,无参构造器就会被销毁 消失掉
    public Sub(String name){
        super(name);
        System.out.println("Sub(String name)构造器被调用....");
    }


    //定义两个参数的构造器
    public Sub(String name , int age){
        //super()完成对父类的初始化,必须放在第一行   因为先有父亲,再有儿子
        //默认会调用super()  无参构造器
        //但是这里想通过父类的两个参数的构造器完成对父类的初始化
        //super(name,age);

        //这里想通过父类的一个参数的构造器完成对父类的初始化
        super(name);
        //this(name);
        System.out.println("Sub(String name , int age)构造器被调用");
    }



    public void sayOk(){//子类方法
        //父类中的非私有的属性和方法可以直接在子类访问
        System.out.println("n1 " + n1 + " n2 " + n2 + " n3 " + n3);
        test100();
        test200();
        test300();
        //私有的属性和方法不能被直接访问
        //System.out.println("n4" + n4);
        //test400();//私有的方法不能被直接访问

        //虽然私有的属性和方法不能被直接访问,但是可以间接访问，也就是要通过父类提供的公共方法去访问
        System.out.println(getN4());
        callTest400();



    }
}
