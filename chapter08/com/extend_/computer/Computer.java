package javalh.chapter08.com.extend_.computer;

class Computer {
    //电脑中的CPU信息   内存信息   硬盘信息 应该都是保密的  所以使用privacte进行修饰
    //电脑的CPU信息
    private String CPU;
    //电脑的内存信息
    private int ORM;
    //电脑的硬盘信息
    private int HardDisk;

    //因为重写了Computer,所以无参构造器就不存在了
    Computer(){

    }

    //有参构造器
    public Computer(String CPU, int ORM, int hardDisk) {
        this.CPU = CPU;
        this.ORM = ORM;
        HardDisk = hardDisk;
    }

    //返回Computer的详细信息
    //对外展示的信息是让用户看的,所以是公开的
    public String getDetail(){
        return  "CPU : " + this.CPU + "\r\n"
               +"ORM : " + this.ORM + "\r\n"
               +"HardDisk" + this.HardDisk + "\r\n"
                ;
    }

    public String getCPU() {
        return CPU;
    }

    public int getORM() {
        return ORM;
    }

    public int getHardDisk() {
        return HardDisk;
    }

    public void setCPU(String CPU) {
        this.CPU = CPU;
    }

    public void setORM(int ORM) {
        this.ORM = ORM;
    }

    public void setHardDisk(int hardDisk) {
        HardDisk = hardDisk;
    }
}
