package javalh.chapter08.com.extend_.computer;

/*
* 编写 Computer 类，包含 CPU、内存、硬盘等属性，getDetails 方法用于返回 Computer 的详细信息
* 编写 PC 子类，继承 Computer 类，添加特有属性【品牌 brand】
* 编写 NotePad 子类，继承 Computer 类，添加特有属性【color】
* 编写 Test 类，在 main 方法中创建 PC 和 NotePad 对象，
* 分别给对象中特有的属性赋值，以及从 Computer 类继承的 属性赋值，并使用方法并打印输出信息
* */

public class ExtendsExercise03 {
    public static void main(String[] args) {
        System.out.println("PC电脑的信息如下====================================");
        PC lenovo = new PC("LENOVO-PC", 8, 128);
        //设置PC的品牌
        lenovo.setBrand("联想PC电脑");
        //打印PC电脑的基本信息
        String lenovo_info = lenovo.getDetail();
        //获取联想电脑的品牌信息
        String brand = lenovo.getBrand();
        //打印出联想电脑的所有信息
        System.out.println("PC电脑的信息：" + lenovo_info + "品牌: " + brand);

        System.out.println();

        //打印笔记本的信息
        System.out.println("笔记本电脑的信息如下==================================");
        NotePad huaWei = new NotePad("HuaWei", 5, 125);
        huaWei.setColor("至尊蓝");
        //获取笔记本的基本信息
        String huawei_info = huaWei.getDetail();
        //获取笔记本电脑的特有信息颜色
        String color = huaWei.getColor();
        //打印出笔记本电脑的所有信息
        System.out.println("笔记本电脑的信息" + huawei_info + "颜色: " + color);


    }
}
