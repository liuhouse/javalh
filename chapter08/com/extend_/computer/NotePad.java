package javalh.chapter08.com.extend_.computer;

public class NotePad extends Computer{
    //添加笔记本电脑的特有属性
    private String color;

    //无参构造器
    NotePad(){

    }

    //有参构造器
    NotePad(String CPU, int ORM , int hardDisk){
        //调用父类的初始化有参构造器
        super(CPU,ORM,hardDisk);
    }

    //笔记本电脑的特殊属性设置,特殊对待
    public void setColor(String color){
        this.color = color;
    }


    //笔记本电脑的特殊属性获取
    public String getColor(){
        return this.color;
    }


}
