package javalh.chapter08.com.extend_.computer;

class PC extends Computer{
    //特有属性 - 品牌
    private String brand;

    //无参构造器
    public PC(){

    }

    //有参构造器，为了使用父类Computer的初始化方法
    public PC(String CPU,int ORM,int hardDisk){
        super(CPU,ORM,hardDisk);
    }

    //给特有的属性赋值
    public void setBrand(String brand){
        this.brand = brand;
    }

    //获取特有的属性
    public String getBrand(){
        return this.brand;
    }


}
