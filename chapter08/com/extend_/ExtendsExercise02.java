package javalh.chapter08.com.extend_;

public class ExtendsExercise02 {
    public static void main(String[] args) {
        C1 c1 = new C1();
        //我是A1类   hahah我是B1类的有参构造器   我是c类的有参构造 我是C1类的无参构造器
    }
}

class A1{//A1类
    //无参构造器
    public A1(){
        System.out.println("我是A1类");
    }
}

class B1 extends A1{//B1类继承A1类
    //无参构造器
    public B1(){
        System.out.println("我是B1类的无参构造器");
    }

    //有参构造器
    public B1(String name){
        System.out.println(name + "我是B1类的有参构造器");
    }
}

class C1 extends B1{//C1类,继承B1类
    //无参构造器
    public C1(){
        this("hello");
        System.out.println("我是C1类的无参构造器");
    }

    //有参构造器
    public C1(String name){
        super("hahah");
        System.out.println("我是c类的有参构造");
    }
}
