package javalh.chapter08.com.extend_.super_;

public class B extends A{

    //private int n1 = 888;

    //编写测试的方法
    public void test(){
        //super的访问不限于直接父类,如果爷爷类和本身的类中有同名的成员,也可以使用super去访问爷爷类的成员
        //如果多个基类(上级类)中都有同名的成员,使用super访问的时候遵循就近原则 A -> B -> C
        //super(参数列表);只能放在构造器的第一句,只能出现一句

        //使用super.属性方法 访问父类的属性
        //访问父类的属性,但是不能访问父类的private属性
        //System.out.println("super.n1 = " + super.n1);
        //System.out.println("super.n2 = " + super.n2);
        //System.out.println("super.n3 = " + super.n3);
        //System.out.println("super.n4 = " + super.n4);//无法访问  因为是私有的

        //访问父类的方法,但是不能访问父类的private方法
        //super.test100();
        //super.test200();
        //super.test300();
        //super.test400();//私有方法不能访问
    }

    //无参的构造方法
    public B(){
        //默认是super()
        //默认调用父类的无参构造器
        //super();

        //调用父类的一个参数的构造器
        //super("华帝");

        //调用父类的两个参数的构造器
        super("华帝",100);
    }


//    public void cal(){
//        System.out.println("B类的cal()方法...");
//    }

    public void sum(){
        System.out.println("B类的sum()....");

        /*
        *希望调用父类[A]的cal方法
        * 这时因为子类B没有cal方法,因此我们可以使用下面三种形式来访问父类的cal方法
        *
        * 找到cal方法的时候(cal() 和 this.cal())顺序是：
        * (1)先找本类,如果有,则调用
        * (2)如果没有,则找父类(如果有,并可以调用,则调用)
        * (3)如果父类没有，则继续查找父类的父类,整个规则,都是意义昂的,直到Object类
        * 提示：如果查找方法的过程中,找到了,但是不能访问[private]，则报错,cannot access
        * 如果查找方法的过程中,没有找到,则提示方法不存在
        * */


        //调用本类的cal()方法[第一种方式]
        //cal();
        //调用本类的cal()方法 【第二种方法】
        //this.cal();
        //上面两种方法是等价的

        //调用父类的cal方法
        //super.cal();

        //调用cal()方法   如果本类中没有cal()方法的时候,会向父类查找cal()
        //cal();



        //演示访问属性的规则
        /*
        * n1 和 this.n1 查找的规则是
        * (1)先查找本类,如果有,则调用
        * (2)如果没有,就找父类(如果有,并且可以调用,则调用)
        * (3)如果父类没有,就继续查找父类的父类,整个规则,都是一样的,直到Object类
        * 提示：如果查找属性的过程中,找到了,但是不能访问,就报错cannot access
        * 如果查找属性的过程中,没有找到,则提示属性不存在
        * */

        //查找本类的n1
        //如果本类有的话,就使用本类的,否则就会去父类查找
        //本类中的private修饰的属性也是可以直接访问的
        //System.out.println(n1);//888
        //System.out.println(this.n1);//888
        //上面两种方法是等价的
        //调用父类的n1
        //System.out.println(super.n1);

        //调用父类的n1
        //如果父类中的n1存在,就直接访问   如果父类中查找不到n1，就在父类的父类中查找
        System.out.println(super.n1);

        super.cal();

    }


}
