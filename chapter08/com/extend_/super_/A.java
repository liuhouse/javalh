package javalh.chapter08.com.extend_.super_;

public class A extends Base{
    //4个属性
    //public int n1 = 100;
    protected int n2 = 200;
    int n3 = 300;
    private int n4 = 400;

    //无参构造器
    public A(){
        System.out.println("A()构造方法...");
    }

    //一个参数的构造器
    public A(String name){
        System.out.println("A(String name)构造方法...");
    }

    //两个参数的构造器
    public A(String name , int age){
        System.out.println("A(String name , int age)构造方法...");
    }

    public void test100(){

    }

    protected void test200(){

    }

    void test300(){

    }

    private void test400(){

    }

//    public void cal(){
//        System.out.println("A类的cal()方法....");
//    }

}
