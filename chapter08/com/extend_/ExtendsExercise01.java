package javalh.chapter08.com.extend_;

public class ExtendsExercise01 {
    public static void main(String[] args) {
        B b = new B();
        //a  b name   b
    }
}


class A{
    //无参构造器
    A(){
        System.out.println("a");
    }

    //有参构造器
    A(String name){
        System.out.println("a name");
    }
}

//我是A类    haha我是B类的有参构造器  我是C类的有参构造   我是C类的无参构造

class B extends A{
    //无参构造器
    B(){
        //这边不会去调用默认的super()方法,因为,这边已经有this了,this和 super()都只能放在构造方法的第一行
        //如果这边什么都没有写 , 那么会自动调用super() 也就是父类的无参构造器
        //如果这边有this了就不会再执行默认的super()方法了
        this("abc");
        System.out.println("b");
    }

    //有参构造器
    B(String name){
        //这边如果不写的话  是有默认的super()方法的
        System.out.println("b name");
    }
}
