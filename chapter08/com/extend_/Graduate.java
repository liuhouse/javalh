package javalh.chapter08.com.extend_;
/*
 * 大学生类 有名字   有年龄   有成绩
 * 有考试操作
 * 有返回考生的整体信息
 * */
class Graduate {
    //学生的名字
    public String name;
    //学生的年龄
    public int age;
    //学生的分数
    private double score;

    public void setScore(double score){
        this.score = score;
    }

    //学生正考试
    public void testing(){//除了输出不一样,其他的都一样
        System.out.println(this.name + "进行了高数考试,分数是 : " + this.score);
    }

    //返回这个学生的信息
    public String getUserInfo(){
        return "姓名: " + this.name + "年龄:" + this.age + "分数:" + this.score;
    }
}
