package javalh.chapter08.com.extend_.improve_;

/*
 * 大学生类 有名字   有年龄   有成绩
 * 有考试操作
 * 有返回考生的整体信息
 * */
class Graduate extends Student{
    //学生正考试
    public void testing() {//除了输出不一样,其他的都一样
        System.out.println(this.name + "进行了高数考试,分数是 : " + this.score);
    }
}
