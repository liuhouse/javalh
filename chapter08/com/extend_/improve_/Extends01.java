package javalh.chapter08.com.extend_.improve_;

//同一个包下面的类是不需要导入的

public class Extends01 {
    public static void main(String[] args) {
        javalh.chapter08.com.extend_.improve_.Pupil pupil = new javalh.chapter08.com.extend_.improve_.Pupil();
        pupil.name = "银角大王";
        pupil.age = 15;
        pupil.setScore(20);
        pupil.testing();
        System.out.println(pupil.name + "的信息如下============");
        System.out.println(pupil.getUserInfo());

        System.out.println();
        javalh.chapter08.com.extend_.improve_.Graduate graduate = new javalh.chapter08.com.extend_.improve_.Graduate();
        graduate.name = "金角大王";
        graduate.age = 150;
        graduate.setScore(145);
        graduate.testing();
        System.out.println(graduate.name + "的信息如下===========");
        System.out.println(graduate.getUserInfo());
    }
}
