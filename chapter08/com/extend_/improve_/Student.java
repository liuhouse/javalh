package javalh.chapter08.com.extend_.improve_;

//父类

/*
* 总结继承
* 有多个类,找到这些类的相同属性和方法 提取出来   放到另外一个文件   然后这些文件继承这个文件
* 就自动会拥有这些属性和方法了    使得代码特别的简洁
*
* 而子类中可以有自己的特殊方法  和  特殊属性
* */

public class Student {
    //学生的名字
    public String name;
    //学生的年龄
    public int age;
    //学生的分数
    public double score;

    //设置分数
    public void setScore(double score) {
        this.score = score;
    }

    //返回这个学生的信息
    public String getUserInfo() {
        return "姓名: " + this.name + "年龄:" + this.age + "分数:" + this.score;
    }

}
