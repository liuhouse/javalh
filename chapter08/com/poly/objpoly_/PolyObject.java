package javalh.chapter08.com.poly.objpoly_;

public class PolyObject {
    public static void main(String[] args) {
        //对象多态【核心,困难,重点】
        //体验对象多态特点
        //老韩说明
        //animal 编译类型就是 Animal , 运行类型是Dog
        Animal animal = new Dog();
        //因为运行的时候,执行到该行的时候,animal的运行类型是Dog,所以cry就是Dog的cry
        animal.cry();

        //animal编译类型 Animal,运行类型就是Cat
        animal = new Cat();
        //运行的时候,执行到该行,animal的运行类型是Cat,所以cry就是Cat的cry
        animal.cry();
    }
}






