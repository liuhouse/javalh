package javalh.chapter08.com.poly.objpoly_;

public class DynamicBinding {
    public static void main(String[] args) {
        //此时编译类型是A  运行类型是B
        //属性访问看编译类型     方法访问看运行类型

//        System.out.println(a.sum());//40
//        System.out.println(a.sum1());//30


        A a = new B();//向上转型
        /*
         * java的动态绑定机制
         * (1):当调用对象方法的时候,该方法会和该对象的内存地址/运行类型绑定
         * (2):当调用对象属性的时候,没有动态绑定机制,哪里声明,哪里使用
         * */
        System.out.println(a.sum());//30
        System.out.println(a.sum1());//20
    }
}

class A {//父类
    public int i = 10;

    public void setI(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }

    public int sum() {
        return getI() + 10;//20 + 10
    }

    public int sum1() {
        return i + 10;//10 + 10 = 20
    }


}


class B extends A {//子类
    public int i = 20;

    @Override
    public void setI(int i) {
        this.i = i;
    }

    @Override
    public int getI() {
        return i;
    }

//    public int sum(){
//        return i + 20;
//    }

//    public int sum1(){
//        return i + 10;
//    }
}