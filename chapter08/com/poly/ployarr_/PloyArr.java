package javalh.chapter08.com.poly.ployarr_;

public class PloyArr {
    public static void main(String[] args) {

        /*
        * 需求：
        * 现有一个继承结构如下：
        * 要求创建1个Person对象
        * 2个Student对象和2个Teacher对象,统一放在数组中,并且调用每个对象的say()方法
        *
        * //总结一点
        * 当想要调用子类的特有的方法的时候  可以使用向下转型
        * */

        //将父类Person作为编译类型,创建长度为5的数组
        Person person[] = new Person[5];

        //创建一个Person对象  [编译类型是Person 父类]
        person[0] = new Person("jack" , 20);
        //创建两个Student对象
        person[1] = new Student("marry",18,100);
        person[2] = new Student("smith",19,30.1);
        //创建两个教师对象
        person[3] = new Teacher("scoot",30,20000);
        person[4] = new Teacher("king",50,25000);

        //遍历多态数组,调用say
        for(int i = 0 ; i < person.length ; i++){
            //老师提示：person[i]编译类型是Person，运行类型是根据实际的情况有JVM来判断
            //其实这里使用了向上转型
            System.out.println(person[i].say());//因为这里调用了方法,所以这里使用了动态绑定机制

            //现在想要访问老师和学生的特有方法
            //这里是错误的,因为这些对象的编译类型是Person  但是Person类里面并没有这两个方法
            //这两个方法是Teacher类和Study类的独有对象    所有Person是不能访问的
            //person[i].study();
            //person[i].teach();

            //这里大家要聪明一点 使用 类型判断  + 向下转型
            //向下转型之后  编译类型就改变成为自己的类  所以就可以访问到了

            //如果当前对象是属于学生对象的话
            if(person[i] instanceof Student){
                //因为当前对象通过判断已经是属于Student , 所以可以向下转型
                Student student = (Student)person[i];
                student.study();
            }else if(person[i] instanceof Teacher){
                //如果当前对象是属于教师对象的话,使用向下转型
                Teacher teacher = (Teacher)person[i];
                teacher.teach();
            }else if(person[i] instanceof Person){

            }else{
                System.out.println("类型错误,请自己检查...");
            }
        }

    }
}
