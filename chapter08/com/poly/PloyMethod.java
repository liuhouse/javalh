package javalh.chapter08.com.poly;

public class PloyMethod {
    public static void main(String[] args) {
        //多态的基本介绍【多种状态】
        //方法或者对象具有多种形态,是面向对象的第三大特征,多态是建立在封装和继承的基础之上的
        //多态的具体体现

        //方法重载体现多态
        A a = new A();
        //这里我们传入了不同的参数,就会调用不同的sum方法,根据参数的个数以及参数的类型,会调用
        //不同的方法    所以我们说  方法重载  体现多态
        System.out.println(a.sum(1,2));
        System.out.println(a.sum(1,2,3));


        //方法重写体现多态
        //我们使用不同的对象调用相同的方法   但是他们会去找各自的方法 去分别进行调用
        //也就是子类对父类的重写  体现出多态
        B b = new B();
        a.say();
        b.say();

    }
}

class B{//父类
    public void say(){
        System.out.println("B say()方法被调用...");
    }
}

class A extends B{//子类
    public int sum(int n1 , int n2){//这个sum()方法和下面的sum()方法构成重载
        return n1 + n2;
    }

    public int sum(int n1 , int n2 , int n3){
        return n1 + n2 + n3;
    }

    public void say(){
        System.out.println("A say()方法被调用...");
    }
}
