package javalh.chapter08.com.poly;

public class Master {

    /*
    * 老韩的几句重要的话(几句话)
    * (1)一个对象的编译类型和运行类型可以不一致
    * (2)编译类型在定义对象的时候,就确定了,不能改变
    * (3)运行类型是可以变化的
    * (4)编译类型看定义时 = 号的左边  ， 运行 类型看 = 号的右边
    * */

    private String name;

    public Master(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    //使用多态的机制解决主人给动物喂食的问题
    //因为可以使用父类调用子类的方法,多态    左边编译类型    右边 运行类型    编译类型不变
    //但是运行类型可以发生变化   所以  可以使用Animal来进行接收
    //使用Animal[父类]来进行接收  其实跟使用子类自己接收所达到的目的是一样的
    //其实多态 的重点在于    编译类型 可以是子类,可以是父类 , 而使用父类   可以接收子类的对象
    public void feed(Animal animal , Food food){
        System.out.println("animal" + animal + "food" + food);
        System.out.println(animal.getName() + "吃" + food.getName());
    }

    //先不使用多态处理
    //主人喂食的方法
    //主人给狗喂骨头
//    public void feed(Dog dog , Bone bone){
//        System.out.println("主人 " + name + "给" + dog.getName() + "吃" + bone.getName());
//    }


    //主人给小猫喂黄花鱼
//    public void feed(Cat cat , Fish fish){
//        System.out.println("主人 " + name + "给" + cat.getName() + "喂" + fish.getName());
//    }


}
