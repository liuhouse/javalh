package javalh.chapter08.com.poly.polyparameter_;

//员工类
public class Employee {
    //姓名
    private String name;
    //月工资
    private double salary;

    public Employee(String name,double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    //计算年工资getAnnual
    public double getAnnual(){
        return salary * 12;
    }

}
