package javalh.chapter08.com.poly.polyparameter_;

//普通员工类
public class Work extends Employee{
    public Work(String name,double salary) {
        super(name,salary);
    }

    //重写父类的获取年工资的方法
    public double getAnnual(){//因为员工没有其他的收入,所以直接调用父类的方法
        //当调用对象方法的时候,该方法会和对象的内存地址/运行类型绑定
        return super.getAnnual();
    }

    //普通员工多个work方法
    public void work(){
        System.out.println("员工" + getName() + "正在工作...");
    }
}
