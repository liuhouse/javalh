package javalh.chapter08.com.poly.polyparameter_;

//测试类
public class PloyParameter {
    public static void main(String[] args) {
        //在main方法中调用获取员工的年工资
        PloyParameter ployParameter = new PloyParameter();
        //定义普通员工
        Work smith = new Work("普通员工-smith",3000);
        //获取普通员工的工资
        System.out.println(ployParameter.showEmpAnnual(smith));

        //定义经理
        //为什么这里传入员工就使用员工的工资来进行计算呢   为什么传入的是经理 就使用经理的工资来计算呢
        //这是因为这里的运行类型都是实例化的对象  而且
        //1：当调用对象方法的时候,该方法会和该对象的内存地址,也就是运行类型绑定
        //2: 当调用对象属性的时候,没有动态绑定机制,哪里声明,哪里使用
        Manage mange = new Manage("经理王总", 20000, 200000);
        //获取经理的工资
        System.out.println(ployParameter.showEmpAnnual(mange));

        //如果是普通员工,就调用work方法 , 如果是管理员的话   就调用manage方法
        ployParameter.testWork(smith);
        ployParameter.testWork(mange);
    }

    //实现获取任何员工对象的年工资,意思就是传入工人对象就获取员工对象的年工资,传入经理的对象,就获取经理对象的年工资
    //这里的核心思想就是    使用 父类   可以获取子类的实例化对象
    public double showEmpAnnual(Employee e){
        //获取员工的年工资
        return e.getAnnual();
    }


    //测试类中添加一个方法,testWork，如果是普通员工,就调用work方法,如果是经理,则调用manage方法
    //切记.... 不要眼高手低
    public void testWork(Employee e){
        //如果当前的对象是属于普通员工的
        //这里能调用是因为要使用向下转型机制 ， 然后调用方法的时候   会使用运行类型 ， 调用方法的时候
        //会跟当前的运行类型进行绑定
        if(e instanceof Work){
            ((Work) e).work();
        }else if(e instanceof Manage){
            ((Manage) e).manage();
        }else if(e instanceof Employee){
            System.out.println("没有特殊的方法");
        }else{
            System.out.println("方法是错误的...");
        }
    }


}
