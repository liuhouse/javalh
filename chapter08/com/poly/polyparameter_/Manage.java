package javalh.chapter08.com.poly.polyparameter_;

public class Manage extends Employee{
    //奖金
    private double bonus;

   //构造方法
    public Manage(String name ,double salary, double bouns){
        super(name,salary);
        this.bonus = bouns;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public double getBonus(){
        return this.bonus;
    }

    public double getAnnual(){
        return super.getAnnual() + bonus;
    }

    //经理是有管理方法的
    public void manage(){
        System.out.println("经理" + getName() + "正在managing...");
    }


}
