package javalh.chapter08.com.poly.detail_;

public class Cat extends Animal{
    public void eat(){//对父类的方法进行了方法重写
        System.out.println("猫吃鱼");
    }

    //添加猫特有的方法
    //Cat独有的方法
    public void catchMouse(){
        System.out.println("猫捉老鼠");
    }
}
