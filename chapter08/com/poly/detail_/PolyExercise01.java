package javalh.chapter08.com.poly.detail_;

public class PolyExercise01 {
    /*
    * 练习
    * 请说出下面的每条语言,哪些是正确的,哪些是错误的,为什么?
    * */
    public static void main(String[] args) {
        double d = 13.4;//ok
        long i = (long)d;//ok
        System.out.println(i);//13
        int in = 5;//ok
        //boolean b = (boolean)in; // 不对  boolean -> int
        Object obj = "Hello";//可以的,向上转型
        String objStr = (String)obj;//向下转型  因为obj本身就是字符串  所以可以向下转型
        System.out.println(objStr);//hello
//        Object objPri = new Integer(5);//可以向上转型
//        System.out.println(objPri);//5
        //String str = (String)objPri; //错误的 objPri本身是整形   整形不能转换成String
//        Integer str1 = (Integer)objPri;//可以的  向下转型 Object -> int  并且  objPri 本身就是INT
//        System.out.println(str1);
    }
}
