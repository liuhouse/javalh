package javalh.chapter08.com.poly.detail_;

public class PolyDetail03 {
    public static void main(String[] args) {
        //实例化一个BB对象
        BB bb = new BB();
        System.out.println(bb instanceof BB);//true
        System.out.println(bb instanceof AA);//true

        //使用父类编译子类的对象
        //aa的编译类型是AA ， 运行类型是BB
        //BB是AA的子类

        //下面的例子可以看出来
        //实例化的对象是属于他的编译类型   也是属于编译类型的子类的
        AA aa = new BB();
        System.out.println(aa instanceof AA);//true
        System.out.println(aa instanceof BB);//true



        Object obj = new Object();
        //Object是一切对象的父类
        //obj 不属于AA 也不属于AA的子类   所以这里是false
        //System.out.println(obj instanceof AA);//false

        String str = "hello";
        //字符串不是AA的子类
        //System.out.println(str instanceof AA);

        System.out.println(str instanceof Object);//字符串是Object的子类


    }
}


class AA{}//父类
class BB extends AA{}//子类   也就是BB继承了AA