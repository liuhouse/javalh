package javalh.chapter08.com.poly.detail_;

import javalh.chapter08.com.poly.Dog;

public class PloyDetail {
    public static void main(String[] args) {
        //实例化猫
        //向上转型 ： 父类的引用指向子类的对象
        //语法：父类类型 引用名 = new 子类类型();
        Animal animal = new Cat();
        //Object obj = new Cat();//这样可以吗?可以，因为Object也是Cat的父类  准确的说是祖宗



        //向上转型调用的方法规则如下
        //(1)可以调用父类中的所有成员(需要遵守访问权限)
        //(2)但是不能调用子类中的特有成员
        //   (#)因为在编译阶段,能调用哪些成员,是由编译类型决定的
        //   animal.catchMouse();
        //(4)最终运行效果看子类(运行类型)的具体实现,即调用方法的时候,按照从子类(运行类型)开始查找方法
        //然后进行调用,规则跟我们讲的调用规则一致

        animal.eat();//猫吃鱼
        animal.run();//跑
        animal.show();//Hello 你好
        animal.sleep();//睡


        System.out.println("================================");


        //老师希望,可以调用Cat的 catchMouse方法
        //这里我们就要用到多态的向下转型了

        //(1)语法：子类类型  引用名  = (子类类型) 父类引用;
        //问一个问题? cat的编译类型Cat，y运行类型是Cat
//        animal.catchMouse();//这是错误的,因为在编译的时候找不到
        //Animal animal = new Cat();


        //其实就是很简单的一句话   不能把猫对象转化为狗对象

        //所以就需要进行向下转型了
        //将Animal向下转型成 Cat
        Cat cat = (Cat) animal;
        //此时编译类型是Cat，而运行类型也是Cat
        cat.catchMouse();

        //(2)要求父类的引用必须指向的是当前目标类型的对象
        //Dog dog = (Dog) animal;

    }
}
