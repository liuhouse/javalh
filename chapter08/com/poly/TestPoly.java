package javalh.chapter08.com.poly;

public class TestPoly {
    public static void main(String[] args) {

        /*
        * 使用传统的方式来解决
        * 传统的问题代码的复用性不高,而且不利于代码的维护
        * 解决方法:引出我们要讲解的多态【变态】
        * */

        //主人类
        Master tom = new Master("tom");
        //狗类
        Dog dog = new Dog("小狗狗");
        //骨头类
        Bone bone = new Bone("脆骨");
        //主人给狗喂骨头
        tom.feed(dog,bone);

        //猫类
        Cat cat = new Cat("小猫猫");
        //鱼类
        Fish fish = new Fish("小黄鱼");
        //主人给猫喂鱼
        tom.feed(cat,fish);
    }
}
