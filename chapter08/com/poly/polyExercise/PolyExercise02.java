package javalh.chapter08.com.poly.polyExercise;

public class PolyExercise02 {
    public static void main(String[] args) {
        Sub s = new Sub();
        System.out.println(s.count);//20
        s.display();//20

        //编译类型变化成了b ,  也就是进行了向上转型
        Base b = s;
        System.out.println(b == s);//true
        //这个时候编译类型已经变化成了 b 所以   所以这里的count 指的就是编译类型Base里面的count
        System.out.println(b.count);//10  属性访问是根据编译类型
        //所以这里的display()就是Base里面的display()
        b.display();//20  方法访问看运行类型   属性访问看编译类型



    }
}
