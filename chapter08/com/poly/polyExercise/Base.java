package javalh.chapter08.com.poly.polyExercise;

public class Base {//父类
    int count = 10;
    public void display(){
        System.out.println(this.count);
    }
}


class Sub extends Base{//子类
    int count = 20;
    //子类重写了父类的方法
    public void display(){
        System.out.println(this.count);
    }
}
