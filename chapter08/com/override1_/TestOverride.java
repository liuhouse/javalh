package javalh.chapter08.com.override1_;

public class TestOverride {
    public static void main(String[] args) {
        System.out.println("Person类的输出情况如下......");
        Person person = new Person("人啊", 1000);
        System.out.println(person.say());


        System.out.println("Student类的输出情况如下....");
        Student liming = new Student("黎明", 55, 1, 99.9);
        System.out.println(liming.say());
    }
}
