package javalh.chapter08.com.override1_;

public class Person {
    //姓名
    private String name;
    //年龄
    private int age;

    //构造器
    public Person(String name,int age){
        this.name = name;
        this.age = age;
    }




    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    //返回自我介绍的字符串
    public String say(){
        return "我的名字：" + this.name + "我的年龄: " + this.age;
    }



}
