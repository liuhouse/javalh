package javalh.chapter08.com.override1_;

public class Student extends Person{
    private int id;
    private double score;

    //构造器
    public Student(String name , int age , int id , double score){
        super(name,age);
        this.id = id;
        this.score = score;
    }



    //对父类方法进行重写
    public String say(){
        return "我的姓名 :" + getName() + "我的年龄 :" + getAge() + "id :" + this.id + "我的分数：" + this.score;
    }
}
