package javalh.chapter08.com.packagestudy;

import javalh.chapter08.com.modifier.A;

public class Test1 {
    //演示在不同包下面对访问修饰符的使用
    public static void main(String[] args) {
        A a = new A();
        //不同包的访问    只能访问public修饰的属性和方法

        System.out.println(a.n1);
        //System.out.println(a.n2);//不能访问   因为是受保护的
        a.m1();
        //a.m2();//不能访问,因为是受保护的
    }
}
