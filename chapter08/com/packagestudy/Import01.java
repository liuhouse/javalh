package javalh.chapter08.com.packagestudy;

/*
* 导入包：
* 语法:import 包名;
* 我们引入一个包的主要目的就是要使用该包下面的类
* 比如 import java.util.Scanner; 就只是引入一个类Scanner
* 老韩建议：我们使用到哪个类,就导入哪个类即可,不建议使用*进行导入
* */
//第一种导入方式  说明:只会引入java.util包下的Scanner
import java.util.Arrays;
import java.util.Scanner;

//第二种导入方式 说明:表示将java.util包下面所有的类都引入 也称作导入
//import java.util.*;


public class Import01 {
    public static void main(String[] args) {
        //Scanner scanner = new Scanner(System.in);

        //使用系统提供的Arrays 类  完成数组排序
        int[] arr = {-1,20,2,3,5};

        //这样排序就显得很简单了
        //比如将其进行排序
        //传统的方法是,自己编写排序(冒泡)
        //系统提供了相关的类,可以很方便的完成数组的排序   Arrays

        Arrays.sort(arr);

        for(int i = 0 ; i < arr.length ; i++){
            System.out.print(arr[i] + " ");
        }


    }
}
