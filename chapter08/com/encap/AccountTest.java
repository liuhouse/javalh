package javalh.chapter08.com.encap;

public class AccountTest {
    public static void main(String[] args) {

        /*
        * 总结：封装的意义和大致流程
        * (1):先进行属性的私有化
        * (2):进行 set   get 的设置  用作 设置属性   和获取属性
        * (3):根据需求,定义相关的方法
        * (4):在别的类中根据两种实例化对象的形式对类中的属性或者方法根据业务逻辑 进行调用即可
        * */

        System.out.println("第一种调用的方式=======================================");
        Account sbn = new Account("撒贝宁", 10, "123456");
        System.out.println("撒贝宁的账户信息如下=============");
        System.out.println(sbn.getAccountInfo());
        System.out.println("撒贝宁的账号密码是,不能公开调用,只能撒贝宁自己调用");
        System.out.println("密码:" + sbn.getPassword());

        System.out.println("第二种调用的方式=======================================");
        Account account = new Account();
        account.setName("撒贝宁");
        account.setBalance(6);
        account.setPassword("666666");
        System.out.println(account.getAccountInfo());
        System.out.println("撒贝宁的账号密码是,不能公开调用,只能撒贝宁自己调用");
        System.out.println("密码：" + account.getPassword());
    }
}
