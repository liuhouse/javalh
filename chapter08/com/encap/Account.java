package javalh.chapter08.com.encap;

/*
* 需求：
* 1:Account类要求具有属性:姓名(长度为2位或3位或4位),余额(必须>20),密码(必须是6位),如果不满足,就给出提示条件
* 并给默认值(程序员自己定)
* 2:通过setXXX的方法给Account的属性赋值
* 3:在AccountTest中进行测试
*
* 思路分析
* (1)定义Account类
* (2)定义属性 姓名，余额,密码
* (3)定义设置属性的方法 set get
* (4)进行数据验证 (姓名长度 2 || 3 || 4)   余额必须大于20 密码必须是6位
* (5)定义获取个人账户信息的方法
* */
public class Account {
    //定义账户名字
    private String name;
    //定义余额
    private double balance;
    //定义密码
    private String password;

    //无参构造器
    public Account(){

    }

    //三个参数的构造器
    public Account(String name,double balance , String password){
        this.setName(name);
        this.setBalance(balance);
        this.setPassword(password);
    }

    //设置名字
    public void setName(String name) {
        //进行姓名的验证
        if(name.length() == 2  || name.length() == 3 || name.length() == 4){
            this.name = name;
        }else{
            System.out.println("姓名的长度不合法(必须在2或者3位或者4位之间，给一个默认姓名:非法姓名)");
            this.name = "非法姓名";
        }

    }

    //设置薪水
    public void setBalance(double balance) {
        //进行余额验证,必须大于20
        if(balance < 20){
            System.out.println("余额已经不足20了");
        }
        this.balance = balance;
    }

    //设置密码
    public void setPassword(String password) {
        if(password.length() != 6){
            System.out.println("密码必须是6位");
            this.password = "888888";
        }else{
            this.password = password;
        }

    }


    //获取名字
    public String getName() {
        return name;
    }

    //获取余额
    public double getBalance() {
        return balance;
    }

    //获取密码
    public String  getPassword() {
        if("撒贝宁".equals(this.name)){
            return password;
        }
        return "只有本人能查看密码";
    }

    //获取账户信息
    public String getAccountInfo(){
         return "账户名称 : " + this.name + "\r\n" +
                "账户余额 : " + this.balance + "\r\n";
    }
}
