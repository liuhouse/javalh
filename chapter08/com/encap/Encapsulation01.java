package javalh.chapter08.com.encap;

public class Encapsulation01 {
    public static void main(String[] args) {
        //如果要使用快捷键 alt + r , 需要先配置主类
        //第一次,我们使用鼠标点击的形式运算程序,后面就可以使用
        //给这个人设置名字年龄和薪水

        /*
        * 小结1：
        * 先进行类的定义,定义里面的属性和方法,然后根据要求进行合理性的验证,最后在需要的地方实例化调用即可
        * */


        Person person = new Person();
        person.setName("王");
        person.setAge(200);
        person.setSalary(50000);

        //读取单独的名字
        System.out.println(person.getName());
        //读取单独的年龄
        //person.age;//这样的方法是错误的,因为现在的年龄是私有化的
        //正确的读取年龄的方式 - 用系统指定的方式进行读取,可以加上业务逻辑的判断
        System.out.println(person.getAge());
        //获取薪水
        System.out.println(person.getSalary());

        //我们使用构造器指定属性,这个时候,使用构造器进行初始化数据的时候,验证就失效了
        Person hsp = new Person("韩", 888, 60000);
        System.out.println("韩顺平的完整信息如下===============");
        System.out.println(hsp.getUserInfo());
        System.out.println("\r\n");

        //获取这个人的完整信息
        System.out.println("这个人的完整信息如下===============");
        System.out.println(person.getUserInfo());
    }
}

/*
 * 一个案例
 * 在java中如何实现这种类似的控制呢?
 * 请大家看一个小程序：
 * 不能随便查看人的年龄,工资等信息,并且对设置的年龄进行合理的判断,年龄合理就进行设置,否则就给默认年龄,必须在1-120之间,
 * 年龄(private),工资(private) 不能随便查看  name的长度在2-6个字符
 *
 * 需求分析：
 * (1)定义一个类Person
 * (2)属性有 名字,年龄,工资
 * (3)定义设置这些字段的方法set    定义读取这些属性的方法
 * (4)根据要求加入对应的业务逻辑
 * (5)定义一个公共的方法用作查看这个人员的信息
 *
 * 原则  越慢越快  越快越慢
 * */

class Person{
    //名字是公开的
    public String name;
    //年龄是私有的
    private int age;
    //薪水也是私有的
    private double salary;

    //但是当使用构造器的时候,之前的限制就没有作用了

    //无参构造器
    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //三个参数的构造器
    public Person(String name,int age , double salary){
        //这个时候,使用构造器进行初始化数据的时候,验证就失效了
        //解决办法
        //我们可以将set方法写在构造器中,这样仍然可以进行验证
        setName(name);
        setAge(age);
        setSalary(salary);

//        this.name = name;
//        this.age = age;
//        this.salary = salary;
    }


    //如果觉得手写太慢的话  使用 alt + enter  快速完成

    public void setName(String name){
        //姓名的长度为2-6个字符
        if(name.length() < 2 || name.length() > 6){
            System.out.println("姓名的长度不合法，长度必须在(2-6)个之间,所以给了一个默认的名字");
            this.name = "无名";
        }else{
            this.name = name;
        }
    }

    public void setAge(int age){
        //年龄必须在1-120之间
        if(age < 1 || age > 120){
            System.out.println("年龄不合法,必须在(1-120之间),给了一个默认的年龄18");
            this.age = 18;
        }else{
            this.age = age;
        }
    }

    public void setSalary(double salary){
        this.salary = salary;
    }

    public String getName(){
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getSalary(){
        return salary;
    }

    //查看这个人的整体信息
    public String getUserInfo(){
        return "名字:" + this.name + "\r\n" + "年龄：" + this.age + "\r\n" + "薪水：" + this.salary;
    }



}


