package javalh.chapter08.com.modifier;

public class A {
    public int n1 = 100;
    protected int n2 = 200;
    int n3 = 300;
    private int n4 = 400;

    public void m1(){}
    protected void m2(){}
    void m3(){}
    private void m4(){}

    //演示同类下面的属性访问
    public void TongLei(){
        //在同一个类中,可以访问public，protected，默认和private 修饰属性
        System.out.println("n1 = " + n1 + "n2 = " + n2 + " n3=" + n3 + "n4 = " + n4);

        //在同一个类中,可以访问public,protected,默认和private 修饰方法
        m1();
        m2();
        m3();
        m4();
    }

    //入口方法
    public static void main(String[] args) {
        A a = new A();
        a.TongLei();
    }

}
