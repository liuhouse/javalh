package javalh.chapter08.com.modifier;

//只有 默认和public可以修饰类
class Test {
    public static void main(String[] args) {
        //演示包下面的访问机制
        B b = new B();
        b.say();

        A a = new A();
        a.TongLei();
    }
}
