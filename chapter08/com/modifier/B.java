package javalh.chapter08.com.modifier;

public class B {
    //演示同包下面的不同类
    public void say(){
        A a = new A();
        // + "n4 = " + a.n4   不能访问  因为是private修饰的
        //在同一个包下,可以访问public , 默认修饰符 , protected    但是不能访问private的属性
        System.out.println("n1 = " + a.n1 + "n2 = " + a.n2 + "n3 = " + a.n3);

        //在同一个包下面,可以访问public,protected和默认修饰符修饰的属性和方法，不能访问private修饰的方法
        a.m1();
        a.m2();
        a.m3();
        //a.m4();//不能访问

    }



}
