package javalh.chapter08.HomeWork.HomeWork07;

public class HomeWork07 {
    //Test Demo  Rose Jack
    //john jack
    public static void main(String[] args) {
        new Demo().test();//匿名读对象
        new Demo("john").test();//匿名
    }
}


class Test{//父类
    String name = "Rose";
    Test(){
        System.out.println("Test");//(1)Test
    }
    Test(String name){//john
        this.name = name;//属性是没有动态绑定机制的,在哪里声明,在哪里使用
    }
}

class Demo extends Test{//子类
    String name = "jack";
    Demo(){
        super();
        System.out.println("Demo");//(2)Demo
    }

    Demo(String s){
        super(s);
    }

    public void test(){
        System.out.println(super.name);//(3)Rose  (5)john
        System.out.println(this.name);//(4)jack   (6)jack
    }
}
