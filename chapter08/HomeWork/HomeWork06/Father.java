package javalh.chapter08.HomeWork.HomeWork06;

public class Father extends Grand{//父类继承了基类
    String id = "001";
    private double score;
    public void f1(){}
    //问题!super()可以访问哪些成员(属性和方法)
    //super.name  super.g1()
    //问题!this可以访问哪些成员
    //this.id this.score this.f1() this.name this.g1()

    public void test(){
        //测试super
        System.out.println(super.name);
        super.g1();

        //测试this
        System.out.println(this.id);
        System.out.println(this.score);
        this.f1();
        this.g1();
        System.out.println(this.name);
    }
}
