package javalh.chapter08.HomeWork.HomeWork06;

class Son extends Father{
    String name = "BB";
    public void g1(){}
    private void show(){}
    //问题！super可以访问哪些成员(属性和方法)?
    //super.id super.f1() super.name super.g1()
    //this可以访问哪些成员方法
    //this.name this.g1() this.show() this.id  this.f1()
    //父类的 this.name   this.g1() 这两个是不能被访问到的 因为  本类中已经有name属性,所以,找到了就会停止向上查找
    //因为本类中已经有了g1() 方法   所以  在本类中找到了就不会到父类中去查找了
    

    @Override
    public void test() {
        //测试super的访问
        System.out.println(super.id);
        super.f1();
        System.out.println(super.name);
        super.g1();

        //测试this
        System.out.println(this.name);
        this.g1();
        this.show();
        System.out.println(this.id);
        this.f1();
        System.out.println(this.name);
        this.g1();

    }
}
