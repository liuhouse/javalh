package javalh.chapter08.HomeWork.HomeWork08;

public class SavingsAccount extends CheckingAccount {
    /*
     * 需求,每个月都有利息产生,(earnMonthInterest方法被调用),并且有每月三次免手续费的存款,在earnMonthInterest方法中充值交易计数
     * */
    //交易次数
    private int count = 0;
    //利率
    private double interest = 0.01;
    //超过的次数就要收取手续费
    private int max_count = 3;

    public SavingsAccount(double initialBalance) {
        super(initialBalance);
    }

    //因为这里也牵扯到存款和取款,所以这里要重写父类的存款和取款的方法
    //重写父类存款的方法

    @Override
    public void deposit(double amount) {
        //也就是说前三次存款不需要手续费
        if (count < max_count) {
            super.setBalance(super.getBalance() + amount);
        } else {
            //如果超过3次就需要手续费了
            super.deposit(amount);
        }
        //每次存完钱,都要给交易次数+1
        count++;
    }

    //重写父类的取钱方法

    @Override
    public void withdraw(double amount) {
        //前三次不要手续费,也就是当前的取钱的次数 小于等于   取钱不收手续费的最大次数 ， 就不收手续费
        if (count < max_count) {
            super.setBalance(super.getBalance() - amount);
        } else {
            super.withdraw(amount);
        }
        //每次取完钱,都要给交易次数+1
        count++;
    }

    //每个月都会有利息产生,所以当利息产生之后,就代表这个月完了
    public void earnMonthInterest() {
        //最新的余额 = 原本余额 + 原本余额 * 利率
        super.setBalance(super.getBalance() + super.getBalance() * interest);
        //当利息产生的时候,并且已经加入到你的银行卡中,那么就代表这个月已经完了,那么就要重置交易次数
        this.count = 0;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public int getMax_count() {
        return max_count;
    }

    public void setMax_count(int max_count) {
        this.max_count = max_count;
    }
}
