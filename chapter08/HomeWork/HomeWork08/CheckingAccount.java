package javalh.chapter08.HomeWork.HomeWork08;

public class CheckingAccount extends BankAccount{
    //每次存款和取款的时候都收取1美元的手续费

    //手续费
    private double ServiceCharge = 1;

    public CheckingAccount(double initialBalance) {
        super(initialBalance);
    }

    //既然存款和取款都要收取1美元的手续费,那么就要重写存款和取款的方法


    @Override
    //这里使用父类的setBalance设置余额
    //最新余额 = 当前的初始化余额 + 存的钱 - 手续费
    public void deposit(double amount) {
        //存款，收取一元的手续费
       super.deposit(amount - ServiceCharge);
    }

    //重写取钱的方法
    //这里使用setBalance设置余额
    //最新余额 = 初始化余额 - 取的钱 - 手续费
    @Override
    public void withdraw(double amount) {
        //要多给银行手续费
        super.withdraw((amount + ServiceCharge));
//        super.setBalance(super.getBalance() - amount - ServiceCharge);
    }


    public double getServiceCharge() {
        return ServiceCharge;
    }

    public void setServiceCharge(double serviceCharge) {
        ServiceCharge = serviceCharge;
    }
}
