package javalh.chapter08.HomeWork.HomeWork08;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class HomeWork08OOP {

    /*
     * 思路分析
     * (1)先打印出菜单
     *       1：查看明细
     *       2：存钱
     *       3：取钱
     *       4：退出系统
     * (2)完成查看明细功能
     * (3)完成存钱功能
     * (4)完成取钱功能
     * (5)完成退出系统功能
     *
     * */

    //==============================定义属性================================

    //初始化银行卡余额
    private double balance = 50000;
    //存取明细
    private String details = "\n----------------存取明细--------------------";
    //目前是否要继续再ATM机子上进行操作
    private boolean flag = true;
    //当前选择项目
    private String key = "";
    //钱数
    private double amount = 0;
    //存钱时间
    Date date = null;
    //格式化时间
    SimpleDateFormat simp = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    //手续费
    private double ServiceCharge = 0;
    //利息
    private double interest = 0;
    //退出系统
    protected String is_exit = "";
    //初始化余额的方法
    SavingsAccount savingsAccount = new SavingsAccount(balance);


    //需要手动收入
    Scanner scanner = new Scanner(System.in);

    public HomeWork08OOP(double balance){
        this.balance = balance;
    }


    //主菜单的显示
    public void menu(){
        do {
            System.out.println("------------菜单项目-----------");
            System.out.println("\t\t\t 1：查看明细");
            System.out.println("\t\t\t 2：存钱");
            System.out.println("\t\t\t 3：取钱");
            System.out.println("\t\t\t 4：月结【初始化三次免费存取钱的机会】");
            System.out.println("\t\t\t 5：退出系统");
            System.out.print("请选择你要做的事情:");
            key = scanner.next();
            switch (key){
                //查看明细
                case "1":
                    details();
                    break;
                case "2":
                    deposit();
                    break;
                case "3":
                    withdraw();
                    break;
                case "4":
                    setCount();
                    break;
                case "5":
                    exit();
                default:
                    System.out.println("您输入的有误【1-5之间】");
            }
        }while (flag == true);
        System.out.println("您已经离开ATM机子！欢迎下次再来！");
    }


    //查看明细
    public void details(){
        System.out.println(this.details);
    }

    //存钱
    public void deposit(){
        System.out.print("请输入你的存钱数：");
        amount = scanner.nextDouble();
        savingsAccount.deposit(amount);
        //当把钱存进去之后,需要加入一条记录
        date = new Date();
        //获取手续费
        if(savingsAccount.getCount() > savingsAccount.getMax_count()){
            ServiceCharge = savingsAccount.getServiceCharge();
        }
        details += "\n 存入：" + amount + "\t时间：" + simp.format(date) + "\t手续费：" + ServiceCharge + "\t余额：" + savingsAccount.getBalance();
    }


    //取钱
    public void withdraw(){
        System.out.print("请输入您要取款的数额：");
        amount = scanner.nextDouble();
        savingsAccount.withdraw(amount);
        //当钱取出来的时候,需要加一条记录
        date = new Date();
        //获取手续费
        if(savingsAccount.getCount() > savingsAccount.getMax_count()){
            ServiceCharge = savingsAccount.getServiceCharge();
        }
        details += "\n 取出：" + amount + "\t时间：" + simp.format(date) + "\t手续费：" + ServiceCharge + "\t余额：" + savingsAccount.getBalance();
    }


    //初始化存取钱的手续费机会  初始化之后  就代表是三次了，但是初始化的时候  要加上这个月产生的利息
    public void setCount(){
        savingsAccount.earnMonthInterest();
        date = new Date();
        //获取利息
        interest = savingsAccount.getInterest();
        details += "\n 月初：" + "利息收益" + "\t时间：" + simp.format(date) + "\t利息：" + interest + "\t余额：" + savingsAccount.getBalance();
    }


    //退出系统
    public void exit(){
        //通过while循环确定用户收入的必须是y或者n
        while (true){
            System.out.println("您确定要退出系统吗[y/n]?");
            is_exit = scanner.next();
            //如果输入的值是 y 或者是 n 的话  就退出循环
            if("y".equals(is_exit) || "n".equals(is_exit)){
                break;
            }
        }
        if("y".equals(is_exit)){
            flag = false;
        }
    }


}
