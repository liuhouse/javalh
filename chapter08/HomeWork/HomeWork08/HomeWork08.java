package javalh.chapter08.HomeWork.HomeWork08;

import java.util.Scanner;

public class HomeWork08 {
    public static void main(String[] args) {
        /*
            //初始化现在的余额
            CheckingAccount checkingAccount = new CheckingAccount(10000);
            //存了一千元
            checkingAccount.deposit(1000);
            System.out.println("存钱之后的银行卡余额为:");
            System.out.println(checkingAccount.getBalance());

            //取了1000元
            checkingAccount.withdraw(1000);
            System.out.println("取钱之后的银行卡余额为:");
            System.out.println(checkingAccount.getBalance());
        */

        //做一个存钱和取钱的程序，并且打印出流水
        HomeWork08OOP homeWork08OOP = new HomeWork08OOP(5000);
        homeWork08OOP.menu();


    }
}
