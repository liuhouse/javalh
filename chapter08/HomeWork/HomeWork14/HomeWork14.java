package javalh.chapter08.HomeWork.HomeWork14;

public class HomeWork14 {
    public static void main(String[] args) {
        /*
        * 1：我是A类
        * 2: haha 我是B类的有参构造
        * 3: 我是C类的有参构造
        * 4: 我是C类的无参构造
        * */
        C c = new C();

    }
}



class A{//超类
    public A(){
        System.out.println("我是A类");
    }
}


class B extends A{//父类
    public B(){
        System.out.println("我是B类的无参构造");
    }
    public B(String name){
        System.out.println(name + "我是B类的有参构造");
    }
}

class C extends B{//子类
    public C(){
        this("Hello");
        System.out.println("我是C类的无参构造");
    }


    public C(String name){
        super("hahah");
        System.out.println("我是C类的有参构造");
    }
}


