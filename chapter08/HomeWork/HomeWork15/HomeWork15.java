package javalh.chapter08.HomeWork.HomeWork15;

public class HomeWork15 {
    public static void main(String[] args) {
        //什么是多态，多态的具体体现有哪些(可以举例说明)
        //多态：方法或者对象具有多种形态,是OOP的第三大特性,是建立在封装和继承的基础之上
        //多态的具体体现
        //1:方法多态
        //(1)重载体现多态   (2)重写体现多态

        //2:对象多态
        //(1)对象的编译类型和运行类型可以不一致,编译类型在定义时,就确定了下来,是不能再改变的
        //(2)对象的运行类型是可以变化的,可以通过getClass()来查看运行的类型
        //(3)编译类型看 = 的左边 , 运行类型看 = 的右边

        //对象的运行类型是可以随时变换的  但是变换一定要在适当的范围内变化  必须是父类   或者说是父类的子类
        //但是编译类型只要定好了 就始终不能变化了
        //举例说明
        AAA obj = new BBB();//向上转型
        AAA b1 = obj;
        System.out.println("obj的运行类型=" + obj.getClass());
        System.out.println("b1的运行类型=" + b1.getClass());
        obj = new CCC();//向上转型 -- 相当于之前 new BBB()的这条线断了   然后 obj 和  new CCC()这条线链接起来了
        System.out.println("obj的运行类型为=" + obj.getClass());
        obj = b1;
        System.out.println("obj的运行类型=" + obj.getClass());


    }
}

class AAA{}

class BBB extends AAA{}

class CCC extends BBB{}
