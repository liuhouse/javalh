package javalh.chapter08.HomeWork.HomeWork09;

public class Point {
    //x������
    private double x;
    //y������
    private double y;

    public Point(double x , double y){
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
