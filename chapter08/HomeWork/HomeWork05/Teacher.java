package javalh.chapter08.HomeWork.HomeWork05;

public class Teacher extends Employee{
    //上课的天数
    private int classDay;
    //上课一天的课酬
    private double classSal;

    public Teacher(String name, int age, double sal, int classDay, double classSal) {
        super(name, age, sal);
        this.classDay = classDay;
        this.classSal = classSal;
    }

    public int getClassDay() {
        return classDay;
    }

    public void setClassDay(int classDay) {
        this.classDay = classDay;
    }

    public double getClassSal() {
        return classSal;
    }

    public void setClassSal(double classSal) {
        this.classSal = classSal;
    }

    @Override
    //一年的基本工资  + 课酬
    public double printYearSal() {
        return super.printYearSal() + (classDay * classSal);
    }


}
