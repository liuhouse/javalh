package javalh.chapter08.HomeWork.HomeWork05;

public class HomeWork05 {
    public static void main(String[] args) {

        //固定的属性   可以在初始化对象的时候就进行赋值
        //但是不确定的属性  或者说   随时在变的属性  最好使用动态赋值  set  get


//        System.out.println("工人的全年工资为================");
//        Worker work = new Worker("work", 50, 5000);
        //工人一年发15个月的工资
        //经验总结,像这种不确定的属性,或者说是在随时变的属性,在实例化对象的时候就不要去操作,可以在后面根据实际的业务逻辑进行改变
        //当然是使用set  get   方法是最好的
//        work.setMonthDays(15);
//        System.out.println(work.printYearSal());
//        System.out.println("农民的全年工资为================");
//        Peasant peasant = new Peasant("peasant", 45, 3000);
//        System.out.println(peasant.printYearSal());
//        System.out.println("教师的全年工资为================");
//        Teacher hsp = new Teacher("hsp", 35, 10000, 200, 100);
//        System.out.println(hsp.printYearSal());
        System.out.println("科学家的全年工资为===============");
        Scientist ylp = new Scientist("ylp", 80, 20000);
        //设置科学家一年发几个月的工资
        ylp.setMonthDays(15);
        //给科学家设置奖金
        ylp.setBonus(1000000);
        System.out.println(ylp.printYearSal());
//        System.out.println("服务生的全年工资为===============");
//        Waiter waiter = new Waiter("waiter", 18, 2000);
//        System.out.println(waiter.printYearSal());

    }
}
