package javalh.chapter08.HomeWork.HomeWork05;

public class Employee {
    //姓名
    private String name;
    //年龄
    private int age;
    //收入
    private double sal;

    //一年发几个月的工资(有的是12薪,有的是13薪,有的是15薪,默认是12)
    //这个一年发几个月的工资是不需要在创建对象的时候初始化的,因为,每家公司的情况都是不一样的
    private int monthDays = 12;

    public Employee(String name, int age, double sal) {
        this.name = name;
        this.age = age;
        this.sal = sal;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSal() {
        return sal;
    }

    public void setSal(double sal) {
        this.sal = sal;
    }

    public int getMonthDays() {
        return monthDays;
    }

    public void setMonthDays(int monthDays) {
        this.monthDays = monthDays;
    }

    //打印全年工资
    public double printYearSal(){
        return monthDays * sal;
    }
}
