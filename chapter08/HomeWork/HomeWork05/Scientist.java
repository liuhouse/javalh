package javalh.chapter08.HomeWork.HomeWork05;

public class Scientist extends Employee{
    //年终奖
    private double bonus = 0;
    public Scientist(String name, int age, double sal){
        super(name, age, sal);
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    //打印全年的工资
    public double printYearSal() {
        return super.printYearSal() + bonus;
    }
}
