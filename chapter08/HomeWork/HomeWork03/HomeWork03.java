package javalh.chapter08.HomeWork.HomeWork03;

public class HomeWork03 {
    /*
    *需求:
    * 编写老师类
    * (1) 要求有属性 “姓名name” , "年龄 age" ,"职称 post" , "基本工资salary"
    * (2) 编写业务方法,introduce(),实现输出一个教师的信息
    * (3) 编写教师类的三个子类:教授类(Professor)，副教授类,讲师类。工资级别分别为:教授1.3,副教授1.2，讲师类1.1，
    *      在三个子类中都重写父类的introduce()方法
    * (4)定义并初始化一个老师对象,调用业务方法,实现对象的基本信息的后台打印
    * */

    public static void main(String[] args) {
        //打印教授
        System.out.println("============教授的信息==============");
        Professor professor = new Professor("jack", 60, "教授", 8000, 1.3);
        System.out.println(professor.introduce());
        System.out.println("============副教授信息==============");
        AssociateProfessor associateProfessor = new AssociateProfessor("fu_jack", 80, "副教授", 6000, 1.2);
        System.out.println(associateProfessor.introduce());
        System.out.println("============讲师的信息==============");
        Lecturer lecturer = new Lecturer("老韩", 35, "IT讲师", 10000, 1.1);
        System.out.println(lecturer.introduce());

    }

}




