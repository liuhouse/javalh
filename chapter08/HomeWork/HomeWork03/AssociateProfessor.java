package javalh.chapter08.HomeWork.HomeWork03;

public class AssociateProfessor extends Teacher{
    private double level;

    public AssociateProfessor(String name, int age, String post, double salary, double level) {
        super(name, age, post, salary);
        this.level = level;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    public String introduce() {
        return super.introduce() + " ���ʵȼ� \t" + level;
    }
}
