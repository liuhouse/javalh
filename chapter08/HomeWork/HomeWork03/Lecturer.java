package javalh.chapter08.HomeWork.HomeWork03;
//讲师类
public class Lecturer extends Teacher {
    private double level;

    public Lecturer(String name, int age, String post, double salary, double level) {
        //使用父类对子类进行初始化
        super(name, age, post, salary);
        //自己特有的属性自己重新赋值
        this.level = level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    public double getLevel() {
        return this.level;
    }

    public String introduce(){
        return super.introduce() + "工资等级\t" + level;
    }
}
