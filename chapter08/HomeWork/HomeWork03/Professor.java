package javalh.chapter08.HomeWork.HomeWork03;

public class Professor extends Teacher{
    private double level;

    public Professor(String name, int age, String post, double salary, double level) {
        super(name, age, post, salary);
        this.level = level;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    //重写父类的方法
    @Override
    public String introduce() {
        return super.introduce() + "工资级别\t" + level;
    }
}
