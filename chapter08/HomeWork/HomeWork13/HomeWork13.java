package javalh.chapter08.HomeWork.HomeWork13;

import java.util.Arrays;

public class HomeWork13 {
    public static void main(String[] args) {
        Teacher teacher = new Teacher("jack", '男', 20, 5);
        System.out.println(teacher.printInfo());
        System.out.println("--------------------------------------------");
        Student student = new Student("小明", '男', 18, "001002003");
        System.out.println(student.printInfo());

        Person p[] = new Person[4];
        p[0] = new Student("小明", '男', 26, "001002003");
        p[1] = new Student("小李", '女', 25, "001002001");
        p[2] = new Teacher("李老师", '男', 55, 5);
        p[3] = new Teacher("韩老师", '男', 35, 5);


        System.out.println("==============原始数组===========");
        for(int i = 0 ; i < p.length ; i++){
            System.out.println("age = " + p[i].getAge() + " name = " + p[i].getName());
        }

        //对数组进行排序
        Person temp = null;
        for(int i = 0 ; i < p.length-1 ; i++){//外层循环
            for(int j = 0 ; j < p.length - 1 - i ; j++){
                if(p[j].getAge() < p[j+1] .getAge()){
                    temp = p[j];
                    p[j] = p[j + 1];
                    p[j + 1] = temp;
                }
            }
        }

        System.out.println("==============排序后的数组===========");
        for(int i = 0 ; i < p.length ; i++){
            System.out.println("age = " + p[i].getAge() + " name = " + p[i].getName());
        }

        System.out.println("=========打印出用户的承诺信息===============");
        Person pno = new Person();
        for(int i = 0 ; i < p.length ; i++){
            System.out.println(pno.specialDO(p[i]));
        }


//
//
//        System.out.println("学生的特殊方法==============");
//
//        System.out.println(pno.specialDO(student));
//
//        System.out.println("老师的特殊方法==============");
//        System.out.println(pno.specialDO(teacher));
    }
}
