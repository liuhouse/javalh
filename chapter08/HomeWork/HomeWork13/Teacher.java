package javalh.chapter08.HomeWork.HomeWork13;
/*
* 教师类
* */
public class Teacher extends Person{
    private int work_age;

    public Teacher(String name, char sex, int age, int work_age) {
        super(name,sex,age);
        this.work_age = work_age;
    }

    //教师会有教书的方法
    public String teach(){
        return "我承诺,我会认真教学";
    }

    @Override
    public String play() {
        return super.play() + "象棋";
    }

    @Override
    public String printInfo() {
        return "老师的信息：\n" + super.printInfo()
                + "工龄：" + work_age + "\n"
                + this.teach() + "\n"
                + this.play()
                ;
    }


    public int getWork_age() {
        return work_age;
    }

    public void setWork_age(int work_age) {
        this.work_age = work_age;
    }


    @Override
    public String toString() {
        return "Teacher{" +
                "work_age=" + work_age +
                '}' + super.toString();
    }
}
