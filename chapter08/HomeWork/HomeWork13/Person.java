package javalh.chapter08.HomeWork.HomeWork13;
//父类
public class Person {
    private String name;
    private char sex;
    private int age;

    Person(){}

    public Person(String name, char sex, int age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

    //老师和学生都有一个玩的方法,所以定义在父类中
    public String play(){
        return this.name + "爱玩";
    }


    //分析出我们需要一个打印的方法  因为最后要返回学生或者老师的信息
    public String printInfo(){
        return   "姓名：" + this.name + "\n"
                +"年龄：" + this.age + "\n"
                + "性别：" + this.sex + "\n"
                ;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    //打印老师或者学生的特殊方法
    public String specialDO(Person p){
        if(p instanceof Teacher){
            Teacher t = (Teacher) p;
            return t.teach();
        }else if(p instanceof Student){
            return ((Student) p).study();
        }else{
            return "";
        }
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", sex=" + sex +
                ", age=" + age +
                '}';
    }
}
