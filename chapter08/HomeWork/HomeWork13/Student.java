package javalh.chapter08.HomeWork.HomeWork13;
/*
* 学生类
* */
public class Student extends Person{
    //学号
    private String stu_id;

    public Student(String name, char sex, int age, String stu_id) {
        super(name,sex,age);
        this.stu_id = stu_id;
    }

    //学习的方法
    public String study(){
        return "我承诺,我会好好学习";
    }

    @Override
    public String play() {
        return super.play() + "足球";
    }

    @Override
    public String printInfo() {
        return "学生的信息\n" + super.printInfo()
                + "学号：" + this.stu_id + "\n"
                + this.study() + "\n"
                + this.play()
                ;
    }


    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }


    @Override
    public String toString() {
        return "Student{" +
                "stu_id='" + stu_id + '\'' +
                '}' + super.toString();
    }
}
