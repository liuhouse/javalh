package javalh.chapter08.HomeWork.HomeWork10;

import javax.print.Doc;

public class Doctor {
    private String name;
    private int age;
    private String job;
    private char gender;
    private double sal;

    public Doctor(String name, int age, String job, char gender, double sal) {
        this.name = name;
        this.age = age;
        this.job = job;
        this.gender = gender;
        this.sal = sal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public double getSal() {
        return sal;
    }

    public void setSal(double sal) {
        this.sal = sal;
    }

    //重写父类的equals()方法
//    public boolean equals(Object o) {
//        //使用地址判断,如果两个对象的地址是一样的,那么就证明两个对象是相等的
//        if (this == o) {
//            return true;
//        }
//        //判断对象中的各个属性是不是相等,如果两个对象中的所有的属性都是相等的,那么就证明是一个对象,否则就证明
//        //不是一个对象
//        //因为传递过来的对象的编译类型是Object所以根本就获取不到这些属性,所以这里要使用到向下转型
//        if (o instanceof Doctor) {
//            //将传递过来的对象进行向下转型
//            Doctor newO = (Doctor) o;
//            if (this.name.equals(newO.name) &&
//                    this.age == newO.age &&
//                    this.job.equals(newO.job) &&
//                    this.gender == newO.gender &&
//                    this.sal == newO.sal
//            ) {
//                return true;
//            }
//            return false;
//        }
//        return false;
//
//    }


    public boolean equals(Object o){
        if(this == o) return true;
        if(!(o instanceof Doctor)) return false;
        Doctor doctor = (Doctor)o;
        return this.name.equals(doctor.name)&&
                this.age == doctor.age &&
                this.job.equals(doctor.job)&&
                this.gender == doctor.gender &&
                this.sal == doctor.sal;
    }

}
