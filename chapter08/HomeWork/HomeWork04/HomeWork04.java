package javalh.chapter08.HomeWork.HomeWork04;

public class HomeWork04 {
    public static void main(String[] args) {
        System.out.println("======普通员工的工资如下====");
        Worker jack = new Worker("jack", 50, 10, 1.0);
        System.out.println(jack.printSalary());
        System.out.println("==========管理员的工资如下====");
        Manager han = new Manager("老韩", 100, 20, 1.2);
        System.out.println(han.printSalary());
    }
}
