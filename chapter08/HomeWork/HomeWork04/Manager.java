package javalh.chapter08.HomeWork.HomeWork04;

//部门经理类
public class Manager extends Employee{
    private double level;

    public Manager(String name, double day_salary, int days, double level) {
        super(name, day_salary, days);
        this.level = level;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    @Override
    //经理  就是  奖金 + 基本工资
    public double printSalary() {
        return 1000 + super.printSalary()*level;
    }
}
