package javalh.chapter08.HomeWork.HomeWork04;

public class Worker extends Employee{
    private double level;

    public Worker(String name, double day_salary, int days, double level) {
        super(name, day_salary, days);
        this.level = level;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    @Override
    public double printSalary() {
        return super.printSalary() * level;
    }
}
