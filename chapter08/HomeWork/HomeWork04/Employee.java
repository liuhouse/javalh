package javalh.chapter08.HomeWork.HomeWork04;

//员工类
public class Employee {
    //姓名
    private String name;
    //单日工资
    private double day_salary;
    //工作天数
    private int days;

    public Employee(String name, double day_salary, int days) {
        this.name = name;
        this.day_salary = day_salary;
        this.days = days;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDay_salary() {
        return day_salary;
    }

    public void setDay_salary(double day_salary) {
        this.day_salary = day_salary;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    //打印工资
    public double printSalary(){
        return day_salary * days;
    }




}
