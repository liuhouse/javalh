package javalh.chapter08.HomeWork.HomeWork11;

public class HomeWork11 {
    public static void main(String[] args) {
        /*
        * 需求 ：
        * 现有Person类,里面有run,eat 方法   Student类继承了Person类,并重写了额run方法
        * 自定义了study方法,试着写出向上转型和向下转型的代码,并写出各自都可以调用哪些方法,
        * 并写出方法输出了什么
        * */
        /*
        * 核心  左边的类是编译类    右边的是运行类
        * */
        //向上转型,父类的引用指向子类的对象
        //向上转型只能访问编译类型里面的属性和方法
        //但是在具体访问的时候是就近原则
        //编译的时候是按照编译类型来进行编译的
        //访问的时候是根据运行类型来进行运行的
        Person p = new Student();
        p.eat();//Person eat
        p.run();//Student run

        //向下转型
        //就是将向上转型的对象向下转型  但是 此对象必须要跟要转型的类对应
        //编译类型是Student   运行类型也是Student
        //先在子类进行查找   如果在子类中查找到了  就不会向父类继续查找了   如果在子类中没有找到  需要继续
        //从父类中进行查找
        Student s = (Student)p;
        s.run();//Student run
        s.study();//Student study
        s.eat();//Person eat

    }
}
