package javalh.chapter08.HomeWork;

public class HomeWork01 {
    public static void main(String[] args) {
        /*
         * 需求 定义一个Person类,{name,age.job},初始化Person对象数组,有3个person对象,并按照age从大到小进行排序,提示,使用冒泡排序法
         * */
        Person person_arr[] = new Person[3];
        //给数组的第一项赋值
        person_arr[0] = new Person("jack1", 125, "学生");
        //给数组的第二项赋值
        person_arr[1] = new Person("jack22222", 300, "程序员");
        //给数组的第三项赋值
        person_arr[2] = new Person("jack333", 50, "教师");

        //使用冒泡排序法进行排序
        Person p = new Person();
        p.sortBubble(person_arr);
        System.out.println("最终得到的数组的信息=====");
        for (int i = 0; i < person_arr.length; i++) {
            //System.out.println("姓名:" + person_arr[i].getName() + "年龄：" + person_arr[i].getAge() + "工作：" + person_arr[i].getJob());
            System.out.println(person_arr[i]);
        }
    }
}


class Person {
    private String name;
    private int age;
    private String job;

    public Person() {

    }

    public Person(String name, int age, String job) {
        this.name = name;
        this.age = age;
        this.job = job;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }




    //使用冒泡排序法进行排序
    public void sortBubble(Person person_arr[]) {
        for (int i = 0; i < person_arr.length - 1; i++) {
            for (int j = 0; j < person_arr.length - 1 - i; j++) {
                //定义临时变量保存人员信息
                Person tmp_person = person_arr[j];
                //如果第一个人的年龄比第二个人的年龄小
                //按照年龄从大到小排
//                if (person_arr[j].getAge() < person_arr[j + 1].getAge()) {
//                    person_arr[j] = person_arr[j + 1];
//                    person_arr[j + 1] = tmp_person;
//                }

                //按照姓名的长度从小到大进行排列
                if(person_arr[j].getName().length() > person_arr[j + 1].getName().length()){
                    person_arr[j] = person_arr[j + 1];
                    person_arr[j + 1] = tmp_person;
                }
            }
        }
    }





    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", job='" + job + '\'' +
                '}';
    }
}
