package javalh.chapter08.debug_;

import java.util.Arrays;

public class Debug04 {
    /*
    * 演示如何执行到下一个断点 F9 resume
    * 老韩小技巧：断点可以在debug的过程中,动态的下断点
    * 下断点的作用是   判断是否可以执行到下一个断点    以及在这个断点中间执行了哪些代码    结果是什么  用作业务逻辑的判断
    * */
    public static void main(String[] args) {
        /*
         * 演示如何追溯原码,看看java的设计者是怎么实现的(提高编程思想)
         * 小技巧,将光标放在某个变量上,可以看到最新的数据
         * */
        int arr[] = {1,-1,10,-20,100};
        //我们看看Arrays.sort方法底层的实现 ->Debug
        Arrays.sort(arr);
        for(int i = 0 ; i < arr.length ; i++){
            System.out.print(arr[i] + "\t");
        }

        System.out.println("hello100");
        System.out.println("hello200");
        System.out.println("hello300");
        System.out.println("hello400");
        System.out.println("hello500");
        System.out.println("hello600");
        System.out.println("hello700");
        System.out.println("hello800");
        System.out.println("hello900");
    }
}
