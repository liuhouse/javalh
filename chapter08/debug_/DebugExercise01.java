package javalh.chapter08.debug_;

public class DebugExercise01 {
    public static void main(String[] args) {
        //初始化流程
        /*
        * (1):加载Person类，加载里面的属性  name = null   age = 0     , 加载到 实际参数   name : jack  age : 100
        * (2)第一次赋值的时候   name = null   age = 0
        * (3)接着第二次赋值的时候  将实际参数进行赋值  name = jack  age = 100
        * (4)赋值完成之后返回到实例化他的位置
        * (5)进行打印  打印对象的时候  默认调用了toString方法
        * */
        Person jack = new Person("jack", 100);
        System.out.println(jack);
    }
}

class Person{
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}