package javalh.chapter08.debug_;

import java.util.Arrays;

public class Debug03 {
    public static void main(String[] args) {
        /*
        * 演示如何追溯原码,看看java的设计者是怎么实现的(提高编程思想)
        * 小技巧,将光标放在某个变量上,可以看到最新的数据
        * */
        int arr[] = {1,-1,10,-20,100};
        //我们看看Arrays.sort方法底层的实现 ->Debug
        Arrays.sort(arr);
        for(int i = 0 ; i < arr.length ; i++){
            System.out.print(arr[i] + "\t");
        }
    }
}
