package javalh.chapter16.tankgame2;

import javax.swing.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HspTankGame02 extends JFrame {
    //在画框中定义画板
    MyPanel mp = null;

    public static void main(String[] args) {
        new HspTankGame02();
    }


    //使用构造器进行初始化
    public HspTankGame02(){
        //对画板进行初始化
        mp = new MyPanel();
        //将画板加载到画框中
        this.add(mp);
        //想要坦克动起来,窗口就必须作为监听者来监听键盘事件对象
        this.addKeyListener(mp);
        //设置画板的大小
        this.setSize(1000,750);
        //设置点击关闭X的时候退出程序
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置画板可视
        this.setVisible(true);
    }

}
