package javalh.chapter16.tankgame2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

/**
 * @author 刘皓
 * @version 1.0
 * 坦克大战的绘图区域
 * 画板
 */

/*
* 总结几个点
* (1):想要改变坦克的方向,就要在坦克的父类做一个方向的变量,然后在进行事件处理的时候,来改变这个变量的反向值
* (2):每次改变方向之后,只要是键盘一直按着,那么就要修改坦克的坐标
* (3):想要坦克移动,就必须在窗口上加上对面板的监听事件
* (4):进行重绘repaint()
* (5):跟坦克相关的公共属性和方法,都写在坦克的父类,然后在创建坦克的时候就定好坦克的速度
* */

//现在我们需要让坦克动起来,那么这里我们就要相应键盘的事件,实现事件监听接口
public class MyPanel extends JPanel implements KeyListener {
    //定义我的坦克
    Hero hero = null;
    //定义敌方坦克的数量
    int enemyTankSize = 3;

    //(1):因为敌人的坦克,是在MyPanel上,所以我们的代码写在MyPanel
    //(2):因为敌人的坦克,后面有自己的特殊属性和方法,可以单开一个EnemyTank
    //(3):因为敌人的坦克数量多,可以放入集合Vector中,因为考虑到多线程问题
    Vector<EnemyTank> enemyTanks = new Vector<>();

    //构造器
    public MyPanel(){
        //初始化自己的坦克
        hero = new Hero(100,100);
        hero.setSpeed(5);
        for (int i = 0 ; i < enemyTankSize ; i++){
            //创建一个敌方的坦克
            EnemyTank enemyTank = new EnemyTank(((i + 1) * 100), 0);
            //设置方向，因为这些都是和坦克相关的,所以在实例化坦克的时候,就要设置好坦克的方向
            enemyTank.setDirect(2);
            //将坦克加入集合中(多线程)
            enemyTanks.add(enemyTank);
        }
    }

    //画笔
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //填充矩形,并填充颜色,作为画板的区域
        //默认为黑色
        g.fillRect(0,0,1000,750);

        //画出我方坦克-封装方法
        drawTank(hero.getX(), hero.getY(),g,hero.getDirect(),1);

        //画出敌方坦克
        for(int i = 0 ; i < enemyTanks.size(); i++){
            EnemyTank emTank = enemyTanks.get(i);
            drawTank(emTank.getX(),emTank.getY(),g,emTank.getDirect(),0);
        }
    }

    //编写方法,画出坦克
    /**
     *
     * @param x     坦克的左上角x坐标
     * @param y     坦克的左上角y坐标
     * @param g     画笔
     * @param direct    坦克方向(上下左右)
     * @param type  坦克类型
     */
    public void drawTank(int x , int y ,Graphics g, int direct , int type){
        //根据不同坦克的类型,设置不同的颜色
        switch (type){
            case 0://敌人的坦克
                g.setColor(Color.cyan);
                break;
            case 1://自己的坦克
                g.setColor(Color.yellow);
                break;
        }

        //根据坦克的方向,来绘制坦克
        //direct 表示方向(0:向上 1：向右 2:向下 3：向左)
        switch (direct){
            case 0://表示向上
                g.fill3DRect(x,y,10,60,false);//画出左边的轮子
                g.fill3DRect(x+30,y,10,60,false);//画出右边的轮子
                g.fill3DRect(x+10,y+10,20,40,false);//画出坦克的身体
                g.fillOval(x+10,y+20,20,20);//画出坦克的盖子
                g.drawLine(x+20,y,x+20,y+30);//画出炮筒
                break;
            case 1://表示向右
                g.fill3DRect(x,y,60,10,false);//画出上面的轮子
                g.fill3DRect(x,y+30,60,10,false);//画出下面的轮子
                g.fill3DRect(x+10,y+10,40,20,false);//画出坦克的身体
                g.fillOval(x+20,y+10,20,20);//画出坦克盖子
                g.drawLine(x+30,y+20,x+60,y+20);//画出炮筒
                break;
            case 2://表示向下
                g.fill3DRect(x,y,10,60,false);//画出左边的轮子
                g.fill3DRect(x+30,y,10,60,false);//画出右边的轮子
                g.fill3DRect(x+10,y+10,20,40,false);//画出坦克的身体
                g.fillOval(x+10,y+20,20,20);//画出坦克的盖子
                g.drawLine(x+20,y+20,x+20,y+60);//画出炮筒
                break;
            case 3://表示向左
                g.fill3DRect(x,y,60,10,false);//画出上面的轮子
                g.fill3DRect(x,y+30,60,10,false);//画出下面的轮子
                g.fill3DRect(x+10,y+10,40,20,false);//画出坦克的身体
                g.fillOval(x+20,y+10,20,20);//画出坦克盖子
                g.drawLine(x,y+20,x+30,y+20);//画出炮筒
                break;
            default:
                System.out.println("暂时没有处理");
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    /*
        1：这里我们主要的是相应键盘按下的事件
        2：我们使用 wdsa  w(上) d(右) s(下) a(左)
        3：然后修改方向即可 direct
        4：方向可以改变之后,就要使得坦克可以移动,如何移动?
            (1)：在相应事件的时候,对对应的事件的坐标进行修改  ++ 或者 --
     */

    @Override
    public void keyPressed(KeyEvent e) {
        //向上
        if(e.getKeyCode() == KeyEvent.VK_W){
            hero.setDirect(0);
            hero.moveUp();
        }else if(e.getKeyCode() == KeyEvent.VK_D){
            hero.setDirect(1);
            hero.moveRight();
        }else if(e.getKeyCode() == KeyEvent.VK_S){
            hero.setDirect(2);
            hero.moveDown();
        }else if(e.getKeyCode() == KeyEvent.VK_A){
            hero.setDirect(3);
            hero.moveLeft();
        }
        //相应到了事件,画板上的内容也就发生了变化,那么这个时候就要重新绘制面板,这样坦克才能动起来
        this.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
