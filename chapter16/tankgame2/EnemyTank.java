package javalh.chapter16.tankgame2;

/**
 * @author 刘皓
 * @version 1.0
 * 敌方坦克
 */
public class EnemyTank extends Tank{
    public EnemyTank(int x, int y) {
        //使用父类的构造方法进行初始化
        super(x, y);
    }
}
