package javalh.chapter16.tankgame2;

/**
 * @author 刘皓
 * @version 1.0
 * 定义一个坦克父类,以后所有的坦克继承父类来生成子坦克
 */
public class Tank {
    private int x;//坦克的横坐标
    private int y;//坦克的纵坐标
    private int direct;//坦克的方向,这里默认是0
    private int speed = 1;//坦克的移动速度

    public Tank(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /*
    * 在坦克的父类,定义四个方法,分别用作坦克的不同方向的移动,这样只要是继承了父类的坦克,都可以进行直接调用
    * */

    //向上移动
    public void moveUp(){
        y -= speed;
    }

    //向右移动
    public void moveRight(){
        x += speed;
    }

    //向下移动
    public void moveDown(){
        y += speed;
    }

    //向左移动
    public void moveLeft(){
        x -= speed;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDirect() {
        return direct;
    }

    public void setDirect(int direct) {
        this.direct = direct;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
