package javalh.chapter16.tankgame;

import javax.swing.*;
import java.awt.*;

/**
 * @author 刘皓
 * @version 1.0
 * 坦克大战的绘图区域
 * 画板
 */
public class MyPanel extends JPanel {
    //定义我的坦克
    Hero hero = null;

    //构造器
    public MyPanel(){
        //初始化自己的坦克
        hero = new Hero(100,100);
    }

    //画笔
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //填充矩形,并填充颜色,作为画板的区域
        //默认为黑色
        g.fillRect(0,0,1000,750);

        //画出坦克-封装方法
        drawTank(hero.getX(), hero.getY(),g,0,0);
    }

    //编写方法,画出坦克

    /**
     *
     * @param x     坦克的左上角x坐标
     * @param y     坦克的左上角y坐标
     * @param g     画笔
     * @param direct    坦克方向(上下左右)
     * @param type  坦克类型
     */
    public void drawTank(int x , int y ,Graphics g, int direct , int type){
        //根据不同坦克的类型,设置不同的颜色
        switch (type){
            case 0://我们的坦克
                g.setColor(Color.cyan);
                break;
            case 1://敌人的塔克
                g.setColor(Color.yellow);
                break;
        }

        //根据坦克的方向,来绘制坦克
        switch (direct){
            case 0://表示向上
                g.fill3DRect(x,y,10,60,false);//画出左边的轮子
                g.fill3DRect(x+30,y,10,60,false);//画出右边的轮子
                g.fill3DRect(x+10,y+10,20,40,false);//画出坦克的身体
                g.fillOval(x+10,y+20,20,20);//画出坦克的盖子
                g.drawLine(x+20,y,x+20,y+30);
                break;
            default:
                System.out.println("暂时没有处理");
        }

    }

}
