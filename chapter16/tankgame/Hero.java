package javalh.chapter16.tankgame;

/**
 * @author 刘皓
 * @version 1.0
 * 自己的坦克
 */
public class Hero extends Tank{
    public Hero(int x , int y){
        //使用父类构造器进行初始化
        super(x,y);
    }
}
