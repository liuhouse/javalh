package javalh.chapter16;

import javax.swing.*;
import java.awt.*;

/*
* 演示如何在面板上画出图形
* */
@SuppressWarnings({"all"})
//JFrame对应窗口,可以理解成是一个画框
public class DrawCircle extends JFrame{



    //在画框上定义一个画板
    private MyPanel mp = null;


    public static void main(String[] args) {
        new DrawCircle();
        System.out.println("退出程序~~~");
    }

    //构造器
    public DrawCircle(){
        //初始化面板 - 准备好画板
        mp = new MyPanel();
        //给画框里加入面板
        this.add(mp);
        //设置窗口的大小
        this.setSize(900,660);
        //当点击窗口的x的时候,程序完全退出
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //可以显示
        this.setVisible(true);
    }
}


//1：先定义一个MyPanel【画板类】，继承JPanel类,画图形,就在面板上画
class MyPanel extends JPanel{
    /*
    * 说明：
    * 1：MyPanel对象就是一个画板
    * 2: Graphics g 把g理解成一支画笔
    * 3:Graphics 提供了很多绘图的方法
    * */

    //首绘制图形,窗口变大变小的时候会被调用
    @Override
    public void paint(Graphics g) {
        super.paint(g);//调用父类的方法完成初始化
//        System.out.println("paint方法被调用");
//        System.out.println(g.getFont());
        //画出一个圆形
        //g.drawOval(10,10,100,100);

        //绘制不同的图形
        //画一条直线 drawLine(int x1 , int y2, int x2 , int y2)
        //g.drawLine(10,10,100,10);

        //画矩形边框 drawRect(int x , int y , int width , int height)
        //g.drawRect(10,10,200,150);

        //画出椭圆边框
        //g.drawOval(20,20,200,150);

        //设置填充颜色
        //g.setColor(Color.red);
        //填充矩形
        //g.fillRect(10,10,200,100);

        //填充椭圆
        //g.setColor(Color.GREEN);
        //g.fillOval(50,50,200,180);

        //画图片 drawImage(Image img ,int x , int y,...)
        //1:获取图片资源,/bg.png表示该项目的跟目录去获取bg.png的图片资源
        //这里的加载资源应该放在当前类的同级目录下面,加载图片的时候使用当类加载
        Image image = Toolkit.getDefaultToolkit().getImage(DrawCircle.class.getResource("bg.png"));
        System.out.println(image);
        g.drawImage(image,10,10,395,610,this);

        //画字符串 drawString(String str,int x , int y)写字
        //给画笔设置颜色和字体
        //这里设置的 20,50 是 “北京欢迎你” 的左下角
//        g.setColor(Color.RED);
//        g.setFont(new Font("隶书",Font.BOLD,40));
//        g.drawString("北京欢迎你",20,50);

        //设置笔画的字体  setFont(Font font) 传递一个Font对象
        //设置笔画的颜色  setColor(Color c) 传递一个Color 对象

        //region -- 绘制一个坦克
        //(1)绘制左边的矩形
        g.setColor(Color.blue);
        g.fillRect(50,50,10,60);
        //(2)绘制右边的矩形
        g.fillRect(85,50,10,60);
        //(3)绘制中间的矩形
        g.setColor(Color.cyan);
        g.fillRect(60,65,25,35);
        //(4)绘制一个圆
        g.setColor(Color.MAGENTA);
        g.fillOval(62,72,20,20);
        //(5)绘制炮筒
        g.setColor(Color.RED);
        g.drawLine(72,45,72,72);

        //endregion


    }
}
