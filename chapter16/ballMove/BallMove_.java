package javalh.chapter16.ballMove;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author 刘皓
 * @version 1.0
 * 演示小球通过键盘控制上下左右的移动 -> 讲解Java的事件控制
 *
 * 总结：
 * (1):想要绘图,必须继承JFrame类，这是一个窗口类
 * (2):必须定义一个画板类,画板里面的paint(Graphics g)方法,用作画图
 * (3):g代表的是画板,可以用g[画笔]画出各式各样的图形
 * (4):要在窗口中使用面板,定义面板,然后在构造方法中实例化面板,在实例化的时候,已经调用了paint()方法,绘图已经完成
 * (5):将面板加入画框,设置画框的大小,设置关闭的效果,设置图形可视化
 * (6):想要实现键盘事件,需要在画板上实现KeyListener(键盘监听接口),并实现里面的方法
 * (7):想要在窗口的画板上使用,就要给窗口加上这个面板的键盘监听事件
 * (8):当图像的位置发生变化的时候,需要调用repaint()对图像进行重绘
 */

/*
* 事件处理机制的深入理解
* (1):前面我们提到的几个重要的概念,事件源,事件,事件监听器我们下面来全面的介绍他们
* (2):事件源：事件源是一个产生事件的对象,比如,按钮,窗口等
* (3):事件：事件就是承载事件源状态改变的时候的对象,比如当键盘事件,鼠标对象,窗口事件等等
*       会生成一个事件对象,该对象保存着当前事件的很多信息,比如KeyEvent对象有含有被按下的Code值
*       ,java.awt.event和javax.swing.event包中定义了各种事件类型
* (4):事件类型：查询jdk文档
* (5):
*   1:当事件源产生一个事件,可以传送给事件监听者进行处理
*   2:事件监听者实际上就是一个类,该类实现了某个事件监听器接口,比如我们前面案例中的MyPanel就是一个类，它实现了
*       KeyListener接口,他就可以作为一个事件监听者,对接收到的事件进行处理
*   3：事件监听器接口有多种,不同的事件监听器接口可以监听不同的事件,一个类可以实现多个监听接口
*   4:这些接口在java.awt.event包和javax.swing.event包中定义,列出常用的事件监听器接口,查看jdk文档
* */

//(1)需要面板,所以继承JFrame
public class BallMove_ extends JFrame {//窗口
    MyPanel mp = null;

    //构造方法
    public BallMove_(){
        //对面板进行初始化
        mp = new MyPanel();
        //给当面画框加入画板
        this.add(mp);
        //设置窗口的大小
        this.setSize(1000,600);
        //窗口JFrame对象可以监听键盘事件,即可以监听到面板发生的键盘事件
        this.addKeyListener(mp);
        //设置点击X的时候退出程序
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置窗口可视
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new BallMove_();
    }


}

//画板,可以画出小球
//KeyListener是监听器,可以监听键盘事件
class MyPanel extends JPanel implements KeyListener {
    int x = 10;
    int y = 10;
    //绘制小球
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //绘制小球
        g.fillOval(x,y,20,20);//默认为黑色的小球
    }

    //在有字符输出的时候,就会触发
    @Override
    public void keyTyped(KeyEvent e) {

    }

    //当某个键按下的时候,就会触发
    @Override
    public void keyPressed(KeyEvent e) {
        //所以,我们这里需要的是触发这个事件,按下的时候小球就会移动
        System.out.println((char) e.getKeyCode() + "被按下");
        //根据用户按下的不同的键,来处理移动的小球(上下左右的键)
        //在java中,会给每一个键,分配一个值
        //向下  y轴增加  ++
        //向上  y轴减少  --
        //向左  x减少    --
        //向右  x增加    ++

        if(e.getKeyCode() == KeyEvent.VK_DOWN){//KeyEvent.VK_DOWN就是向下的箭头对应的code
            y++;
        }else if(e.getKeyCode() == KeyEvent.VK_UP){
            y--;
        }else if(e.getKeyCode() == KeyEvent.VK_LEFT){
            x--;
        }else if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            x++;
        }
        //绘制完成之后,要重新的绘制面板
        this.repaint();
    }


    //当某个键释放(松开),的时候,该方法会触发
    @Override
    public void keyReleased(KeyEvent e) {

    }
}
