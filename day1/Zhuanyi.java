public class Zhuanyi{
	public static void main(String[] args){
		// \t一制表位   \n 换行符 \\ 一个\ \" 一个" \' 一个' \r一个回车
		// System.out.println("韩顺平教育\r\n北京");
		//  \t 一个制表位,实现对齐的功能
		System.out.println("北京\t上海\t天津");
		//换行符
		System.out.println("jack\nnsmith\nlihao");
		//一个 \
		System.out.println("C:\\windows\\System32\\\\cmd.exe");
		//一个 "
		System.out.println("老韩说:\"我们要好好的学习java,有前途\"");
		//一个 '
		System.out.println("老韩说:\'我们要好好的学习java,有前途\'");
		//一个回车
		System.out.println("韩顺平教育\r\n北京");
	}
}