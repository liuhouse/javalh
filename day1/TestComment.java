/**
 * @author  刘皓
 * @version  1.0
 */
public class TestComment{
	public static void main(String[] args){
		//下面这句话是输出"hello world"
		System.out.println("hello world");
		/*
			注释1
			注释2
		 */
		System.out.println("Hello world 01");
		System.out.println("Hello world 02");
		System.out.println("Hello world 03");
	}
}