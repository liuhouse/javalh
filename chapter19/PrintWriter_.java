package javalh.chapter19;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * @author 刘皓
 * @version 1.0
 */
public class PrintWriter_ {
    public static void main(String[] args) throws IOException {
        //应用实例,打印只有输出流,没有输入流
//        PrintStream
//        PrintWriter

        PrintWriter printWriter = new PrintWriter(System.out);
        printWriter.println("哇哦我熬");
        printWriter.close();//flush + 关闭流 ， 才会将数据写入到文件
        //System.out 的编译类型是PrintStream

        /*
             public void print(String s) {
                write(String.valueOf(s));
             }

        * */


        //PrintStream out = System.out;

        //因为print的底层使用的是write，所以我们可以直接调用write进行打印输出
//        out.print("john,hello");
        //out.write("韩顺平你好".getBytes());

//        out.close();

        //我们可以去修改打印流输出的位置/设备
        //1:输出修改成到 "e:\\f1.txt"
        //2:hello,韩顺平教育 就会输出到e:\\f1.txt
        //3:

        /*
            public static void setOut(PrintStream out) {
                checkIO();
                setOut0(out);
            }
        * */

        //在默认情况下 , PrintStream输出数据的位置是标准输出,就是显示器
        //将输出的对象从默认的显示器到文件中
        System.setOut(new PrintStream("e:\\f1.txt"));
        System.out.println("hello,韩顺平教育~");
    }
}
