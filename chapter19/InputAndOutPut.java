package javalh.chapter19;

/**
 * @author 刘皓
 * @version 1.0
 */
public class InputAndOutPut {
    public static void main(String[] args) {
        //编译类型 InputStream 输入流 - 标准输入
        //System.in;
        //BufferedInputStream 运行类型  -- 默认设备是键盘
        //System.out.println(System.in.getClass());

        //编译类型 PrintStream 打印流 - 标准输出
        //运行类型 PrintStream -- 默认设备是显示器
        System.out.println(System.out.getClass());
    }
}
