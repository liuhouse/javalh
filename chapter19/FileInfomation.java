package javalh.chapter19;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class FileInfomation {
    public static void main(String[] args) {
        /*
        * 应用案例演示
        * 如何获取到文件的大小,文件名,路径,父File，是文件还是目录(目录的本质也是文件,一种特殊的文件)是否存在
        * */
    }


    @Test
    public void info(){
        //getName   getAbsolutePath , getParent , length   exists   isFile   isDirectory
        //先创建文件对象
        File file = new File("e:\\news5.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //调用相应的方法,得到对应的信息
        System.out.println("文件名字=" + file.getName());
        //得到文件的绝对路径
        System.out.println("文件的绝对路径=" + file.getAbsolutePath());
        //该文件的父级目录
        System.out.println("文件的父级目录=" + file.getParent());
        //获取文件的大小(字节)
        System.out.println("文件大小(字节) = " + file.length());
        //文件是否存在
        System.out.println("文件是否存在 = " + file.exists());
        //是不是一个文件
        System.out.println("是不是一个文件 = " + file.isFile());
        //是不是一个目录
        System.out.println("是不是一个目录 = " + file.isDirectory());
    }


    /**
     * 目录的操作和文件的删除
     *
     */
    @Test
    public void info01(){
        //mkdir创建单级目录
//        String path = "e:\\aass";
//        File file = new File(path);
//        file.mkdir();

        //创建多级目录
//        String paths = "e:\\cc\\dd\\ee";
////        File file = new File(paths);
////        file.mkdirs();



    }


    /*
    * 应用案例演示
    *
    * */
    @Test
    public void info2(){
      //(1)判断 e:\\news1.txt是不是存在,如果存在就删除
//      String file1 = "e:\\news1.txt";
//        File file_obj = new File(file1);
//        if(file_obj.exists()){
//            file_obj.delete();
//            System.out.println(file1 + "文件删除成功");
//        }else{
//            System.out.println("文件不存在");
//        }

        //(2)判断d:\\demo02是否存在,存在就删除,否则就提示不存在
//        String file2 = "e:\\demo02";
//        File file2_obj = new File(file2);
//        if(file2_obj.exists()){
//            file2_obj.delete();
//            System.out.println(file2 + "目录删除成功");
//        }else{
//            System.out.println("目录不存在");
//        }

        //(3)判断e:\\cc\\dd\\ee目录是否存在,如果存在就提示已经存在,否则就创建
        String file3 = "e:\\cc\\dd\\ee5";
        File file_obj3 = new File(file3);
        if(!file_obj3.exists()){
            file_obj3.mkdirs();
            System.out.println(file3 + "目录创建成功");
        }else{
            System.out.println("目录已经存在了");
        }

    }

}
