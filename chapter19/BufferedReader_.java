package javalh.chapter19;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class BufferedReader_ {
    public static void main(String[] args) throws IOException {
        /*
        * 处理流的功能主要体现在下面的两个方面
        * 1:性能的提高：主要以增加缓冲的方式来提高输入输出的效率
        * 2：操作的便捷,处理流可能提供了一系列的方法来一次输入或者输出大批量的数据,使用更加的灵活方便
        *
        * 处理流 BufferedReader 和 BufferedWriter
        * BufferedReader 和 BufferedWriter  是属于字符流的,按照字符来读取数据的
        * 关闭时处理流,只需要关闭外层流即可
        * */

        String filePath = "e:\\store.txt";
        //创建bufferedReader
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        //读取
        String line;//按行读取,效率高
        //说明
        //1.bufferedReader.readLine() 是按行读取文件,效率是比较高的
        //2.当返回null的时候,就代表文件都已经读取完毕[参考手册来看]
        //告诉这个流是否支持mark()操作
            System.out.println(bufferedReader.markSupported());
        bufferedReader.mark(10);
        while ((line = bufferedReader.readLine()) != null){
            System.out.println(line);
        }


        //关闭流,这里注意,只需要关闭 BufferedReader , 因为底层会去自动的关闭 节点流
        //通过阅读源码会发现,其实最后还是调用的是FileReader.close()
        /*

             public void close() throws IOException {
                    synchronized (lock) {
                        if (in == null)
                            return;
                        try {
                            in.close();
                        } finally {
                            in = null;
                            cb = null;
                        }
                    }
                }



        * */
        bufferedReader.close();


    }
}
