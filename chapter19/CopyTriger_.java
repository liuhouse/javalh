package javalh.chapter19;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class CopyTriger_ {
    public static void main(String[] args) {
        /*
        * 将e:\\laohu.jpg 复制到  e:\\laohu1.jpg
        * 思路分析:
        * （1）创建输入流对象,读取图片数据到内存中
        * （2）创建输出流对象,将读取到的图片的数据保存到本地中
        * */

        String srcPath = "e:\\laohu.jpg";
        String destPath = "e:\\laohu1.jpg";

        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;

        try {
            fileInputStream = new FileInputStream(srcPath);
            fileOutputStream = new FileOutputStream(destPath,true);
            int readLen = 0;
            byte[] buf = new byte[1024];
            while ((readLen = fileInputStream.read(buf)) != -1){
                //边读边写
                fileOutputStream.write(buf,0,readLen);
            }
            System.out.println("文件拷贝成功...");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭文件和资源
            try {
                if(fileInputStream != null){
                    fileInputStream.close();
                }
                if(fileOutputStream != null){
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
