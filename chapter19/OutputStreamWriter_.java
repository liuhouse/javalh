package javalh.chapter19;

import java.io.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class OutputStreamWriter_ {
    public static void main(String[] args) throws IOException {

        /*
        * 方法说明：
        * (1)FileOutputStream : 用于写入诸如图像数据的原始字节流.
        * (2)OutputStreamWriter : 是字符的桥梁流以字节流：向其写入的字符编码成使用字节的charset
        *                         字节流转化为字符流
        * */

        /*
        * 应用案例
        * 2.编程将字节流 FileOutputStream 包装成(转换成)字符流OutputStreamWriter
        * 对文件进行写入(按照gbk格式,可以指定其他,比如utf-8)
        * */
        //1:创建流对象
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("e:\\aa.txt"),"gbk");
        //为了最大效率,请考虑使用BufferedWriter包装一个OutputStreamWriter
        BufferedWriter bufferedWriter = new BufferedWriter(osw);
        //2:写入
        bufferedWriter.write("hello,韩顺平教育111111awas".toCharArray());
        //3:关闭外层流
        bufferedWriter.close();
    }
}
