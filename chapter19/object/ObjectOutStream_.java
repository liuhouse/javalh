package javalh.chapter19.object;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ObjectOutStream_ {
    public static void main(String[] args) throws Exception {
        /*
         * 应用案例
         * 1.使用ObjectOutPutStream序列化,基本数据类型和一个Dog对象(name,age),并保存到data.dat文件中
         * 完成对数据的序列化
         *
         * */

        //序列化后,保存的文件格式,不是存文本,而是按照他的格式来进行保存
        String filePath = "e:\\data.dat";
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath));

        //序列化数据到 e:\\data.dat
        oos.writeInt(100);//int -> Integer(实现了Serializable)
        oos.writeBoolean(true);//boolean ->Boolean(实现了Serializable)
        oos.writeChar('a');//char -> Character(实现了Serializable)
        oos.writeDouble(9.5);//double -> Double(实现了Serializable)
        oos.writeUTF("韩顺平教育");//String(实现了Serializable)
        //保存一个dog对象 , 这里可能会报一个错,是因为想要将一个数据序列化写进去,就必须实现Serializable接口
        oos.writeObject(new Dog("旺财11111",10,"中国","红色"));
        //这里的Master没有实现序列化接口,所以是会报错的,解决方法,实现序列化接口即可
        oos.writeObject(new Master());
        System.out.println("文件保存成功(序列化形式)~~");
        //保存完成之后关闭文件
        oos.close();

    }
}


