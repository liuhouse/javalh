package javalh.chapter19.object;

import java.io.Serializable;


//一个狗类
public class Dog implements Serializable {
    private String name;
    private int age;
    private static int SerialVersionUID = 1;

    //使用static修饰的成员不能被序列化
    private static String zh;
    //使用transient修饰的成员不能被序列化
    private transient String color;

    public Dog(String name, int age,String zh , String color) {
        this.name = name;
        this.age = age;
        this.zh = zh;
        this.color = color;
    }


    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age + '\'' +
                ",zh = " + zh + '\'' +
                ", color = " + color +
                '}';
    }
}
