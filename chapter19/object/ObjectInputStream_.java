package javalh.chapter19.object;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ObjectInputStream_ {
    public static void main(String[] args) throws Exception {
        /*
        * 创建流对象
        * 这里必须按照顺序进行读取
        *
        * */


        /*
        * 注意事项和细节说明
        * (1):读写顺序要一致,顺序不一致的话,会报错,会找不到
        * (2):要求序列化或反序列化对象,要实现Serializable
        * (3):序列化的类中建议添加SerialVersionUID,为了提高版本的兼容性
        * (4):序列化对象的时候,默认将里面所有的属性都进行了序列化，但是除了static或者transient修饰的成员
        * (5):序列化对象的时候,要求里面的属性类型也必须实现序列化接口
        * (6):序列化具备可继承性,也就是如果某类已经实现了序列化,则它的所有的子类已经默认实现了序列化
        *
        * */



        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("e:\\data.dat"));
        //读取的时候注意顺序
        System.out.println(ois.readInt());
        System.out.println(ois.readBoolean());
        System.out.println(ois.readChar());
        System.out.println(ois.readDouble());
        System.out.println(ois.readUTF());
        //相同包下面的默认访问修饰符是可以访问到的
        System.out.println(ois.readObject());
        //只要是实现了序列化接口的话,都是可以进行序列化的,并且按照顺序是可以进行读取的
        System.out.println(ois.readObject());

        //关闭文件
        ois.close();

        System.out.println("以反序列化的方式读取(恢复)ok~~~");
    }
}
