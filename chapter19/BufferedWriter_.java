package javalh.chapter19;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class BufferedWriter_ {
    public static void main(String[] args) throws IOException {
        /*
        * 需求,将hello,韩顺平教育,写入到文件中
        * BufferedWriter的使用
        * */

        //说明
        //1.new FileWriter(filePath,true)表示以追加的方式写入
        //2.new FileWriter(filePATH) 表示以覆盖的方式写入

        //这里一定要明白一点,这里所说的覆盖并不是每一次调用了write方法的时候的覆盖,节点不在这里    而是每次启动程序的时候
        //都会首先将原本文件里面的东西进行删除操作   然后将新增加的内容全部写入到文件里面

        //声明文件的路径
        String filePath = "e:\\ok.txt";
        //创建BufferedWriter对象

        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePath,true));
        bufferedWriter.write("hello,韩顺平教育");
        //默认写入的数据是不会换行的,想要换行就要使用 bufferedWriter.newLine()进行换行
        bufferedWriter.newLine();//插入一个和系统相关的换行
        bufferedWriter.write("hello1，韩顺平教育");

        //关闭文件
        //说明：关闭外层流即可,传入的new FileWriter(filePATH),会在底层进行关闭
        bufferedWriter.close();


    }
}
