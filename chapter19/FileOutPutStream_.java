package javalh.chapter19;

import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 * 输出流
 */
public class FileOutPutStream_ {
    public static void main(String[] args) {
        /*
        * FileOutPutStream应用实例
        * 要求：请使用FileOutPutStream在a.txt文件中,写入 "hello,world"，如果文件不存在,就会创建文件
        * (注意：前提是目录已经存在)
        * */
    }


    //写文件
    @Test
    public void writeFile(){
        //创建FileOutputStream对象
        //定义文件路径
        String filePath = "e:\\a.txt";
        FileOutputStream fileOutputStream = null;
        try {
            //创建FileOutputStream对象
            //老师说明
            //1:new FileOutputStream(filePath)，创建方式,当写入内容是,会覆盖原来的内容
            //2:new FileOutputStream(filePath,true)创建方式,当写入内容的时候,会追加到文件的后面
            fileOutputStream = new FileOutputStream(filePath,true);

            //写入一个字节
            //fileOutputStream.write('B');

            //写入字符串
            String str = "hs1p,world";
            //可以写入byte类型的数组
            //str.getBytes() 将字符串转化成为byte类型的数组  字节数组
            //fileOutputStream.write(str.getBytes());

            //write(byte[] b , int off , int len)将len字节从位于偏移量 off的指定字节数组写入此文件输出流
            //fileOutputStream.write(str.getBytes(),0,3);
            fileOutputStream.write(str.getBytes(),0,str.length());




            System.out.println("写入文字成功");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
