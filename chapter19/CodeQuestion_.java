package javalh.chapter19;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 * 看一个中文乱码的问题
 */
public class CodeQuestion_ {
    public static void main(String[] args) throws IOException {
        //读取 e:\\a.txt文件到程序
        //思路
        //1: 创建字符输入流 BufferedReader[处理流]
        //2: 使用BufferedReader对象读取a.txt
        //3: 默认情况下,读取文件是按照utf-8的编码
        //4: 如果换一种编码的话,就会出现乱码的情况

        //定义文件路径
        String filePath = "e:\\a.txt";
        //实例化读取文件的处理流
        BufferedReader br = new BufferedReader(new FileReader(filePath));

        //读取文件，只读取一行即可
        String s = br.readLine();
        System.out.println("读取到的内容：" + s);
        br.close();

    }
}
