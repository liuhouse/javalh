package javalh.chapter19;

import java.io.*;

/**
 * @author 刘皓
 * @version 1.0
 * 演示使用InputStreamReader转换流解决中文乱码问题
 * 将字节流FileInputStream转成字符流 InputStreamReader,指定编码 gbk / utf-8
 */
public class InputStreamReader_ {
    public static void main(String[] args) throws IOException {
        String filePath = "e:\\a.txt";
        //1:把FileInputStream转成InputStreamReader 将字节流文件转成字符流
        //2:指定编码gbk
        //InputStreamReader isr = new InputStreamReader(new FileInputStream(filePath), "Unicode");
        //3:把 InputStreamReader 传入 BufferedReader
        //BufferedReader br = new BufferedReader(isr);


        //使用的方法说明
        /*
        * (1)FileInputStream 用于读取诸如图像数据的原始字节流
        * (2)InputStreamReader 是从字节流到字符流的桥,他读取字节,并使用指定的charset将其解码为字符
        * (3)BufferedReader 从字符输入流获取文本,缓冲字符,以提供字符,数组和行的高效读取
        * */

        //将2和3合并在一起
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "Unicode"));
        //4:读取
        String s = br.readLine();
        System.out.println("读取的内容 = " + s);
        //5：关闭外层流
        br.close();

    }
}
