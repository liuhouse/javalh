package javalh.chapter19;

import java.io.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class BufferedCopy02_ {
    public static void main(String[] args) {
        /**
         * 要求：编程完成图片/音乐的拷贝(要求使用Buffered...流)
         * 演示使用BufferedOutputStream 和 BufferedInputStream使用
         * 使用他们,可以完成对二进制文件的拷贝
         * 思考：字节流可以操作二进制文件,可以操作文本文件吗?当然可以
         */
        /*
        * 这边总结一下
        * 操作文本文件的时候使用  BufferedInputReader 和 BufferedOutReader 操作文本文档效率比较高
        * 操作图片,视频,pdf,等等二进制文件的时候使用BufferedInputStream和BufferedOutStream操作
        * */

        //我们先来复制图片

//        String srcFilePath = "e:\\kaola.jpg";
//        String destFilePath = "e:\\liuhouse.jpg";

        //拷贝视频
        String srcFilePath = "http://tyxy.hpu.edu.cn/tyweb/wmv/hpu.wmv";
        String destFilePath = "e:\\hpu.mp4";

        //拷贝文本文件
//        String srcFilePath = "e:\\store.txt";
//        String destFilePath = "e:\\store02.txt";

        //创建BufferedOutputStream对象和BufferedInputStream对象
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        //创建输入和输出流的BufferStream对象
        try {
            //为什么可以是 FileInputStream  因为是InputStream的子类
            bis = new BufferedInputStream(new FileInputStream(srcFilePath));
            bos = new BufferedOutputStream(new FileOutputStream(destFilePath));

            //循环的读取文件,并且一边读取一边写入文件
            byte[] buff = new byte[1024];
            int readLen = 0;

            //当返回-1的时候,就代表文件已经读取完毕
            while ((readLen = bis.read(buff)) != -1){
                bos.write(buff,0,readLen);
            }
            System.out.println("文件拷贝完毕~~~");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //文件读取完毕之后,一定要进行关闭操作,因为在关闭文件的时候才真正的将数据写入新文件
            try {
                if(bis != null){
                    bis.close();
                }

                if(bos != null){
                    bos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
