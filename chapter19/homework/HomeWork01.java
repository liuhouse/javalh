package javalh.chapter19.homework;

import java.io.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01 {
    public static void main(String[] args) throws IOException {
        /*
        * (1)判断e盘下面是否有文件夹mytemp,如果没有就创建mytemp
        * (2)在e:\\mytemp目录下,创建文件hello.txt
        * (3)如果hello.txt已经存在,提示该文件已经存在,就不需要再重复创建了
        * (4)并且再hello.txt文件中,写入hello,world
        * */
        String dir_path = "e:\\mytemp";
        String file_path = "hello.txt";
        String all_path = dir_path +"\\"+file_path;
        File parentFile = new File(dir_path);
        //创建文件夹
        if(!parentFile.exists()){
            parentFile.mkdir();
        }

        //创建文件
        File filePath = new File(parentFile, file_path);
        if(!filePath.exists()){
            System.out.println("文件还不存在,准备进行创建");
            filePath.createNewFile();
        }else{
            System.out.println("文件已经存在,不必再重复创建~~~");
        }

        //给文件中写入hello,world
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(all_path));
        bufferedWriter.write("hello,world".toCharArray());
        bufferedWriter.close();
        System.out.println("文字写入成功");


    }
}
