package javalh.chapter19.homework;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01_1 {
    public static void main(String[] args) throws IOException {

        /*
         * (1)判断e盘下面是否有文件夹mytemp,如果没有就创建mytemp
         * (2)在e:\\mytemp目录下,创建文件hello.txt
         * (3)如果hello.txt已经存在,提示该文件已经存在,就不需要再重复创建了
         * (4)并且再hello.txt文件中,写入hello,world
         * */

        //目录的路径
        String dir = "e:\\youtemp";
        //文件的路径
        String path = dir + "\\hello.txt";

        //判断文件夹是不是存在
        File dirObj = new File(dir);
        if(!dirObj.exists()){
            dirObj.mkdirs();
            System.out.println("创建文件夹" + dir + "成功");
        }

        //判断文件是不是存在
        File file = new File(path);
        if(!file.exists()){
            //如果文件不存在就进行创建
            if(file.createNewFile()){
                System.out.println("文件" + path + "创建成功");
                //如果文件创建成功,给文件中写入数据，使用输出流
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path));
                bufferedWriter.write("hello,world韩顺平教育");
                bufferedWriter.close();
                System.out.println("写入内容成功");
            }else{
                System.out.println("文件" + path + "创建失败");
            }
        }else{
            System.out.println("文件" + path + "已经存在");
        }

    }
}
