package javalh.chapter19.homework;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork03 {
    public static void main(String[] args) {
        /*
        * 编程题
        * (1):要求编写一个dog.properties
        * name=tom
        * age=5
        * color=red
        * (2)编写Dog类(name,age,color)创建一个dog对象,读取dog.properties用相应的内容完成属性初始化,并输出
        * (3)将创建的Dog对象,序列化到文件dog.dat文件
        * 所以,有了编程思想,然后知识点只需要学习就行了,然后基础牢固,框架什么的都简单
        * */

        //实例化Properties
        Properties properties = new Properties();
        //设置字段，和值
        properties.setProperty("name","tom");
        properties.setProperty("age","5");
        properties.setProperty("color","red");
        //保存到文件中
        try {
            //输出流
            properties.store(new FileOutputStream("javalh\\chapter19\\homework\\dog.properties"),null);
            System.out.println("数据保存成功");

            //读取配置文件的信息
            properties.load(new FileReader("javalh\\chapter19\\homework\\dog.properties"));
            properties.list(System.out);
            //读取各个的属性值
            System.out.println(properties.getProperty("name"));
            System.out.println(properties.getProperty("color"));
            System.out.println(properties.getProperty("age"));

            //创建一个dog对象
            //编写Dog类(name,age,color)创建一个dog对象,读取dog.properties用相应的内容完成属性初始化,并输出
            Dog dog = new Dog(properties.getProperty("name"), properties.getProperty("age"), properties.getProperty("color"));
            System.out.println(dog);

            //进行文件的序列化
            String ser_file = "javalh\\chapter19\\homework\\dog.dat";
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(ser_file));
            oos.writeObject(dog);
            System.out.println("序列化文件保存成功~~~");
            //只有挂芭比文件的时候才会保存数据
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

    }

    @Test
    public void readDog(){
        ObjectInputStream objectInputStream = null;
        try {
            //这里需要注意的细节是,当类中的文件发生了变化的时候,首先要去指定原本的类的创建的代码
            //加载文件
            objectInputStream = new ObjectInputStream(new FileInputStream("javalh\\chapter19\\homework\\dog.dat"));
            //这边进行反序列化,而在读取里面具体属性值的时候,需要使用向下转型
            Dog o = (Dog) objectInputStream.readObject();
            System.out.println(o.getName());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                objectInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
