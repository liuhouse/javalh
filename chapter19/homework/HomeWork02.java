package javalh.chapter19.homework;

import java.io.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork02 {
    public static void main(String[] args) throws IOException {
        /*
        * 要求：使用BufferedReader读取一个文本文件,为每行加上行号
        * 再连同内容输出到屏幕上
        * */
        String file_name = "e:\\store.txt";
        //更换编码之后就会出现乱码的现象
        //BufferedReader bufferedReader = new BufferedReader(new FileReader(file_name));


        //FileInputStream => 字节输入流 ， 将文件以字节的形式写入
        //InputStreamReader => 字节转化为字符  称为转化流
        //BufferedReader => 将转化流 使用包装流进行包装  然后使用包装流对应的方法进行读取接口
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file_name), "utf-8"));
        //每次读取一行
        String readLen;
        int line = 1;
        while ((readLen = bufferedReader.readLine()) != null){
            System.out.println(line + " " + readLen);
            line++;
        }
        bufferedReader.close();
    }
}
