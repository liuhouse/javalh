package javalh.chapter19;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class FileCopy_ {
    public static void main(String[] args) {
        /*
        * 要求：编程完成图片/音乐的拷贝
        * 完成文件拷贝,将e:\\kaola.jpg拷贝到e盘重新命名
        *
        * 思路分析
        * (1):创建文件的输入流,将文件读入到程序
        * (2):创建文件的输出流,将读取到的文件数据,写入到指定的文件中
        * */

        //原始图片的地址
        String srcFilePath = "e:\\kaola.jpg";
        //新图片的的地址
        String destFilePath = "e:\\kaola1.jpg";
        //创建输入流对象
        FileInputStream fileInputStream = null;
        //创建输出流对象
        FileOutputStream fileOutputStream = null;


        try {
            //实例化输入流对象
            fileInputStream = new FileInputStream(srcFilePath);
            //实例化输出流对象,因为我们读取文件的时候是要进行追加的,所以使用第二个参数true
            fileOutputStream = new FileOutputStream(destFilePath,true);
            //进行文件的读读取
            //定义一个字节数组,提高读取效果
            byte[] buf = new byte[1024];
            int readLen = 0;
            //没有读取到-1的时候,就代表没有到达文件的末尾
            while((readLen = fileInputStream.read(buf)) != -1){
                //读取到文件后,将写入文件,通过fileOutputStream
                //即,是一边读,一边写
                fileOutputStream.write(buf,0,readLen);
            }
            System.out.println("文件拷贝成功");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭输入流和输出流，释放资源
            try {
                if(fileInputStream != null){
                    fileInputStream.close();
                }
                if(fileOutputStream != null){
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }


}
