package javalh.chapter19;

import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class FileInputStream_ {
    public static void main(String[] args) {
        /*
          要求：请使用FileInputStream读取hello.txt文件,并且将文件内容显示到控制台
         */
    }


    /*
    * 演示读取文件
    * 单个字节的读取,效率比较低,使用字节读取文件的时候会发生中文乱码的现象
    * */
    @Test
    public void readFile01(){
        //需要读取文件的路径
        String filePath = "e:\\hello.txt";
        //读取数据的索引
        int readData = 0;
        //常见读取文件的对象初始化
        FileInputStream fileInputStream = null;
        try {

            //创建FileInputStream对象,用于读取文件
            //创建FileInputStream对象,用于读取文件中的内容
             fileInputStream = new FileInputStream(filePath);
             //使用这种方法一次只能读取一个字节,所以这里要使用while循环来进行读取
            //从该输入流读取一个字节的数据,如果没有输入可用,此方法将阻止
            //如果返回-1,表示读取完毕
            while ((readData = fileInputStream.read()) != -1){
                System.out.print((char)readData);//转换成char类型显示
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //到最后一定是要关闭文件流资源的,不然会造成资源浪费
            //关闭文件流,释放资源
            try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /*
    * 使用read(byte[] b) 读取文件,提高效率
    * */
    @Test
    public void readFile02(){
        String filePath = "e:\\hello.txt";
        //字节数组
        byte[] buf = new byte[8];//一次读取8个字节
        int readLen = 0;
        //初始化读取文件的对象
        FileInputStream fileInputStream1 = null;

        try {
            //创建FileInputStream对象,用于读取文件
             fileInputStream1 = new FileInputStream(filePath);
             //从该输入流读取最多b.length字节的数据到字节数组,此方法将堵塞,直到某些输入可用
            //如果返回-1.表示读取完毕
            //如果读取正常,返回实际读取的字节数
            //当一次性读取byte[]的多个数据的时候    其实是将取出来的数据给了byte[] 类型的数组
            while ((readLen = fileInputStream1.read(buf)) != -1){
                //将读取出来八个字符数组转化为字符串
                System.out.println(new String(buf,0,readLen));//显示
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭文件流,释放资源
            try {
                fileInputStream1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
