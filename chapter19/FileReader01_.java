package javalh.chapter19;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class FileReader01_ {
    public static void main(String[] args) {
        /**
         * FileReader相关方法
         * (1)new FileReader(File/String)
         * (2)read:每次读取单个字符,返回该字符,如果到达文件的末尾返回-1
         * (3)read(char[]):批量读取多个字符到数组,返回读取到的字符数,如果到达文件的末尾返回-1
         * 相关API
         * (1)new String(char[]):将char[]转化为String
         * (2)new String(char[],off,len):将char[]的指定部分转换成String
         */
    }


    /**
     * 一个需求 使用FileReader从story.txt读取内容,并显示
     */

    @Test
    //单个字符读取文件
    public void readFile01() {
        String filePath = "e:\\store.txt";
        FileReader fileReader = null;
        int data = 0;
        try {
            fileReader = new FileReader(filePath);
            //循环读取,单个字符读取,不等于-1,代表还没有到达文件的末尾
            while ((data = fileReader.read()) != -1) {
                //如果还没有到达文件的末尾,也就是不等于-1的话,就打印出当前读取到的字符
                System.out.print((char) data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //最后是一定要关闭文件的
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /*
     * 字符数组读取文件
     * */
    @Test
    public void readFile02() {
        System.out.println("~~~readFile02~~~");
        String filePath = "e:\\store.txt";
        FileReader fileReader = null;

        int readLen = 0;
        //根据数组来进行读取字符串,每个字符串都是由一个一个的字符组成的
        char[] buf = new char[8];

        //1:创建一个FileReader对象
        try {
            fileReader = new FileReader(filePath);
            //循环读取 使用read(buf),返回的是实际读取到的字符数
            //如果返回的是-1，说明文件结束
            while ((readLen = fileReader.read(buf)) != -1) {
                //将读取到的字符数全部打印出来
                System.out.print(new String(buf, 0, readLen));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭文件
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
