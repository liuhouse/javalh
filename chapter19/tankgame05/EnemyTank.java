package javalh.chapter19.tankgame05;

import java.util.Vector;

/**
 * @author 刘皓
 * @version 1.0
 * 敌方坦克
 * 因为是敌方坦克要发射子弹,所以要在敌方坦克的类上做子弹的相关操作
 */
//敌人的坦克是多个的,每个自己都是运行自己的路线,所以需要使用线程
public class EnemyTank extends Tank implements Runnable{
    //在敌人坦克类,使用Vector,保存多个Shot，因为是多个子弹,所以需要使用多线程的集合来保存
    Vector<Shot> shots = new Vector<>();
    //增加成员,EnemyTank 可以得到敌人坦克的Vector[所有的敌人坦克]
    //分析
    //1. Vector<EnemyTank> 所有的敌人坦克
    Vector<EnemyTank> enemyTanks = new Vector<>();
    //默认子弹都是活着的
    boolean isLive = true;

    public EnemyTank(int x, int y) {
        //使用父类的构造方法进行初始化
        super(x, y);
    }

    //设置所有的敌人坦克在坦克的属性中,用作比较
    //这里提供一个方法,可以将MyPanel的成员Vector<EnemyTank> enemyTanks = new Vector<>()
    //保存到 EnemyTank的成员 enemyTanks 里面
    public void setEnemyTanks(Vector<EnemyTank> enemyTanks) {
        this.enemyTanks = enemyTanks;
    }

    //编写方法,判断当前的这个敌人坦克,是否和enemyTanks中的其他坦克发生重叠或者碰撞
    //0 => 上  1 => 右  2=> 下  3=> 左
    public boolean isTouchEnemyTank(){
        //判断当前坦克(this)的方向
        switch (this.getDirect()){
            //当前坦克的方向是向上的
            case 0://上
                //让当前敌人坦克和其他的所有的敌人坦克比较
                for (int i = 0 ; i < enemyTanks.size() ; i++){
                    //从Vector所有的敌人坦克中取出一个坦克
                    EnemyTank enemyTank = enemyTanks.get(i);
                    //因为所有的敌人坦克中是包含自己的坦克的,但是自己的坦克不和自己比较
                    if(enemyTank != this){
                        //================== 敌方坦克上下方向,当前坦克上 start================================
                        /*
                            分析一下
                            只要当前坦克的左上角或者右上角进入敌人坦克区域，就代表碰撞了
                            //所以要分情况,分别判断当前的坦克的左上角和右上角面对不同坦克方向的位置是否碰撞或者重叠
                            1:如果敌人的坦克是上下的情况
                            x的范围 [enemyTank.getX() , enemyTank.getX() + 40]
                            y的范围 [enemyTank.getY() , enemyTank.getY() + 60]
                         */
                        if(enemyTank.getDirect() == 0 || enemyTank.getDirect() == 2){
                            //2.当前坦克"左上角"的坐标 [this.getX() , this.getY()]
                            //代表当前坦克的"左上角"已经进入敌方坦克区域
                            if(this.getX() >= enemyTank.getX()
                                    && this.getX() <= enemyTank.getX() + 40
                                    && this.getY() >= enemyTank.getY()
                                    && this.getY() <= enemyTank.getY() + 60
                            ){
                                return true;
                            }

                            //当前坦克"右上角"的坐标 [this.getX()+40 , this.getY()]
                            //代表当前坦克的"右上角的坐标"已经进入敌方坦克区域
                            if(this.getX() + 40 >= enemyTank.getX()
                                    && this.getX() + 40 <= enemyTank.getX() + 40
                                    && this.getY() >= enemyTank.getY()
                                    && this.getY() <= enemyTank.getY() + 60
                            ){
                                return true;
                            }
                        }

                        //================ 敌方坦克上下方向,当前坦克上 end=======================

                        //================ 敌方坦克左右方向,当前坦克上 start=====================
                        if(enemyTank.getDirect() == 1 || enemyTank.getDirect() == 3){
                            //当前坦克"左上角"的坐标[this.getX() , this.getY()]
                            //代表当前坦克的"左上角的坐标"已经进入敌方坦克区域
                            if(this.getX() >= enemyTank.getX()
                                    && this.getX() <= enemyTank.getX() + 60
                                    && this.getY() >= enemyTank.getY()
                                    && this.getY() <= enemyTank.getY() + 40
                            ){
                                return true;
                            }

                            //3.当前坦克"右上角"的坐标[this.getX() + 40 , this.getY()]
                            //代表当前坦克的"右上角的坐标"已经进入了敌方坦克区域
                            if(this.getX() + 40 >= enemyTank.getX()
                                    && this.getX() + 40 <= enemyTank.getX() + 60
                                    && this.getY() >= enemyTank.getY()
                                    && this.getY() <= enemyTank.getY() + 40
                            ){
                                return true;
                            }
                        }

                        //================ 敌方坦克左右方向,当前坦克上 end=======================
                    }
                }
                break;
            case 1://右
                //让当前敌人坦克和其他的所有的敌人坦克比较
                for (int i = 0 ; i < enemyTanks.size() ; i++){
                    //从Vector所有的敌人坦克中取出一个坦克
                    EnemyTank enemyTank = enemyTanks.get(i);
                    //因为所有的敌人坦克中是包含自己的坦克的,但是自己的坦克不和自己比较
                    if(enemyTank != this){
                        //================== 敌方坦克上下方向,当前坦克上 start================================
                        /*
                            分析一下
                            只要当前坦克的左上角或者右上角进入敌人坦克区域，就代表碰撞了
                            //所以要分情况,分别判断当前的坦克的左上角和右上角面对不同坦克方向的位置是否碰撞或者重叠
                            1:如果敌人的坦克是上下的情况
                            x的范围 [enemyTank.getX() , enemyTank.getX() + 40]
                            y的范围 [enemyTank.getY() , enemyTank.getY() + 60]
                         */
                        if(enemyTank.getDirect() == 0 || enemyTank.getDirect() == 2){
                            //2.当前坦克"右上角"的坐标 [this.getX() + 60 , this.getY()]
                            //代表当前坦克的"右上角"已经进入敌方坦克区域
                            if(this.getX() + 60 >= enemyTank.getX()
                                    && this.getX() + 60 <= enemyTank.getX() + 40
                                    && this.getY() >= enemyTank.getY()
                                    && this.getY() <= enemyTank.getY() + 60
                            ){
                                return true;
                            }

                            //当前坦克"右下角"的坐标 [this.getX()+60 , this.getY() + 40]
                            //代表当前坦克的"右下角的坐标"已经进入敌方坦克区域
                            if(this.getX() + 60>= enemyTank.getX()
                                    && this.getX() + 60 <= enemyTank.getX() + 40
                                    && this.getY() + 40 >= enemyTank.getY()
                                    && this.getY() + 40 <= enemyTank.getY() + 60
                            ){
                                return true;
                            }
                        }

                        //================ 敌方坦克上下方向,当前坦克上 end=======================

                        //================ 敌方坦克左右方向,当前坦克上 start=====================
                        if(enemyTank.getDirect() == 1 || enemyTank.getDirect() == 3){
                            //当前坦克"右上角"的坐标[this.getX() + 60 , this.getY()]
                            //代表当前坦克的"右上角的坐标"已经进入敌方坦克区域
                            if(this.getX() + 60 >= enemyTank.getX()
                                    && this.getX() + 60 <= enemyTank.getX() + 60
                                    && this.getY() >= enemyTank.getY()
                                    && this.getY() <= enemyTank.getY() + 40
                            ){
                                return true;
                            }

                            //3.当前坦克"右下角"的坐标[this.getX() + 60 , this.getY() + 40]
                            //代表当前坦克的"右下角的坐标"已经进入了敌方坦克区域
                            if(this.getX() + 60 >= enemyTank.getX()
                                    && this.getX() + 60 <= enemyTank.getX() + 60
                                    && this.getY() + 40 >= enemyTank.getY()
                                    && this.getY() + 40 <= enemyTank.getY() + 40
                            ){
                                return true;
                            }
                        }

                        //================ 敌方坦克左右方向,当前坦克上 end=======================
                    }
                }
                break;
            case 2://下
                //让当前敌人坦克和其他的所有的敌人坦克比较
                for (int i = 0 ; i < enemyTanks.size() ; i++){
                    //从Vector所有的敌人坦克中取出一个坦克
                    EnemyTank enemyTank = enemyTanks.get(i);
                    //因为所有的敌人坦克中是包含自己的坦克的,但是自己的坦克不和自己比较
                    if(enemyTank != this){
                        //================== 敌方坦克上下方向,当前坦克上 start================================
                        /*
                            分析一下
                            只要当前坦克的左上角或者右上角进入敌人坦克区域，就代表碰撞了
                            //所以要分情况,分别判断当前的坦克的左上角和右上角面对不同坦克方向的位置是否碰撞或者重叠
                            1:如果敌人的坦克是上下的情况
                            x的范围 [enemyTank.getX() , enemyTank.getX() + 40]
                            y的范围 [enemyTank.getY() , enemyTank.getY() + 60]
                         */
                        if(enemyTank.getDirect() == 0 || enemyTank.getDirect() == 2){
                            //2.当前坦克"左下角"的坐标 [this.getX() , this.getY() + 60]
                            //代表当前坦克的"右上角"已经进入敌方坦克区域
                            if(this.getX()>= enemyTank.getX()
                                    && this.getX()<= enemyTank.getX() + 40
                                    && this.getY() + 60 >= enemyTank.getY()
                                    && this.getY() + 60<= enemyTank.getY() + 60
                            ){
                                return true;
                            }

                            //当前坦克"右下角"的坐标 [this.getX()+40 , this.getY() + 60]
                            //代表当前坦克的"右下角的坐标"已经进入敌方坦克区域
                            if(this.getX() + 40>= enemyTank.getX()
                                    && this.getX() + 40 <= enemyTank.getX() + 40
                                    && this.getY() + 60 >= enemyTank.getY()
                                    && this.getY() + 60 <= enemyTank.getY() + 60
                            ){
                                return true;
                            }
                        }

                        //================ 敌方坦克上下方向,当前坦克上 end=======================

                        //================ 敌方坦克左右方向,当前坦克上 start=====================
                        if(enemyTank.getDirect() == 1 || enemyTank.getDirect() == 3){
                            //当前坦克"右上角"的坐标[this.getX() , this.getY() + 60]
                            //代表当前坦克的"右上角的坐标"已经进入敌方坦克区域
                            if(this.getX() >= enemyTank.getX()
                                    && this.getX() <= enemyTank.getX() + 60
                                    && this.getY() + 60 >= enemyTank.getY()
                                    && this.getY() + 60 <= enemyTank.getY() + 40
                            ){
                                return true;
                            }

                            //3.当前坦克"右下角"的坐标[this.getX() + 40 , this.getY() + 60]
                            //代表当前坦克的"右下角的坐标"已经进入了敌方坦克区域
                            if(this.getX() + 40 >= enemyTank.getX()
                                    && this.getX() + 40 <= enemyTank.getX() + 60
                                    && this.getY() + 60 >= enemyTank.getY()
                                    && this.getY() + 60 <= enemyTank.getY() + 40
                            ){
                                return true;
                            }
                        }
                        //================ 敌方坦克左右方向,当前坦克上 end=======================
                    }
                }
                break;
            case 3://左
                //让当前敌人坦克和其他的所有的敌人坦克比较
                for (int i = 0 ; i < enemyTanks.size() ; i++){
                    //从Vector所有的敌人坦克中取出一个坦克
                    EnemyTank enemyTank = enemyTanks.get(i);
                    //因为所有的敌人坦克中是包含自己的坦克的,但是自己的坦克不和自己比较
                    if(enemyTank != this){
                        //================== 敌方坦克上下方向,当前坦克上 start================================
                        /*
                            分析一下
                            只要当前坦克的左上角或者右上角进入敌人坦克区域，就代表碰撞了
                            //所以要分情况,分别判断当前的坦克的左上角和右上角面对不同坦克方向的位置是否碰撞或者重叠
                            1:如果敌人的坦克是上下的情况
                            x的范围 [enemyTank.getX() , enemyTank.getX() + 40]
                            y的范围 [enemyTank.getY() , enemyTank.getY() + 60]
                         */
                        if(enemyTank.getDirect() == 0 || enemyTank.getDirect() == 2){
                            //2.当前坦克"左上角"的坐标 [this.getX() , this.getY()]
                            //代表当前坦克的"左上角"已经进入敌方坦克区域
                            if(this.getX()>= enemyTank.getX()
                                    && this.getX() <= enemyTank.getX() + 40
                                    && this.getY() >= enemyTank.getY()
                                    && this.getY() <= enemyTank.getY() + 60
                            ){
                                return true;
                            }

                            //当前坦克"左下角"的坐标 [this.getX() , this.getY() + 40]
                            //代表当前坦克的"右下角的坐标"已经进入敌方坦克区域
                            if(this.getX()>= enemyTank.getX()
                                    && this.getX() <= enemyTank.getX() + 40
                                    && this.getY() + 40 >= enemyTank.getY()
                                    && this.getY() + 40 <= enemyTank.getY() + 60
                            ){
                                return true;
                            }
                        }

                        //================ 敌方坦克上下方向,当前坦克上 end=======================

                        //================ 敌方坦克左右方向,当前坦克上 start=====================
                        if(enemyTank.getDirect() == 1 || enemyTank.getDirect() == 3){
                            //当前坦克"左上角"的坐标[this.getX() , this.getY()]
                            //代表当前坦克的"右上角的坐标"已经进入敌方坦克区域
                            if(this.getX() >= enemyTank.getX()
                                    && this.getX() <= enemyTank.getX() + 60
                                    && this.getY() >= enemyTank.getY()
                                    && this.getY() <= enemyTank.getY() + 40
                            ){
                                return true;
                            }

                            //3.当前坦克"左下角"的坐标[this.getX() , this.getY() + 40]
                            //代表当前坦克的"右下角的坐标"已经进入了敌方坦克区域
                            if(this.getX() >= enemyTank.getX()
                                    && this.getX() <= enemyTank.getX() + 60
                                    && this.getY() + 40 >= enemyTank.getY()
                                    && this.getY() + 40 <= enemyTank.getY() + 40
                            ){
                                return true;
                            }
                        }
                        //================ 敌方坦克左右方向,当前坦克上 end=======================
                    }
                }
                break;
        }
        return false;
    }

    @Override
    public void run() {
        //当实例化子弹的时候,创建Shot子弹,每一个坦克都对应一个子弹
        while (true){
            //如何让敌方坦克一直不停的发射子弹呢,很简单,只要敌方的坦克活着,就不断的发射子弹
            //因为是多现成的，所以每个坦克和每个坦克所属的子弹   都是一个个整体   自己运行自己的,并不会有什么牵扯
            if(isLive && shots.size() < 1){// 现在只允许敌方每次发射一个子弹,当敌方的一颗子弹消亡之后再发射另外一颗子弹
                Shot s = null;
                //根据敌方坦克的方向,创建对应的子弹
                switch (getDirect()){
                    case 0://上
                        s = new Shot(getX() + 20 , getY() , 0);
                        break;
                    case 1://右
                        s = new Shot(getX() + 60 , getY() + 20 ,1);
                        break;
                    case 2://下
                        s = new Shot(getX() + 20 , getY() + 60 , 2);
                        break;
                    case 3://左
                        s = new Shot(getX(),getY()+20,3);
                        break;
                }
                //子弹创建好之后,写入子弹集合中，其实这里的集合的作用很简单,就是保存数据,用作遍历
                shots.add(s);
                //启动子弹线层
                new Thread(s).start();
            }


            //让敌人的坦克可以随机的移动
            // 0 : 上   1：右   2：下  3：左
            /*
            * 实现逻辑【先死后活】
            * 化繁为简,先死后活,编程思想最重要
            * (1):根据方向移动坦克
            * (2):每次移动完了之后进行休眠
            * */
            //如果坦克活着的话,才让坦克跑
            if(isLive){
                switch (getDirect()){
                    case 0://向上
                        //对照的坐标点为坦克的左上角位置
                        //如果当前的坦克的y坐标大于0的时候,才能向下移动
                        //因为坐标大于0就可以持续的向下移动,所以才会重叠
                        //现在要做的就是当坦克没有和其他坦克重叠的时候才让移动,否则的话就换方向
                        for(int i = 0 ; i < 100 ; i++) {
                            if (getY() > 0 && !isTouchEnemyTank()) {
                                moveUp();
                            }else{
                                break;
                            }
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                        break;
                    case 1://向右
                        //如果x左边点+坦克的长度60 小于 1000   那么就继续向右移动  否则直接break换方向
                        for(int i = 0 ; i < 110 ; i++){
                            if(getX() + 60 < 1000 && !isTouchEnemyTank()){
                                moveRight();
                            }else{
                                break;
                            }
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                        break;
                    case 2://向下
                        //对照的坐标点为坦克的坐标点位置
                        //如果当前的坦克的y轴坐标+坦克的高度60，如果小于750,才让继续往下移动
                        //休眠50秒再向下移动dddddddddddddddd
                        for(int i = 0 ; i < 120 ; i++){
                            //如果y坐标+坦克的高度已经到了750 也就是到达了边界,就不会再继续向下移动了,直接break，50ms之后直接换方向
                            if(getY() + 60 < 750 && !isTouchEnemyTank()){
                                moveDown();
                            }else{
                                break;
                            }
                            try {
                                Thread.sleep(20);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                        break;
                    case 3://向左
                        //休眠50秒再向下移动
                        //如果当前的x轴坐标大于0的情况下才能继续向左移动
                        for(int i = 0 ; i < 80 ; i++){
                            if(getX() > 0 && !isTouchEnemyTank()){
                                moveLeft();
                            }else{
                                break;
                            }
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                }

                //上面是根据敌人坦克的方向进行移动的,所以每次换方向的时候进行休眠
                //50毫秒移动一次,
                //每次移动的时候换方向,必须在0-3之间
                //使用这种方式会出现非常奇怪的现象,就是每隔50毫秒,就会换方向,这是肯定不行的,那么如何处理呢
                //每次在换方向走的时候,让他走一段时间再换方向
                setDirect((int)(Math.random() * 4));
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else{
                //什么时候退出线程,当敌方坦克死亡的情况下,退出线程
                break;
            }

        }

    }
}
