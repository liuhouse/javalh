package javalh.chapter19.tankgame05;
/*
* 恢复敌人坦克
* 使用集合保存敌人对象信息,然后使用循环进行读取即可
* */
public class Node {
    //x轴
    private int x;
    //y轴
    private int y;
    //方向
    private int direct;

    public Node(int x, int y, int direct) {
        this.x = x;
        this.y = y;
        this.direct = direct;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDirect() {
        return direct;
    }

    public void setDirect(int direct) {
        this.direct = direct;
    }
}
