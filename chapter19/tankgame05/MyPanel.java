package javalh.chapter19.tankgame05;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.Vector;

/**
 * @author 刘皓
 * @version 1.0
 * 坦克大战的绘图区域
 * 画板
 */

/*
* 总结几个点
* (1):想要改变坦克的方向,就要在坦克的父类做一个方向的变量,然后在进行事件处理的时候,来改变这个变量的反向值
* (2):每次改变方向之后,只要是键盘一直按着,那么就要修改坦克的坐标
* (3):想要坦克移动,就必须在窗口上加上对面板的监听事件
* (4):进行重绘repaint()
* (5):跟坦克相关的公共属性和方法,都写在坦克的父类,然后在创建坦克的时候就定好坦克的速度
* */

//现在我们需要让坦克动起来,那么这里我们就要相应键盘的事件,实现事件监听接口
    //为了让Panel不停的重绘子弹,需要将MyPanel实现Runnable，当做一个线程使用
public class MyPanel extends JPanel implements KeyListener,Runnable{
    //定义我的坦克
    Hero hero = null;
    //定义敌方坦克的数量
    int enemyTankSize = 5;
    //定义墙的数量
    int wallSize = 20;

    //(1):因为敌人的坦克,是在MyPanel上,所以我们的代码写在MyPanel
    //(2):因为敌人的坦克,后面有自己的特殊属性和方法,可以单开一个EnemyTank
    //(3):因为敌人的坦克数量多,可以放入集合Vector中,因为考虑到多线程问题
    Vector<EnemyTank> enemyTanks = new Vector<>();

    //定义现在的要恢复的敌人坦克数据
    Vector<Node> nodes = null;

    //炸弹爆炸是要在面板上,所以在这里定义炸弹的集合
    //说明：当子弹击中坦克的时候,加入一个Bomb对象到bombs
    Vector<Bomb> bombs = new Vector<>();

    //保存墙的对象
    Vector<Wall> walls = new Vector<>();

    //定义三张炸弹图片,用于显示爆炸效果
    Image image1 = null;
    Image image2 = null;
    Image image3 = null;

    //构造器

    /**
     * 1：开始新游戏
     * 2：继续上局游戏
     * @param type
     */
    public MyPanel(String type){
        //初始化自己的坦克
        hero = new Hero(600,100);
        hero.setSpeed(5);

        //在绘画之前判断记录文件是不是存在,如果存在,就正常执行 接着继续游戏,如果文件不存在,就开始新游戏
        File file = new File(Recorder.getRecordFile());
        if(!file.exists()){
            System.out.println("记录文件不存在,只能开始新游戏!!");
            type = "1";
        }

        //在这里进行绘画敌人坦克的时候,要判断是开始新游戏还是继续上局游戏
        //这里使用switch
        switch (type){
            case "1"://开始新游戏
                //初始化敌方坦克
                for (int i = 0 ; i < enemyTankSize ; i++){
                    //创建一个敌方的坦克
                    EnemyTank enemyTank = new EnemyTank(((i + 1) * 100), 0);
                    //初始化坦克的时候,将所有的敌人的坦克集合放入坦克的enemyTanks的集合中
                    //是给每个坦克都要放入所有的坦克,因为是当前这个坦克和其他的坦克做对比
                    enemyTank.setEnemyTanks(enemyTanks);
                    //设置方向，因为这些都是和坦克相关的,所以在实例化坦克的时候,就要设置好坦克的方向
                    enemyTank.setDirect(2);
                    //启动敌人坦克线程,让他动起来,因为敌人的坦克类实现了Runnable接口,所以必须使用new Thread(enemyTank)来启动线程
                    //调用start()之后,就会调用start0()，然后会调用里面的run()方法,然后run方法创建好子弹之后,又启动子弹的线程
                    //完成子弹发射的动作
                    //然后里面的子弹才能生成
                    new Thread(enemyTank).start();
                    //给每个enemyTank加入一颗子弹，这是在画坦克的时候给的默认子弹
                    //这里会给每个敌人坦克默认的添加一个子弹,并且启动子弹的线程,调用子弹的run方法进行发射,这里只能保证发一颗子弹
                    //思考,如何让敌方的坦克发射多个子弹呢,其实很简单,敌方的子弹是属于敌方坦克的,当敌方的坦克启动线程的时候
                    //判断,当敌方坦克只要活着,就不断的让发子弹,这个是由程序员自己指定的
                    Shot shot = new Shot(enemyTank.getX() + 20 , enemyTank.getY() + 60 , enemyTank.getDirect());
                    //加入enemyTank的Vector成员
                    enemyTank.shots.add(shot);
                    //启动shot对象
                    new Thread(shot).start();
                    //将坦克加入集合中(多线程)，保存到全局使用
                    enemyTanks.add(enemyTank);
                }
                break;
            case "2"://继续上局游戏
                //如果是继续上局游戏的话,现在会有所不同,需要在记录中通过nodes将敌方坦克信息取出
                nodes = Recorder.recoverEnemyTanksAndNum();
                //初始化敌方坦克
                for (int i = 0 ; i < nodes.size() ; i++){
                    //获取一个敌方坦克
                    Node node = nodes.get(i);
                    //创建一个敌方的坦克
                    EnemyTank enemyTank = new EnemyTank(node.getX(), node.getY());
                    //初始化坦克的时候,将所有的敌人的坦克集合放入坦克的enemyTanks的集合中
                    //是给每个坦克都要放入所有的坦克,因为是当前这个坦克和其他的坦克做对比
                    enemyTank.setEnemyTanks(enemyTanks);
                    //设置方向，因为这些都是和坦克相关的,所以在实例化坦克的时候,就要设置好坦克的方向
                    enemyTank.setDirect(node.getDirect());
                    //启动敌人坦克线程,让他动起来,因为敌人的坦克类实现了Runnable接口,所以必须使用new Thread(enemyTank)来启动线程
                    //调用start()之后,就会调用start0()，然后会调用里面的run()方法,然后run方法创建好子弹之后,又启动子弹的线程
                    //完成子弹发射的动作
                    //然后里面的子弹才能生成
                    new Thread(enemyTank).start();
                    //给每个enemyTank加入一颗子弹，这是在画坦克的时候给的默认子弹
                    //这里会给每个敌人坦克默认的添加一个子弹,并且启动子弹的线程,调用子弹的run方法进行发射,这里只能保证发一颗子弹
                    //思考,如何让敌方的坦克发射多个子弹呢,其实很简单,敌方的子弹是属于敌方坦克的,当敌方的坦克启动线程的时候
                    //判断,当敌方坦克只要活着,就不断的让发子弹,这个是由程序员自己指定的
                    Shot shot = new Shot(enemyTank.getX() + 20 , enemyTank.getY() + 60 , enemyTank.getDirect());
                    //加入enemyTank的Vector成员
                    enemyTank.shots.add(shot);
                    //启动shot对象
                    new Thread(shot).start();
                    //将坦克加入集合中(多线程)，保存到全局使用
                    enemyTanks.add(enemyTank);
                }
                break;
        }


        //当刚初始化游戏的时候,就讲坦克enemyTanks设置到Recorder类中的enemyTanks属性中
        Recorder.setEnemyTanks(enemyTanks);

        //当实例化面板的时候 初始化图片
        //加载图片的时候 需要使用本面板类来进行加载
        image1 = Toolkit.getDefaultToolkit().getImage(MyPanel.class.getResource("bomb_1.gif"));
        image2 = Toolkit.getDefaultToolkit().getImage(MyPanel.class.getResource("bomb_2.gif"));
        image3 = Toolkit.getDefaultToolkit().getImage(MyPanel.class.getResource("bomb_3.gif"));




        for(int i = 0 ; i < 2 ; i++){
            //初始化墙，第一层
            int y = 600;
            if(i == 1){
                y = 631;
            }
            for(int j = 0 ; j < wallSize ; j++){
                Wall wall = new Wall(((j + 1) * 31), y, 30, 30);
                walls.add(wall);
            }
        }

        //当所有的信息 坦克初始化,子弹初始化,爆炸图片初始化 全部初始化完毕之后,就播放音乐
        new AePlayWave("javalh\\chapter19\\tankgame05\\111.wav").start();
    }


    //现在页面上需要一个记录成绩的元素
    //所以我们需要用画笔来进行绘画,也当然是在MyPanel上进行绘画,然后在paint()方法中执行绘画操作
    //编写方法,显示我方击毁敌人坦克的信息
    public void showInfo(Graphics g){
        //画出玩家的总成绩
        g.setColor(Color.BLACK);
        Font font = new Font("宋体", Font.BOLD, 23);
        g.setFont(font);

        //画出字符串
        g.drawString("您累积击毁敌方的坦克",1020,30);
        //画出一个敌方坦克
        drawTank(1020,60,g,0,0);
        //将颜色重新设置为黑色
        g.setColor(Color.BLACK);
        //画出现在被击毁的敌方坦克数
        g.drawString(Recorder.getAllEnemyTankNum() + "",1080,100);
    }

    //画笔
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //填充矩形,并填充颜色,作为画板的区域
        //默认为黑色
        g.fillRect(0,0,1000,750);

        showInfo(g);

        //绘画白色的墙
        drawWall(g);

        //画出我方坦克-封装方法
        //如果我方坦克活着就画出来
        if(hero.isLive){
            drawTank(hero.getX(), hero.getY(),g,hero.getDirect(),1);
        }


        //画子弹的时候肯定是要在画板上进行绘画
        //画出子弹
        //如果子弹对象不为null，并且子弹是活着的,就绘制子弹

        //====================只允许我方坦克发射一颗子弹start====================
//        if(hero.shot != null && hero.shot.isLive == true){
//            g.fillOval(hero.shot.x,hero.shot.y,3,3);
//        }
        //===================end==============================================

        //====================允许我方英雄发射多颗子弹start=========================

        for (int i = 0 ; i < hero.shots.size() ; i++){
            //取出单个子弹
            Shot shot = hero.shots.get(i);
            //如果当前的子弹是存在的,或者子弹还活着的话,就绘制
            System.out.println(shot);
            if(shot != null && shot.isLive){
                g.fillOval(shot.x,shot.y,3,3);
            }else{
                //如果发现子弹==null的时候或者说子弹已经死了
                //就把子弹从集合中取出掉 remove
                System.out.println("我方的子弹已经被移除了");
                hero.shots.remove(shot);
            }
        }

        //====================end=============================================

        //在何时绘制爆炸效果呢
        //在绘制面板的时候发现有爆炸得坦克,就绘制爆炸效果
        //如果bombs 集合中有对象,就画出    说明有爆炸得坦克
        for(int i = 0 ; i < bombs.size() ; i++){
            //取出炸弹
            Bomb bomb = bombs.get(i);
            //根据当前的这个bomb对象的life值画出对应的图片
            if(bomb.life > 6){
                //如果生命值大于6,就画出一张图片
                g.drawImage(image1,bomb.x,bomb.y,60,60,this);
            }else if(bomb.life > 3){
                g.drawImage(image2,bomb.x,bomb.y,60,60,this);
            }else{
                g.drawImage(image3,bomb.x,bomb.y,60,60,this);
            }
            //每绘制一次,炸弹的生命值减少
            bomb.lifeDown();
            //如果bomb lift 值为0,就从bom的集合中删除
            if(bomb.life == 0){
                bombs.remove(bomb);
            }
        }

        //画出敌方坦克
        for(int i = 0 ; i < enemyTanks.size(); i++){
            EnemyTank emTank = enemyTanks.get(i);
            //如果敌方的坦克还活着的时候,我们才画出该坦克
            if(emTank.isLive){
                drawTank(emTank.getX(),emTank.getY(),g,emTank.getDirect(),0);
                //画出敌人坦克的所有子弹
                for (int j = 0 ; j < emTank.shots.size() ; j++){
                    //取出子弹
                    Shot shot = emTank.shots.get(j);
                    //绘制,如果子弹是活着的
                    if(shot.isLive){//isLive == true
                        g.fillOval(shot.x, shot.y, 2,2);
                    }else{
                        //如果子弹死了,就将子弹移除
                        emTank.shots.remove(shot);
                    }
                }
            }
        }
    }



    //画墙
    public void drawWall(Graphics g){
        g.setColor(Color.WHITE);
        for(int i = 0 ; i < walls.size() ; i++){
            //取出墙
            Wall wall = walls.get(i);
            g.fillRect(wall.getX(),wall.getY(),wall.getWidth(),wall.getHeight());
        }
    }

    //编写方法,画出坦克
    /**
     *
     * @param x     坦克的左上角x坐标
     * @param y     坦克的左上角y坐标
     * @param g     画笔
     * @param direct    坦克方向(上下左右)
     * @param type  坦克类型
     */
    public void drawTank(int x , int y ,Graphics g, int direct , int type){
        //根据不同坦克的类型,设置不同的颜色
        switch (type){
            case 0://敌人的坦克
                g.setColor(Color.cyan);
                break;
            case 1://自己的坦克
                g.setColor(Color.yellow);
                break;
        }

        //根据坦克的方向,来绘制坦克
        //direct 表示方向(0:向上 1：向右 2:向下 3：向左)
        switch (direct){
            case 0://表示向上
                g.fill3DRect(x,y,10,60,false);//画出左边的轮子
                g.fill3DRect(x+30,y,10,60,false);//画出右边的轮子
                g.fill3DRect(x+10,y+10,20,40,false);//画出坦克的身体
                g.fillOval(x+10,y+20,20,20);//画出坦克的盖子
                g.drawLine(x+20,y,x+20,y+30);//画出炮筒
                break;
            case 1://表示向右
                g.fill3DRect(x,y,60,10,false);//画出上面的轮子
                g.fill3DRect(x,y+30,60,10,false);//画出下面的轮子
                g.fill3DRect(x+10,y+10,40,20,false);//画出坦克的身体
                g.fillOval(x+20,y+10,20,20);//画出坦克盖子
                g.drawLine(x+30,y+20,x+60,y+20);//画出炮筒
                break;
            case 2://表示向下
                g.fill3DRect(x,y,10,60,false);//画出左边的轮子
                g.fill3DRect(x+30,y,10,60,false);//画出右边的轮子
                g.fill3DRect(x+10,y+10,20,40,false);//画出坦克的身体
                g.fillOval(x+10,y+20,20,20);//画出坦克的盖子
                g.drawLine(x+20,y+20,x+20,y+60);//画出炮筒
                break;
            case 3://表示向左
                g.fill3DRect(x,y,60,10,false);//画出上面的轮子
                g.fill3DRect(x,y+30,60,10,false);//画出下面的轮子
                g.fill3DRect(x+10,y+10,40,20,false);//画出坦克的身体
                g.fillOval(x+20,y+10,20,20);//画出坦克盖子
                g.drawLine(x,y+20,x+30,y+20);//画出炮筒
                break;
            default:
                System.out.println("暂时没有处理");
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    /*
        1：这里我们主要的是相应键盘按下的事件
        2：我们使用 wdsa  w(上) d(右) s(下) a(左)
        3：然后修改方向即可 direct
        4：方向可以改变之后,就要使得坦克可以移动,如何移动?
            (1)：在相应事件的时候,对对应的事件的坐标进行修改  ++ 或者 --
     */

    @Override
    public void keyPressed(KeyEvent e) {
        //向上
        if(e.getKeyCode() == KeyEvent.VK_W){//向上移动
            hero.setDirect(0);
            //如果当前的y坐标大于0,可以继续向上移动
            if(hero.getY() > 0){
                hero.moveUp();
                HeroRunMusic();
            }
        }else if(e.getKeyCode() == KeyEvent.VK_D){//向右移动
            hero.setDirect(1);
            //如果当前的x坐标+60 小于 1000 就可以继续向右移动
            if(hero.getX() + 60 < 1000){
                hero.moveRight();
                HeroRunMusic();
            }
        }else if(e.getKeyCode() == KeyEvent.VK_S){//向下移动
            hero.setDirect(2);
            //如果当前的y坐标+60 小于750 就可以继续向下移动
            if(hero.getY() + 60 < 750){
                hero.moveDown();
                HeroRunMusic();
            }
        }else if(e.getKeyCode() == KeyEvent.VK_A){//向左移动
            hero.setDirect(3);
            //如果x坐标大于0,就可以继续向左移动
            if(hero.getX() > 0){
                hero.moveLeft();
                HeroRunMusic();
            }
        }
        //相应到了事件,画板上的内容也就发生了变化,那么这个时候就要重新绘制面板,这样坦克才能动起来

        //如果按下的是J,就发射子弹
        //这是我方的坦克发射子弹,判断当我方子弹消亡之后,才能够发射新的子弹
        if(e.getKeyCode() == KeyEvent.VK_J){
            //如果我方的子弹没有了 hero.shot = null  代表的是第一次发射子弹
            //如果我方子弹消亡了
            //再允许发射新子弹

            //(1)===========只允许我方坦克发射一颗子弹,当我方子弹的坦克消亡之后才能发射新子弹 start====================
//            if(hero.shot == null || !hero.shot.isLive){
//                hero.shotEnemyTank();
//            }
            //=============end======================================================

            //(2)===============可以发射多个子弹start=====================
            //hero.shotEnemyTank();
            //===============end=====================================

            //(3)============我方坦克最多可以发射五颗子弹start===============
             hero.shotEnemyTank();

            //======================end================================


        }
        this.repaint();
    }

    //编写方法,只要坦克行走,就添加行走的音乐
    public void HeroRunMusic(){
        new AePlayWave("javalh\\chapter19\\tankgame05\\music\\hit.wav").start();
    }


    //编写方法,判断我方的子弹是否击中敌人坦克

    /**
     *
     * @param s 我方子弹
     * @param enemyTank  敌方坦克
     * 判断我方的子弹是否击中敌方坦克
     * 什么时候判断,我方的子弹是否击中敌人的坦克 ? run方法中进行判断
     * 为什么要在run方法中进行判断,因为run方法是一直运行的,是不断进行重绘的,每次在重绘的时候判断我方的子弹是否击中敌人的坦克
     */
    public void hitTank(Shot s , Tank enemyTank){
        //判断s是否击中坦克
        //根据敌方坦克的方向判断是否被子弹击中的区域
        // 0:上  1：右  2：下  3:左
        switch (enemyTank.getDirect()){
            case 0://坦克向上
            case 2://坦克向下
                //如果子弹的x坐标在敌方坦克的x坐标的最小到最大的距离之中,并且
                //子弹的y坐标在敌方坦克的y坐标的最小到最大的距离之中的话
                //就代表击中了敌方的坦克
                if(s.x > enemyTank.getX() && s.x < enemyTank.getX() + 40
                        && s.y > enemyTank.getY() && s.y < enemyTank.getY() + 60
                ){
                    //在区域内,子弹就死亡
                    s.isLive = false;
                    //敌方坦克也消失
                    enemyTank.isLive = false;
                    //当我的子弹击中敌方坦克的时候,将enemyTank 从Vector拿掉
                    Bomb bomb = new Bomb(enemyTank.getX(), enemyTank.getY());
                    bombs.add(bomb);
                    enemyTanks.remove(enemyTank);
                    //当敌方坦克挂掉之后,创建炸弹Bomb对象,加入到bombs集合
                    //如果当前的坦克是敌方坦克,并且被击落的时候,就要给敌方坦克的销毁数量+1
                    if(enemyTank instanceof EnemyTank){
                        Recorder.addAllEnemyTankNum();
                        //当敌方坦克被击毁的时候,就播放爆炸得声音
                        new AePlayWave("javalh\\chapter19\\tankgame05\\music\\bom.wav").start();
                    }
                }
                break;
            case 1://坦克向右
            case 3://坦克向左
                //如果子弹的x坐标在敌人坦克的x坐标的最小到最大的举例之中,并且
                //子弹的y坐标在敌人的y坐标的最小到最大的举例之中的话
                //就代表已经击中了敌方的坦克
                if(s.x > enemyTank.getX() && s.x < enemyTank.getX() + 60
                        && s.y > enemyTank.getY() && s.y < enemyTank.getY() + 40
                ){
                    //在子弹击中的区域内,子弹就死亡
                    //敌方坦克也死亡
                    s.isLive = false;
                    enemyTank.isLive = false;
                    //创建Bomb对象,加入到bombs集合
                    Bomb bomb = new Bomb(enemyTank.getX(), enemyTank.getY());
                    //加入到bombs集合
                    bombs.add(bomb);
                    enemyTanks.remove(enemyTank);
                    //如果当前摧毁的坦克是敌方坦克的时候,就给摧毁的敌方坦克数+1
                    if(enemyTank instanceof EnemyTank){
                        Recorder.addAllEnemyTankNum();
                        //当敌方坦克被击毁的时候,就播放爆炸得声音
                        new AePlayWave("javalh\\chapter19\\tankgame05\\music\\bom.wav").start();
                    }

                }
                break;
        }
    }


    /**
     * 编写方法
     * 判断敌人的坦克的子弹是否击中我方坦克
     * 逻辑：遍历所有的敌人坦克,并且遍历所有敌人坦克的子弹,然后用每个子弹判断是否撞击我方坦克
     */
    public void hitHero(){
        //遍历敌方坦克
        for(int i = 0 ; i < enemyTanks.size() ; i++){
            //取出每一个敌方坦克
            EnemyTank enemyTank = enemyTanks.get(i);
            //循环每一个敌方坦克的子弹
            for(int j = 0 ; j < enemyTank.shots.size() ; j++){
                //取出每个子弹
                Shot shot = enemyTank.shots.get(j);
                //判断敌方坦克的每个子弹是否击中我方英雄
                //如果我方英雄是存活的,并且敌方的子弹也是存活的
                if(hero.isLive && shot.isLive){
                    //就用敌方的子弹打击我方英雄
                    hitTank(shot,hero);
                }
            }
        }
    }



    //我方子弹击中敌方坦克
    /*
    * 拿出我方的子弹,取出敌人的所有坦克,进行遍历，因为不确定哪个敌方坦克,所以就必须遍历判断哪个敌方坦克被击中
    * */

    /**
     * 摧毁敌方坦克
     * 如果说我们的坦克可以发射多颗子弹
     * 在判断我方子弹是否击中敌人的坦克的时候,就需要把我们的子弹集合中所有的子弹都取出来和敌人的所有的坦克进行判断
     */
    public void hitEnemyTank(){



        //=======================单颗子弹start=============================

        //单颗子弹
        //如果英雄的子弹是存在的并且是存活的
//        if(hero.shot != null && hero.shot.isLive){
//            //遍历敌人的所有坦克
//            for (int i = 0 ; i < enemyTanks.size() ; i++){
//                //得到敌人的坦克
//                EnemyTank enemyTank = enemyTanks.get(i);
//                hitTank(hero.shot , enemyTank);
//            }
//        }

        //========================单颗子弹end=================================

        //========================多颗子弹start===================================

        //遍历我们的子弹
        for(int i = 0 ; i < hero.shots.size() ; i++){
            //取出单个子弹
            Shot shot = hero.shots.get(i);
            //如果我方的子弹是存在的,并且我方的子弹是存活的
            if(shot !=null && shot.isLive){
                //遍历敌方坦克,只要有我方的一颗子弹撞击到地方的坦克,那么就将我方的子弹对敌方的坦克都销毁
                //一个原则  一颗子弹只能消灭一个敌人
                for(int j = 0 ; j < enemyTanks.size() ; j++){
                    //得到敌人的坦克
                    //获取子弹是 get(j)
                    EnemyTank enemyTank = enemyTanks.get(j);
                    hitTank(shot,enemyTank);
                }
            }
        }


        //=======================多颗子弹end=====================================


    }



    @Override
    public void keyReleased(KeyEvent e) {

    }

    //每隔100毫秒,重绘区域,刷新绘图区域,子弹就移动
    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //判断我方子弹是否击中敌人坦克，一直在判断
            hitEnemyTank();
            //击中我方英雄
            hitHero();
            //每隔100毫秒重绘一次
            this.repaint();
        }
    }
}
