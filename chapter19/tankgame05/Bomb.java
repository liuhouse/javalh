package javalh.chapter19.tankgame05;

/**
 * @author 刘皓
 * @version 1.0
 * 炸弹类
 */
public class Bomb {
    int x , y ; // 炸弹的坐标
    int life = 9;//炸弹的生命周期
    boolean isLive = true;//炸弹是否存活

    //构造方法初始化炸弹
    public Bomb(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //减少生命值
    //配合图片的爆炸效果
    public void lifeDown(){
        if(life > 0){
            life--;
        }else{
            isLive = false;
        }
    }
}
