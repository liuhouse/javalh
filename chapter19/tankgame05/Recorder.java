package javalh.chapter19.tankgame05;

import java.io.*;
import java.util.Vector;

/*
* 该类用于记录相关信息的,和文件交互
* */
public class Recorder {
    //定义变量,记录我方坦克击毁敌方坦克数
    private static int allEnemyTankNum = 0;
    //定义IO对象,准备写数据到文件中
    private static BufferedWriter bw = null;
    //定义IO对象,准备读文件中的数据
    private static BufferedReader br = null;
    //定义读写文件的地址
    private static String recordFile = "javalh\\chapter19\\tankgame05\\myRecord.txt";
    //定义所有的敌人坦克数据
    private static Vector<EnemyTank> enemyTanks = null;
    //定义恢复敌方坦克的Node节点集合
    private static Vector<Node> nodes = new Vector<>();

    //设置敌方坦克的数据
    public static void setEnemyTanks(Vector<EnemyTank> enemyTanks) {
        Recorder.enemyTanks = enemyTanks;
    }

    //获取击毁的敌人坦克数量
    public static int getAllEnemyTankNum() {
        return allEnemyTankNum;
    }

    //设置击毁敌人的坦克数量
    public static void setAllEnemyTankNum(int allEnemyTankNum) {
        Recorder.allEnemyTankNum = allEnemyTankNum;
    }

    //获取记录文件
    public static String getRecordFile() {
        return recordFile;
    }

    //当我方坦克击毁一个敌人坦克的时候后,就应该allEnemyTankNum++
    public static void addAllEnemyTankNum(){
        Recorder.allEnemyTankNum++;
    }

    //增加一个方法,当游戏退出的时候,我们将allEnemyTankNum保存到recordFile
    //强化方法,记录退出游戏的时候敌人的坦克坐标/方向,存盘退出
    public static void keepRecord(){
        try {
            //实例化输出流,使用BufferedWriter高效率的数据
            bw = new BufferedWriter(new FileWriter(recordFile));
            //写入数据
            bw.write(allEnemyTankNum+"\r\n");

            //保存敌方存活坦克的数据
            for(int i = 0 ; i < enemyTanks.size() ; i++){
                //记录每一条的敌方坦克的信息
                EnemyTank enemyTank = enemyTanks.get(i);
                if(enemyTank.isLive){
                    String enemyTankInfo = enemyTank.getX() + " " + enemyTank.getY() + " " + enemyTank.getDirect();
                    bw.write(enemyTankInfo);
                    bw.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(bw != null){
                    bw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //恢复所有敌人的坦克,坐标和方向,以及击毁敌人坦克的数量
    //其实就是读取记录游戏的文件,然后放入Node节点中保存下来以便使用即可
    public static Vector<Node> recoverEnemyTanksAndNum(){
        //开始读取文件
        try {
            br = new BufferedReader(new FileReader(recordFile));
            String AllEnemyTankNum = br.readLine();
            //设置击毁敌人的坦克数量,设置好之后,读取的时候,就直接用记录的击毁敌人的坦克数量即可
            setAllEnemyTankNum(Integer.parseInt(AllEnemyTankNum));

            String line = "";

            //循环读取敌方坦克的信息,并且使用Node类进行实例化保存
            //因为上面已经读取了一行了,所以这里直接是从第二行开始的,也就是敌方的坦克信息
            while((line = br.readLine()) != null){
                //获取坦克的x轴,y轴,方向的数组信息
                String[] xyd = line.split(" ");
                //使用Node进行实例化对象
                Node node = new Node(Integer.parseInt(xyd[0]), Integer.parseInt(xyd[1]), Integer.parseInt(xyd[2]));
                //将node全部加入到集合中并且返回
                nodes.add(node);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return nodes;
    }


}
