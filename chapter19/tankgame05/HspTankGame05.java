package javalh.chapter19.tankgame05;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 *
 *
 * 线程-应用到坦克大战
 * java线程的基本知识,现在我们来实际运用一下
 *
 * 在坦克大战游戏(0.2版本)基础上添加如下功能,当玩家按下j键,就发射一颗子弹
 *
 * 分析如何实现当用户按下J键的时候,我们的坦克就发射一颗子弹
 * 老师思路
 * 1:当发射一颗子弹之后,就相当于是启动一个线程
 * 2:Hero有子弹的对象,当按下J键的时候,我们就启动一个发射行为(线程),让子弹不停的移动,形成一个射击的效果
 * 3:我们MyPanel需要不停的重绘子弹,才能出现该效果
 * 4:当子弹移动到面板的边界的时候,就应该销毁(把启动的子弹的线程销毁)
 *
 *
 *
 * 坦克大战0.4版本
 * 增加功能
 * 1:让敌人的坦克也能够发射子弹(可以有多颗子弹)
 * 2：当我方坦克击中敌人坦克的时候,敌人坦克消失,如果能做出爆炸效果会更好
 */


/**
 * 子需求
 * 让敌人的坦克能够发射子弹(可以有多颗子弹)
 * 1.在敌人坦克类,使用Vector保存多个shot
 * 2.当每创建一个敌人坦克对象,给该敌人坦克对象初始化一个Shot对象,同时启动Shot
 * 3.在绘制敌人坦克的时候,需要遍历敌人坦克对象Vector，绘制每个敌人坦克的子弹,当子弹isLive == false的时候,就从Vector移除
 *
 */


/**
 * 增加功能
 * (1):我方坦克在子弹消亡之后,才能发射新的子弹 （发射多颗子弹怎么办,控制在我们的面板上,最多只有5颗）,在课后完善
 *         思路分析：在我方坦克在发射子弹的动作的时候,判断一下我方的子弹是否存活,当我方坦克的子弹消亡之后,再进行发射子弹的动作（ok）
 *
 */

/*
* 让敌人的坦克也可以自由随机的上下左右移动
* 思路分析
* 1：因为要求敌人的坦克,可以自由的移动,因此需要将敌人的坦克做成线程使用 【各自移动各自的】
* 2：我们需要EnemyTank implements Runnable接口
* 3：因为要不停的移动,所以需要在run方法中写上相应的业务代码
* 4：在创建敌人坦克的对象的时候,启动线程
*
* */

/*
* 版本5
* (1)防止敌人坦克重叠运动
* (2)记录玩家的总成绩(累计击毁敌人的坦克数量),存盘退出[io流]
* (3)记录退出游戏时敌人坦克的坐标/方向,存盘退出[io流]
* (4)玩游戏的时候,可以选择是开新游戏还是继续上局游戏[io流]
*    --将每个敌人信息,恢复成Node对象 => Vector   通过Node的Vector去恢复敌人的坦克位置和方向
*
* */


public class HspTankGame05 extends JFrame {
    //在画框中定义画板
    MyPanel mp = null;

    public static void main(String[] args) {
        new HspTankGame05();
    }

    //使用构造器进行初始化
    public HspTankGame05(){
        //对画板进行初始化，代表一个线程
        //在这里需要传递一个参数,表示当前是开始新游戏还是继续上次的游戏
        //在对构造器进行初始化的时候,就进行是否开始新游戏判断
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入：1-开始新游戏; 2-继续上局游戏");
        String type = scanner.next();
        mp = new MyPanel(type);
        //将mp放入到Thread，并启动
        //因为实现了Runnable接口,所以要启动就要放到Thread方法中
        //因为子弹要自己走, 而且面板也要一直刷新,所以就把两个都做成多线程的 ， 自己运行自己的即可
        Thread thread = new Thread(mp);
        thread.start();
        //将画板加载到画框中
        this.add(mp);
        //想要坦克动起来,窗口就必须作为监听者来监听键盘事件对象
        this.addKeyListener(mp);
        //设置画板的大小
        //画板的大小要设置的大一点,因为他包括了外边界,所以要设置的大一点
        this.setSize(1300,800);
        //设置点击关闭X的时候退出程序
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置画板可视
        this.setVisible(true);
        //在JFrame中增加相关关闭窗口的处理
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Recorder.keepRecord();
                System.exit(0);
            }
        });
    }

}
