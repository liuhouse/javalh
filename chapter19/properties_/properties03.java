package javalh.chapter19.properties_;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 */
public class properties03 {
    public static void main(String[] args) throws IOException {
        //使用Properties 类来创建配置文件,修改配置文件的内容
        /*
        Properties的父类是HashTable，底层就是HashTable的核心方法
        * */
        Properties properties = new Properties();
        //设置属性
        //创建
        //1:如果该文件没有这个key，就是创建
        //2:如果该文件有这个key，就是修改
        properties.setProperty("charset","utf8");
        properties.setProperty("user","汤姆11");//注意这里进行保存的时候,是中文的 unicode码值
        properties.setProperty("pwd","123456");
        //将k-v存储到文件中使用
        properties.store(new FileOutputStream("javalh\\chapter19\\properties_\\mysql2.properties"),null);
        System.out.println("保存配置文件成功~~~");
    }
}
