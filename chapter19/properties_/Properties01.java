package javalh.chapter19.properties_;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Properties01 {
    public static void main(String[] args) throws IOException {
        /**
         * 请编程读取 ip , user , pwd 的值是多少
         */
        //分析
        //1:使用传统的方式
        //2:使用Properties类可以方便实现

        //使用传统的方式读取配置文件
        BufferedReader bufferedReader = new BufferedReader(new FileReader("javalh\\chapter19\\properties_\\mysql.properties"));
        //开始读取文件
        String readLine;
        while ((readLine = bufferedReader.readLine()) != null){
            String[] split = readLine.split("=");
            if("ip".equals(split[0])){
                System.out.println(split[0] + " =" + split[1]);
            }
        }
        //关闭文件
        bufferedReader.close();

    }
}
