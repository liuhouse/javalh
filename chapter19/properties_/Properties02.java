package javalh.chapter19.properties_;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Properties02 {
    public static void main(String[] args) throws IOException {
        /*
        * (1):专门用于读写配置文件的集合类
        *   配置文件的格式
        *   键=值
        *   键=值
        *
        * (2)注意：键值不需要有空格,值不需要使用引号引起来,默认是String
        *
        * (3)Properties的常见方法
        *   . load:加载配置文件的键值对到Properties对象
        *   . list:将数据显示到指定设备
        *   . getProperty(key) : 根据键获取值
        *   . setProperty(key,value):设置键值对到Properties对象
        *   . store:将Properties中的键值对存储到配置文件,在idea中,保存信息到配置文件,如果含有中文,会存储为unicode码
        *
        * */

        //使用Properties类完成对mysql.properties的读取
        //1:创建Properties对象
        Properties properties = new Properties();
        //2：加载指定配置文件
        properties.load(new FileReader("javalh\\chapter19\\properties_\\mysql.properties"));
        //3:把k-v显示在控制台
        properties.list(System.out);
        //4:根据key获取对应的值
        String user = properties.getProperty("user");
        String pwd = properties.getProperty("pwd");
        System.out.println("用户名 = " + user);
        System.out.println("密码 = " + pwd);




    }
}
