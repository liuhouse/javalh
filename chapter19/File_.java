package javalh.chapter19;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 * IO流
 */
public class File_ {
    public static void main(String[] args) {
        /*
        * 文件
        * 什么是文件
        * 文件对我们并不陌生,文件是保存数据的地方,比如大家经常使用的word文档,.txt文件,excel文件都是文件
        * 它即可保存一张图片,也可以保存视频音频
        *
        * 文件流
        * 文件在程序中是以流的形式来操作的
        * 流：数据在数据源(文件)和程序(内存)之间经历的路径
        * 输入流：数据从数据源(文件)到程序(内存)的路径
        * 输出流：数据从程序(内存)到数据源(文件)的路径
        *
        * 请在e盘下面,创建文件news1.txt,news2.txt，news3.txt，用三种不同的方式创建
        * */
    }

    //方式1创建文件
    @Test
    public void create01(){
        String filePath = "e:\\news1.txt";
        //在内存中创建一个File对象
        File file = new File(filePath);
        try {
            file.createNewFile();
            System.out.println("文件创建成功");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    //使用第二种方式创建文件 new File(File parent , String child)//根据父目录文件+子路径创建
    @Test
    public void create02(){
        //父路径
        String parentDir = "e:\\";
        //创建父路径对象
        File parentFile = new File(parentDir);
        //需要创建的文件的名称
        String fileName = "news2.txt";
        //实例化创建文件的对象
        //这里的file对象,在java程序中,只是一个对象
        //只有真正的执行了createNewFile()方法,才会真正的在磁盘创建该文件
        File file = new File(parentFile, fileName);
        try {
            file.createNewFile();
            System.out.println("创建成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //方式3创建文件
    //new File(String parent , String Child) 根据父目录 + 子路径构建
    @Test
    public void create03(){
        //父路径
        String parentPath = "e:\\";
        //子路径 文件名称
        String fileName = "news4.txt";
        //实例化文件 根据父路径
        File file = new File(parentPath, fileName);
        try {
            file.createNewFile();
            System.out.println("创建文件成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

