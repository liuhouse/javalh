package javalh.chapter19.reader_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Test {
    public static void main(String[] args) {

        /*
        * 1：节点流是底层流/低级流,直接跟数据源相接 - 扩展性很小
        * 2:处理流(包装流)包装节点流,既可以消除不同节点流之间的实现差异,也可以提供更方便的方法来完成输入输出
        * 3:处理流(也叫包装流),对节点流进行包装,使用了修饰器设计模式,不会直接与数据源相连[模拟修饰器设计模式]
        * */

        BufferedReader_ bufferedReader_ = new BufferedReader_(new FileReader_());
        //读取多次文件
        bufferedReader_.readFiles(10);

        //读取字符串
        BufferedReader_ bufferedReader_1 = new BufferedReader_(new StringRender_());
        bufferedReader_1.readStrings(10);
        //读取多次的字符串
//        bufferedReader_1.readFile();
    }
}
