package javalh.chapter19.reader_;

/**
 * @author 刘皓
 * @version 1.0
 */
public abstract class Reader_ {//抽象类
    //读取文件
    public void readFile(){

    }
    //读取字符串
    public void readString(){}

    //在Reader_抽象类,使用read方法统一管理
    //后面再调用的时候,利于对象动态绑定机制,绑定到对应的实现子类即可
    //public abstract void read();
}
