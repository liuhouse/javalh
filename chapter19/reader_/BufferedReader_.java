package javalh.chapter19.reader_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class BufferedReader_ extends Reader_{
    private Reader_ reader_;//属性是Render_类型  可以接收Render_或者Render_的子类实例化对象

    //接收Render_的子类对象
    public BufferedReader_(Reader_ reader_) {
        this.reader_ = reader_;
    }

    //封装一层，既可以直接调用文件里面的方法
    public void readFile(){
        reader_.readFile();
    }

    //让方法更加灵活,多次读取文件,或者加缓冲区byte[] ....
    public void readFiles(int num){
        for(int i = 0 ; i < num ; i++){
            reader_.readFile();
        }
    }

    //扩展readString，批量处理字符串数据
    public void readStrings(int num){
        for(int i = 0 ; i < num ; i++){
            reader_.readString();
        }
    }



}
