package javalh.chapter19;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class FileWriter_ {
    public static void main(String[] args) {
        /*
        * 重点
        * (1):在给文件写数据的时候,实例化FileWriter(filePath,true) 第二个参数 是否覆盖数据的意思是在数据全部写入之后
        *       也就是调用了close()或者flush的时候，才算一次完整的数据存储   然后再次执行此程序如果是false，就是替换,否则就是追加
        * (2):只要是牵扯到文件操作的时候,就必须close 第一能够节省资源 第二,如果不关闭文件,数据是不能写入的,因为数据的写入就是在关闭的时候写入的
        * */


        /*
        * fileWriter常用方法
        * (1)new FileWriter(File/String):覆盖模式,相当于流的指针在首端
        * (2)new FileWriter(File/String,true):追加模式,相当属于流的指针在尾端
        * (3)write(int)写入单个字符
        * (4)write(char[]):写入指定数组
        * (5)write(char[] , off , len) 写入指定数组的指定部分
        * (6)write(String);写入整个字符串
        * (7)write(String,off,len)写入指定字符串的指定部分
        * 相关API：String类， toCharArray:将String转换成char[]
        * 注意:
        * FileWriter使用后,必须要关闭close()或者刷新(flush)，否则写入不到指定的文件
        * */


        //使用FileWriter将 "风雨之后,定见彩虹"写入到note.txt文件中,注意细节
        String filePath = "e:\\note.txt";
        //创建FileWriter对象
        FileWriter fileWriter = null;
        char[] chars = {'a','b','c'};

        try {
            //默认是覆盖写入
            fileWriter = new FileWriter(filePath,true);
            //(3)write(int)写入单个字符
            fileWriter.write('H');
            //(4)写入指定数组
            fileWriter.write(chars);
            //(5)write(char[],off,len)写入指定数组的指定部分
            fileWriter.write("韩顺平教育".toCharArray(),0,3);
            //(6)write(string)：写入整个字符串
            fileWriter.write("风雨之后,定见彩虹");
            //(7)write(string,off,len):写入字符串的指定部分
            fileWriter.write("上海天津",0,2);
            //在数据量大的情况下的时候,可以使用循环操作
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //切记这里是一定要进行关闭的,因为,只有文件在close()或者flush()的时候,数据才能写入文件中,一会追源码
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
