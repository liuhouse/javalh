package javalh.chapter19;

import java.io.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class BufferedCopy_ {
    public static void main(String[] args) {
        /*
        * 应用案例
        * 综合使用BufferedReader 和 BufferedWriter 完成文本文件的拷贝,注意文件的编码
        * */
        //原文件
        String srcFilePath = "e:\\store.txt";
        String destFilePath = "e:\\store_copy.txt";

        //尝试拷贝二进制的文件
        //老刘说明
        //1.BufferedReader 和 BufferedWriter 是安装字符操作
        //2.不要去操作 二进制文件[声音,视频,doc,pdf,图片],可能会造成文件损坏

//        String srcFilePath = "e:\\kaola.jpg";
//        String destFilePath = "e:\\kaola2.jpg";

        //先读后写
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;

        try {
            //实例化读文件的Buffer对象
            bufferedReader = new BufferedReader(new FileReader(srcFilePath));
            //实例化写文件的Buffer对象
            bufferedWriter = new BufferedWriter(new FileWriter(destFilePath));
            //开始读取文件
            String line;
            while ((line = bufferedReader.readLine()) != null){
                //读取到文件之后,将文件写入目标文件
                bufferedWriter.write(line);
                //写进去之后进行换行,因为本身就是按照行读取的
                bufferedWriter.newLine();
            }

            System.out.println("文件拷贝成功~~~");

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //写完之后一定要关闭文件,因为数据是在文件关闭的时候写入的

                try {
                    //关闭读取的文件
                    if(bufferedReader != null){
                        bufferedReader.close();
                    }

                    //关闭新写入的文件
                    if(bufferedWriter != null){
                        bufferedWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
