package javalh.chapter10;

public class mainDetail {
    public static void main(String[] args) {
        /*
        * (1)main方法是虚拟机在调用
        * (2)java虚拟机需要调用类的main()方法,所以该方法的访问权限必须是public
        * (3)java虚拟机在执行main()方法的时候不必创建对象,所以该方法必须是static
        * (4)该方法在接收String参类型的数组参数,该数组中保存执行java命令时传递的所有运行的时候类型的参数
        * (5)java 执行的程序 参数1  参数2  参数3
        * */

        for(int i = 0 ; i < args.length ; i++){
            System.out.println(args[i]);
        }
    }
}
