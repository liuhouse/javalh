package javalh.chapter10.final_;

public class FinalDetail02 {
    public static void main(String[] args) {
        System.out.println(BBB.num);

        //包装类,String是final修饰的类,不能被继承,更加不能被重写
        String a = "a";

    }
}

//final和static往往搭配使用,效率更高,不会导致了类的加载,底层的编译器做了优化处理
/*
* 使用final修饰属性,读取的时候只会读取此属性,不会导致类的加载  效率更高
* 不适用final修饰属性,读取的时候会读取次属性,会导致类的加载
* */
class BBB{
    public final static int num = 10000;
    static {
        System.out.println("BBB的静态代码块被执行....");
    }
}

final class AAA{
    //一般来说.如果一个类已经是final类了,就没有必要再将方法修饰成final类
    //因为我们把方法使用final修饰的原因是不想被子类继承之后重写,但是如果类是final修饰的,已经被继承了,就不存在重写这么一说了
    public void cry(){

    }
}