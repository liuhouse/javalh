package javalh.chapter10.final_;

public class Final01 {
    public static void main(String[] args) {
        //基本介绍
        /*
        * final中文意思,最后的,最终的
        * final 可以修饰类,属性,方法和局部变量
        * 在某些情况下,程序员可能会有下面的需求,就会使用到final
        * （1）当不希望类被继承的时候,可以使用final关键字进行修饰
        * （2）当不希望父类的某个方法被子类重写/覆盖的时候,可以使用final关键字修饰
        * (3)当不希望类的某个属性的值被修改,可以使用final修饰
        * (4)当不希望某个局部变量被修改,可以使用final修饰
        * */

        E e = new E();
        //此时是可以改变的
//        e.TAX_RATE = 0.09;
//        System.out.println(e.TAX_RATE);

        F f = new F();
        f.cry();
    }
}

//A是能够被B继承的
//如果我们要求A类不能被其他类继承


//被final修饰的类不能被其他的类继承  也就是 A是最后一个类
final class A{}

//class B extends A{
//
//}


class C{
    //如果我们要求hi方法不能被子类重写
    //可以使用final关键字修饰hi方法

    //被final关键字修饰了之后,此方法将不能被重写
    public final void hi(){

    }
}

class D extends C{
//    @Override
//    public void hi() {
//        System.out.println("重写了C类的hi方法...");
//    }
}

//当不希望类的某个属性的值被修改的时候,可以使用final关键字修饰
class E{
    public final double TAX_RATE = 0.08;//常量
}

//当不希望某个局部变量被修改,可以使用final修饰
class F{
    public void cry(){
        final double  NUM = 0.01;
//        NUM = 0.9;
        System.out.println(NUM);
    }
}
