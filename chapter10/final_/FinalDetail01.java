package javalh.chapter10.final_;

public class FinalDetail01 {
    public static void main(String[] args) {

        /*
        * final的使用注意事项和细节讨论
        * (1)final修饰的属性又叫做常量,一般用XX_XX_XX来命名
        * (2)final修饰的属性在定义的时候必须初始化赋值,并且以后不能修改,赋值可以在如下位置之一
        *       1:定义时
        *       2:在构造器中
        *       3:在代码块中
        * (3)如果final修饰的属性是静态的,则初始化的位置只能是
        *       1:定义时
        *       2:静态代码块  不能再构造器中赋值
        * (4)final类不能被继承,但是可以实例化对象
        * (5)如果类不是final类,但是含有final方法,则该方法虽然不能被重写,但是可以被继承,并且可以正常使用
        * */


        CC cc = new CC();
        System.out.println(cc);

        //能被继承并且能够正常使用里面的final修饰方法
        EE ee = new EE();
        ee.cal();
    }
}


class AA{
    /*
    * 1:定义的时候赋值  比如  public final double TAX_RATE = 0.08
    * 2:在构造器中
    * 3:在代码块中赋值
    * 说明 ： 在构造器中赋值和在代码块中赋值是一样的,因为代码块是对构造器的补充
    * */

    public final double TAX_RATE = 0.08;//1：定义的时候赋值
    public final double TAX_RATE1;
    public final double TAX_RATE2;



    //2：构造器中赋值
    public AA(){
        TAX_RATE1 = 0.09;
    }

    //在代码块中赋值
    {
        TAX_RATE2 = 1.0;
    }
}

class BB{
    /*
    * 如果final修饰的属性是静态的,则初始化的位置只能是
    * 1：在定义的时候
    * 2:在静态代码块中,不能再构造器中赋值
    * */
    //在定义的时候赋值
    public static final double TAX_RATE = 0.09;

    public static final double TAX_RATE2;

    //在静态代码块中赋值
    //为什么只能在静态代码块中赋值呢，因为静态属性方法和代码块在加载类的时候就加载了,所以可以赋值
    //不能再构造方法中赋值 , 因为常量是必须有初始值的  但是   这个类不一样会被实例化

    static {
        TAX_RATE2 = 0.1;
    }

    //不能再构造器中赋值，静态属性在类加载的时候就加载了,所以必须赋值,但是普通的代码块,或者构造方法不会自动加载
//    BB(){
//        TAX_RATE2 = 0.5;
//    }


}

//final类不能被继承,但是可以被实例化
final class CC{

}


/*
* 如果类不是final类,但是里面含有final方法,则该方法虽然不能被重写,但是可以被继承,仍然遵守继承的机制
* */
class DD{
    public final void cal(){
        System.out.println("cal()方法....");
    }
}


class EE extends DD{

}