package javalh.chapter10.final_;

public class FinalExercise02 {
    public static void main(String[] args) {

    }

    //方法中的形参可以使用final修饰,表示这个形参传递过来就不能发生改变
    public int addOne(final int x){
        //++x;//这里是错误的,原因是不能修改final修饰的值
        return x+1;//这是正确的,因为这是返回了一个新的值
    }

}



