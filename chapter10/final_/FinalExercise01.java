package javalh.chapter10.final_;

public class FinalExercise01 {
    public static void main(String[] args) {
        /*
        * 需求：请编写一个程序,能够计算圆的面积,要求圆周率为3.14，赋值的位置3个方式都写一下
        * */
        Circle circle = new Circle(10);
        System.out.println(circle.area());
    }
}

class Circle{
    private double radius;
    //因为已经规定好圆周律为3.14所以不能改变,所以这里使用final修饰
    //第一种方式
    //private final double PI = 3.14;
    //第二种方式
//    private final double PI;
//    {
//        PI = 3.14;
//    }

    //第三种方式
    private final double PI;

    public Circle(double radius) {
        PI = 3.14;
        this.radius = radius;
    }

    public double area(){
        return PI*radius*radius;
    }
}
