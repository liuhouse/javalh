package javalh.chapter10.Db;
//B程序员实现Oracle数据库
//必须实现项目经理规定的数据库接口
public class OracleDB implements DBInterface{
    @Override
    public void connect() {
        System.out.println("Oracle数据库已经连接...");
    }

    @Override
    public void close() {
        System.out.println("Oracle数据库已经关闭...");
    }
}
