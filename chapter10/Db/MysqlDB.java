package javalh.chapter10.Db;
//A程序实现mysql数据库
//必须去实现项目经理规定的数据库接口
public class MysqlDB implements DBInterface{
    @Override
    public void connect() {
        System.out.println("mysql数据库已经连接...");
    }

    @Override
    public void close() {
        System.out.println("mysql数据库已经关闭...");
    }
}
