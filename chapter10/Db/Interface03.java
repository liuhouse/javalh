package javalh.chapter10.Db;

//实现根据调用不同的数据库达到不一样的效果
public class Interface03 {
    public static void main(String[] args) {
        //创建mysqlDb对象
        MysqlDB mysqlDB = new MysqlDB();
        work(mysqlDB);
        //创建Oracle对象
        OracleDB oracleDB = new OracleDB();
        work(oracleDB);
    }

    //创建一个数据库工作的方法
    //接收一个已经实现了DBInterface接口的类的对象
    public static void work(DBInterface db){
        db.connect();//这里使用了多态的数据绑定
        db.close();//这里使用了多态的数据绑定
    }
}
