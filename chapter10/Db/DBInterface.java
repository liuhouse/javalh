package javalh.chapter10.Db;
/*
* 项目经理规定数据库链接的接口
* */
public interface DBInterface {
    //规定数据库必须实现两个方法
    public void connect();//连接数据库
    public void close();//关闭连接
}
