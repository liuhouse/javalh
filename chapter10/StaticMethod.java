package javalh.chapter10;

public class StaticMethod {
    public static void main(String[] args) {

        /*
        * 当方法中不涉及到任何和对象相关的成员,则可以将方法涉及成静态方法,提高开发效率
        * 比如工具类中的方法  utils
        * Math类 Arrays类,Collections集合类看下源码
        *
        * 小结
        * 在程序员的实际开发中,往往会将一些通用的方法,设计成静态方法,这样我们就不需要创建对象就可以使用了
        * 比如打印一维数组,冒泡排序,完成某个计算任务
        * */

        //创建两个对象,交学费
        Stu tom = new Stu("tom");
        //tom.payFee(100);
        Stu.payFee(100);
        Stu mary = new Stu("mary");
        //mary.payFee(200);
        //因为payFee()是静态方法,所以可以直接使用  类名.静态类名方法调用
        Stu.payFee(200);

        //输出当前收到的总学费
        Stu.showFee();

        //如果我们希望不创建实例,也可以调用某个方法(即当做工具来使用)
        //这个时候,我们把方法做成静态的方法非常合适

        System.out.println("9开平方的结果是=" + Math.sqrt(9));

        //使用自定义的工具类计算两个数的和
        double v = MyTools.calSum(10, 20);
        System.out.println(v);

        double c = MyTools.calCha(10,20);
        System.out.println(c);

    }
}


//开发自己的工具类,可以将方法做成静态的,方便调用
class MyTools{
    //求出两个数的和
    public static double calSum(double n1 , double n2){
        return n1 + n2;
    }

    //计算两个数的差
    public static double calCha(double n1 , double n2){
        return n1 - n2;
    }

}


//学生交学费
class Stu{
    //学生的名字
    public String name;

    //学生的总缴费费用
    //定义一个静态变量,来累积学生的学费
    public static double fee = 0;


    public Stu(String name) {
        this.name = name;
    }

    //学生缴费的方法
    /*
    * (1)当方法使用static修饰之后,该方法就是静态方法
    * (2)静态方法就可以访问静态属性和静态变量    可以直接通过类名进行调用   可以当做工具类来使用
    * */
    public static void payFee(double fee){
        Stu.fee += fee;
    }

    //输出当前一共缴费的费用
    public static void showFee(){
        System.out.println(Stu.fee);
    }
}
