package javalh.chapter10;

public class VisitStatic {
    public static void main(String[] args) {
        //如何访问类变量
        //类名.类变量名   || 对象名.类变量名
        //推荐使用  类名.类变量名

        //类名.类变量名
        //说明：类变量是随着类的加载而创建,所以即使没有创建对象实例也可以访问
        System.out.println(A.name);

        //也可以通过 对象名.类变量名 访问
        A a = new A();
        System.out.println("a.name = " + a.name);

    }
}


class A{
    //类变量
    //类变量的访问,必须遵守相关的访问权限
    public static String name = "韩顺平教育";
    //普通属性/普通成员变量/非静态属性/非静态成员变量/实例变量
    private int num = 10;

}
