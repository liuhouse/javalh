package javalh.chapter10;

public class CodeBlockDetail04 {
    public static void main(String[] args) {
        //老师说明
        //(1)进行类的加载
        //1.1  先加载父类A02 , 1.2 在加载子类B02
        //(2)创建对象
        //2.1从子类的构造器开始加载
        new B02();
    }
}

class A02{//父类
    //静态属性初始化
    private static int n1 = getVal01();

    //静态代码块
    static {
        System.out.println("A02的一个静态代码块..");//(2)
    }

    //普通代码块
    {
        System.out.println("A02的一个普通代码块...");//(5)
    }

    //普通属性的初始化
    public int n3 = getVal02();


    //静态方法
    public static int getVal01(){
        System.out.println("getVal01");//(1)
        return 10;
    }

    //非静态方法
    public int getVal02(){
        System.out.println("getVal02");//(6)
        return 10;
    }

    //构造器
    public A02(){
        //实际上在实例化的时候
        //隐藏
        /*
        * (1)super()
        * (2)普通代码和普通属性的初始化
        * */
        System.out.println("A02的构造器");//(7)
    }


}

class B02 extends A02{//子类
    //静态属性初始化
    private static int n3 = getVal03();

    //静态代码块
    static {
        System.out.println("B02的一个静态代码块...");//(4)
    }

    //普通属性的初始化
    public int n5 = getVal04();

    {
        System.out.println("B02的一个普通代码块...");//(9)
    }


    public int getVal04(){
        System.out.println("getVal04");//(8)
        return 10;
    }

    //静态方法
    public static int getVal03(){
        System.out.println("getVal03");//(3)
        return 10;
    }

    //打印出
    //(1)静态成员sam初始化
    //(2)static块执行
    //(3)sam1成员初始化
    //(4)Test默认构造器函数被调用


    //构造器
    public B02() {
        //隐藏了
        //super()
        //普通代码块和普通属性的初始化
        System.out.println("B02的构造器...");//(10)
    }
}


class C02{
    //普通属性
    private int n1 = 100;
    //静态属性
    private static int n2 = 200;

    //普通方法
    private void m1(){}

    //静态方法
    private static void m2(){}

    //静态代码块
    static {
        //静态代码块只能调用静态成员方法
        //System.out.println(n1);//错误  因为n2是普通属性,在静态代码块中不能调用
        System.out.println(n2);//正确
        //m1();//错误的 静态代码块中不能调用非静态的方法
        m2();//正确的  静态代码块可以调用静态方法
    }

    //普通代码块
    {
        //普通代码块,可以使用任意成员
        System.out.println(n1);
        System.out.println(n2);
        m1();
        m2();
    }
}