package javalh.chapter10;

public class Main01 {

    //静态的变量/属性
    private static String name = "韩顺平教育";
    //非静态的变量/属性
    private int n1 = 10000;

    //静态方法
    public static void hi(){
        System.out.println("Main01的h1方法");
    }

    //非静态方法
    public void cry(){
        System.out.println("Main01的cry方法");
    }

    public static void main(String[] args) {
        /*
        * 概念
        * （1）在main()方法中,我们可以直接调用main()方法所在类的静态方法或者静态属性
        * （2）但是,不能直接访问该类中的非静态成员,必须创建该类的一个实例对象后,才能通过这个实例对象去访问该类中的非静态成员
        * */

        //可以直接使用name
        //1:静态main方法可以访问本类的静态成员
        System.out.println("name = " + name);
        hi();

        //2静态方法Main不可以访问本类的非静态成员
//        System.out.println("n1 = " + n1);//错误
//        cry()//错误的


        //3.静态方法main要访问本类的非静态成员，需要先创建对象,再调用即可
        Main01 main01 = new Main01();
        System.out.println(main01.n1);//ok
        main01.cry();

    }

}
