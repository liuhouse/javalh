package javalh.chapter10;

public class CodeBlockDetail03 {
    public static void main(String[] args) {
        /*
        * 构造器的最前面其实隐含了super()和普通代码块，
        * 静态相关的代码块,属性初始化,是在类加载的时候,就执行完毕了,因此是优先于构造器和普通代码块执行的
        * */

        /*
        * 执行顺序
        * (1)AAAA的普通代码块
        * (2)AAAA()构造器被调用...
        * */
//        AAAA aaaa = new AAAA();

        /*
        * AAA的普通代码块
        * AAA()构造器被调用...
        * BBBB的普通代码块...
        * BBBB()构造器被调用...
        * */
        BBBB bbbb = new BBBB();

    }
}


class AAAA{//父类是Object

    {
        System.out.println("AAA的普通代码块");
    }

    public AAAA() {
        //(1)super()
        //(2)调用本类的普通代码块
        System.out.println("AAA()构造器被调用...");
    }
}


class BBBB extends AAAA{
    {
        System.out.println("BBBB的普通代码块...");
    }

    public BBBB() {
        //(1)super()
        //(2)调用本类的普通代码块
        System.out.println("BBBB()构造器被调用...");
    }
}


