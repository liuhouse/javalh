package javalh.chapter10;

public class CodeBlockDetail01 {
    public static void main(String[] args) {

        /*
        * (1)static代码块也叫静态代码块,作用是对类进行初始化,而且他随着类的加载而执行,并且只会执行一次，如果是普通代码块,每创建一个对象
        *   就会执行一次
        * (2)代码块什么时候会被加载
        *   1:创建对象实例(new)
        *   2:创建子类对象实例的时候 ,父类也会被加载
        *   3:使用类的静态成员(静态属性,静态方法)
        * (3)普通代码块,在创建对象实例的时候,会被隐士的调用,被创建一次,就调用一次,如果只是使用类的静态成员时,普通代码块不会被执行
        *
        * 小结：
        * 1:static代码是类加载时候执行的,只会执行一次
        * 2:普通代码块是在创建对象的时候调用的,创建一次,调用一次
        * 3:类加载的3中情况,需要记住
        * */


        //类被加载的情况举例
        //下面的情况被执行后代码块都会被执行

        //静态代码块只会被加载一次

        //1:创建对象的实例的时候(new)
        //AA aa = new AA();
        //2:创建子类对象实例,父类也会被加载,而且,父类的代码块会先被加载,子类的后被加载
        //AA aa1 = new AA();
        //3:使用类的静态成员时(静态成员,静态方法),代码块会被执行，而且是首先执行的
        //System.out.println(Cat.n1);

        //static代码块,是在类加载的时候执行的,而且只会执行一次
        //new DD();
        //new DD();

        //普通的代码块,在创建对象实例的时候,就会被隐士的调用
        //被创建一次,就会调用一次
        //如果只是使用类的静态成员,普通代码块不会执行
        System.out.println(DD.n1);





    }
}


class DD{
    //静态属性
    public static int n1 = 8888;
    //静态代码块
    static {
        System.out.println("DD的静态代码块1被执行...");
    };

    //普通代码块
    {
        System.out.println("DD的普通代码块...");
    };

}


class Cat{
    //静态属性
    public static int n1 = 999;
    //静态代码块
    //普通代码块,在new对象的时候,被调用,而且是每创建一个对象,就调用一次
    //可以这样理解的,普通代码块就是构造器的补充
    static {
        System.out.println("Cat的静态代码块1被执行...");
    };
}


class BB{
    //静态代码块
    static {
        System.out.println("BB的静态代码块1被执行...");
    }
}

class AA extends BB{
    //静态代码块
    static {
        System.out.println("AA的静态代码块1被执行...");
    }
}

