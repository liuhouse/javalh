package javalh.chapter10;

public class CodeBlockDetail02 {
    public static void main(String[] args) {

        /*
        * 创建一个对象的时候,在一个类中调用顺序是：（重点,难点）
        * (1):调用静态代码块和静态属性初始化(注意代码块和静态属性初始化的调用的优先级是一样的,如果有多个静态代码块和多个静态变量的初始化,则按照他们
        *       的定义顺序调用)
        * (2):调用普通代码块和普通属性的初始化(注意：普通代码块和普通属性的初始化的调用优先级一样,如果有多个普通代码块和多个普通属性初始化,则按照定义顺序调用)
        * (3):最后才调用构造方法
        * */

        /*
        * 调用顺序
        * (1):A的静态代码块01
        * (2):getN1被调用...
        * (3):A的普通代码块01
        * (4):getN2被调用...
        * (5):AAA()构造器被调用
        * */
        AAA aaa = new AAA();

    }
}

class AAA{

    //普通代码块
    {
        System.out.println("A的普通代码块01");
    }

    //普通属性的初始化
    private int n2 = getN2();


    //静态代码块
    static {
        System.out.println("A的静态代码块01");
    };

    //静态属性的初始化
    private static int n1 = getN1();

    public static int getN1(){
        System.out.println("getN1被调用...");
        return 100;
    }

    public int getN2(){//普通方法/非静态方法
        System.out.println("getN2被调用...");
        return 200;
    }

    //无参构造器
    public AAA(){
        System.out.println("AAA()构造器被调用");
    }
}
