package javalh.chapter10.template;

/*
* 抽象类
* 模板设计模式
* */
abstract public class Template {

    //子类必须要使用的类
    abstract public void job();

    //多个类共同要调用的类
    public void calculateTime(){
        //获取开始时间[以毫秒为单位的当前时间]
        long start = System.currentTimeMillis();
        job();//多态绑定机制
        //获取结束时间
        long end = System.currentTimeMillis();
        System.out.println("执行时间为" + (end - start));
    }
}
