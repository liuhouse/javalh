package javalh.chapter10.template;

//AA类和BB类有一样的方法,所以这个时候感情的自然流露
//编写一个抽象的父类，将相同的代码放到父类   将子类必须实现的方法做成抽象方法   子类就必须实现此方法了
//那么其实这个模式   就称作    抽象类-模板设计模式

public class BB extends Template{

    //我们发现AA和BB都有共同的代码,有很多重复的代码,所以可以考虑将重复的代码提取出来

    //重写了父类的job
    @Override
    public void job(){
        long num = 1;
        for(int i = 1 ; i <= 800000 ; i++){
            num *= i;
        }
    }
}
