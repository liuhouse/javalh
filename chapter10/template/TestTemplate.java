package javalh.chapter10.template;

public class TestTemplate {
    public static void main(String[] args) {
        /*
        * 基本介绍
        * 抽象类体现的就是一种模板模式的设计,抽象类作为多个子类的模板,子类在抽象类的基础上进行扩展,改造,但是子类总体上会保留抽象类的行为方式
        *
        * (1)当功能内部一部分实现是确定的,一部分实现是不确定的,这个时候可以把不确定的部分暴露出去,让子类去实现
        * (2)编写一个抽象父类,父类提供了多个子类的通用方法,并把一个或者多个方法留给其子类实现,这就是一种模板模式
        * */

        AA aa = new AA();
        aa.calculateTime();

        BB bb = new BB();
        bb.calculateTime();
    }
}
