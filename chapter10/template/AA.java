package javalh.chapter10.template;

public class AA extends Template{

    //我们在这个时候发现AA类和BB类的job中有重复的代码,所以我们编写一个单独的方法进行复用

    //计算任务
    //1+...+ 800000
    @Override
    public void job(){
        long num = 1;
        for(long i = 0 ; i <= 800000 ; i++){
            num += i;
        }
    }
}
