package javalh.chapter10;

public class StaticMethodDetail {
    public static void main(String[] args) {

        /*
        * (1)类方法和普通方法都是随着类的加载而加载,将结构信息存储在方法区：
        * 类方法中无this的参数
        * 普通方法中隐藏着this的参数
        * (2)类方法可以通过类名调用,也可以通过对象名调用
        * (3)普通方法和对象有关,需要通过对象名调用,比如对象名.方法名(参数),不能通过类名直接调用
        * (4)类方法中不允许使用和对象有关的关键字,比如this,super.普通方法(成员方法)可以
        * (5)类方法(静态方法)中,只能访问静态变量和静态方法
        * (6)普通成员方法,即可以访问非静态成员,也可以访问静态成员
        * 小结：
        * 静态方法,只能访问静态成员,非静态的方法,可以访问静态成员和非静态成员（必须遵守访问权限）
        *
        * */

        //小结：静态方法可以访问静态成员和静态方法
        //小结：非静态方法可以访问静态成员和静态方法    非静态成员和非静态方法

        D.hi();//ok  因为是静态方法,可以直接调用
        //非静态方法,不能通过类名进行调用
//        D.say();
        //非静态方法,需要先创建对象,再调用
        new D().say();//可以的

    }
}

class D{
    //正常变量
    private int n1 = 100;
    //静态变量
    private static int n2 = 200;

    //非静态方法,普通方法
    public void say(){

    }

    //静态方法,类方法
    public static void hi(){
        //类方法中不允许使用和对象有关的关键字
        //比如 this 和 super . 普通方法(成员方法)可以
//        System.out.println(this.n1);
    }


    //类方法(静态方法)中,只能访问静态变量或者静态方法
    public static void hello(){
        //静态属性可以直接访问
        System.out.println(n2);
        //静态属性可以通过 类名.静态属性名访问
        System.out.println(D.n2);
        //静态方法中不能使用this
//        System.out.println(this.n2);
        //可以直接访问静态方法
        hi();
        //非静态方法不能访问
//        say();//错误的
    }

    //普通成员方法,即可以访问 非静态成员,也可以访问静态成员
    public void ok(){
        //访问非静态成员
        System.out.println(n1);
        //访问 非静态方法
        say();
        //访问静态成员
        System.out.println(n2);
        //访问静态方法
        hello();
    }
}
