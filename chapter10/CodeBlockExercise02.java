package javalh.chapter10;

public class CodeBlockExercise02 {
    //普通属性初始化
    Sample sam1 = new Sample("sam1成员初始化");//(3)sam1成员初始化
    //静态属性初始化
    static Sample sam = new Sample("静态成员sam初始化");

    //静态代码块
    static {
        System.out.println("static块执行");//(2)static块执行
        if(sam == null){
            System.out.println("sam is null");
        }
    };

    //构造器
    CodeBlockExercise02(){
        System.out.println("CodeBlockExercise02默认构造器函数被调用...");//(4)CodeBlockExercise02默认构造器函数被调用
    }

    public static void main(String[] args) {
        new CodeBlockExercise02();
    }
}

class Sample{

    //有参构造器
    Sample(String s){
        System.out.println(s);//(1)静态成员sam初始化
    }

    //无参构造器
    Sample(){
        System.out.println("Sample默认构造器被调用");
    }
}
