package javalh.chapter10;

public class ChildGame {
    public static void main(String[] args) {
        /*
        * 需求
        * 提出问题的主要目的就是让大家思考解决之道,从而引出我们要讲的知识点
        * 说：有一群小孩玩堆雪人,不时的有小孩加入,请问如何知道现在共有多少人在玩,编写程序解决
        *
        * 思路：
        * 1:在main方法中定义一个变量count
        * 2:当一个小孩加入游戏后 count++ , 最后这个count就是记录有多少个小孩玩游戏
        * */


        /*
        * 有些书是说在方法区...jdk版本有关系,记住一点
        * static变量是对象共享信息,不管static变量在哪里,共识
        * (1)static变量是同一个类所有的对象共享
        * (2)static类变量,在类加载的时候就生成了
        *
        *
        * */


        int count = 0;
        Child child1 = new Child("白骨精");
        child1.join();
        //count++;
        child1.count++;
        Child child2 = new Child("小钻风");
        child2.join();
        //count++;
        child2.count++;
        Child child3 = new Child("观音");
        child3.join();
        child3.count++;
        //count++;

        //类变量,可以通过类名来访问
        System.out.println("一共有" + Child.count + "个小朋友加入了游戏");

        //思考一下,下面的代码输出的是什么
        //静态变量是可以被本类的所有实例化对象共享的,也就是说所有的实例化对象都指向静态变量的同一个地址
        System.out.println("child1.count = " + child1.count);
        System.out.println("child2.count = " + child2.count);
        System.out.println("child3.count = " + child3.count);
    }
}


class Child{//类
    private String name;

    //定义一个变量 count , 是一个类变量(静态变量)static 静态修饰符
    //该变量最大的特点就时会被Child类所有的对象实例共享
    public static int count = 0;
    static public String offer = "程序员";

    public Child(String name){
        this.name = name;
    }

    public void join(){
        System.out.println(name + "加入了游戏");
    }
}


