package javalh.chapter10.abstract_;

public class AbstractDetail02 {
    public static void main(String[] args) {

    }
}


//(5)抽象类的本质还是类,所以可以有类的各种成员
abstract class D{
    public int n1 = 10;
    public static String name = "韩顺平教育";
    public void hi(){
        System.out.println("hi");
    }
    public abstract void hello();
    public static void ok(){
        System.out.println("ok");
    }

    //(6)抽象方法不能主体,即不能实现
//    public abstract void eat(){};//实现了就是错误的
}


//抽象方法不能使用private，final和static来修饰,因为这些关键字都是和重写相违背的
abstract class H{
    public abstract void hi();//抽象方法
}


//如果一个类继承了抽象类,则他必须实现抽象类的所有抽象方法,除非他自己也声明为abstract类
abstract class E{
    public abstract void hi();
}

//继承过来是会报错的,除非自己也是抽象类
abstract class F extends E{
    public abstract void hello();
}

//必须实现抽象类里面的所有抽象方法
class G extends F{
    @Override
    public void hi() {

    }

    @Override
    public void hello() {

    }
}

