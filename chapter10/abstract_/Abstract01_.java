package javalh.chapter10.abstract_;

public class Abstract01_ {
    public static void main(String[] args) {
        /*
        * 说明:当父类的某些方法,需要声明,但是不确定如何实现的时候,可以将其声明为抽象方法,那么这个类就必须是抽象类
        * */
    }
}


abstract class Animal{
    private String name;
    public Animal(String name){
        this.name = name;
    }

    /*
    * 思考：这里的eat  实现了什么？ 其实没有什么意义
    * 即：父类方法不确定性的问题
    * 考虑将该方法设计为抽象(abstract)方法
    * 所谓的抽象方法就是没有实现的方法
    * 所谓的没有实现,就是没有方法体
    * 当一个类中存在抽象方法的时候,需要将该类声明为抽象类(abstract)
    * 一般来说,抽象类会被继承,由子类来实现抽象方法
    * */
//    public void eat(){
//        System.out.println("这是一个动物,但是不知道吃什么...");
//    }

    public abstract void eat();
}

