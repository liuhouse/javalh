package javalh.chapter10.abstract_.employee;

public class AbstractExercise02 {
    public static void main(String[] args) {
        //测试
        Manager jacks = new Manager("jacks", 1, 20000);
        jacks.setBonus(8000);
        jacks.work();

        //普通员工
        CommonEmployee xl = new CommonEmployee("小李", 2, 10000);
        xl.work();

    }
}
