package javalh.chapter10.abstract_.employee;
/*
* 经理类
* */
public class Manager extends Employee{
    //奖金
    private double bonus;

    //构造方法
    public Manager(String name, int id, double salary) {
        super(name, id, salary);
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    //父类是抽象类,所以父类的抽象方法必须实现
    public void work() {
        System.out.println("经理" + getName() + "工作中");
    }
}
