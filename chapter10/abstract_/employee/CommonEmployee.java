package javalh.chapter10.abstract_.employee;
/*
* 普通员工
* */
public class CommonEmployee extends Employee{
    public CommonEmployee(String name, int id, double salary) {
        super(name, id, salary);
    }

    @Override
    //因为父类是抽象类,子类继承了抽象类,所以抽象类中的方法必须实现
    public void work() {
        System.out.println("员工" + getName() + "工作中...");
    }
}
