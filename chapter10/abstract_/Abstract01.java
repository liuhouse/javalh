package javalh.chapter10.abstract_;

public class Abstract01 {
    public static void main(String[] args) {
        //抽象类不能被实例化
//        new Animal01();
    }
}

//(1)用abstract关键字来修饰一个类,这个类就叫抽象类
//访问修饰符 abstract class 类名{}
abstract class Animal01{
    //(2)使用abstract关键字修饰的方法,那么这个方法就是抽象方法
    //访问修饰符 abstract 返回类型 方法名(参数列表);没有方法体
    //public abstract void cry();

    //(3)抽象类的价值更多的作用在于设计,是设计者设计好后,让子类继承实现的抽象类
    //(4)抽象类,是考官比较爱问的知识点,在框架和设计模式中使用较多

    public void eat(){}

//    public abstract void jump();
    /*
    * 抽象类使用的细节事项和细节讨论
    * (1)抽象类不能被实例化
    * (2)抽象类不一定要包含abstract方法,也就是说抽象类可以没有abstract方法，也可以有普通方法
    * (3)一旦包含了abstract方法,则这个类必须声明为abstract类
    * (4)abstract只能修饰类和方法,不能修饰属性和其他的
    * */

}


class Cat extends Animal01{
    //@Override
//    public void cry() {
//
//    }
}


//abstract只能修饰类和方法,不能修饰属性和其他的
class C{
    //public abstract int n1 = 1;
}

