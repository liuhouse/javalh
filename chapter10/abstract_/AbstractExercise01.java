package javalh.chapter10.abstract_;

public class AbstractExercise01 {
    public static void main(String[] args) {

    }
}

//题1 错误的,final是不能被继承的
//abstract final class A{}

//题2
abstract class B{
    //错误的 static关键字和重写无关
    //abstract public static void test2();

    //错误的 private修饰的方法不能被重写
    //abstract private void test3();
}

