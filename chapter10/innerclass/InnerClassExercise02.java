package javalh.chapter10.innerclass;

public class InnerClassExercise02 {
    public static void main(String[] args) {
        CellPhone cellPhone = new CellPhone();

        //老韩解读
        //1:传递的是实现了Bell接口的匿名内部类 InnerClassExercise02$1
        //2:重写了ring()
        cellPhone.alarmclock(new Bell(){
            public void ring(){
                System.out.println("懒猪起床了");
            }
        });

        cellPhone.alarmclock(new Bell() {
            @Override
            public void ring() {
                System.out.println("小伙伴上课了");
            }
        });
    }
}


//铃声接口
interface Bell{
    void ring();
}

//电话类
class CellPhone{
    public void alarmclock(Bell bell){//形参是Bell接口类型
        System.out.println(bell.getClass());
        bell.ring();//这里使用了多态的数据绑定
    }
}
