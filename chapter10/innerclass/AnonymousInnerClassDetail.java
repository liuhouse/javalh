package javalh.chapter10.innerclass;

/*
* 匿名内部类详情
* */
public class AnonymousInnerClassDetail {
    public static void main(String[] args) {
        Outer05 outer05 = new Outer05();
        System.out.println(outer05);
        outer05.f1();
    }
}


class Outer05{
    private int n1 = 99;
    public void f1(){
        //创建一个基于类的匿名内部类
        //匿名内部类
        new Person(){
            private int n1 = 100;
            //重写父类的Person里面的hi()方法
            @Override
            public void hi() {
                //可以直接访问类的所有成员,包括私有的
                //如果外部类和匿名内部类的成员重名的时候,匿名内部类访问的话
                //默认遵循就近原则,如果想访问外部类的成员,则可以使用(外部类名.this.成员)去访问
                System.out.println("匿名内部类重写了方法 n1=" + n1 + "外部的n1 = " + Outer05.this.n1);
                //这里的Outer05.this   就是调用f1的对象   outer05
                System.out.println("Outer05.this.hashCode = " + Outer05.this);
            }

            @Override
            public void ok(String str) {
                super.ok(str);
            }
        }.ok("jack111");

        //调用内部类的第二种方式
        //可以直接调用,因为匿名内部类本身返回的也是对象

        //调用内部类中的第一种方式
        //动态数据绑定  运行类型是Outer05$1
        //person.hi();

    }
}

//类
class Person{
    public void hi(){
        System.out.println("Person hi()");
    }

    public void ok(String str){
        System.out.println("Person ok() + " + str);
    }
}
//抽象类/接口...
