package javalh.chapter10.innerclass;

//成员内部类的使用
public class MemberInnerClass {
    public static void main(String[] args) {
        Outer08 outer08 = new Outer08();
        //outer08.printInner08();

        //外部其他类,使用成员内部类的三种方式
        //老韩解读
        //这就是一个语法,不用特别的纠结
        //最终是属于Inner08类的
//        Outer08.Inner08 inner08 = outer08.new Inner08();
//        inner08.say();

        //第二种方式,在外部类中，编写一个方法,可以返回Inner08对象
        Outer08.Inner08 inner08Instance = outer08.getInner08Instance();
        inner08Instance.say();


    }
}

//外部类
/*
* 说明：成员内部类是定义在外部类的成员位置,并且没有static修饰符
* */
class Outer08{
    private int n1 = 10;
    public String name = "张三";

    //成员内部类
    //1:可以直接访问外部类的所有成员,包含私有的
    //2:可以添加任意的访问修饰符(public protected 默认 private)，因为它的地位就是一个成员【因为把成员内部类当做成员来对待,所以普通成员有的,它也有】
    //3:作用域,和外部类的其他成员一样,为整个类体,比如前面的案例,在外部类的成员方法中创建内部类的对象
    //      ---如果全部都使用的是静态变量或者静态方法的时候,那么就可以直接进行访问
    //4:成员内部类 - 访问 外部内部类(比如属性) [访问方式：直接访问]
    //5:外部类 - 访问  -- 成员内部类(说明)  【访问方式,在外部类中的方法中创建内部类对象,然后访问】 隔山打牛

    //如果外部类和内部类的成员重名时,内部类访问的话,默认遵循就近原则,如果想要访问外部类的成员的时候,就可以使用 (外部类型.this.成员)去访问
    public class Inner08{
        private int n1 = 90;
        public void say(){
            System.out.println("Outer08的n1 = " + n1 + "Outer08的name=" + name + "外部类的n1 = " + Outer08.this.n1);
        }
    }

    public void printInner08(){
        Inner08 inner08 = new Inner08();
        inner08.say();
    }


    //方法,返回一个Inner08实例
    public Inner08 getInner08Instance(){
        return new Inner08();
    }

}
