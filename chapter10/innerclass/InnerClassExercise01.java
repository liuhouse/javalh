package javalh.chapter10.innerclass;

public class InnerClassExercise01 {
    public static void main(String[] args) {
        //因为f1(IL il)里面的编译类型是IL，所以要传递 编译类型为IL的实例化对象
        //所以既然是对象  那么传递一个匿名对象也可以啊 【匿名类 】

        //使用匿名类的好处是  这里的匿名类是一次性的   用完即走   修改的话 只需要修改这里的一点代码就可以了
       f1(new IL(){
           public void show(){
               System.out.println("我是一幅名画~~");
           }
       });


       //使用传统方法
        //使用传统方式的不足是    如果只用一次,就会造成资源的浪费    如果多次 ,  那么修改一个地方的代码   其他地方也就全部修改了
        f1(new Picture());

    }


    //静态方法,形参是接口类型
    public static void f1(IL il){
        il.show();
    }
}


//接口
interface IL{
    void show();
}


class Picture implements IL{

    @Override
    public void show() {
        System.out.println("这真的是一副名画~~~");
    }
}

