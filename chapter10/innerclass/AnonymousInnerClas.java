package javalh.chapter10.innerclass;

public class AnonymousInnerClas {
    public static void main(String[] args) {
//        Tirger tirger = new Tirger();
//        tirger.cry();
//        Dog dog = new Dog();
//        dog.cry();

        Outer04 outer04 = new Outer04();
        outer04.method();
    }
}

/*
* 匿名内部类的使用
* (1)本质是类
* (2)内部类
* (3)该类没有名字
* (4)同时还是一个对象
* 说明：匿名内部类是定义在外部类的局部位置,比如方法中,并且没有类名
* 1：匿名内部类的基本语法
* new 类或接口(参数列表){
*       类体
* };
* */

class Outer04{//外部类
    private int n1 = 10;//属性
    public void method(){//方法
        //基于接口的匿名内部类
        //老韩解读
        //1:需求,想要使用IA接口,并创建对象
        //2:传统方式,就是写一个类,实现该接口,并创建对象即可
        //3:老韩的需求是Tiger/Dog类只使用一次,后面不再使用
        //4:可以使用匿名内部类来简化开发
        //5:tiger的编译类型?IA
        //6:tiger的运行类型?就是匿名内部类  Outer04$1
        /*
        * 其实这个匿名内部类,表面上看没有名字,但是实际上在jdk的底层,是分配了一个内部类的名称的
        * 我们看底层会分配类名 Outer04$1
        * 也就是相当于
        * class Outer04$1 implements IA{
        *   @Override
        *   public void cry(){
        *       System.out.println("老虎叫唤");
        *   }
        * }
        * 7：jdk底层在创建匿名内部类的Outer04$1，立即马上就创建了Outer04$1实例,并且把地址返回给tiger
        * 8:匿名内部类使用一次,就不能再使用
        * */


        //这就是一个匿名内部类
        IA tiger = new IA() {
            @Override
            public void cry() {
                System.out.println("老虎叫唤...");
            }
        };

        //System.out.println("tiger的运行类型 = " + tiger.getClass() );
        //tiger.cry();
        //tiger.cry();

        //声明一个小狗的内部类
        IA dog = new IA() {
            @Override
            public void cry() {
                System.out.println("小狗汪汪叫....");
            }
        };
        //System.out.println("dog的运行类型是 = " + dog.getClass());
        //dog.cry();



        //==============================================================================

        //演示基于类的匿名内部类
        //分析
        //1:father的编译类型是Father
        //2:father的运行类型是Outer04$3
        //3:底层会创建匿名内部类
        /*
            class Outer04$3 extends Father{
                @Override
                public void test(){
                    System.out.println("匿名内部类重写了父类的方法...");
                }
            }

        * */
        //4:同时也直接返回了匿名内部类Outer04$3的对象
        //5:注意("jack")参数列表会传递给构造器

        //这就代表一个匿名内部类
        //没有后面的方法体,就是一个普通的对象
        //如果有后面的方法体,就是一个匿名内部类
        Father father = new Father("jack"){
            //匿名内部类里面可以重写父类的方法
            @Override
            public void test() {
                System.out.println("匿名内部类重写了父类的方法...");
            }
        };
        System.out.println("father对象的运行类型是=" + father.getClass());//Outer04$3
        father.test();


        //================================================================================
        //基于抽象类的匿名内部类
        Animal animal = new Animal(){
            @Override
            public void eat(){
                System.out.println("小狗吃骨头");
            }
        };
        System.out.println("animal的运行类型是 = " + animal.getClass());
        animal.eat();
    }
}

interface IA{//接口
    public void cry();
}


//在外部如何使用IA,直接使用类进行实现即可
class Tirger implements IA{
    @Override
    public void cry() {
        System.out.println("老虎叫唤...");
    }
}

class Dog implements IA{

    @Override
    public void cry() {
        System.out.println("小狗汪汪叫...");
    }
}



class Father{//类
    //构造器
    public Father(String name){
        System.out.println("接收到 name =" + name);
    }

    //方法
    public void test(){

    }
}


//抽象类
abstract class Animal{
    //抽象方法
    abstract void eat();
}
