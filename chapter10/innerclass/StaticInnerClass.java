package javalh.chapter10.innerclass;

/*
* 静态内部内的使用
* */
public class StaticInnerClass {
    public static void main(String[] args) {
        Outer10 outer10 = new Outer10();
        //outer10.m1();


        //外部其他类   访问   静态内部类
        //方式1
        //因为静态内部类,是可以通过类名直接进行访问的(前提是要满足访问权限)
        Outer10.Inner10 inner10 = new Outer10.Inner10();
        inner10.say();

        //方式2
        //编写一个方法,可以返回静态内部类的对象实例
        Outer10.Inner10 inner101 = outer10.getInner10();
        inner101.say();

        //方式3
        //直接使用静态的方式去调用
        System.out.println("*******************************");
        Outer10.Inner10 inner10_ = Outer10.getInner10_();
        inner10_.say();


    }
}


//外部类
/*
* 说明：静态内部类是定义在外部类的成员位置,并且有static控制
* 1:可以直接访问外部类的所有成员,包含私有的,但是不能直接访问非静态成员
* 2:可以添加任意的访问修饰符(public,protected,默认,private)因为他的的地位就是一个成员
* 3:作用域：同其他的成员,为整个类体
* 4:静态内部类->访问外部类(比如：静态属性)【访问方式 ： 直接访问所有的静态成员】
* 5:外部类->访问->静态内部类 访问方式：创建对象，再访问
* 6:外部其他类 访问 静态内部类
* 7：如果外部类和静态内部类的成员重名的时候,静态内部类访问的时候,默认遵循就近原则,如果想访问外部类的成员,则可以使用(外部类名.成员)去访问
* */
class Outer10{
    //普通属性
    private static int n1 = 10;
    private static String name = "张三";
    private static void cry(){}

    //静态成员内部类  5  5
    static class Inner10{
        private static int n1 = 20;
        private static String name = "韩顺平教育";
        public void say(){
            System.out.println("n1 =" + n1);//错误，因为n1不是静态属性
            System.out.println("name = " + name + "外部类的n1 = " + Outer10.n1 );//可以直接访问,因为name是静态属性
            cry();
        }
    }


    //5:外部类->访问->静态内部类 访问方式：创建对象，再访问
    public void m1(){
        Inner10 inner10 = new Inner10();
        inner10.say();
    }



    //返回静态内部类实例
    public Inner10 getInner10(){
        return new Inner10();
    }

    //返回静态内部类的实例
    public static Inner10 getInner10_(){
        return new Inner10();
    }

}


