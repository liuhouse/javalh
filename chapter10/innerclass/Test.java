package javalh.chapter10.innerclass;

//外部类
public class Test {
    //构造器
    public Test(){
        Inner s1 = new Inner();
        s1.a = 10;
        Inner s2 = new Inner();
        System.out.println(s2.a);
    }

    //内部类,成员内部类
    class Inner{
        public int a = 5;
    }

    public static void main(String[] args) {
        Test t = new Test();//5
        Inner r = t.new Inner();
        System.out.println(r.a);//5
    }
}
