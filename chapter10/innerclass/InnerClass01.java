package javalh.chapter10.innerclass;

public class InnerClass01 {
    public static void main(String[] args) {
        /*
        * 内部类的基本概念
        * 如果定义类在局部位置(方法中,代码块中)
        * (1):局部内部类 (2):匿名内部类
        *
        * 基本介绍
        * 一个类的内部又完整的嵌套了另外一个类结构,被嵌套的类成为内部类(inner class),
        * 嵌套其他类的类成为外部类(outer class)，是我们类的第五大成员(思考,类的五大成员是哪些)
        * 属性,方法,构造器,代码块,内部类，内部类的最大特点就时可以直接访问私有属性,并且可以体现类与类之间的包含关系,注意,内部类是学习的难点
        * 同时也是重点,后面看底层原码的时候,有大量的内部类
        *
        * 内部类的分类
        * 定义在外部类的局部位置上(比如方法内)
        * (1)局部内部类(有类名)
        * (2)匿名内部类(没有类名,重点！！！)
        *
        * 定义在外部类的成员位置上
        * (1)成员内部类(没有static修饰)
        * (2)静态内部类(使用static修饰)
        * */
    }
}

//外部类
class Outer{
    private int n1 = 100;//属性
    public Outer(int n1){//构造器
        this.n1 = n1;
    }

    public void m1(){//方法
        System.out.println("m1()");
    }
    //代码块
    {
        System.out.println("代码块...");
    }

    //内部类 ， 在Outer类的内部
    class Inner{

    }

}
