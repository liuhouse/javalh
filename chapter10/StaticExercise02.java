package javalh.chapter10;

public class StaticExercise02 {
    public static void main(String[] args) {
        //这样是可以的,因为静态方法可以直接通过类名进行调用
        System.out.println("Number of total is" + Person.getTotalPerson());//0
        Person person = new Person();
        System.out.println("Number of total is" + Person.getTotalPerson());//1
    }
}

class Person{
    //非静态属性
    private int id;
    //静态属性
    private static int total = 0;
    //静态方法
    public static int getTotalPerson(){
        //id++;//这是错误的,因为静态方法只能访问静态方法和静态属性
        return total;
    }

    //构造器
    public Person(){
        //这样是可以的,因为,非静态的方法,可以访问所有的成员的属性和方法
        total++;//total = 1
        id = total;//id = 1
    }


}
