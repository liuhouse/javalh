package javalh.chapter10;

public class StaticDetail {
    public static void main(String[] args) {
        B b = new B();
        //访问普通变量
//        System.out.println(B.n1);
        System.out.println(b.n1);
        //静态变量也是可以直接通过实例化对象访问的   因为对象共享此属性
        System.out.println(b.n2);

        //直接访问静态属性
        System.out.println(B.n2);//200

        //静态变量是类在加载的时候,就创建了.所以我们没有创建对象实例
        //也是可以通过 类名.类变量名来访问
        System.out.println(C.address);
    }
}

class B{
    //普通属性
    public int n1 = 100;
    //静态属性
    public static int n2 = 200;
}


class C{
    public static String address = "北京";
}
