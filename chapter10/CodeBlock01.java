package javalh.chapter10;

public class CodeBlock01 {
    public static void main(String[] args) {
        Movie movie = new Movie("你好,李焕英");
        System.out.println("==========================");
        Movie movie2 = new Movie("唐探3", 100);
    }


}

class Movie{
    private String name;
    private double price;
    private String director;


    //一个需求,每次在初始化电影的时候都要做一些前置工作

    /*
    * 老韩解读
    * (1)下面的三个构造器都有相同的语句
    * (2)这样代码看起来比较冗余
    * (3)这个时候我们可以把相同的语句,放入到一个代码块中,即可
    * (4)这样当我们不管调用哪个构造器,创建对象的时候,都会首先调用代码块中的内容
    * (5)代码块的调用顺序优先于构造器
    * */

    //(1)相当于是另外一中形式的构造器,这是对构造器的补充机制,可以做初始化的操作
    //(2)场景,如果多个构造器中有重复的语句,可以抽取到初始化块中,提高代码的重用性
     {
        System.out.println("电影屏幕打开...");
        System.out.println("广告开始...");
        System.out.println("电影正是时候...");
    };

    public Movie(String name) {
        System.out.println("Movie(String name)被调用");
        this.name = name;
    }

    public Movie(String name, double price) {
        System.out.println("Movie(String name, double price)被调用...");
        this.name = name;
        this.price = price;
    }

    public Movie(String name, double price, String director) {
        System.out.println("Movie(String name, double price, String director)被调用...");
        this.name = name;
        this.price = price;
        this.director = director;
    }
}
