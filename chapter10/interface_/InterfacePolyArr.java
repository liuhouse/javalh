package javalh.chapter10.interface_;
/*
* 多态数组
* */
public class InterfacePolyArr {
    public static void main(String[] args) {
        //多态数组 --> 接口类型数组
        USB[] usbs = new USB[2];
        //第一个数组中存放电话
        usbs[0] = new Phone_();
        //第二个数组中存放相机
        usbs[1] = new Camera_();

        for(int i = 0 ; i < usbs.length ; i++){
            usbs[i].work();
            //这里不能直接调用call方法,因为相机类没有call()方法,编译类型是Usb,编译的时候是没有call方法的,所以编译不能通过,如果想通过
            //这里必须使用向下转型
            //和前面的一样我们仍然要进行类型的向下转型
            if(usbs[i] instanceof Phone_){
                ((Phone_)usbs[i]).call();
            }
        }
    }
}


/*
* 演示一个案例：给Usb数组中,存放Phone和相机对象,Phone类还有一个特有的方法call(),请遍历Sub()数组,如果是Phone对象,除了调用Usb接口定义的方法外,
* 还需要调用Phone特有的方法call
* */

interface USB{
    void work();
}

class Phone_ implements USB{
    public void call(){
        System.out.println("开始打电话了...");
    }

    @Override
    public void work() {
        System.out.println("电话开始工作了...");
    }
}

class Camera_ implements USB{

    @Override
    public void work() {
        System.out.println("相机开始工作了...");
    }
}