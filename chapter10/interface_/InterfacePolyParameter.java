package javalh.chapter10.interface_;

public class InterfacePolyParameter {
    public static void main(String[] args) {
        //接口的多态体现
        //接口类型的变量 Ia01 可以指向 实现了Ia接口类型的对象实例
        //向上转型
        //编译类型 Ia   运行类型  Monster
        Ia Ia01 = new Monster();
        //编译类型 Ia 运行类型 Car
        Ia01 = new Car();

        //继承的多态体现
        //使用父类的变量指向继承AAA的子类的对象实例
        //编译类型是AAA  运行类型是BBB
        //向上转型
        AAA a = new BBB();
        a = new CCC();


    }
}

//接口Ia
interface Ia{}
class Monster implements Ia{}
class Car implements Ia{}


class AAA{}
class BBB extends AAA{}
class CCC extends AAA{}


