package javalh.chapter10.interface_;

//usb接口
public interface UsbInterface {
    //规定接口的相关方法,老师规定的,即规范...
    public void start();//启动
    public void stop();//停止
}
