package javalh.chapter10.interface_;

public class InterfaceDetail02 {
    public static void main(String[] args) {
        //老韩证明  接口中的属性是 public static final
        System.out.println(IB.n1);//只有静态变量才能使用类名直接调用,所以这里就证明n1是static的
        //IB.n1 = 10;//因为不能再次赋值,就证明n1变量是final修饰的
    }
}

//(5)一个类可以同时实现多个接口  -- 一旦实现了多个接口,那么这些接口中的抽象方法也都必须实现
//(6)接口中的属性,只能是final的,而且是public static final 修饰符,比如,int = 1 实际上就是  public static final int a = 1;(必须初始化)
//(7)接口中属性的访问形式：接口名.属性名  IB.n1
//(8)接口不能继承其他的类,但是可以继承多个别的接口
//(9)接口的修饰符只能是public和默认,这点和类的修饰符是一样的
class Dog implements IB,IC{

    @Override
    public void hi() {

    }

    @Override
    public void say() {

    }
}

interface IB{
    //接口中的属性,只能是final的,而且必须是public static final 修饰符
    int n1 = 10;//等价 public static final int n1 = 10;
    void hi();
}

interface IC{
    void say();
}

//接口可以继承别的接口
interface IF extends IB,IC{
    public void hello();
}

//一个类继承了一个接口之后,那么就必须实现这个接口以及这个接口父接口的所有的抽象的方法
class E implements IF{

    @Override
    public void hi() {

    }

    @Override
    public void say() {

    }

    @Override
    public void hello() {

    }
}

class A{}
class B{}

//接口不能继承其他的类,但是可以继承多个别的接口
//interface IE extends A,B{
//
//}


