package javalh.chapter10.interface_;

public class InterfaceExercise02 {
    public static void main(String[] args) {
        new C_().pX();
    }
}


interface A_{
    //因为是在接口中定义的属性
    //所以就应该立马想到 public static final int x = 0;
    int x = 0;
}

class B_{
    //这就是一个普通属性
    int x = 1;
}

//C_类继承了B_类,实现了A_类
class C_ extends B_ implements A_{
    public void pX(){
        //这里是错误的,因为继承的B_类中有一个x属性   实现的A_接口中也有一个x   ， 那么这里就不知道x到底指的是哪个x
        //专业术语     对x的引用不明确
//        System.out.println(x);

        //如果修改
        //可以明确的指定x
        //访问接口A的x,就使用A_.x
        //使用父类的x,就使用super.x
        System.out.println("接口A的x = " + A_.x + "使用父类B_的x" + super.x);
    }
}
