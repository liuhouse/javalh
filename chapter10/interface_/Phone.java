package javalh.chapter10.interface_;

//Phone类实现了UsbInterface接口
//解读： 1:即Phone类需要实现UsbInterface接口 所 规定的/声明的方法
public class Phone implements UsbInterface{
    @Override
    public void start() {
        System.out.println("手机开始工作....");
    }

    @Override
    public void stop() {
        System.out.println("手机停止工作....");
    }
}
