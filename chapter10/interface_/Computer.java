package javalh.chapter10.interface_;
//电脑类
public class Computer {
    //要将手机或者相机插入到电脑上
    //所以电脑要工作

    //接口引用可以指向实现了接口的类的对象
    public void work(UsbInterface usbInterface){
        usbInterface.start();
        usbInterface.stop();
        //多态数据绑定
    }
}
