package javalh.chapter10.interface_;

public class Interface01 {
    public static void main(String[] args) {
        //创建手机,相机对象
        //Phone实现了UsbInterface
        Phone phone = new Phone();
        //Camera实现了UsbInterface
        Camera camera = new Camera();

        //创建计算机
        Computer computer = new Computer();

        computer.work(phone);//这里使用了面向对象的多态的数据绑定,谁调用,就去谁那边查找   查找出执行完成之后返回

        System.out.println("===================");
        computer.work(camera);


    }
}
