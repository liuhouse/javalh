package javalh.chapter10.interface_;

interface AInterface01 {
    //其实这些里面有自己的方法,也有自己的属性,已经实现的方法可以直接使用,没有实现的方法,需要在子类进行实现而且必须实现
    //自己的属性
    public int n1 = 10;
    //自己的方法【此方法不实现】
    //接口中没有实现的方法都是抽象的   写不写abstract都是一样
    public void eat();

    //自己的方法【此方法是要实现的】
    default  public void cry(){
        System.out.println("我在接口中,但是我可以实现...但是我需要使用default关键字进行修饰...");
    }

    //自己的方法【此方法是要实现的】
    static void haha(){
        System.out.println("我在接口中,但是我可以实现...但是我需要是静态的方法...");
    }

}
