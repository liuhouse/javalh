package javalh.chapter10.interface_.extendvsInterface;

public class ExtendsVsInterface {
    public static void main(String[] args) {

        /*
        * 接口和继承解决的问题不同
        * 继承的价值主要在于,解决代码的复用性和可维护性
        * 接口的价格只要在于,设计,设计好各种规范(方法),让其子类去实现这些方法,要更加的灵活
        *
        * 接口比继承更加的灵活
        *  接口比继承更加灵活,继承是满足is - a 的关系    猫是动物  狗是动物
        * 接口只需满足like  - a 的关系    猴子像鸟一样飞翔   猴子像鱼一样游泳
        *
        * 接口在一定的程度上实现了代码解耦 (即：接口规范性 + 动态绑定机制)
        * */

        LittleMonkey littleMonkey = new LittleMonkey("孙悟空");
        littleMonkey.climbing();
        littleMonkey.flying();
        littleMonkey.swimming();
    }
}
