package javalh.chapter10.interface_.extendvsInterface;
/*
* 猴子类
* */
public class Monkey {
    private String name;
    //构造器
    public Monkey(String name){
        this.name = name;
    }

    //猴子类特有的方法
    public void climbing(){
        System.out.println(name + "会爬树...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
