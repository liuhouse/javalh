package javalh.chapter10.interface_.extendvsInterface;

//当子类继承了父类之后,就自动拥有父类的功能,并且是可以直接使用的 -- {孙悟空继承了猴子类,所以生下来就会爬树}
//如果子类需要扩展功能,就可以通过实现接口的方式扩展
//可以理解为 实现接口 是对 java 单继承机制的一种补充

//如果继承不能完全满足条件的时候,那么就使用接口继承来补充想要的功能

//现在有一个需求 -- 孙悟空想通过学习实现飞翔和游泳的本事,但是这个猴子类本身是没有的，所以这个时候就要通过实现接口去实现功能了
public class LittleMonkey extends Monkey implements Birdable,Fishable{
    //构造方法
    public LittleMonkey(String name) {
        super(name);
    }

    @Override
    public void flying() {
        System.out.println(getName() + "通过学习,可以像鸟儿一样飞翔...");
    }

    @Override
    public void swimming() {
        System.out.println(getName() + "通过学习,可以像鱼儿一样游泳...");
    }
}
