package javalh.chapter10.interface_.extendvsInterface;
/*
* 鱼类接口
* */
public interface Fishable {
    //鱼是可以游泳的
    void swimming();
}
