package javalh.chapter10.interface_;

public class InterFaceExercise01 {
    public static void main(String[] args) {
        //一个类实现了一个接口,那么这个类就拥有这个接口中的所有属性,并且可以直接访问
        //实现相当于是一种更加高级的继承   添加了一些规则

        BB bb = new BB();//ok
        System.out.println(bb.a);//23
        System.out.println(AA.a);//23
        System.out.println(BB.a);//23
    }
}


interface AA{
    //等价于public static final int a = 23;
    int a = 23;
}

//这是正确的,因为AA里面并没有抽象方法
class BB implements AA{

}
