package javalh.chapter10.interface_;
//接口的基本介绍
public class Interface02 implements AInterface01{

    public static void main(String[] args) {

        /*
        * 接口就是给出一些没有实现的方法,封装到一起,到某个类要使用的时候,在根据具体情况把这些方法写出来
        * 语法
        * interface 接口名{
        *   //属性
        *   //方法
        * }
        *
        *
        * class 类名  implements 接口{
        *      自己的属性
        *      自己的方法
        *      必须实现接口中的抽象方法
        * }
        *
        *
        * 接口是更加抽象的抽象的类,抽象类里面的方法可以有方法体,接口里的所有方法都没有方法体[jdk7.0].接口体现了程序设计的多态和高内聚低耦合的设计思想
        * 特别说明：
        * jdk8.0后接口类可以有静态方法,默认方法,也就是说接口中可以有方法的具体实现
        *
        *
        * */

        System.out.println(Interface02.n1);
        Interface02 interface02 = new Interface02();
        interface02.eat();
        interface02.cry();
        AInterface01.haha();
    }


    //既然实现了接口,那么就要实现里面的所有的未实现的方法【也就是没有方法体的方法】

    @Override
    public void eat() {
        System.out.println("我正在吃牛肉...");
    }



}
