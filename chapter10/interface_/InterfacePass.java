package javalh.chapter10.interface_;
/*
* 演示多态传递现象
* */
public class InterfacePass {
    //接口类型的变量可以指向实现了该接口类型的对象实例
    IG ig = new Teacher();
    //如果IG继承了IH接口,而Teacher类实现了IG接口
    //那么,实际上就相当于Teacher也实现了IH接口,所以Teacher需要将IG,IH的两个接口里面的抽象方法都要实现
    //这就是所谓的 接口多态传递现象
    IH ih = new Teacher();

}

interface IH{
    void hi();
}
interface IG extends IH{}
class Teacher implements IG{

    @Override
    public void hi() {

    }
}