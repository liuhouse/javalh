package javalh.chapter10.interface_;
/*
* 接口的注意事项和使用细节
* */
public class InterfaceDetail01 {
    public static void main(String[] args) {
        //new IA();
    }
}

//3:一个普通类实现接口,就必须将该接口的所有的抽象方法都实现了,可以使用alt+enter来解决
class Cat implements IA{

    //实现的接口只能是public进行修饰  实现的方法   去除掉public修饰符会报错
    //attempting to assign weaker access privileges
    //正在尝试分配较弱的访问权限   public -> 默认  降级了  这是不允许的
    @Override
    public void say() {

    }

    @Override
    public void hi() {

    }
}


//1:接口不能被实例化
//2:接口中所有的方法都是public方法,接口中抽象方法,可以不用abstract修饰,因为默认就是抽象的
//3:一个普通类实现接口,就必须将该接口的所有的抽象方法都实现了,可以使用alt+enter来解决
//4:抽象类去实现接口的时候,可以不实现接口的抽象方法


interface IA{
    void say();//修饰符public  并且是抽象的   abstract
    public abstract void hi();
}


//4:抽象类去实现接口的时候,可以不实现接口的抽象方法
abstract class Tiger implements IA{

}

