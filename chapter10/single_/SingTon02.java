package javalh.chapter10.single_;

public class SingTon02 {
    public static void main(String[] args) {
        //new Cat(); // 构造器私有化之后就不能被实例化了
        //当调用Cat.n1的时候,这个类中的所有静态属性和静态方法,静态代码块都会被调用
        //但是里面的构造方法并不会被调用,因为构造方法只有是类的对象被实例化之后才会被调用
        System.out.println(Cat.n1);
        Cat instance = Cat.getInstance();
        System.out.println(instance);

        //再次进行调用
        Cat instance1 = Cat.getInstance();
        System.out.println(instance1);

        System.out.println(instance == instance1);
    }
}

//希望在程序运行的过程中,只能创建一个Cat对象
//使用单例模式 懒汉式
class Cat{
    private String name;
    public static int n1 = 999;
    //定义一个static的静态属性对象
    //这里为什么是静态呢,因为要在getInstance()方法中使用
    private static Cat cat;

    /*
    * 步骤
    * 1:将构造器私有化
    * 2:定义一个static静态属性对象
    * 3:提供一个public的static方法,可以返回一个Cat对象
    * 4:懒汉式,只有当用户使用getInstance的时候,才创建并返回真正的Cat对象,后面再次调用的时候,就会返回上次创建的Cat对象
    * 从而保证了单例
    * */

    private Cat(String name){
        System.out.println("构造器被调用...");
        this.name = name;
    }

    public static Cat getInstance(){
        //如果没有,就创建Cat对象
        if(cat == null){
            //因为cat对象是一个静态变量,所以是存在在堆中的,发生改变后所有的对象是共享的,只有类被销毁之后才会跟着销毁
            //也就是实例化之后发生了改变之后,cat就存在值了
            cat = new Cat("小可爱");
        }
        return cat;
    }


    //对toString方法进行重写
    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                '}';
    }
}