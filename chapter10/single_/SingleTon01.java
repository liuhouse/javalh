package javalh.chapter10.single_;

/*
* 单例模式
* 1：饿汉式
* */
public class SingleTon01 {
    public static void main(String[] args) {
        //下面的形式并不是单例模式,因为单例模式只能创建一个对象
        //如果构造器私有化了,那么这边就不能使用new 创建一个对象了
//        GirlFriend xh = new GirlFriend("小红");
//        GirlFriend xb = new GirlFriend("小白");

        //通过方法可以获取对象
        //所以你的女朋友永远是 小红红 了
        GirlFriend instance = GirlFriend.getInstance();
        System.out.println(instance);
        GirlFriend instance1 = GirlFriend.getInstance();
        System.out.println(instance1);

        //就相当于是类在加载的时候已经给你分配了一个变量n1 = 100  和 一个女朋友gf  你已经拥有了,但是深入交流不交流那是你的事情

        //为什么称为 饿汉式 呢  因为,当类在调用里面的n1的时候,其实也将里面的静态属性gf 也创建好了,也就是进行了实例化
        //因为我们知道,当类进行初始化或者调用静态属性的时候,就会吧所有的静态属性以及静态代码块都执行了
        //按照这个例子来说的话,其实我只想要访问的n1,但是还是实例化了一个GirlFriend对象,但是实际上我没有用他,但是
        //还是给送来了,有点迫不及待的感觉  ，   所以称作饿汉
        System.out.println(GirlFriend.n1);
        System.out.println(instance == instance1);

    }
}

//有一个类,GirlFriend
//单例模式：此类只能实例化一个对象
//也就是一个人只能有一个女朋友
class GirlFriend{
    private String name;
    public static int n1 = 100;

    //在类的内部直接创建对象
    //为了能够在静态方法中,返回gf对象,需要将其修饰为static
    //静态方法中只能访问静态成员【静态属性和静态方法】

    //这边返回的永远是一个对象,因为静态成员属性在类加载的时候就已经加载好了
    //也就是在类加载的时候已经分类了一块内存,值是小红红,后面无论再怎么调用,都不会变了,始终都会是小红红
    //因为静态变量加载好之后,就不会再执行了,也就不会在在改变了


    private static GirlFriend gf = new GirlFriend("小红红");

    //如何保障我们只能创建一个GirlFriend对象
    //步骤【单例模式-饿汉式】
    /*
    * 1：将构造器私有化
    * 2：在类的内部直接创建对象(该对象是static)
    * 3：提供一个公共的static方法,返回gf对象
    * */

    //构造器
    //私有化的构造器
    private GirlFriend(String name){
        System.out.println("构造器被调用...");
        this.name = name;
    }

    //这边要使用静态方法的原因是,要直接使用类来调用此方法
    //如果现在这个类是一个普通方法,那么就必须使用实例化对象来进行调用,但是因为此时是单例模式
    //构造方法已经是私有的构造方法了,所以不能new

    //返回的永远是一个对象
    public static GirlFriend getInstance(){
        return gf;
    }

    @Override
    public String toString() {
        return "GirlFriend{" +
                "name='" + name + '\'' +
                '}';
    }
}
