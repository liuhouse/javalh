package javalh.chapter10;

public class StaticExercise03 {
    public static void main(String[] args) {

        /*
        * 小结：记住两句话
        * (1)静态方法,只能访问静态成员
        * (2)非静态方法,可以访问所有的成员
        * (3)在编写代码的时候,仍然要遵守访问权限
        * */

        Person1.setTotalPerson(3);
        new Person1();
        System.out.println(Person1.total);//4
    }
}

class Person1{
    private int id;
    protected static int total = 0;

    public static void setTotalPerson(int total){
        //this.total = total;//这是错误的,因为在static方法,不能使用this关键字
        Person1.total = total;//这样是可以的,静态属性可以直接通过类名进行访问
    }

    //无参构造器
    public Person1(){
        total++;
        id = total;
    }


}
