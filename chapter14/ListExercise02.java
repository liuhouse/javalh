package javalh.chapter14;

import java.util.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ListExercise02 {
    public static void main(String[] args) {
        /*
        * 需求
        * 使用List的实现类添加三本图书，并遍历,打印出如下的效果
        * 名称 : xx   价格 : xx    作者:xx
        * 名称 : xx   价格 : xx    作者:xx
        * 名称 : xx   价格 : xx    作者:xx
        * 要求：
        * (1)按照价格排序,从低到高(使用冒泡排序)
        * (2)要求使用ArrayList,LinkedList和Vector三种集合实现
        * */

        //List list = new ArrayList();
        //List list = new LinkedList();
        List list = new Vector();
        list.add(new Book1("西游記",999,"吳承恩"));
        list.add(new Book1("紅樓夢",18.8,"曹雪芹"));
        list.add(new Book1("三國演義",998,"羅貫中"));

        //使用冒泡排序
        //bubbleSort(list);

        //使用迭代器进行遍历
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Object next =  iterator.next();
            System.out.println(next);
        }
    }


    //使用冒泡排序法对对象进行排序
    public static void bubbleSort(List list){
        for(int i = 0 ; i < list.size() - 1 ; i++){
            for(int j = 0 ; j < list.size() - 1 - i ; j++){
                //进行向下转型
                Book1 book1 = (Book1)list.get(j);
                Book1 book2 = (Book1)list.get(j+1);
                if(book1.getPrice() > book2.getPrice()){
                    list.set(j,book2);
                    list.set(j+1,book1);
                }
            }
        }
    }
}


class Book1{
    //图书名称
    private String name;
    //价格
    private double price;
    //作者
    private String author;

    public Book1(String name, double price, String author) {
        this.name = name;
        this.price = price;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "名称 :" +  name + "\t\t\t价格 :"  + price + "\t\t\t作者:" + author+"\n";
    }
}
