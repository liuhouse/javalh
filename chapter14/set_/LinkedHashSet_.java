package javalh.chapter14.set_;

import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedHashSet_ {
    public static void main(String[] args) {
        /*
        * (1)在LinkedHashSet中维护了一个hash表和双向链表[linkedHashSet有head和tail]
        * (2)每一个节点都有before和after属性,这样可以形成双向链表
        * (3)再添加一个元素的时候,先求hash值,再求索引,确定该元素再table的位置,然后将添加的元素加入到竖向链表(如果已经存在,不添加(原则和hashset一样))
        * tail.next = newElement//示例代码
        * newElement.prev = tail
        * tail = newElement
        * (4)这样的话,我们遍历LinkedHashSet也能确保插入的顺序和遍历的顺序一致
        * */

        Set set = new LinkedHashSet();
        set.add(new String("AA"));
        set.add(456);
        set.add(456);
        set.add(new Customer("刘",1001));
        set.add(123);
        set.add("HSP");
        System.out.println(set);
    }
}

//

class Customer{
    private String name;
    private int no;

    public Customer(String name, int no) {
        this.name = name;
        this.no = no;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", no=" + no +
                '}';
    }
}