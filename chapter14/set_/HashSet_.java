package javalh.chapter14.set_;

import java.util.HashSet;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HashSet_ {
    public static void main(String[] args) {
        /*
        *1： HashSet实现了Set接口
        *2： HashSet的底层实际上是HashMap
            public HashSet() {
                map = new HashMap<>();
            }
        *3:可以存放null值,但是只能有有一个null
        * 4：HashSet不保证元素是有序的,取决于hash后,再确定索引的结果(即，不保证存放元素的顺序和取出的顺序一致)
        * 5:不能有重复的元素/对象,前面在讲Set的时候已经讲过了
        * */

        //老韩解读
        /*
        * 构造器走的源码
            public HashSet(){
                map = new HashMap<>();

             }

        *2:HashSet可以存放null,但是只能有一个null，即元素不能重复
        * */

        HashSet hashSet = new HashSet();
        hashSet.add(null);
        hashSet.add(null);
        System.out.println("HashSet = " + hashSet);
    }
}
