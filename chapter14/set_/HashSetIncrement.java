package javalh.chapter14.set_;

import java.util.HashSet;
import java.util.Objects;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HashSetIncrement {
    public static void main(String[] args) {
        //分析HashSet的扩容和转成红黑树机制
        /*
        * (1)HashSet底层是HashMap,第一次添加的时候,table数组扩容到16，临界值(threshold)是16*加载因子(loadFactor)是0.75 = 12
        * (2)如果table数组使用到了临界值,就会扩容,16*2 = 32 , 新的临界值就是32*0.75 = 24，以此类推
        * (3)在java8中,如果一条链表的元素到达 TREEIFY_THRESHOLD（默认是8）,并且table的大小 >= MIN_TREEIFY_CAPACITY(默认64)
        * 就会进行树化(红黑树),否则仍然采用数组扩容机制
        * */

        HashSet hashSet = new HashSet();
        //演示自动扩容,演示链条 --


//        for(int i = 0 ; i <=100 ; i++){
//            hashSet.add(new B(i));
//        }




//        for(int i = 0 ; i < 12 ; i++){
//            hashSet.add(new B(i));
//        }



        /*
        * 当我们向hashSet增加一个元素，->加入Node -> 加入table,就算是增加了一个size++
        * */

        //在table的某一条链表上添加了7个对象

        //当我们向hashSet增加一个元素，不论这个元素是加入Node还是加入table，计算是增加了一个size   size++
        for(int i = 0 ; i < 7 ; i++){
            hashSet.add(new A(i));
        }

        //在table的另外一条链表上添加了7个B对象
        for(int i = 0 ; i < 7 ; i++){
            hashSet.add(new B(i));
        }

    }
}


class B{
    private int n;
    public B(int n){
        this.n = n;
    }

    @Override
    public int hashCode() {
        return 200;
    }
}


class A{
    private int n;

    public A(int n) {
        this.n = n;
    }

    @Override
    public int hashCode() {
        return 100;
    }
}



