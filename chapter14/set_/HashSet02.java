package javalh.chapter14.set_;

import java.util.HashSet;
import java.util.Objects;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HashSet02 {
    public static void main(String[] args) {
        /*
        * 定义一个Employee类,该类包含:private成员属性 name,sal,birthday(MyDate类型)
        * 其中birthday为MyDate类型(属性包括:year,month,day)要求:
        * 1.创建3个Employee放入HashSet中
        * 2.当name和birthday的值相同的时候,认为是相同的员工,不能添加到HashSet集合中
        * */

        /*
        * 
        * */

        HashSet hashSet = new HashSet();
        hashSet.add(new Employee1("jack",18,new MyDate(1995,12,6)));
        hashSet.add(new Employee1("rose",19,new MyDate(1996,11,16)));
        hashSet.add(new Employee1("jack",19,new MyDate(1995,12,6)));
        System.out.println("hashSet = " + hashSet);
    }
}

class Employee1{
    private String name;
    private double sal;
    private MyDate birthday;

    public Employee1(String name, double sal, MyDate birthday) {
        this.name = name;
        this.sal = sal;
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee1 employee1 = (Employee1) o;
        return Objects.equals(name, employee1.name) &&
                Objects.equals(birthday, employee1.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birthday);
    }

    @Override
    public String toString() {
        return "Employee1{" +
                "name='" + name + '\'' +
                ", sal=" + sal +
                ", birthday=" + birthday +
                '}';
    }
}

class MyDate{
    private int year;
    private int month;
    private int day;

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public String toString() {
        return "MyDate{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyDate myDate = (MyDate) o;
        return year == myDate.year &&
                month == myDate.month &&
                day == myDate.day;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, day);
    }
}
