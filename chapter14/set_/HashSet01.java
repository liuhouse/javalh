package javalh.chapter14.set_;

import java.util.HashSet;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HashSet01 {
    public static void main(String[] args) {
        HashSet set = new HashSet();

        //说明
        //1.在执行add方法之后,会返回一个boolean值
        //2.如果添加成功,就返回true，否则就返回false
        //3.可以通过remove指定删除哪个对象

        System.out.println(set.add("john"));//true
        System.out.println(set.add("lucy"));//true
        System.out.println(set.add("john"));//false 因为集合中已经有一个了
        System.out.println(set.add("jack"));//true
        System.out.println(set.add("Rose"));//true

        //删除元素
        set.remove("john");
        System.out.println("set = " + set);

        System.out.println("========================");
        //对Set集合进行初始化
        set = new HashSet();
        System.out.println("set = " + set);//0

        //4:HashSet不能添加相同的元素/数据?
        set.add("lucy");//添加成功了
        set.add("lucy");//加入不了
        //因为是两个独立的对象,所以是可以添加成功的
        set.add(new Dog("tom"));//加入成功
        set.add(new Dog("tom"));//加入成功

        //再加深一下,非常经典的面试题
        //看源码做分析,先给小伙伴们留一个坑,以后讲源码,你就了然
        //去看看他的源码,看看add到底发生了什么
        set.add(new String("hsp"));//ok
        set.add(new String("hsp"));//加入不了

        System.out.println("set = " + set);



    }
}



//定义一个狗类
class Dog{
    private String name;

    public Dog(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                '}';
    }
}
