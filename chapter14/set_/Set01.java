package javalh.chapter14.set_;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Set01 {
    public static void main(String[] args) {
        /*
        * Set接口基本介绍
        * (1)无序(添加和取出的顺序不一致),没有索引
        * (2)不允许有重复元素,所以最多包含一个null
        * (3)JDK Api中Set接口的实现类
        * AbstractSet ， ConcurrentHashMap.KeySetView ， ConcurrentSkipListSet ， CopyOnWriteArraySet ，
        * EnumSet ， HashSet ， JobStateReasons ， LinkedHashSet ， TreeSet
        *
        *
        * Set接口的遍历方式
        * 同Collection的遍历方式,因为Set接口是Collection接口的子接口
        * (1)可以使用迭代器
        * (2)可以使用增强for循环
        * (3)不能使用索引进行遍历   因为Set是没有索引
         * */

        /*

        * 老韩解读：
        * 1：以Set接口实现类的HashSet来讲解Set接口的方法
        * 2:set接口的实现的类的对象(Set的接口对象),不能存放重复的元素,可以添加一个null
        * 3:set接口对象存放数据是无序的(即添加的顺序和取出的顺序是不一致的)
        * 4:注意：取出的顺序的顺序虽然不是添加的顺序,但是是固定的
        * */


        HashSet set = new HashSet();
        set.add("john");
        set.add("lucy");
        set.add("john");//重复
        set.add("jack");
        set.add("hsp");
        set.add("mary");
        set.add(null);
        set.add(null);//再次添加null
        //System.out.println("set = " + set);
        //输出是有序的,不论输出多少次都是一样的
//        for(int i = 0 ; i < 10 ; i++){
//            System.out.println("set = " + set);
//        }

        set.remove(null);


        //遍历
        //方式1：使用迭代器
        System.out.println("=====使用迭代器======");
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Object obj =  iterator.next();
            System.out.println("obj = " + obj);
        }

        //使用增强for循环====
        System.out.println("======增强for循环=====");
        for (Object o : set) {
            System.out.println(o);
        }

        //set接口对象,不能通过索引来获取



    }
}
