package javalh.chapter14.set_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HashSetSource_ {
    public static void main(String[] args) {
        //HashSet底层机制说明
        /*
        * 分析HashSet的底层是HashMap,HashMap的底层是(数组+链表+红黑树)
        * 为了让大家真正理解,老韩模拟简单的数组+链表结构
        *
        * 1：先获取元素的哈希值(hashCode方法)
        * 2：对哈希值进行运算,得出一个索引值即为要存放在哈希表中的位置号【存放在表中的哪个位置】
        * 3:如果该位置上没有其他元素,则直接存放
        * 如果该位置上已经有其他的元素,则要进行equals判断,如果相等,则不能添加,如果不相等,则以链表的方式添加
        *
        *
        * (1):HashSet底层是HashMap
        * (2):添加一个元素的时候,先得到hash值-会转化成对应的索引值
        * (3):找到存储数据表table，看这个索引位置是否已经存放的有元素
        * (4):如果没有,直接加入
        * (5):如果有,调用equals比较,如果相同,就放弃添加,如果不相同,则添加到最后
        * (6):在java8中,如果链表的元素个数到达TREEIFY_THRESOLD(默认是8),并且table的大小 >= MIN_TREEIFY_CAPACITY(默认64),
        * 就会进行树化(红黑树)
        *
        *
        * */

        //1：创建一个数组,数组的类型是Node[]
        //2:有些人,直接把Node[]数组称为表

        Node[] table = new Node[16];
        //3:创建节点
        Node john = new Node("john", null);

        table[2] = john;

        //刚创建好节点其实跟任何的节点以及数组都是没有关系的
        Node jack = new Node("jack", null);

        john.next = jack;//将jack节点挂载到john

        Node rose = new Node("Rose", null);
        jack.next = rose;//将rose节点挂载到jack

        //给数组的下标为3的元素添加一个节点
        Node lucy = new Node("lucy", null);
        table[3] = lucy;//将lucy放到table表的索引为3的位置

        for (Object o : table) {
            System.out.println(o);
        }





    }
}

//节点,存储数据,可以指向下一个节点,从而形成链表
class Node{
    Object item;//存放数据
    Node next;//指向下一个节点
    public Node(Object item, Node next) {
        this.item = item;
        this.next = next;
    }
}
