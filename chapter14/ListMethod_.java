package javalh.chapter14;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ListMethod_ {
    public static void main(String[] args) {
        //List接口的常用方法
        List list = new ArrayList();
        list.add("张三丰");
        list.add("贾宝玉");

        //在index = 1 的位置插入一个对象
        list.add(1,"韩顺平");
        System.out.println("list = " + list);

        //addAll(int index,Collection eles):从index位置开始将eles中的所有元素添加进来
        List list2 = new ArrayList();
        list2.add("jack");
        list2.add("tom");
        list.addAll(1,list2);
        System.out.println(list);

        //Object.get(int index),返回集合中指定位置的元素
        System.out.println(list.get(3));

        //indexOf(obj),返回obj在集合中首次出现的位置
        System.out.println(list.indexOf("jack"));
        //lastIndexOf(obj);返回obj在当前集合中末次出现的位置
        System.out.println(list.lastIndexOf("韩顺平"));

        list.add("韩顺平");
        System.out.println(list.lastIndexOf("韩顺平"));

        //remove(index); 移除指定index位置的元素,并且返回此元素
        Object remove = list.remove(0);
        System.out.println(remove);
        System.out.println(list);

        //set(index,elem);设置指定的index位置的元素为elem，相当于是替换
        list.set(0,"玛丽");
        System.out.println(list);

        //List subList(int fromIndex , int toIndex):返回fromIndex到toIndex位置的子集合
        //注意返回的子集合, fromIndex <= subList < toIndex

        List list1 = list.subList(0, 1);
        System.out.println(list1);



    }
}
