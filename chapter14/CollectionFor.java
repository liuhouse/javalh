package javalh.chapter14;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author 刘皓
 * @version 1.0
 */
public class CollectionFor {
    public static void main(String[] args) {
        //Collection接口遍历对象方式 2 for-循环增强
        Collection co = new ArrayList();
        co.add(new Book("西游记","吴承恩",23));
        co.add(new Book("红楼梦","曹雪芹",24));
        co.add(new Book("三国演义","罗贯中",25));

        for(Object o : co){
            System.out.println(o);
        }



    }
}
