package javalh.chapter14;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ArrayListDetail {
    public static void main(String[] args) {
        /*
        * (1) permits all elements,including null,ArrayList 可以加入Null，并且使可以是多个
        * (2) ArrayList是由数组来实现数据存储的（看源码）
        * (3) ArrayList 基本上是等同于 Vector , 除了ArrayList是线程不安全(执行效率高）看源码
        *  在多数情况下,不建议使用ArrayList
        *
        *
        * */

        List list = new ArrayList();
        list.add(null);
        list.add("jack");
        list.add(null);
        System.out.println(list);
    }
}
