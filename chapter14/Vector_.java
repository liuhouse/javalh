package javalh.chapter14;

import java.util.Arrays;
import java.util.Vector;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Vector_ {
    public static void main(String[] args) {





        //(1)Vector类的定义说明
        //(2)Vector底层也是对象数组,protected Object[] elementData
        //(3)Vector是线程同步的,即线程安全,Vector类的操作方法带有synchronized
        //(4)在开发中,需要线程同步安全的时候,考虑使用Vector

        //无参构造器
        //有参构造器
        Vector vector = new Vector(8);
        for(int i = 0 ; i < 10 ; i++){
            vector.add(i);
        }

        vector.add(100);

        System.out.println("vector=" + vector);

        //老韩解读源码
        /*
        1.new Vector()
        public Vector() {
            this(10);
        }
         补充，如果是
          Vector vector = new Vector(8);
           public Vector(int initialCapacity) {
             this(initialCapacity, 0);
           }
           其实底层最终走的方法都是一样的

           2.vector.add(i);
           //2.1下面这个方法就是添加数据到vector集合

            public synchronized boolean add(E e) {
                modCount++;
                add(e, elementData, elementCount);
                return true;
            }

             private void add(E e, Object[] elementData, int s) {
                if (s == elementData.length)
                    elementData = grow();
                elementData[s] = e;
                elementCount = s + 1;
            }

            2.2 确定是否需要扩容

            private void add(E e, Object[] elementData, int s) {
                if (s == elementData.length)
                    elementData = grow();
                elementData[s] = e;
                elementCount = s + 1;
            }

              private Object[] grow() {
                    return grow(elementCount + 1);
                }


            2.3真正的进行扩容操作
            如果需要的数组大小不够用,就扩容,扩容的算法
              int newLength = Math.max(minGrowth, prefGrowth) + oldLength;
                if (newLength - MAX_ARRAY_LENGTH <= 0) {
                    return newLength;
                }
                的到的结果就是扩容的两倍


                private Object[] grow(int minCapacity) {
                int oldCapacity = elementData.length;
                int newCapacity = ArraysSupport.newLength(oldCapacity,
                        minCapacity - oldCapacity,
                capacityIncrement > 0 ? capacityIncrement : oldCapacity

                return elementData = Arrays.copyOf(elementData, newCapacity);




        * */

    }
}
