package javalh.chapter14.HomeWork06_;

import java.util.HashSet;
import java.util.Objects;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork06_ {
    public static void main(String[] args) {
        /*
        * 下面的代码输出什么
        * 老韩提示,这道题是很有意思的,稍不注意就会掉进陷阱
        * 已知:Person类按照id和name重写了hashCode()方法和equals()方法,问下面的代码输出什么
        * */
        HashSet set = new HashSet();//ok
        Person p1 = new Person(1001, "AA");//ok
        Person p2 = new Person(1002, "BB");//ok
        set.add(p1);//ok
        set.add(p2);//ok
        p1.name = "CC";
        System.out.println(set);//2
        //这里移除的是AA,CC的是保留的,只是移除了链表中的一个值,其实节点还是在
        set.remove(p1);
        System.out.println(set);//2

        set.add(new Person(1001,"CC"));
        System.out.println(set);//3

        set.add(new Person(1001,"AA"));
        System.out.println(set);//4

        set.add(new Person(1001,"AA"));
        System.out.println(set);//4



    }
}

class Person{
    public int num;
    public String name;

    public Person(int num, String name) {
        this.num = num;
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return num == person.num &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(num, name);
    }
}
