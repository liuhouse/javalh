package javalh.chapter14;

import java.util.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ListFor_ {
    public static void main(String[] args) {
        /*
        * List 接口实现子类 Vector LinkedList
        * List list = new ArrayList();
        * List list = new Vector();
        * */
        List list = new LinkedList();



        list.add("jack");
        list.add("tom");
        list.add("鱼香肉丝");
        list.add("北京烤鸭");

        //遍历

        //1:使用迭代器进行遍历
        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            Object next = iterator.next();
            System.out.println(next);
        }


        System.out.println("========增强for============");
        //2:增强for
        for (Object o : list) {
            System.out.println(o);
        }

        System.out.println("============普通for==============");
        //普通for
        for(int i = 0 ; i < list.size() ; i++){
            System.out.println(list.get(i));
        }

    }
}
