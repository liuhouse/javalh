package javalh.chapter14;

/**
 * @author 刘皓
 * @version 1.0
 */
//LinkedList底层结构
public class LinkedList_ {
    public static void main(String[] args) {
        /*
        * LinkedList的全面说明
        * (1)LinkedList底层实现了双向链表和双端队列的特点
        * (2)可以添加任意元素(元素可以重复),包括null
        * (3)线程不安全,没有实现同步
        * */

        /*
        * LinkedList的底层操作机制
        * (1)LinkedList底层维护了一个双向链表
        * (2)LinkedList中维护了两个属性fist和last分别指向首节点和尾结点
        * (3)每个节点(Node对象),里面又维护了prev,next,item三个属性,其中是通过prev指向前一个,通过next指向后一个节点,最终实现书香篱笆白哦
        * (4)所有LinkedList的元素的添加和删除,不是通过数组来完成的,相对来说效率比较高
        * (5)模拟一个简单的双向链表
        * */

        /*
        * 模拟一个简单的双向链表
        * */
        //定义第一个节点
        Node jack = new Node("jack");
        //定义第二个节点
        Node tom = new Node("tom");
        //定义第三个节点
        Node hsp = new Node("hsp");

        //连接三个节点,形成双向列表
        //jack -> tom -> hsp 【正向链接】
        jack.next = tom;
        tom.next = hsp;

        //hsp -> tom -> jack
        hsp.pre = tom;
        tom.pre = jack;

        //让first引用指向jack，就是双向链表的头结点
        Node first = jack;
        //让last引用指向hsp，就是双向链表的尾结点
        Node last = hsp;

        //演示,从头到尾进行遍历
        System.out.println("======从头到尾进行遍历====");
        while (true){
            if(first == null){
                break;
            }
            //输出first的信息
            System.out.println(first);
            first = first.next;
        }

        //演示,从尾到头进行遍历
//        System.out.println("=======从尾到头进行遍历=======");
//        while(true){
//            if(last == null){
//                break;
//            }
//            //输出last的信息
//            System.out.println(last);
//            last = last.pre;
//        }


        //演示链表的添加对象/数据,是多么的方便
        //要求，是在tom----hsp 之间 插入一个smith

        //1:先创建一个Node节点,name = smith
        Node smith = new Node("smith");
        //下面就是把smith加入到双向链表了
        smith.next = hsp;
        smith.pre = tom;
        hsp.pre = smith;
        tom.next = smith;

        //让fist再次指向jack
        //让first引用指向jack，表示的就是双向链表的头结点
        //这里是尽心了指针的初始化
        first = jack;
        System.out.println("====从头到尾进行遍历=====");
        while (true){
            if(first == null){
                break;
            }
            System.out.println(first);
            first = first.next;
        }




    }
}

//定义一个Node类,Node对象,表示双向链表的一个结点
class Node{
    public Object item;//存放真正的数据
    public Node next;//指向后一个结点
    public Node pre;//指向前一个结点
    //构造方法
    public Node(Object name){
        this.item = name;
    }

    @Override
    public String toString() {
        return "Node name = " + item;
    }
}
