package javalh.chapter14;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 刘皓
 * @version 1.0
 */
public class List_ {
    public static void main(String[] args) {
        //1:List集合类中的元素是有序的(即添加顺序和取出顺序一致),可以重复
        List list = new ArrayList();
        list.add("jack");
        list.add("tom");
        list.add("mary");
        list.add("marry");
        list.add("hsp");
        System.out.println("list = " + list);

        //2.List集合中每个元素都有与其对应的顺序索引，即支持索引
        //索引是从0开始的
        System.out.println(list.get(3));
        //获取第一个元素
        System.out.println(list.get(0));
    }
}
