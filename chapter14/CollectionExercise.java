package javalh.chapter14;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author 刘皓
 * @version 1.0
 */
public class CollectionExercise {
    /*
    * 需求
    * 1：创建3个Dog{name,age}对象,放入到ArrayList中,赋给List引用
    * 2：用迭代器和增强for循环两种方式来遍历
    * 3:重写Dog的toString方法,输出name和age
    * */

    public static void main(String[] args) {
        List dogList = new ArrayList();
        dogList.add(new Dog("小迪",20));
        dogList.add(new Dog("阿黄",3));
        dogList.add(new Dog("小花",5));

        //使用迭代器来遍历
        Iterator iterator = dogList.iterator();
        while (iterator.hasNext()){
            Object next = iterator.next();
            System.out.println(next);
        }

        //使用增强for循环来遍历
        for(Object o : dogList){
            System.out.println(o);
        }


    }
}


class Dog{
    private String name;
    private int age;

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}



