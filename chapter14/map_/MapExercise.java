package javalh.chapter14.map_;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapExercise {
    public static void main(String[] args) {
        /*
        * 需求：使用HashMap添加三个员工对象,要求
        * 键:员工id
        * 值员工对象
        * 并遍历显示工资>18000的员工(变脸方式至少两种)
        * 员工类:姓名,工资,员工id
        * */
        Map hashMap = new HashMap();
        Emp xiaoli = new Emp("小李", 15000, 1);
        Emp xiaoming = new Emp("小明", 19000, 2);
        Emp xiaozhang = new Emp("小张",28000,3);
        hashMap.put(xiaoli.getId(),xiaoli);
        hashMap.put(xiaoming.getId(),xiaoming);
        hashMap.put(xiaozhang.getId(),xiaozhang);

        //进行遍历
        //使用增强for循环

        //使用增强for循环
        System.out.println("使用增强for循环===========");
        for(Object key: hashMap.keySet()){
            Emp o = (Emp) hashMap.get(key);
            if(o.getSalary() > 18000){
                System.out.println(o);
            }
        }


        //使用迭代器进行循环
        System.out.println("使用迭代器进行遍历================");
        Iterator iterator = hashMap.keySet().iterator();
        while (iterator.hasNext()) {
            Emp o = (Emp) hashMap.get(iterator.next());
            if(o.getSalary() > 18000){
                System.out.println(o);
            }
        }


        //使用entryset进行遍历
        Set entryset = hashMap.entrySet();
        System.out.println("使用entrySet来进行遍历=========");
        for(Object entry : entryset){
            Map.Entry m = (Map.Entry) entry;
            Emp em = (Emp)m.getValue();
            if(em.getSalary() > 18000){
                System.out.println(em);
            }
        }

        //使用第三种遍历方式
        Iterator iterator1 = hashMap.entrySet().iterator();
        while (iterator1.hasNext()) {
            Map.Entry entry =  (Map.Entry)iterator1.next();
            Emp value = (Emp)entry.getValue();
            if(value.getSalary() > 18000){
                System.out.println(value);
            }
        }


    }
}
class Emp{
    private String name;
    private double salary;
    private int id;

    public Emp(String name, double salary, int id) {
        this.name = name;
        this.salary = salary;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", id=" + id +
                '}';
    }
}
