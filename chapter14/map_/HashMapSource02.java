package javalh.chapter14.map_;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author 刘皓
 * @version 1.0
 * 模拟HashMap触发扩容,树化情况,并Debug验证
 */
@SuppressWarnings({"all"})
public class HashMapSource02 {
    public static void main(String[] args) {

        //(1)hashMap默认有一个数组table，默认为null
        //(2)往进put数据的时候,对数据进行第一次扩容,第一次扩容的大小为16
        //(3)遇到相同的key,就要进行链表的添加,当单个链表到达临界值后,并且数组的大小没有到到64,对数组table进行扩容操作
        //(4)当扩容到到达64，并且链表的大小超过了8之后,就进行树化

        HashMap hashMap = new HashMap();
        for(int i = 1 ; i <= 12; i++){
            hashMap.put(new A(i),i);
        }

        hashMap.put("aaa","bbb");
    }
}



class A{
    private int num;
    public A(int num){
        this.num = num;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        A a = (A) o;
//        return num == a.num;
//    }

    @Override
    //返回所有的hashCode都是一样
    public int hashCode() {
        return 100;
    }

    @Override
    public String toString() {
        return "A{" +
                "num=" + num +
                '}';
    }
}
