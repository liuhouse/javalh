package javalh.chapter14.map_;

import java.util.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class MapFor_ {
    @SuppressWarnings({"all"})
    public static void main(String[] args) {
        Map map = new HashMap();
        map.put("邓超","孙俪");
        map.put("王宝强","马蓉");
        map.put("宋喆","马蓉");
        map.put("刘凌波",null);
        map.put(null,"刘亦菲");
        map.put("鹿晗","关晓彤");

        //第一组:先取出所有的Key，通过Key取出对应的Value
        Set keyset = map.keySet();
        //(1)增强for
        System.out.println("---------第一种方式-----");
        for(Object key : keyset){
            System.out.println(key + "-" + map.get(key));
        }

        //(2)迭代器
        System.out.println("------第二种方式---------");
        Iterator iterator = keyset.iterator();
        while (iterator.hasNext()) {
            Object key =  iterator.next();
            System.out.println(key + " - " + map.get(key));
        }


        //第二组：把所有的values取出
        Collection values = map.values();
        //这里可以使用所有的Collections使用的遍历方法
        //(1)增强for
        System.out.println("--取出所有的value增强for--");
        for (Object value : values){
            System.out.println(value);
        }


        //(1)迭代器
        System.out.println("--取出所有value的迭代器--");
        Iterator iterator1 = values.iterator();
        while (iterator1.hasNext()) {
            Object obj =  iterator1.next();
            System.out.println(obj);
        }

        //第三组:通过EntrySet来获取k-v  //EntrySet<Map.Entry<K,V>>
        Set entryset = map.entrySet();
        //(1)增强for
        System.out.println("---使用EntrySet的增强for循环(第三种)----");
        for(Object entry : entryset){
            //entry 默认是将键和值全部都打印出来
            //将entry转成Map.Entry
            Map.Entry m =(Map.Entry) entry;
            //转化了之后就可以使用getKey() 和 getValue()分别获取键和值
            System.out.println(m.getKey() + " - " + m.getValue());
        }


        //(2)迭代器
        System.out.println("-----使用EntrySet的迭代器(第四种)----");
        Iterator iterator2 = entryset.iterator();
        while (iterator2.hasNext()){
            Object entry = iterator2.next();
            System.out.println(entry.getClass());//HashMap$Node -> 实现->Map.Entry(getKey,getValue);
            //进行向下转型 Map.Entry
            Map.Entry m = (Map.Entry) entry;
            System.out.println(m.getKey() + " + " + m.getValue());
        }


    }
}
