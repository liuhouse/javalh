package javalh.chapter14.map_;

import java.util.HashMap;

/**
 * @author 刘皓
 * @version 1.0
 */
public class MapInfo_ {
    public static void main(String[] args) {
        /*
        * HashMap小结
        * (1)HashMap接口常用实现类:HashMap,HashTable和Properties
        * (2)HashMap是Map接口使用频率最高的实现类
        * (3)HashMap是以key->val的键值对的方式存储数据的(HashMap$Node类型)
        * (4)key不能重复,但是值是可以重复的,允许使用null键和null值
        * (5)如果添加相同的key,则会覆盖原来的key-val,等同与修改(key不会替换,val会替换)
        * (6)与HashSet一样,不保证映射的顺序,因为底层是以hash表的方式进行存储的(jdk8的HashMap 底层  数组+链表—+红黑树)
        * (7)HashMap没有实现同步,因此线程是不安全的,方法没有做同步互斥的操作,没有synchronized
        * */

        /*
        * HashMap的扩容机制
        * 扩容机制[和HashSet相同]
        * (1)HashMap底层维护了Node类型的数组table,默认为null
        * (2)当创建对象的时候,将加载因子(loadfactor)初始化为0.75
        * (3)当添加key-val的时候,通过key的Hash值得到在table的索引,然后判断该索引处是否有元素,如果没有元素就直接添加,如果该索引处有元素
        *   继续判断该元素的key和准备加入的key是否相等,如果相等,就直接替换val;如果不相等需要判断是树结构还是链表结构,做出相对应的处理
        *   如果添加的时候发现容量不够,则需要进行扩容操作
        * (4)第一次添加,则需要扩容table的容量为16,临界值(threshold)为12(16*0.75)
        * (5)以后再扩容,则需要扩容table容量为原来的2倍(32),临界值为原来的两倍,即24,依次类推
        * (6)在java8中,如果一条链表的元素个数超过TREEIFY_THRESHOLD(默认是8)，并且table的大小>=MIN_TREEIFY_CAPACITY(默认64),就会进行树化(红黑树)
        * */

        HashMap map = new HashMap();
        map.put("java",10);//ok
        map.put("php",10);//ok
        map.put("java",20);//替换value;
        System.out.println("map = " + map);
    }
}
