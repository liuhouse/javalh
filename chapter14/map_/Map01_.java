package javalh.chapter14.map_;

import java.util.HashMap;
import java.util.Map;

public class Map01_ {
    public static void main(String[] args) {
        //老韩解读Map接口实现类的特点,使用实现类HashMap

        Map map = new HashMap();
        map.put("no1","韩顺平");//k - v
        map.put("no2","张无忌");//k - v
        map.put("no1","张三丰");//当有相同的key，就等价与替换
        map.put("no3","张三丰");// k - v
        map.put(null,null);
        map.put(null,"abc");//等价替换
        map.put("no4",null);//k - v
        map.put("no5",null);//k-v
        map.put(1,"赵敏");// k - v
        map.put(new Object(),"金毛狮王");// k - v

        //通过get方法,传入key，会返回对应的value
        System.out.println(map.get("no5"));

        System.out.println(map);

    }
}
