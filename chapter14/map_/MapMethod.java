package javalh.chapter14.map_;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 刘皓
 * @version 1.0
 */
public class MapMethod {

    public static void main(String[] args) {
        //演示map接口的常用方法
        Map map = new HashMap();
        map.put("邓超",new Book("邓超",100));
        map.put("邓超","孙俪");//替换 -> 一会分析源码
        map.put("王宝强","马蓉");
        map.put("宋喆","马蓉");
        map.put("刘凌波",null);
        map.put(null,"刘亦菲");
        map.put("鹿晗","关晓彤");
        map.put("hsp","hsp的老婆");
        System.out.println("map = " + map);


        //remove：根据键删除映射关系
        //删除val为刘亦菲的
        map.remove(null);
        System.out.println("map = " + map);

        //get：根据键获取值
        Object val = map.get("hsp");
        System.out.println("val = " + val);

        //size获取元素的个数
        System.out.println("k - v = " + map.size());

        //clear 清除所有的 k - v
        //map.clear();

        //isEmpty()：判断元素的个数是否为0
        System.out.println(map.isEmpty());//false

        //containsKey：查找键是否存在
        System.out.println("结果=" + map.containsKey("hsp"));//true

    }
}

class Book{
    private String name;
    private int age;

    public Book(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
