package javalh.chapter14;

import java.util.LinkedList;

/**
 * @author 刘皓
 * @version 1.0
 */
//LinkedList的增删改查案例
public class LinkedListCRUD {
    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();

        //演示添加节点
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        System.out.println("linkedList=" + linkedList);

        //演示删除一个节点
        linkedList.remove();//这里默认删除的是第一个节点

        System.out.println("linkedList=" + linkedList);
        //

        /*
            源码阅读
            1:
                LinkedList linkedList = new LinkedList();
                调用了无参构造器
                public LinkedList() {}
            2：
                这个时候的linkedList的属性 first = null last = null size = 0 modCount = 0
            3: 执行 添加
                 public boolean add(E e) {
                    linkLast(e);
                    return true;
                 }
            4:将新的节点,加入到双向链表的最后

                void linkLast(E e) {
                final Node<E> l = last;
                final Node<E> newNode = new Node<>(l, e, null);
                last = newNode;
                if (l == null)
                    first = newNode;
                else
                    l.next = newNode;
                size++;
                modCount++;
            }
        * */


        /*
        1：执行 removeFirst
            源码解读 linkedList.remove(); 这里默认删除的是第一个节点
            public E remove() {
                return removeFirst();
            }
        2:执行
         public E removeFirst() {
            final Node<E> f = first;
            if (f == null)
                throw new NoSuchElementException();
            return unlinkFirst(f);
          }

         3：执行unlinkFirst,将f指向的双向链表的第一个节点拿掉
         private E unlinkFirst(Node<E> f) {
            // assert f == first && f != null;
            final E element = f.item;
            final Node<E> next = f.next;//最终的第一个节点
            f.item = null;
            f.next = null; // help GC
            first = next;
            if (next == null)
                last = null;
            else
                next.prev = null;
            size--;
            modCount++;
            return element;
        }

        * */
    }
}
