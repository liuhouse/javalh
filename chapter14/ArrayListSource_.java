package javalh.chapter14;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ArrayListSource_ {
    public static void main(String[] args) {
        /*
        * (1)ArrayList中维护了一个Object类型的数组elementData
        * transient Object[] elementData  transient 表示瞬间,暂时的,表示该属性不会被序列化
        * (2)当创建ArrayList对象的时候,如果使用的是无参构造器,则初始elementData容量为0,第一次添加
        * 则扩容elementData为10,如果要再次扩容,则扩容elementData为1.5倍
        * (3)如果使用的是指定大小的构造器,则初始elementData容量为指定大小,如果需要扩容,则直接扩容elementData为1.5倍
        * 老师建议：自己去debug一把我们的ArrayList的创建和扩容的流程
        * */

        /*
        * 老韩解读代码
        * 注意注意,idea默认情况下,Debug显示的数据是简化的后的,如果希望看到完整的数据,需要做设置
        * 使用无参构造器创建ArrayList();
        *
        * */

        //List list = new ArrayList();

        List list = new ArrayList(8);

        //使用for循环给list集合添加1-10数据
        for(int i = 1 ; i <= 10 ; i++){
            list.add(i);
        }

        //使用for给list集合添加 11 - 15 数据

        for(int i = 11 ; i <= 15  ; i++){
            list.add(i);
        }


    }
}
