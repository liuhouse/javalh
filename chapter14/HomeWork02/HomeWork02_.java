package javalh.chapter14.HomeWork02;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings("all")
public class HomeWork02_ {

    public static void main(String[] args) {
        /*
        * 使用ArrayList完成对 对象 Car{name,price}的各种操作
        *
        * */
        ArrayList list = new ArrayList();
        Car car1 = new Car("宝马",400000);
        Car car2 = new Car("宾利",5000000);
        Car car3 = new Car("比亚迪",100000);
        Car car4 = new Car("玛莎拉蒂",500000);
        Car car5 = new Car("玛莎拉蒂",500000);
        //1:add添加单个元素
        list.add(car1);
        list.add(car2);
        list.add(car3);
        list.add(car4);

        ArrayList newList = new ArrayList();
        newList.add(car1);
        newList.add(car2);
        newList.add(car3);
        newList.add(car4);


        //remove：删除指定元素
        list.remove(0);

        //contains:查找元素是否存在
        System.out.println(list.contains(car1));

        //size:获取元素个数
        System.out.println("list的元素的个数 = " + list.size());

        //isEmpty:判断是否为空
        System.out.println("是否为空" + list.isEmpty());

        //clear:清空
        list.clear();

        newList.add(car1);
        //addAll添加元素 -- 添加多个元素  其中的参数是一个集合
        list.addAll(newList);

        ArrayList arrayList1 = new ArrayList();
        arrayList1.add(car5);
        //containsAll:查找多个元素是否存在
        System.out.println("全部都是存在的" + list.containsAll(arrayList1));
        list.add(car5);
        //removeAll:删除多个元素
//        list.removeAll(newList);

        //使用增强for和迭代器来遍历所有的car，需要重写Car的toString方法
        System.out.println("使用增强for循环遍历================");
        for (Object o : list) {
            System.out.println(o);
        }

        //使用迭代器遍历
        System.out.println("使用迭代器遍历===============");
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Object next =  iterator.next();
            System.out.println(next);
        }


//        System.out.println("list = " + list);
    }
}

class Car{
    private String name;
    private double price;

    public Car(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
