package javalh.chapter14.HomeWork04;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork04 {
    public static void main(String[] args) {
        /*
        * 简答题
        * 试分析HashSet和TreeSet分别是如何实现去重的
        * (1)HashSet的去重机制,hashCode()+equals(),底层先通过存入对象,进行运算得到一个Hash值
        *   通过hash值得到对应的索引,如果发现table索引所在的位置,没有数据,就直接存放,如果有数据,就进行
        *   equals()比较【遍历比较】,如果比较不相同,就加入,否则就不加入
        * (2)TreeSet的去重机制：如果你传入了一个Comparator匿名对象，就使用实现的compare去去重,如果方法返回的是0
        *   就认为是相同元素/数据,就不添加,如果你没有传入一个Comparator匿名对象,则以你添加的对象实现的Compareable接口
        *   的compareTo去重
        * */
    }
}
