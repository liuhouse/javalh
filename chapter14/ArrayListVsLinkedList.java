package javalh.chapter14;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ArrayListVsLinkedList {
    public static void main(String[] args) {
        /*
        * ArrayList和LinkedList的比较
        *
        * 名称            底层结构        增删的效率         改查的效率
        * ArrayList       可变数组          较低,数组扩容     较高,可以直接定位索引
        * LikedList       双向链表          较高,通过链表追加   较低
        *
        * 如何选择ArrayList和LinkedList
        * (1)如果我们改查的操作多,选择ArrayList
        * (2)如果我们的增删操作多,选择LinkedList
        * (3)一般来说,在程序中,80%-90%都是查询,因此大部分情况都是选择的是ArrayList
        * (4)在一个项目中,根据业务灵活选择,也可能这样,一个模块使用的是ArrayList,另外一个模块使用的是LinkedList，也就是说
        * 要根据业务逻辑来选择
        * */
    }
}
