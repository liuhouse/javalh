package javalh.chapter14;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Collection01_ {
    public static void main(String[] args) {
        /*
        * 保存多个数据的时候使用数组,数组有很多不足的地方
        * (1)长度开始的时候必须指定,而且一旦指定,不能修改
        * (2)保存的元素必须为同一类型
        * (3)使用数组进行增加/删除元素的示例代码,比较麻烦
        * */
        //数组的长度是1
        Person persons[] = new Person[1];
        //给数组填充数据
        persons[0] = new Person();

        //数组扩容,数组扩容代表每次都要新创建一个新的数组
        Person person2[] = new Person[persons.length+1];//创建一个新的数组,数组的长度为原数组的长度+1
        for(int i = 0 ; i < persons.length ; i++){
            person2[i] = persons[i];
        }
        person2[person2.length - 1] = new Person();//给数组元素添加新的对象

        for(int i = 0 ; i < person2.length ; i++){
            System.out.println(person2[i].name);
        }

        //可以看出来,使用数组扩容的时候是真的很麻烦


        //集合
        /*
        * (1)可以动态的保存任意多个对象,使用比较方便
        * (2)提供了一系列方便的操作对象的方法：add , remove , set  , get
        * (3)使用集合添加,删除新元素的实例代码 - 简洁了
        * */

        //Collection
        //Map
        //老韩解读
        //1:集合主要是两组(单列集合,双列集合)
        //2:Collection接口有两个重要的子接口,List 和 Set,他们的实现子类都是单列集合
        //3:Map接口实现子类是双列集合 存放的是  K-V
        //4:把老师梳理的两张图记住

        ArrayList arrayList = new ArrayList();
        arrayList.add("jack");
        arrayList.add("tom");
        arrayList.add("hu");
        System.out.println(arrayList);

        HashMap hashMap = new HashMap();
        hashMap.put("NO1","北京");
        hashMap.put("NO2","上海");
        System.out.println(hashMap);

    }
}

class Person{
    String name = "1";
}
