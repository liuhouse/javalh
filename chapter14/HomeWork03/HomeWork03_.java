package javalh.chapter14.HomeWork03;

import java.util.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork03_ {
    public static void main(String[] args) {
        /*
        * 按要求完成下面的任务
        * (1)使用HashMap类实例化一个Map类型的对象m,键(String)和值int分別用于存储员工的姓名和工资
        *   存入方式如下 jack - 650元 ; tom - 1200元  smith - 2900元
        * (2)将jack的工资更改为2600元
        * (3)为所有员工工资加薪100元
        * (4)遍历集合中所有的员工
        * (5)遍历集合中所有的工资
        * */

        Map m = new HashMap();
        //给hashMap中添加元素
        m.put("jack",650);
        m.put("tom",1200);
        m.put("smith",2900);

        //将jack的工资修改为2600
        m.put("jack",2600);

        Set keys = m.keySet();
        for (Object key :keys) {
            //给所有员工的工资+100
            m.put(key,(Integer) m.get(key) + 100);
        }

        //使用EntrySet进行遍历
        //遍历集合中的所有员工
        Set entryset = m.entrySet();
        Iterator iterator = entryset.iterator();
        while (iterator.hasNext()) {
            Map.Entry entry =  (Map.Entry)iterator.next();
            //打印出所有的员工
            System.out.println(entry.getKey() + "---" + entry.getValue());
        }


        //得到所有的工资
        Collection values = m.values();
        for (Object o :values) {
            System.out.println(o);
        }


    }
}
