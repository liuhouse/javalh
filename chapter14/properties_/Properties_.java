package javalh.chapter14.properties_;

import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Properties_ {
    public static void main(String[] args) {
        Properties properties = new Properties();
        //properties.put(null,"abc");//抛出 空指针异常
        //properties.put("abc",null);//抛出 空指针异常

        properties.put("john",100);//k - v
        properties.put("lucy",100);//k - v
        properties.put("lic",100);//k - v
        properties.put("lic",88);//如果有相同的key,value被替换

        //通过k来获取对应的值
        System.out.println(properties.get("lic"));

        //删除
        properties.remove("lic");

        //修改
        properties.put("john","约翰");

        System.out.println(properties);

    }
}
