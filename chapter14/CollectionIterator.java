package javalh.chapter14;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author 刘皓
 * @version 1.0
 */
public class CollectionIterator {
    public static void main(String[] args) {
        //Collection接口遍历元素方式 1-使用Iterator迭代器
        /*
        * (1)Iterator对象称为迭代器,主要用于遍历Collection集合中的元素
        * (2)所有实现了Collection接口的集合都有一个iterator()方法,用以返回一个实现了iterator接口的对象,即可以返回一个迭代器
        * (3)Iterator的结构【看一张图】
        * (4)Iterator仅仅用于集合的遍历,Iterator本身并不存放对象
        *
        *  迭代器的执行原理
        *  Iterator iterator = coll.iterator(); //得到一个集合的迭代器
        *  hasNext():判断是否还有下一个元素
        *  while(iterator.hasNext()){
        *       //作用：1：下移 2：将下移以后的集合位置上的元素返回
        *       next();
        *       System.out.println(iterator.next());
        *  }
        *
        *  老韩提示
        *  在调用iterator.next()方法之前必须调用iterator.hasNext()进行检测,若不调用,且下一条记录无效
        *   直接调用ie.next()会抛出NoSuchElementException异常
        * */

        Collection col = new ArrayList();
        col.add(new Book("三国演义","罗贯中",10.1));
        col.add(new Book("小李飞刀","古龙",5.1));
        col.add(new Book("红楼梦","曹雪芹",34.6));

        //现在希望能够遍历col集合
        //1:先得到col对应的迭代器
        Iterator iterator = col.iterator();
        //2.使用while循环遍历
        while (iterator.hasNext()){
            //返回下一个元素,类型是object
            Object obj = iterator.next();
            System.out.println("obj = " + obj);
        }

        //3。当退出while循环后,这个时候的iterator迭代器,指向最后的元素
        iterator.next();//NoSuchElementException

        //这里老师交大家一个快捷键,快速生成 while   =>   itit
        //显示所有快捷键的快捷键 ctrl+j


        //4:如果希望再次遍历,需要重置我们的迭代器
        iterator = col.iterator();
        while (iterator.hasNext()) {
            Object next =  iterator.next();
            System.out.println("next = " + next);
        }




    }
}

class Book{
    private String name;
    private String author;
    private double price;

    public Book(String name, String author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                '}';
    }
}


