package javalh.chapter14.HomeWork05;

import java.util.TreeSet;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork05_ {
    public static void main(String[] args) {
        /*
        * 下面的代码运行会不会抛出异常,并从源码层面说明原因[考察 读源码+接口编程+动态绑定]
        * 因为这里的TreeSet没有传入Comparator匿名对象,所以这里使用的是对象的实现的Compareable接口的
        * compareTo去重   如果Person类不去实现此接口   就会报错
        * 如果compareTo返回的是0,则永远不会添加进去,如果是别的就可以
        * */
        TreeSet treeSet = new TreeSet();
        treeSet.add(new Person());
        treeSet.add(new Person());
        System.out.println(treeSet);
    }
}

class Person implements Comparable{

    @Override
    public int compareTo(Object o) {
        return -1;
    }
}
