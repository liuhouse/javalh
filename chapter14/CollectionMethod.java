package javalh.chapter14;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 刘皓
 * @version 1.0
 */
public class CollectionMethod {
    public static void main(String[] args) {
        /*
        * (1)Collection实现子类可以存放多个元素,每个元素可以是Object
        * (2)有些Collection的实现类,可以存放重复的元素,有些不可以
        * (3)有些Collection的实现类,有些是有序的(List),有些不是有序的(Set)
        * (4)Collection接口没有直接的实现子类,是通过他的子接口Set和List来实现的
        * */
        //Collection接口常用方法,以实现子类的ArrayList来演示

        //ArrayList可以使用继承的父接口来作为编译类型接收
        List list = new ArrayList();
        //add添加单个元素
        list.add("jack");
        list.add(10);//实际上就相当于list.add(new Integer(10))
        list.add(true);
        //remove删除指定元素
        //list.remove(0);//删除第一个元素
        list.remove(true);//指定删除某个元素


        //contains：查找元素是否存在
        System.out.println(list.contains("jack"));//true

        //size：获取元素个数
        System.out.println(list.size());

        //isEmpty:判断是否为空
        System.out.println(list.isEmpty());//false

        //clear:清空
        list.clear(); // []

        //addAll：添加多个元素
        ArrayList list2 = new ArrayList();
        list2.add("红楼梦");
        list2.add("三国演义");
        list.addAll(list2);

        //containsAll:查找多个元素是否都存在
        //检查list2里面的元素是不是都存在在list中
        System.out.println(list.containsAll(list2));//True

        //removeAll:删除多个元素
        list.add("聊斋");
        //将list元素中的元素为跟list2元素匹配的全部删除掉
        list.removeAll(list2);

        System.out.println("list=" + list);



    }
}
