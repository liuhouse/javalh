package javalh.chapter14.table_;

import java.util.Hashtable;

/**
 * @author 刘皓
 * @version 1.0
 * Hashtable基本介绍
 */
public class HashTableExercise {
    public static void main(String[] args) {
        /*
        * Hashtable的基本介绍
        * (1)存放的元素是键值对:即k-v
        * (2)hashtable的键和值都不能为null,否则会抛出NullPointerException异常
        * (3)hahTable使用的方式基本上和HashMap是一样的
        * (4)hashTable是线程安全的(synchronized),hashMap是线程不安全的
        * (5)简单看一下底层结构
        *
        *
        *
        * 简单的说一下HashTable的底层
        * (1):底层有数组 Hashtable$Entry[] 初始化大小为11
        * (2):临界值 threshold 8 = 11 * 0.75
        * (3):扩容：按照自己的扩容机制来进行即可
        * (4):执行方法addEntry(hash,key,value,index);添加k-v封装到Entry
        * (5):当if(count > threshold)满足的时候,就进行扩容
        * (6):按照int newCapacity = (oldCapacity << 1) + 1的大小进行扩容
        * */
        //下面的代码是不是正确,如果错误,为什么?
        Hashtable table = new Hashtable();//ok
        table.put("john",100);//ok;
        //table.put(null,100);//异常
        //table.put("john",null);//异常
        table.put("lucy",100);//ok
        table.put("lic1",100);//ok
        table.put("lic2",88);//替换
        table.put("lic3",88);//替换
        table.put("lic4",88);//替换
        table.put("lic5",88);//替换
        table.put("lic6",88);//替换
        table.put("lic7",88);//替换
        table.put("lic8",88);//替换
        table.put("lic9",88);//替换

        //临界因子   0.75    11*0.75  就是临界值

        System.out.println(table);
    }
}
