package javalh.chapter14.treeSet_;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * @author 刘皓
 * @version 1.0
 */
public class TreeSet_ {
    public static void main(String[] args) {
        /*
        * 基本使用
        * 1:当我们使用无参构造器,创建TreeSet的时候,默认是按照首字母的顺序进行排列的
        * 2:老师希望添加的元素,按照字符串的大小来进行排序
        * 3:使用TreeSet提供的一个构造器,可以传入一个比较器(使用匿名内部类)
        * 指定排序规则
        * 4:简单的看看源码
        * */
        //TreeSet treeSet = new TreeSet();
        TreeSet treeSet = new TreeSet(new Comparator() {
            //下面调用String的compareTo方法进行字符串的大小比较
            //如果老韩要求加入的元素,是按照长度的大小进行排序
            @Override
            public int compare(Object o1, Object o2) {
                //进行向下转型
                //return ((String) o1).compareTo((String) o2);
                //按照字符串的长度进行大小排序
                //在这个时候会发现abc其实是加不进去的,因为现在判断键相同的条件是字符串的长度
                return ((String) o1).length() - ((String)o2).length();
            }
        });
        //添加数据
        treeSet.add("jack");
        treeSet.add("tom");
        treeSet.add("sp");
        treeSet.add("abc");


        System.out.println("treeSet=" + treeSet);

        /*
        1:构造器把穿日的比较器的独享,赋给了TreeSet的底层的TreeMap属性,this.comparator
         public TreeMap(Comparator<? super K> comparator) {
                this.comparator = comparator;
            }

        2:在调用treeSet.add("tom")，底层会执行到此处
         private V put(K key, V value, boolean replaceOld) {
        Entry<K,V> t = root;

        //如果发现当前的hashMap索引处的值为null的话,就直接进行添加,完事
        if (t == null) {
            addEntryToEmptyMap(key, value);
            return null;
        }
        int cmp;
        Entry<K,V> parent;
        // split comparator and comparable paths
        Comparator<? super K> cpr = comparator;
        //此时此刻,cpr就是我们的匿名内部类(对象)
        if (cpr != null) {
            do {
                parent = t;
                动态绑定我们匿名内部类(对象)compare
                cmp = cpr.compare(key, t.key);
                //如果第一个比第二个小,那么就放在左边
                if (cmp < 0)
                    t = t.left;
                    //如果第一个比第二个大,就放在右边
                else if (cmp > 0)
                    t = t.right;
                else {
                //如果相等,即返回0,进行值的替换,没有加入,还是返回原来的值，原来的位置
                    V oldValue = t.value;
                    if (replaceOld || oldValue == null) {
                        t.value = value;
                    }
                    return oldValue;
                }
            } while (t != null);
        } else {
            Objects.requireNonNull(key);
            @SuppressWarnings("unchecked")
            Comparable<? super K> k = (Comparable<? super K>) key;
            do {
                parent = t;
                cmp = k.compareTo(t.key);
                if (cmp < 0)
                    t = t.left;
                else if (cmp > 0)
                    t = t.right;
                else {
                    V oldValue = t.value;
                    if (replaceOld || oldValue == null) {
                        t.value = value;
                    }
                    return oldValue;
                }
            } while (t != null);
        }
        addEntry(key, value, parent, cmp < 0);
        return null;
    }




        * */
    }
}
