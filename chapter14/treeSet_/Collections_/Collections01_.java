package javalh.chapter14.treeSet_.Collections_;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Collections01_ {
    public static void main(String[] args) {
        /*
        * (1)Collections是一个操作Set,List和Map等集合的工具类
        * (2)Collections中提供了一系列的静态方法对集合的元素进行排序,查询和修改操作
        * */

        //创建ArrayList集合,用于测试
        //list是有顺序的,按照添加顺序
        ArrayList list = new ArrayList();
        list.add("tom");
        list.add("smith");
        list.add("king");
        list.add("milan");
        list.add("tom1");
        System.out.println("原始的排序=======");
        System.out.println("list = " + list);
        //reverse(List):反转List中元素的顺序
        Collections.reverse(list);
        System.out.println("reverse反转之后======");
        System.out.println("list = " + list);

        //shuffle(list):对List集合元素进行随机排序
        //可以用作抽奖活动

        System.out.println("随机打乱之后的数组====");
        for(int i = 0 ; i <= 5 ; i++){
            Collections.shuffle(list);
            System.out.println("list = " + list);
        }

        //sort(List):根据元素的自然顺序对指定List集合元素升序排列
        Collections.sort(list);
        System.out.println("按照自然排序之后的数组====");
        System.out.println("List = " + list);


        //自定义排序
        //sort(List,Comparator):根据指定的Comparator产生的顺序对List集合元素进行排序;
        //我们希望按照字符串的长度大小进行排列
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                //这里可以加入效验代码
                //按照字符串的长度进行排序
                return ((String)o2).length() - ((String) o1).length();
            }
        });
        System.out.println("按照自定义排序之后的数组====");
        System.out.println("List = " + list);

        //swap(List,int,int):将指定list集合中的i处元素和j处元素进行交换
        //比如
        Collections.swap(list,1,2);
        System.out.println("交换后的情况");
        System.out.println("list = " + list);

        //Object max(Collection):根据元素的自然排序,返回给定集合中的最大元素
        System.out.println("自然排序最大元素=" + Collections.max(list));

        //Object max(Collection,Comparator):根据Comparator指定的顺序,返回给定集合中的最大元素
        //比如,我们要返回长度最大的元素
        Object maxObject = Collections.max(list, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                //return ((String) o2).length() - ((String) o1).length();
                return ((String) o2).compareTo((String) o1);
            }
        });

        System.out.println("list元素中长度最小的元素是 = " + maxObject);

        //获取列表中的最小元素
        System.out.println("自然排序中的最小元素=" + Collections.min(list));

        //void copy(List dest , List src)：将src中的内容复制到dest数组中

        ArrayList dest = new ArrayList();

        //进行数组拷贝
        //这里会进行报错,因为默认新的数组dest是为空的,也就是长度为0
        // int srcSize = src.size();
        //        if (srcSize > dest.size())
        //            throw new IndexOutOfBoundsException("Source does not fit in dest");

        //为了完成一个完整的拷贝,我们需要先给dest赋值,进行扩容,大小和list.size()的大小一样
        for(int i = 0 ; i < list.size() ; i++){
            dest.add("");
        }

        Collections.copy(dest,list);
        System.out.println("dest = " + dest);

        //boolean replaceAll(List list , Object oldValue , Object newVal);使用新值替换List对象的所有旧值

        //如果list中,查找到tom的话,就将tom替换成汤姆,这里是进行全局替换
//        Collections.replaceAll(list,"tom","汤姆");
//        System.out.println("List = " + list);


        //int frequency(Collection,Object):返回指定集合中指定元素出现的次数
        System.out.println("tom出现的次数 = " + Collections.frequency(list,"tom"));



    }
}
