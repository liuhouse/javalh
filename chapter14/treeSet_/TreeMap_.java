package javalh.chapter14.treeSet_;

import java.util.Comparator;
import java.util.TreeMap;

/**
 * @author 刘皓
 * @version 1.0
 */
public class TreeMap_ {
    public static void main(String[] args) {
        /*
        * 使用默认构造器,创建treeMap，默认是按照首字母的大小从小到大进行排序的
        * */
        /*
        * 老韩要求:按照传入的k(string)的大小进行排序
        * */



        //TreeMap treeMap = new TreeMap();
        TreeMap treeMap = new TreeMap(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                //按照传入的k(string)的大小进行排序
                return ((String) o2).compareTo((String) o1);
                //按照K(String)的长度大小进行排序
                //return ((String) o1).length() - ((String) o2).length();
            }
        });

        //添加元素
        treeMap.put("jack","杰克");
        treeMap.put("tom","汤姆");
        treeMap.put("kristina","克瑞斯替诺");
        treeMap.put("smith","史密斯");
        treeMap.put("hsp","韩顺平");//按照长度的话,这个元素是加不进去的 ， 值会被替换

        System.out.println("treeMap = " + treeMap);


        /*
        原码解读


        1：构造器：把传入的实现了Comparator接口的匿名内部类(对象),传给TreeMap的comparator
         public TreeMap(Comparator<? super K> comparator) {
                this.comparator = comparator;
            }
        2：调用put方法
        2.1 第一次添加,把k-v封装到Entry对象,放入root

        private V put(K key, V value, boolean replaceOld) {
        Entry<K,V> t = root;
        //执行第一次添加的操作
        if (t == null) {
            addEntryToEmptyMap(key, value);
            return null;
        }
        int cmp;
        Entry<K,V> parent;
        // split comparator and comparable paths

        2.2以后添加的时候
        Comparator<? super K> cpr = comparator;
        if (cpr != null) {
            do {//遍历所有的key，给当前的key找到适当的位置
                parent = t;
                cmp = cpr.compare(key, t.key);//cpr是比较构造器，动态绑定我们的匿名内部类的compare
                if (cmp < 0)
                    t = t.left;
                else if (cmp > 0)
                    t = t.right;
                else {
                //如果在遍历的过程中,发现准备添加的key和当前已有的key相等,就不添加
                    V oldValue = t.value;
                    if (replaceOld || oldValue == null) {
                        t.value = value;
                    }
                    return oldValue;
                }
            } while (t != null);
        } else {
            Objects.requireNonNull(key);
            @SuppressWarnings("unchecked")
            Comparable<? super K> k = (Comparable<? super K>) key;
            do {
                parent = t;
                cmp = k.compareTo(t.key);
                if (cmp < 0)
                    t = t.left;
                else if (cmp > 0)
                    t = t.right;
                else {
                    V oldValue = t.value;
                    if (replaceOld || oldValue == null) {
                        t.value = value;
                    }
                    return oldValue;
                }
            } while (t != null);
        }
        addEntry(key, value, parent, cmp < 0);
        return null;
    }

        * */


    }
}
