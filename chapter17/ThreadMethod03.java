package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ThreadMethod03 {
    public static void main(String[] args) throws InterruptedException {
        /*
        * 用户线程和守护线程
        * (1): 用户线程：也叫工作线程,当线程的任务执行完或者通知的方式结束
        * (2): 守护线程：一般是为了工作线程服务的,当所有的用户线程结束,守护线程自动结束
        *
        * 应用案例
        * 下面我们测试如何将一个线层设置成守护线程
        * */

        MyDaemonThread dt = new MyDaemonThread();
        //将dt设置为守护线程,当所有的线程结束之后,dt就自动结束
        //如果没有设置,那么即使main线程执行完毕,dt也不退出,可以体验一下
        //将dt设置为守护线程
        //一旦设置了守护线程,那么当主线程执行完毕之后,守护线程也就结束了
        //一个很形象的例子   宝强在努力的工作.而马蓉和宋喆一直在开心的聊天,然后一旦宝强工作结束了之后,马蓉和宋喆就立马不聊天了
        dt.setDaemon(true);
        //开启子线程
        dt.start();
        for(int i = 0 ; i <= 50 ; i++){
            Thread.sleep(50);
            System.out.println("宝强辛苦工作...." + i);
        }

    }
}

//守护线程
class MyDaemonThread extends Thread{
    @Override
    public void run() {
        //无限执行
        for (; ;){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("马蓉和宋喆快乐的聊天,哈哈~~~");
        }
    }
}
