package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Therad04 {
    public static void main(String[] args) {

        /*
        * 需求：
        * 我们要做一个多线程的人,同时可以干好几件事
        * 一边看书
        * 一边锻炼身体
        * */

        /*
        * 老刘解读
        * (1):因为Book继承了Thread类,并且重写了Thread中的run()方法,所以现在可以实现多线程
        * (2):调用book.start()，相当于是在main线程中开启了一个子线程,然后继续执行main线程中的代码
        * (3):开启的子线程,会在后台去执行子线程Thread-01
        * (4):这两个就相当于是一个进程中的两个线程   main线程  和  Thread-01线程
        * (5):两个分别执行,全部执行完毕之后结束进程
        * */


        Book book = new Book();
        System.out.println("我要开始看书了.....");
        book.start();
        for (int i = 0 ; i < 10 ; i++){
            System.out.println("我在锻炼身体" + i);
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }

        }
    }
}


class Book extends Thread{
    int look_num = 0;
    @Override
    public void run() {
        //不停的看书，看够30秒之后,然后去休息
        while (true){
            System.out.println("我在看书" + (++look_num) + "秒");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(look_num == 30){
                System.out.println("已经看了好久的书了,我退出不看了....");
                break;
            }
        }
    }
}
