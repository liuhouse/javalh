package javalh.chapter17.HomeWork;

import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01 {
    public static void main(String[] args) {
        /*
        * 编程题
        * (1)在main方法中启动两个线程
        * (2)第一个线程循环随机打印100以内的整数
        * (3)直到第二个线程从键盘读取了Q命令
        * 使用通知的方式退出
        * */
        T1 t1 = new T1();
        t1.start();

        T2 t2 = new T2(t1);
        t2.start();

    }
}

//线程1
class T1 extends Thread {

    private boolean flag = true;

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void run() {
        //如果是true的话,就一直打印
        while (flag) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "输出 " + (int)(Math.random()*100));
        }
        System.out.println("A线程已经退出来了...");
    }
}


class T2 extends Thread{

    private T1 t1;
    private Scanner scanner = new Scanner(System.in);

    public T2(T1 t1) {
        this.t1 = t1;
    }

    @Override
    public void run() {
        while (true){
            System.out.println("请输入Q退出~！");
            if(scanner.next().toUpperCase().charAt(0) == 'Q'){
                System.out.println("B线程已经退出了...");
                t1.setFlag(false);
                break;
            }
        }

    }
}
