package javalh.chapter17.HomeWork;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork02 {
    public static void main(String[] args) {
        /**
         *  (1)有两个用户分别从一个卡上取钱(总额：10000)
         *  (2)每次都取1000元,当余额不足的时候,就不能取款了
         *  (3)不能出现超取现象 => 线程同步问题
         */

        WithdrawMoney withdrawMoney = new WithdrawMoney();
        //第一个人取钱
        Thread thread1 = new Thread(withdrawMoney);
        thread1.setName("第一个人");
        thread1.start();
        Thread thread2 = new Thread(withdrawMoney);
        thread2.setName("第二个人");
        thread2.start();
    }
}

//会出现重复取钱,或者取了没有扣钱的情况,这种情况我们是不允许的,所以这里我们需要使用线程同步
//当一个人在ATM取钱的时候,另外一个人只能等待这个人取完之后,才能去取钱
//实现Runnable接口,其主要的原因就是因为存在资源共享的问题
class WithdrawMoney implements Runnable{
    private double money = 100000;
    private boolean flag = true;

    /*
    * 解读
    * 1：这里使用synchronized 实现了线程同步
    * 2:当多个线程执行到这里的时候,就会去争夺this对象锁
    * 3:哪个线程争夺到(获取到)this对象锁,就执行synchronized 代码,执行完毕之后,就会释放synchronized对象锁
    * 4:争夺不到this对象锁,就blocked，准备继续争夺
    * 5:this对象锁是非公平锁
    * */

    //synchronized  线程同步
    public synchronized void getMoney(){
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(money < 1000){
            System.out.println("余额不足....");
            flag = false;
            return;
        }
        money -= 1000;
        System.out.println(Thread.currentThread().getName() + " - " + "取出了1000元,剩余:" + (money));
    }

    @Override
    public void run() {
        while (flag){
            getMoney();
        }
    }
}
