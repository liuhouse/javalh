package javalh.chapter17.HomeWork;

import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01_1 {
    public static void main(String[] args) {
        /*
         * 编程题
         * (1)在main方法中启动两个线程
         * (2)第一个线程循环随机打印100以内的整数
         * (3)直到第二个线程从键盘读取了Q命令
         * 使用通知的方式退出
         * */

        //先让T11一直循环执行
        T11 t11 = new T11();
        //设置线程的名称
        t11.setName("线程1");
        t11.start();
        //实例化线程2
        //切记这里要监听的是t11的实例化对象
        T22 t22 = new T22(t11);
        //设置线程2的名称
        t22.setName("线程2");
        t22.start();
    }
}


//第一个线程
class T11 extends Thread{
    private boolean flag = true;
    @Override
    public void run() {
        while (flag){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " - " + (int)(Math.random() * 100 + 1));
        }
        System.out.println("A线程1退出了！！！");
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}


//第二个线程
class T22 extends Thread{
    //因为这里T2要控制T1，那么这里需要一个变量接收t1
    private T11 t11;
    //因为这里要接收用户输入,所以这里要使用Scanner
    private Scanner scanner = new Scanner(System.in);

    public T22(T11 t1){
        this.t11 = t1;
    }

    @Override
    public void run() {
        //因为有输错的可能,所以如果不符合条件就让其一直输入
        while (true){
            System.out.println("请输入你想操作的内容！【！Q退出】");
            if(scanner.next().toUpperCase().charAt(0) == 'Q'){
                System.out.println("B线程退出了");
                //接收到了Q,通知线程1退出
                t11.setFlag(false);
                //确定执行完毕之后,就break
                break;
            }
        }
    }
}
