package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Thread05 {
    public static void main(String[] args) {
        /*
        * 线程使用应用案例 - 多线程执行
        * 要求
        * 请编写一个程序,创建两个线程，一个线程每隔一秒输出"hello world"，输出10次,退出,一个线程每隔一秒输出 "hi",输出5次退出
        * */


        /*
        * 线程如何理解
        * 形象化的例子
        * （1）：孙悟空现在想干两件事, 但是一个人不能同时干两件事,所以,他做了两个动作,变出两只猴子
        *          --1:使用猴毛变了一只猴子,去扫地
        *          --2:使用猴毛变了一个猴子,去摘香蕉
        *       而在此时孙悟空使用猴毛变猴子 => main()主线程
        *       猴子1 => 线程1
        *       猴子2 => 线程2
        *
        * (2)程序化理解
        * main线程
        *   创建一个线程去计算  => 线程1
        *   创建一个线程去打印  => 线程2
        *
        * 原理理解
        * 当运行一个程序的时候,开启了一个main主线程,在main主线程中调用start创建了两个子线程,然后各自执行,哪个线程执行完毕之后
        * 哪个线程先消失 当所有的线程都执行完毕之后   该进程就结束了   什么时候执行完取决于JVM的运行机制
        * */


        /*
        * 继承Thread vs 实现 Runnable的区别
        * (1):从java的是设计来看,通过继承Thread或者实现Runnable接口来创建线程本质上没有什么区别,从jdk帮助文档上我们可以看出来
        * Thread类本身就实现了Runnable接口
        * (2):实现Runnable接口方式更加适合多个线程共享一个资源的情况,并且避免了单继承的限制,建议使用Runnable
        * */



        //线程1
        XC1 xc1 = new XC1();
        //构建线程1的实体
        Thread thread = new Thread(xc1);
        //启动线程1
        thread.start();

        //线程2
        XC2 xc2 = new XC2();
        //构建线程2的实体
        Thread thread1 = new Thread(xc2);
        thread1.start();

    }
}

//线程1
class XC1 implements Runnable{
    int xc1_count = 0;
    @Override
    public void run() {
        while (true){
            System.out.println("hello,world " + (++xc1_count) + Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(xc1_count == 60){
                break;
            }
        }
    }
}

//线程2
class XC2 implements Runnable{
    int xc2_count = 0;
    @Override
    public void run() {
        while (true){
            System.out.println("hi " + (++xc2_count) + Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(xc2_count == 50){
                break;
            }
        }

    }
}
