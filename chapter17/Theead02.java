package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 * 线程应用案例2-实现Runnable接口
 */
public class Theead02 {
    public static void main(String[] args) {
        /*
        * 1:java是单继承的,在某些情况下一个类可能已经继承了某个父类,这个时候使用继承Thread类方法来创建线程显然是不可能的
        * 2:java的设计者们提供了另外一个方式来创建线程,就是通过实现Runnable接口来创建线程
        * */

        /*
        * 应用案例
        * 请编写程序,该程序可以每隔1秒,在控制台输出"hi",当输出10次之后,自动退出,请使用实现Runnable接口的方式实现
        * Thread02.java，这里底层使用了设计模式[代理模式] => 代码模式实现了Runnable接口,开发线程的机制
        * */

        Dog dog = new Dog();
        //dog.start();这里不能调用start
        //创建了Thread对象,把dog对象(实现Runnable)，放入Thread
        Thread thread = new Thread(dog);
        thread.start();


    }
}

//通过实现Runnable接口,开发线程
class Dog implements Runnable{
    //记录次数
    int count = 0;
    @Override
    public void run() {//普通方法
        while (true){
            System.out.println("小狗汪汪叫....hi" + (++count) + Thread.currentThread().getName());
            //休眠一秒
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(count == 10){
                break;
            }
        }
    }
}
