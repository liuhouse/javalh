package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ThreadState_ {
    public static void main(String[] args) throws InterruptedException {
        /*
        * 线程的声明周期
        * JDK中用Thread.State枚举表示了线程的几种状态
        * public static enum Thread.State extends Enum<Thread.State>
        * 线程状态.线程可以处于以下之一
        * (1)NEW 尚未启动的线程处于此状态
        * (2)RUNNABLE 在Java虚拟机中执行的线程处于此状态
        * (3)BLOCKED 被阻塞等待监听器锁定的线程处于此状态
        * (4)WAITING 正在等待另外一个线程执行特定动作的线程处于此状态
        * (5)TIMED_WAITING 正在等待另一个线程执行动作到达指定等待时间的线程处于此状态
        * (6)TERMINATED 已退出的线程处于此状态
        * */



        T3 t3 = new T3();
        System.out.println(t3.getName() + "状态 " + t3.getState());//NEW
        t3.start();

        //如果线程还没有退出的时候
        while (Thread.State.TERMINATED != t3.getState()){
            System.out.println(t3.getName() + "状态" + t3.getState());
            //插队
//            t3.join();
            Thread.sleep(500);//TIMED_WAITING
        }

        System.out.println(t3.getName() + "状态" + t3.getState());

    }
}



class T3 extends Thread{
    @Override
    public void run() {
        while(true){
            for(int i = 0 ; i < 10 ; i++){
                System.out.println("hi" + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            break;
        }
    }
}
