package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 * 线程的基本使用
 */
public class Threaduse {
    public static void main(String[] args) throws InterruptedException {
        /*
        * 线程的基本使用
        * 在java中线程来使用有两种方法
        * (1):继承Thread类,重写run方法
        * (2):实现Runnable接口,重写run方法
        *
        * 一个需求
        * (1)请编写程序,开启一个线程,该线程每隔一秒,在控制台输出 "喵喵,我是小猫咪"
        * (2)对上题改进：当输出80次 "喵喵,我是小猫咪",结束该线程
        * (3)使用JConsole金控线程执行情况,并画出程序示意图
        * */

        /*
        * 线程的执行
        * (1):当执行(run)这个程序的时候,会创建一个进程
        * (2):会首先创建一个main线程,也就是主方法里面执行的程序
        * (3):然后调用start()方法的时候,会创建一个子线程Thraed-01
        * (4):并不是主线程结束了,应用程序就结束了,而是主线程的任务结束,主线程产生的子线程的任务也执行完毕之后,这个进程才会结束
        * */


        /**
         * 总结：
         * 为什么要调用start
         * 因为调用了start 就真正的启动了线程,然后不会堵塞主线程的执行,会和子线程交替执行
         * 直接调用run方法的时候,其实没有启动线程,只是一个普通方法,会造成堵塞,run方法里面的代码执行完毕之后才会执行main里面的代码
         *
         *
        （1）
         public synchronized void start() {
            start0();
         }

         (2)
         private native void start0();

         start0()是本地方法,是JVM调用,底层是c/c++实现的
         //真正的多线程效果,是start0(),而不是run

         *
         *
         */


        //创建Cat对象,可以当做线程使用

        Cat cat = new Cat();

        //当调用start方法的时候,开启一个子线程 Thread-0 80

        //启动线程->最终会执行cat的run方法
        cat.start();



        //run()方法就是一个普通的方法,没有真正的启动一个线程,就会把run()方法执行完毕,才向下执行
        //说明：当main线程启动一个子线程 Thread-0,主线程不会堵塞,代码会继续的向下执行
        //这个时候,主线程和子线程是交替执行的

        //cat.run();


        System.out.println("主线程继续执行" + Thread.currentThread().getName());//名字 main
        //这里就是主线程程序main
        for(int i = 0 ; i <= 60 ; i++){
            System.out.println("main主线程" + i);
            //休眠一秒钟
            Thread.sleep(1000);
        }

    }
}


class Cat extends Thread{
    int times;
    @Override
    public void run() {//重写run方法,写上自己的业务逻辑
        while (true){
            //该线程每隔一秒,在控制台输出 "喵喵,我是小猫咪"
            System.out.println("喵喵,我是小猫咪" + (++times) + "线程名=" + Thread.currentThread().getName());
            //让该线程休眠1秒 ctrl+alt+t
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(times == 80){
                break;//当times到达80 的时候,就会退出while，这个时候线程也就退出了
            }
        }

    }
}
