package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ThreadMethodExercise {
    public static void main(String[] args) {
        /*
        * 线程练习
        * (1):主线程每隔1s,输出hi,一共10次
        * (2):当输出到hi5的时候,启动一个子线程,要求实现Runnable，每隔1s输出hello,等该线程输出10次hello之后,退出
        * (3):主线程继续输出hi，直到主线程退出
        * */
        for (int i = 1 ; i <= 10 ; i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("主线程 hi" + i);

            if(i == 5){
                Thread t = new Thread(new T2());
                t.start();
                try {
                    //插队
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

    }
}

//这里代表的是一个子线程
class T2 implements Runnable{

    @Override
    public void run() {
        for(int i = 1 ; i <= 10 ; i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("hello" + i);
        }
    }
}
