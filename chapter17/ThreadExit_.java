package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 * 线程终止
 */
public class ThreadExit_ {
    public static void main(String[] args) {
        /*
        * 基本说明：
        * 1:当线程完成任务后,会自动退出
        * 2：还可以通过使用变量来控制run方法的退出的方式停止线程,即通知方式
        *
        * 通知停止
        * 需求
        * */

        AThread aThread = new AThread();
        new Thread(aThread).start();

        for(int i = 0 ; i <= 60 ; i++){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("main线程运行中" + i);
            if(i == 30){
                //中断子线程 StopThread
                aThread.setLoop(false);
            }
        }
    }
}

//一个类实现了Runnable接口,那么这个类就是多线程的类
class AThread implements Runnable{
    boolean loop = true;//步骤1：定义标记变量,默认为true
    @Override
    public void run() {
        while (loop){//步骤2：将loop作为循环条件
            try {
                Thread.sleep(50);//让当前线程休眠10ms
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("AThread运行中...");
        }
    }

    //步骤3：提供公共的set方法，用于更新loop

    public void setLoop(boolean loop) {
        this.loop = loop;
    }
}


