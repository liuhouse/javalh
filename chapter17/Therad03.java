package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 * 手写线程的原理
 */
public class Therad03 {
    public static void main(String[] args) {
        Tiger tiger = new Tiger();//实现了Runnable接口
        ThreadProxy threadProxy = new ThreadProxy(tiger);
        threadProxy.start();
    }
}

class Animal{}
class Tiger extends Animal implements Runnable{
    @Override
    public void run() {
        System.out.println("老虎嗷嗷叫...");
    }
}

//线程代理类,模拟了一个极简的Thread类
class ThreadProxy implements Runnable{//你可以把Proxy类当做ThreadProxy
    private Runnable target = null;//属性,类型是Runnable
    @Override
    public void run() {
        if(target != null){
            System.out.println(target.getClass());
            target.run();//动态绑定(运行类型Tiger)
        }
    }

    //构造方法
    public ThreadProxy(Runnable target){
        this.target = target;
    }

    public void start(){
        start0();//这个方法真正实现多线程
    }

    public void start0(){
        run();
    }


}



