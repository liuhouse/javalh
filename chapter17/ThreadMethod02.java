package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 * 常用的方法第二组
 */
public class ThreadMethod02 {
    public static void main(String[] args) throws InterruptedException {
        /*
        * 1.yield:线程的礼让.让出cpu,让其他线程执行,但是礼让的时间不确定,所以不一定礼让成功
        * 2.join:线程的插入,插队的线程一旦插队成功，则肯定先执行完插入的线程的所有任务
        * 案例：main线程创建一个子线程,每隔1s输出hello,输出20次,主线程每隔1s输出hi,输出20次
        * 要求：两个线程同时执行,当主线程输出5次后,就让子线程执行完毕,主线程再继续执行
        *
        * 应用案例
        * 测试yield和join方法,注意体会方法的特点,看老师代码演示
        *
        * */

        //下面代码的正常情况下,是会交替执行的
        //因为当开启子线程之后,子线程就开始执行了,而主线程的程序(for循环)也会继续执行,所以就会交替执行

        //测试join的使用
        T t1 = new T();
        //在main主线程中开启一个子线程,然后激活子线程的任务,然后进行运行
        t1.start();

        /*
        * 说明：
        * 1：让t1子线程插队到主线程前面,这样main线程就会等待t1子线程执行完毕,再继续执行主线程的程序
        * 2:如果没有join,那么,JoinThread和Main线程就会交替执行
        * */
        t1.join();

        for(int i = 0 ; i < 20 ; i++){
            Thread.sleep(50);
            System.out.println("主线程执行");
        }


    }
}

class T extends Thread{
    @Override
    public void run() {
        for(int i = 0 ; i <= 20 ; i++){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("JoinThread..." + i);
        }
    }
}
