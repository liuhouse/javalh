package javalh.chapter17;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ThreadMethod01 {
    public static void main(String[] args) throws InterruptedException {
        /*
        * 常用方法第一组
        * 1:setName // 设置线程的名称,使之与参数name相同
        * 2:getName // 返回该线程的名称
        * 3:start //使该线程开始执行;java虚拟机层调用该线程的start0()方法
        * 4:run //调用线程对象的run()方法
        * 5:setPriority //更改线程的优先级
        * 6:getPriority //获取线程的优先级
        * 7:sleep // 在指定的毫秒数内让正在执行的线程休眠(暂停执行)
        * 8:interrupt // 中断线程
        *
        * 注意事项和细节
        * 1:start底层会创建新的线程,调用run,run就是一个简单的方法调用,不会启动新的线程
        * 2:线程优先级的范围
        * 3:interrupt中断线程,单并没有真正的结束线程,所以一般用于中断正在休眠的线程
        * 4:sleep:线程的静态方法,使当前线程休眠
        * */

        //方式1
        ThreadDemo01 td = new ThreadDemo01();
        //设置线程的名称
        td.setName("刘皓");
        //更改线程的优先级,优先级越大,越靠前
        //优先级分为三种  1：最小优先级     5：默认优先级   10：最大优先级
        td.setPriority(Thread.MIN_PRIORITY);
        td.start();


        //测试优先级
        //优先级越大,越靠前,会先执行
        System.out.println("默认优先级 = " + Thread.currentThread().getPriority());
        //测试interrupt
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Thread.sleep(3000);
        td.interrupt();
    }
}


class ThreadDemo01 extends Thread{
    @Override
    public void run() {
        while(true){
            for(int i = 0 ; i < 100 ; i++){
                System.out.println(Thread.currentThread().getName() + "吃包子-----" + i);
            }
            //每次吃完包子休眠两秒钟
            try {
                System.out.println(Thread.currentThread().getName() + "休眠中...");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                //当线程执行到interrupt方法的时候,就会catch一个异常,可以加入自己的业务代码
                System.out.println(Thread.currentThread().getName() + "被interrupt了");
            }
        }

    }
}
