package javalh.chapter17.sync_;

/**
 * @author 刘皓
 * @version 1.0
 * 线程同步机制
 */
public class Synchronized_ {
    public static void main(String[] args) {
        //线程同步
        /*
        * 先看一个问题
        * 以前面讲过的窗口卖票为例子看问题
        * Synchronized
        * 线程同步机制
        * 1：在多线程编程,一些敏感数据不允许被多个线程同时访问,此时就使用同步访问技术,保证数据在任何的同一时刻,最多有一个线程访问
        *   ，以保证数据的完整性
        *  2:也可以这样理解：线程同步,即当有一个线程在对内存进行操作的时候,其他线程都不可以对这个内存地址进行操作,
        *   直到该线程完成操作,其他线程才能对该内存地址进行操作
        *
        * 同步具体方法 - Synchronized
        * */

        /*
        * 1：同步代码块
        * synchronized (对象) {//得到对象的锁,才能操作同步代码
        *   //需要被同步代码.
        * }
        *
        * 2:synchronized还可以放在声明方法中,表示整个方法-为同步方法
        * public synchronized void main m(String name){
        *   //需要被同步的代码
        * }
        *
        * 3:如何理解线程同步
        * 就好像某个小伙伴上厕所前先把门关上(上锁),完事之后再出来(解锁),那么其他的小伙伴就可以继续的使用厕所了
        *
        * 使用synchronized 解决售票问题
        *
        * */


        /*
        * 互斥锁的基本介绍
        * 1.在java语言中,引入了对象互斥锁的概念,来保证共享数据操作的完整性
        * 2.每个对象都对应于一个可称为"互斥锁"的标记,这个标记用来保证在任一时刻,只能有一个线程访问该对象
        * 3.关键字synchronized来与对象的互斥锁联系.当某个对象用synchronized修饰的时候,表明该对象在任一时刻只能有一个线程访问
        * 4.同步的局限性：导致程序的执行效率要降低
        * 5.同步的方法(非静态的)的锁可以是this，也可以是其他对象(要求是同一个对象)
        * 6.同步方法(静态的)的锁为当前类本身
        * */



        /*
        * 注意事项和使用细节
        * 1：同步方法如果没有使用static修饰：默认锁对象就是this
        * 2: 如果方法使用static修饰,默认锁对象:当前类.class
        * 3:实现的落地步骤
        *   (1):需要分析上锁的代码
        *   (2):选择同步代码块或同步方法
        *   (3):要求多个线程的锁对象为同一个对象即可
        * */




        SellTicket sellTicket = new SellTicket();
        new Thread(sellTicket).start();
        new Thread(sellTicket).start();
        new Thread(sellTicket).start();

    }
}

class SellTicket implements Runnable{
    private int ticketNum = 100;//让多个线程共享ticketNum
    private boolean floor = true;
    Object object = new Object();


    /*
    * 同步方法(静态的)的锁为当前类本身
    * 老韩解读
    * 1.public synchronized static void mi(){} 锁是加在SellTicket.class上的
    * 2.如何在静态代码块中,实现一个同步代码块
    * */
    public synchronized static void m1(){

    }

    //这是一个静态方法,实现同步代码
    public static void m2(){
        synchronized (SellTicket.class){
            System.out.println("m2");
        }
    }

    /*
    * 老韩说明
    * 1：public synchronized void sell(){} 就是一个同步方法
    * 2：这时锁在this对象
    * 3：也可以在代码块上写 synchronized , 同步代码块,互斥锁还是在this对象
    * */

    //去掉synchronized代表不是同步方法,这个时候票就有可能会超卖
    public /*synchronized*/ void sell(){//同步方法,在同一时刻,只能有一个线程来执行sell方法

        //使用代码块来实现线程同步,这个时候票就不会超卖
        //线程同步  锁对象必须是一个对象  如果不是同一个对象 就会出现超卖的情况
        synchronized (object){
            if(ticketNum <= 0){
                System.out.println("售票结束...");
                floor = false;
                return;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("窗口" + Thread.currentThread().getName() + "售出了一张票,剩余票数" + (--ticketNum));
        }

    }

    @Override
    public void run() {
        while (floor){
            sell();
        }
    }
}
