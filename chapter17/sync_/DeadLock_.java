package javalh.chapter17.sync_;

/**
 * @author 刘皓
 * @version 1.0
 * 线程的死锁
 */
public class DeadLock_ {
    public static void main(String[] args) {
        /*
        * 线程死锁的基本介绍
        * 多个线程都占用了对方的锁资源,但不肯想让,导致了死锁,在编程中是一定要避免死锁的发生
        * 应用案例：
        * */

        //模拟死锁的现象

        DeadLockDemo A = new DeadLockDemo(true);
        A.setName("A线程");
        DeadLockDemo B = new DeadLockDemo(false);
        B.setName("B线程");
        A.start();
        B.start();
    }
}



//线程
class DeadLockDemo extends Thread{
    static Object o1 = new Object();//静态是保证多线程的,共享一个对象,这里使用static
    static Object o2 = new Object();//静态是保证多线程的,共享一个对象,这里使用static
    boolean flag;

    //构造器
    public DeadLockDemo(boolean flag){
        this.flag = flag;
    }

    //重写线程中的run方法
    
    @Override
    public void run() {
        /*
        * 下面的业务逻辑分析
        * 1:如果flag为true，线程A就会得到/持有o1 对象锁,然后尝试去获取o2对象锁
        * 2:如果线程A得不到o2的对象锁,就会Blocked
        * 3:如果flag为F,线程就会得到/持有o2对象锁,然后尝试去获取o1对象锁
        * 4：如果线程B得不到o1的对象锁,就会Blocked
        *
        * 如果是两两不想让,就会形成线程死锁   写程序的时候要避免出现这种情况
        * */


        if(flag){
            synchronized (o1){//对象互斥锁,这里被synchronized,下面就是同步代码
                System.out.println(Thread.currentThread().getName() + " 进入1");
                //进入对象o1的时候去监视o1
                synchronized (o2){//这里获取o2的监视权
                    System.out.println(Thread.currentThread().getName() + " 进入2");
                }
            }
        }else{
            synchronized (o2){
                System.out.println(Thread.currentThread().getName() + " 进入3");
                synchronized (o1){//这里获取li对象的监视权
                    System.out.println(Thread.currentThread().getName() + " 进入4");
                }
            }
        }
    }
}
