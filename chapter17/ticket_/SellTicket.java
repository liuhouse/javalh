package javalh.chapter17.ticket_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class SellTicket {
    public static void main(String[] args) {
        /*
        * [售票系统],编程模拟三个售票窗口售票100张，分别使用 继承Thread和实现Runnable方式,并分析有什么问题
        * */
        //使用Thread的方式进行售票

        /*
        * 使用下面的售票方式会出现的问题！
        * 因为每个进程进来之后都会先判断当前的票量是否还有剩余,如果三个线程同时进入判断,比如此时票数只剩下一个
        * 也就是三个进程都符合条件,所以会出现把相同的一张票重复卖的情况,还会出现卖负票的情况     这种情况肯定是不允许的
        * 经典的例子  淘宝上的秒杀
        * */

        /*
            使用继承的方式来实现售票
            也是同样会遇到相同的问题，三个线层同时来抢资源,发现票还有1个,所以三个都能卖票,所以会出现卖负票的情况
            1 - 0 - 1 = -1    =   -2   相当于多卖了两张不存在的票  这是不允许的
            //第一个窗口
            SellTicket01 sellTicket01 = new SellTicket01();
            //第二个窗口
            SellTicket01 sellTicket02 = new SellTicket01();
            //第三个窗口
            SellTicket01 sellTicket03 = new SellTicket01();
            //启动三个线程
            sellTicket01.start();
            sellTicket02.start();
            sellTicket03.start();

         */

        System.out.println("==使用接口的方式来实现售票==");
        SellTicket02 sellTicket02 = new SellTicket02();
        new Thread(sellTicket02).start();//第一个线程  窗口
        new Thread(sellTicket02).start();//第二个线程  窗口
        new Thread(sellTicket02).start();//第三个线程  窗口
    }
}


//使用Thread方式
class SellTicket01 extends Thread{
    //让多个线程共享ticketNum，使用静态属性就可以实现共享属性问题
    private static int ticketNum = 100;

    //这样是锁不住的,因为每个锁都是对象本身,他的对象本身都是独立的,所以这里就相当于是自娱自乐,毫无意义
    public void m1(){
        synchronized (this){
            System.out.println("hello");
        }
    }

    @Override
    public void run() {
        while (true){
            //如果票数小于等于0就代表现在票已经卖完了
            if(ticketNum <= 0){
                System.out.println("售票结束了...");
                break;
            }

            //休眠50毫秒，模拟售票
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("窗口 " + Thread.currentThread().getName() + "售出一张票"
                + " 剩余票数 = " + (--ticketNum)
            );

        }
    }
}


//实现接口实现
class SellTicket02 implements Runnable{
    private int ticketNum = 100;//让多个线程共享ticketName，也就是启动多个线程
    @Override
    public void run() {
        while (true){
            if(ticketNum <= 0){
                System.out.println("售票结束....");
                break;
            }

            //休眠50毫秒,模拟
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("窗口 " + Thread.currentThread().getName() + "售出一张票 " + "剩余票数 = " + (--ticketNum));
        }
    }
}

