package javalh.chapter15;

import org.junit.jupiter.api.Test;

import java.util.*;

public class HomeWork01 {
    public static void main(String[] args) {

    }

    @Test
    public void test(){
        DAO<User> users = new DAO<>();
        //添加数据到自定义泛型类中
        users.save("1",new User(1,18,"jack"));
        users.save("2",new User(2,19,"smith"));
        users.save("3",new User(3,20,"rose"));
        users.update("1",new User(1,19,"JACKS"));
        System.out.println(users.get("1"));
        users.delete("2");
        System.out.println(users.list());

    }
}


class DAO<T>{

    //切记这里,如果想要添加元素的话,首先必须进行实例化
    private Map<String,T> m = new HashMap<>();

    //保存T类型的对象到Map成员变量中
    public void save(String id , T entity){
        m.put(id,entity);
    }

    //从map中获取id对应的对象
    public T get(String id){
        return m.get(id);
    }

    //替换map中key为id的内容,修改为entity对象
    public void update(String id , T entity){
        m.put(id,entity);
    }

    //返回map中存放的所有T对象
    public List<T> list(){
        Set<String> strings = m.keySet();
        ArrayList<T> ts = new ArrayList<>();
        for (String s :strings) {
            ts.add(m.get(s));
        }
        return ts;
    }

    //删除指定id对象
    public void delete(String id){
        m.remove(id);
    }

}


class User{
    private int id;
    private int age;
    private String name;

    public User(int id, int age, String name) {
        this.id = id;
        this.age = age;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
