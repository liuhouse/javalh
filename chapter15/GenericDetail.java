package javalh.chapter15;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 刘皓
 * @version 1.0
 */
public class GenericDetail {
    public static void main(String[] args) {
        /*
        * 泛型的注意事项和使用细节
        * 1:interface List<T>{} , public class HashSet<E>{}...等等
        *   说明：T,E只能是引用类型
        *
        * 2:在给泛型指定具体的类型之后,可以传入该类型或者该类型的子类类型
        * 3.泛型的使用形式
        * List<Integer> list1 = new ArrayList<Integer>();
        * List<Integer> list2 = new ArrayList<>();
        * 4:如果我们这样写 List list3 = new ArrayList();默认给它的泛型是[ <E> E就是Object ]
        * */

        //看看下面的语句是不是正确
        //1:给泛型指向的数据类型是，要求是引用类型,不能是基本数据类型
        ArrayList<Integer> integers = new ArrayList<Integer>();//OK
        //ArrayList<int> ints = new ArrayList<int>();//这是错误的,因为int是基本数据类型

        //2:说明
        //因为E指定了A类型.所以构造器传入的是new A()
        //在给泛型指定具体类型之后,可以传入该类型或者其子类类型
        Pig<A> aPig = new Pig<A>(new A());
        //运行类型是A
        aPig.f();//class javalh.chapter15.

        //运行类型是B
        Pig<A> aPig1 = new Pig<A>(new B());
        aPig1.f();

        //3:泛型的使用形式
        ArrayList<Integer> integers1 = new ArrayList<Integer>();
        List<Integer> integers2 = new ArrayList<Integer>();

        //在实际开发中,我们往往是简写的
        //编译器会进行类型推断,老师推荐使用下面的写法
        //因为默认的会把当前遍历类型的泛型类型 作为 实例化时候的泛型类型
        ArrayList<Integer> integers3 = new ArrayList<>();
        ArrayList<Pig> pigs = new ArrayList<>();

        //4.如果是这样写,泛型默认就是Object，也就是默认情况下,任意类型的数据都是可以进行接收的
        //等价于 ArrayList<Object> arrayList = new ArrayList<>();
        /*
        实际上默认就是你传入的对象   Object
        *  public boolean add(E e) {
                modCount++;
                add(e, elementData, size);
                return true;
            }
        * */

        ArrayList arrayList = new ArrayList();
        arrayList.add(new Tiger("老虎"));




    }
}



class A{}

class B extends A{}

//类
class Tiger<E>{
    E e;
    public Tiger(){}
    public Tiger(E e){
        this.e = e;
    }
}

//定义一个Pig类型,泛型为E
class Pig<E>{
    E e;
    public Pig(E e) {
        this.e = e;
    }
    //获取运行类型
    public void f(){
        System.out.println(e.getClass());
    }
}




