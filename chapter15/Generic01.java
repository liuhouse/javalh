package javalh.chapter15;

import java.util.ArrayList;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Generic01 {
    public static void main(String[] args) {
        /*
        * 需求：
        * (1):请编写一个程序,在ArrayList中,添加3个Dog对象
        * (2):Dog对象含有name和age，并输出name和age(要求使用getXxx)
        * 先使用传统的方法来进行解决,然后引出泛型
        * */

        //使用传统的方法来解决
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Dog("旺财",10));
        arrayList.add(new Dog("发财",1));
        arrayList.add(new Dog("小黄",5));

        //假如我们的程序员,不小心,添加了一只猫
        //这里的编译器是不会报错的,因为语法都是正确的,但是在运行的时候会报错  ClassCastException
        //报错是在遍历的时候进行向下转型的时候报错的  将猫转化为狗自然是错误的
        arrayList.add(new Cat("招财猫",8));


        //进行遍历
        for (Object o :arrayList) {
            //这里需要对象的向下转型，才能访问到对象的属性
           Dog dog = (Dog) o;
           //得到狗的名字和狗的年龄
            System.out.println(dog.getName() + " - " + dog.getAge());
        }


    }
}



class Dog{
    private String name;
    private int age;

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}


class Cat{
    private String name;
    private int age;

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}