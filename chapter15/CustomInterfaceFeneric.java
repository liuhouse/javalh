package javalh.chapter15;

public class CustomInterfaceFeneric {
    public static void main(String[] args) {
        /*
        * 自定义泛型接口
        * 基本语法
        * interface 接口名<T,R>{}
        * 注意细节
        * (1):接口中,静态成员不能使用泛型(这个和泛型类的规定是一样的)
        * (2):泛型接口的类型,在继承接口或者实现接口的时候确定
        * (3):没有指定类型,默认为Object
        * */

    }
}

//在继承接口的时候指定泛型接口的类型
//这就就代表  U=>String R => Double
interface IA extends IUsb<String,Double>{

}


//当我们去实现接口IA的时候,因为IA在继承IUsb接口的时候，指定了U为String,R为Double
//在实现Iusb接口的方法的时候,使用String替换U，使用Double替换R
class AA implements IA{

    @Override
    public Double get(String s) {
        return null;
    }

    @Override
    public void hi(Double aDouble) {

    }

    @Override
    public void run(Double r1, Double r2, String u1, String u2) {

    }
}

//实现接口的时候，直接指定泛型接口的类型
//给U指定Integer , 给R指定Float
class BB implements IUsb<Integer,Float>{

    @Override
    public Float get(Integer integer) {
        return null;
    }

    @Override
    public void hi(Float aFloat) {

    }

    @Override
    public void run(Float r1, Float r2, Integer u1, Integer u2) {

    }
}


//没有指定类型,默认是Object
//建议直接写成IUsb<Object,Object>
//下面的继承等价于
//   class CC implements IUsb<Object,Object>
class CC implements IUsb{

    @Override
    public Object get(Object o) {
        return null;
    }

    @Override
    public void hi(Object o) {

    }

    @Override
    public void run(Object r1, Object r2, Object u1, Object u2) {

    }
}


//自定义泛型接口
interface IUsb<U,R>{
    //接口中,静态成员不能使用泛型(这个和泛型类的规定是一样的),接口的成员默认都是静态的
//    R name;
    //普通方法中,可以使用接口泛型
    //返回的类型是R ，参数的类型是U
    R get(U u);
    void hi(R r);
    //普通方法中的参数可以使用泛型
    void run(R r1,R r2,U u1,U u2);
    //在jdk8中,可以在接口中使用默认方法
    default R method(U u){
        return null;
    }
}