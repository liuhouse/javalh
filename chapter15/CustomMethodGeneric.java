package javalh.chapter15;

import java.util.ArrayList;

public class CustomMethodGeneric {
    public static void main(String[] args) {
        /*
        * 自定义泛型方法
        * 修饰符<T,R> 返回类型 方法名(参数列表){}
        * */

        Car car = new Car();
        //当调用方法的时候,传入参数的时候,编译器就会确定方法中泛型的类型
        //比如这里,T就代表String   R就代表Integer
        car.fly("宝马",1000);

        System.out.println("=-================");
        //当调用方法的的时候,就会确定泛型的类型<T,R> T代表Integer   R代表Float
        car.fly(300,100.1f);


        System.out.println("=====================");
        Fish<String, ArrayList> fish = new Fish<>();
        //调用泛型类里面的泛型接口
        fish.eat("ALL",100);


        System.out.println("=====================");
        fish.hello(new ArrayList(),11.3f);

    }
}

//泛型方法,可以定义在普通类中,也可以定义在泛型类中

//普通类
class Car{
    //普通方法
    public void run(){

    }

    //泛型方法
    //(1).<T,R>就是泛型
    //(2).是提供给fly方法使用的
    //一般定义了,就肯定是要使用的,不然是没有任何意义的
    public <T,R> void fly(T t , R r){
        System.out.println(t.getClass());
        System.out.println(r.getClass());
    }
}


//在泛型类中使用
class Fish<T,R>{
    //普通方法
    public void run(){}
    //泛型方法
    public <U,M> void eat(U u , M m){
        System.out.println(u.getClass());
        System.out.println(m.getClass());
    }

    //泛型方法,可以使用类声明的泛型,也可以使用自己声明的泛型
    public <K> void hello(R r,K k){
        System.out.println(r.getClass());
        System.out.println(k.getClass());
    }

    //这边要说明一下
    //1:下面的hi方法不是泛型方法
    //2:是hi方法使用了类声明的泛型
    public void hi(T t){

    }
}