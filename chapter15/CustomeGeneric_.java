package javalh.chapter15;

/**
 * @author 刘皓
 * @version 1.0
 */
public class CustomeGeneric_ {
    public static void main(String[] args) {
        /*
         自定义泛型类
         基本语法
        class 类名<T,R,...>{//...表示可以有多个泛型
            成员
        }
        注意细节
        (1)普通成员可以使用泛型(属性,方法)
        (2)使用泛型的数组,不能初始化
        (3)静态方法中不能使用类的泛型
        (4)泛型类的类型,是在创建对象的时候创建的(因为创建对象的时候,需要指定确定类型)
        (5)如果在创建对象的时候,没有指定类型,默认为Object

        * */

        //T => Double , R => String , M => Integer
        MyTiger<Double,String,Integer> g = new MyTiger<>("john");
        g.setT(10.9);//ok
        //g.setT("yy");//错误的,类型不对,要求必须传入Double类型
        System.out.println(g);

        //此时没有指定泛型的类型,所以默认都是Object
        //OK T=>Object   R=>Object  M=>Object   也就是传递任何类型的的参数都是可以的
        MyTiger myTiger = new MyTiger("json---");
        myTiger.setT("yy");//OK的 T=>Object "yy" => String   String 是 Object的子类
        System.out.println("myTiger = " + myTiger);
    }
}

/*
* 老韩解读
* 1：MyTiger后面泛型,所以我们就把MyTiger就称为自定义泛型类
* 2:T,R,M 泛型的标识符,一般是单个的大写字母
* 3:泛型标识符可以有多个
* 4:普通成员可以使用泛型(属性,方法)
* 5:使用泛型的数组,不能进行初始化
* 6:静态方法中不能使用类的泛型
* */

class MyTiger<T,R,M>{
    String name;
    R r;
    M m;//属性使用泛型
    T t;

    //这是错误的,因为数组在new不能确定T的类型，无法在内存中开辟对象
    //只有实例化对象的时候才能确定类型
    //T[] ts = new T[5];

    public MyTiger(String name) {
        this.name = name;
    }


    //构造器使用泛型
    public MyTiger(String name, R r, M m, T t) {
        this.name = name;
        this.r = r;
        this.m = m;
        this.t = t;
    }

    //构造器使用泛型
    public MyTiger(R r, M m, T t) {
        this.r = r;
        this.m = m;
        this.t = t;
    }

    //静态方法中不能使用泛型
    //因为静态是和类相关的,在类加载的时候,对象还没有被创建
    //所以如果静态方法和静态属性使用了泛型的话,JVM就无法完成初始化
//    public static void m1(M m){
//
//    }
    //static R r2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //方法使用泛型
    public R getR() {
        return r;
    }

    public void setR(R r) {
        this.r = r;
    }

    public M getM() {
        return m;
    }

    public void setM(M m) {
        this.m = m;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}



