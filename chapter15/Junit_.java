package javalh.chapter15;

import org.junit.jupiter.api.Test;

public class Junit_ {
    public static void main(String[] args) {
        /*
        * 1.Junit是java语言的单元测试框架
        * 2.多数的java开发环境已经集成了JUnit作为单元测试工具
        * */
        //这样的单元测试真的是很麻烦
        //使用传统的方法进行测试
//        new Junit_().m1();
        //new Junit_().m2();
    }

    @Test
    public void m1(){
        System.out.println("m1方法被调用");
    }

    @Test
    public void m2(){
        System.out.println("m2方法被调用");
    }
}
