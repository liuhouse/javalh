package javalh.chapter15;

import java.util.*;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings("all")
public class GenericExercise {
    public static void main(String[] args) {

        /*
        泛型的声明
        interface 接口<T>{}
        public interface List<E>

        class 类<K,V>{}
        public class ArrayList<E>

        说明：
        (1)T,K,V不代表值,而是表示类型
        (2)任意字母都可以,常用T表示,是Type的缩写

        泛型的实例化
        要在类名后面指定类型参数的值
        (1)List<String> strList = new ArrayList<String>();
        (2)Iterator<Customer> iterator = customers.iterator();

        * */

        /*
        * 泛型的使用举例
        * 举例说明,泛型在HashSet,HashMap的使用情况
        * (1)创建3个学生对象
        * (2)放入到HashSet中学生对象使用
        * (3)放入到HashMap中,要求key是String name,Value就是学生对象
        * (4)使用两种遍历方式
        * */

        //使用泛型的方式给HashSet放入3个学生对象
        //后面的实例化对象后面是Student类型,代表只能添加Student对象
        //遍历类型后面<Student>说明编译类型是Student，访问对象的时候可以直接访问
        HashSet<Student> students = new HashSet<Student>();
        students.add(new Student("jack",18));
        students.add(new Student("tom",28));
        students.add(new Student("mary",19));
        //因为编译类型是Student，所以不需要进行向下转型
        for (Student student :students) {
            System.out.println(student.getName() + "--" + student.getAge());
        }


        //使用泛型的方式给HashMap传入3个学生对象
        //K->String V->Student
        HashMap<String, Student> hm = new HashMap<String,Student>();
        /*
        * public class HashMap<K,V>{}
        * */
        hm.put("milan",new Student("milan",38));
        hm.put("smith",new Student("smith",48));
        hm.put("hsp",new Student("hsp",28));

        //使用迭代器进行遍历
        System.out.println("使用迭代器进行遍历=========");
        Set<String> strings = hm.keySet();
        for (String key :strings) {
            System.out.println(hm.get(key));
        }


        /*


         public Set<Map.Entry<K,V>> entrySet() {
                Set<Map.Entry<K,V>> es;
                return (es = entrySet) == null ? (entrySet = new EntrySet()) : es;
         }

        * */


        //迭代器 EntrySet
        Set<Map.Entry<String, Student>> entries = hm.entrySet();
        /*

        public final Iterator<Map.Entry<K,V>> iterator() {
            return new EntryIterator();
        }

        * */

        Iterator<Map.Entry<String, Student>> iterator = entries.iterator();
        System.out.println("===============MapEntry=================");
        while (iterator.hasNext()) {
            Map.Entry<String, Student> next = iterator.next();
            System.out.println(next.getKey() + " - " + next.getValue().getName() + " - " + next.getValue().getAge());
        }


    }
}


class Student{
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
