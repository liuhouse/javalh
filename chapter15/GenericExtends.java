package javalh.chapter15;

import java.util.ArrayList;
import java.util.List;

public class GenericExtends {
    public static void main(String[] args) {
        /*
        * 泛型的继承和通配符
        * (1)泛型不具备继承性
        * (2)<?>:支持任意泛型类型
        * (3)<? extends A>:支持A类以及A类的子类,规定了泛型的上限
        * (4)<? super A>:支持A类以及A类的父类但不局限于直接父类,规定了泛型的下限
        * */

        //(1)泛型不具备继承性 ,编译的时候跟实力化的时候必须一致
        //ArrayList<Object> strings = new ArrayList<String>();

        //举例说明下面三个方法的使用
        ArrayList<Object> list1 = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();
        ArrayList<AA1> list3 = new ArrayList<>();
        ArrayList<BB1> list4 = new ArrayList<>();
        ArrayList<CC1> list5 = new ArrayList<>();


        //如果是List<?> c，可以接受任意的泛型类型
        printCollection1(list1);
        printCollection1(list2);
        printCollection1(list3);
        printCollection1(list4);
        printCollection1(list5);

        //如果List<? extends AA1> c 规定了上限,只能是AA1类或者AA1的子类
        //printCollection2(list1);//错误的 Object不是AA1的子类
        //printCollection2(list2);//错误的 String 不是AA1的子类
        printCollection2(list3);//正确的 ,因为是AA1类
        printCollection2(list4);//正确的，因为BB1是AA1的子类
        printCollection2(list5);//正确,因为CC1是AA1的子类

        //List<? super AA1> c 规定了下限,支持AA1类或者AA1的父类,但不局限于直接父类
        printCollection3(list1);//正确的  Object是AA1的父类
        //printCollection3(list2);//错误的,因为String不是AA1的父类
        printCollection3(list3);//正确的AA1
        //printCollection3(list4);//错误的  BB1是AA1的子类
        //printCollection3(list5);//错误的 CC1是AA1的子类

    }


    //说明：List<?> 表示  任意的泛型类型都可以接受
    public static void printCollection1(List<?> c){
        for (Object o : c) {
            System.out.println(o);
        }
    }


    //List<? extends AA1>  表示最多到AA1  可以接受AA1或者AA1的子类  这里规定了上限是AA1
    public static void printCollection2(List<? extends AA1> c){
        for (Object o : c) {
            System.out.println(o);
        }
    }

    //List<? super AA1>子类类名AA1，表示支持AA1或者AA1的父类,但不局限于直接父类 规定了泛型的下限
    public static void printCollection3(List<? super AA1> c){
        for (Object o : c) {
            System.out.println(o);
        }
    }


}


class AA1{}
class BB1 extends AA1{}
class CC1 extends BB1{}
