package javalh.chapter15;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

/**
 * @author 刘皓
 * @version 1.0
 */
public class GenericExercise02 {
    public static void main(String[] args) {
        /*
        * 泛型课堂练习题
        * 定义Employee类
        * (1)该类包含：private成员变量name,sal,birthday，其中birthday为MyDate类的对象
        * (2)为每一个属性定义getter，setter方法
        * (3)重写toString方法输出name,sal,birthday
        * (4)MyDate类包含:private成员变量month,day,year;并且为每一个属性定义getter,setter方法
        * (5)创建该类的3个对象,并把这些对象放入ArrayList集合中(ArrayList需要使用泛型来进行定义),对集合中的元素进行排序
        *       并且遍历输出
        *
        * 排序方式：调用ArrayList的sort方法,传入Comparator对象,使用泛型,先按照name进行排序,如果name相同
        * 就按照生日日期进行先后排序
        * */
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee("cack",1000,new MyDate(1995,11,10)));
        employees.add(new Employee("dmith",2000,new MyDate(2001,12,6)));
        employees.add(new Employee("cack",500,new MyDate(1995,11,8)));

        employees.sort(new Comparator<Employee>() {
//            /

            /*
            * 调用ArrayList的sort方法,传入Comparator对象,使用泛型,先按照name进行排序,如果name相同
             * 就按照生日日期进行先后排序
            * */

            @Override
            public int compare(Employee o1, Employee o2) {
                if(!(o1 instanceof Employee && o2 instanceof Employee)){
                    System.out.println("类型不正确...");
                    return 0;
                }

                //把两个对象的名称进行比较,最终会返回一个数字
                int n = o1.getName().compareTo(o2.getName());
                //如果数字不等于0,就等于其实已经比较出两个字符串的大小了,然后就可以直接进行返回了
                if(n != 0){
                    return n;
                }

                //下面是对birthday的比较,因此我们最好把这个比较,放在MyDate中完成
                //封装后,将来的可维护性就大大的增强
                return o1.getBirthday().compareTo(o2.getBirthday());
            }
        });

        //进行遍历
        for (Employee employee :employees) {
            System.out.println(employee);
        }


    }
}


//员工类
class Employee{
    private String name;
    private double sal;
    private MyDate birthday;

    public Employee(String name, double sal, MyDate birthday) {
        this.name = name;
        this.sal = sal;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSal() {
        return sal;
    }

    public void setSal(double sal) {
        this.sal = sal;
    }

    public MyDate getBirthday() {
        return birthday;
    }

    public void setBirthday(MyDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", sal=" + sal +
                ", birthday=" + birthday +
                '}';
    }
}


//比较的时候只能传递Employee泛型
//MyDate类实了Comparable接口,所以一定要实现此接口的compareTo()方法
class MyDate implements Comparable<MyDate>{
    private int year;
    private int month;
    private int day;

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "MyDate{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyDate myDate = (MyDate) o;
        return year == myDate.year &&
                month == myDate.month &&
                day == myDate.day;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, day);
    }

    @Override
    public int compareTo(MyDate m) {
        //如果相等就按照生日的先后顺序进行排序
        //比较年份的大小
        //这里的this指的就是当前的对象,谁调用此方法,谁就是当前对象,这里的this指的是o1里面的MyDate对象
        //这里的year就是o1里面的MyDate对象里面的year属性

        //而这里的m，指的就是要比较的对象,参数里面传递过来的对象 这里指的是o2里面的额MyDate对象
        //这里的getYear()就是o2里面MyDate对象里面的getYear()方法
        int year_cha = this.year - m.getYear();
        //如果年份的差不为0,那么就证明已经比较出大小了,那么就可以直接返回
        if(year_cha != 0){
            return year_cha;
        }

        //比较月的大小
        int month_cha = month - m.getMonth();
        //如果月份的差不为0,那么就证明已经比较出大小了,那么也就可以直接返回
        if(month_cha != 0){
            return month_cha;
        }

        //最后比较日的大小,不管是不是等于0都要进行返回,因为已经是最后的了
        return day - m.getDay();
    }
}