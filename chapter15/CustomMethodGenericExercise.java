package javalh.chapter15;

public class CustomMethodGenericExercise {
    public static void main(String[] args) {
        /*
        * 自定义泛型方法
        * 课堂练习，下面的代码是不是正确,如果有错误,修改正确,并说明输出什么？
        * */
        //下面的代码输出什么
        Apple<String, Double, Integer> apple = new Apple<>();
        apple.fly(10);
        apple.fly(new Dog2());
    }
}


//自定义泛型类
class Apple<T,R,M>{
    //泛型方法,E是给本方法使用的
    //泛型方法的参数是在调用的时候确定的额,传入的是什么类型,最终的参数就是什么类型
    public <E> void fly(E e){
        System.out.println(e.getClass().getSimpleName());
    }

    //public void eat(U u){}//这是错误的,因为U并没有进行声明

    //正确的
    public void run(M m){

    }
}


class Dog2{}


