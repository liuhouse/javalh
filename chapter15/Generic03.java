package javalh.chapter15;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Generic03 {
    public static void main(String[] args) {
        //注意：特别强调：E具体的数据类型在定义Person对象的时候指定,即在编译期间，就确定E是什么类型
        Person<String> hsp = new Person<String>("韩顺平教育");
        hsp.show();

        //这里限定了传递过去的参数只能是Integer类型
        Person<Integer> integerPerson = new Person<Integer>(100);
        integerPerson.show();
        /*
        你可以这样理解,上面的Person类
        //E => String
        class Person<String>{
            //E表示s的数据类型,该数据类型在定义Person对象的时候指定,在编译期间,就确定了E是什么类型,传入的参数必须是什么类型
            String s;

            //E也可以是参数类型    这里的E也代表String
            public Person(E s){
                this.s = s;
            }


            //E也可以代表返回类型E 这里的E代表String 代表返回的数据类型也必须是String
            public E f(){
                return s;
            }

            public void show(){
                System.out.println(s.getClass());
            }

        }


        * */

        //泛型的作用是：可以在类声明的时候通过一个标识来表示类中某个属性的类型
        //或者是某个方法或者某个返回值的类型,或者是参数的类型

    }
}


class Person<E>{
    E s;
    public Person(E s){
        this.s = s;
    }

    public E f(){
        return s;
    }

    public void show(){
        System.out.println(s.getClass());
    }

}