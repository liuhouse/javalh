package javalh.chapter15;

import java.util.ArrayList;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Generic02 {
    public static void main(String[] args) {
        /*
        * 使用传统的方法解决问题分析
        * (1)不能对加入到集合ArrayList中的数据类型进行约束(不安全)
        * (2)遍历的时候需要进行类型转换,如果集合中的数量比较大的时候,会影响效率
        *   泛型的快速体验-使用泛型来解决前面的问题
        * */


        /*
        * 使用传统的方法来解决  ===> 使用泛型
        * 老韩解读
        * 1:当我们ArrayList<Dog1>表示存放到ArrayList集合中的额元素只能是Dog1类型(细节后面说)
        * 2:如果编译器发现添加的类型,不满足要求,就会直接报错,也就是编译不能通过
        * 3:在遍历的时候,可以直接取出Dog1类型,而不是Object,不用进行向下转型操作
        * 4:public class ArrayList<E>{} E称为泛型 , 那么 Dog -> E
        * */


        /*
        * 使用泛型来完成需求
        * */
        ArrayList<Dog1> array_list = new ArrayList<Dog1>();
        array_list.add(new Dog1("旺财",10));
        array_list.add(new Dog1("发财",1));
        array_list.add(new Dog1("小黄",5));

        //这里是错误的,因为已经限定了只能存放Dog1的实例化对象
        //array_list.add(new Cat("小花",6));


        //使用泛型进行遍历
        System.out.println("-----====使用泛型进行遍历==========");
        for (Dog1 dog1 : array_list) {
            System.out.println(dog1.getName() + "---" + dog1.getAge());
        }


    }
}

class Dog1{
    private String name;
    private int age;

    public Dog1(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}


