package javalh.chapter12.try_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class TryCatchExercise02 {
    public static void main(String[] args) {
        System.out.println(method());
    }


    public static int method(){
        int i = 1;
        try{
            i++;//i=2
            String[] names = new String[3];
            //数组中的元素全部都是null，所以使用null做运算或者比较的时候,会报空指针的错误
            if(names[1].equals("tom")){//NullPointException
                System.out.println(names[1]);
            }else{
                names[3] = "hspedu";
            }
            return 1;
        }catch (ArrayIndexOutOfBoundsException e){
            return 2;
        }catch (NullPointerException e){
            return ++i;//i=3
        }finally {//必须执行
            return ++i;//i=4
        }
    }

}
