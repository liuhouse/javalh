package javalh.chapter12.try_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class TryCatchDetail01 {
    public static void main(String[] args) {

        /*
        * (1)如果异常发生了,则异常后面的代码将不会执行,直接进入到catch块
        * (2)如果异常没有发生,就顺序的执行try代码块,不会进入catch
        * (3)如果希望不管异常是否发生,都执行某段代码(比如关闭连接,释放资源等,则使用如下的代码 -- finally)
        * */

        String str = "1212";
        try{
            int a = Integer.parseInt(str);
            System.out.println("数字" + a);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            System.out.println("不管是否发生异常,始终执行的代码...");
        }
        System.out.println("程序继续执行...");

    }
}


