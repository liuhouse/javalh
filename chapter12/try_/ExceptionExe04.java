package javalh.chapter12.try_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ExceptionExe04 {
    public static void main(String[] args) {
        System.out.println(method());
    }

    public static int method(){
        int i = 1; // i =1
        try{
            i++;// i =2
            String[] names = new String[3];
            if(names[1].equals("tom")){//空指针
                System.out.println(names[1]);
            }else{
                names[3] = "hspedu";
            }
            return 1;
        }catch(ArrayIndexOutOfBoundsException e){
            return 2;
        }catch (NullPointerException e){
            //但是这里最后是以返回的为最准的,因为 finally中没有返回   所以这里使用的是 临时变量  temp = 3
            return ++i;// i = 3   => 保存到临时变量 temp = 3
        }finally {//最后一定是会执行的,所有这里i = 4
            ++i;// i = 4
            System.out.println("i = " + i);//i = 4
        }
    }
}
