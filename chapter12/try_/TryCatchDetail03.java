package javalh.chapter12.try_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class TryCatchDetail03 {
    public static void main(String[] args) {

        /*
        *
        * 可以进行try - finally 来配合使用,这种用法相当于是没有捕获异常
        * 因此程序会直接崩溃掉,退出执行   应用场景,就是执行一段代码,不管是不是发生异常,都必须执行某个业务逻辑
        * */

        try{
            int n1 = 10;
            int n2 = 0;
            System.out.println(n1 / n2);
        }finally {
            //finally的主要作用就是不管是不是捕捉到异常finally都会执行的  存在是非常的有必要的，即使程序崩溃,finally也会是执行的
            System.out.println("执行了finally。。。。");
        }
        //如果程序崩掉了,没有使用catch捕捉异常的话,下面的程序是不会继续向下执行的
        System.out.println("程序继续执行....");
    }
}


