package javalh.chapter12.try_;

import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 */
public class TryCatchExercise04 {
    static boolean flag = true;
    static String str = "";
    public static void main(String[] args) {
        /*
        * 需求
        * 如果用户输入的不是一个整数,就提示他反复输入,直到输入一个整数为止
        * */
//        is_print();

        is_print_two();
    }

    public static void is_print(){

        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("请输入一个整数:");
            str = scanner.next();
            try {
                int res = Integer.parseInt(str);
                System.out.println("输入的内容是 "+res);
                flag = false;
            }catch (NumberFormatException e){
                System.out.println("您输入的不对");
            }
        }while (flag);
    }


    public static void is_print_two(){
        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.println("请输入一个整数");
            str = scanner.next();
            try{
                int res = Integer.parseInt(str);
                System.out.println("您输入的数字是" + res);
                break;
            }catch (NumberFormatException e){

            }
        }
    }
}
