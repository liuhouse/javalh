package javalh.chapter12.try_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class TryCatchDetail02 {
    public static void main(String[] args) {

        /*
        * 可以有多个catch语句,捕获不同的异常(进行不同的业务处理),要求父类异常在后,子类异常在前,比如Exception在后,NullPointerException在前
        * 如果发生异常的话，只会匹配一个catch
        * */

        /*
        * 老韩解读
        * 1:如果try代码块有可能有多个异常
        * 2:可以使用多个catch分别捕获不同的异常,相应的进行处理
        * 3:要求子类异常写在父类异常的前面,父类异常写在最后面
        * */

        try{
            Person person = new Person();
//            person = null;
            System.out.println(person.getName());//NullPointerException
            int n1 = 10;
            int n2 = 0;
            int res = n1 / n2;
            System.out.println("n1/n2" + res);//ArithmeticException
        }catch (NullPointerException e){
            System.out.println("空指针异常=" + e.getMessage());
        }catch (ArithmeticException e){
            System.out.println("算术异常=" + e.getMessage());
        } catch (Exception e){
            System.out.println("父类报错 " + e.getMessage());
        }finally {
            System.out.println("释放资源...");
        }
    }
}



class Person{
    private String name = "jack";

    public String getName() {
        return name;
    }
}

