package javalh.chapter12.try_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class TryCatch01 {
    public static void main(String[] args) {
        /*
        * (1)java提供try和catch块来处理异常,try块用于包含可能出错的代码,catch块用于处理try块中发生的异常，可以根据需要在程序中有多个try...catch{}块
        * (2)基本语法
        * try{
        *   可疑代码
        *   将异常生成对应的异常对象,传递给catch块
        * }catch(异常){
        *   对异常进行处理
        * }
        * 如果没有finally，语法是可以通过的
        * */

        //快速入门
        int num1 = 10;
        int num2 = 0;
        try{
            //如果这里出现了异常,那么就会被Exception捕捉到然后给对象e 传递给catch   try里面的代码遇到异常之后就不会继续向下执行了
            int res = num1 / num2;
            System.out.println(res);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        //捕捉完毕之后  后面的代码还是可以继续执行的
        System.out.println("程序仍然在执行...");


    }
}
