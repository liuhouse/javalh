package javalh.chapter12.exception_;

/**
 * @author 刘皓
 * @version 1.0
 * 数学运算异常
 *
 */
public class ArithmeticException_ {
    public static void main(String[] args) {
        //当出现异常的运算条件时候,抛出异常,例如一个整数 除以0 时候,抛出此类的一个实例
        int num1 = 10;
        int num2 = 0;
        int res = num1 / num2 ;

    }
}
