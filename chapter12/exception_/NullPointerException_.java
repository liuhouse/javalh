package javalh.chapter12.exception_;

/**
 * @author 刘皓
 * @version 1.0
 * 空指针异常
 */
public class NullPointerException_ {
    public static void main(String[] args) {
        //当应用程序试图在需要对象的地方使用null的时候,抛出该异常
        String name = null;
        System.out.println(name.length());
    }
}


