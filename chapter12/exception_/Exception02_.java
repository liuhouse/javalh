package javalh.chapter12.exception_;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Exception02_ {
    public static void main(String[] args) {
        /*
        * 编译异常是指在编译期间,就必须处理的异常,否则代码不能通过编译
        * 常见的编译异常
        * SQLException//操作数据库时,查询表可能发生的异常
        * IOException//操作文件的时候,发生的异常
        * FileNotFoundException//当操作一个不存在的文件的时候,发生的异常
        * EOFException//操作文件,到达文件末尾,发生异常
        * ILLgalArguementException//参数异常
        *
        * */

        try {
            FileInputStream fis;
            fis = new FileInputStream("C:\\Users\\Administrator\\eclipse-workspace-java\\javalh\\image\\break.png1");
            int len;
            while((len = fis.read()) != -1){
                System.out.println(len);
            }
            fis.close();
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println(e.getMessage());
        }

    }
}
