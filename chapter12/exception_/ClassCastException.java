package javalh.chapter12.exception_;

/**
 * @author 刘皓
 * @version 1.0
 * 类型转换异常
 */
public class ClassCastException {
    public static void main(String[] args) {
        /*
        * 当试图将对象强制转化为不是实例的子类的时候,抛出该异常,例如，下面的代码将生成一个classCastException
        * */
        A b = new B();//向上转型
        B b2 = (B)b;//向下转型
        C c2 = (C)b;//抛出异常 ClassCastException   类型转换异常


    }
}


class A{}
class B extends A{}
class C extends A{}
