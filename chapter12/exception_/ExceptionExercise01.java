package javalh.chapter12.exception_;

import java.util.Date;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ExceptionExercise01 {
    int x;//默认0
    public static void main(String[] args) {
        /*
        * 异常课堂练习
        * */
        //看看下面的代码是不是正常,为什么

        /*
        String friends[] = {"tom","jack","milan"};
        for(int i = 0 ; i < 4 ; i++){
            System.out.println(friends[i]);
        }
        */
        //出现了ArrayIndexOutOfBoundsException

        /*
        Cat cat = new Cat();
        cat = null;
        //NullPointerException
        System.out.println(cat.name);
         */

        /*
        int y;//默认也是0
        ExceptionExercise01 exceptionExercise01 = new ExceptionExercise01();
        y = 3 / exceptionExercise01.x;
        //这边除数为0
        //ArithmeticException  计算异常    by zero
        System.out.println("progran ends ok!");

         */


    }
}


class Cat{
    public String name = "大威天龙";
}



class Person{
    public static void main(String[] args) {
        Object date = new Date();
        Person person;
        person = (Person) date;
        //ClassCastException
        System.out.println(person);
    }
}