package javalh.chapter12.exception_;

/**
 * @author 刘皓
 * @version 1.0
 * 数组下标越界异常
 */
public class ArrayIndexOutOfBoundsException_ {
    public static void main(String[] args) {
        /*
        * 使用非法索引数组时抛出异常,如果索引为负或大于等于数组的长度大小,则该索引为非法索引
        * */

        int[] arr = {1,2,4};
        for(int i = 0 ; i <= arr.length ; i++){
            System.out.println(arr[i]);
        }

    }
}
