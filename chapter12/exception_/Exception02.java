package javalh.chapter12.exception_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Exception02 {
    public static void main(String[] args) {
        /*
        *   1：异常分为两大类,运行异常和编译时异常
        *   2：运行时候异常,编译器检查不出来,一般指的是编程时候出现的逻辑错误,是程序员应该避免其出现的异常,java.lang.RuntimeException类以及他的子类运行时候的异常
        *   3：对于运行时候的异常,可以不做处理,因为这类异常很常见,如果全部都处理的话可能会对程序的可读性和运行效率产生影响
        *   4：编译时候的异常,是编译器要求必须处理的异常
        *
        *
        * 常见异常
        * Throwable
        *   Error: - 会导致程序崩溃,不会继续向下执行 extends Throwable
        *       1：StackOverFlowError    堆栈溢出错误
        *       2：OutOfMemoryError      内存不足异常
        *   Exception:可以捕捉的异常,捕捉之后程序会继续向下执行 extends Throwable
        *       1：RuntimeException      运行时异常
        *           --- NumberFormatException   数字格式化异常
        *           --- ArrayIndexOutOfBoundsException  数组越界异常
        *           --- ArithmeticException 算术异常
        *           --- NullPointerException 空指针异常
        *           --- ClassCastException 类异常
        *       2：RuntimeException      未找到类异常
        *       3：FileNotFoundException 文件没有找到异常
        *
        * */
    }
}
