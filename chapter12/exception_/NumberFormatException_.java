package javalh.chapter12.exception_;

/**
 * @author 刘皓
 * @version 1.0
 * 数字格式化异常
 */
public class NumberFormatException_ {
    public static void main(String[] args) {
        String name = "韩顺平教育";
        //将字符串转化为整数
        int num = Integer.parseInt(name);
        System.out.println(num);
    }
}
