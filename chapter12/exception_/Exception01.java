package javalh.chapter12.exception_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Exception01 {
    public static void main(String[] args) {
        /*
        * 需求：运行下面的代码,看看有什么问题--》 引出异常和异常处理机制
        * 我们可以看到报错了  ArithmeticException : /by zero
        * 一旦发现报错了,程序就不会继续向下执行了
        *
        * 老韩解读：
        * 1：num1 / num2 => 10 / 0
        * 2:当执行到num1/num2的时候,因为num2=0,程序就会出现(抛出)异常 ArithmeticException
        * 3:当抛出异常后,程序就会退出,也就是崩溃了,下面的代码就不会再继续向下执行了
        * 4:大家想想这样的程序好吗?这不好,不应该出现了一个不算致命的问题,就导致整个系统崩溃
        * 5:java设计者,提供了一个叫做异常处理机制来解决此问题
        *
        * int res = num1 / num2
        * 如果程序员认为一段代码可能会出现异常/问题,可以使用try-catch异常机制来进行解决
        * 从而保证程序的健壮性
        * 将该代码块->选中->快捷键 ctrl + alt + t ->选中  try-catch
        * 6:如果进行异常处理,那么即使出现了异常,程序还是可以继续向下执行的,这边会将异常打印出来
        * */

        int num1 = 10;
        int num2 = 0;
        try {
            int res = num1 / num2;
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("出现异常的原因:" + e.getMessage());//输出异常信息
        }
        System.out.println("程序继续执行");
    }
}
