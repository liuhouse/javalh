package javalh.chapter12.custom_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class CustomException {
    public static void main(String[] args) {



        int age = 190;
        //要求范围在18-120之间,否则就抛出一个异常
        if(!(age >= 18 && age <= 120)){
            //这里我们可以通过构造器,设置信息
            throw new AgeException("年龄需要在18-120之间");
        }
        System.out.println("年龄范围正确...");
    }
}

//自定义异常
/*
* 老韩解读
* 1：一般情况下,我们自定义异常是继承RuntimeException
* 2:即把自定义异常做成运行时异常的好处是,我们可以使用默认的异常处理机制
* 3:即就是比较方便
* */
class AgeException extends RuntimeException{
    //构造器
    public AgeException(String message) {
        super(message);
    }
}
