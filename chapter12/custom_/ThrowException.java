package javalh.chapter12.custom_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ThrowException {
    public static void main(String[] args) {
        //进入方法A
        //用A方法的finally
        //制造异常


        //进入方法B
        //调用B方法的finally

        //throw 和 throws的区别
        //          意义              位置          后面跟的东西
        //throws   处理异常的一种方式   方法声明处    异常类型
        //throw     收订生成异常对象的关键字    方法体中    异常对象

        try{
            ReturnExceptionDemo.methodA();
        }catch (Exception e){
            System.out.println(e.getMessage());//制造异常
        }
        ReturnExceptionDemo.methodB();

    }
}



class ReturnExceptionDemo{
    //这边执行try catch 程序  就算是遇到异常   也会首先执行完finally代码块的程序  再去处理异常  即先内部 再外部
    static void methodA(){
        try{
            System.out.println("进入方法A");//1
            throw new RuntimeException("制造异常");
        }finally {
            System.out.println("用A方法的finally");//2
        }
    }

    static void methodB(){
        try{
            System.out.println("进入方法B");//4
            return;
        }finally {
            System.out.println("调用B方法的finally");//5
        }
    }
}
