package javalh.chapter12.exercise;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork02 {
    public static void main(String[] args) {
        //1：会发生ArrayIndexOutOfBoundsException异常,因为数组的长度可能不足5个
        //2: 会发生空指针异常 NullPointerException  因为五个元素可能为null
//        args[4] = null;
        if(args[4].equals("john")){
            System.out.println("AA");
        }else{
            System.out.println("BB");
        }
        Object o = args[2];//String -> Object  向上转型 ok
        //因为o是属于字符串向上转型的,所以向下转型的时候只能使用字符串来接收和转化
        Integer i = (Integer) o;//3：会发生ClassCastException  类型转化异常
    }
}
