package javalh.chapter12.exercise;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork04 {
    public static void main(String[] args) {
        //其实就是一句话
        //如果在此方法调用外部的方法  外部的方法中有使用try{}finally{} 结构   抛出异常  在子类没有处理的话  ， 会先执行完finally，
        //然后再到调用他的位置去catch这个错误    就是先内部   再外部    如果仅仅是在一个代码块中   执行顺序   try - catch - finally
        //这边对showExce()方法抛出的异常选择了自己处理的方式
        try {
            showExce();
            System.out.println("A");
        }catch (Exception e){
            System.out.println("B");//1
        }finally {
            System.out.println("C");//2

        }
        System.out.println("D");//3
    }

    public static void showExce() throws Exception{
        throw new Exception();
    }
}

