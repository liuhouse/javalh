package javalh.chapter12.exercise;

/**
 * @author 刘皓
 * @version 1.0
 */
public class EcmDef_ {
    public static void main(String[] args) {
        /*
        * 需求：
        * a:编写应用程序EcmDef.java，接收命令行的两个参数(整数),计算两数相除
        * b:计算两个数相除,要求使用方法cal(int n1 , int n2)
        * c:对数据格式不正确(NumberformatException),缺少命令行参数(ArrayIndexOutOfBoundsException),除0进行异常处理(AirthmeticException)
        * */

        try {
            //如果当前输入的参数不等于2的话，就抛出异常
            if(args.length != 2){
                throw new ArrayIndexOutOfBoundsException("参数必须是两位");
            }
            //将数据类型在进行转化的时候,可能会抛出异常
            int n1 = Integer.parseInt(args[0]);
            int n2 = Integer.parseInt(args[1]);

            //参数个数正确并且参数类型正确的话
            //就进行计算
            double res = cal(n1,n2);
            System.out.println(res);
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println(e.getMessage());
        }catch (NumberFormatException e){
            System.out.println(e.getMessage());
        }catch (ArithmeticException e){
            System.out.println(e.getMessage());
        }

    }

    public static double cal(int n1 , int n2){
        return n1 / n2;
    }

}


