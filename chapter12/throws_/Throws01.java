package javalh.chapter12.throws_;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Throws01 {
    public static void main(String[] args) throws Exception{
        /*
        * (1)如果一个方法(中的语句执行的时候),可能产生某种异常,但是并不能确定如何处理这些异常,则此方法应该显示的声明抛出异常
        *   表明该方法将不对这些异常进行处理,而是由该方法的调用者负责处理
        * (2)在方法声明中使用throws语句可以声明的抛出异常的列表,throws后面的异常类型可以是方法中产生出来的异常类型,也可以是他的父类
        * */
         f1();

    }

    public static void f1() throws Exception{
//        try {
//            f2();
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
        f2();

    }

    //如果f2()方法不想进行处理的话  需要显示的使用throws抛出异常  可以抛出具体的异常  可以是多个  比如下面的形式
    //可以直接抛出Exception    记住使用throws进行抛出异常的话   被调用的方法的异常 要么相同  要么是子类
    public static void f2() throws NumberFormatException,NullPointerException,ClassCastException{
        int num1 = 10;
        int num2 = 0;
        int res = num1 / num2;
        System.out.println(res);
    }
}
