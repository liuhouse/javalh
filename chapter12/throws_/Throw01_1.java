package javalh.chapter12.throws_;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @author ���
 * @version 1.0
 */
public class Throw01_1 {
    public static void main(String[] args) throws FileNotFoundException{
        readFile("file.txt");
    }

    public static void readFile(String file) throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(file);
    }
}
