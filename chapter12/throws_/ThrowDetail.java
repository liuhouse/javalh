package javalh.chapter12.throws_;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @author 刘皓
 * @version 1.0
 * 注意事项和使用细节
 */
public class ThrowDetail {
    public static void main(String[] args) throws Exception{
//        f2();
//        f1();
//        f4();
        new Son().method();
    }


    public static void f2(){
        //1：对于编译异常,程序中必须处理,比如try-catch 或者  throws
        //2：对于运行时候的异常,程序中如果没有处理,默认就是throws的方式处理
        int n1 = 10;
        int n2 = 1;
        double res = n1 / n2;
    }

    public static void f1() throws FileNotFoundException{
        /*
        * 这里大家思考问题  调用f3()报错
        * 老韩解读
        *   1:因为f3()方法抛出的是一个编译异常
        *   2:即这个时候,就要求f1()必须处理这个异常
        *   3:在f1()中,要么try-catch-finally，或者继续throws这个编译异常
        * */
        f3();//抛出异常
    }

    //这边是编译错误,所以必须处理
    public static void f3() throws FileNotFoundException {
        FileInputStream fis = new FileInputStream("d://aa.txt");
    }


    public static void f4() throws ArithmeticException{
        /*
        * 老韩解读
        * 1：在f4()中调用方法f5() 是ok的
        * 2：原因是f5()抛出的是运行异常
        * 3：而在java中,并不要求程序员显示处理，因为有默认处理机制
        * */
        f5();
    }

    //运行异常
    public static void f5() throws ArithmeticException{
        int n1 = 10;
        int n2 = 0;
        int res = n1 / n2;
    }

}



class Father{//父类
    public void method() throws Exception{

    }
}


//子类
class Son extends Father{
    @Override
    public void method() throws ArithmeticException{

        /*
        * 3：子类重写父类的方法的时候,对抛出异常的规定：子类重写父类的方法
        *   所抛出的异常类型,要么和父类的抛出的异常一致,要么为父类抛出异常类型的子类型
        *
        * 4：在throws过程中,如果有方法try-catch-finally,就相当于是异常处理,就可以不用throws
        * */

        int n1 = 10;
        int n2 = 0;
        int res = n1 / n2;
        System.out.println("子类重写了父类的方法...");
    }
}
