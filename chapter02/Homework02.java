package chapter02;

//第二个作业
//将个人的基本信息(姓名,性别,籍贯,住址) 打印到控制台输出,各条信息各占一行
public class Homework02 {
    public static void main(String[] args){
        System.out.println("姓名\t性别\t籍贯\t住址\n刘皓\t男\t咸阳\t雁塔");
    }
}