package chapter05;
import java.util.Scanner;

public class DoWhileExercise04 {
	public static void main(String[] args) {
		//需求,如果李三不还钱,则老韩将一直使用闪电五连鞭
		//[System.out.println("老韩问:还钱吗?y/n")] do....while
		
		//化繁为简
		//(1)不停的问,还钱吗?
		//(2)使用char answer 接收回答,定义一个scanner对象
		//(3)在do...while 的 while 判断如果是y就不在进行循环
		Scanner myScanner = new Scanner(System.in);
		char answer = 'n';
		do {
			System.out.println("老韩使出了闪电五连鞭！问张三还钱吗?[y/n]");
			answer = myScanner.next().charAt(0);
			System.out.println("他的回答是" + answer);
		}while(answer != 'y');
		System.out.println("还钱了,撤退");
	}
}
