package chapter05;
import java.util.Scanner;
public class SwitchExercise2_1 {
	public static void main(String[] args) {
		//important 最重要的是思路,不一定要按照老师的来做,拿到一个需求,只要你能拿你所学的知识完成需求,达到目的,那么你就是成功的
		//这是自己思路,学到老师的思路那更好    不要局限自己,学到的东西能结合使用 完成需求  最好
		//其实完成一道题的解题思路有很多,能找到更多的方法是能力提升的一种表现
		/**
		 * 	需求：对学生成绩大于 60 分的，输出"合格"。低于 60 分的，输出"不合格"。(注：输入的成绩不能大于 100), 提示 成绩/60
		 * 	
		 *	思路分析：
		 *	
		 *	1：实例化Scanner，定义变量 double score  
		 *	2:使用switch语句进行判断
		 *	
		 *	特别说明：这里是牵扯到算法的   ---  int(成绩/60) = 1 代表当前成绩[60-100] 合格     |       int(成绩/60) = 0 [0-60)不合格
		 * */
		//代码实现
		System.out.println("请输入你的分数...");
		Scanner scoreScanner = new Scanner(System.in);
		//因为分数有可能是小数,所以要使用double类型来接收
		double double_f = scoreScanner.nextDouble();
		//因为switch不能使用double或者float 使用强制转换
		int score = (int)(double_f /60);
		if(double_f <= 100 && double_f >= 0) {
			switch(score) {
				case 0:
					System.out.println("不合格");
					break;
				case 1:
					System.out.println("合格");
					break;
			}
		}else {
			System.out.println("成绩必须在1-100之间");
		}
		
		
		
		
		
		
		
		
	}
}
