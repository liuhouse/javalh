package chapter05;

public class HomeWork03 {
	public static void main(String[] args) {
		/*
		 * 	需求  判断一个年份是否为闰年
		 * 	思路分析
		 * 	年份能被 4 整除，但不能被 100 整除；(2)能被 400 整除
		 * 	(1):定义一个年份, 根据公式判断  符合就是闰年
		 * 	if - else  逻辑运算符  ||
		 * */
		int year = 2021;
		String year_name = "";
		if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
			year_name = "闰年";
		}else {
			year_name = "平年";
		}
		System.out.println(year + "年是" + year_name);
	}
	
}
