package chapter05;
//细节案例分析和说明
public class ContinueDetail {
	public static void main(String[] args) {
		label1://第一次循环的标签
			for(int j = 0 ; j < 2 ; j++) {
				label2://第二次循环的标签
					for(int i = 0; i < 3 ; i++) {
						if(i == 2) {
							//continue label2;//后面不写标签名称，相当于 continue label2
							continue label1;
						}
						System.out.println("i=" + i);
					}
			}
	}
}
