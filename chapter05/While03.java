package chapter05;

public class While03 {
	public static void main(String[] args) {
		/*
		 * 	需求 - 打印出 40 -200 之间所有的偶数
		 * 	思路分析 【由繁化简】 【先死后活】
		 * 	(1) 先打印出 40 -200 之间所有的数
		 * 	(2)进行筛选,打印出 i % 2 == 0 的数【能被2整除的都是偶数】
		 * */
		int i = 40;
		while(i <= 200) {
			//所有能被2整除的数就是偶数
			if(i % 2 == 0) {
				System.out.println(i);
			}
			i++;
		}
	}
}
