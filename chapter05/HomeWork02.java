package chapter05;
import java.util.Scanner;

public class HomeWork02 {
	public static void main(String[] args) {
		/*
		 * 	需求：实现一个整数,属于哪个范围,大于0；小于0；等于0 
		 * 	思路分析
		 * 	(1)实例化Scanner  定义一个整数  int num = 20  使用Scanner进行保存 
		 * 	(2)使用if else-if else   进行数据的判断
		 * */
		Scanner myScanner = new Scanner(System.in);
		System.out.println("请输入一个整数");
		
		int num = myScanner.nextInt();
		
		if(num > 0) {
			System.out.println("大于0");
		}else if(num < 0) {
			System.out.println("小于0");
		}else {
			System.out.println("等于0");
		}
		
	}
}
