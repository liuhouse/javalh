package chapter05;

public class BreakDetail {
	public static void main(String[] args) {
		/*
		 * 	需求
		 * 1-100以内的数求和,当第一次大于20的当前数 (for + break)
		 * 	思路分析
		 * (1):打印出1-100之间的数
		 * (2):定义变量 sum   进行叠加
		 * (3):进行判断，如果当前的sum大于20 使用break进行跳出操作 并打印出当前的循环数
		 * */
		int sum = 0;
		int count = 0;
		for(int i = 1; i <= 100; i++) {
			sum += i;
			if(sum >= 20) {
				//这是在里面打印的情况
				System.out.println("已经找到sum大于20的数了，是在第" + i + "次找到的");
				//如果符合条件了，就把当前的记录数给 count
				count = i;
				break;
			}
			
		}
		
		//如果想在外面进行打印呢
		System.out.println("我是在外部打印的" + count);
		
		
	}
}
