package chapter05;

public class ManyFor01 {
	public static void main(String[] args) {
		/**
		 * 	1:将一个循环放在另外一个循环体内,就形成了嵌套循环,其中,for,while,do...while 都可以作为外层循环和内层循环
		 * 	【建议一般使用两层,最多不要超过三层，否则,代码的可读性很差】
		 * 	2:实际上,嵌套循环就是把内层循环当成外层循环的循环体，当内层循环的循环条件为fasle的时候,才完全跳出内层循环,才可以结束外层的当次循环
		 * 	开始下一次的循环
		 * 	3：设外层的循环次数为m次,内层为n次,则内层循环体的实际上需要执行m*n次
		 * */
		for(int i = 1 ; i <= 7 ; i++) {//第一层循环 7
			for(int j = 1 ; j <= 2 ; j++) {//第二层循环2
				System.out.println("ok~~");
			}
		}
	}
}
