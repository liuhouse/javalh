package chapter05;

public class ForExercise02 {
	public static void main(String[] args) {
		/*
		 * 	需求 完成下面表达式的输出
		 * 	0 + 5 = 5
		 * 	1 + 4 = 5
		 * 	2 + 3 = 5
		 * 	3 + 2 = 5
		 * 	4 + 1 = 5
		 * 	5 + 0 = 5
		 * 
		 * 	化繁为简   先死后活
		 * 
		 * 	思路分析 
		 * 	(1) 先输出 0 - 5
		 * 	(2) 构建 + 后面的值
		 * 	(3) 使用先死后活的思想将5使用变量替代
		 * 	
		 * 
		 * */
		
//		int n = 10;
//		for(int i = 0 ; i <= n ; i++) {
//			System.out.println(i + " + " + (n - i) + "= " + n);
//		}
		
		
		/*
		 * 	换一种方式进行处理
		 * (1)先输出0-5
		 * (2)再输出5-0
		 * (3)构建表达式
		 * */
		int i = 0;
		int j = 20;
		int n = 20;
		for(;i<=n;) {
			System.out.println(i + "+" + j + " = " + n);
			i++;
			j--;
		}
		
		
	}
}
