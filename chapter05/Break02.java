package chapter05;

public class Break02 {
	//编写一个main方法
	public static void main(String[] args) {
		//测试代码
		for(int i = 0 ; i < 10; i++) {
			//如果当前的i等于3的时候,就终止循环
			if(i == 3) {
				break;
			}
			System.out.println("i= " + i);
		}
	}
}
