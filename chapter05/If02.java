package chapter05;
import java.util.Scanner;
/*
 * 演示双分支
 * 如果条件表达式为true，执行代码块1,否则执行代码块2，如果只有一条语句,则{}可以省略
 * */
public class If02 {
	public static void main(String[] args) {
		//案例 - 编写一个程序,可以输入人的年龄,如果年龄大于18对，则输出“你的年龄大于18，要对自己的行为负责”
		//否则,输出 “你的年龄还小这次就放过你了”
		
		//思路分析
		//1:接收人的年龄,使用Scanner扫描器
		//2：保存人的年龄  int age
		//3:使用双分支 if -else 进行判断,输出对应的结果
		
		System.out.println("请输入你的年龄...");
		//实例化Scanner对象
		Scanner myScanner = new Scanner(System.in);
		//使用变量进行保存
		int age = myScanner.nextInt();
		if(age > 18) {
			System.out.println("你已经大于18岁了，要对自己的行为负责...");
		}else {
			System.out.println("你的年龄还小,这次就放过你了");
		}
		
		System.out.println("程序仍然还在进行中.....");
		
	}
	
}
