package chapter05;

public class HomeWork08 {
	public static void main(String[] args) {
		/*
		 * 	需求 
		 * 	求出1-1/2 + 1/3 - 1 /4 + 1/5 - 1/6 ... 1/100的和
		 * 	思路分析
		 * 	(1)根据需求 推出公式  1 - 1/(2) + 1/(3) - 1/(4) ... 1/(100)的和
		 * 	(2)如果被除数能被2整除的话，前面就使用 减号   否则就使用加号  [if -else]
		 * 	 
		 * */
		
		int m = 1;
		for(int i = 2 ; i <= 100 ; i++){
			//如果当前是偶数,就使用减号
			if(i % 2 == 0) {
				m -= 1 / i;
			}else {
				m += 1 / i;
			}
		}
		System.out.println(m);
		
	}
}
