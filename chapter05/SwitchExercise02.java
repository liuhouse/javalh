package chapter05;
import java.util.Scanner;
public class SwitchExercise02 {
	public static void main(String[] args) {
		/**
		 * 	需求：对学生成绩大于 60 分的，输出"合格"。低于 60 分的，输出"不合格"。(注：输入的成绩不能大于 100), 提示 成绩/60
		 * 	
		 *	思路分析：
		 *	
		 *	1：实例化Scanner，定义变量 double score
		 *	2:使用switch语句进行判断
		 * */
		//代码实现
		System.out.println("请输入你的分数...");
		Scanner scoreScanner = new Scanner(System.in);
		//因为分数有可能是小数,所以要使用double类型来接收
		double double_f = scoreScanner.nextDouble();
		//因为switch不能使用double或者float 使用强制转换
		int score = (int)double_f;
		int my_score;
		//不管你使用那種方式,能得到最終的結果就可以
		if(score >= 60 && score <= 100) {
			 my_score = 1;
		}else if(score < 60) {
			 my_score = 2;
		}else {
			 my_score = 0;
		}
		
		switch(my_score) {
			case 1:
				System.out.println("合格");
				break;
			case 2:
				System.out.println("不合格");
				break;
			default:
				System.out.println("输入的数不能大于100");	
		}
		
	}
}
