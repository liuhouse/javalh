package chapter05;
import java.util.Scanner;
//switch语句 课堂练习
public class SwitchExercise {
	public static void main(String[] args) {
		//作业1
		/*
		 * 	使用 switch 把小写类型的 char 型转为大写(键盘输入)。只转换 a, b, c, d, e. 其它的输出 "other"。
		 * 
		 * 	思路分析
		 * 	1:	实例化Scanner对象，定义变量 char inp 保存输入的值
		 * 	2:	使用switch语句进行转换,default -> other
		 * */
		System.out.println("请输入您要转换的字符[a-e]");
		Scanner inpScanner = new Scanner(System.in);
		//获取到输入的字符
		char inp = inpScanner.next().charAt(0);
		switch(inp) {
			case 'a':
				System.out.println('A');
				break;
			case 'b':
				System.out.println('B');
				break;
			case 'c':
				System.out.println('C');
				break;
			case 'd':
				System.out.println('D');
				break;
			case 'e':
				System.out.println('E');
				break;
			default:
				System.out.println("Other");
		}
	}
}
