package chapter05;
import java.util.Scanner;

public class BreakExercise03 {
	public static void main(String[] args) {
		//实现登录验证，有 3 次机会，如果用户名为"丁真" ,密码"666"提示登录成功，否则提示还有几次机会，请使用 for+break
		/*
		 *	 思路分析
		 *	(1):创建Scanner对象,用作输入
		 *	(2):循环三次,每循环一次,递减循环,可以做为剩余次数
		 *	(3):定义变量 String username ; 定义变量 String pasword 
		 *	(4):进行用户名和密码的判断,当用户名和密码全部正确的时候使用break跳出循环,打印登录成功
		 *		否则,打印出剩余此次
		 *		提醒,字符串的比较使用equals方法   字符串最好写在前面
		 * */
		Scanner myScanner = new Scanner(System.in);
		String username = "";
		String password = "";
		for(int i = 3 ; i >= 1 ; i--) {
			System.out.println("请输入用户名:");
			username = myScanner.next();
			System.out.println("请输入密码:");
			password = myScanner.next();
			if("丁真".equals(username) && "666".equals(password)) {
				System.out.println("登录成功~");
				//登录成功,跳出循环
				break;
			}
			System.out.println("您还有" + (i-1) + "次机会");
		}
		
	}
}
