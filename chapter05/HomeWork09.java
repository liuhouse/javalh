package chapter05;

public class HomeWork09 {
	public static void main(String[] args) {
		/*
		 * 	需求 
		 * 	求 1 + (1+2) + (1+2+3) + (1+2+3+4) + ... + (1+2+3+4+...100)的结果
		 * 	思路分析
		 * 	(1) 先打印出 1 - 100
		 * 	(2) 再打印出  1   1,2  1,2,3   1,2,3,4   1,2,3,4,5....100
		 * 	(3) 在最外层定义一个num ,用作记录总数 相加
		 * */
		int sum = 0;
		for(int i = 1 ; i <=100 ; i++) {
			for(int j = 1 ; j <= i; j++) {
//				System.out.print(j + " ");
				sum += j;
			}
			
		}
		System.out.println(sum);
		
	}
}
