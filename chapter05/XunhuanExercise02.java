package chapter05;

//打印出九九乘法表
public class XunhuanExercise02 {
	public static void main(String[] args) {
		for(int i = 1 ;  i <=9 ;i++) {//9代表的是打印9行
			for(int j = 1 ; j <= i ; j++) {
				System.out.print(j + "*" + i + "=" + (i * j) + " \t");
			}
			//一行打印完成之后换行
			System.out.println("\r\n");
		}
	}
}
