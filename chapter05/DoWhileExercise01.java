package chapter05;

/**
 * 	do....while 循环练习题
 * */
public class DoWhileExercise01 {
	public static void main(String[] args) {
		
		 //需求1 打印出[1-100]
		 
			/*
				int i = 1;
				do {
					System.out.println(i);
					i++;
				}while(i<=100);
			*/
		
		
		//需求2 计算 1-100的和
		int sum = 0;
		int j = 1;
		do {
			System.out.println("sum = " + sum + "+" + j);
			sum += j;//sum = 0 + 1 = 1     sum = 1 + 2 = 3       sum = 3 + 3 = 6
			j++; //j = 2  j = 3  
		}while(j <= 100);
		System.out.println(sum);
		
		
		
		
	}
}
