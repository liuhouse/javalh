package chapter05;
import java.util.Scanner;
//单分支的案例
public class If01 {
	public static void main(String[] args) {
		//需求：编写一个程序输入人的年龄,如果该同志的年龄大于18岁,那么就输出,你犯错了,因为你的年龄超过18对了,所以送你去监狱
		
		//我自己的思路分析
		/*
		 * 	需求分析
		 * 	1：输入人的年龄 使用  Scanner扫描器
		 * 	2：保存人的年龄  int 类型保存
		 * 	3：编写 流程控制做判断,打印出对应的结果
		 * */
		
		
		
		//老韩的思路分析
		//1：接收输入的年龄,应该定义一个Scanner对象
		//2:把年龄保存到一个变量  int age
		//3:使用if判断,输出对应的信息
		
		
		
		System.out.println("请输入你的年龄?");
		//实例化Scanner类
		Scanner myScanner = new Scanner(System.in);
		//保存年龄
		int age = myScanner.nextInt();
		//进行流程控制的判断
		if(age > 18) {
			System.out.println("你犯错了,因为年龄超过了18岁,所以送你去监狱...");
		}
		System.out.println("程序仍然在执行.....");
	}
}
