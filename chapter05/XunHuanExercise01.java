package chapter05;
/*
 * 打印出九九乘法表
 * */
public class XunHuanExercise01 {
	public static void main(String[] args) {
		/*
		 * 	需求 打印出九九乘法表
		 * 	思路分析
		 * 	(1)先打印出9个1
		 * */
		for(int i = 1 ; i <= 9 ; i++) {
			for(int j = 1 ;  j <= i ; j++) {
				System.out.print(j+" * "+i+ " = " + (i * j) + "\t");
			}
			System.out.println("\r\n");
		}
	}
}
