package chapter05;

public class While02 {
	public static void main(String[] args) {
		//循环条件是返回一个布尔值的表达式
		//while循环是先判断后执行
		//课堂练习
		//1打印 1 -100 之间所有能被3整除的数【使用while】
		/*
		 * 	思路分析   【先繁后简 先死后活】
		 * 	(1) 先打印出1-100之间所有的数
		 * 	(2)进行筛选,得到 i%3==0的数
		 * */
		int n = 200;
		int i = 1;
		int c = 3;
		while(i <= n) {
			//进行筛选
			if(i % c == 0) {
				System.out.println(i);
			}
			i++;
		}
		
	}
}
