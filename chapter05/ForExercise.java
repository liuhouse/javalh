package chapter05;

public class ForExercise {
	public static void main(String[] args) {
		/*
		 * 	需求 - 打印 1~100 之间所有是 9 的倍数的整数，统计个数 及 总和.[化繁为简,先死后活]
		 * 	自己的思路分析
		 * 	1)先打印出1-100之间的所有整数
		 * 	2)打印出 当前数 % 9 = 0 的整数
		 * 	3)定义个数变量   int count = 0 每次循环+1
		 * 	4)定义总数变量  int sum = 0   每次循环把当前的数进行相加
		 * */
		//初始化个数
		int count = 0;
		//初始化总和
		int sum = 0;
		System.out.println("1-100中是9的倍数的数据有");
		for(int i = 1 ; i <= 100 ; i++) {
			//(1)打印出1-100之间的所有整数
			//System.out.println(i);
			
			//打印出所有9的倍数的整数
			if(i % 9 == 0) {
				System.out.print(i + "-");
				//每次找到符合条件的,就给count+1
				count ++;
				//每次找到符合条件的,就个sum+当前的数
				sum+=i;
			}
			
		}
		System.out.println();
		System.out.println("总共有" + count + "个数据");
		System.out.println("总数为" + sum);
		
	}
}
