package chapter05;
import java.util.Scanner;

public class NestedIf {
	public static void main(String[] args) {
		/*
		 * 	需求 - 参加歌手比赛,如果初赛的成绩大于8.0进入决赛,否则提示淘汰,并且根据性别提示进入男子组或者女子组
		 * 	输入成绩和性别,进行判断和输出信息
		 * 
		 *	思路分析
		 *	1)	创建Scanner对象,接收用户的输入
		 *	2)	接收  将成绩保存到 double score
		 *	3)	假定成绩在0.0 - 10.0 之间，如果错误,就提示错误
		 *	4)	使用if-else 判断，如果初赛成绩大于8.0进入决赛,否则提示淘汰
		 *	5)	如果进入到决赛,再接收 char gender,使用if-else输出信息
		 * */
		
		//进行扫描器对象的实例化
		Scanner myScanner = new Scanner(System.in);
		System.out.println("请输入你的初赛成绩....");
		double score = myScanner.nextDouble();
		
		
		//假定分数在0.0 - 10.0之间 
		if(score >= 0.0 && score <= 10.0) {
			if(score > 8.0) {
				System.out.println("请输入你的性别...");
				char gender = myScanner.next().charAt(0);
				//当成绩合格的时候才判断性别
				if(gender == '男') {
					System.out.println("成绩合格-分到男子组~");
				}else if(gender == '女') {
					System.out.println("成绩合格-分到女子组");
				}else {
					System.out.println("性别输入有误，请检查....");
				}
			}else {
				System.out.println("成绩不合格,被淘汰了");
			}
		}else {
			System.out.println("分数必须在0.0 ~ 10.0之间");
		}
		
	}
}
