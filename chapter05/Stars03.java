package chapter05;
/*
 * 打印空心菱形
 * 
 * */
public class Stars03 {
	public static void main(String[] args) {
		//定义总层数
		int total_ceng = 29;
		//计算中间的层
		int middle_ceng = (total_ceng / 2) + 1;
		//上半部分空格使用
		int reduce_space = (total_ceng / 2) + 1;
		//下半部分空格使用
		int space_down = 1;
		//下半部分数据使用
		int reduce_down = (total_ceng / 2);
		for(int i = 1 ; i <= total_ceng; i++) {
			//上半部分,包括中间层
			if(i <= middle_ceng) {
				//进行空格填充[中间层之上这样填充]
				//填充空格的个数   4 3 2 1 0
				for(int k = 1; k <= reduce_space-1;k++) {
					System.out.print(" ");
				}
				reduce_space--;
				
				for(int j = 1 ;  j <=2*i-1 ; j++) {
					//如果当前的位置是第一个或者最后一个填充 *
					if(j == 1 || j == 2*i-1) {
						System.out.print("*"); 
					}else {
						System.out.print(" ");
					}
					
				}
			}else {
				//打印空格 1,2,3,4
				for(int ds = 1 ; ds <= space_down ; ds ++) {
					System.out.print(" ");
				}
				//每次打完之后进行+1   直到四次的循环结束
				space_down++;
				//剩余层,目前还剩四层   7 5 3 1
				//数据方式应该是    2*4-1=7   2*3-1=5    2*2-1 = 3  2*1-1 = 1
				for(int x = 1 ; x <= 2*(reduce_down)-1;x++) {
					//进行空心操作
					//当前的位置等于第一个或者最后一个的时候再填充*   否则 其他的都填充空格
					if(x == 1 || x == 2*(reduce_down)-1) {
						System.out.print("*");
					}else {
						System.out.print(" ");
					}
					
				}
				reduce_down--;
			}
			
			System.out.println(" ");
		}
	}
}
