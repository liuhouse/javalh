package chapter05;

public class SwitchDetail {
	public static void main(String[] args) {
		/*
		 * 	1：表达式的数据类型,应和case后的常量类型一致,或者是可以自动转成可以相互比较的类型,比如输入的是字符,而常量是Int
		 * 	2：switch(表达式)中的表达式的返回值必须是:(byte,short,int,char,enum[枚举],String)
		 * 	3：case字句中的值必须是常量,而不能是变量
		 * 	4：default字句是可选的,当没有匹配的case的时候,执行default
		 * 	5：break语句用来在执行一个case分支后使得程序跳出switch语句块,如果没有写break，程序会顺序执行到switch结尾,除非遇到break
		 * */
		
		//double c = 1.1;//double不能作为表达式的返回值
		//float c = 1.1f;//float也不能作为表达式的返回值
		//long c = 10;//long也不能作为表达式的返回值
		String c = "c";
		String c_tmp = "";
		switch(c) {
			case "c":
				System.out.println('a');
				//break;
			case "a":
				System.out.println("20");
				break;
			default:
				System.out.println("一个都没有匹配到");
		}
	}
}
