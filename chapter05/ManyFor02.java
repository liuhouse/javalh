package chapter05;
/*
 * 多重循环执行步骤分析
 *	总结一个很重要的编程思想   不论是什么业务   思想 逻辑  一点   化繁为简   先死后活   先分析把最简单的做出来   然后再根据业务补充细节    也就是先搭框架  再进行细节填充
 *	因为直接写肯定是写不出来的  循序渐进
 * */
public class ManyFor02 {
	public static void main(String[] args) {
		//请分析 下面的多重循环执行步骤,并写出输出 =》 韩老师的内存分析法
		//双层for
		
		for(int i = 0; i < 2; i++) {//第一层循环
			
			for(int j = 0 ; j < 3; j++) {
				System.out.println("i= " + i + " j= " + j);
			}
			
		}
	}
}
