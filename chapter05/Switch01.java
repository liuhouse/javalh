package chapter05;
import java.util.*;

public class Switch01 {
	public static void main(String[] args) {
		
		/*
		 * 	知识点解读
		 * 	1：switch关键字,表示switch分支
		 * 	2：表达式,对应一个值
		 * 	3：case 常量1，当表达式的值等于常量1，就执行语句块1
		 * 	4:break:表示退出switch
		 * 	5:如果和case 常量1 匹配,就执行语句块1,如果没有匹配,就继续匹配case常量2
		 * 	6:如果一个都没有匹配上,就执行default
		 * */
		
		/*
		 * 	需求
		 * 	请编写一个程序，该程序可以接受一个字符，比如,a,b,c,d,e,f,g
		 * 	a表示星期一,b表示星期二..... 一直到周天
		 * 	根据用户输入显示相应的信息,要求使用switch语句完成
		 * 	
		 * 	思路分析
		 * 	1：接收一个字符,创建Scanner对象
		 * 	2：使用switch语句来判断并输出相对应的信息
		 * */
		System.out.println("请输入一个字符...");
		Scanner myScanner = new Scanner(System.in);
		//获取输入的char字符
		char c_str = myScanner.next().charAt(0);
		switch(c_str) {
			case 'a':
				System.out.println("今天星期一,猴子穿新衣");
				break;
			case 'b':
				System.out.println("今天星期二,猴子做小二");
				break;
			case 'c':
				System.out.println("今天星期三,猴子去爬山");
				break;
			case 'd':
				System.out.println("今天星期四,猴子去集市");
				break;
			case 'e':
				System.out.println("今天星期五,猴子去练武");
				break;
			case 'f':
				System.out.println("今天星期六,猴子六六六");
				break;
			case 'g':
				System.out.println("今天星期天,猴子要上天");
				break;
			default:
				System.out.println("输入有误,请重新输入");
		}
		
		System.out.println("switch语句已经执行完毕,程序继续向下执行	");
		
	}
}
