package chapter05;

public class DoWhile01 {
	public static void main(String[] args) {
		/*
		 * 	使用do...while 完成打印10句,你好韩顺平教育
		 * */
		int i = 1;//循环变量初始化
		do {
			System.out.println("你好！韩顺平教育!" + i);//执行代码块
			i++;//进行变量迭代
		}while(i <= 10);//进行条件判断
		System.out.println("退出 do...while 继续执行");
		
		//注意事项和细节说明
		//循环条件是返回一个布尔值的表达式
		//do..while 循环是先执行,再判断，因为他至少执行一次
		
	}
}
