package chapter05;
import java.util.Scanner;

/*
 * do ... while 练习题3
 * */
public class DoWhileExercise03 {
	public static void main(String[] args) {
		/*
		 * 	需求 - 如果李三不还钱,则老韩将一直使用闪电五连鞭,直到李三说还钱为止
		 * 	思路分析
		 * 	化繁为简
		 * 	(1)：先打印出,老韩问,还钱吗?y/n
		 * 	(2):默认声明的是不还钱 【n】 使用Scanner保存张三说的话【y/n】
		 * 	(3):判断当前张三的输入  如果是y的话退出循环   如果是n的话将一直问,一直打
		 * */
		//张三默认不还钱
		int is_through = 0;
		do {
			System.out.println("老韩问,还钱吗【还钱:y / 不还钱 n】?");
			Scanner myScanner = new Scanner(System.in);
			char san_speack = myScanner.next().charAt(0);
			//还钱的情况下,就放过你了
			if(san_speack == 'y') {
				is_through = 1;
			}else {
				is_through = 0;
				System.out.println("老韩使用闪电五连鞭把张三打了一顿。");
			}
		}while(is_through == 0);
		
		System.out.println("你把钱既然已经还了,那么我现在就放过你了.");
	}
}
