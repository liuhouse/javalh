package chapter05;

public class Return01 {
	public static void main(String[] args) {
		//return使用在方法中,表示跳出所在的方法, 如果return写在main方法中,表示退出本程序
		for(int i = 1;i<=5;i++) {
			if(i == 3) {
				System.out.println("韩顺平教育" + i);
				//return;//return 代表的是退出本程序
				//continue;
				break;
			}
			System.out.println("Hello world");
		}
	}
}
