package chapter05;
//Continue快速入门
public class Continue01 {
	public static void main(String[] args) {
		//代码
		int i = 1;
		while(i <= 4) {
			i++;
			//如果i == 2的时候  跳出本次循环,继续后面的循环
			if(i == 2) {
				continue;
			}
			System.out.println("i=" + i);
		}
	}
}
