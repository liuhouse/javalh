package chapter05;
/*
 * break语句的注意事项和细节说明
 * */
public class Break03 {
	public static void main(String[] args) {
		//老韩解读
		//(1):break语句可以指定退出哪层
		//(2):label1是循环的标签,可以由程序员随意指定
		//(3):break后指定到哪个label就退出到哪里
		//(4):在实际开发中,老韩尽量不要使用标签
		//(5):如果没有指定break,默认退出最近的循环体
		
		label1://第一层for循环的标签
			for(int j = 0 ; j < 4 ; j++) {
				label02://第二层for循环的标签
					for(int i = 0 ; i < 10; i++) {
						if(i == 2) {
							break label1;
						}
						System.out.println("i = " + i);
					}
			}
	}
}
