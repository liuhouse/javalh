package chapter05;
import java.lang.Math;
public class Break01 {
	public static void main(String[] args) {
		int i = 1;
		//循环,但是循环的次数不知道,->break.当某个条件满足的时候,终止循环,通过改需求可以说明其他流程控制的必要性 比如break
		//break语句用于终止某个语句块的执行,一般使用在switch或者循环【for,while,do...while】中
		for(;  ;) {
			//Math.random() [0.00-0.99] [0 - 99]  1 - 100  
			int num = (int)(Math.random()*100)+1;
			if(num == 97) {
				System.out.println("找到了"+ 97 +"一共用了" + i + "次");
				break;
			}
			i++;
		}
	}
}
