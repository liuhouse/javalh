package chapter05;
import java.util.Scanner;

public class Piao {
	public static void main(String[] args) {
		/**
		 * 	需求：
		 * 	出票系统：根据淡旺季的月份和年龄,打印票价
		 * 	4-10 旺季：
		 * 		成人 (18 - 60) : 60
		 * 		儿童 ( < 18) : 半价
		 * 		老人 (>60) : 1/3
		 * 	淡季：
		 * 		成人:40
		 * 		其他：20
		 * 
		 * 	思路分析：(1)淡旺季 -> 使用 if-else  (2)在旺季中,有三种情况,使用多分支处理  (3)在淡季中，有两种情况,使用双分支处理
		 * */
		System.out.println("请输入当前的月份...");
		Scanner myScanner = new Scanner(System.in);
		int mouth = myScanner.nextInt();
		
		if(mouth >= 1 && mouth <= 12) {
			System.out.println("请输入您的年龄...");
			int age = myScanner.nextInt();
			//代表现在是旺季
			if(mouth >= 4 && mouth <= 10) {
				//成人
				if(age >= 18 && age <= 60) {
					System.out.println("旺季-您属于成人,所以票价为60元....");
				}else if(age < 18) {//代表是儿童
					System.out.println("旺季-你是儿童,你的票是半价" + (60 /2) + '元');
				}else if(age > 60) {//代表是老年人
					System.out.println("旺季-你是老年人,你的票价是" + (60/3) + "元");
				}else {
					System.out.println("年龄输入有误");
				}
			}else {//代表现在是淡季
				//成人 18 -60
				if(age >= 18 && age <= 60) {
					System.out.println("淡季-您是属于成人,所以您的票价是40元...");
				}else {
					System.out.println("淡季-您属于其他您的票价是20元");
				}
			}
		}else {
			System.out.println("月份在1-12月之间...");
		}
		
		
		
	}
}
