package chapter05;

public class HomeWork07 {
	public static void main(String[] args) {
		/*
		 * 需求 ： 输出小写的a-z以及大写的Z-A
		 * 	思路分析：
		 * 	小写的a-z
		 * (1)得到 a 的asicll码  使用变量 int small_a 接收
		 * (2)得到 z 的asicll码  使用变量 int small_z 接收
		 * (3)使用for循环得到 a - z 之间的ascill码数，将数转化为对应的字符即可
		 * 
		 * 	大写的Z-A
		 * (1)得到Z的ascill码 使用变量 int big_z接收
		 * (2)得到A的ascill码 使用变量 int big_a接收
		 * (3)得到for循环得到 Z - A之间的ascill码,将数字转化成对应的字符即可
		 * */
		int small_a = (int)'a';
		int small_z = (int)'z';
		for(int i = small_a ; i <= small_z; i++) {
			System.out.print((char)i + " ");
		}
		
		System.out.println(" ");
		int big_z = (int)'Z';
		int big_a = (int)'A';
		for(int i = big_z ; i >= big_a ; i--) {
			System.out.print((char)i+" ");
		}
		
		
	}
}
