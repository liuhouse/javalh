package chapter05;
import java.util.Scanner;
/**
 * 	多分支案例演示
 * */
public class If03 {
	public static void main(String[] args) {
		/*
		 * 	输入马保国同志的芝麻信用分:
		 * 	如果：
		 * 	1) 信用分为 100 分时，输出 信用极好；
		 * 	2) 信用分为(80，99]时，输出 信用优秀；
		 * 	3) 信用分为[60,80]时，输出 信用一般；
		 * 	4)其它情况 ，输出 信用 不及格
		 * 	5) 请从键盘输入保国的芝麻信用分，并加以判断
		 * */
		
		System.out.println("请输入保国同志的芝麻信用分[1-100分]....");
		Scanner myScanner = new Scanner(System.in);
		//接收用户的输入
		int zhiMaFen = myScanner.nextInt();
		//先对输入的信用分,进行一个范围的有效判断 1-100，否则提示输入错误，这样使得程序更加的健壮
		if(zhiMaFen >= 1 && zhiMaFen <= 100) {
			//因为马保国的信用分有四种情况,所以使用多分支
			if(zhiMaFen == 100) {
				System.out.println("信用极好...");
			}else if(zhiMaFen > 80 && zhiMaFen <= 99) {
				System.out.println("信用优秀...");
			}else if(zhiMaFen >= 60 && zhiMaFen <= 80) {
				System.out.println("信用一般...");
			}else {
				System.out.println("信用不及格...");
			}
		}else {
			System.out.println("芝麻分必须在1-100之间...");
		}
		
	}
}
