package chapter05;

public class HomeWork04_1 {
	public static void main(String[] args) {
		//需求：判断一个整数是否为水仙花数,所谓的水仙花数指的就是一个3位数,其各个位上数字的立方和等于其本身
		 //例如 ： 153 = 1*1*1 + 3*3*3 + 5*5*5
		/*
		 * 	思路分析
		 * 	(1)	定义一个三位数的整数 int num = 153
		 * 	(2)	获取这个整数的百位
		 * 	(3)	获取这个数的十位
		 * 	(4)	获取这个数的个位
		 * 	(5)	使用if-else进行判断 相加的数等于此数代表就是水仙花数
		 * */
		
		int num = 154;
		//获取百位
		int num1 = num / 100;
		//获取十位
		int num2 = num % 100 / 10;
		//获取个位
		int num3 = num % 10;
		//计算各个数的立方和
		int numadd = num1 * num1 * num1 + num2 * num2 * num2 + num3 * num3 * num3;
		//进行判断
		if(numadd == num) {
			System.out.println(num +"是水仙花数");
		}else {
			System.out.println(num + "不是水仙花数");
		}
		
	}
}
