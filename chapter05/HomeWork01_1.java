package chapter05;

public class HomeWork01_1 {
	public static void main(String[] args) {
		/*
		 * 	需求
		 * 	某人有100000元,每经过一次路口,需要缴费,规则如下
		 * 	(1) 	当现金 > 50000的时候,每次交5%
		 * 	(2)		当现金<=50000的时候,每次交1000
		 * 	问?编程计算该人可以经过多少次路口,要求,使用while + break 方式完成
		 * 
		 * 	思路分析
		 * 	1:初始化变量 int total_money = 100000元
		 * 	2:使用while循环进行操作,在里面进行判断
		 * 		2.1 total_money > 50000 使用公式  total_money *= 0.95; 
		 * 		2.2 total_money <= 50000 使用公式 total_money = total_money - 1000
		 * 		2.3 total_money < 1000 的时候   跳出循环 不能经过路口了
		 * 	3:定义路口的次数,钱数每减少一次,代表经过了一次路口,就给路口的次数+1    当total_money不足1000的时候,代表不能再过路口了,使用break跳出程序
		 * */
		
		double total_money = 100000;
		int cross_count = 0;
		while(true) {
			if(total_money > 50000) {
				total_money *= 0.95;
				//每经过一次路口的时候,给次数+1
				cross_count++;
			}else if(total_money >= 1000) {
				total_money -= 1000;
				//每经过一次路口的时候,给次数+1
				cross_count++;
			}else {
				//钱数不够1000的话  就 过不去路口了
				break;
			}
		}
		
		System.out.println("一共通过了" + cross_count + "个路口" + "目前还有" + total_money + "钱");
		
		
	}
}
