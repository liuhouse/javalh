package chapter05;
/*
 * 单分支和双分支的练习题
 * */
public class IfExercise01 {
	public static void main(String[] args) {
		
		//===============练习题1========================
		/*
		 * 	对于下面的代码,若有输出,指定输出的结果
		 *  int x = 7;
			int y = 4;
			if(x > 5) {
				if(y > 5) {
					System.out.println(x + y);
				}
				System.out.println("韩顺平教育...");
			}else {
				System.out.println("x is" + x);
			}
			最终得到的结果是 "韩顺平教育...."
			
			思路分析：
				1) 如果 x > 5的话   执行代码里面的   否则执行else 后面代码块  
				2)这里为true 执行if后面的代码块  
				3)因为y > 5 是 false 的   所以   if(y > 5)后面的代码块不会执行
				4)不管 y>5 是 true  还是false  分支外面的代码块都会被执行  -- 所以最终打印出   “韩顺平教育”
		*/
		
		//=====练习题2=====
		//编写程序,声明2个double型变量并赋值.判断第一个数大于10.0，且第二个数小于20.0，打印两数之和
		double num1 = 15.0;
		double num2 = 18.0;
		/**
		 * 	思路分析
		 * 	1)	声明两个double类型的变量   num1,num2
		 * 	2)	使用if控制语句进行判断  (因为是且,所以使用短路与&&)  num1 > 10.0 && num2 < 20.0
		 * 	3) 	进行打印
		 * */
		if(num1 > 10.0 && num2 < 20.0) {
			System.out.println("num1 + num2 = " + (num1 + num2));
		}
		
		
		//=======练习题3=====
		//定义两个变量int，判断两者的和,是否能被3又能被5整除，打印提示信息  又能 所以使用  &&
		/*
		 * 	思路分析
		 * 	1)	定义两个变量int n1 ,int n2
		 * 	2)	定义两个变量的和 n_res = n1 + n2
		 * 	3) 	判断是否能被3整除，又能(从这里判断,应该使用短路与&&)被5整除 n_res % 3 ==0 && n_res % 5 == 0
		 * 	4)	使用if - else 打印出对应的结果
		 * */
		int n1 = 11;
		int n2 = 5;
		int n_res = n1 + n2;
		if(n_res % 3 == 0 && n_res % 5==0) {
			System.out.println("n1+n2的结果能被3整除又能被5整除");
		}else {
			System.out.println("n1+n2的结果不能把3和5都整除了");
		}
		
		
		//=======练习题4====
		//判断一个年份是否是闰年,闰年的条件是符合下面的二者之一： 二者之一   所以使用 | |
		//(1)年份能被4整除，但不能被100整除 (2)能被400整除
		/**
		 * 	思路分析
		 * 	1)	定义一个年份变量  int year
		 * 	2)	年份能被4整除,但是不能(短路与&&)被100整除  year % 4 == 0 && year % 100 != 0
		 * 	3)	年份能被400整除 year %	400 == 0
		 * 	4)	因为符合条件的二者之一  所以是短路或 | |
 		 * */
		int year = 2021;
		if((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
			System.out.println(year + "年是闰年....");
		}else {
			System.out.println(year + "年不是闰年....");
		}
		
	}
}
