package chapter05;
import java.util.Scanner;
//多重循环应用实例
public class MulForExercise01 {
	public static void main(String[] args) {
		/*
		 * 	需求 
		 * 		统计是3个班的成绩情况,每个班有5个同学
		 *  	求出各个班的平均分和所有班级的平均分【学生的成绩用键盘输入】
		 * 		统计三个班的及格人数,每个班有五个同学
		 * 	
		 * 	===自己的思路 【按照自己的思路做出来的   完全正确】
		 * 	思路分析
		 * 	化繁为简
		 * 	(1)	统计一个班的5个同学的分数 ,【键盘输入使用Scanner】[for]
		 * 	(2)	统计出这个班的平均分  【定义一个double sum_score 保存这个班的总数     sum_score /5 一每个班的平均分】
		 * 	(3) 外层使用for循环，统计3个班每个学生的成绩 
		 * 	(4)计算每个班的平均分【自然就出来了】
		 * 	(5)计算所有班级的平均分 将三个班的成绩相加   然后 / 15   
		 * 	(6)统计三个班的及格人数  定义一个及格的数   当学生的成绩 >= 60 给这个数+1
		 * 
		 * 	先死后活
		 * 	(1)班级数可以做成变量  int classNum = 3;
		 * 	(2)每个班级的同学数也可以做成变量  stuNum = 5;
		 * 
		 * 
		 * 	===老韩的思路
		 * 	(1)	先计算一个班,5个学生的成绩和平均分,使用for循环
		 * 		1.1 创建Scanner对象,然后,接收用户输入
		 * 		1.2 得到该班级的平均分,定义一个double sum 把该班级5个学生的成绩累加
		 * 	(2)	统计3个班(每个班5个学生)平均分
		 * 	(3)	所有班级的平均分
		 * 		3.1 定义一个变量,double totalScore 累计所有学生的成绩
		 * 		3.2	当多重循环结束的时候,totalScore/(3*5)
		 * 	(4)	统计三个班的及格人数
		 * 		4.1	定义变量 int passNum = 0 ; 当有一个学生的成绩>=60，passNum++
		 * 		4.2	如果 >= 60 passNum++
		 * 	(5)	可以优化【效率,可读性,结构】
		 * */
		
		//因为Scanner要多次使用,所以定义在外面
		Scanner myScanner = new Scanner(System.in);
		
		//统计三个班的学生的成绩
		
		//统计所有班级的总分   总分/15就是平均分
		double scoreTotal = 0;
		
		//统计三个班的及格人数
		int passNum = 0;
		
		//班级数
		int classNum = 3;
		
		//同学数
		int stuNum = 5;
		
		for(int i = 1 ; i <= classNum ; i++) {
			//一个班级的总数
			double sum_score = 0;
			
			for(int j = 1; j <= stuNum; j++) {
				System.out.println("请输入第"+i+"个班级的第"+j+"个学生的成绩：");
				double score = myScanner.nextDouble();
				//如果当前的成绩大于等于60就代表合格,那么就给合格数+1
				if(score >= 60) {
					passNum++;
				}
				sum_score += score;
				System.out.println("第"+i+"个班级的第"+j+"个学生的成绩为"+score+"分");
			}
			//计算所有班级的总分【就是将每个班级的总分进行相加】
			scoreTotal += sum_score;
			System.out.println("第"+i+"个班级的总分为" + sum_score + " / 平均分为" + (sum_score / stuNum)+"==============");
		}
		//所有班级的总分,自然是把每个班级的成绩统计完成之后才能统计出来总分
		System.out.println("所有班级的总分数为" + scoreTotal + "所有班级的平均分为" + (scoreTotal / (classNum * stuNum)));
		System.out.println("合格人数【成绩大于等于60】的有" + passNum + "人");
		System.out.println("====================================");
		System.out.println("班级成绩的统计工作已经结束了......");
	}
}
