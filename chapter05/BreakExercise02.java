package chapter05;
import java.util.Scanner;

public class BreakExercise02 {
	
	public static void main(String[] args) {
		/*
		 * 	实现登录验证，有 3 次机会，如果用户名为"丁真" ,密码"666"提示登录成功，否则提示还有几次机会，请使用 for+break
		 * 	思路分析
		 * 	(1):使用for循环三次
		 * 	(2):在循环中使用Scanner,因为是输入的
		 * 	(3):定义变量String username    定义变量String password ,并使用Scanner进行接收
		 * 	(4):判断当前username是不是 == 丁真  && password 是不是 == 666 
		 * 	(5):如果是：登录成功,使用break跳出循环 ,如果不是,就提示错误,并且给出还剩多少次，然后继续输入
		 * */
		//定义在外面,因为要多次使用
		int shengyu = 3;
		Scanner myScanner = new Scanner(System.in);
		for(int i = 1 ; i <= 3;i++) {
			System.out.println("请输入用户名：");
			//定义名称
			String username = myScanner.next();
			//定义密码
			System.out.println("请输入密码");
			String password = myScanner.next();
			
			if(username.equals("丁真") && password.equals("666")) {
				System.out.println("登录成功");
				//登录成功之后跳出循环
				break;
			}
			shengyu--;
			if(shengyu > 0) {
//				System.out.println("用户名或者密码错误,您还可以登录" + shengyu + '次');
			}else {
				System.out.println("剩余次数已经使用完,您的账号已经被冻结");
			}
			
		}
	}
}
