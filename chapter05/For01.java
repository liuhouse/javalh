package chapter05;

public class For01 {
	public static void main(String[] args) {
		//1:提出需求，使用现在的技术能够解决,如果現在要求打印出1000个,就很不方便了
		//2:提出优化方案  使用for循环
		//打印10句,你好java!
//		System.out.println("你好!java");
//		System.out.println("你好!java");
//		System.out.println("你好!java");
//		System.out.println("你好!java");
//		System.out.println("你好!java");
//		System.out.println("你好!java");
//		System.out.println("你好!java");
//		System.out.println("你好!java");
//		System.out.println("你好!java");
//		System.out.println("你好!java");
		
		//使用for循环完成上面的需求
		for(int i = 1 ; i <= 1000 ; i++) {
			System.out.println("你好java!" + i);
		}
		
		
	}
}
