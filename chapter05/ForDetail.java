package chapter05;

public class ForDetail {
	public static void main(String[] args) {
		
		/**
		 *	 概念
		 *	1：循环条件是返回一个布尔值的表达式
		 *	2:for(;循环判断条件;)中的初始化和变量迭代可以写到其他地方,中间两边的分号是不能省略的
		 *	3:循环初始值可以有多条初始化语句,但是要求类型一样,并且中间用逗号隔开,循环变量迭代也可以有多条变量迭代语句,中间用逗号隔开
		 * */
		
		
		int i = 0;
		for(;i < 3;) {
			System.out.println("i= " + i);
			i++;
		}
		
		//i = 0 j = 0 [会输出]
		//i = 1 j = 2[会输出]
		//i = 2 j = 4[会输出]
		//i = 3 j = 6[不会输出]
//		int count = 3;
//		for(int i = 0,j=0;i < count ; i++,j+=2) {
//			System.out.println("i= " + i + "j= " + j);
//		}
	}
}
