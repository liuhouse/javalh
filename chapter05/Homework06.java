package chapter05;

public class Homework06 {
	public static void main(String[] args) {
		/*
		 * 	需求 输出1-100之间不能被5整除的数,每五个一行
		 * 	//思路分析
		 * 	(1)输出1-100之间的数
		 * 	(2)进行判断 打印出不能被5整除的数  n %5 != 0
		 * 	(3)定义一个计数器,当计数器的值能被5整除的时候  换行
		 * */
		int count = 0;
		for(int i = 1 ; i <= 100 ; i++) {
			if(i % 5 != 0) {
				System.out.print(i + " ");
				count++;
				if(count % 5 == 0) {
					System.out.println(" ");
				}
			}
			
		}
		
	}
}
