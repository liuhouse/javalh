package chapter05;
import java.util.Scanner;

public class HomeWork04 {
	public static void main(String[] args) {
		/*
		 * 	需求：判断一个整数是否为水仙花数,所谓的水仙花数指的就是一个3位数,其各个位上数字的立方和等于其本身
		 * 	例如 ： 153 = 1*1*1 + 3*3*3 + 5*5*5
		 * 
		 * 	思路分析
		 * 	//1:实例化对象Scanner  因为要输入整数
		 * 	//2:定义一个三位数整数，使用scanner的特性得到三位数的每一位
		 * 	//3:使用公式进行计算 总数
		 * 	//4:if -else   如果符合  就是水仙花数  否则就不是
		 * */
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("请输入一个三位数的整数.");
		
		String num_str = myScanner.next();
		
		int num1 = Integer.parseInt(num_str.charAt(0) + "");
		int num2 = Integer.parseInt(num_str.charAt(1) + "");
		int num3 = Integer.parseInt(num_str.charAt(2) + "");
		
		//计算总数
		int sum_total = (num1*num1*num1) + (num2*num2*num2) + (num3*num3*num3);
		
		//进行判断
		//字符串和整数不能直接做比较,要进行转化
		//这里要使用equals() 进行字符串的比较   所以 把运算结果转化成字符串
		if(num_str.equals(sum_total+"")) {
			System.out.println(num_str + "是水仙花数");
		}else {
			System.out.println(num_str + "不是水仙花数");
		}
		
		
		
	}
}
