package chapter05;
/*
 * 	课堂练习2
 * */
public class DoWhileExercise02 {
	public static void main(String[] args) {
		/*
		 * 	需求
		 * 	统计 1 - 200 之间能被5整除但是不能被3整除的个数
		 * 	思路分析
		 * 	化繁为简
		 * 	(1) 先打印出1-200之间的所有整数
		 * 	(2)	筛选 使用判断条件进行控制 逻辑与&&  筛选出能被5整除但是不能被3整除的数 
		 * 	(3)定义一个变量 count   符合条件的就进行 +1 操作
		 * 	
		 * 	先死后活
		 * 	(1)变量范围 可以进行替换   start = 1  end = 200 
		 * 	(2)能同时被整除的数    num1 = 5   num2 = 3
		 * 	
		 * */
		
		//声明范围开始的值
		int start = 1;
		//声明范围结束的值
		int end = 2000;
		int num1 = 5;
		int num2 = 3;
		int count = 0;
		do {
			if(start % num1 == 0 && start % num2 != 0) {
				System.out.println(start);
				count++;
			}
			start++;
		}while(start <= end);
		System.out.println("1 - 200 之间能被5整除但是不能被3整除的个数" + count + "个");
	}
	
}
