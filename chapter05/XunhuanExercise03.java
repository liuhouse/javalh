package chapter05;
/*
 * 打印九九乘法表
 * */
public class XunhuanExercise03 {
	public static void main(String[] args) {
		//先打印出1*1 = 1
		for(int i = 1 ; i <= 9 ; i++) {
			for(int j = 1 ; j <= i ;j++) {
				System.out.print(j + " * " + i + " = " + (j * i)  + "\t");
			}
			System.out.println("\r\n");
		}
	}
}
