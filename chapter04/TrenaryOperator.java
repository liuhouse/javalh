package chapter04;

//三元运算符的使用
public class TrenaryOperator {
	public static void main(String[] args) {
		/*
		 * 	条件表达式 ? 表达式1 : 表达式2
		 * 	运算规则
		 * 	(1)如果条件表达式为真,执行表达式1 
		 * 	(2)如果条件表达式为假,执行表达式2
		 * 	口诀 一真大师 
		 * */
		
		//解读
		//1：a > b 为 false
		//2:返回 b--,因为是后--，所以先返回b的值赋给result,然后再执行b = b -1
		//3:返回的结果 
		
		int a = 10;
		int b = 99;
		
		int result = a > b ? a++ : b--;
		System.out.println(result);//99
		System.out.println("a = " + a);//10
		System.out.println("b = " + b);//98
	}
}
