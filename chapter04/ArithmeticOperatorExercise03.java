package chapter04;

public class ArithmeticOperatorExercise03 {
	public static void main(String[] args) {
		
		/*
		 * 	一：需求分许   二：整理思路   三：编码
		 * 	1:需求 ->
		 * 		定义一个变量保存华氏温度，华氏温度转换摄氏温度的公式为：5/9*(华氏温度-100),请求出华氏温度对应的摄氏温度。 [234.5]
		 * 	2:思路分析
		 * 		(1) 使用 float Hua_Wen = 234.5;
		 * 		(2) 根据公式计算摄氏温度  float She_Wen = 5 / 9 * (Hua_Wen - 100)
		 * 		(3) 输出
		 * */
		//代码
		double Hua_Wen = 1234.6;
		double She_Wen = 5.0 / 9 * (Hua_Wen - 100);
		//输出
		System.out.println("华氏温度" + Hua_Wen + "对应的摄氏维度为" + She_Wen);
	}
}
