package chapter04;
//运算符的使用
//演示 | 和 || 的使用
public class LogicOperator02 {
	public static void main(String[] args) {
		//短路或 || 和 逻辑或 | 的演示案例
		//短路或 || 两个条件中只要有一个是成立的,结果就是true,否则就为false
		//逻辑或 |  两个条件中只要有一个是成立的,结果就是true,否则就是false
		
		int age = 50;
		if(age > 20 || age < 30) {//true
			System.out.println("ok100");
		}
		
		//逻辑或的使用
		if(age > 20 | age < 30) {//true
			System.out.println("ok200");
		}
		
		
		//进行测试
		//短路或 || 第一个条件为true的时候  就不会执行第二个条件了   结果为true  执行效率高
		//逻辑或 | 不管第一个条件是true还是false  第二个条件都会执行判断的   执行效率不高
		int a = 4;
		int b = 9;
		if(a > 1 || ++b > 4) {//true
			System.out.println("ok300");
		}
		System.out.println("a = " + a + ' ' + " b = " + b);//4 9
		
		if(a > 1 | ++b > 4) {//true
			System.out.println("ok400");
		}
		System.out.println("a = " + a + ' ' + " b = " + b);//4 10
	}
}
