package chapter04;

public class ArithmeticOperatorExercise02 {
	//编写一个main方法
	public static void main(String[] args) {
		//假如还有 59 天放假，问：合 xx 个星期零 xx 天
		/*
		 * 	编程思路
		 * 	一:分析需求   二：思路分析   三：走代码
		 * 		1：需求
		 * 			假如还有59天放假,问：合xx个星期零xx天
		 * 		2：思路分析
		 * 			（1）使用int类型的变量days保存天数59
		 * 			（2）计算星期数 weeks = days / 7
		 * 			（3）计算剩余天数 leftDays = days % 7
		 * 		3：输出
		 * */
		
		
		//3：走代码
		int days = 54;//总天数
		int weeks = days / 7;//星期数
		int leftDays = days % 7; // 零天数
		System.out.println("离放假还有" + days + "天" + "合" + weeks + "个星期" + "零" + leftDays + "天");
	}
}
