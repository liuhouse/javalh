package chapter04;

/**
 * 	!和^案例演示
 * */
public class InverseOperator {
	public static void main(String[] args) {
		//操作是取反 !true -> false    !false -> true
		System.out.println(60 > 20);//true
		System.out.println(!(60 > 20));//false
		
		//a^b ： 叫做异或,当a和b不相同的时候,结果返回true，否则返回false
		boolean b = (10 > 1) ^ (3 > 5);//true
		System.out.println(b);
		
		
		System.out.println((4 < 1) ^ (6 > 3));
	}
}
