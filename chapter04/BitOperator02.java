package chapter04;

public class BitOperator02 {
	public static void main(String[] args) {
		//算术右移,低位溢出,符号位不变,用符号位补充溢出的高位
		int a = 1 >> 2;
		//1的源码   00000001 => 00000000   最终的结果是0 本质 1/2/2 = 0
		System.out.println(a);
		
		//算术左移,符号位不变,低位补0
		int b = 1 << 2;
		//1的源码 00000001 => 00000100 最终的结果是4  本质1*2*2 = 4
		System.out.println(b);//4
	}
}
