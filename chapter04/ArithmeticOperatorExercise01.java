package chapter04;

public class ArithmeticOperatorExercise01 {
	public static void main(String[] args) {
		//面试题1
		//问,i最后等于多少?为什么
		int i = 1;//i => 1
		i = i++;//后++ 规则使用临时变量 (1)temp = i;=>1   (2)i=i+1 => 2   (3) i = temp => 1
		System.out.println(i);
		
		//面试题2
		int j = 1;// j => 1
		j = ++j;//前++规则使用临时变量(1)j = j + 1 => 2 (2)temp = j => 2 (3) j = temp
		System.out.println(j);
		//10 20 19 19
		
		//System.out.println(6.3 / 7);
	}
}
