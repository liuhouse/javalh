package chapter04;
/*
 *	 演示逻辑运算符的使用
 * */
public class LogicOperator {
	public static void main(String[] args) {
		/*
		 * 	名称		语法			特点
		 * 	短路与&&	条件1&&条件2	两个条件都为true,结果为true,否则为false
		 * 	逻辑与&	条件1&条件2 	两个条件都为true,结果为true,否则为false
		 * */
		
		//&&短路与  和  逻辑与&案例演示
		int age = 50;
		if(age > 20 && age < 90) {//true
			System.out.println("ok100");
		}
		
		//&逻辑与使用
		if(age > 20 & age < 90) {//true
			System.out.println("ok200");
		}
		
		
		//区别
		int a = 4;
		int b = 9;
		//对于&&短路与而言,如果第一个条件为false,后面的条件就不再判断
		//对于&逻辑与而言,如果第一个条件为false的话,后面的条件仍然会判断
		/*if(a < 1 & ++b < 50) {
			System.out.println("ok300");
		}*/
		
		
		//System.out.println("a = " + a + " b=" + b);//4 10
		
		
		//因为第一个条件不符合   所以短路了  所以后面的++b不会执行
		if(a < 4 && ++b < 50) {
			System.out.println("ok300");
		}
		System.out.println("a = " + a + " b=" + b);//4 9
		
		
		
		
		
	}
}
