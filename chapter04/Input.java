package chapter04;

import java.util.Scanner;
public class Input {
	public static void main(String[] args) {
		//要求，可以从控制台接收用户信息 【姓名,年龄,薪水】
		
		//演示接收用户的输入
		//步骤
		//Scanner类 表示的是简单的文本扫描器,在java.util包中
		//1:引入/导入 Scanner类所在的包
		//2:创建Scanner对象,new创建一个对象，体会
		// myScanner就是 Scanner类的对象
		//3:接收用户的输入,使用相关的方法
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("请输入姓名：");
		//当程序执行到next()方法的时候,会等待用户输入
		String name = myScanner.next();//接收用户输入的字符串
		System.out.println("请输入年龄：");
		int age = myScanner.nextInt();//接收用户输入的int
		System.out.println("请输入薪水：");
		double sal = myScanner.nextDouble();//接收用户输入的double
		System.out.println("姓名 =  " + name + "年龄 =  " + age + "薪水 = " + sal);
	}
}
