package chapter04;

public class ArithmeticOperatorExercise03_1 {
	public static void main(String[] args) {
		//定义一个变量保存华氏温度,华氏温度转换摄氏温度的公式为 
		//5/9*(华氏温度-100),请求出华氏温度对应的摄氏温度
		
		//思路分析
		//(1):先定义一个double huaShi  保存华氏温度
		//(2):根据给出的公式,进行计算即可 5/9*(华氏温度-100)
		//(3):将得到的结果保存到double sheShi 
		//走代码
		double huaShi = 1234.6;
		//这里要考虑数学公式和java语言的特性
		//因为 5 /9 = 0    所以后面再怎么计算都是0   所以这边要使用double类型来计算  这样就可以保留小数
		double sheShi = 5.0 / 9 * (huaShi - 100);
		System.out.println("华氏温度" + huaShi + " 对应的摄氏温度为" + sheShi);
		System.out.println(5.0 / 9);
	}
}
