package chapter04;

public class FourZuoYe {
	public static void main(String[] args) {
		//计算下面表达式的结果
		/**
		 * 	10 / 3 = 3
		 * 	10 / 5 = 2
		 * 	10 % 2 = 0
		 * 	-10.5 % 3 = -10.5 - int(-10.5)/3*3 = -1.5
		 *  a % b 当a是小数的时候,公式 = a - int(a) / b * b;
		 *  注意，有小数运算的时候，得到的结果是近似值
		 * */
		System.out.println(-10.5%3);
		
		/*
		 * 	试着说出下面代码的结果
		 * 
		 * */
		int i = 66;
		//执行 前++ i = i + 1 = 67     67 + 67 = 134
		System.out.println(++i + i);//134
		
			
		//错误示例 - 字符串不能直接进行整形的强制转换
		//int num1 = (int)"18";
		//正确的应该是
		int num1 = Integer.parseInt("18");
		//System.out.println(num1);
		
		//int num2 = 18.0; 错误的,double => int 
		double num3 = 3d;//正确的,后面是d，代表的是double类型
		double num4 = 4;//正确的 int -> double
		
		//j+1 运算结果是int  char => int  错误
		//int j = 48; char ch = j+ 1;
		
		//byte b = 19； short s = b + 2 错误的  short => int  是错误的
		
		//试着写出将String转换成double类型的语句,以及将char类型转化成String类型的语句
		//举例子说明即可 编写简单的代码
		String str = "18.5";
		//转化成double类型
		double d1 = Double.parseDouble(str);
		System.out.println(d1);
		
		//将char类型转化成字符串、
//		char c1 = '韩';
//		String c_str = c1 + "";
//		System.out.println(c_str);
	}
}
