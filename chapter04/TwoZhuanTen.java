package chapter04;

/*
 * 进制转换
 * */
public class TwoZhuanTen {
	
	public static void main(String[] args) {
		
		//0b开头的是二进制   ;   011 0开头的是八进制   ;    10 : 十进制    0x开头的是十六进制 
		
		
		//二进制转十进制
		/*规则 ： 从最低位(右边开始),将每个位上的数提取出来,乘以2的(位数-1)次方，然后求和
		   * 案例 ： 请将 0b1011 转成十进制的数
		 *0b1011 = 1*2(1-1)次方  + 1*2(2-1)次方  + 0*2(3-1)次方 + 1*2(4-1)次方 = 1 + 2 + 0 + 8 = 11
		*/
		
		
		//八进制转十进制
		/*
		 *  规则：从最低位(右边)开始,将每个位上的数提取出来,乘以8的(位数-1次方)，然后求和
		 *  案例：请将0234转成十进制的数
		 *  0234 = 4*8(1-1) + 3*8(2-1) + 2 * 8(3-1) = 4 + 24 + 128 = 156
		 * */
		
		
		//十六进制转成十进制
		/*
		 * 	规则：从最低位(右边开始)，将每个位上的数提取出来,乘以16的(位数-1)次方，然后求和。
		 * 	案例：请将 0x23A转成十进制的数
		 * 	0x23A = 10*16^0 + 3*16^1 + 2*16^2 = 10 + 48 +  512 = 570
		 * */
		
		
		
		//int shiliures = 0x23A;
		//System.out.println(shiliures);
		
		
		//课堂练习:请将
		//0b110001100  【二进制】 转化成十进制
		//0b110001100  0*2^0 + 0*2^1 + 1*2^2 + 1 *2^3 + 0*2^4 + 0 *2^5 + 0*2^6 + 1*2^7 + 1 *2^8
		//             0     + 0     + 4     +    8   + 0   +  0     + 0    + 128   + 256 = 396
		//int n1 = 0b110001100;
		//System.out.println(n1);//396
		
		//02456 【八进制】 转化成十进制
		//02456 6*8^0 + 5*8^1 + 4*8^2 + 2*8^3
		//      6     + 40    + 256   + 1024 = 1326
		//int n2 = 02456;
		//System.out.println(n2);
		
		
		//0xA45 【十六进制】 转化成十进制
		//0xA45 5*16^0 + 4*16^1 + 10*16^2 = 5 + 64 + 2560 = 2629
		//int n3 = 0xA45;
		//System.out.println(n3);
		
		//01230 【八进制】 转化成二进制
		//01230 = 0B001010011000
		
		//0xAB29 【十六进制】 转化成二进制
		//0xAB29 = 0B1010101100101001
		
		
		int n1 = 0xAB29;
		System.out.println(n1);
		
		
		
		
		
		
		
		
	}
	
	
	
	
	
	
}
