package chapter04;

public class TrenaryDetail {
	public static void main(String[] args) {
		//表达式1 和 表达式2要为可以赋给接收变量的类型 (或者可以自动转换)
		//三元运算符可以转化成  if   else 语句
		int a = 3;
		int b = 8;
//		int c = a > b ? a : b; //这样是可以的  因为  a 和 b都是整数     而变量c也是整数
//		System.out.println(c);
		
		//int 类型 不能接收double类型的值
		
		//使用类型的强制转化是可以的
		int c = a > b ? (int)1.1 : (int)3.4;
		
		//第二种解决方式
		double d = a > b ? a : b + 3; // int -> double   所以这是正确的
		
		
		int res = a > b ? a : b;
		//上面的写法的底层逻辑是
		int res1;
		if(a > b) res1 = a;
			else res1 = b;
		System.out.println(res1);
	}
}
