package chapter04;

public class TernaryOperatorExercise {
	public static void main(String[] args) {
		//需求：实现三个数的最大值
		/**
		 * 	案例：实现三个数的最大值
		 * 	int n1 = 10; 
		 *  int n2 = 22; 
		 *  int n3 = 35;
		 *  
		 *  //思路 
		 *  1：先得到n1和n2的最大值 保存到max1
		 *  2:再得到max1和n3的最大值 保存到max3
		 *  3:输出
		 * */
		
		//编码
		int n1 = 101;
		int n2 = 22;
		int n3 = 35;
		
		int max1 = n1 > n2 ? n1 : n2;//22
		int max2 = n3 > max1 ? n3 : max1;//35
		System.out.print("经过比较得出的最大值是" + max2);
		
		
		int abcclass = 10;
		int n = 40;
		int N = 50;
		
		//请记得   java中的变量是区分大小写的   abc 和  aBc是两个不同的变量
		int abc = 100;
		int aBc = 200;
		
		
		//下面是错误的赋值运算符的案例
//		int a b = 300;
		
//		int a-b = 10;
		
//		int goto 1 = 10;
		
		int $a = 1;
		System.out.println($a);
		
		//正确   正确   错误   错误   错误  正确  错误  错误 错误 错误 错误  错误  正确
			
		//包名  aaa.bbb.ccc
		//类名,接口名  TankFight
		//变量名, tankFight
		//常量名  PATH   ROOT_PATH
		
		
		
	}
}
