package chapter04;

/*
 * 逻辑运算符练习题2
 * */
public class LogicOperatorTest2 {
	public static void main(String[] args) {
		boolean x = true;
		boolean y = false;
		short z = 46;
		//当 ++z  或者  z++ 当做一条单独的语句使用的时候  效果是一样的  都相当于  z = z + 1;
		if((z++ == 46) && (y = true)) z++;//48
		System.out.println(z);
		if((x = false) || (++z == 49)) z++;//50
		System.out.println(z);
	}
}



