package chapter04;

/*
 * 赋值运算符的演示
 * */
public class AssignOperator {
	public static void main(String[] args) {
		//赋值运算符的特点
		/*
		 * (1)运算顺序从右到左  int num = a + b +c
		 * (2)赋值运算符的左边只能是变量,右边可以是变量,表达式,常量 int num = 1; int num = a; int num = a + b + c
		 * (3)复合赋值运算符等价与下面的效果  a += 3 相当于  a = a + 3  其他的类推
		 * (4)复合赋值运算符会进行类型转化 byte b = 2;   b += 3 ; 相当于   b = byte(b + 3)  ;b++ 相当于  b = byte(b+1)
		 * */
		
		//普通的赋值运算符
		int n1 = 10;
		n1 += 4; // n1 = n1 + 4  其实做了类型转换  但是这里知道原理即可
		System.out.println(n1);//14
		
		n1 /= 3; // n1 = n1 / 3
		System.out.println(n1);//4
		
		//复合赋值运算符会进行类型转换
		byte b = 3;
		b += 2;//等价于 b = byte(b + 2);
		System.out.println(b);//5
		b++;//等价于  b = byte(b + 1);
		System.out.println(b);
		
	}
}
