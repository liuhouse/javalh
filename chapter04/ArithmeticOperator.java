package chapter04;
/**
 * 	演示算数运算符的使用
 * */
public class ArithmeticOperator {
	//编写一个main方法
	public static void main(String[] args) {
		//使用
		//进行计算的时候,计算的结果的类型  是里面精度最大的类型   最后的结果会自动进行转化
		System.out.println(10 / 4);//从数学来看是2.5,java中是2 因为 除数和被除数都是int  所以结果也是int型的
		System.out.println(10.0 / 4);//java是2.5 
		//注释的快捷键是ctrl+/ , 再次输入 ctrl+/ 就会取消注释
		double d = 10 / 4;// 10 /4 = 2     2 => 2.0
		System.out.println(d);
		
		
		//% 取模  (取余)
		//%的本质 看一个公式      a % b = a - a / b * b
		// -10 - (-10) / 3 * 3  = -1
		System.out.println(-10 % 3);//-1
		// 10 - 10 / (-3) * (-3) = 1
		System.out.println(10 % -3);
		// -10 - (-10) / (-3) * (-3) = -1
		System.out.println(-10 % -3);//-1
		// 10 - 10 / 3 * 3 = 1
		System.out.println(10 % 3);//1
		
		
		
		//++的使用
		//当 ++i 或者 i++ 单独作为一条语句使用的话   全部都等价于 i = i + 1 
		int i = 10;
		//i++;//自增 等价于  i = i + 1; => i = 11;
		++i;//自增 等价于 i = i + 1; => i = 11;
		System.out.println(i);
		
		
		
		/*
		 * 	作为表达式使用
		 * 	前++; ++i先自增后赋值
		 *       后++;i++先赋值后自增
		 * */
		int j = 8;
		int k = j++;//等价于 k = j ; j = j + 1
		
		System.out.println(j);
		System.out.println(k);
		
		int m = 8;
		int n = ++m;//m = m + 1 = > 9     n = m = 9
		System.out.println(m);
		System.out.println(n);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
