package chapter04;
//演示关系运算符的使用
public class RelationalOperator {
	//编写一个main方法
	public static void main(String[] args) {
		//细节说明
		//1)关系运算符的结果都是boolean型,也就是要么是true,要么是false
		//2)关系运算符组成的表达式,我们成为关系表达式 a > b
		//3)比较运算符"==" 不能误写为=
		int a = 9;//在开发中,不可以使用a,b作为变量
		int b = 8;
		System.out.println(a > b);//true
		System.out.println(a >= b);//true
		System.out.println(a <= b);//false
		System.out.println(a < b);//false
		System.out.println(a == b);//false
		System.out.println(a != b);//true
		boolean flag = a > b;//true
		System.out.println("flag = " + flag);
	}
}
