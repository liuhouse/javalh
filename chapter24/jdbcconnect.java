package javalh.chapter24;

import com.mysql.cj.jdbc.Driver;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 * 获取数据库连接的五中方式
 */
@SuppressWarnings("all")
public class jdbcconnect {
    //方式1
    @Test
    public void conn1() throws SQLException {
        //获取Driver实现类对象
        Driver driver = new Driver();
        String url = "jdbc:mysql://localhost/hsp_db03";
        Properties properties = new Properties();
        properties.setProperty("user" , "root");
        properties.setProperty("password" , "root");
        Connection connect = driver.connect(url, properties);
        System.out.println(connect);
    }

    //方式二
    //方式1 会直接使用com.mysql.cj.jdbc.Driver(),属于静态加载,灵活性差，依赖性强
    // -- 推出 ---> 方式二
    @Test
    public void conn2() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        //使用反射加载驱动
        Class<?> aClass = Class.forName("com.mysql.cj.jdbc.Driver");
        //实例化driver对象
        Driver driver = (Driver) aClass.newInstance();
        //创建连接的url
        String url = "jdbc:mysql://localhost:3306/hsp_db03";
        //创建用户名和密码
        Properties properties = new Properties();
        properties.setProperty("user" , "root");
        properties.setProperty("password" ,"root");

        Connection connect = driver.connect(url, properties);
        System.out.println(connect);
    }

    //方式3
    @Test
    public void conn3() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        //使用DriverManager替换Driver
        Class<?> aClass = Class.forName("com.mysql.cj.jdbc.Driver");
        Driver driver = (Driver) aClass.newInstance();
        String url = "jdbc:mysql://localhost:3306/hsp_db03";
        String user = "root";
        String password = "root";
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println(connection);
    }


    //方式4
    @Test
    public void conn4() throws ClassNotFoundException, SQLException {
        //使用Class.forName 完成自动注册驱动,简化代码,分析源码
        Class<?> aClass = Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/hsp_db03";
        String user = "root";
        String password = "root";
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println(connection);

        //老韩提示:
        //1.mysql驱动5.1.6可以无需Class.forName("com.mysql.cj.jdbc.Driver")
        //2.从jdk1.5以后使用了jdbc4,不再需要显示调用class.forName()注册驱动而是自动调用驱动
        // jar包META-INF\services\java.sql.Driver文本中的类名称去注册
        //3.建议还是写上Class.forName("com.mysql.cj.jdbc.Driver");更加明确
    }

    //方式五
    @Test
    public void conn5() throws IOException, ClassNotFoundException, SQLException {
        //使用配置文件,连接数据库更加灵活
        //在方式4的基础上进行改进,增加配置文件,让mysql更加的灵活
        //通过Properties对象获取配置文件信息
        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter24\\mysql.properties"));
        //获取相关的值
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String driver = properties.getProperty("driver");
        String url = properties.getProperty("url");
        Class.forName(driver);
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println(connection);
    }




}
