package javalh.chapter24;

import com.mysql.cj.jdbc.Driver;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 */
public class jdbc02 {
    public static void main(String[] args) throws SQLException {
        //创建驱动
        Driver driver = new Driver();
        //连接mysql
        String s = "jdbc:mysql://localhost:3306/hsp_db03";
        Properties properties = new Properties();
        properties.setProperty("user" , "root");
        properties.setProperty("password" , "root");
        Connection connect = driver.connect(s, properties);
        Statement statement = connect.createStatement();
        //创建sql语句,执行sql语句
        String sql = "insert into actor value(null, '张卫健' , '男' , '1995-11-12','17691107518')";
        int rows = statement.executeUpdate(sql);
        System.out.println(rows > 0 ? "操作成功" : "操作失败");


    }
}
