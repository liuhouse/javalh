package javalh.chapter24;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Mysql01 {
    public static void main(String[] args) {
        /**
         * 连接到Mysql服务(Mysql数据库)的指令
         * mysql -h 主机IP -P 端口号 -u用户名 -p密码
         * 例子：
         * mysql -h localhost -P 3306 -u root -proot
         * 老韩提醒
         * (1)-p 密码不要有空格
         * (2)-p 后面没有密码,回车会要求输入密码
         * (3)如果没有写-h 主机 ,默认就是本机
         * (4)如果没有写 -P 端口,默认就是3306
         * (5)在实际工作中,3306 一般修改
         *
         */


        /**
         * 数据库的三层结构 - 破除MySql的神秘
         * 1.所谓的安装MySql数据库,就是在主机上安装一个数据库管理系统(DBMS),这个管理程序可以管理管理多个数据库,DBMS(database manage system)
         * 2.一个数据库可以创建多个表,以保存数据(信息)
         * 3.数据库管理系统(DBMS)，数据库和表的关系如图所示
         */

        /**
         * 数据在数据库中的存储方式
         * 使用行(row)和列(column)表示
         *              列(column)
         * ------------------------------------------
         * userid   username   password  email
         * 1          hsp1       hsp123    hsp123@qww
         * ------------------------------------------     行(row)
         * 2          hsp2       hsp123     hsp123@qww
         * ------------------------------------------
         * 3          hsp3       hsp123    hsp123@qww
         * ------------------------------------------
         *
         *表的一行称为一条记录 -> 在java程序中,一条记录往往使用对象表示
         */

        /**
         * SQL的语句分类
         * DDL:数据定义语句 [create 表 ， 库]
         * DML:数据库操作语句 [增insert ， 修改  update , 删除 delete]
         * DQL:数据库查询语句[select]
         * DCL:数据库控制语句[管理数据库：比如用户权限 grant revoke]
         * 
         */


    }
}
