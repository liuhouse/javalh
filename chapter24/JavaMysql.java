package javalh.chapter24;

import java.sql.*;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings({"all"})
public class JavaMysql {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //这里老韩给大家演示一下  java程序如何操作mysql
        //加载类,得到mysql连接
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lh_test", "root", "root");

        //创建一个商品表,选用适当的数据类型
        //增加两条数据
        //删除表goods

        //这里可以编写sql 【create ， select , insert , update ,delete...】
        //创建数据表
        //String sql = "create table lh_goods(id int(11) , name varchar(32) , price double , intrudce text)";

        //插入数据
        String sql = "insert into lh_goods values(2,'华为手机',2100,'这真的是一款不错的手机')";


        //查询数据
        //String sql = "select * from lh_goods";

        //得到statement对象把sql语句发送给mysql执行
        Statement statement = connection.createStatement();
        //执行操作
        statement.executeUpdate(sql);

        //关闭连接
        statement.close();
        connection.close();
        System.out.println("成功~");

    }
}
