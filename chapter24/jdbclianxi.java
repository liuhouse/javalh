package javalh.chapter24;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 */
public class jdbclianxi{
    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
        Properties properties = new Properties();
        //读取文件的内容使用的是文件的输入流
        properties.load(new FileInputStream("javalh\\\\chapter24\\\\newsmysql.properties"));
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String url = properties.getProperty("url");
        String driver = properties.getProperty("driver");
        Class.forName(driver);
        Connection connection = DriverManager.getConnection(url, user, password);
        Statement statement = connection.createStatement();
        //2.使用jdbc添加5条数据
        String sql = "insert into news values" +
                "(null,'四大天王之刘德华','1995-12-06')," +
                "(null , '四大天王之张学友' , '1996-11-11')," +
                "(null , '四大天王之黎明' , '1963-08-09')," +
                "(null , '四大天王之郭富城' , '1556-08-02')," +
                "(null , '抗争四大天王王杰' , '1526-06-02')";
        //3:修改id=1的记录,将content改成一个新的消息
        sql = "update news set content = '樊振东获得冠军' where id = 1";
        //4:删除id=3的记录
        sql = "delete from news where id = 3";
        int rows = statement.executeUpdate(sql);
        System.out.println(rows > 0 ? "操作成功" : "操作失败");


    }
}
