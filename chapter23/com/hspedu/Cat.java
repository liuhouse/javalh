package javalh.chapter23.com.hspedu;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Cat {
    private String name = "小花花";
    public int age = 12;

    //无参构造器
    public Cat(){}

    //有参构造器
    public Cat(String name){

    }

    public void hi(){

    }

    public void cry(){
        System.out.println("小孩子哭了");
    }

}
