package javalh.chapter23.com.hspedu.class_;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 * 类加载
 */
public class Class04_ {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException, ClassNotFoundException {
        /**
         * 反射机制是java实现动态语言的关键,也就是通过反射实现类动态加载
         * 1.静态加载：编译时加载相关的类,如果没有则报错,依赖性太强
         * 2.动态加载,运行时加载需要的类,如果运行时不用该类,即使不存在该类,则不报错,降低了依赖
         * 
         */
        Scanner scanner = new Scanner(System.in);
        System.out.println("please key 1|2:");

        String k_str = scanner.next();
        switch(k_str){
            case "1":
                Cat cat = new Cat();
                cat.cry();
                break;
            case "2":
                Class class1 = Class.forName("Cat1");
                Object o = class1.newInstance();
                Method method = class1.getMethod("cry");
                method.invoke(o);
                break;
        }
    }
}
class Cat{
    public void cry(){
        System.out.println("hi");
    }
}

