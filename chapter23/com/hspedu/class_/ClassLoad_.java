package javalh.chapter23.com.hspedu.class_;

/**
 * @author 刘皓
 * @version 1.0
 * Initialization(初始化)
 */
public class ClassLoad_ {
    public static void main(String[] args) {
        /**
         * 1.初始化阶段,才真正开始执行类中定义的java代码，此阶段是执行<clinit>() 方法的过程
         * 2.<clinit>()方法是由编译器按照语句在源文件中出现的顺序,依次自动收集类中所有的静态变量的赋值动作
         * 和静态代码块的语句,并进行合并
         * 3.虚拟机会保证一个类的<clinit>()方法在多线程环境中被正确的加锁,同步,如果多个线程同时去初始化一个类
         * 那么只有一个线程去执行这个类<clinit>() 方法,其他线层都需要阻塞等待,直到活动线程执行<clinit>()方法完毕
         *
         *
         */

        /**
         * 老韩分析
         * 1.加载B类,生成B的class对象
         * 2.链接num=0
         * 3.初始化阶段
         *  依次自动收集类中的所有静态变量的赋值动作和静态代码块中的语句,并合并

         clinit(){
            System.out.println("B静态代码块被执行");
            num = 300;
            num = 100;
         }

         并合并
         num = 100

         类在加载的时候,是有同步机制控制的

         //正因为有这个机制,才能保证某个类的内存中,只有一份Class对象
         synchronized(getClassLoadingLock(name)){

         }

         */

//        new B();//类加载
        //System.out.println(B.num);//100,如果直接使用类的静态属性,也会导致类的加载

        System.out.println(B.num);//100 , 如果直接使用类的静态属性,也会导致类的加载
    }
}

class B{
    static {
        System.out.println("B静态代码块被执行");
        num = 300;
    }
    static int num = 100;
    public B(){//构造器
        System.out.println("B()构造器被执行...");
    }
}
