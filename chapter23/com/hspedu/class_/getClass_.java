package javalh.chapter23.com.hspedu.class_;

import javalh.chapter23.com.hspedu.Car;

/**
 * @author 刘皓
 * @version 1.0
 * 演示得到Class对象的各种方式
 */
public class getClass_ {
    public static void main(String[] args) throws Exception {
        //1.Class.forName
        //获取全路径
        //通过读取配置文件读取
        //得到class对象
        String classAllPath = "javalh.chapter23.com.hspedu.Car";
        Class<?> cls1 = Class.forName(classAllPath);
        System.out.println(cls1);

        //2.类名.class,应用场景：用于参数传递
        //获取这个Car的class   主要用于参数传递
        Class<Car> cls2 = Car.class;
        System.out.println(cls2);

        //对象.getClass(),应用场景,有对象实例
        Car car = new Car();
        Class<? extends Car> cls3 = car.getClass();
        System.out.println(cls3);

        //4.通过类加载器(4种)来获取到类的Class对象
        //(1):先得到类加载器car
        ClassLoader classLoader = car.getClass().getClassLoader();
        //(2):通过类加载器得到class对象
        Class<?> cls4 = classLoader.loadClass(classAllPath);
        System.out.println(cls4);

        //这里必须要说明的是   cls1 , cls2 , cls3 , cls4 其实是同一个对象
        System.out.println(cls1.hashCode());
        System.out.println(cls2.hashCode());
        System.out.println(cls3.hashCode());
        System.out.println(cls4.hashCode());



        //5.基本数据类型对应的包装类,可以通过 .TYPE得到对应的Class类对象
        Class<Integer> integerClass = int.class;
        Class<Character> characterClass = char.class;
        Class<Boolean> booleanClass = boolean.class;

        //会获取到当前基本数据类型的包装类
        //但是在输出的时候又会转化为基本数据类型
        System.out.println(integerClass);


        //包装类.TYPE  可以获取基本数据类型
        Class<Integer> type = Integer.TYPE;
        Class<Character> type1 = Character.TYPE;
        //输出的时候  输出的是基本的数据类型
        System.out.println(type);
        System.out.println(type1);


        //其实两个是一样的 因为类只会加载一次
        System.out.println(integerClass.hashCode());
        System.out.println(type.hashCode());



    }
}
