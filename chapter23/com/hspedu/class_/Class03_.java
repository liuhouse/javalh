package javalh.chapter23.com.hspedu.class_;

import java.io.Serializable;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Class03_ {
    public static void main(String[] args) {
        Class<String> cs1 = String.class;//外部类
        Class<Serializable> cs2 = Serializable.class;//接口
        Class<Integer[]> cs3 = Integer[].class;//数组
        Class<float[][]> cs4 = float[][].class;//二维数组
        Class<Deprecated> cs5 = Deprecated.class;//注解对象
        Class<Thread.State> cs6 = Thread.State.class;//枚举
        Class<Long> cs7 = long.class;//基本数据类型
        Class<Void> cs8 = void.class;//void数据类型
        Class<Class> cs9 = Class.class;

        //只要是数据类型都有其自己对象的类
        System.out.println(cs1);
        System.out.println(cs2);
        System.out.println(cs3);
        System.out.println(cs4);
        System.out.println(cs5);
        System.out.println(cs6);
        System.out.println(cs7);
        System.out.println(cs8);
        System.out.println(cs9);


    }
}
