package javalh.chapter23.com.hspedu.class_;

import javalh.chapter23.com.hspedu.Car;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author 刘皓
 * @version 1.0
 * 演示Class类的常用方法
 */
@SuppressWarnings({"all"})
public class Class02 {
    public static void main(String[] args) throws Exception {
        //定义类的完整路径
        String classAllPath = "javalh.chapter23.com.hspedu.Car";
        //1.获取到Car类 对应的Class对象
        //<?>表示不确定的Java类型
        Class<?> cls = Class.forName(classAllPath);
        //2.输出cls
        System.out.println(cls);
        System.out.println(cls.getClass());//输出cls的运行类型 java.lang.Class

        //3.得到包名
        System.out.println(cls.getPackage().getName());

        //4.得到全类名
        System.out.println(cls.getName());//javalh.chapter23.com.hspedu.Car

        //5.通过cls创建对象实例
        Object o = cls.newInstance();
        System.out.println(o);

        //5.1 . 获取对象实例，对象实例是可以进行向下转型的
        Car car = (Car)cls.newInstance();
        System.out.println(car);

        //6.通过反射获取属性 brand
        //私有的获取产品
        Field brand = cls.getField("brand");
        System.out.println(brand.get(car));

        //7.通过反射给属性赋值
        brand.set(car , "奔驰");
        System.out.println(brand.get(car));

        //8.我希望可以得到所有的属性(字段)
        System.out.println("==============所有的字段属性============");
        //这里要记得,私有属性是获取不到的,只有公开的属性和默认的属性才能获取到
        Field[] fields = cls.getFields();
        for (Field field : fields){
            System.out.println(field.getName());
        }

        //获取Class类对象的接口
        Class<?>[] interfaces = cls.getInterfaces();
        System.out.println(interfaces.length);

        System.out.println(cls.getClassLoader());

        //这样的方式获取属性,不管是公有的还是私有的,都可以获取到
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field f : declaredFields){
            System.out.println(f.getName());
            System.out.println(f.get(car));
        }

        Method hi = cls.getMethod("hi");
        System.out.println(hi);
        hi.invoke(car);




    }
}
