package javalh.chapter23.com.hspedu.class_;

import javalh.chapter23.com.hspedu.Cat;

/**
 * @author 刘皓
 * @version 1.0
 * 对Class类特点的梳理
 */
public class Class01_ {
    public static void main(String[] args) throws ClassNotFoundException {
        /**
         *
         * 1.Class也是类,因此也继承了Object类【类图】
         * 2.class类对象不是New出来，而是由系统创建的
         * 3.对于某个类的Class对象,在内存中只有一份,因为类只加载一次
         * 4.每个类的实例都会记得自己是由哪个Class所生成
         * 5.通过Class对象可以完整的得到一个类的完整的结构,通过一系列API实现
         * 6.Class对象是存放在堆中的
         * 7.类的字节码二进制数据,是存在在方法区的,有的地方称为类的元数据(包括 方法代码,变量名,方法名,访问权限等等)
         *
         */

        //看看Class类图
        //1.Class也是类,因此继承了Object类
        //2.Class类对象不是new出来的,而是由系统创建的
        //(1):传统New对象
        /**
         * ClassLoader类,仍然是通过ClassLoader类加载Cat类的Class对象
         * public Class<?> loadClass(String name) throws ClassNotFoundException{
         *     return loadClass(name,false);
         * }
         */
//        Cat cat = new Cat();
//        cat.hi();


        //反射方式
        /**
         * ClassLoader类,仍然是通过ClassLoader类加载Cat类的Class对象
         * public Class<?> loadClass(String name) throws ClassNotFoundException {
         *         return loadClass(name, false);
         *     }
         */
        Class<?> aClass = Class.forName("javalh.chapter23.com.hspedu.Cat");

        //3.对于某个类的Class类对象,内存中只能有一份,因为类只能加载一次
        Class<?> aClass1 = Class.forName("javalh.chapter23.com.hspedu.Cat");
        System.out.println(aClass.hashCode());
        System.out.println(aClass1.hashCode());

        //一个类只会加载一次
        Class<?> aClass2 = Class.forName("javalh.chapter23.com.hspedu.Dog");
        System.out.println(aClass2.hashCode());


    }
}
