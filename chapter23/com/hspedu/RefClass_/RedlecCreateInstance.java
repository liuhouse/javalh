package javalh.chapter23.com.hspedu.RefClass_;

import java.lang.reflect.Constructor;

/**
 * @author 刘皓
 * @version 1.0
 * 通过反射创建对象
 */
public class RedlecCreateInstance{
    //1.方式1:调用类中的public修饰的无参构造器
    //2.方式二：调用类中的指定构造器

    //Class类相关方法
    //.newInstance : 调用类中的无参构造器,获取对应类的对象
    //.getConstructor(Class...clazz):根据参数列表,获取public的构造器对象
    //.getDecalaredConstructor(Class...clazz):根据参数列表,获取对应的所有构造器对象

    //4.Constructor类相关方法
    //setAccessible : 爆破
    //newInstance(Object...obj):调用构造器

    //测试1：通过反射创建某类的对象,要求该类中必须有public的无参构造器
    //测试2：通过调用某个特定的构造器方式,实现创建某类的对象

    @SuppressWarnings({"all"})
    public static void main(String[] args) throws Exception{
        //1.先获取到User类的Class对象
        Class<?> userClass = Class.forName("javalh.chapter23.com.hspedu.RefClass_.User");

        //2.通过public的无参构造器创建实例
        Object o = userClass.newInstance();
        System.out.println(o);

        //3.通过public的有参构造器创建实例
        /*
            constructor对象就是
            //只有一个字符串参数的构造器
            public User(String name){
                this.name = name;
            }
         */

        //3.1 先得到对应的构造器
        Constructor<?> constructor = userClass.getConstructor(String.class);
        //3.2 创建实例,并传入实参
        Object hsp = constructor.newInstance("hsp");
        System.out.println("hsp=" + hsp);

        //4.通过非public的有参构造器创建实例
        //4.1 得到private的构造器对象
        //得到有两个参数的构造器,第一个参数是Int类型  , 第二个类型是String类型
        Constructor<?> declaredConstructor = userClass.getDeclaredConstructor(int.class, String.class);
        System.out.println(declaredConstructor);

        //4.2 创建实例
        //因为此构造方法是private的   所以不能直接创建对象实例
        //爆破 【暴力破解】 , 使用反射可以访问private构造器/方法/属性,在反射面前,都是纸老虎
        declaredConstructor.setAccessible(true);//代表开启爆破
        Object user2 = declaredConstructor.newInstance(100, "张三丰");
        System.out.println("user2=" + user2);



    }




}


//User类
class User{
    private int age = 10;
    private String name = "韩顺平教育";

    //无参构造器
    public User(){}

    //public的有参构造器
    public User(String name){
        this.name = name;
    }

    //private的有参构造器
    private User(int age , String name){
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}

