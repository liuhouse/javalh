package javalh.chapter23.com.hspedu.RefClass_;

import java.lang.reflect.Field;

/**
 * @author 刘皓
 * @version 1.0
 * 通过反射访问类中的成员
 */
public class ReflecAccessProperty {
    @SuppressWarnings({"all"})
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        //访问属性
        //1.根据属性名获取Field对象
        //Field f = clazz对象.getDeclaredField(属性名);

        //2.爆破：f.setAccessible(true);//f是Field

        //3.访问
        //f.set(0,值)//o表示对象
        //f.get(o);//o表示对象

        //4.注意：如果是静态属性,则set和get中的参数o，可以写成null

        //1.得到Student类对应的Class对象
        Class<?> stuClass = Class.forName("javalh.chapter23.com.hspedu.RefClass_.Student");
        System.out.println(stuClass);
        //创建对象
        Object o = stuClass.newInstance();
        System.out.println(o.getClass());//o的运行类型就是Student

        //3.使用反射得到age的属性
        Field age = stuClass.getField("age");
        //给属性设置值
        age.set(o,100);
        System.out.println(age.get(o));//返回age属性的值

        //使用反射操作name属性
        Field name = stuClass.getDeclaredField("name");
        System.out.println(name);
        //设置name的属性值
        //因为name是私有的属性,所以不能直接设置,也不能直接访问,需要使用爆破
        //对name进行爆破,可以操作private属性
        name.setAccessible(true);
        name.set(null,"hsp");//因为name是static属性,因此o也可以写成null
        //读取name的属性值
//        System.out.println(name.get(o));
        System.out.println(name.get(null));











    }
}


//学生类
class Student{
    public int age;
    private static String name;

    //无参构造器
    public Student(){}

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                '}';
    }
}
