package javalh.chapter23.com.hspedu.RefClass_;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 刘皓
 * @version 1.0
 * 访问方法
 */
public class ReflecAccessMethod {
    @SuppressWarnings({"all"})
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        //1.根据方法名和参数列表获取Method方法对象 : Method m = clazz.getDeclaredMethod(方法名,XXX.class)//得到本类的所有方法
        //2.获取对象 : Object o = clazz.newInstance()
        //3.爆破:m.setAccessible(true);
        //4.访问:Object returnValue = m.invoke(o,实参列表);//o就是对象
        //5.注意:如果是静态方法,则invoke的参数o，可以写成null

        //演示
        // 1 . 得到Boss类对应的Class对象
        Class<?> bossCls = Class.forName("javalh.chapter23.com.hspedu.RefClass_.Boss");
        // 2 . 创建对象
        Object o = bossCls.newInstance();
        // 3 . 调用public的hi方法
        //Method hi = bossCls.getMethod("hi", String.class);
        //3.2 . 得到hi方法对象
        Method hi = bossCls.getDeclaredMethod("hi", String.class);
        // 3.1 调用 - 因为此方法是有一个字符串类型的参数的,所以在调用的时候也必须传递一个String类型的参数
        hi.invoke(o,"hsp~~~");


        //调用private static 方法
        //调用say方法 , 请记得,因为是私有方法所以必须使用getDeclaredMethod方法来调用
        //并且say方法有三个参数,所以必须要传递三个参数,证明你确实是要调用此方法
        Method say = bossCls.getDeclaredMethod("say", int.class, String.class, char.class);
        //因为say方法是private,所以需要爆破,原理和前面讲的构造器和属性一样
        say.setAccessible(true);
        System.out.println(say.invoke(o,10,"奥利给",'男'));

        //4.3因为say方法是static的,还可以这样调用,可以传入null
        System.out.println(say.invoke(null , 20 , "奥给给",'女'));

        //5.在反射中,如果方法有返回值,统一返回Object , 但是他运行类型和方法定义的类型返回类型一致
        Object relVal = say.invoke(null, 200, "李白", '女');
        System.out.println("reVal的运行类型=" + relVal.getClass());//String

        //在演示一个返回的案例
        Method m1 = bossCls.getDeclaredMethod("m1");
        //在反射中,所有的返回类型都是Object ，但是运行类型是在定义方法的时候返回的类型
        Object invoke = m1.invoke(o);
        System.out.println(invoke.getClass());//Monster

    }
}

class Monster{}
//Boss类
class Boss{
    public int age;
    private static String name;

    //构造器
    public Boss(){}

    public Monster m1(){
        return new Monster();
    }

    //静态方法
    private static String say(int n , String s ,char c){
        return n + " " + s + " " + c;
    }

    //普通的public方法
    public void hi(String s){
        System.out.println("hi" + s);
    }

}
