package javalh.chapter23.com.hspedu.RefClass_;

import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author 刘皓
 * @version 1.0
 */
public class ReflectionUtils_ {
    public static void main(String[] args) {

    }

    //第一组方法api
    @Test
    public void api_01() throws Exception {
        //测试反射的api
        //得到Class对象
        Class<?> personCls = Class.forName("javalh.chapter23.com.hspedu.RefClass_.Person");
        System.out.println(personCls);//class javalh.chapter23.com.hspedu.RefClass_.Person
        //getName:获取全类名
//        System.out.println(personCls.getName());//javalh.chapter23.com.hspedu.RefClass_.Person
        //getSimpleName : 获取简单类名
        System.out.println(personCls.getSimpleName());//Person
        //getFiles:获取所有public修饰的属性,包括本类以及父类的
        Field[] fields = personCls.getFields();
        //使用增强for循环读取数据
        for (Field field : fields) {
            System.out.println("本类以及父类的属性=" + field.getName());
        }

        //getDeclaredFields:获取本类的所有属性
        Field[] declaredFields = personCls.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            System.out.println("本类中所有属性" + declaredField.getName());
        }

        //getMethods:获取所有public修饰的方法,包含本类以及父类的
        Method[] methods = personCls.getMethods();
        for (Method method : methods) {
            System.out.println("本类中所有的方法=" + method.getName());
        }

        //getDeclaredMethods:获取本类中所有方法
        Method[] declaredMethods = personCls.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println("本类中的所有方法====" + declaredMethod.getName());
        }

        //getConstructors:获取所有public修饰的构造器,包含本类
        Constructor<?>[] constructors = personCls.getConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println("本类的构造器=" + constructor.getName());
        }

        //获取所有本类的构造器
        Constructor<?>[] declaredConstructors = personCls.getDeclaredConstructors();
        for (Constructor<?> declaredConstructor : declaredConstructors) {
            System.out.println("本类所有的构造器 = " + declaredConstructor);
        }

        //getPackage:以Package的形式返回 包信息
        System.out.println(personCls.getPackage());

        //getSuperClass"以Class的形式返回父类信息
        Class<?> superclass = personCls.getSuperclass();
        System.out.println("父类的class对象=" + superclass);

        //getInterfaces:以Class[]的形式返回接口信息
        Class<?>[] interfaces = personCls.getInterfaces();
        for (Class<?> anInterface : interfaces) {
            System.out.println("接口信息 = " + anInterface);
        }

        //getAnnotations:以Annotations[]形式返回注解信息
        Annotation[] annotations = personCls.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("注解信息=" + annotation);
        }
    }






    @Test
    public void api_02() throws Exception {
        //得到Class对象
        Class<?> personCls = Class.forName("javalh.chapter23.com.hspedu.RefClass_.Person");
        //getDeclaredFields:获取本类中的所有属性
        //规定：说明：默认修饰符是0，public : 1 , private :2 , protected : 4 static :8  final:16
        Field[] declaredFields = personCls.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            System.out.println("本类中所有的属性=" + declaredField.getName()
            + " 该属性的修饰符的值 = " + declaredField.getModifiers()
                    + "该属性的类型 = " + declaredField.getType()
            );
        }

        System.out.println("===============================================================");
        //getDeclaredMethods:获取本类的所有方法
        Method[] declaredMethods = personCls.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println("本类中所有的方法=" + declaredMethod.getName()
            + "该方法的访问修饰符的值 = " + declaredMethod.getModifiers()
                    + "该方法的返回类型" + declaredMethod.getReturnType()
            );

            //输出这个方法的形参数组情况
            Class<?>[] parameterTypes = declaredMethod.getParameterTypes();
            for (Class<?> parameterType : parameterTypes) {
                System.out.println("该方法的形参类型" + parameterType);
            }
        }


        System.out.println("====================================================");

        //getDeclaredConstructors:获取本类中所有的构造器
        Constructor<?>[] declaredConstructors = personCls.getDeclaredConstructors();
        for (Constructor<?> declaredConstructor : declaredConstructors) {
            System.out.println("=============");
            System.out.println("本类中所有的构造器=" + declaredConstructor.getName());
            //获取各个构造器的参数
            Class<?>[] parameterTypes = declaredConstructor.getParameterTypes();
            for (Class<?> parameterType : parameterTypes) {
                System.out.println("该构造器的形参类型=" + parameterType);
            }

        }
    }
}


//父类
class A{
    //父类的属性
    public String hobby;

    //父类的方法
    public void hi(){}

    //父类的构造器
    public A(){}

    //父类的有参构造器
    public A(String name){}
}

//接口A
interface IA{}

//接口B
interface IB{}

//子类
@Deprecated
class Person extends A implements IA,IB{
    //属性
    public String name;
    protected static int age;//4+8=12
    String job;
    private double sal;

    //构造器
    public Person(){}

    //有参构造器
    public Person(String name){}

    //私有的
    private Person(String name , int age){}

    //公共方法
    public void m1(String name , int age , double sal){

    }

    //受保护的方法
    protected String m2(){
        return null;
    }

    //默认访问权限的方法
    void m3(){}

    //私有的方法
    private void m4(){}
}
