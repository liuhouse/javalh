package javalh.chapter23.com.hspedu.homework;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01 {
    @SuppressWarnings({"all"})
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        //创建PrivateTest的类
        Class<?> aClass = Class.forName("javalh.chapter23.com.hspedu.homework.PrivateTest");
        Object o = aClass.newInstance();

        //这样也可以获取到PrivateTest 的 Class对象
//        Class<PrivateTest> privateTestClass = PrivateTest.class;
//        privateTestClass.getDeclaredField("name");
        //得到name的私有属性
        Field name = aClass.getDeclaredField("name");
        //因为name是私有属性,所以不能直接修改,需要使用爆破
        name.setAccessible(true);
        name.set(o,"多啦A梦");
        //打印name的属性值
        //先通过类获取方法
        Method getName = aClass.getMethod("getName");
        //调用方法获取对应的属性
        //调用name的属性,必须要通过类的实例化对象类调用,所以需要获取类的实例化对象  aClass.newInstance()
        System.out.println(getName.invoke(o));
    }
}

class PrivateTest{
    private String name = "hellokitty";
    public String getName(){
        return name;
    }
}
