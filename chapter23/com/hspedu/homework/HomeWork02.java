package javalh.chapter23.com.hspedu.homework;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork02 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //利用Class类的forName方法得到File类的class对象
        Class<?> fileClass = Class.forName("java.io.File");
        //打印所有的File类的构造器
        Constructor<?>[] declaredConstructors = fileClass.getDeclaredConstructors();
        for (Constructor<?> declaredConstructor : declaredConstructors) {
            System.out.println(declaredConstructor);
        }
        System.out.println("===============");
        //得到只有一个字符串参数的的构造器
        //这边指定了一个字符串类型的参数
        Constructor<?> constructor = fileClass.getConstructor(String.class);
        System.out.println(constructor);
        //创建文件的实例化对象
        //这边在创建文件的实例化对象的时候,就要传入实际的参数,这个时候已经创建成功了一个有参构造器
        Object fileObj = constructor.newInstance("e:\\myAAA111.txt");
//        //打印文件对象里面的所有方法
        System.out.println("===============");
//        Method[] declaredMethods = fileClass.getDeclaredMethods();
//        for (Method declaredMethod : declaredMethods) {
//            System.out.println(declaredMethod);
//        }

        //获取到创建文件的方法  createNewFile()
        Method createNewFile = fileClass.getMethod("createNewFile");
        //然后使用文件对象调用方法即可
        createNewFile.invoke(fileObj);
    }
}
