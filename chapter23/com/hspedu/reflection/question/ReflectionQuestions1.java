package javalh.chapter23.com.hspedu.reflection.question;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings({"all"})
public class ReflectionQuestions1 {
    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        //使用反射实例化调用方法
        //1.使用Properties对象读取配置文件
        //2.使用反射相关的方法实现功能

        //读取配置信息
        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter23\\com\\hspedu\\re.properties"));
        String classfullpath = properties.get("classfullpath").toString();
        String methodName = properties.get("method").toString();

        //加载类
        Class aClass = Class.forName(classfullpath);
        //得到类的实例化对象
        Object o = aClass.newInstance();
        //得到类中需要调用的方法  这里的方法  在这里也称为对象    在编程中 一切皆对象
        Method method = aClass.getMethod(methodName);

        //进行调用即可
        //方法.invoke(对象)
        method.invoke(o);


    }
}
