package javalh.chapter23.com.hspedu.reflection.question;

import javalh.chapter23.com.hspedu.Cat;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 * 反射问题的引入
 */
@SuppressWarnings({"all"})
public class ReflectionQuestion {
    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        //根据配置文件 re.properties指定信息,创建Cat对象并调用方法

        //传统的方式 new 对象 -> 调用方法
//        Cat cat = new Cat();
//        cat.hi();

        //我们尝试着做一下 -> 明白反射

        //1.使用Properties类,可以读写配置文件
        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter23\\com\\hspedu\\re.properties"));
        String classfullpath = properties.get("classfullpath").toString();
        String methodName = properties.get("method").toString();
        System.out.println("classfullpath = " + classfullpath);
        System.out.println("method = " + methodName);

        //使用这种方式创建对象是行不通的   因为 classfullpath 返回的虽然和实例化类的路径是一样的  但是这个所谓的路径只是字符串
        //所以实例化字符串来创建对象是行不通的
        //new classfullpath().method();

        /**
         * 解决的问题
         * 请看下面的问题
         * 1.根据配置文件re.properties指定信息,创建Cat对象并调用方法
         * classfullpath=com.hspedu.Cat
         * 使用现在的技术是做不到的
         * 2.这样的需求在学习框架的时候特别多,即通过外部文件配置,在不修改源码的情况下来控制程序,也符合设计模式ocp原则
         * (开闭原则：不修改源码：扩展功能)
         *
         */


        //使用反射机制解决
        //(1)加载类，返回Class类型的对象 cls
        Class cls = Class.forName(classfullpath);
        //(2)通过cls得到你要加载的类, javalh.chapter23.com.hspedu.Cat的对象实例
        Object o = cls.newInstance();
        System.out.println("o的运行类型=" + o.getClass());//运行类型 Cat
        //(3)通过cls得到你要加载的类 com.hspedu.Cat 的 methodName "hi" 的方法对象
        //即在反射中,可以把方法视为对象(万物皆对象)
        Method method = cls.getMethod(methodName);
        //(4)通过method调用方法,即通过方法对象来实现调用方法
        System.out.println("===========================");
        method.invoke(o);//传统方法 对象.方法() , 反射机制 方法.invoke(对象)

    }
}
