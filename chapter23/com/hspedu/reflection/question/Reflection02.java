package javalh.chapter23.com.hspedu.reflection.question;

import javalh.chapter23.com.hspedu.Cat;

import java.io.FileInputStream;
import java.lang.reflect.Method;

/**
 * @author 刘皓
 * @version 1.0
 * 反射的优点和缺点
 */
public class Reflection02 {
    public static void main(String[] args) throws Exception {
        /**
         * 反射的优点和缺点
         * 1.优点：可以动态的创建和使用对象(也是框架底层核心),使用灵活,没有反射机制,框架技术也就失去了底层支撑
         * 2.缺点：使用反射的基本是解释执行,对执行速度有影响
         */

        //Field
        //Method
        //Constructor
        m1();
        m2();
        m3();

    }

    //使用传统的方法来调用hi
    public static void m1(){
        Cat cat = new Cat();
        long start = System.currentTimeMillis();
        for(int i = 0 ; i < 900000000 ; i++){
            cat.hi();

        }
        long end = System.currentTimeMillis();
        System.out.println("m1()耗时=" + (end - start));
    }

    //反射机制调用方法hi
    //缺点体现的很明显,因为是解释执行,所以非常的耗时
    public static void m2() throws Exception {
        Class aClass = Class.forName("javalh.chapter23.com.hspedu.Cat");
        Object o = aClass.newInstance();
        Method hi = aClass.getMethod("hi");
        long start = System.currentTimeMillis();
        for(int i = 0 ; i < 900000000 ; i++){
            hi.invoke(o);
        }
        long end = System.currentTimeMillis();
        System.out.println("m2()耗时=" + (end - start));
    }


    //反射优化 + 关闭访问检查
    //关闭访问检查效率可以提升一倍

    /**
     * 反射调用优化,关闭访问检查
     * 1.Method和Field,Constructor对象都有setAccessible()方法
     * 2.setAccessible作用是启动和禁用安全检查的开关
     * 3.参数值为true表示反射对象在使用的时候取消访问检查,提高反射的效率,参数值为false的时候表示反射的对象执行访问检查
     * @throws Exception
     */
    public static void m3() throws Exception {
        Class aClass = Class.forName("javalh.chapter23.com.hspedu.Cat");
        //获取类的实例化对象
        Object o = aClass.newInstance();
        //获取方法对象
        Method hi = aClass.getMethod("hi");
        hi.setAccessible(true);//在反射调用方法时,取消访问检查
        long start = System.currentTimeMillis();
        for(int i = 0 ; i < 900000000 ; i++){
            hi.invoke(o);
        }
        long end = System.currentTimeMillis();
        System.out.println("m3()耗时 = " + (end - start));
    }


}
