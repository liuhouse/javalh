package javalh.chapter23.com.hspedu.reflection.question;

import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 * 反射的使用
 */
@SuppressWarnings({"all"})
public class Reflection01 {
    public static void main(String[] args) throws Exception {
        /**
         * java的反射机制可以完成
         * 1.在运行时判断任意一个对象所属的类
         * 2.在运行时构造任意一个类的对象
         * 3.在运行时得到任意一个类所具有的成员变量和方法
         * 4.在运行时调用任意一个对象的成员变量和方法
         * 5.生成动态代理
         *
         * 反射相关的主要类
         * 1.java.lang.Class:代表一个类,Class对象表示某个类加载后在堆中的对象
         * 2.java.lang.reflect.Method:代表类的方法,Method对象表示某个类的方法
         * 3.java.lang.reflect.Field:代表类的成员变量,Field对象表示某个类的成员变量
         * 4.java.lang.reflect.Constructor:代表类的构造方法,Constructor对象表示构造器吗+
         *
         * 这些类在 java.lang.reflection
         */

        //1:使用Properties类,可以读写配置文件
        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter23\\com\\hspedu\\re.properties"));
        String classfullpath = properties.get("classfullpath").toString();
        String methodName = properties.get("method").toString();

        //使用反射机制解决问题
        //(1):加载类,返回Class类型的对象cls
        Class aClass = Class.forName(classfullpath);
        //(2):获取类的对象实例
        Object o = aClass.newInstance();
        System.out.println("o的运行类型是" + o.getClass());
        //(3)得到method对象
        Method method = aClass.getMethod(methodName);
        //传统方法 ， 对象.方法()  反射机制 方法.invoke(对象)
        method.invoke(o);

        //java.lang.reflect.Field:代表类的成员变量,Field对象表示某个类的成员变量
        //得到name字段
        //getField 不能得到私有属性
        //Field nameField = aClass.getField("name");
        //获取Cat类的age字段
        Field ageField = aClass.getField("age");
        System.out.println(ageField);
        //获取age的属性的值
        //传统方法  对象.成员变量  反射 : 成员变量对象.get(对象)
        System.out.println(ageField.get(o));

        //java.lang.reflect.Constructor : 代表类的构造方法,Constructor对象表示构造器
        //获取当前类的构造方法
        //()中可以指定构造器的参数类型,返回无参构造器
        Constructor constructor = aClass.getConstructor();
        System.out.println(constructor);


        //这里传入的String.class 就是 String类的Class对象
        Constructor constructor1 = aClass.getConstructor(String.class);
        System.out.println(constructor1);

    }
}
