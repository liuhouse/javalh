package javalh.chapter23.com.hspedu;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Car {
    public String brand = "宝马";//品牌
    public String color = "白色";//颜色
    public String price = "100000";//价格

    public void hi(){
        System.out.println('a');
    }


    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
