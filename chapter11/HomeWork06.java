package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork06 {
    public static void main(String[] args) {

        Person.is_rever = true;
        Person t1 = new Person("唐僧", useModel());
        t1.getVehicles().work();
    }

    //根据是否遇到河流判断使用哪种交通工具
    public static Vehicles useModel(){
        Vehicles horse = vehicleCon.getHorse();
        Vehicles boat = vehicleCon.getBoat();
        Vehicles plane = vehicleCon.getPlane();
        return plane;
        //如果遇到大河的时候，就使用轮船
//        return !!Person.is_rever ? boat : horse;
    }
}



//交通工具接口
interface Vehicles{
    void work();
}

//马类
class Horse implements Vehicles{

    @Override
    public void work() {
        System.out.println("以马类作为交通工具...");
    }
}


//船类
class Boat implements Vehicles{

    @Override
    public void work() {
        System.out.println("以轮船作为交通工具...");
    }
}


//飞机类
class Plane implements Vehicles{

    @Override
    public void work() {
        System.out.println("以飞机做为交通工具...");
    }
}

//交通工具工厂类
class vehicleCon{
    //获得交通工具
    public static Horse getHorse(){
        return new Horse();
    }

    //获得轮船类
    public static Boat getBoat(){
        return new Boat();
    }

    public static Plane getPlane(){
        return new Plane();
    }
}


class Person{
    private String name;
    private Vehicles vehicles_;
    public static boolean is_rever = false;

    public Person(String name, Vehicles vehicles_) {
        this.name = name;
        this.vehicles_ = vehicles_;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vehicles getVehicles() {
        return vehicles_;
    }

    public void setVehicles(Vehicles vehicles) {
        this.vehicles_ = vehicles;
    }
}