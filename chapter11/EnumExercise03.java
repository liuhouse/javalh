package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class EnumExercise03 {
    public static void main(String[] args) {
        Music.CLASS_MUSIC.playing();
    }
}

class A{

}

//1:使用enum关键字后,就不能继承其他类了,因为enum会隐式的继承Enum，而java是单继承机制
//enum Season3 extends A{
//
//}


//接口
interface IPlaying{
     void playing();
}

//2:enum 实现的枚举类,仍然是一个类,所以还是可以实现接口的
enum Music implements IPlaying{
    CLASS_MUSIC;

    @Override
    public void playing() {
        System.out.println("播放好听的音乐...");
    }
}
