package javalh.chapter11.HomeWork06_;

import javalh.houserent.domain.House;

/**
 * @author 刘皓
 * @version 1.0
 */


/*
* 有Person类,有name和Vehicles属性,在构造器中为两个属性赋值
* */

public class Person {
    //名字属性
    private String name;
    //交通工具属性
    private Vehicles vehicles;

    //使用构造器进行初始化赋值
    //在创建对象的时候,事先分配一个交通工具
    public Person(String name , Vehicles vehicles){
        this.name = name;
        this.vehicles = vehicles;
    }

    /*
    * 过河的情况
    * 使用轮船作为交通工具
    *
    * 实例化Person对象"唐僧",要求一般情况下使用Horse作为交通工具,遇到大河的时候使用Boot作为交通工具
    * 这里涉及到一个编程思路，就是可以把具体的需求,封装成方法 -》 这里就是编程思想
    * 思考一个问题,如何做到不浪费,在构建对象的时候,传入交通工具类
    * */
    public void passRiver(){
        /*
        * 先得到船
        * 判断一下,当前的vehicles属性是null，就获取一条船
        * Boat boat = VehiclesFactory.getBoat();
        * boat.work
        *
        * 如何防止始终使用的是传入的马 instanceOf
        *   vehicles instanceof Boat 是判断当前的vehicles是不是Boat
        *   (1)vehicles = null : vehicles instanceof Boat => false
        *   (2)vehicles = 马对象 : vehicles instanceof Boat => false
        *   (3)vehicles = 船对象 : vehicles instanceof Boat => true
        *
        *   if(vehicles == null || !(vehicles instanceof Boat)){
                vehicles = VehiclesFactory.getBoat();
            }
        *
        * */

        if(!(vehicles instanceof Boat)){
            vehicles = VehiclesFactory.getBoat();
        }

        vehicles.work();
    }

    /*
    * 普通情况
    * 使用马作为交通工具
    * */
    public void common(){
        //得到马儿
        //我们知道,唐僧西天取经,一直使用的是白龙马,但是这里每次在创建对象的时候都是一个新马,这样的业务逻辑不对
        //那么我们该如何解决的,使用单例模式
//        if(vehicles == null || !(vehicles instanceof Horse)){
//            vehicles = VehiclesFactory.getHorse();
//        }

        //如果传递过来的对象不属于House的类的话,就创建House类
        //因为我这边只允许使用House类的对象实例

        if(!(vehicles instanceof House)){
            vehicles = VehiclesFactory.getHorse();
        }
        //这里体现了接口调用
        vehicles.work();
    }


    public void fly(){
        if(!(vehicles instanceof Plane)){
            vehicles = VehiclesFactory.getPlane();
        }
        vehicles.work();
    }


}
