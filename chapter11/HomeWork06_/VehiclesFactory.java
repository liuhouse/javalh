package javalh.chapter11.HomeWork06_;


/**
 * @author 刘皓
 * @version 1.0
 */
//交通工具类
/*
* 创建交通工具工厂类,将两个方法分别获得交通工具Horse和Boat对象
* */
public class VehiclesFactory {
    //马儿始终是同一批
    //将马儿设计成静态的，在内存加载的时候就始终都是一匹马了,无论怎么改变,始终都是一匹马
    //单例模式  - 饿汉模式
    private static Horse horse = new Horse();

    private VehiclesFactory(){}

    //获取马儿 这里我们使用static   就不用实例化了  可以直接访问
    public static Horse getHorse(){
//        return new Horse();
        return horse;
    }

    //获取轮船
    public static Boat getBoat(){
        return new Boat();
    }

    //获取飞机
    public static Plane getPlane(){
        return new Plane();
    }
}
