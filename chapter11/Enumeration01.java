package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 * 枚举
 */
public class Enumeration01 {
    public static void main(String[] args) {
        /*
        * 先看一个需求
        * 要求创建季节Season对象,设计并完成
        * */

        /*
        * 因为对于季节而言,他的对象(具体值),是固定的四个,不会有更多
        * 按照老师的设计的思路,不能体现一年四季是个固定的四个对象
        * 因此,这样设计不好====>所以我们使用枚举类(枚:一个一个  举:举例，即把具体的对象一个一个列举出来的类,就成为枚举类)
        * */

        //使用
        Season spring = new Season("春天", "温暖");
        Season winter = new Season("冬天", "寒冷");
        Season summer = new Season("夏天", "炎热");
        Season autumn = new Season("秋天", "凉爽");

        //这样可以达到目的,但是需求是我们只要求一年有四季,而这样的情况可以有第五个季节,比如
        //这样我们是不允许的,我们希望一旦一年四季订下来,四季的描述也就订下来了,不应该再被改变
        //这里添加了第五个季节,所以是不合理的
        Season five = new Season("第五季", "不冷不热");
        //而且还能对季节的名称以及季节的描述进行修改,这是我们不允许的
        //举例

        autumn.setDesc("非常的热");

        System.out.println(autumn);

    }
}

//类
class Season{
    //季节名称
    private String name;
    //季节描述
    private String desc;

    public Season(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Season{" +
                "name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
