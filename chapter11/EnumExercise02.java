package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class EnumExercise02 {
    public static void main(String[] args) {
        /*
        * 需求：声明Week枚举类,其中包含星期一至星期日的定义：
        * MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
        * */
        Week[] values = Week.values();
        System.out.println("所有星期的信息如下");
        for (Week value:values){
            //System.out.println(value.getName());
            System.out.println(value);
        }
    }
}

enum Week{
    MONDAY("星期一"),
    TUESDAY("星期二"),
    WEDNESDAY("星期三"),
    THURSDAY("星期四"),
    FRIDAY("星期五"),
    SATURDAY("星期六"),
    SUNDAY("星期天")
    ;

    private String name;

    private Week(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return name;
    }
}
