package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork04 {
    public static void main(String[] args) {
        Cellphone cellphone = new Cellphone();
        cellphone.TestWork();
    }
}

//计算器类
interface Calculator{
     void work();
}

//手机类
class Cellphone{
    public void TestWork(){
        Calculator cal = new Calculator(){
            @Override
            public void work() {
                System.out.println("电话的计算器正在运算...");
            }
        };
        cal.work();
    }
}
