package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork02 {
    public static void main(String[] args) {

    }
}

class Frock{
    //序列号初始值
    private static int currentNum = 100000;
    //序列号属性
    private int serialNumber;

    public Frock() {
        serialNumber = getNextNum();
    }

    //生成上衣唯一序列号的方法
    public static int getNextNum(){
         currentNum += 100;
         return currentNum;
    }


    //获取序列号
    public int getSerialNumber() {
        return serialNumber;
    }
}

class TestFrock{
    public static void main(String[] args) {
        System.out.println(Frock.getNextNum());
        System.out.println(Frock.getNextNum());

        Frock frock = new Frock();
        Frock frock1 = new Frock();
        Frock frock2 = new Frock();
        System.out.println(frock.getSerialNumber());
        System.out.println(frock1.getSerialNumber());
        System.out.println(frock2.getSerialNumber());


    }
}
