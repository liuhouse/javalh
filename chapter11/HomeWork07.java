package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork07 {
    public static void main(String[] args) {
        Car_ car_ = new Car_(20);
        car_.mshow();
//        Car_.Air_ air_ = car_.new Air_();
//        air_.flow();

    }
}

class Car_{
    //温度属性
    private double temperature = 30;

    public Car_(double temperature) {
        this.temperature = temperature;
    }

    //空调类
    class Air_{
        public void flow(){
            if(temperature > 40){
                System.out.println("吹冷气");
            }else if(temperature < 0){
                System.out.println("吹暖气");
            }else{
                System.out.println("关闭空调");
            }
        }
    }

    //第二种调用的方式
    public void mshow(){
        Air_ air_ = new Air_();
        air_.flow();
    }
}