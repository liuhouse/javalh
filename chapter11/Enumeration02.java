package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 * 自定义类实现枚举
 */
public class Enumeration02 {

    /*
    * 自定义枚举类的小结
    * (1)构造器私有化
    * (2)本类的内部创建一组对象(四个,春夏秋冬)
    * (3)对外暴露对象(通过为对象添加public static final修饰符)
    * (4)可以提供get方法,但是不要提供set方法
    * */

    public static void main(String[] args) {
        //使用,打印出春夏秋冬
        System.out.println(Season1.SPRING);
        System.out.println(Season1.SUMMER);
        System.out.println(Season1.AUTUMN);
        System.out.println(Season1.WINTER);
    }
}


class Season1{
    private String name;//名称
    private String desc;//描述

    //定义了四个对象,固定
    public static final Season1 SPRING = new Season1("春天","温暖");
    public static final Season1 WINTER = new Season1("冬天","寒冷");
    public static final Season1 AUTUMN = new Season1("秋天","凉爽");
    public static final Season1 SUMMER = new Season1("夏天","炎热");

    /*
    * 自定义枚举类
    * 1:将构造器私有化,防止new对象
    * 2:去掉setXXX方法,防止属性被修改
    * 3:在Sesson1对象中,直接创建固定的对象,必须是public,因为要提供给外部进行访问
    * 4:优化,可以加入final修饰符,因为访问的时候只是访问了此属性,不会加载后面的构造器
    * */

    private Season1(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "Season1{" +
                "name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}


