package javalh.chapter11;

public class HomeWork08 {
    public static void main(String[] args) {
        //这边是一个枚举对象
        Color myColor = Color.valueOf("GREEN");
        switch (myColor){
            case RED:
                Color.RED.show();
                break;
            case BLUE:
                Color.BLUE.show();
                break;
            case BLACK:
                Color.BLACK.show();
                break;
            case YELLOW:
                Color.YELLOW.show();
                break;
            case GREEN:
                Color.GREEN.show();
                break;

        }
    }

}

enum Color implements myColor{
    RED(255,0,0),
    BLUE(0,0,255),
    BLACK(0,0,0),
    YELLOW(255,255,0),
    GREEN(0,255,0)
    ;

    private int redValue;
    private int greenValue;
    private int blueValue;

    Color(int redValue, int greenValue, int blueValue) {
        this.redValue = redValue;
        this.greenValue = greenValue;
        this.blueValue = blueValue;
    }

    @Override
    public void show() {
        System.out.println("red :" + redValue + " green :" + greenValue + "blue : " + blueValue );
    }
}

interface myColor{
    void show();
}
