package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 *
 * enum关键字实现枚举
 */
public class EnumExercise01 {
    public static void main(String[] args) {
        Gender boy = Gender.BOY;//ok的   BOY枚举对象属于Gender枚举类 所以是可以使用Gender类接收的
        Gender boy1 = Gender.BOY;//ok的  BOY枚举对象属于Gender枚举类,所以是可以使用Gender类接收的
        //返回此枚举变量的名称
        System.out.println(boy);//BOY   本质就是调用Gender类的父类Enum的toString()
        System.out.println(boy == boy1);//true    因为枚举常量是静态的   在内存中指向同一个地址
    }
}
enum Gender{
    //这里其实就是调用了Gender类的无参构造器
    BOY,GIRL;

    //如果这里的有参构造器被定义,那么无参构造器就不存在了,所以上面的定义的枚举对象就会报错,如果想不报错,那么需要将无参构造器显示的写一遍
    private Gender(String name){

    }

    private Gender(){}
}

//(1)上面的语法是ok的
//(2)有一个Gender类,没有属性
//(3)有两个枚举对象BOY,GIRL,使用的是无参构造器创建

