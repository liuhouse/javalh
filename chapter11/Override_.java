package javalh.chapter11;

//@Override注解的案例
public class Override_ {

}


class Father{
    public void fly(){
        System.out.println("Father fly....");
    }
}
//@Override：限定某个方法,是重写父类方法,该注解只能用于方法
class Son extends Father{

    /*
    * 老韩解读
    * 1：@Override注解放在fly方法上,表示子类的fly()方法重写了父类的fly
    * 2：如果这里没有写@Override,还是重写了父类的fly
    * 3:如果你写了@Override注解,编译器就会去检查该方法是否真的重写了父类的方法.如果的确重写了,则编译通过,
    *   如果没有构成重写,则编译错误
    * 4:看看@Override的定义  解读  @interface 表示的是一个注解类
    * */


    /*
    * public @interface Override
    * */
    @Override//说明  代表重写父类的方法
    public void fly(){

    }


    public void say(){

    }
}