package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork04_1 {
    public static void main(String[] args) {
        new CellPhones().testWork();
    }
}

//计算器接口
interface Cal{
    void work();
}

class CellPhones{
    public void testWork(){
        new Cal(){
            @Override
            public void work() {
                System.out.println("手机上的计算器正在计算...");
            }
        }.work();
    }
}
