package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Deprecated_ {
    public static void main(String[] args) {
        /*
        * @Deprecated : 用于表示某个程序的元素(类,方法已过时)
        *
        *
        *   @Documented
            @Retention(RetentionPolicy.RUNTIME)
            @Target(value={CONSTRUCTOR, FIELD, LOCAL_VARIABLE, METHOD, PACKAGE, MODULE, PARAMETER, TYPE})
            public @interface Deprecated {
        * */
        AA aa = new AA();
        aa.hi();
        System.out.println(aa.n1);
    }
}

/*
* 老韩解读
* (1)@Deprecated 修饰某个元素,表示该元素已经过时
* (2)即不在推荐使用,但是仍然可以使用
* (3)查看@Deprecated注解类的原码
* (4)可以修饰方法,类,字段,包,参数,等等
* (5)@Deprecated可以做版本升级过度的使用
* */

@Deprecated
class AA{

    @Deprecated
    public int n1 = 10;

    @Deprecated
    public void hi(){

    }
}