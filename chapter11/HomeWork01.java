package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01 {
    public static void main(String[] args) {
        Car car = new Car();
        Car car1 = new Car(100);
        System.out.println(car);//9.0 red
        System.out.println(car1);//100.0 red
    }
}


class Car{
    double price = 10;
    static String color = "white";

    @Override
    public String toString() {
        return price + color;
    }

    //无参构造器
    public Car() {
        this.price = 9;
        this.color = "red";
    }

    //有参构造器
    public Car(double price) {
        this.price = price;
    }
}
