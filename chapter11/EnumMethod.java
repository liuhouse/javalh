package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 * 演示Enum类各种方法的使用
 */
public class EnumMethod {
    public static void main(String[] args) {


        /*
        * (1)toString:Enum类已经重写了,返回的是当前对象名称,子类可以重写该方法,用于返回对象的属性信息
        * (2)name:返回的是当前的对象名(常量名),子类中不能重写
        * (3)ordinal:返回的是当前对象的位置号,默认从0开始
        * (4)values:返回当前枚举对象中所有的常量
        * (5)valueOf:将字符串转换成枚举对象,要求字符串必须为已有的常量名,否则报异常
        * (6)compareTo:比较两个枚举常量,比较的就是编号
        * */


        //使用Season2枚举类,来演示各种方法
        //同包类下面的类是可以直接调用的
        Season2 autumn = Season2.AUTUMN;

        //输出枚举对象的名字
        System.out.println(autumn.name());
        //ordinal() 输出的是该枚举对象的次序/编号,从0开始编号
        //因为AUTUMN是第三个,所以下标为2
        System.out.println(autumn.ordinal());//2

        //从反编译可以看出values()方法,返回Season2[]
        //含有定义所有枚举对象
        Season2[] values = Season2.values();
        System.out.println("====比那里取出枚举对象(增强for)========");
        for(Season2 season:values){
            System.out.println(season);
        }

        //valueOf:将字符串转成枚举对象,要求字符串必须为已有的常量名,否则报异常
        //valueOf:将字符串转换成枚举对象,要求字符串必须为已有的常量名称,否则报异常
        //执行流程
        //1:根据你输入的AUTUMN到Season2枚举对象中去查找
        //2:如果找到了,就返回,如果没有找到,就报错
        Season2 autumn1 = Season2.valueOf("AUTUMN");
        System.out.println(autumn1);

        System.out.println(autumn == autumn1);

        /*
        * 老韩解读
        * 1:就是把Season2.AUTUMN枚举对象的编号和Season2.SUMMER枚举对象的编号比较
        * 2:看看结果
        *
        * public final int compareTo(E o){
        *      return self.ordinal - other.ordinal;
        * }
        * Season2.AUTUMN的编号[2] - Season2.SUMMER的编号[3]
        * */

        //compareTo:比较两个枚举常量,比较的就是编号
        System.out.println(Season2.AUTUMN.compareTo(Season2.SUMMER));

        System.out.println("====================");

        int nums[] = {1,2,3};
        System.out.println("=======普通的for循环========");
        for(int i = 0 ; i < nums.length ; i++){
            System.out.println(nums[i]);
        }

        System.out.println("=======增强for循环=======");
        //执行流程是依次从nums数组中取出数据,赋给i，如果取出完毕,则退出for
        for(int i:nums){
            System.out.println("i = " + i);
        }


    }
}
