package javalh.chapter11;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork04_2 {
    public static void main(String[] args) {
        CellPhone2 cellPhone2 = new CellPhone2();
        //计算相加的结果
        cellPhone2.testWork(new Cal1() {
            @Override
            public double work(double num1, double num2) {
                return num1 + num2;
            }
        },10,20);

        //计算相乘的结果
        cellPhone2.testWork(new Cal1() {
            @Override
            public double work(double num1, double num2) {
                return num1 * num2;
            }
        },50,50);

    }
}


interface Cal1{
     public double work(double num1,double num2);
}

class CellPhone2{
    public void testWork(Cal1 cal,double num1 , double num2){
        System.out.println("运算结果是 ： " + cal.work(num1,num2));
    }
}