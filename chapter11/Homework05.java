package javalh.chapter11;



/**
 * @author 刘皓
 * @version 1.0
 */
public class Homework05 {
    public static void main(String[] args) {
        A_ a_ = new A_("smith");
        a_.show();

    }
}

class A_ {

    private String name;

    public A_() {
    }

    public A_(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //局部内部类是要在方法中的
    public void show(){
        class B_ {
            private final String name;

            public B_(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }

            public void show() {
                System.out.println("B_.name " + name + "A_.name " + A_.this.name);
            }
        }

        B_ b_ = new B_("jack");
        b_.show();
    }



}
