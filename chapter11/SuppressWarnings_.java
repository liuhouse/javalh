package javalh.chapter11;


import java.util.ArrayList;

/**
 * @author 刘皓
 * @version 1.0
 */
/*
* 想把当前类中所有的警告信息都抑制住  可以写在类上
* */
//@SuppressWarnings({"all"})
public class SuppressWarnings_ {

    /*
    * 老韩解读
    * 1：当我们不希望看到这些警告的时候,可以使用@SuppressWarnings注解来抑制警告信息
    * 2:在{""}中可以写入你希望抑制(不显示的)警告信息
    * 3:可以指定的警告类型有
    *   all:抑制所有警告
    *   boxing:抑制封装/拆装作业相关的警告
    *   cast抑制与强制转换作业相关的警告
    *   dep-ann,抑制与淘汰注释相关的警告
    *   deprecation 抑制与描述相关的警告
    *   fallthrougth 抑制与陈述式中遗漏breack的相关警告
    *   finally，抑制未传回finally区块的相关的警告
    *   hiding:抑制与隐藏变数的区块相关的警告
    *   incomplate-swith,抑制与switch陈述式(enum case)中遗漏项目相关的警告
    *   javadoc,抑制与javadoc相关的警告
    *   nls，抑制与非nls子串文字相关的警告
    *   null,抑制与空值分析相关的警告
    *   rawtypes，抑制与raw类型相关的警告
    *   resource,抑制与使用Closeable类型的资源相关的警告
    *   serial,抑制与可序列化的类别遗漏serialVersionUID栏位相关的警告
    *   static-access 抑制与静态存取不正确的相关的警告
    *   static-method,抑制与可能宣告为static的方法相关的警告
    *   super,抑制与置换方法相关但是不含super呼叫的警告
    *   synthetic-access,抑制与内部类别的存取未最佳化相关的警告
    *   sync-override，抑制因为置换同步方法而遗漏的同步化的警告
    *   unchecked,抑制与未检查的作业相关的警告
    *   unqualified-filed-access,抑制与栏位存取不合格的相关警告
    *   unused,抑制与未用的程式码及停用的程式码相关的警告
    *
    * 4：关于@SupperessWarnings作用范围是和你存放的位置相关
    *   比如@SupperWarnings放置在main方法,那么抑制的警告范围就是main
    *   通常我们可以放在具体的语句,方法,类
    * */


    //可以使用精准的抑制警告信息
    //@SuppressWarnings({"rawtypes","unchecked","unused"})
    public static void main(String[] args) {
        /*
        * @SuppressWarnings:抑制编译器警告
        * */
        //单独语句的抑制
        @SuppressWarnings({"rawtypes"})
        ArrayList<String> list = new ArrayList<String>();
        list.add("jack");
        list.add("tom");
        list.add("mary");
        @SuppressWarnings({"unused"})
        int i;
        System.out.println(list.get(1));
    }

    //可以直接使用all全部进行抑制
   // @SuppressWarnings("all")
    public void f1(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("jack");
        list.add("tom");
        list.add("mary");

        int i;
        System.out.println(list.get(1));
    }
}
