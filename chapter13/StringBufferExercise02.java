package javalh.chapter13;

import java.util.Scanner;

public class StringBufferExercise02 {
    public static void main(String[] args) {
        /*
        * 需求：
        * 输入商品名称和商品价格,要求打印效果案例,使用前面学习的方法完成：
        * 商品名  ： 商品价格
        * 手机  123,456,789.56
        * 要求：价格的小数点前面的每三位用逗号隔开,在输出
        *
        *
        * 思路分析,
        * （1）使用scanner进行输入
        *   (2)可以将字符串转化成StringBuffer对象,
        * (3)使用insert在合适的位置然后插入 , 即可
        * */

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入数字字符串：");
        String str = scanner.next();
//        String str = "121212345.78";//12,345.78
        //将字符串转化成StringBuffer对象
        StringBuffer sb = new StringBuffer(str);
        //找到小数点的位置
        int index = sb.lastIndexOf(".");
//        //向前三位,然后插入 逗号  即可
//        sb.insert(index - 3,",");
//        System.out.println(sb);

        //上面只是完成了一个而已,但是如果数据多了就达不到效果了,所以这里我们需要使用for循环

        //初始化第一个逗号的位置
        index = index - 3;

        for(int i = index ; i > 0 ; i-=3){
            sb.insert(i,",");
        }
        System.out.println(sb);


    }
}
