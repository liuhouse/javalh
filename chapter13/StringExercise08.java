package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringExercise08 {
    public static void main(String[] args) {
        String a = "hello";//创建了a对象
        String b = "abc";//创建了b对象
        String c = a + b;//创建了几个对象?画出内存图

        System.out.println(c == ("hello" + "abc"));

        System.out.println(c.equals(("hello" + "abc")));

        //关键就是要分析String c = a + b 是如何执行的
        //一共是有三个对象的
        /*



        *老韩小结
        * 底层是StringBuilder sb = new StringBuilder();
        * sb.append(a);
        * sb.append(b);
        *
        * sb是在堆中的.并且append是在原来的字符串的基础上追加的
        *
        * 重要规则
        * String c1 = "ab" + "cd";常量相加,看的是常量池. String c1 = a + b; 变量相加,是放在堆中
        * 学习思路：一定要尽量的看源码学习
        * */
    }
}
