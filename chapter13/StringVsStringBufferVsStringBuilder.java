package javalh.chapter13;

public class StringVsStringBufferVsStringBuilder {
    public static void main(String[] args) {
        //测试 String , StringBuffer ,StringBuilder 的效率测试

        /*
        * 使用的原则，结论:
        * 1：如果字符串存在大量的修改操作,一般使用StringBuffer或者StringBuilder
        * 2:如果字符串存在大量的修改操作,并且是单线程的情况,使用StringBuilder
        * 3:如果字符串存在大量的修改操作,并且是多线程的情况,使用StringBuffer
        * 4:如果我们的字符串很少修改,被多个对象引用,使用String，比如配置信息
        * StringBuffer方法使用和StringBuilder一样  不再说
        * */


        long startTime = 0L;
        long endTime = 0L;
        StringBuffer Buffer = new StringBuffer("");

        //系统的当前时间
        //=============StringBuffer=========================================
        startTime = System.currentTimeMillis();
        for(int i = 0 ; i < 80000 ; i++){
            Buffer.append(String.valueOf(i));
        }
        endTime = System.currentTimeMillis();
        System.out.println("StringBuffer的执行时间：" + (endTime - startTime));

        //==============StringBuilder=======================================
        startTime = System.currentTimeMillis();
        StringBuilder Builder = new StringBuilder("");
        for(int i = 0 ; i < 80000 ; i++){
            Builder.append(String.valueOf(i));
        }
        endTime = System.currentTimeMillis();
        System.out.println("StringBuilder的执行时间" + (endTime - startTime));


        //==================String=============================
        String text = "";
        startTime = System.currentTimeMillis();
        for(int i = 0 ; i < 80000 ; i++){
            text += i;
        }
        endTime = System.currentTimeMillis();
        System.out.println("String的执行时间=" + (endTime - startTime));


    }
}
