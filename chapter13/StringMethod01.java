package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringMethod01 {
    public static void main(String[] args) {
        //1:equals前面已经讲过了,比较内容是否相同,区分大小写
        String str1 = "hello";
        String str2 = "hello";
        System.out.println(str1.equals(str2));

        //2:equalsIgnoreCase忽略大小写的判断内容是否相等
        String username = "johN";
        if("john".equalsIgnoreCase(username)){
            System.out.println("suceess");
        }else{
            System.out.println("Failure");
        }

        //3.length获取字符的个数,字符串的长度
        System.out.println("韩顺平".length());

        //4.indexOf 获取字符在字符串对象中第一次出现的索引,索引从0开始,如果找不到,就返回-1
        String s1 = "wer@terwe@g";
        int index = s1.indexOf('@');
        System.out.println(index);
        System.out.println("weIndex = " + s1.indexOf("1te"));

        //5:lastIndexOf获取字符在字符串中最后一次出现的索引,索引从0开始,如果找不到就返回-1
        s1 = "Wer@terwe@g@";
        index = s1.lastIndexOf('@');
        System.out.println(index);
        System.out.println("ter的位置=" + s1.lastIndexOf("ter"));

        //6.substring 截取指定范围的子串
        String name = "hello,张三";
        //下面 name.substring(6)从索引为6开始截取后面的内容
        System.out.println(name.substring(6));

        //name.substring(0,5)表示从索引0开始截取,截取到索引为5的位置,但是不包括索引为5 (实际上截取的索引为5-1=4)
        //包括左不包括右
        System.out.println(name.substring(2,5));

    }
}
