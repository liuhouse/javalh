package javalh.chapter13;

import java.util.Arrays;
import java.util.List;

public class ArraysMethod02 {
    public static void main(String[] args) {
        Integer[] arr = {1,2,90,123,567};
        //binarySearch通过二分搜索进行查找,并且要求必须排好
        /*
        * 老韩解读
        * 1:使用binarySearch进行二分查找
        * 2:要求该数组是有序的,如果该数组是无序的,不能使用binarySearch
        * 3:如果数组中不存在该元素,就返回 return -(low+1);
        * 负数就是代表没有找到元素
        * */
        int index = Arrays.binarySearch(arr,10);
        //System.out.println(index);

        //copyOf数组元素的复制
        //老韩解读
        //1:从arr数组中,拷贝arr.length个元素到newArr数组中
        //2:如果拷贝的长度>arr.length，就会在新数组的后面增加null
        //3.如果拷贝的长度<0就抛出异常，NegativeArraySizeException
        //4.该方法底层使用的是System.arraycopy()
        Integer[] newArr = Arrays.copyOf(arr,arr.length);
        //System.out.println("拷贝执行完毕之后==========");
        //System.out.println(Arrays.toString(newArr));

        //ill数组元素的填充
        Integer[] num = new Integer[]{9,3,2};
        //老韩解读
        //1:使用99去填充num数组,可以是替换原数组的元素
        Arrays.fill(num,99);
        //System.out.println(Arrays.toString(num));

        //equals比较两个数组元素的内容是否完全一致
        Integer[] arr2 = {1,2,90,123,567};
        //老韩解读
        //1:如果arr和arr2数组的元素一样,则返回true
        //2:如果不是完全一样,就返回false
        boolean equals = Arrays.equals(arr,arr2);
        //System.out.println("equals=" + equals);

        /*
        * asList将一组值,转化成list
        * 老韩解读：
        * 1：asList 方法,会将(2,3,4,5,9)数据转成一个List集合
        * 2：返回asList编译类型 List(接口)
        * 3:asList运行类型 java.util.Arrays#ArrayList，是Arrays类的静态内部类
        * */
        List asList = Arrays.asList(2,3,4,5,9);
        System.out.println("asList=" + asList);
        System.out.println("asList的运行类型是" + asList.getClass());
    }
}
