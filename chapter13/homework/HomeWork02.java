package javalh.chapter13.homework;

import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork02 {
    public static void main(String[] args) {
        /*
        * 需求
        * 输入用户名,密码,邮箱，如果信息录入正确,则提示注册成功,否则生成异常对象
        * (1)用户名的长度为2或者3或者4
        * (2)密码的长度为6,要求全是数字,isDigital
        * (3)邮箱中包含@和.，并且@在.的前面
        * */

        try {
            new User().register();
        }catch (RuntimeException e){
            System.out.println(e.getMessage());
        }

    }
}


class User{
    //姓名
    private String name;
    //密码
    private String password;
    //邮箱
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public void register(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入用户名[用户名的长度为2-4之间]:");
        name = scanner.next();
        //用户名的长度为2或者3或者4
        if(!(name.length() >= 2 && name.length() <= 4)){
            throw new RuntimeException("用户名的长度为2-4之间");
        }

        System.out.print("请输入密码[密码的长度为6,要求全部都是数字]:");
        password = scanner.next();

        //密码的长度为6,要求全部都是数字 isDigital
        if(!(password.length() == 6) || !is_num()){
            throw new RuntimeException("密码的长度为6,要求全部都是数字");
        }


        System.out.print("请输入邮箱");
        email = scanner.next();
        //邮箱中包含@和.并且@要在.的前面
        //1：查看邮箱中是不是包含@和.
        //2.判断索引的大小
        //1312797840@qq.com
        int i1 = email.indexOf("@");
        int i2 = email.indexOf(".");
        if(i1 == -1 || i2 == -1 || i1 > i2){
            throw new RuntimeException("邮箱格式错误");
        }

        System.out.println("注册成功" + this.toString());
    }


    //判断是不是数字
    private boolean is_num(){
        boolean is_num = true;
        int i = 0;
        while (i < password.length()){
            boolean digit = Character.isDigit(password.charAt(i));
            if(!digit){
                is_num = false;
                break;
            }
            i++;
        }
        return is_num;
    }

}
