package javalh.chapter13.homework;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01_1 {
    public static void main(String[] args) {
        /*
         * 需求
         * (1)将字符串中的指定部分进行反转,比如将"abcdef"反转成"aedcbf"
         * (2)编写方法 public static String reverse(String str, int start , int end)搞定
         * */
        String str = "abcdefg";
        String new_str = null;
        try {
            new_str = reverse("abcdefg",1,5);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println(new_str);
    }


    /*
    * (1)将字符串中指定部分进行反转,比如将 "abcdefg" 反转为 "afedcbg"
    * (2)编写方法 public static String reverse(String str , int start , int end)搞定
    * //编程思想是最重要的
    * (1)先把方法定义确定
    * (2)把String转成char[]，因为char[]元素是可以进行交换的
    * (3)画出分析示意图
    * (4)代码实现
    * */
    public static String reverse(String str , int start , int end){

        //对输入的参数做一个验证
        //老韩重要的编程技巧分享
        //(1)写出正确的情况
        //(2)然后取反即可
        //(3)这样写你的思路不会乱
        if(!(str != null && start > 0 && end > start && end < str.length())){
            throw  new RuntimeException("参数不正确");
        }

        char[] chars = str.toCharArray();
        //前面的数
        int i = start;
        int j = end;

        for(; i < j ;){
            char temp = chars[i];
            chars[i] = chars[j];
            chars[j] = temp;
            i++;
            j--;
        }

        //使用char数组重新构建一个String，然后返回即可

        return new String(chars);
    }

}
