package javalh.chapter13.homework;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork03 {
    public static void main(String[] args) {
        /*
        * 需求
        * (1)编写java程序,输入形式为：Han shun Ping 的人名,以 Ping,Han.S的形式打印出来,其中.S是中间单词的首字母
        * */
        String s = "Han shun Ping";
//        String s = null;
        String s_res = overString01(s);
        System.out.println(s_res);
    }


    public static String overString(String s){
        String s_arr[] = s.split(" ");
        String s_res = "";
        s_res += s_arr[2];
        s_res += ",";
        s_res += s_arr[0];
        s_res += " .";
        s_res += s_arr[1].toUpperCase().substring(0,1);
        return s_res;
    }

    public static String overString01(String s){
        //进行容错处理
        if(s == null) {
            System.out.println("数据不能为空");
            return "false";
        }
        String s_arr[] = s.split(" ");
        if(s_arr.length != 3){
            System.out.println("数据格式错误!!");
            return "false";
        }
        String s_res = String.format("%s,%s .%c",s_arr[2],s_arr[0],s_arr[1].toUpperCase().charAt(0));
        return s_res;
    }



}
