package javalh.chapter13.homework;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01 {
    public static void main(String[] args) {
        /*
        * 需求
        * (1)将字符串中的指定部分进行反转,比如将"abcdef"反转成"aedcbf"
        * (2)编写方法 public static String reverse(String str, int start , int end)搞定
        * */
        String str = "abcdef";
        String strs = reverse(str,1,3);
        System.out.println(strs);
    }

    public static String reverse(String str , int start , int end){


        //得到开始的字符串
        String str_start = str.substring(0,start);
        //得到结束的字符串
        String str_end = str.substring(end+1);
        System.out.println(str_end);

        //得到中间的字符串
        String str_middle = str.substring(start,end+1);
        System.out.println("--" + str_middle);
        //将中间的字符串转化为char数组
        char[] middle_chars = str_middle.toCharArray();
        char[] new_middle = new char[middle_chars.length];
        for(int i = 0 ; i < middle_chars.length ; i++){
            new_middle[i] = middle_chars[middle_chars.length - 1 - i];
        }

//        for(int i = 0 ; i < new_middle.length ; i++){
//            System.out.println(new_middle[i]);
//        }

        String new_middle_str = new String(new_middle);
        System.out.println(new_middle_str);

        String res_string = str_start.concat(new_middle_str).concat(str_end);

        return res_string;

    }

}
