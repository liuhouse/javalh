package javalh.chapter13.homework;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Homework04 {
    public static void main(String[] args) {
        /*
        * 需求
        * 输入字符串,判断里面有多少个大写字母,多少个小写字母,多少个数字
        * */
        String str = "12121sasasasASSASASA4545DFDFDFsdgggg4444858525 ";
        countStr(str);
    }

    public static String countType(String str){
        char[] chars = str.toCharArray();
        int Da = 0;
        int Xi = 0;
        int Num = 0;
        for(int i = 0 ; i < chars.length ; i++){
            //判断如果是数字的情况下
            if(Character.isDigit(chars[i])){
                Num++;
            }else{//判断如果不是数字,再判断是大写还是小写
                if(Character.isUpperCase(chars[i])){
                    Da++;
                }else{
                    Xi++;
                }
            }
        }
        return "数字:" + Num + "大写字母:" + Da + "小写字母：" + Xi;
    }


    public static void countStr(String str){
        if(str == null){
            System.out.println("输入不能为null...");
            return;
        }
        int strLength = str.length();
        int numCount = 0;
        int lowerCount = 0;
        int upperCount = 0;
        int otherCount = 0;
        for(int i = 0 ; i < strLength ; i++){
            //这里的关系必须是 && 的关系 ， 因为如果是或者的关系 那么ascill码的所有都是大于0的,那么就会全部走第一个
            if(str.charAt(i) >= '0' && str.charAt(i) <= '9'){
                numCount++;
                //这里使用字符串在进行比较的时候会默认的吧字符串转为ascill码然后再进行比较
            }else if(str.charAt(i) >= 'a' && str.charAt(i) <= 'z'){
                lowerCount++;
            }else if(str.charAt(i) >= 'A' && str.charAt(i) <= 'Z'){
                upperCount++;
            }else{
                otherCount++;
            }
        }
        System.out.println("数字有:" + numCount + "大写字母：" + upperCount + "小写字母：" + lowerCount + "其他：" + otherCount);
    }


}
