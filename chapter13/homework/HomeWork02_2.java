package javalh.chapter13.homework;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork02_2 {
    public static void main(String[] args) {
        String name = "abc";
        String pwd = "123456";
        String email = "1312797840@qq.com";
        try {
            userRegister(name,pwd,email);
            //如果真的捕捉到异常了,那么就会直接打印出错误
            //如果没有捕捉到异常,就显示注册成功了
            System.out.println("用户注册成功了");
        }catch (RuntimeException e){
            System.out.println(e.getMessage());
        }

    }

    /*
    * 用户注册
    *
    * */
    public static void userRegister(String name , String pwd , String email){
        //使用过关斩将的方式
        //第一关
        int userLength = name.length();
        if(!(userLength >= 2 && userLength <= 4)){
            throw new RuntimeException("用户名的长度为2或3或4");
        }

        //第二关
        if(!(pwd.length() == 6 && isDigital(pwd))){
            throw new RuntimeException("密码的长度为6,并且应该全部都为数字");
        }

        //第三关
        int i = email.indexOf('@');
        int j = email.indexOf('.');
        if(!(i > 0 && j > i)){
            throw new RuntimeException("邮箱中包含@和.,并且@要在.的前面");
        }


    }


    //单独写一个方法,判断密码是否全部为数字字符 boolean
    public static boolean isDigital(String str){
        //先将字符串转化为char数组
        char[] chars = str.toCharArray();
        for(int i = 0 ; i < chars.length ; i++){
            //要求字符串的每个字符全都都为数字
            //如果里面有字符小于‘0’或者大于‘9’的时候,就代表不是一个字符了
            if(chars[i] < '0' || chars[i] > '9'){
                return false;
            }
        }
        return true;
    }


}
