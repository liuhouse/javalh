package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class String01 {
    public static void main(String[] args) {
        /*
        * String类的理解和创建对象
        * (1)String对象用于保存字符串，也就是一组字符序列
        * (2)字符串常量对象是用双引号括起来的字符序列,例如:"你好","12.97","boy"等等
        * (3)字符串的字符使用的是Unicode字符编码,一个字符(不区分是字母还是汉字)占两个字节
        * (4)String类的常用构造器(其他的看手册),构造器的重载
        * String s1 = new String();
        * String s2 = new String(String original);
        * String s3 = new String(char[] a);
        * String s4 = new String(char[] a , int startIndex , int count);
        *
        * 5：String对象实现了Serializable，说明可以串行化【String可以串行化：可以在网络上传输】
        *   String实现了Comparable接口,说明String对象可以比较
        * 6：String是final类,不能被其他类继承
        * 7：String有属性 private final char value[]; 用于存放字符串内容
        * 8：一定要注意,value是一个final类型,不可以修改【地址不可以修改】,即value不能指向新的地址,但是单个字符的内容是可以变化的
        * */

        String name = "jack";
        name = "tom";

        final char[] value = {'a','b','c'};
        char[] v2 = {'t','o','m'};
        value[0] = 'A';
        //因为这里的value是使用final修饰的,所以不能改变地址    但是数组里面的内容是可以进行修改的
        //value = v2;






    }
}
