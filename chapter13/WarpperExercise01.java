package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class WarpperExercise01 {
    public static void main(String[] args) {
        //正确
        //正确
        //1.0  1

        Double d = 100d;//ok,自动装箱 Double.valueOf(100d)
        Float f = 1.5f;//ok自动装箱  Float.valueOf(1.5f)

        //如下的两个题目输出的结果相同吗?为什么
        //三元运算符，是一个整体  运算的时候会进行精度的向上转型，所以最终的返回类型就是最大精度
        Object obj1 = true ? new Integer(1) : new Double(2.0);
        System.out.println(obj1);

        Object obj2;
        //if else  是单独语句  所以各自执行各自的
        if(true){
            obj2 = new Integer(1);
        }else{
            obj2 = new Double(2.0);
        }
        System.out.println(obj2);

        //输出什么1



    }
}
