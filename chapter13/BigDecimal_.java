package javalh.chapter13;

import java.math.BigDecimal;

public class BigDecimal_ {
    public static void main(String[] args) {
        //当我们需要保存一个精度很高的数的时候,double不够用
        //打印出来之后默认数据就会被截取
        //double d = 1111888888888.121111111111d;

        //可以使用bigDecimal
        BigDecimal bigDecimal = new BigDecimal("1111888888888.121121212");
        BigDecimal bigDecimal1 = new BigDecimal("3");

        //老韩解读
        //1:如果对BigDecimal进行计算,比如加减乘除,不能直接使用+-*/ , 需要使用对应的方法
        //2:需要创建一个操作的 BigDecimal 然后调用相应的方法即可
        //相加
        System.out.println(bigDecimal.add(bigDecimal1));
        //相减
        System.out.println(bigDecimal.subtract(bigDecimal1));
        //相乘
        System.out.println(bigDecimal.multiply(bigDecimal1));
        //相除
        //System.out.println(bigDecimal.divide(bigDecimal1));//可能抛出异常 AirthmeticException

        //在调用divide方法的时候,指定精度即可 BigDecimal.ROUND_CEILING
        //如果有无限个循环的小数,就保留分子的精度

        System.out.println(bigDecimal.divide(bigDecimal1,BigDecimal.ROUND_CEILING));
//        System.out.println(bigDecimal);
    }
}
