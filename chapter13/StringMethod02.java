package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringMethod02 {
    public static void main(String[] args) {
        //1:toUpperCase转换成大写
        String s = "heLLo";
        System.out.println(s.toUpperCase());

        //2.toLowerCase将字符串转换成小写
        System.out.println(s.toLowerCase());

        //3.concat拼接字符串
        String s1 = "宝玉";
        s1 = s1.concat("林黛玉").concat("薛宝钗").concat("together");
        System.out.println(s1);

        //4.replace替换字符串中的字符
        s1 = "宝玉 and 林黛玉 林黛玉 林黛玉";
        //在s1中,将所有的林黛玉全部替换成薛宝钗
        //老韩解读:s1.replace()方法执行后,返回的结果才是替换过的
        //注意对s1的原本数据是没有任何影响的
        String s11 = s1.replace("林黛玉","薛宝钗");
        System.out.println(s11);

        System.out.println("==========");
        //5:split分割字符串,对于某些分割字符,我们需要转义比如 | \\等等
        String poem = "锄禾日当午,汗滴禾下土,谁知盘中餐,粒粒皆辛苦";
        String[] split = poem.split(",");
        for(int i = 0 ; i < split.length ; i++){
            System.out.println(split[i]);
        }

        System.out.println("++++++++++++++++++++++++");
        poem = "E:\\aaa\\bbb";
        //进行特殊字符的分割 \\  必须使用\ 进行转义
        split = poem.split("\\\\");
        System.out.println("====分割后的内容=======");
        for(int i = 0 ; i < split.length ; i++){
            System.out.println(split[i]);
        }

        //6:toCharArray 转化成字符数组
        s = "happy";
        char[] chars = s.toCharArray();
        for(int i = 0 ; i < chars.length ; i++){
            System.out.println(chars[i]);
        }

        //7:compareTo比较两个字符串的大小,
        // 如果前者大就返回正数,
        // 后者大,就返回负数
        // 如果相等,返回0
        //老韩解读
        /*
        * (1)如果长度相同,并且每个子串都是相同的,就返回0
        * (2)如果长度相同或者不相同.在进行比较的时候,就可以区分
        *   只要发现有一个字符不相同,就进行相减操作
        *   if(c1 != c2){
        *       return c1 - c2;
        *   }
        * (3)如果前面的部分都相同,就返回 str1.length - str2.length
        *
        * */

        String a = "jcck";//len = 2
        String b = "jack";//len = 4
        System.out.println(a.compareTo(b));//返回的是 'c' - 'a' = 2


//        String a = "ja";//len = 2
//        String b = "jack";//len = 4
//        System.out.println(a.compareTo(b));//返回的是 2 -4 = -2

        System.out.println("===========");

        //8.format 格式化字符串
        /*
        * 占位符有：
        * %s字符串 %c 字符 %d 整形 %.2f 浮点型
        * */
        String name = "john";
        int age = 10;
        double score = 56.857;
        char gender = '男';

        //将所有的信息都拼接在一个字符串中
        String info = "我的姓名是" + name + "年龄是" + age +
                "成绩是" + score + "性别是" + gender + "希望大家喜欢我!";
        System.out.println(info);


        System.out.println("===============");

        /*
        * 老韩解读
        * (1):%s,%d,%.2f %c 称为占位符
        * (2):这些占位符由后面的变量来替换
        * (3):%s表示后面由字符串来进行替换
        * (4):%d是整数来替换
        * (5):%.2f表示小数来替换,替换后,只保留小数点后两位,并且会自动的进行四舍五入的处理
        * (6):%c使用char类型来进行处理
        * */


        String formatStr =  "我的姓名是%s,年龄是%d,成绩是%.2f,性别是%c,希望大家喜欢我";
        String info2 = String.format(
                formatStr,
                name,
                age,
                score,
                gender
        );
        System.out.println(info2);





    }
}
