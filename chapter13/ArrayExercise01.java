package javalh.chapter13;

import java.util.Arrays;
import java.util.Comparator;

public class ArrayExercise01 {
    public static void main(String[] args) {
        /*
        * 需求：
        * 案例：自定义Book类,里面包含name和price，按照price排序(从小到大),要求使用两种方式排序,有一个Book[] books = 4本书的对象
        *
        *使用前面学习过的传递,实现Compare接口匿名内部类,也成为定制排序,
        * (1):按照price（从大到小）
        * (2):按照price（从小到大）
        * (3)按照书名长度从大到小
        * */
        Book[] books = new Book[4];
        books[0] = new Book("红楼梦",100);
        books[1] = new Book("金瓶梅111111111111",90);
        books[2] = new Book("青年文摘20年",50);
        books[3] = new Book("java从入门到入土",300);

        //price从大到小
//        Arrays.sort(books, new Comparator() {
//            @Override
//            public int compare(Object o1, Object o2) {
//                //先进行向下转型
//                Book book1 = (Book) o1;
//                Book book2 = (Book) o2;
//                //因为价格是double类型的所以不能直接转结果为int   比如  0.1 转化成int 就是0
//                double priceRes = book1.getPrice() - book2.getPrice();
//                if(priceRes > 0){
//                    return -1;
//                }else if(priceRes < 0){
//                    return 1;
//                }else{
//                    return 0;
//                }
//            }
//        });

        //按照价格从小到大
//        Arrays.sort(books, new Comparator() {
//            @Override
//            public int compare(Object o1, Object o2) {
//                //先进行向下转型
//                Book book1 = (Book) o1;
//                Book book2 = (Book) o2;
//                //因为价格是double类型的所以不能直接转结果为int   比如  0.1 转化成int 就是0
//                double priceRes = book1.getPrice() - book2.getPrice();
//                if(priceRes > 0){
//                    return 1;
//                }else if(priceRes < 0){
//                    return -1;
//                }else{
//                    return 0;
//                }
//            }
//        });


        //按照名字的长度从大到小排序
        Arrays.sort(books, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                //先进行向下转型
                Book book1 = (Book) o1;
                Book book2 = (Book) o2;
                //因为价格是double类型的所以不能直接转结果为int   比如  0.1 转化成int 就是0
                return book2.getName().length() - book1.getName().length();
            }
        });






        /*
        Book.bubbleByPrice(books, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Book book1 = (Book) o1;
                Book book2 = (Book) o2;
                //返回的顺序是按照价格从小到大
                //return (int)(book1.getPrice() - book2.getPrice());
                //返回的顺序是按照价格从大到小
                //return (int)(book2.getPrice() - book1.getPrice());

                //上面的直接转是不对的,因为两个数的差小于1的话,那么到最后都是0,所以就排不出来了

                //按照价格从大到小进行排序
                double priceRes = book2.getPrice() - book1.getPrice();
                if(priceRes > 0){
                    return -1;
                }else if(priceRes < 0){
                    return 1;
                }else{
                    return 0;
                }

                //返回按照书名的长度从大到小
//                return (book2.getName().length() - book1.getName().length());
            }
        });
        */

        for(int i = 0 ; i < books.length ; i++){
            System.out.println("名称" + books[i].getName() + "价格" +books[i].getPrice());
        }
    }
}

class Book{
    //书名
    private String name;
    //价格
    private double price;

    public Book(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    //按照价格排序
    //使用冒泡排序
    public static void bubbleByPrice(Book books[], Comparator c){
        Book temp = null;
        for(int i = 0 ; i < books.length - 1 ; i++){
            for(int j = 0 ; j < books.length - 1 - i ; j++){
                //进行从小到大进行排序
                if(c.compare(books[j],books[j+1]) > 0){
                    temp = books[j];
                    books[j] = books[j+1];
                    books[j+1] = temp;
                }
            }
        }
    }



}
