package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class WrapperExercise03 {
    @SuppressWarnings("all")
    public static void main(String[] args) {
        //false
        //false
        //true
        //false
        //false
        //true
        //true


        //示例1
        Integer i1 = new Integer(127);
        Integer i2 = new Integer(127);
        System.out.println(i1 == i2);//false

        //实例2
        Integer i3 = new Integer(128);
        Integer i4 = new Integer(128);
        System.out.println(i3 == i4);//false

        //实例3
        Integer i5 = 127;//底层是valueOf(127)
        Integer i6 = 127;//底层是valueOf(127)  -128 - 127
        System.out.println(i5 == i6);//true

        //实例4
        Integer i7 = 128;
        Integer i8 = 128;
        System.out.println(i7 == i8);//false

        //实例5
        Integer i9 = 127;//Integer.valueOf(127)
        Integer i10 = new Integer(127);//new一个新的对象
        System.out.println(i9 == i10);//false

        //实例6
        //只要有基本数据类型,判断的都是值是否相等
        Integer i11 = 127;
        int i12 = 127;
        System.out.println(i11 == i12);//true

        //实例7
        Integer i13 = 128;
        int i14 = 128;
        //只要有基本数据类型,那么就是比较值是否相等
        System.out.println(i13 == i14);//true

    }
}
