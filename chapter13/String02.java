package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class String02 {
    public static void main(String[] args) {
        //创建String对象的两种方式
        //(1)方式1:直接赋值String s = "hspedu";
        //(2)方式2：调用构造器 String s = new String("hspedu");
        //两种创建String对象的区别

        /*
        * 1:方式一,先从常量池查看是否有"hsp"数据空间,如果有,直接指向,如果没有则重新创建,然后指向,s是最终指向的是常量池的空间地址
        * 2：方式二:先在堆中创建空间,里面维护了value属性,指向常量池的hsp空间
        *   如果常量池没有"hsp"，就重新创建,然后指向空间,如果有,直接通过value指向,最终指向的是堆中的空间地址
        * */

    }
}
