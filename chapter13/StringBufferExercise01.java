package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringBufferExercise01 {
    public static void main(String[] args) {
        String str = null;//ok
        StringBuffer sb = new StringBuffer();//ok
        //这里需要看源码,底层调用的是AbstractStringBuilder 和 appendNull  最终返回的是null字符串
        sb.append(str);//true
        System.out.println(sb.length());//4
        System.out.println(sb);//null

        //下面的构造器,会抛出空指针错误   NullPointerException
        try {
            StringBuffer sb1 = new StringBuffer(str);//查看底层源码 super(str.length) null.length  使用null来进行运算的时候会报空指针错误
            System.out.println(sb1);
        } catch(NullPointerException e){
            System.out.println("空指针错误" + e.getMessage());
        }

    }
}
