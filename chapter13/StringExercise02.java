package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringExercise02 {
    public static void main(String[] args) {
        String a = new String("abc");
        String b = new String("abc");
        System.out.println(a.equals(b));//true 比较的是里面的值是不是相等
        System.out.println(a == b);//false   因为是两个不同的对象
    }
}
