package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringBuffer01 {
    public static void main(String[] args) {
        //java.lang.StringBuffer代表可变的字符序列，可以对字符串的内容进行增删
        //很多方法与String相同,但是StringBuffer是可变长度的
        //StringBuffer是一个容器

        /*
        * 老韩解读
        * (1):StringBuffer 的直接父类是AbstractStringBuilder
        * (2):StringBuffer 实现了Serializable,即StringBuffer的对象可以串行化
        * (3):在父类中 AbstractStringBuilder有属性char[] value,不是final
        *       该value数组存放字符串内容,引出存放在堆中
        * (4):StringBuffer是一个final类,不能被继承
        * (5):因为StringBuffer字符内容是存放在char[] value,所以是在变化的(增加/删除)
        *   不用每次都更换地址(即不是每次都是创建新的对象),所以效率高于String
        * */

        StringBuffer hello = new StringBuffer("hello");
    }
}


