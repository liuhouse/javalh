package javalh.chapter13;

import java.util.Arrays;

public class System_ {
    public static void main(String[] args) {
        /*
        * System类的常见方法和案例
        * */

        //exit 退出当前程序
        //老韩解读
        //1:exit(0)表示退出程序
        //2:0表示一个状态,正常的状态
//        System.out.println("OK");
//        System.exit(0);
//        System.out.println("ok2");


        //arraycopy:复制数组元素,比较适合底层调用
        //一般使用Arrays.copyOf()完成数组复制

        /*
        * 老韩解读
        * 1：主要是搞清楚这五个参数的含义
        * 2：
        *  src  源数组
        * @param      src      the source array.
        *  srcPos : 从源数组的哪个索引位置开始拷贝
        * @param      srcPos   starting position in the source array.
        *  dest  目标数组  ， 即把源数组的数据拷贝到哪个数组
        * @param      dest     the destination array.
        * destPos : 把源数组的数据拷贝到目标数组的哪个索引
        * @param      destPos  starting position in the destination data.
        * length : 从源数组中拷贝多少个数据到目标数组
        * @param      length   the number of array elements to be copied.
        * */

        int[] src = {1,2,3};
        int[] dest = new int[3];//dest当前是{0,0,0}



        System.arraycopy(src,1,dest,0,2);
        System.out.println(Arrays.toString(dest));


        //currentTimeMillens   返回当前时间距离 1970-1-1的毫秒数
        //老韩解读
        System.out.println(System.currentTimeMillis());



    }
}
