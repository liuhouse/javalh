package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringBufferMethod {
    public static void main(String[] args) {
        //StringBuffer常见方法
        StringBuffer s = new StringBuffer("hello");
        //增  append  追加
        //向字符串后面追加内容
        s.append(',');//"hello,"
        s.append("张三丰");// "hello,张三丰"
        s.append("赵敏").append(100).append(true).append(10.5);//"hello,张三丰赵敏100true10.5"
        System.out.println(s);

        /*
        *
        * 删除索引为 >= start && < end 处的字符
        * 解读：删除 11-14的字符[11 , 14)
        * */

        //删     hello,张三丰赵敏100true10.5
        s.delete(11,14);//hello,张三丰赵敏true10.5
        System.out.println(s);
        s.deleteCharAt(0);
        System.out.println(s);//ello,张三丰赵敏true10.5


        //改
        //老韩解读,使用周芷若替换张三丰[5,8)  包括第五个 但是不包括第八个
        s.replace(5,8,"周芷若");
        System.out.println(s);//ello,周芷若赵敏true10.5

        int indexOf = s.indexOf("敏");
        System.out.println(indexOf);


        //插
        //老韩解读，在索引为0的位置插入 ‘H’
        s.insert(0,'H');
        //在赵敏后面加上张无忌
        //在索引为11的位置开始插入 "张无忌"
        s.insert(11,"张无忌");
        System.out.println(s);

        //计算字符串的长度
        System.out.println(s.length());//22








    }
}
