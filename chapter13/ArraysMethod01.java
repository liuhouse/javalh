package javalh.chapter13;

import java.util.Arrays;
import java.util.Comparator;

public class ArraysMethod01 {
    public static void main(String[] args) {
        /*
        * Arrays里面包含了一系列的静态方法,用于管理或者操作数组(比如排序和搜索)
        * (1)toString 返回数组的字符串形式
        * (2)sort排序(自然排序和定制排序)
        * */

//        int[] arr = {5,8,7};
//        System.out.println(Arrays.toString(arr));

        Integer arr1[] = {1,-1,7,0,89};
        //自然排序,从小到大
//        Arrays.sort(arr1);
//        System.out.println(Arrays.toString(arr1));

        //定制排序

        /*
        *

        *
        *
        * 源码分析
        * (1)Arrays.sort(arr,new Compare())
        * (2)最终到TimSort类的 private static <T> void binarySort(T[] a,int lo,int hi,int start,Comaprator<? super T>c)()
        * (3)执行到binarySort方法的代码,会根据动态绑定机制 c.compare()执行我们传入的匿名内部类compare
        *  *  while (left < right) {
                int mid = (left + right) >>> 1;
                if (c.compare(pivot, a[mid]) < 0)
                    right = mid;
                else
                    left = mid + 1;
            }
            *
            new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                //从小到大排序   o1 - o2    从大到小排序 o2 - o1
                //向下转型
                Integer i1 = (Integer) o1;
                Integer i2 = (Integer) o2;
                return i2 - i1;
            }
            * (5)public int compare(Object o1 , Object o2) 返回的值>0 还是 < 0
            * 会影响整个排序结果,这就充分体现了 接口编程+动态绑定+匿名内部类的综合使用
            *
            *
            *
            *
            *
            *
        *
        * */


        Arrays.sort(arr1, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                //从小到大排序   o1 - o2    从大到小排序 o2 - o1
                //向下转型
                Integer i1 = (Integer) o1;
                Integer i2 = (Integer) o2;
                return i2 - i1;
            }
        });
        System.out.println(Arrays.toString(arr1));

    }
}
