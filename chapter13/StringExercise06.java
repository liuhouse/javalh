package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringExercise06 {
    public static void main(String[] args) {
        //(1)String是一个final类,代表不可变的字符序列
        //(2)字符串是不可变的,一个字符串对象一旦被分配,其内容是不可变的

        //下面的语句创建了几个对象?画出内存布局图
//        String s1 = "hello";
//        s1 = "haha";
        //创建了两个常量对象


        //面试题
        //题1
        String a = "hello" + "abc";
        //创建了几个对象
        //创建了一个对象
        //老韩解读：String a = "hello" + "abc"  底层优化等价于 String a = "helloabc";
        //分析
        //1:编译器不傻,做一个优化,判断创建的常量池对象,是否有引用指向
        //2:String a = "hello" + "abc" => String a = "helloabc"



    }
}
