package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringExercise05 {
    public static void main(String[] args) {
        //true true true   false
        Person1 p1 = new Person1();
        p1.name = "hspedu";
        Person1 p2 = new Person1();
        p2.name = "hspedu";

        //比较两个属性值的字符串是否相等
        System.out.println(p1.name.equals(p2.name));//true
        //两个对象的name指向的都是同一个常量池, == 是做为地址进行比较的
        System.out.println(p1.name == p2.name);//true
        //比较的也是地址   p1.name的地址就是常量池的地址   "hspedu"也是常量池的地址
        System.out.println(p1.name == "hspedu");//true

        String s1 = new String("bcde");
        String s2 = new String("bcde");
        //因为是New出了两个对象,所以内存地址是肯定不一样的
        System.out.println(s1 == s2);//false


    }
}

class Person1{
    public String name;
}
