package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringExercise04 {
    public static void main(String[] args) {
        //false true true false
        String s1 = "hspedu";//指向常量池"hspedu"
        String s2 = "java";//指向常量池java
        String s4 = "java";//指向常量池 java
        String s3 = new String("java");//指向的是堆中的对象

        System.out.println(s2 == s3);//false 因为指向的不是一个地址
        System.out.println(s2 == s4);//true 因为指向的是同一个地址
        System.out.println(s2.equals(s3));//true  判断两个对象的内容是否相等
        System.out.println(s1 == s2);//false 地址都不一样


    }
}
