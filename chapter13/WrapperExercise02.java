package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class WrapperExercise02 {
    @SuppressWarnings("all")
    public static void main(String[] args) {

        /*
        * 老韩解读
        * 1：如果i在IntegerCache.low(-128) - Integer.high(127)之间，就直接从数组返回
        * 2: 如果不在-128 - 127 之间 就直接newInteger(i)
        * 原码解读
        * public static Integer valueOf(int i){
        *      if(i >= IntegerCache.low && i <= Integer.high){
        *           return InegerCache.cache[i + (-IntegerCache.low)];
        *      }
        *      return new Integer(i);
        * }
        *
        *
        * */

        //1：false 因为是两个对象,比较的是地址
        //2:true
        //3:false

        //看看下面的代码输出的是什么?为什么
        Integer i = new Integer(1);
        Integer j = new Integer(1);
        //false 因为这里是new 了两个对象   所以是比较地址的   所以不相等
        System.out.println(i == j);

        //因为范围是在 -128 - 127 之间 所以返回的数字
        //所以这里主要就是看范围 -128 - 127 之间,就直接返回,否则就是  new Integer(x)
        Integer m = 1;//底层 Integer.valueOf(1) -> 阅读原码   返回的直接是数值1
        Integer n = 1;//底层 Integer.valueOf(1) -> 阅读原码   返回的直接就是数值1
        System.out.println(m == n);//true

        Integer x = 128;//底层 Integer.valueOf(128)
        Integer y = 128;//底层 Integer.valueOf(128)
        System.out.println(x == y);//false




    }
}
