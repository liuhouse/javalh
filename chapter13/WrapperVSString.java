package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class WrapperVSString {
    public static void main(String[] args) {
        //包装类型和String类型的相互转换
        //案例演示 ， 以Integer和String为例,其他类似
        //包装类(Integer) -> String
        Integer i = 100;//自动装箱
        //方式1
        String str1 = i + "";
        //方式2
        String str2 = i.toString();
        //方式3
        String str3 = String.valueOf(i);

        //String -> 包装类(Integer)
        String str4 = "12345";
        Integer i2 = Integer.parseInt(str4);//使用到自动装箱
        Integer i3 = Integer.valueOf(str4);//自动装箱\
        @SuppressWarnings({"all"})
        Integer i4 = new Integer(str4);

        System.out.println("ok!");

    }
}
