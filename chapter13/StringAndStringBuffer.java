package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringAndStringBuffer {
    public static void main(String[] args) {
        //看String -> StringBuffer
        String str = "hello tom";
        //方式1 使用构造器
        //注意：返回的才是StringBuffer对象,对str本身没有影响
        StringBuffer stringBuffer = new StringBuffer(str);
        System.out.println(stringBuffer);

        //方式2 使用的是append方法
        StringBuffer stringBuffer1 = new StringBuffer();
        stringBuffer1.append(str);
        System.out.println(stringBuffer1);


        //看看 StringBuffer -> String
        StringBuffer stringBuffer2 = new StringBuffer("韩顺平教育");
        //方式1：使用StringBuffer 提供的toString方法

        //其实toString的底层也是使用的是 new String(s)
        String s = stringBuffer2.toString();
        System.out.println(s);

        //方式2：使用构造器来搞定
        String s1 = new String(stringBuffer2);
        System.out.println(s1);


    }
}
