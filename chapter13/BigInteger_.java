package javalh.chapter13;

import java.math.BigInteger;

public class BigInteger_ {
    public static void main(String[] args) {
        //当我们变成中,需要处理很大的整数,long不够用
        //long l = 2365565598898945454545l;
        //System.out.println("l=" + l);
        //可以使用BigInteger类来搞定,无论多长的数字都是可以的,很强大的
        BigInteger bigInteger = new BigInteger("1222222222222222222221212121212");
        System.out.println(bigInteger);

        //老韩解读
        //1:在对BigInteger进行加减乘除的时候,需要使用对应的方法,不能直接使用 + - * /
        //2:可以创建一个要操作的BigInteger然后进行相应的操作
        //相除最终的是整数  因为本身就是整数
        BigInteger bigInteger2 = new BigInteger("100");

        //进行两数相加
        BigInteger add = bigInteger.add(bigInteger2);
        System.out.println(add);

        //进行两数相减
        BigInteger subtract = bigInteger.subtract(bigInteger2);
        System.out.println(subtract);

        //进行两数相乘
        BigInteger multiply = bigInteger.multiply(bigInteger2);
        System.out.println(multiply);

        //进行相除操作
        BigInteger divide = bigInteger.divide(bigInteger2);
        System.out.println(divide);


    }
}
