package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringExercise03 {
    public static void main(String[] args) {
        //true  false  true  false
        String a = "hsp";//a指向常量池的hsp
        String b = new String("hsp");//b指向堆中的对象
        System.out.println(a.equals(b));//比较a和b的内容是否相等   true
        System.out.println(a == b);//比较两个对象的地址相等 false
        System.out.println(a == b.intern());//true a的地址 和 b的地址  都是常量池 hsp 的地址   所以是相等的
        System.out.println(b == b.intern());//false b 指向的是堆中对象的地址,b.intern() 指的是常量池中"hsp"的地址






    }
}
