package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class WrapperMethod {
    public static void main(String[] args) {
        //Integer类和Character 有一些常用的方法,我们一起来使用一下
        System.out.println(Integer.MAX_VALUE);//返回最大值
        System.out.println(Integer.MIN_VALUE);//返回最小值

        System.out.println(Character.isDigit('a'));//判断是不是数字
        System.out.println(Character.isLetter('a'));//判断是不是字母
        System.out.println(Character.isUpperCase('a'));//判断是不是大写
        System.out.println(Character.isLowerCase('a'));//判断是不是小写

        System.out.println(Character.isWhitespace('a'));//判断是不是空格
        System.out.println(Character.toUpperCase('a'));//转化成大写
        System.out.println(Character.toLowerCase('A'));//转化成小写


    }
}
