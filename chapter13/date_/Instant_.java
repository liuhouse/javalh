package javalh.chapter13.date_;

import java.time.Instant;
import java.util.Date;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Instant_ {
    public static void main(String[] args) {
        //1:通过静态方法now(),获取表示当前时间戳的对象
        Instant now = Instant.now();
        System.out.println(now);
        //2:通过from可以把Instant转成Date
        Date from = Date.from(now);
        System.out.println(from);
        //3:通过date的toInstant()方法可以把date转化成Instant对象
        Instant instant = from.toInstant();
        System.out.println(instant);
    }
}
