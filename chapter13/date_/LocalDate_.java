package javalh.chapter13.date_;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * @author 刘皓
 * @version 1.0
 */
public class LocalDate_ {
    public static void main(String[] args) {
        //第三代日期类
        LocalDateTime ldt = LocalDateTime.now();
//        System.out.println(ldt);

        //使用DateTimeFormatter对象来进行格式化
        //创建DateTimeFormatter对象

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String format = dateTimeFormatter.format(ldt);
        System.out.println("格式化的日期" + format);


//        System.out.println("年=" + ldt.getYear());
//        System.out.println("月=" + ldt.getMonth());
//        System.out.println("月=" + ldt.getMonthValue());
//        System.out.println("日=" + ldt.getDayOfMonth());
//        System.out.println("日=" + ldt.getDayOfYear());
//        System.out.println("时=" + ldt.getHour());
//        System.out.println("分=" + ldt.getMinute());
//        System.out.println("秒=" + ldt.getSecond());

        //可以获取年月日
//        LocalDate now = LocalDate.now();
//        System.out.println(now.getYear());
//        System.out.println(now.getMonth());
//        System.out.println(now.getDayOfMonth());
//        System.out.println(now.getMonthValue());


        //LocalTime获取时分秒
//        LocalTime now1 = LocalTime.now();
//        System.out.println(now1.getHour());
//        System.out.println(now1.getMinute());
//        System.out.println(now1.getSecond());


        //提供 plus 和 minus方法可以对当前时间进行加或者减
        //看看890天后,是什么时候,把 年月日-时分秒
        LocalDateTime localDateTime = ldt.plusDays(365);
        System.out.println("365天后=" + dateTimeFormatter.format(localDateTime));

        //看看3456分钟是什么时候,把年月日-时分秒输出
        LocalDateTime localDateTime1 = ldt.minusMinutes(3456);
        System.out.println("3456分钟前 日期=" + dateTimeFormatter.format(localDateTime1));


    }
}
