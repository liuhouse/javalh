package javalh.chapter13.date_;

import java.util.Calendar;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Calendar_ {
    public static void main(String[] args) {
        //创建日历对象//比较简单,自由
        Calendar c = Calendar.getInstance();
//        System.out.println("c = " + c);
        //获取日历对象的某个日历字段
        System.out.println(c.get(Calendar.YEAR));
        //获取月,这里为什么要+1,因为Calender返回月的时候,是按照0开始编号
        System.out.println(c.get(Calendar.MONTH) + 1);
        //获取日 (月中的日)
        System.out.println(c.get(Calendar.DAY_OF_MONTH));
        //获取小时
        System.out.println(c.get(Calendar.HOUR));
        //获取分钟
        System.out.println(c.get(Calendar.MINUTE));
        //获取秒
        System.out.println(c.get(Calendar.SECOND));

        //Calender 没有专门的格式化方法,所以需要程序员自己来组合显示
        System.out.println(c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.DAY_OF_MONTH)
            + " " + c.get(Calendar.HOUR_OF_DAY) + " " + c.get(Calendar.MINUTE) + " " +c.get(Calendar.SECOND)
        );

    }
}
