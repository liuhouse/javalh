package javalh.chapter13.date_;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Date_ {
    public static void main(String[] args) throws ParseException {
        //(1)Date:精确到毫秒.代表特定的瞬间
        //(2)SimpleDateFormat:格式和解释日期的类SimpleDateFormat格式化和解析日期的具体类,他允许进行格式化
        //(日期 -> 文本) ， (文本 -> 日期) 和规范化

        //老韩解读
        //1.获取当前的系统时间
        //2这里的Date是在java.util包中
        //3.默认输出的日期格式是国外的方式,因此通常需要对格式进行转化
        Date date = new Date();//获取当前系统时间
        System.out.println("当前日期" + date);

        Date date1 = new Date(91245678);//通过指定毫秒数得到时间
        System.out.println("date1=" + date1);

        //老韩解读
        //1.创建一个 SimpleDateFormat对象,可以指定相应的格式
        //2:这里的格式使用的字母都是规定好的,不能乱写

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy 年 MM 月 dd 日 hh:mm:ss E");
        String format = sdf.format(date);//format:将日期转换成指定格式的字符串
        System.out.println("当前日期= " + format);


        //老韩解读
        //1:可以把一个格式化的String转换成对应的Date
        //2:得到Date仍然在输出的时候,还是按照国外的形式,如果希望指定格式输出,需要转换格式
        //3:在把String->Date,使用sdf格式需要和你给的String格式一样,否则会抛出转换异常
        String s = "2021 年 12 月 06 日 10:20:30 周二";
        Date parse = sdf.parse(s);
        System.out.println(sdf.format(parse));

    }
}

class Dog{
    private String name;

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
