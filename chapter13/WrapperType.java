package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class WrapperType {
    public static void main(String[] args) {
        /*
        * 包装类的分类
        *
        * 1：针对八种基本数据类型相应的引用类型 - 包装类
        * 2：有了类的特点,就可以调用类的方法
        * */

        //boolean  -> Boolean extend Object
        //char -> Character extends Object
        //byte -> Byte extends Number extends Object
        //short -> Short extends Number extends Object
        //int -> Integer extends Number extends Object
        //long -> Long extends Number extends Object
        //float -> Float extends Number extends Object
        //double -> Double extends Number extends Object

    }
}
