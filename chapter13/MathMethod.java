package javalh.chapter13;

public class MathMethod {
    public static void main(String[] args) {
        /*
        * Math类包含用于执行基本数学运算的方法，如初等指数,对数,平方很,和三角函数
        * */
        //看看Math的常用方法(静态方法)

        //abs绝对值
        int abs = Math.abs(-9);
        System.out.println(abs);//9

        //pow求幂
        double pow = Math.pow(2,4);
        System.out.println(pow);//16

        //ceil 向上取整,返回>=该数的最小整数(转换成double)
        double ceil = Math.ceil(3.9);
        System.out.println(ceil);//4

        //floor向下取整.返回<=该参数的最大整数(转换成double)
        double floor = Math.floor(4.0001);
        System.out.println(floor);//4.0

        //round四舍五入 Math.floor(该参数 + 0.5)
        long round = Math.round(5.41);//
        System.out.println(round);//5

        //求开方
        double sqrt = Math.sqrt(9.0);
        System.out.println(sqrt);//3.0

        //random求随机数
        /*
        * random 返回的是 0<=x<1 之间的随机小数
        * 思考：请写出获取a-b之间的一个随机整数,a,b均为整数,比如a=2, b =7
        * 即返回一个数x   2<=x<=7
        * 老韩解读：
        * int(Math.random()*(b-a))  返回的就是 0<=数<=b-a
        * (1)(int)(a)<=x<=(int)(a+Math.random()*(b-a+1))
        * (2)使用具体的数给小伙伴们介绍  a = 2    b = 7
        *   (int)(a+Math.random()*(b-a+1)) = (int)(2+Math.random()*6)
        *   Math.random()*6 返回的是 0<=x<6的小数
        *   2 + Math.random()*6  返回的是2<=x<8的小数
        *   (int)(2+Math.random()*(b-a+1)) = 2<=x<=7
        * */
        for(int i = 0 ; i < 20 ; i++){
            System.out.println((int)(2 + Math.random() * (7-2+1)));
        }

        System.out.println("============================================");

        //max, min 返回最大值和最小值
        int min = Math.min(1,2);
        System.out.println(min);

        int max = Math.max(45,90);
        System.out.println(max);


    }
}
