package javalh.chapter13;

public class StringBuilder01 {
    public static void main(String[] args) {
        /*
        * 老韩解读
        * (1)StringBuilder的直接父类是 AbstractStringBuilder
        * (2)StringBuilder 实现了Serializable , 即 StringBuilder 的对象可以串行化
        * (3)在父类中 AbstractStringBuilder 中有属性 char[] value 不是final
        *   该value数组存放 字符串的内容 ， 因此存放在堆中
        * (4)StringBuilder是一个final类,不能被继承
        * (5)因为StringBuilder字符串内容是存在char[] value  , 所以在变化(增加/删除)
        * 不用每次都更换地址(即不是每次都创建新对象),所以效率是高于String
        * (6)StringBuilder的方法,没有做互斥的处理,即没有syncchronized关键字,因此在单线程的情况下使用
        * StringBuilder
        * */


        StringBuilder hello = new StringBuilder("hello");

        new StringBuffer("hello");
    }
}
