package javalh.chapter13;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringExercise10 {
    String str = new String("hsp");
    final char[] ch = {'j','a','v','a'};

    //传递两个参数 第一个是字符串   第二个是char类型的数组
    public void change(String str , char ch[]){
        //就近原则,只会修改当前栈方法中的str，各自栈方法中会就近的访问自己的属性数据
        //就近原则  局部的str只对局部有用   对全局是没用的    并且方法执行完毕之后就会销毁掉
        str = "java";
        //而ch[]是引用类型的数组,所以发生改变的时候就数组也会发生改变
        ch[0] = 'h';
    }

    public static void main(String[] args) {
        //下面程序的运行结果是什么,尝试画出内存布局图
        StringExercise10 ex = new StringExercise10();
        ex.change(ex.str,ex.ch);


        System.out.print(ex.str + " and");
        System.out.println(ex.ch);
    }

}
