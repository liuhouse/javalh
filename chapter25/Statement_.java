package javalh.chapter25;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 * 演示statement的注入问题
 */
@SuppressWarnings({"all"})
public class Statement_ {
    public static void main(String[] args) throws Exception {
        //1.表示数据库结果集的数据表,通常通过执行查询数据库的语句生成
        //2.ResultSet对象保持一个光标指向当前的数据行,最初,光标位于第一行之前
        //3.next()方法将光标移动到下一行,并且由于在ResultSet对象中没有更多行的时候返回false
        //因此可以在while循环中使用循环来遍历结果集
        //通过properties对象获取配置文件的信息

        Scanner scanner = new Scanner(System.in);

        //让用户输入管理员和密码
        System.out.println("请输入管理员的名称:");//next();当接收到空格或者 '就表示结束
        String admin_name = scanner.nextLine();//老师说明,如果希望看到sql注入,这里需要使用nextLine()
        System.out.println("请输入管理员的密码:");
        String admin_pwd = scanner.nextLine();

        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter25\\mysql.properties"));
        //获取相关的值
        //获取用户名
        String user = properties.getProperty("user");
        //获取密码
        String password = properties.getProperty("password");
        //获取驱动
        String driver = properties.getProperty("driver");
        //获取url
        String url = properties.getProperty("url");

        //1.注册驱动
        Class.forName(driver);//建议写上,不写也可以
        //2.得到连接
        Connection connection = DriverManager.getConnection(url, user, password);
        //3.得到Statement对象
        Statement statement = connection.createStatement();
        //4.组织sql语句
        String sql = "select `name`,pwd from admins where `name` = '"+admin_name+"' and pwd = '"+admin_pwd+"'";
        //执行给定的sql语句,该语句返回单个的Result对象
        java.sql.ResultSet resultSet = statement.executeQuery(sql);
        if(resultSet.next()){//如果查询到一条记录,说明该管理存在
            System.out.println("恭喜,登录成功");
        }else{
            System.out.println("对不起,登录失败");
        }
        //切记 - 关闭连接
        resultSet.close();
        statement.close();
        connection.close();
    }
}
