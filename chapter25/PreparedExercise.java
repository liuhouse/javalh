package javalh.chapter25;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 */
public class PreparedExercise {
    public static void main(String[] args) throws Exception {
        /**
         * 要求：
         * 1.创建bd_admin表
         * 2.使用PreparedStatement添加5条数据
         * 3.修改tom的记录,将name改成king
         * 4.删除一条记录
         * 5.查询全部记录,并显示在控制台
         */

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("请输入要添加的管理员的名称:");
//        String admin_name = scanner.nextLine();

        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter25\\mysql.properties"));
        //获取用户信息
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String driver = properties.getProperty("driver");
        String url = properties.getProperty("url");

        //注册驱动
        Class.forName(driver);
        //得到连接
        Connection connection = DriverManager.getConnection(url, user, password);
        //构建sql语句
//        String sql = "insert into bd_admin(`name`) values(?)";

        //将id=5的姓名修改为二郎神
        //String sql = "update bd_admin set name = ? where id = ?";

        //将 id = 2 的数据进行删除
//        String sql = "delete from bd_admin where name= ?";

        //查询所有的管理员数据
        String sql = "select * from bd_admin";

        //创建预处理对象
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //设置要添加的数据
//        preparedStatement.setString(1, admin_name);
//        preparedStatement.setInt(2,5);

        //执行sql语句
//        int rows = preparedStatement.executeUpdate();

        ResultSet resultSet = preparedStatement.executeQuery();

//        System.out.println(rows > 0 ? "操作成功" : "操作失败");
        System.out.println("id \t name");
        while (resultSet.next()){
//            int id = resultSet.getInt(1);
//            String name = resultSet.getString(2);
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            System.out.println(id + " \t " + name);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();
    }
}
