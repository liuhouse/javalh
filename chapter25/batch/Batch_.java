package javalh.chapter25.batch;

import com.mysql.cj.PreparedQuery;
import com.mysql.cj.QueryBindings;
import com.mysql.cj.exceptions.CJException;
import com.mysql.cj.jdbc.exceptions.SQLExceptionsMapping;
import javalh.chapter25.utils.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Batch_ {
    public static void main(String[] args) {
        //批处理
        //1.基本介绍
        /**
         * 1.当想要成批插入或者更新记录的时候,可以采用java的批量更新机制,这一机制允许多条语句一次性
         *  提交给数据库批量处理,通常情况下比单独提交处理更有效率
         * 2.JDBC的批量处理语句包括下面的语法：
         *  addBatch():添加需要批量处理的sql语句或者参数
         *  executeBatch():批量执行处理语句
         *  clearBatch():清空批处理包的语句
         * 3.JDBC连接Mysql的时候,如果需要使用批处理功能,请在url中添加参数 ?rewriteBatchedStatements=true
         * 4.批量处理往往和PreparedStatement对象一起搭配使用,可以减少编译次数,又减少运行次数,效率大大提高
         */

        //简单的需求
        //1.演示向admin2表中添加5000条数据,看看使用批处理耗时多久
        //2.注意需要修改配置文件jdbc.properties
        //  url=jdbc:mysql://localhost:3306/数据库?rewriteBatchedStatements=true
    }

    //传统方法,添加5000条数据到admin2
    @Test
    public void noBatch(){
        Connection connection = JDBCUtils.getConnection();
        String sql = "insert into admin2 values(null , ? , ?)";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            System.out.println("开始执行");
            long start = System.currentTimeMillis();
            for(int i = 0 ; i < 5000 ; i++){//5000执行
                //填充第一个参数
                preparedStatement.setString(1 , "jack" + i);
                //填充第二个参数
                preparedStatement.setString(2,"666");
                //执行更新操作
                preparedStatement.executeUpdate();
            }
            long end = System.currentTimeMillis();
            System.out.println("传统的方式 耗时 = " + (end - start));//10min
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.close(null , preparedStatement ,connection);
        }
    }



    //使用批量的方式添加数据
    //可以看出来,批量添加数据的效率是非常的高的

//    public void addBatch() throws SQLException {
//        try {
//            synchronized(this.checkClosed().getConnectionMutex()) {
//                QueryBindings<?> queryBindings = ((PreparedQuery)this.query).getQueryBindings();
//                queryBindings.checkAllParametersSet();
//                this.query.addBatch(queryBindings.clone());
//            }
//        } catch (CJException var6) {
//            throw SQLExceptionsMapping.translateException(var6, this.getExceptionInterceptor());
//        }
//    }

//    public void addBatch(Object batch) {
//        if (this.batchedArgs == null) {
//            this.batchedArgs = new ArrayList();
//        }
//
//        this.batchedArgs.add(batch);
//    }

    //1.第一在第一次添加的数据的时候 会创建 ArrayList -> elementData => Object[]
    //2.elementData => Object[] 就会存放我们预处理的sql语句
    //3.当elementData满后,就按照1.5扩容
    //4.当添加到指定的值后,就执行executeBatch
    //5.批量处理会减少我们发送sql语句的网络开销,而且会减少编译的次数,因此效率比较的高
    //6.不是批量处理的话,每一条sql语句都会先去执行sql，会进行编译,也就是要编译5000次
    //而且每次写入数据的时候其实是IO操作,所以很慢
    //举例理解：就好像是5000个人,用一辆小车来运到学校,一个一个运,需要一周的时间,但是使用大巴车
    //一辆大巴车一次1000个客户,只需要5次就完了,效率是非常高的

    @Test
    public void batch(){
        Connection connection = JDBCUtils.getConnection();
        String sql = "insert into admin2 values(null , ? , ?)";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            System.out.println("开始执行");
            long start = System.currentTimeMillis();
            for(int i = 0 ; i < 5000 ; i++){
                preparedStatement.setString(1 , "jack" + i);
                preparedStatement.setString(2 , "666");
                preparedStatement.addBatch();
                //当有1000条记录的时候,再批量执行
                if((i + 1) %1000 == 0){//满足1000条sql
                    //批量执行
                    preparedStatement.executeBatch();
                    //批量执行完成之后,清空一把
                    preparedStatement.clearBatch();
                }
            }
            long end = System.currentTimeMillis();
            System.out.println("批量方式 耗时=" + (end - start));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.close(null , preparedStatement , connection);
        }

    }


}
