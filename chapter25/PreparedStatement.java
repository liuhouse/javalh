package javalh.chapter25;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings({"all"})
public class PreparedStatement {
    public static void main(String[] args) throws Exception {
        //基本介绍
        //1.PreparedStatement 执行的sql语句中的参数用问号(?)来表示,调用PreparedStatement对象的setXxx()
        //方法有两个参数,第一个参数是要设置的sql语句中的参数的索引(从1开始),第二个是设置sql语句中的参数的值
        //2.调用executeQuery() , 返回ResultSet对象
        //3.调用executeUpdate():执行更新,包括增,删,修改

        //预处理的好处
        //1.不再使用+拼接sql语句,减少语法错误
        //2.有效的解决了sql注入问题
        //3.大大的减少了编译次数,效率较高

        //看PreparedStatement类图
        Scanner scanner = new Scanner(System.in);
        //让用户输入管理员和密码
        System.out.println("请输入管理员的名字:");//next()：当接收到空格或者'就是表示结束
        String admin_name = scanner.nextLine();//老师说明,如果希望看到sql注入,这里需要使用nextLine()
        System.out.println("请输入管理员的密码:");
        String admin_pwd = scanner.nextLine();

        //通过Properties对象获取配置文件信息
        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter25\\mysql.properties"));
        //获取相关的值
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String driver = properties.getProperty("driver");
        String url = properties.getProperty("url");

        //1.注册驱动
        Class.forName(driver);//建议写上
        //2.得到连接
        Connection connection = DriverManager.getConnection(url, user, password);
        //3.得到PreparedStatement
        //3.1 组织sql,sql语句的?就相当于占位符  -- ? 占位符
        String sql = "select name , pwd from admins where name=? and pwd= ?";
        //3.2 preparedStatement对象实现了PreparedStatement接口的实现类对象
        //这里相当于是预处理
        java.sql.PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //3.3 给?赋值
        preparedStatement.setString(1,admin_name);
        preparedStatement.setString(2,admin_pwd);
        //4.执行select语句 s使用executeQuery
        //如果执行的是dml(update , insert , delete) executeUpdate()
        //这里执行executeQuery，不要再写sql
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next()){//如果查询到一条记录,则说明该管理员存在
            System.out.println("恭喜,登录成功");
        }else{
            System.out.println("对不起,登录失败");
        }
        //关闭连接
        resultSet.close();
        preparedStatement.close();
        connection.close();
    }
}
