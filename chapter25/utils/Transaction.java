package javalh.chapter25.utils;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Transaction {
    public static void main(String[] args) {
        //事务
        //1.基本介绍
        /**
         * 1.JDBC程序中当一个Connection对象创建的时候,默认情况下是自动提交事务的:每次执行一个
         * sql语句的时候，如果执行成功,就会向数据库自动提交,而且不能回滚
         * 2.JDBC程序中为了让多个sql语句作为一个整体执行,需要使用事务
         * 3.调用Connection的setAutoCommit(false)可以取消自动提交事务
         * 4.在所有SQL语句都成功执行后,调用Connection的commit():方法提交事务
         * 5.在其中某个操作失败或者出现异常的时候,调用Connection的rollback()；方法回滚事务
         * 模拟经典的转账业务
         */

        //操作转账的业务
        //1.得到连接
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = JDBCUtils.getConnection();
            //组织一个sql语句
//            String createSql = "create table accounts(" +
//                    "id int primary key auto_increment," +
//                    "name varchar(32) not null default ''," +
//                    "balance double not null default 0" +
//                    ") engine=innodb character set utf8";

            String add_sql = "insert into accounts values(null , '马化腾' , 10000)";
            preparedStatement = connection.prepareStatement(add_sql);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.close(null ,preparedStatement, connection);
        }

    }



    //没有使用事务
    @Test
    public void noTransaction(){
        //操作转账的业务
        //1.得到连接
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sql = "update accounts set balance = balance - 100 where id = 1";
        String sql2 = "update accounts set balance = balance + 100 where id = 2";
        try {
            connection = JDBCUtils.getConnection();//在默认情况下,connection默认是自动提交的
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();//执行第一条sql , 这条sql语句执行成功了
            int i = 1 / 0; //抛出异常
            preparedStatement = connection.prepareStatement(sql2);
            preparedStatement.executeUpdate();//执行第二条sql , 因为上面的抛出了异常,所以这条语句执行失败了

            //马云给马化腾转账
            //这样最后的结果是,马云的钱缺少减少了100块,但是马化腾却没有收到100元
            //这100元从人间蒸发了
            //这样的情况肯定是不允许的

            System.out.println("所有的sql语句都执行成功了");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.close(null , preparedStatement , connection);
        }
    }


    //没有使用事务
    @Test
    public void Transactions(){
        //操作转账的业务
        //1.得到连接
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sql = "update accounts set balance = balance - 100 where id = 1";
        String sql2 = "update accounts set balance = balance + 100 where id = 2";
        try {
            connection = JDBCUtils.getConnection();//在默认情况下,connection默认是自动提交的
            //将 connection 设置为不自动提交
            connection.setAutoCommit(false);//开启了事务
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();//执行第一条sql , 这条sql语句执行成功了
//            int i = 1 / 0; //抛出异常
            preparedStatement = connection.prepareStatement(sql2);
            preparedStatement.executeUpdate();//执行第二条sql , 因为上面的抛出了异常,所以这条语句执行失败了

            //马云给马化腾转账
            //这样最后的结果是,马云的钱缺少减少了100块,但是马化腾却没有收到100元
            //这100元从人间蒸发了
            //这样的情况肯定是不允许的

            System.out.println("所有的sql语句都执行成功了");

            //提交事务
            connection.commit();

        } catch (SQLException throwables) {
            try {
                //这里我们可以进行回滚,即撤销执行的sql
                //默认回滚到事务开始的状态
                System.out.println("执行发生了异常,撤销执行的sql");
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            throwables.printStackTrace();
        } finally {
            JDBCUtils.close(null , preparedStatement , connection);
        }
    }
}
