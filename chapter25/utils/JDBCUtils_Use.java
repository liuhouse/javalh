package javalh.chapter25.utils;

import org.junit.jupiter.api.Test;

import java.sql.*;

/**
 * @author 刘皓
 * @version 1.0
 */
public class JDBCUtils_Use {
    public static void main(String[] args) {

    }


    @Test
    public void testSelect() {
        //1.得到连接
        Connection connection = null;
        //2.声明sql语句
        String sql = "select * from actor where id = ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        //3.得到预处理的对象
        try {
            connection = JDBCUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, 3);
            resultSet = preparedStatement.executeQuery();
            System.out.println("id" + "\t" + "name" + "\t" + "sex" + "\t" + "borndate" + "\t" + "phone");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String sex = resultSet.getString("sex");
                Date borndate = resultSet.getDate("borndate");
                String phone = resultSet.getString("phone");
                System.out.println(id + "\t" + name + "\t" + sex + "\t" + borndate + phone);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.close(resultSet, preparedStatement, connection);
        }
    }


    @Test
    public void testDML() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            //得到连接
            connection = JDBCUtils.getConnection();
            //构建sql语句
            String sql = "update actor set name =? where id=?";
            preparedStatement = connection.prepareStatement(sql);
            //填充数据
            preparedStatement.setString(1, "樊振东");
            preparedStatement.setInt(2, 2);
            int rows = preparedStatement.executeUpdate();
            System.out.println(rows > 0 ? "操作成功" : "操作失败");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.close(null, preparedStatement, connection);
        }
    }
}
