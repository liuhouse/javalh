package javalh.chapter25.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 * 封装JDBCUtils【关闭连接 ， 得到连接】
 * 说明:在jdbc操作中,获取连接和释放资源是经常用到的,可以将其封装成JDBC连接的工具类
 *
 * 这是一个工具类,完成对mysql的连接和关闭资源
 */
public class JDBCUtils {
    //定义相关的属性(4个),因为只需要一份.因为,我们做成static
    private static String user;//用户名
    private static String password;//密码
    private static String url;//url
    private static String driver;//驱动名

    //因为是静态代码块先执行,所以在静态代码块中完成数据的初始化操作
    static {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("javalh\\chapter25\\utils\\mysql.properties"));
            //读取相关的属性
            user = properties.getProperty("user");
            password = properties.getProperty("password");
            url = properties.getProperty("url");
            driver = properties.getProperty("driver");
        } catch (IOException e) {
            //在实际开发中,我们可以这样处理
            //1. 将编译异常转成运行异常
            //2. 调用者,可以选择捕获该异常,也可以选择默认处理该异常,比较方便
            throw new RuntimeException(e);
        }
    }

    //连接数据库,返回Connection
    public static Connection getConnection() {
        try {
            //注册驱动
            Class.forName(driver);
            return DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            //1.将编译异常转成 运行异常
            //2.调用者,可以选择捕获该异常,也可以选择默认处理该异常,比较方便
            throw new RuntimeException(e);
        }
    }

    /**
     * 关闭相关的资源
     *
     * @param set        结果集
     * @param statement  Statement 或者 PreparedStatement
     * @param connection 连接
     *                   如果需要关闭资源,就传入对象,否则传入null
     */
    public static void close(ResultSet set, Statement statement, Connection connection) {
        //判断是否为null
        try {
            if (set != null) {
                set.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            //将编译异常转成运行异常抛出去
            throw new RuntimeException(e);
        }
    }

}
