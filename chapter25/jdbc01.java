package javalh.chapter25;



import com.mysql.cj.jdbc.Driver;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 * 这是第一个jdbc程序,完成简单的操作
 */
public class jdbc01 {
    public static void main(String[] args) throws SQLException {
        //jdbc程序编写步骤
        //1.注册驱动 - 加载Driver类
        //2.获取连接 - 得到Connection
        //3.执行增删改查 - 发送sql给mysql执行
        //4.释放资源 - 关闭相关连接

        //JDBC第一个程序
        //前置工作：在项目有下创建一个文件夹lib
        //将mysql.jar拷贝到该目录下,点击add to project...加入到项目中
        //1.注册驱动
        Driver driver = new Driver();//创建driver对象
        //2.得到连接
        //老师解读
        //(1)jdbc:mysql:// 规定好表示协议,通过jdbc的方式连接mysql
        //(2)localhost主机,可以是ip地址
        //(3)3306表示mysql监听的端口
        //(4)hsp_db03连接到mysql dbms的哪个数据库
        //(5)mysql的连接本质就是我们前面学过的socket连接

        String url = "jdbc:mysql://localhost:3306/hsp_db03";
        //将用户名和密码放入到Properties对象中
        Properties properties = new Properties();
        //说明user和password是提前规定好的,后面的值根据实际情况进行写
        properties.setProperty("user","root");//用户名
        properties.setProperty("password","root");//密码
        Connection connect = driver.connect(url, properties);

        //执行sql
        //String sql = "insert into actor values(null, '刘德华','男','1970-11-11','110')";
        //String sql = "update actor set name = '周星驰' where id = 1";
        String sql = "delete from actor where id = 1";

        Statement statement = connect.createStatement();
        int rows = statement.executeUpdate(sql);//如果是dml语句,返回的就是影响的行数
        System.out.println(rows > 0 ? "成功" : "失败");

        //4.关闭资源
        statement.close();
        connect.close();

    }
}
