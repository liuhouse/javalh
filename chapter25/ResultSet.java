package javalh.chapter25;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 * 演示select语句返回ResultSet，并取出结果集
 */
@SuppressWarnings({"all"})
public class ResultSet {
    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
        //1.表示数据库结果集的数据表,通常通过执行查询数据库的语句生成
        //2.ResultSet对象保持一个光标指向当前的数据行,最初,光标位于第一行之前
        //3.next()方法将光标移动到下一行,并且由于在ResultSet对象中没有更多行的时候返回false
        //因此可以在while循环中使用循环来遍历结果集
        //通过properties对象获取配置文件的信息
        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter25\\mysql.properties"));
        //获取相关的值
        //获取用户名
        String user = properties.getProperty("user");
        //获取密码
        String password = properties.getProperty("password");
        //获取驱动
        String driver = properties.getProperty("driver");
        //获取url
        String url = properties.getProperty("url");

        //1.注册驱动
        Class.forName(driver);//建议写上,不写也可以
        //2.得到连接
        Connection connection = DriverManager.getConnection(url, user, password);
        //3.得到Statement对象
        Statement statement = connection.createStatement();
        //4.组织sql语句
        String sql = "select id , name ,sex ,borndate from actor";
        //执行给定的sql语句,该语句返回单个的Result对象
        java.sql.ResultSet resultSet = statement.executeQuery(sql);

        //5使用while循环取出数据
        while (resultSet.next()){//让光标向后移动,如果没有更多航,则返回false
            int id = resultSet.getInt(1);//获取该行的第一列
            String name = resultSet.getString(2);//获取该的第二列
            String sex = resultSet.getString(3);//获取该行的第三列
            Date date = resultSet.getDate(4);//获取该行的第四列
            System.out.println(id + "\t" + name + "\t" + sex + "\t" + date);
        }

        //切记 - 关闭连接
        resultSet.close();
        statement.close();
        connection.close();


        //Statement基本介绍
        /**
         * 1.Statement对象,用于执行静态sql语句并返回其生成的结果的对象
         * 2.在连接建立之后,需要对数据库进行访问,执行命令或者是sql语句,可以通过
         *  Statement[存在sql注入]
         *  PreparedStatement[预处理]
         *  CallableStatement[存储过程]
         * 3.Statement对象,执行sql语句,存在sql注入风险
         * 4.sql注入是利用某些系统对用户输入的数据没有进行充分的检查,而在用户输入数据
         * 中注入非法的sql语句段或者命令,恶意攻击数据库
         * 5.要防范sql注入,只要用PreparedStaement(从Statement扩展而来)取代Statement就可以了
         *
         */
    }
}
