package javalh.chapter25;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings({"all"})
public class PreparedStatement2 {
    public static void main(String[] args) throws Exception {
        //基本介绍
        //1.PreparedStatement 执行的sql语句中的参数用问号(?)来表示,调用PreparedStatement对象的setXxx()
        //方法有两个参数,第一个参数是要设置的sql语句中的参数的索引(从1开始),第二个是设置sql语句中的参数的值
        //2.调用executeQuery() , 返回ResultSet对象
        //3.调用executeUpdate():执行更新,包括增,删,修改

        //预处理的好处
        //1.不再使用+拼接sql语句,减少语法错误
        //2.有效的解决了sql注入问题
        //3.大大的减少了编译次数,效率较高
        //通过Properties对象获取配置文件信息
        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter25\\mysql.properties"));
        //获取相关的值
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String driver = properties.getProperty("driver");
        String url = properties.getProperty("url");

        //1.注册驱动
        Class.forName(driver);//建议写上
        //2.得到连接
        Connection connection = DriverManager.getConnection(url, user, password);
        //3.得到PreparedStatement
        //构建sql语句
        String sql = "select * from employee where id = ? and user_name = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);

        //设置值,防止sql注入
        preparedStatement.setInt(1 , 1);
        preparedStatement.setString(2,"孙悟空");

        //执行sql
        ResultSet resultSet = preparedStatement.executeQuery();

        if(resultSet.next()){
            System.out.println("找到这个人了");
        }else{
            System.out.println("没有找到此人");
        }


        connection.close();
    }
}
