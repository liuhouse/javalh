package javalh.chapter25;

import javalh.chapter25.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class Exercise01 {
    public static void main(String[] args) {
        //课后练习
        //1.创建saccount表
        //2.在表中先添加两条记录 tom 余额100 , king 余额200
        //3.使用事务完成,tom给king转账10元
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        //构建sql语句
        String sql1 = "update saccount set balance = balance - 10 where id = 1";
        String sql2 = "update saccount set balance = balance + 10 where id = 2";

        //准备预处理对象
        try {
            connection = JDBCUtils.getConnection();
            connection.setAutoCommit(false);//关闭自动提交,开启事务
            preparedStatement = connection.prepareStatement(sql1);
            preparedStatement.executeUpdate();//执行sql语句1
//            int i = 1 / 0;
            preparedStatement = connection.prepareStatement(sql2);
            preparedStatement.executeUpdate();//执行sql语句2
            System.out.println("转账成功");
            connection.commit();
        } catch (SQLException throwables) {
            //如果这里碰到错误,就使用事务进行回滚
            try {
                System.out.println("转账的过程中出现了异常,转账失败...");
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            throwables.printStackTrace();
        } finally {
            //关闭资源
            JDBCUtils.close(null , preparedStatement,connection);
        }
    }
}
