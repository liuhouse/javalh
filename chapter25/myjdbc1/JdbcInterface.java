package javalh.chapter25.myjdbc1;

/**
 * @author 刘皓
 * @version 1.0
 * 我们规定jdbc的接口(方法)
 */
public interface JdbcInterface {
    //连接
    public Object getConnection();
    //crud
    public void crud();
    //关闭连接
    public void close();
}
