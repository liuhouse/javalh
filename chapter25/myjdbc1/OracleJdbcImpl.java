package javalh.chapter25.myjdbc1;

/**
 * @author 刘皓
 * @version 1.0
 * 模拟oracle数据库实现jdbc
 */
public class OracleJdbcImpl implements JdbcInterface{
    @Override
    public Object getConnection() {
        System.out.println("得到oracle的连接");
        return null;
    }

    @Override
    public void crud() {
        System.out.println("完成对oracle的增删改查");
    }

    @Override
    public void close() {
        System.out.println("关闭oracle的连接");
    }
}
