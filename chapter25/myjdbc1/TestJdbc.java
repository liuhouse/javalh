package javalh.chapter25.myjdbc1;

/**
 * @author 刘皓
 * @version 1.0
 * 测试jdbc
 */
public class TestJdbc {
    public static void main(String[] args) {
        //完成对Mysql的操作
        JdbcInterface jdbcInterface = new MysqlJdbcImpl();
        jdbcInterface.getConnection();//通过接口来调用实现类[动态绑定]
        jdbcInterface.crud();
        jdbcInterface.close();

        //完成对oracle的操作
        System.out.println("====================");
        jdbcInterface = new OracleJdbcImpl();
        jdbcInterface.getConnection();//通过接口来调用实现类[动态绑定]
        jdbcInterface.crud();
        jdbcInterface.close();


        //1.如果java直接访问数据库 - 那么,一旦数据库升级,java将不能维护,有可能都会连接不上
        //2.jdbc带来的好处,java提供统一接口,各数据库厂商实现这些接口,用户直接调用接口里面的方法即可
        //3.说明:JDBC是java提供的一套用于数据库操作接口的Api,java程序员只需要面对这套接口编程即可
        //不同的数据库厂商,需要针对这套接口,提供不同的实现

        //JDBC API是一系列的接口,它统一和规范了应用程序与数据库的连接,执行sql语句，并且得到返回的结果
        //的各类操作,相关类和接口在java.sql 与 javax.sql包中





    }
}
