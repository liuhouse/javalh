package javalh.chapter25.datasource;

import javalh.chapter25.utils.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.sql.Connection;

/**
 * @author 刘皓
 * @version 1.0
 * 数据库连接池
 * 5k次连接数据库的问题
 */
public class ConQuestion {
    public static void main(String[] args) {

    }

    /**
     * 需求：
     * 1.请编写程序完成连接Mysql5000次的操作
     * 2.看看有什么问题,耗时又是多久 .=> 数据库连接池
     */


    /**
     * 传统获取Connection的问题分析
     * 1.传统的JDBC数据库连接使用DriverManager来获取,每次向数据库建立连接的时候都要将
     *  Connection加载到内存中,再验证IP地址,用户名和密码(0.05s - 1s时间).需要数据库连接的时候
     *  就向数据库要求一个,频繁的进行数据库连接操作将占用很多的系统资源,容易造成服务器崩溃
     * 2.每一次数据库连接,使用完成之后都要断开,如果程序出现异常而未能关闭,将导致数据库的内存泄露
     *  最终将导致重新启动数据库
     * 3.传统获取连接的方式，不能控制创建的连接的数量,如果连接过多,也可能导致内存泄露,MYSql崩溃
     * 4.解决传统开发中的数据库连接问题,可以采用数据库的连接池技术 (connection pool)
     */

    /**
     * 数据源库连接池种类
     * 1.JDBC的数据库连接池使用java.sql.DataSource来表示,DataSource只是一个接口,该接口通常是由
     *  第三方提供实现[提供.jar]
     * 2.C3P0数据库连接池,速度相对比较慢,稳定性不错(hibernate , spring)
     * 3.DBCP数据库连接池,速度相对c3p0较快,但是不稳定
     * 4.Proxool数据库连接池,有监控连接池状态的功能,稳定性较c3p0差一点
     * 5.BoneCP数据库连接池,速度快
     * 6.Druid(德鲁伊)是阿里提供的数据库连接池,集DBCP , C3P0 , Proxool优点于一身的数据库连接池
     */

    @Test
    public void testCon(){
        //代码,连接Mysql5000次
        //看看连接 - 关闭 connection会耗用多久
        long start = System.currentTimeMillis();
        System.out.println("开始连接....");
        for(int i = 0 ; i < 500 ; i++){
            //使用传统的jdbc方式,得到连接
            Connection connection = JDBCUtils.getConnection();
            //做一些工作,比如得到PreparedStatement，发送sql
            //....
            //关闭
            JDBCUtils.close(null , null , connection);
        }
        long end = System.currentTimeMillis();
        //传统方式连接5000次,大概需要十分钟的时间
        System.out.println("传统方式5000次 耗时=" + (end - start));//传统方式500次  10227
    }
}
