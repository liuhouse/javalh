package javalh.chapter25.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author 刘皓
 * @version 1.0
 * 演示c3p0的使用
 */

public class C3P0_ {

    //方式1：相关参数，在程序中指定user , url , password
    @Test
    public void testC3P0_01() throws Exception {
        //1.创建一个数据源对象
        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        //2.通过配置文件mysql.properties获取相关的连接的信息
        Properties properties = new Properties();
        properties.load(new FileInputStream("javalh\\chapter25\\mysql.properties"));
        //读取相关的属性
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String url = properties.getProperty("url");
        String driver = properties.getProperty("driver");

        //给数据源comboPooledDataSource设置相关的参数
        //注意：连接管理是由comboPooledDataSource来管理
        comboPooledDataSource.setDriverClass(driver);
        comboPooledDataSource.setJdbcUrl(url);
        comboPooledDataSource.setUser(user);
        comboPooledDataSource.setPassword(password);

        //设置初始化连接数
        comboPooledDataSource.setInitialPoolSize(10);
        //设置最大的连接数据
        comboPooledDataSource.setMaxPoolSize(50);

        //测试连接池的效率,测试对mysql 5000次操作
        long start = System.currentTimeMillis();
        for(int i = 0 ; i < 5000 ; i++){
            //这个方法就是从DataSource接口实现的
            Connection connection = comboPooledDataSource.getConnection();
            connection.close();
        }
        long end = System.currentTimeMillis();
        //c3p0 5000 连接mysql耗时=964
        //所以使用数据库连接池是效率非常高的
        System.out.println("c3p0 5000 连接mysql耗时=" + (end - start));
    }

    //第二种方式 创建配置文件模板完成
    //1.将c3p0提供的c3p0.config.xml拷贝到src目录下
    //2.该文件指定了连接数据库和连接池的相关参数
    @Test
    public void testC3P0_02() throws SQLException {
        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource("hsp_db03");
        //测试5000次连接mysql
        Connection connection = comboPooledDataSource.getConnection();
        System.out.println("连接OK~");
        connection.close();
    }


}
