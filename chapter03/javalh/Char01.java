package javalh;

public class Char01 {
	public static void main(String[] args) {
		//字符类型可以表示单个字符,字符类型是char，char表示两个字节（可以存放汉子）
		//代码
		char c1 = 'a';
		char c2 = '\t';

		char c4 = 97;
		
		System.out.println(c1);
		System.out.println(c2);

		System.out.println(c4);
		
		
		
	}
}
