package javalh;

public class VarDetail {
	//编写一个main方法
	public static void main(String[] args) {
		//变量必须先声明,后使用,即有顺序
		//int a = 50;//int
		//该区域的数据/值可以在同一个类型内不断变化
		//a = 'jack';//这是错误的 -- 因为jack是字符串类型
		//a = 88;//这是正确的,因为是整形
		
		//变量在同一个作用域下不能重名
		//int a = 77;//错误 -- 因为变量在同一个作用域下不能重名
		
		
		
		//对于整形的 + 运算 总结
		/**
		 * 1：当左右两边都都是数值类型的时候，则做加法运算
		 * 2：当左右两边有一方为字符串的时候,则做拼接运算
		 * 3：运算顺序，从做导游
		 * 4：如果前面两个是数值第三个是字符串，则前两个先进行相加,然后结果和第三个进行拼接
		 * */
		
//		System.out.println(100 + 98);
//		System.out.println("100" + 98);
//		
//		System.out.println(100 + 3 + "hello");
//		System.out.println("hello" + 100 + 3);
		
		//System.out.println("最后打印出的结果是"+a);
		
		
		
		
		//整数类型
		byte n1 = 127;
//		System.out.println(n1);
		short n2 = 10;
		System.out.println(n2);
		int n3 = 10;//四个字节
		System.out.println(n3);
		long n4 = 1000;//8个字节
		System.out.println(n4);
	}
}



class Dog{
	public static void main(String[] args) {
		int a = 666; // 这是正确的
		System.out.println(a);
	}
}


