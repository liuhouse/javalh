package javalh;

public class Homework03 {
	public static void main(String[] args) {
		//编程,保存两本书名,用+拼接 看效果
		String book1 = "三国演义";
		String book2 = "天龙八部";
		System.out.println(book1  + " " +book2);
		//保存两个性别,用加号拼接,看效果
//		char sex1 = '男';
//		char sex2 = '女';
//		System.out.println((sex1 + sex2));//52906  char类型在进行相加的时候,会先转化为int型,也就是对应的ascill码
		//保存两本书的价格,用加号拼接 看效果
		float price1 = 25.1f;
		float price2 = 99.9f;
		System.out.println(price1 + price2);
		
	}
}
