package javalh;

public class IntDetail {
	public static void main(String[] args) {
		//java的整数类型就是用于存放整数值得,比如12,30,654
		byte n1 = 10; // 1个字节  = 8bit
		short n2 = 10; //2个字节 = 16bit
		int n3 = 10; //4个字节 = 32bit
		long n4 = 10; //8个字节 = 64bit
		
		//整形的使用细节
		//java的整形常量(具体值) 默认为Int型,声明long型常量后面需要加上 L 
		int n5 = 1;//四个字节
		//int n6 = 1L;//对不对？  不对  因为 1L是八个字节的   而int只能存放四个字节   就好像把一个大盒子要放到小盒子里面,所以是错误的
		long n7 = 1L;//这是对的  因为long 本来就是八个字节   所以可以
		
		
		
	}
}
