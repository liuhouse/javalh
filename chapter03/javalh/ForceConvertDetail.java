package javalh;

public class ForceConvertDetail {
	//强制类型转化细节说明
	public static void main(String[] args) {
		//1:当进行数据的大小转化的时候(从大到小),就需要使用到强制转化
		//2:强制符号只针对与最近的操作数有效,往往会使用小括号提升优先级
		//int x = (int)10*3.5+6*1.5; // double => int  这是错误的
		int x = (int)(10*3.5+6*1.5); // int()  最终的结果类型是Int  所以可以使用int来进行接收
		//System.out.println(x);
		//3:char类型可以保存int的常量值,但是不能保存int的变量值,需要强制转化
		char c1 = 100; //ok
		int m = 100; //ok
		//char c2 = m;//错误的  -- 使用变量的时候,会首先进行类型的判断 ，  m是int   char低精度不能保存高精度的变量
		char c3 = (char)m; // 强制类型转化之后就可以了,因为m = 100  现在已经是char类型了
		System.out.println(c3);
		//4:byte,short,char类型在进行运算的时候,当做int类型处理
		byte h1 = 100;
		char h2 = 'c';
		//int h_result = h1 + h2;
		//float h_result = h1 + h2;
		double h_result = h1 + h2;
		System.out.println(h_result);
		
		
		
	}
}
