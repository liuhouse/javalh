package javalh;

public class ForceConvert {
	public static void main(String[] args) {
		//介绍
		//自动类型转换的逆过程,将容量大的数据转化为容量小的数据类型,使用的时候要加上()，但是可能造成精度的降低或者溢出
		int i = (int)1.9;
		//System.out.println(i);//1  精度严重的损失了
		
		int j = 10000;
		byte b1 = (byte)j;
		//当超过byte的范围的时候,数据就溢出了，当在byte范围内,还是正常的
		System.out.println(b1);
		//所以,总结一点,数据不能轻易的进行转化,除非是有需求,否则很有可能造成数据的错误
	}
}
