package javalh;

public class FloatDetail {
	public static void main(String[] args) {
		//java浮点型常量(具体值),默认为double型的,声明float型常量,需要在后面加上 "f" 或者 "F"
		//float num1 = 1.1;//这是不对的,因为 小数的具体值为double双精度  占用8个字节   而 float只能存放4个字节 ，所以错误
		float num2 = 1.1F;//这是正确
		double num3 = 1.1;//这是对的
		double num4 = 1.1F;//这是对的
		
		//十进制数形式 ： 如 5.12   5.12f   .512 (这个是必须有小数点的)
		double num5 = .123;//这里相当于是0.123  双精度 前面的  0 是可以省略的
		//System.out.println(num5);
		
		//科学计数法的形式：如: 5.12e12 [5.12*10的二次方]    5.12e-2 [5.12 / 10的二次方]
		//System.out.println(5.12e2); // 512.0  因为5.12是双精度的,所以计算之后的值也应该是双精度的
		//System.out.println(5.12e-2); // 0.0512
		
		
		//通常情况下,应该使用double双精度型的,因为double比float型更精确
		//使用float会有精度损失
		//举例说明
		double num9 = 2.1234567851;//2.1234567851
		float num10 = 2.1234567851F;//2.1234567  
		//System.out.println(num9);
		//System.out.println(num10);
		
		//浮点数的使用陷阱 : 2.7  和  8.1/3 比较
		//看看一段代码
		double num11 = 2.7;
		double num12 = 8.1 / 3; // 这个计算跟人的脑子计算的是不一样的     计算机认为 这个2.6999999999999997 不是2.7 而是一个无限接近2.7的数
		//System.out.println(num12);
		
		
		//得到一个重要的使用点,当我们对运算结果是小数的进行结果的相等判断的时候,要小心
		//应该是以两个数的差值的绝对值,在某个精度范围内来进行判断
		//System.out.println(num11 == num12);
		
		
		//if(num11 == num12) {
			//System.out.println("num11 == num2 相等");
		//}
		
		
		//注释的正确写法.ctrl+/ 进行注释 , 再次输入就取消注释
		
//		if(Math.abs(num11 - num12) < 0.000001) {
//			System.out.println("差值非常小,到我的规定精度,认为是相等的....");
//		}
		
		
		//可以通过javaApi 来看
		
		//System.out.println(Math.abs(num11 - num12));
		
		//细节,如果是直接查询得到的小数或者赋值,是可以判断相等的
	}
}
