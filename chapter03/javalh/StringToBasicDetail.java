package javalh;

public class StringToBasicDetail {
	//在将字符串转化成基本数据类型的时候,要确保String类型能够转成有效的数据,比如我们可以把'123'转化成一个整数,但是不能把一个 'hello'转化成一个整数
	//如果格式不正确的话,就会抛出异常,程序就会终止
	//编写一个main方法
	public static void main(String[] args) {
		String str = "hello";
		//将字符串str转化成int
		int n1 = Integer.parseInt(str);
		System.out.println(n1);
	}
}
