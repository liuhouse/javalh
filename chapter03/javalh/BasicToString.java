package javalh;

public class BasicToString {
	public static void main(String[] args) {
		//基本数据类型转化为字符串     语法,将基本数据类型的值 + ""
		int n1 = 100;
		float n2 = 1.1f;
		double n3 = 3.14;
		boolean b1 = true;
		
		//进行字符串的转化   + ""
		String str1 = n1 + "";
		String str2 = n2 + "";
		String str3 = n3 + "";
		String str4 = b1 + "";
		
		
		//输出来的结果似乎没有什么区别,但是其实已经将这些变量全部转化为字符串了
		System.out.println(str1 + 1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
		
	}
}
