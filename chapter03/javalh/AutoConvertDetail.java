package javalh;
//自动类型转化细节
public class AutoConvertDetail {
	//编写一个main方法
	public static void main(String[] args) {
		//细节1：有多种类型的数据混合运算的时候，
		//系统首先自动将所有的数据类型转化 成容量最大的那种数据类型,然后再进行计算
		int n1 = 10;//ok
		//float d1 = n1 + 1.1;  //错误的  n1 +1.1 => 结果类型是double  因为运算的时候先将所有的数据类型转化为较大精度的double类型 而float不能存放double
		//上面错误的代码的修改意见是 1
		//double d1 = n1 + 1.1;//这是争取的   结果类型是  double
		
		//上面错误代码的修改意见2
		float d1 = n1 + 1.1F; // 运算结果的类型是 float
		//System.out.println(d1);
		
		
		//细节2：当我们把精度大的数据类型 赋值给精度（容量）小的数据类型的时候,就会报错,反之就会自动类型转化
		//int n2 = 1.1; // 这是错误的   double -> int
		
		
		//细节3 (byte,short) 和 char 之间不会相互自动转化
		//当把具体数赋给byte的时候 , (1)先判断该具体数是否在byte范围内,如果是就可以直接展示读取，就不会去进行数据类型转化了
		byte b1 = 10;//这是对的 -128 - 127
		int n2 = 1; //这是对的 1是int
		//byte b2 = n2;//错误的,原因,如果是变量赋值,则直接判断类型    n2是整形的,已经占用了四个字节,byte只有一个字节,所以是错误的
		
		//java有个规定.byte,short 和 char之间不能进行数据类型转化
		short n3 = 10;
		//char c1 = b1;
		//char c2 = n3;
		
		
		
		//细节4：byte,short,char 他们三者之间是可以计算的,在计算的时候首先转化为int类型
		byte b2 = 1;
		byte b3 = 2;
		short s1 = 1;
		//short s2 = b2 + s1;//这是错误的  b2 + s1  => int    使用short 接收int 是错误的
		int s2 = b2 + s1;//对的  b2 + s1 => int   所以使用int接收int类型是完全没有问题的
		//System.out.println(s2);
		
		
		//byte b4 = b2 + b3;//错误的   b2 + b3 => int 
		
		//boolean不参与转化
		boolean pass = true;
		//int num100 = pass;//错误的 boolean不参与自动类型的转化
		
		//自动提升原则：表达结果的类型自动提升为操作数中的最大类型
		//看一道题
		byte b4 = 1;
		short s3 = 100;
		int num200 = 1;
		float num300 = 1.1F;
		
		//表达式的结果类型是操作数中的最大值
		double num500 = b4 + s3 + num200 + num300; //最终结果  float -> double
		System.out.println(num500);
		
		
		
		
		
		
	}
}
