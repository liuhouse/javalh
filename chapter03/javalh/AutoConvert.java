package javalh;

public class AutoConvert {
	public static void main(String[] args) {
		//介绍
		//当java程序在进行赋值或者运算的时候,精度小的类型会自动转化为精度大的类型,这个就是自动类型转化
		//数据类型按照精度(容量)大小排序(背，规则)
		//char -> int -> long -> float -> double  double双精度是最大的
		//byte -> short -> int -> long -> float -> double    double双精度是最大的
		//看一个基本的案例
		int a = 'c'; //char 类型  转化为 int   这是正确的     ===> 99
		System.out.println(a);
		double d = 80;//int 类型  转化为  double    double双精度是最高级别的   所以 使用double是完全没有问题的
		System.out.println(d);//80.0    当int类型转化为double的时候    类型就转化为double双精度了    80.0
		
		float c = 100;//int -> float  这是正确的   低精度向高精度转化
		System.out.println(c);
	}
}
