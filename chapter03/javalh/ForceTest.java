package javalh;

public class ForceTest {
	public static void main(String[] args) {
		//判断是否能否通过编译
		short s = 12; //ok
		//s = s - 9;//错误的  因为返回的类型是 int 
		
		byte b = 10; //ok -127 - 128
		//b = b + 11; //错误的   因为返回的类型是int   byte 不能接收int   因为byte 比 int 的精度(容量)低
		b = (byte) (b +11);//正确的   返回的结果使用了强制转换   byte   所以是对的
		
		
		char c = 'a';//ok
		int i = 16; //ok
		float d = .314f;//ok
		double result = c + i + d;//ok float -> double
		//System.out.println(result);
		//System.out.println((int)c);
		
		
		byte h1 = 16;//ok
		short h2 = 14;//ok
		//short t = h1 + h2; // 错误的   int     short 不能 保存int
		short t = (short)(h1 + h2);
		System.out.println(t);
		
		
		
		
	}
}
