package javalh;

public class Boolean01 {
	public static void main(String[] args) {
		//演示判断成绩是否通过的案例
		//定义一个布尔变量
		boolean pass = true;
		if(pass) {
			System.out.println("通过考试");
		}else {
			System.out.println("未通过考试");
		}
	}
}
