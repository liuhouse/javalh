package javalh;

public class StringToBasic {
	public static void main(String[] args) {
		//将字符串转化为基本的数据类型
		String s1 = "123";
		//会在oop讲对象和方法的时候详细讲解
		//解读 使用  基本数据类型对应的包装类,的相对应的方法，得到基本数据类型
		int num1 = Integer.parseInt(s1);
		//将字符串转化为整数
		System.out.println(num1);
		//将字符串转化为double类型
		double num2 = Double.parseDouble(s1);
		System.out.println(num2);
		//将字符串转化为float类型
		float num3 = Float.parseFloat(s1);
		System.out.println(num3);
		//将字符串转化为long类型
		long num4 = Long.parseLong(s1);
		System.out.println(num4);
		//将字符串转化为Byte类型
		byte num5 = Byte.parseByte(s1);
		System.out.println(num5);
		//将字符串转化为布尔类型
		boolean b = Boolean.parseBoolean("true");
		System.out.println(b);
		short num6 = Short.parseShort(s1);
		System.out.println(num6);
		
		//怎么把字符串转成字符,char -> 含义是指 把字符串的第一个字符得到  因为char只能保存一个字符
		//解读   s1.charAt(0) 得到  s1字符串中的第一个字符 1
		System.out.println(s1.charAt(0));
		
		
	}
}
