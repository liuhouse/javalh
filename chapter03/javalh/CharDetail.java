package javalh;

public class CharDetail {
	//编写一个main方法
	public static void main(String[] args) {
		//在java中,char的本质是一个整数,在默认输出的时候,是unicode对应的字符
		//要输出对应的字符,可以(int)字符
		char c1 = 97;
		System.out.println(c1);//a 默认会输出97对应的unicode码
		
		char c2 = 'a';//输出a对应的数字
		System.out.println((int)c2); // 97
		System.out.println(c2);
		
		
//		char c3 = '韩';
//		System.out.println((int)c3);//38889
		
		
		char c4 = 38889;
		System.out.println(c4);//输出c4这个unicode编码对应的字符串 韩
		
		System.out.println(0 + c4);
		
		//char类型是可以进行运算的,相当于一个整数,因为他有对应的Unicode码
		//如果使用的是单引号  参与运算的时候   会将a 转化为对应的Unicode编码,97+10 = 107
		//如果使用的是双引号,参与运算的时候   会直接将a和10进行拼接
		System.out.println('a' + 10);
		
		
		
		//课堂小测试
		char c5 = 'b' + 1;//98 + 1 => 99
		System.out.println((int)c5); // 加上int就是99    不加int就是默认输出  对应的99对应的Unicode编码   c
		System.out.println(c5);
		
		
		
		
		
		
		
		
		
		
	}
}
