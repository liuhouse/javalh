package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp12 {
    public static void main(String[] args) {
        String content = "522543396666612345-333666999";
        //1.要匹配两个连续的相同数字 : (\\d)\\1
//        String regStr = "(\\d)\\1";

        //2.要匹配五个连续的相同数字：(\\d)\\1{4}
        //String regStr = "(\\d)\\1{4}";

        //3.要匹配个位与千位相同,十位与百位相同的数 5225 , 1551 , (\\d)(\\d)\\2\\1
        //String regStr = "(\\d)(\\d)\\2\\1";

        //4.思考题
        //请在字符串中检索商品编号,形式如:12321-333999111这样的号码,要求满足前面是一个五位数,然后一个 - 号，然后是一个九位数
        //连续的每三个要相同
        String regStr = "\\d{5}-(\\d)\\1{2}(\\d)\\2{2}(\\d)\\3{2}";

        Pattern pattern = Pattern.compile(regStr);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
            System.out.println("找到 =" + matcher.group(0));
        }
    }
}
