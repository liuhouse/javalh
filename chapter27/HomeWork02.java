package javalh.chapter27;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork02 {
    public static void main(String[] args) {
        //提示：要求验证是不是整数或者小数
        //提示：这个题要考虑是正数和负数
        //比如 123 -345 34.89 -97.9 -1.01 0.45
        String content = "10.9";

        String regStr = "[-+]?([1-9]\\d+|0)(\\.\\d+)?";

        if(content.matches(regStr)){
            System.out.println("是数字");
        }else{
            System.out.println("不是数字");
        }
    }
}
