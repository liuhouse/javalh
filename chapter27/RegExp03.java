package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp03 {
    public static void main(String[] args) {
        //元字符 - 字符匹配符
        /**
         1 . [] 可接收的字符列表    [efgh]   e,f,g,h中的任意一个字符
         2 . [^] 不接收的字符列表   [^abc] 除a,b,c之外的任意一个字符,包括数字和特殊符号
         3 . -   连字符           A-Z  任意单个大写字母
         4 . .  匹配除\n以外的任何一个字符    a..b    以a开头,b结尾,中间包含2个任意字符的长度为4的字符串  ascb aaab  aefb   a#*b
         5 . \\d  匹配单个数字字符,相当于[0-9]  \\d{3}(\\d)?  包含3个或者4个数字的字符串   123 , 6789
         6 . \\D  匹配单个非数字字符,相当于[^0-9]   \\D(\\d)*  以单个非数字字符开头,后接任意个数的数字字符串   a , A4564
         7 . \\w  匹配单个数字,大小写字母字符,相当于[0-9a-zA-Z] \\d{3}\\w{4} 以3个数字字符开头的长度为7的数字字母字符串  234asad   123abcd
         8 . \\W  匹配单个非数字 ， 大小写字母字符,相当于[^0-9a-zA-Z]  \\W+\\d{2}  以至少1个非数字,字母字符开头,2个数字字符结尾的字符串 #29 #@$10
         */

        String content = "a11c8abc_ABCy. @";
        //匹配a-z之间的任意一个字符
        //String regStr = "[a-z]";

        //匹配[A-Z]之间的任意一个字符
        //String regStr = "[A-Z]";

        //匹配 abc 字符串[默认区分大小写]
        //String regStr = "abc";

        //匹配 abc 字符串 不区分大小写
        //String regStr = "(?i)abc";

        //匹配0-9之间的任意一个字符
        //String regStr = "[0-9]";

        //匹配不在a-z之间的任意一个字符
        //String regStr = "[^a-z]";

        //匹配不在0-9之间的任意一个字符
        //String regStr = "[^0-9]";

        //匹配在 abcd 中任意一个字符
        //String regStr = "[abcd]";

        //匹配不在0-9之间的任一字符
        //String regStr = "\\D";

        //匹配大小写英文字母,数字,下划线
        //String regStr = "\\w";

        //匹配不在 大小写字母,数字,下划线的任意内容
        //String regStr = "\\W";

        // \\s 匹配任何的空白字符
        //String regStr = "\\s";

        //匹配任何的非空白字符,和\\s刚好相反
        //String regStr = "\\S";

        // . 匹配除\n之外的所有的字符,如果要匹配.本身,需要使用\\.
        //String regStr = ".";
        String regStr = "\\.";

        //创建Pattern对象
        //Pattern.CASE_INSENSITIVE 不区分大小写
        Pattern pattern = Pattern.compile(regStr/*,Pattern.CASE_INSENSITIVE*/);
        //创建匹配器 匹配内容
        Matcher matcher = pattern.matcher(content);

        while(matcher.find()){
            System.out.println("找到" + matcher.group(0));
        }

    }
}
