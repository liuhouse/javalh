package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp04 {
    public static void main(String[] args) {
        //在匹配某个字符串的时候是选择性的,即：既可以匹配这个,又可以匹配那个,这个时候你需要用到 选择匹配符号 |
        String content = "hanshunping 韩 寒冷";
        String regStr = "han|韩|寒";
        Pattern pattern = Pattern.compile(regStr);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
            System.out.println("找到" + matcher.group(0));
        }
    }
}
