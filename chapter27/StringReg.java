package javalh.chapter27;

/**
 * @author 刘皓
 * @version 1.0
 */
public class StringReg {
    public static void main(String[] args) {
        //String类中使用正则表达式
        //1.替换功能  replaceAll(String regex , String replacement);
        //2.判断功能 public boolean matches(String regex){} //使用Pattern和Matcher类
        //3.分割功能 split

        String content = "2000 年 5 月，JDK1.3、JDK1.4 和 J2SE1.3 相继发布，" +
                "几周后其获得了 Apple 公司 Mac OS X 的工业标准的支持。2001 年 9 月 24 日，" +
                "J2EE1.3 发布。 + 2002 年 2 月 26 日，J2SE1.4 发布。自此 Java 的计算能力有了大幅提升";

        //使用正则表达式的方式,将 JDK1.3 和 JDK1.4 替换成JDK
        content = content.replaceAll("JDK1\\.3|JDK1\\.4" , "JDK");
//        System.out.println(content);

        //要求 验证一个手机号,要求必须是以138 , 139 开头的
//        content = "13877777888";
//        if(content.matches("1(38|39)\\d{8}")){
//            System.out.println("验证成功");
//        }else{
//            System.out.println("验证失败");
//        }


        //要求按照#或者-或者~或者数字来分割
        System.out.println("===================");
        content = "hello#abc-jack12smith~北京";
        String[] split = content.split("#|-|\\d+|~");
        for (String s : split){
            System.out.println(s);
        }


    }
}
