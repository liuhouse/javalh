package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp07 {
    public static void main(String[] args) {
        /*
            分组
            1.(pattern)   非命名捕获,捕获匹配的子字符串,编号为0的第一个捕获是由整个正则表达式模式匹配的文本,其他捕获结果则是根据左括号的顺序从1开始自动编号
            2.(?<name>pattern) 命名捕获,将匹配的子字符串捕获到一个组名或者编号名称中,用于name的字符串不能包含任何的标点符号,并且不能以数字开头,可以使用
                单引号代替尖括号 例如 (?'name' pattern)

         */

        String content = "hanshunping s7788 nn1156han";

        //下面就是非命名分组

        //匹配4个数字字符串
        //1.matcher.group(0) 得到匹配的字符串
        //2.matcher.group(1) 得到匹配字符串的第一个分组的内容
        //3.matcher.group(2) 得到匹配到的字符串的第二个分组的内容

        //String regStr = "(\\d\\d)(\\d\\d)";

        //命名分组 : 即可以给分组命名
        //匹配四个数字的字符串
        String regStr = "(?<g1>\\d\\d)(?<g2>\\d\\d)";


        Pattern pattern = Pattern.compile(regStr);

        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
            System.out.println("找到 " + matcher.group(0));
            System.out.println("分组1 " + matcher.group(1));
            System.out.println("第一个分组的内容[通过命名]=" + matcher.group("g1"));
            System.out.println("分组2 " + matcher.group(2));
            System.out.println("第二个分组的内容[通过命名] " + matcher.group("g2"));
        }
    }
}
