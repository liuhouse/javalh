package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp10 {
    public static void main(String[] args) {
        //应用实例
        //对字符串进行如下验证
        //String content = "韩顺平教育";
        String content = "14525246511";
        //1.汉字
        //String regStr = "^[\u0391-\uffe5]+$";
        //2.邮政编码 - 要求:是1-9开头的六位数,比如123890
        //String regStr = "^[1-9]\\d{5}$";
        //3.QQ号码 要求：是1-9开头的一个(5位数 - 10位数) 比如 : 12389 , 1345678 , 187698765
        //String regStr = "^[1-9]\\d{4,9}$";
        //4.手机号码 要求 - 必须以 13 , 14 , 15 , 18开头的11位数字,比如 13588889999
        String regStr = "^1[3|4|5|8]\\d{9}$";

        Pattern pattern = Pattern.compile(regStr);
        Matcher matcher = pattern.matcher(content);
        if(matcher.find()){
            System.out.println("正确匹配到");
        }else{
            System.out.println("未匹配到");
        }
    }
}
