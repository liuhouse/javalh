package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp11 {
    public static void main(String[] args) {
        String content = "https://www.bilibili.com/video/BV1gQ4y1S75S?spm_id_from=333.851.b_7265706f7274466972737432.6";
        /**
         * 思路
         * 1.先确定url的开始部分 https:// | http://   --  (http|https)://$
         * 2.然后通过匹配域名   www.bilibili.com  -- ([\w-]+\.)+([\w-]+)
         * 3.匹配后面的参数  /video/BV1gQ4y1S75S?spm_id_from=333.851.b_7265706f7274466972737432.6  -- (\/?[\w-?=.&#/%]*)?
         */

        //这里需要注意的是: [.?*] 这些在 [] 中定义的特殊字符,表示匹配的就是本身
        String regStr = "^((http|https)://)([\\w-]+\\.)+([\\w-]+)(\\/?[\\w-?=.&#/%*]*)?$";

        Pattern pattern = Pattern.compile(regStr);

        Matcher matcher = pattern.matcher(content);

        if(matcher.find()){
            System.out.println("满足格式");
        }else{
            System.out.println("不满足格式");
        }


    }
}
