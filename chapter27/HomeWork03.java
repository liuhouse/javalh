package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork03 {
    public static void main(String[] args) {
        //对一个url进行解析
        String content = "http://www.sohu.com:8080/abc/index.htm";
        //(a) 要求得到的协议是什么?  http
//        String regStr = "http|https";
        //(b) 域名是什么? www.sohu.com
        //regStr = "www\\.([a-zA-Z]+\\.)+[a-zA-Z]+";
        //(c) 端口是什么 8080
        //regStr = "\\d+";
        //(d)文件名是什么
//        regStr = "[\\w]+\\.(htm|html|php|java)";
        String regStr = "(http|https):\\/\\/(([a-zA-Z]+\\.)+[a-zA-Z]+):(\\d+)(\\/[\\w]+\\/)+([a-zA-Z]+\\.[a-zA-Z]+)";
        Pattern pattern = Pattern.compile(regStr);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
//            System.out.println(matcher.group(0));
            System.out.println(matcher.group(1));
            System.out.println(matcher.group(2));
            System.out.println(matcher.group(3));

            System.out.println(matcher.group(4));
            System.out.println(matcher.group(5));
        }
    }
}
