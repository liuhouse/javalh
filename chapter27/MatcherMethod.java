package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 * Matcher类的常用方法
 */
public class MatcherMethod {
    public static void main(String[] args) {
        String content = "hello edu jack hspedutom hello smith hello hspedu hspedu";
        String regStr = "hello.*";

        Pattern pattern = Pattern.compile(regStr);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
            System.out.println("=======================");
            //找到每一个hello 出现的开始位置和结束位置
            System.out.println(matcher.start());
            System.out.println(matcher.end());
            //也可以这样读取到匹配的数据
            System.out.println("找到：" + content.substring(matcher.start() , matcher.end()));
        }

        //整体匹配方法,常用于效验某个字符串是否满足某个规则
        //这里是不满足规则的,因为字符串和规则不能整体匹配
        System.out.println("整体匹配 = " + matcher.matches());


        //完成如果content有hspedu替换成 韩顺平教育
        regStr = "hspedu";
        pattern = Pattern.compile(regStr);
        matcher = pattern.matcher(content);

        String newContent = matcher.replaceAll("韩顺平教育");
        System.out.println("newContent = " + newContent);

        //原来的字符串并没有发生变化
        System.out.println("content = " + content);




    }
}
