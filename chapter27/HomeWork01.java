package javalh.chapter27;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01 {
    public static void main(String[] args) {
        //1.验证电子邮件是否合法
        //陆游：纸上得来终觉浅,绝知此事要躬行 ， 做几个练习

        //规定电子邮件的规则为
        //1.只能有一个@
        //2.@前面是用户名,可以是a-zA-Z0-9_-字符
        //3.@后面是域名,并且域名只能是英文字母,比如 sohu.com 或者 tsinghua.org.cn
        //4.写出对应的正则表达式,验证输入的字符串是否满足规则

        //1.先写一个正确的字符串
        String content = "liuhao@qq.com";
        //2.编写正则
        String regStr = "^[\\w-]+@([a-zA-Z]+\\.)+[a-zA-Z]+$";
        boolean matches = content.matches(regStr);
        if(matches){
            System.out.println("邮箱匹配成功...");
        }else{
            System.out.println("邮箱匹配失败...");
        }

    }
}
