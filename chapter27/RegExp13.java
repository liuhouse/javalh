package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp13 {
    public static void main(String[] args) {
        String content = "我....我要...学学学学...编程java";

        //1.去掉所有的.
        Pattern pattern = Pattern.compile("\\.");
        Matcher matcher = pattern.matcher(content);
        //将所有的点匹配成空字符串
        content = matcher.replaceAll("");
//        System.out.println(content);

        //2.去掉重复的字 我我要学学学学编程java
        //思路
        //(1) 使用(.)\\1+   一个字符至少出现两次
        //(2) 使用 反向引用$1 来替换匹配到的内容

        //注意：因为正则表达式变化,所以需要重置 matcher
        //至少两个连着相同的任意字符
//        pattern = Pattern.compile("(.)\\1+");
//        matcher = pattern.matcher(content);
//        while (matcher.find()){
//            System.out.println("找到=" + matcher.group(0));
//        }
//
//       //使用反向引用$1来替换匹配到的内容
//        //这里需要说明的是 ， 这里的$1 匹配到的是  每一个重复字符的 单个字符 ， replaceAll  内部会进行循环处理
//        //使用找到的单个字符,替换正则规则匹配到的字符串
//        content = matcher.replaceAll("$1");
//        System.out.println("content=" + content);

        //3.使用一条语句 去掉重复的字
        //使用匹配到的单个重复字符去替换正则匹配到的内容
        content = Pattern.compile("(.)\\1+").matcher(content).replaceAll("$1");
        System.out.println(content);

    }
}
