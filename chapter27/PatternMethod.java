package javalh.chapter27;

import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class PatternMethod {
    public static void main(String[] args) {
        /*
            正则表达式常用的三个类
            java.util.regex 包主要包括下面三个类 Pattern类 ， Matcher类 , PatternSyntaxException

            1.Pattern类
                pattern对象是一个正则表达式对象,Pattern类没有公共构造方法,要创建一个Pattern对象,调用其公共静态方法
                它返回一个Pattern对象,该方法接收一个正则表达式作为他的第一个参数 . 比如 Pattern r = Pattern.compile(pattern);

            2.Matcher类
                Matcher对象是对输入字符串进行解释和匹配的引擎,与Pattern类一样,Matcher也没有公共构造方法,你需要调用Pattern对象的matcher
                方法来获得一个Matcher对象

            3.PatternSyntaxException是一个非强制的异常类,他表示一个正则表达式模式中的语法错误
         */
        
        //这里演示matches方法,用于整体匹配,在验证输入字符串是否满足条件使用
        String content = "hello abc hello,韩顺平教育";

        //使用这种方式不能整体匹配
        //String regStr = "hello";
        //使用整体匹配
        String regStr = "hello.*";
        //这里代表的是全匹配
        boolean matches = Pattern.matches(regStr, content);
        System.out.println("整体匹配=" + matches);
    }
}
