package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork03_1 {
    public static void main(String[] args) {
        String content = "http://www.suhu.com:8080/abc/index.htm";
        String regStr = "^([a-zA-Z]+)://([a-zA-Z.]+):(\\d+)[\\w-/]+([\\w.]+)$";

        Pattern pattern = Pattern.compile(regStr);
        Matcher matcher = pattern.matcher(content);
        System.out.println(matcher.matches());
        //整体匹配,如果匹配成功,可以通过group(x),获取对应分组的内容
        if(matcher.matches()){
            System.out.println("整体匹配" + matcher.group(0));
            System.out.println("协议:" + matcher.group(1));
            System.out.println("域名:" + matcher.group(2));
            System.out.println("端口:" + matcher.group(3));
            System.out.println("文件:" + matcher.group(4));
        }else{
            System.out.println("没有匹配成功");
        }
    }
}
