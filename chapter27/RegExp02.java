package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp02 {
    public static void main(String[] args) {
        //正则表达式语法
        //基本介绍
        //如果想要灵活的运用正则表达式,必须了解其中各种的元字符功能,元字符从功能上大致分为
        //1.限定符
        //2.选择匹配符
        //3.分组组合和反向引用符
        //4.特殊字符
        //5.字符匹配符
        //6.定位符

        //元字符(Metacharacter) - 转义号 \\
        //    \\符号,说明：在我们使用正则表达式去检索某些特殊字符的时候,需要使用到转义符号,否则 匹配不到结果,甚至是会报错的
        // 案例：使用$去匹配 "abc$(" 会怎么样

        //这里需要特别的说明一下
        //在java的正则表达式中,两个\\代表其他语言的一个\

        //需要用到转移符号的字符有下面的符号
        // . * + ( ) $ \ / ? [ ] ^ {}

        //演示转移符号的使用
        //定义字符串
        String content = "abc$(a.bc(123())";
        //定义正则规则
        // -- 匹配 (
        //String regStr = "\\(";
        // -- 匹配 .
        //String regStr = "\\.";
        // -- 匹配$
        String regStr = "\\$";
        Pattern pattern = Pattern.compile(regStr);
        //匹配器
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
            System.out.println("找到" + matcher.group(0));
        }

    }
}
