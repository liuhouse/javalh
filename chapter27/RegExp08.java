package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp08 {
    public static void main(String[] args) {
        /*
        1.(?:pattern) 匹配pattern但是不捕获该匹配的子表达式,即它是一个非捕获匹配,不存储供以后使用的匹配,这对于用"or"字符(|)组合模式部件的情况很有用
                例如, industr(?:y|ies)是比'industry|industries'更经济的表达式

        2.(?=pattern) 它是一个非捕获匹配,例如 'Windows(?=95|98|NT|2000)'匹配'Windows2000'中的'Windows'，但是不匹配"Windows3.1"中的"Windows"
        3.(?!pattern) 该表达式匹配不处于匹配pattern的字符串的起始点的搜索字符串,它是一个非捕获匹配,例如,'Windows(?!95|98|NT|2000)'匹配 "Windows3.1"
            中的 "Windows"，但是不匹配 "Windows2000"中的"Windows"
        * */

        String content = "hello 韩顺平教育 jack 韩顺平老师 韩顺平同学 hello 韩顺平学生";

        //找到 韩顺平教育 , 韩顺平老师 ,韩顺平同学 子字符串
        //这是正常的写法
        //String regStr = "韩顺平教育|韩顺平老师|韩顺平同学";

        //上面的写法可以等价非捕获分组,但是这里需要注意的是：不能matcher.group(1)
        //String regStr = "韩顺平(?:教育|老师|同学)";

        //找到 韩顺平 这个关键字,但是要求 只查找韩顺平教育 和 韩顺平老师 中含有的韩顺平
        //下面是非捕获分组,不能使用matcher.group(1)
        //String regStr = "韩顺平(?=教育|老师)";

        //找到 韩顺平 这个关键字,但是要求只是查询 不包含( 韩顺平教育 和  韩顺平老师) 中包含有的韩顺平
        //下面是非捕获分组,不能使用matcher.group(1)
        String regStr = "韩顺平(?!教育|老师)";

        Pattern pattern = Pattern.compile(regStr);


        Matcher matcher = pattern.matcher(content);

        while (matcher.find()){
            System.out.println(matcher.group(0));
            //System.out.println(matcher.group(1));
        }


    }
}
