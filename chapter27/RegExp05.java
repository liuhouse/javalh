package javalh.chapter27;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 刘皓
 * @version 1.0
 */
public class RegExp05 {
    public static void main(String[] args) {
        //元字符 - 限定符
        /*
            用于指定其前面的字符的组合项连续出现多少次
            *   指定字符重复0次或者n次(无要求)零到多    (abc)*  仅包含任意个abc字符串,等效于\w*     abc , abcabcabc
            +   指定字符串重复1次或者n次(至少一次)1到多  m+(abc)*    以至少一个m开头,后接任意个abc的字符串   m , mabc , mabcabc
            ?   指定字符重复0次或者1次(最多一次)0到1   m+abc?  以至少一个m开头,后接ab或者abc的字符串    mab , mabc , mmmabc  mmmab
            {n} 只能输入n个字符    [abcd]{3}   由abcd中的字母组成的任意长度为3的字符串      abc , dbc , adc
            {n,}    指定至少n个匹配    [abcd]{3,}  由abcd中字母组成任意长度不小于3的字符串  aab , dbc , adc , aaabdc
            {n , m} 指定至少n个但是不多于m个匹配 [abcd]{3,5} 由abcd中字母组成的任意长度不小于3但是不大于5的字符串   abc, abcd,aaaa,bcdca
         */

        String content = "a211111aaaaaahello";
        //a{3} , 1{4} , \\d{2}

        //表示匹配aaa
        //String regStr = "a{3}";

        //表示匹配1111
        //String regStr = "1{4}";

        //表示匹配 两位任意数字
        //String regStr = "\\d{2}";

        //a{3,4} , 1{4,5} , \\d{2,5}
        //细节说明：java匹配默认是贪婪匹配,即尽可能匹配多的

        //表示匹配 aaa 或者 aaaa
        //String regStr = "a{3,4}";

        //表示匹配1111 或者 11111 , 这里会匹配到 11111 因为这里默认是贪婪匹配
        //String regStr = "1{4,5}";

        //匹配2位数字 或者 3 4 5 位数   默认是五位数 因为是贪婪匹配
        //String regStr = "\\d{2,5}";

        //匹配一个或者多个1 ， 至少一个
        //String regStr = "1+";

        //表示匹配一个或者多个任意数字  , 这里会匹配到这个字符串中的所有数字
        //String regStr = "\\d+";

        //String regStr = "1*";//表示匹配0个或者多个1

        //演示?的使用,遵守贪婪匹配
        //表示匹配 a 或者  a1   ?表示前面的这个字符可有可无
        //String regStr = "a1?";

        //贪婪匹配  匹配到所有的1 尽可能的多
        //String regStr = "1+";

        //禁止贪婪匹配  尽可能的少
        String regStr = "1+?";

        Pattern pattern = Pattern.compile(regStr);

        Matcher matcher = pattern.matcher(content);

        while (matcher.find()){
            System.out.println("找到" + matcher.group(0));
        }


    }
}
