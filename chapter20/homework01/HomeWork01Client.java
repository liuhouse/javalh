package javalh.chapter20.homework01;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01Client {
    public static void main(String[] args) throws IOException {
        //创建客户端
        Socket socket = new Socket(InetAddress.getLocalHost(), 9999);
        Scanner scanner = new Scanner(System.in);
        System.out.println("客户端连接成功");
        //创建输出流,给服务端发送数据
        //创建输出对象
        OutputStream outputStream = socket.getOutputStream();
        //使用字符流的方式进行写入
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        BufferedReader bufferedReader = null;
        while (true){
            System.out.println("请写出你想了解的信息,1:name 2:hobby 3:quit 退出");
            String s = scanner.nextLine();
            bufferedWriter.write(s);//写入数据
            bufferedWriter.newLine();//结束标记
            bufferedWriter.flush();//刷新数据

            //创建输入流,处理服务端发送回来的数据
            InputStream inputStream = socket.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String s1 = bufferedReader.readLine();
            System.out.println(s1);
            if("quit".equals(s1)){
                break;
            }
        }


        //关闭对应的流
        bufferedWriter.close();
        bufferedReader.close();
        socket.close();

        System.out.println("客户端已经退出...");

    }
}
