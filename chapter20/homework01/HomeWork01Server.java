package javalh.chapter20.homework01;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 */
public class HomeWork01Server {
    public static void main(String[] args) throws IOException {
        /*
        * 需求
        * (1):使用字符流的方式,编写一个客户端程序和服务端程序
        * (2):客户端发送"name",服务端接收到后,返回我是nova , nova是你的名字
        * (3):客户端发送 hobby , 服务器端接收到后,返回 "编写java程序"
        * (4):不是这两个问题,回复 "你说啥呢"
        * */

        //自己的思路,在服务端添加监听端口
        //等待客户的连接,客户连接之后发送对应的内容
        //服务端获取输入流,根据输入流的数据，显示对应的内容

        ServerSocket serverSocket = new ServerSocket(9999);
        System.out.println("服务端9999监听,正在等待连接...");
        //等待连接,没有客户端连接,就会堵塞在这里
        Socket socket = serverSocket.accept();
        //使用输入流,获取数据通道的数据
        //获取输入流对象
        InputStream inputStream = socket.getInputStream();
        //获取输出流对象
        OutputStream outputStream = socket.getOutputStream();
        //使用字符流的方式进行读取
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        //读取一行
        while(true){
            String s = bufferedReader.readLine();
            String res = "";
            if("name".equals(s)){
                //需要返回到数据通道,给客户端使用
                //使用字符流的方式回复信息
                res = "我是刘皓";
            }else if("hobby".equals(s)){
                //需要返回到数据通道,给客户端使用
                res = "编写java程序";
            }else{
                res = "你说什么";
            }
            if("quit".equals(s)){
                res = "quit";
            }
            bufferedWriter.write(res);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            if("quit".equals(res)){
                System.out.println("服务端退出了...");
                break;
            }
        }
        //关闭对应的流
        bufferedReader.close();
        bufferedWriter.close();
        socket.close();
        serverSocket.close();
    }
}
