package javalh.chapter20.homework02;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 * 发送端B - 也可以是接收端
 */
public class HomeWork02SenderB {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(9998);
        //发送数据包
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要咨询的内容:");
        String next = scanner.next();
        byte[] data = next.getBytes();
        DatagramPacket packet = new DatagramPacket(data, 0, data.length, InetAddress.getByName("192.168.1.12"), 8888);
        socket.send(packet);

        //接收服务端回复的数据
        byte[] buf = new byte[1024];
        //进行回复数据的接收
        packet = new DatagramPacket(buf, buf.length);
        socket.receive(packet);

        //接收到数据之后进行处理
        int length = packet.getLength();
        data = packet.getData();
        String s = new String(data, 0, length);
        System.out.println(s);

        socket.close();
        System.out.println("B端退出了----");
    }
}
