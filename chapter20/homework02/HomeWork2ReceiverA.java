package javalh.chapter20.homework02;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * @author 刘皓
 * @version 1.0
 * 接收端A 也可以是发送端
 */
public class HomeWork2ReceiverA {
    public static void main(String[] args) throws IOException {
        /*
        * (1):编写一个接收端A,和一个发送端B，使用UPD协议完成
        * (2):接收端在8888端口等待接收数据(receive)
        * (3):发送端向接收端发送数据 "四大名著是哪些?"
        * (4):接收端接收到发送端发送的问题后,返回 "四大名著是 西游记，红楼梦，水浒传，三国演义",否则返回what
        * (5):接收端和发送端程序退出
        * */


        //创建接收数据的UPD包
        DatagramSocket socket = new DatagramSocket(8888);
        System.out.println("接收端创建成功,等待接收数据");
        //构建数据包
        byte buf[] = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        //在没有接收到数据之前,会进行堵塞
        socket.receive(packet);
        //接收到数据之后
        //对接收到的数据进行解包,然后进行处理
        int length = packet.getLength();//数据的长度
        byte[] data = packet.getData();//包里的真实数据
        //进行解析，将包里面的数据转化为字符串
        String s = new String(data, 0, length);
        System.out.println(s);
        //接收到数据之后,根据接收的数据对发送端做对应的回复
        //=====回复信息========
        if("四大名著是哪些?".equals(s)){
            data = "红楼梦,西游记,三国演义,水浒传".getBytes();
        }else{
            data = "what?".getBytes();
        }

        packet = new DatagramPacket(data,0,data.length, InetAddress.getByName("192.168.1.12"),9998);
        socket.send(packet);
        //关闭对应的流
        socket.close();
        System.out.println("A端退出了!!!");
    }
}
