package javalh.chapter20.udp;

import java.io.IOException;
import java.net.*;

/**
 * @author 刘皓
 * @version 1.0
 * 发送端B  => 也可以接收数据
 */
public class UDPSenderB {
    public static void main(String[] args) throws IOException {
        //1.构建DatagramSocket对象,准备在9998端口,接收数据
        DatagramSocket socket = new DatagramSocket(9998);

        //2.将需要发送的数据,封装到DatagramPacket对象 - 构建数据包
        byte[] data = "hello 明天吃火锅~".getBytes();
        DatagramPacket packet = new DatagramPacket(data, 0, data.length, InetAddress.getByName("192.168.1.12"), 9999);
        //将数据包发送出去到9999端口
        socket.send(packet);

        //A端向B端发送了信息,所以这里要进行接收操作
        //准备接收给9998端口发送的数据
        byte[] buf = new byte[1024];
        packet = new DatagramPacket(buf,buf.length);
        //如果接收不到数据,就会堵塞在这里
        socket.receive(packet);

        //将packet进行解包,然后取出数据
        int length = packet.getLength();//得到数据数据的长度
        data = packet.getData();//得到数据包的真实数据
        //将数据包的数据转化成字符串之后显示出来即可
        String s = new String(data, 0,length);
        System.out.println(s);
        //关闭相关的资源
        socket.close();
        System.out.println("B端退出");
    }
}
