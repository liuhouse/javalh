package javalh.chapter20.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * @author 刘皓
 * @version 1.0
 * UDP接收端
 */
public class UDPReceiverA {
    public static void main(String[] args) throws IOException {
        /*
        * 应用案例
        * 1.编写一个接收端A,和一个发送端B
        * 2.接收端A在9999端口等待接收数据(receive)
        * 3.发送端B向接收端A发送数据"hello,明天吃火锅"
        * 4.接收端A接收到发送端B发送的数据,回复"好的,明天见"，再退出
        * 5.发送端接收回复的数据,再退出
        *
        * 这里就充分的说明,UDP编程是通过端口来进行数据通讯的
        * */
        //1:创建一个DatagramSocket 对象，准备在9999接收数据
        DatagramSocket socket = new DatagramSocket(9999);
        //2.构建一个DatagramPacket对象，准备接收数据
        byte[] buf = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        //3.调用接收方法,将通过网络传输的DatagramPacket对象,填充到packet对象
        //老师提示：当有数据包发送到本机的9999端口的时候,就会接收到数据
        //如果没有数据包发送到本机的9999端口,就会堵塞等待
        System.out.println("接收端A等待接收数据...");
        socket.receive(packet);

        //4.可以把packet进行拆包,取出数据,并显示
        int length = packet.getLength();//实际接收到的数据字节的长度
        byte[] data = packet.getData();//实际接收到的数据
        String s = new String(data, 0, length);//将接收到的数据转化成字符串
        System.out.println(s);


        //======回复信息给B端======
        //将需要发送的数据,封装到DatagramPacket对象
        data = "好的,明天见".getBytes();
        //构建数据包
        packet = new DatagramPacket(data,0,data.length, InetAddress.getByName("192.168.1.12"),9998);
        //发送数据
        socket.send(packet);
        //======回复信息结束=====


        //关闭相关的资源
        socket.close();
        System.out.println("A端退出...");
    }
}
