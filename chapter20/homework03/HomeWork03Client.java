package javalh.chapter20.homework03;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author 刘皓
 * @version 1.0
 * 客户端
 */
public class HomeWork03Client {
    public static void main(String[] args) throws IOException {
         //客户端连接服务端的8888端口,得到Socket对象
        Socket socket = new Socket(InetAddress.getLocalHost(), 8888);
        Scanner scanner = new Scanner(System.in);
        System.out.println("客户端创建成功...");
        System.out.println("请输入你要下载的文件名称:");
        String music_name = scanner.next();
        //创建输出流
        OutputStream outputStream = socket.getOutputStream();
        //写入音乐名称数据
        outputStream.write(music_name.getBytes());
        //设置写入数据的结束标记
        socket.shutdownOutput();
        //从服务端的数据通道获取到数据之后,使用输入流进行接收,并进行下载处理

        /*
        * (1):先读取到服务端发送过来的数据通道的图片数据
        * (2):使用输出流进行文件下载即可 【写入操作】
        * */
        BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
        //将读取到的图片资源转化成bytes数组
        byte[] bytes = StreamUtils.streamToByteArray(bis);

        //将输入流得到的bytes数组,写到指定的路径,就得到一个文件了,这里使用输出流写入文件
        //声明目标文件的路径
        String destFilePath = "javalh\\chapter20\\homework03\\"+music_name;
        //创建输出流
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destFilePath));
        //将数据写入文件
        bos.write(bytes);
        System.out.println("内容下载完毕...");

        //关闭对应的资源
        outputStream.close();
        bos.close();
        bis.close();
        socket.close();
        System.out.println("客户端关闭");
    }
}
