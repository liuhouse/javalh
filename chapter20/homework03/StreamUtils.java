package javalh.chapter20.homework03;

import java.io.*;

/**
 * @author 刘皓
 * @version 1.0
 * 此类用于演示关于流的读写方法
 */
public class StreamUtils {

    /**
     * 功能：将输入流转成byte[]
     * @param is
     * @return
     * @throws IOException
     */
    public static byte[] streamToByteArray(InputStream is) throws IOException {
        //创建输出流对象
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] b = new byte[1024];
        int len;
        while((len = is.read(b)) != -1){
            bos.write(b,0,len);
        }
        byte[] array = bos.toByteArray();
        bos.close();
        return array;
    }

    /**
     * 将InputStream转换成String
     * @param is
     * @return
     */
    public static String streamToString(InputStream is) throws IOException {
        //将InputStream输入流转换成转换流InputStreamReader
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        //构建一个没有字符的字符串构造器,初始容量为16个字符
        StringBuilder builder = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null){
            //将读取到的字符串追加到构造器里面
            builder.append(line+"\r\n");
        }
        return builder.toString();
    }

}
