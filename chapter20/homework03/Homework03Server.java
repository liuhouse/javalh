package javalh.chapter20.homework03;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 * 服务端
 */
public class Homework03Server {
    public static void main(String[] args) throws IOException {
        /*
        * 需求
        * (1):编写客户端程序和服务端程序
        * (2):客户端可以输入一个音乐文件名,比如高山流水,服务端收到音乐名后,可以给客户端返回这个音乐文件,如果服务器没有这个文件,返回一个默认的音乐即可
        * (3):客户端收到文件后,保存到本地e:\\
        * (4):提示:该程序可以使用StreamUtils.java
        * 本质：其实就是指定下载文件的应用，先自己结合老师讲的文件上传来做
        * */




        /*
        * 自己的思路
        * (1):在服务端监听并接收客户端传递过来的文件的名称
        * (2):找到对应的文件,然后将这个文件写入数据通道发送给客户端
        * (3):客户端进行接收数据，然后做下载操作
        *
        *
        * */

        //1:服务端在本机监听8888端口
        ServerSocket serverSocket = new ServerSocket(8888);
        System.out.println("服务端在8888端口监听...");
        //2:等待连接
        Socket socket = serverSocket.accept();
        //3.客户端连接成功之后会发送一个 需要下载的文件名 这边使用输入流进行接收   这里是接收一个名字,只是一个字符串而已
        InputStream inputStream = socket.getInputStream();
        //使用StreamUtils的方法,直接将inputStream读取到的内容转换成字符串
        //String s = StreamUtils.streamToString(inputStream);

        //这里需要注意的是,如果只是简单的文件的名称,那么就直接通过读取拼接到文件的名称数据即可
        String s = "";
        int len = 0;
        byte[] buf = new byte[1024];
        while ((len = inputStream.read(buf)) != -1){
            s += new String(buf,0,len);
        }

        //4.得到客户端输出过来的文件的名称,进行资源的查找拼接,然后写入数据通道
        String srcPath = "e:\\\\"+s;
        System.out.println(srcPath);
        File file = new File(srcPath);
        System.out.println("文件存在：" + file.exists());
        //如果要上传的文件不存在,就使用默认的图片
        if(!file.exists()){
            srcPath = "e:\\无名.mp3";
        }
        /*
        * （1）创建文件的输入流得到磁盘上的图片数据
        * （2）创建文件的输出流写入通道
        * */

        //创建读取磁盘文件的输入流
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(srcPath));
        //将输入流转化成成byte数组
        //这里的bytes就是filePath对应的字节数组
        byte[] bytes = StreamUtils.streamToByteArray(bis);
        //通过socket获取到输出流,将bytes数据发送给客户端处理，客户端进行下载即可
        BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
        //数据写入数据通道
        bos.write(bytes);
        //写入数据的结束标记
        socket.shutdownOutput();

        //关闭相对应的流
        inputStream.close();
        bos.close();
        socket.close();
        serverSocket.close();
        System.out.println("服务端关闭");
    }
}
