package javalh.chapter20.stockServerClient02;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings({"all"})
public class SocketTCP02Client {
    public static void main(String[] args) throws IOException {
        //创建客户端
        //客户端创建成功之后,服务器端就会继续向下转型
        Socket socket = new Socket(InetAddress.getLocalHost(), 9998);
        System.out.println("客户端连接成功");
        //创建输出流
        OutputStream outputStream = socket.getOutputStream();
        //写入数据
        outputStream.write("hello,server".getBytes());
        //设置结束标记
        socket.shutdownOutput();

        //因为服务器端的数据通道中有了数据，所以在客户端应该进行读取
        //创建输出流
        //这段代码如果读取到来自服务器端的数据通道的数据,就读取并继续向下走
        //否则就会堵塞,等待服务器端的响应,和打的招呼
        InputStream inputStream = socket.getInputStream();
        byte[] buf = new byte[1024];
        int readLen = 0;
        while ((readLen = inputStream.read(buf)) != -1){
            System.out.println(new String(buf,0,readLen));
        }

        //客户端给数据通道写完数据之后也要告诉服务器,写完了

        //关闭输出流
        //关闭输入流
        outputStream.close();
        inputStream.close();
        socket.close();
        //关闭socket

        System.out.println("客户端已经退出...");

    }
}
