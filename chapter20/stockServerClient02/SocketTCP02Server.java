package javalh.chapter20.stockServerClient02;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings("all")
public class SocketTCP02Server {
    public static void main(String[] args) throws IOException {
        //创建ServerSocket服务对象
        ServerSocket serverSocket = new ServerSocket(9998);
        System.out.println("服务器端9998创建成功,等待连接...");
        //生成一个socket对象
        Socket socket = serverSocket.accept();

        //创建输入流对象
        InputStream inputStream = socket.getInputStream();

        //使用IO流读取数据通道的数据
        byte[] buf = new byte[1024];
        int readLine = 0;
        while ((readLine = inputStream.read(buf)) != -1){
            System.out.println(new String(buf,0,readLine));
        }

        //服务端阅读完客户端的数据通道的数据之后,给客户端回个信
        //获取服务端的输出流
        OutputStream outputStream = socket.getOutputStream();
        //写入数据
        outputStream.write("hello,client".getBytes());

        //服务端在给数据通道写完数据之后,必须告诉客户端,否则的话,客户端就会一直等待
        //设置结束标记
        socket.shutdownOutput();
        //关闭流
        //关闭输出流

        outputStream.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
        System.out.println("服务端已经关闭....");
    }
}
