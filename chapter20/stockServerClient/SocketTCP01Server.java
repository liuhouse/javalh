package javalh.chapter20.stockServerClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 * TCP 网络编程服务端
 */
public class SocketTCP01Server {
    public static void main(String[] args) throws IOException {
        //思路
        //1.在本机的9999端口监听,等待连接
        //  细节：要求在本地没有其他服务在监听9999
        //  细节：这个ServerSocket 可以通过 accept() 返回多个Socket[多个客户端连接服务器的并发]
        ServerSocket serverSocket = new ServerSocket(9999);
        System.out.println("服务端,在9999端口监听,等待连接...");

        //2.当没有客户端连接9999端口的时候,程序会堵塞,等待连接
        //如果有客户端连接,则会返回Socket对象,程序继续执行
        // Socket socket = new Socket(InetAddress.getLocalHost(), 9999);  这就代表连接了
        Socket socket = serverSocket.accept();//等待连接

        //3.通过socket.getInputStream()读取客户端写入数据通道的数据,显示出来
        InputStream inputStream = socket.getInputStream();

        //4.IO读取
        byte[] buf = new byte[1024];
        int readLine = 0;
        //循环读取数据通道的数据  根据字节的数组来进行读取
        while ((readLine = inputStream.read(buf)) != -1){
            //根据读取到的实际长度,显示内容
            System.out.println(new String(buf,0,readLine));
        }
        //5.读取完成之后,关闭流和socket
        inputStream.close();
        socket.close();
        serverSocket.close();
        System.out.println("服务端 socket=" + socket.getClass());


    }
}
