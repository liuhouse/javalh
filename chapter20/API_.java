package javalh.chapter20;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author 刘皓
 * @version 1.0
 */
public class API_ {
    public static void main(String[] args) throws UnknownHostException {
        //InetAddress类的相关方法

        /*
        *
        * 1:获取本机的InetAddress对象 getLocalHost
        * 2:根据指定主机名/域名获取ip地址对象 getByName()
        * 3:获取InetAddress对象的主机名  getHostName
        * 4:获取InetAddress对象的主机地址 getHostAddress
        *
        * */

        //编写代码,获取计算机的主机名和IP地址相关API
        //获取本机InetAddress对象getLocalHost
        InetAddress localHost = InetAddress.getLocalHost();
        System.out.println(localHost);
        //根据指定主机名/域名获取ip地址对象,getByName
        //根据主机名,也就是电脑名称获取ip地址对象
        InetAddress host2 = InetAddress.getByName("DESKTOP-TSCEQMU");
        System.out.println(host2);
        //根据域名获取ip地址对象
        InetAddress host3 = InetAddress.getByName("www.xiaobangcai.com");
        System.out.println(host3);

        //获取InetAddress对象的主机名 getHostName
        String host3Name = host3.getHostName();
        System.out.println(host3Name);
        //获取InetAddress对象的地址 getHostAddress()
        String hostAddress = host3.getHostAddress();
        System.out.println(hostAddress);
    }
}
