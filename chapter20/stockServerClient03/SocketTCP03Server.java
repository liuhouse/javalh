package javalh.chapter20.stockServerClient03;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 *
 * 应用案例3
 * 服务端,使用字符流的方式读写
 */
@SuppressWarnings("all")
public class SocketTCP03Server {
    public static void main(String[] args) throws IOException {
        //创建ServerSocket服务对象
        ServerSocket serverSocket = new ServerSocket(9998);
        System.out.println("服务器端9998创建成功,等待连接...");
        //生成一个socket对象
        Socket socket = serverSocket.accept();

        //创建输入流对象
        InputStream inputStream = socket.getInputStream();

        //使用IO流读取数据通道的数据
//        byte[] buf = new byte[1024];
//        int readLine = 0;
//        while ((readLine = inputStream.read(buf)) != -1){
//            System.out.println(new String(buf,0,readLine));
//        }

        //IO读取,使用字符流,这里使用InputStreamReader 将 inputStream转成字符流
        //如果使用字符的方式来读取,并且结束标记是 newLine()的话,这里必须使用readLine进行读取
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String s = bufferedReader.readLine();
        System.out.println(s);

        //服务端阅读完客户端的数据通道的数据之后,给客户端回个信
        //获取服务端的输出流
        OutputStream outputStream = socket.getOutputStream();
        //写入数据
        //outputStream.write("hello,client".getBytes());
        //服务端在给数据通道写完数据之后,必须告诉客户端,否则的话,客户端就会一直等待
        //设置结束标记
        //socket.shutdownOutput();


        //使用字符输出流的方式回复信息
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        bufferedWriter.write("hello,client,字符流");
        bufferedWriter.newLine();//插入一个换行符,表示回复内容的结束
        bufferedWriter.flush();//注意这里需要手动进行刷新,否则数据不会进入数据通道

        //关闭流
        //关闭输出流
        //使用包装流的时候 关闭流的时候,只需要关闭外层流即可
        bufferedReader.close();
        bufferedWriter.close();

        socket.close();
        serverSocket.close();
        System.out.println("服务端已经关闭....");
    }
}
