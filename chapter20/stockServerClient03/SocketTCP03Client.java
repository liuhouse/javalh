package javalh.chapter20.stockServerClient03;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 */
@SuppressWarnings({"all"})
public class SocketTCP03Client {
    public static void main(String[] args) throws IOException {
        //创建客户端
        //客户端创建成功之后,服务器端就会继续向下转型
        Socket socket = new Socket(InetAddress.getLocalHost(), 9998);
        System.out.println("客户端连接成功");
        //创建输出流
        OutputStream outputStream = socket.getOutputStream();
//        //写入数据
//        outputStream.write("hello,server".getBytes());
//        //设置结束标记
//        socket.shutdownOutput();


        //使用字符流的方式写入数据
        //使用OutputStreamWriter 将 字节流 outputStream 转换成字符流,然后使用包装流进行包装
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        bufferedWriter.write("hello,server 字符流");
        //结束标记
        bufferedWriter.newLine();
        //刷新数据 - 这个是必须要进行刷新的,否则数据不会写入数据通道
        bufferedWriter.flush();

        //因为服务器端的数据通道中有了数据，所以在客户端应该进行读取
        //创建输出流
        //这段代码如果读取到来自服务器端的数据通道的数据,就读取并继续向下走
        //否则就会堵塞,等待服务器端的响应,和打的招呼
        InputStream inputStream = socket.getInputStream();
//        byte[] buf = new byte[1024];
//        int readLen = 0;
//        while ((readLen = inputStream.read(buf)) != -1){
//            System.out.println(new String(buf,0,readLen));
//        }

        //使用字符流来对通道数据进行读取
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        //服务端写入的时候是使用的是 newLine() 来进行写入的,所以这里必须使用readLine来进行读取
        String s = bufferedReader.readLine();
        System.out.println(s);

        //客户端给数据通道写完数据之后也要告诉服务器,写完了

        //关闭输出流
        //关闭输入流
//        outputStream.close();
//        inputStream.close();
        //使用包装流的时候只需要关闭外层流即可
        bufferedWriter.close();
        bufferedReader.close();
        socket.close();
        //关闭socket

        System.out.println("客户端已经退出...");

    }
}
