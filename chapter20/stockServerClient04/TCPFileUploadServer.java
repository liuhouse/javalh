package javalh.chapter20.stockServerClient04;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 * 上传文件的服务端
 */
public class TCPFileUploadServer {
    public static void main(String[] args) throws IOException {
        /*
        * 需要
        * 1.编写一个服务端,和一个客户端
        * 2.服务端在8888端口监听
        * 3.客户端连接到服务器,发送一张图片 e:\\qie.png
        * 4.服务端接收到客户端发送的图片，保存到本目录下面,发送"收到图片"再退出
        * 5.客户端接收到服务器端发送的"收到图片" 之后再退出
        * 6.该程序要求使用StreamUtils.java，我们直接使用
        * */

        /*
        * 当客户端连接到服务端之后,实际上客户端也是通过一个端口和服务端进行通讯的,这个端口是
        * TCP/IP 来分配的  ， 是不确定的,是随机的
        * */

        //1.服务端在本机监听8888端口
        ServerSocket serverSocket = new ServerSocket(8888);
        System.out.println("服务端在8888端口监听...");
        //2.等待连接
        Socket socket = serverSocket.accept();
        //3.客户端连接成功之后,读取客户端发送给数据通道的数据
        //通过Socket得到输入流，准备将客户端发送过来的数据通过输入流接收到
        BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
        byte[] bytes = StreamUtils.streamToByteArray(bis);

        //4.将输入流得到的bytes数组,写到指定的路径,就得到一个文件了,这里使用输出流写入文件
        //声明目标文件的路径
        String destFilePath = "javalh\\chapter20\\stockServerClient04\\laohu1.jpg";
        //创建输出流
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destFilePath));
        bos.write(bytes);
        bos.close();
        System.out.println("图片传输完毕...");

        //向客户端回复 "收到图片"
        //通过socket获取到输出流
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        writer.write("收到图片");
        writer.flush();//把内容刷新到数据通道
        socket.shutdownOutput();

        //关闭其他资源
        writer.close();
        bis.close();
        socket.close();
        serverSocket.close();



    }
}
