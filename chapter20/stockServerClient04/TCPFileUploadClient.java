package javalh.chapter20.stockServerClient04;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author 刘皓
 * @version 1.0
 * 客户端
 */
public class TCPFileUploadClient {
    public static void main(String[] args) throws IOException {
        //客户端连接服务端8888，得到Socket对象
        Socket socket = new Socket(InetAddress.getLocalHost(), 8888);
        String filePath = "e:\\laohu.jpg";
        //创建读取磁盘文件的输入流
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filePath));

        //将输入流的数据转化成byte数组
        //bytes就是filePath对应的字节数组
        byte[] bytes = StreamUtils.streamToByteArray(bis);
        //通过socket获取到输出流,将bytes数据发送给服务端
        BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
        bos.write(bytes);//将文件对应的字节数组的内容,写入到数据通道
        bis.close();
        socket.shutdownOutput();//设置写入数据的结束标记

        //==========接收服务端回复的消息=============
        InputStream inputStream = socket.getInputStream();
        //使用StreamUtils的方法,直接将inputStream读取到的内容转化成字符串
        String s = StreamUtils.streamToString(inputStream);
        System.out.println(s);

        //关闭相应的流
        inputStream.close();
        bos.close();
        socket.close();
    }
}
