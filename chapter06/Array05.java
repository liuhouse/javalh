package chapter06;

/*
 * 	数组应用案例
 * */
public class Array05 {
	public static void main(String[] args) {
		/*
		 * 
		 * 创建一个char类型的26个元素的数组,分别放置'A' - 'Z',使用for循环访问所有元素并且打印出来
		 * 提示：char类型的数据运算 'A' + 1 -> 'B'     'A' + 2 -> 'C'
		 * 
		 * 思路分析
		 * (1)定义一个char类型的数组  char[] chars = new char[26];
		 * (2)因为'A' + 1 = 'B'类推,所以使用for循环类赋值
		 * (3)使用for循环访问所有元素
		 */
		
		char[] chars = new char[26];
		for(int i = 0 ; i < chars.length ; i++) {//循环26次
			//'A'+0 = 'A'  'A'+ 1 = 'B'  'A' + 2 = 'C'
			//chars 是数组类型的   chars[]
			//chars[i] 是 char类型
			chars[i] = (char)('A' + i);//这里存在一个问题    int -> char   这是错误的   所以需要类型的强制转化
		}
		
		//循环输出
		for(int i = 0 ; i < chars.length ; i++) {
			System.out.print(chars[i] + " ");
		}
		
		
	}
}
