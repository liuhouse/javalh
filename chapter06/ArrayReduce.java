package chapter06;
import java.util.Scanner;

public class ArrayReduce {
	public static void main(String[] args) {
		/*
		 * 	需求 
		 * 	有一个数组{1,2,3,4,5}，可以将该数组进行缩减,提示用户是否继续缩减,每次缩减最后那个元素,当元素只剩下最后一个元素的时候,提示,不能再缩减
		 * 	思路分析
		 * 	(1)定义一个静态数组 int arr[] = {1,2,3,4,5};
		 * 		实例化Scanner,用作变量输入   
		 * 		使用do...while循环  条件为true   一直循环   当输入的为n的时候，break退出循环
		 * 	(2)定义一个新的数组 int arrNew[] = new int[arr.length - 1] 新数组的长度为原数组的长度-1  代表缩减
		 * 	(3)使用for循环遍历原数组,赋值新数组
		 * 	(4)arr = newArr
		 * 	(5)循环遍历出最后的数组
		 * 	(6)判断,如果当前数组只剩下一个元素的时候,就提示不能再进行缩减了
		 * */
		
		int arr[] = {1,2,3,4,5};
		Scanner myScanner = new Scanner(System.in);
		
		do {
			System.out.println("是否要对数组进行缩减 y/n");
			char go_on = myScanner.next().charAt(0);
			if(go_on != 'y') {
				break;
			}
			if(arr.length == 1) {
				System.out.println("数组现在只剩下一个元素了,缩减结束");
				break;
			}
			int arrNew[] = new int[arr.length - 1];
			for(int i = 0 ; i < arrNew.length ; i++) {
				arrNew[i] = arr[i];
			}
			arr = arrNew;
			System.out.println("缩减后的数组的元素为=====");
			for(int i = 0 ; i < arr.length ; i++) {
				System.out.print(arr[i] + "\t");
			}
			
		}while(true);
		
		System.out.println("数组缩减结束");
		
	}
}
