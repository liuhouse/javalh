package chapter06;

public class Array04 {
	public static void main(String[] args) {
		//(1)数组是多个相同类型数据的组合.是现在对这些数据的统一管理
		//例子
//		int a[] = {1,2,3,4,5};//正确
//		int a1[] = {1,2.2,3,4,5};//错误的  因为2.2是double   double -> int  类型转换 是错误的
//		int a2[] = {1,2,3,4,5,'c'};//这是正确的,因为 char类型是可以转化为int型的
		
		//(2) 数组中的元素可以是任何的数据类型,包括基本数据类型个和引用数据类型,但是不能混用
		//String[] arr3 = {"北京",1,1.2};//错误的
//		int b[] = {1,2,'c'};//可以混用  但是要记得精度转换问题
//		double b1[] = {1,2,3.3,'c'};//这是正确的   int -> double      char -> int -> double
		
		//(3)数组创建后,如果没有赋值,有默认值
		//int 0,short 0, byte 0 , long 0, float 0.0 , double 0.0 , char \u0000, boolean false String null
		//例子
		//int[] arr = new int[5];
		//double[] arr = new double[5];
		//float[] arr = new float[5];
		//long[] arr = new long[5];
		//short[] arr = new short[5];
		//byte[] arr = new byte[5];
		//char[] arr = new char[5];
		//boolean[] arr = new boolean[2];
		//String[] arr = new String[5];
//		for(int i = 0 ; i < arr.length ; i++) {
//			System.out.println(arr[i]);
//		}
		
		
		//(4)使用数组的步骤 1。声明数组并开辟新空间  2：给数组的各个元素赋值  3：使用数组
		
		//(5)数组的下标是0开始的
		
		//(6)数组的下标必须在指定的范围内使用,否则就会报错,下标越界异常,比如
		
		//int arr[] = new int[5];//数组的下标的范围是  0 - 4 
		//System.out.println(arr[5]);
		
		
		//(7):数组属于引用类型,数组型数据是对象(object) 一切皆对象 String是对象
		
		
		
		
	}
}
