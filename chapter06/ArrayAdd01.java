package chapter06;
import java.util.Scanner;

public class ArrayAdd01 {
	public static void main(String[] args) {
		/*
		 * 要求：实现动态的给数组添加元素效果,实现对数组的扩容
		 * (1) 原始数组使用静态分配  int arr[] = {1,2,3};
		 * (2)增加的元素4,直接放在数组的最后面 arr = {1,2,3,4}
		 * 
		 * (3)用户可以通过如下的方法来决定是否继续添加,添加成功,是否继续? y/n
		 * 
		 * 思路分析
		 * (1)定义一个原数组  int arr[] = {1,2,3};
		 * (2)创建一个新数组,int arr1[] = new int[arr.length + 1];长度为原数组的长度+1
		 * (3)对原来的数组进行遍历,将原数组中的每个位置对应的元素对应的都给新数组
		 * (4)给新数组的最后一个元素进行添加数据 arr1[arr1.length] = 4
		 * (5)arr = arr1
		 * (6)遍历输出
		 * 
		 *  	第3部的思路分析
		 *  	(1)使用do...while循环,条件为true，默认是可以一直添加的
		 *  	(2)实例化Scanner对象，保存两个变量  1：需要添加的值   2：是否继续
		 *  	(3)使用逻辑运算符,进行判断,如果继续,就一直添加,否则就退出循环
		 * */
		//初始化数组
		int arr[] = {1,2,3};
		Scanner myScanner = new Scanner(System.in);
		
		do {
			System.out.println("请输入你要添加的数字");
			int append_num = myScanner.nextInt();
			int arrNew[] = new int[arr.length + 1];
			//填充了数据下标为0-2的数据
			for(int i = 0 ; i < arr.length ; i++) {
				arrNew[i] = arr[i];
			}
			//填充数组的下标为3的数据
			arrNew[arrNew.length - 1] = append_num;
			
			arr = arrNew;
			
			System.out.println("arr数组当前的元素为========");
			//循环输出
			for(int i = 0; i < arr.length ; i++) {
				System.out.print(arr[i] + "\t");
			}
			System.out.println(" ");
			System.out.println("数据添加成功，是否继续y/n");
			
			char go_on = myScanner.next().charAt(0);
			if(go_on != 'y') {
				break;
			}
			
		}while(true);
		
		System.out.println("用户终止了给数组中添加元素....");
		
		
	}
}
