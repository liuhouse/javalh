package chapter06;
import java.util.Scanner;

public class SeqSearch01 {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String names[] = {"金毛狮王","白毛鹰王","紫衫龙王","青翼蝠王"};
		System.out.println("请输入你要查找的大王：");
		String findName = myScanner.next();
		int index = -1;
		for(int i = 0 ; i < names.length ; i++) {
			if(names[i].equals(findName)) {
				System.out.println("找到了" + findName + "对应的下标为" + i);
				index = i;
				break;
			}
		}
		if(index == -1) {
			System.out.println("没有找到" + findName);
		}
	}
}
