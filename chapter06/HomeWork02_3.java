package chapter06;

public class HomeWork02_3 {
	public static void main(String[] args) {
		/*需求：
		 *	 已知有一个升序的数组,要求插入一个元素,该数组的顺序依然是升序
		 * 	[10,12,45,90] 添加23后,数组为 [10,12,23,45,90]
		 * 
		 * 	思路分析：
		 * (1)：定义一个数组 arr，定义一个index,用来记录最终要插入的数据的索引位置
		 * (2):for循环查找到需要插入数字的索引,如果数组中的数大于等于当前这个数，那么这个数就是要插入的索引,后面的两个数往后挪即可
		 * (3):定义一个新数组newArr，长度为arr.length +1  进行数组扩容
		 * (4):使用for循环进行填充
		 * 		逻辑：如果当前的索引不是要插入的索引,就正常进行填充,如果等于当前的索引将当前的插入值给这个索引  这里要注意 i 和 j 的变化
		 * 		j : 代表 原数组，  i 代表新数组     正常填充  i = j, j++     当当前的数为要插入的索引的时候    j的索引不变
		 * 
		 * */
		
		int arr[] = {10,12,45,90};
		int index = -1;
		int inserNum = 100;
		//查找要插入的索引Index
		for(int i = 0 ; i < arr.length ; i++) {
			if(inserNum <= arr[i]) {
				index = i;
				break;
			}
		}
		
		//如果在这些数组中一个都没有找到大于当前数的,所以应该是在最后
		if(index == -1) {
			index = arr.length;
		}
		
		//进行数组扩容
		int newArr[] = new int[arr.length + 1];
		
		//i代表的是新数组 
		//j代表的是原数组
		for(int i = 0 , j = 0; i < newArr.length ; i++) {
			//不是当前要插入的索引的时候,就正常添加
			if(i != index) {
				newArr[i] = arr[j];
				j++;
			}else {
				//如果等于要插入的索引,将要插入的值给当前索引
				newArr[i] = inserNum;
			}
		}
		
		//因为最终要修改的是arr数组,所以需要将地址进行切换
		arr = newArr;
		System.out.println("=====最终arr的数组元素是...");
		
		
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print(arr[i] + "\t");
		}
		
		
	}
}
