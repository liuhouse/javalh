package chapter06;
/*
 * 	数组的动态初始化
 * */
public class TwoDimensionalArray02 {
	public static void main(String[] args) {
		//动态声明二维数组的第一种方法
		//int arr[][] = new int[2][3];
		
		//动态声明二维数组的第二种方式
		int arr[][];//声明二维数组
		//数组的长度一共是两个,每个元素都是数组,每个数组的长度是3
		arr = new int[2][3];
		arr[1][1] = 8;//再开空间
		
		//遍历arr数组
		for(int i = 0 ; i < arr.length ; i++) {
			for(int j = 0 ; j < arr[i].length ; j++) {//对每个一维数组遍历
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();//换行
		}
	}
}
