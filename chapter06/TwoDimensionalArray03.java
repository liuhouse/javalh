package chapter06;

public class TwoDimensionalArray03 {
	public static void main(String[] args) {
		/*
		 * 	看一个需求：动态创建下面的二维数组,并输出
		 * 	i = 0 : 1
		 * 	i = 1 : 2 2
		 * 	i = 2 : 3 3 3
		 * 
		 * 	一个有三个一维数组,每个一维数组的元素个数是不一样的
		 * */
		//创建一个二维数组,这个二维数组有3个元素,但是每个元素【一维数组】还没有开辟数据空间 也就是null
		int[][] arr = new int[3][];
		
		//遍历一维数组,并且给一维数组赋值
		for(int i = 0 ; i < arr.length ; i++) {
			//给每个元素开辟数据空间
			arr[i] = new int[i+1];
			//遍历一维数组,给每个元素赋值
			//arr[i].length 代表的就是每个元素
			for(int j = 0 ; j < arr[i].length;j++) {
				arr[i][j] = i + 1;//赋值
			}
		}
		
		System.out.println("arr数组的元素为===");
		
		//遍历arr输出
		for(int i = 0 ; i < arr.length ; i++) {
			for(int j = 0 ; j < arr[i].length ; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}
}
