package chapter06;

public class HomeWork01 {
	public static void main(String[] args) {
		//String strs[] = {'a','b','c'};//错误的  因为数组元素应该使用字符串类型
		//String[] strs = {"a","b","c"};//正确的
		//String[] strs = new String{"a","b","c"};//错误的
//		String[] strs = new String[] {"a","b","c"};//正确的
		//String[] strs = new String[3] {"a","b","c"};
		
//		for(int i = 0 ; i < strs.length ; i++) {
//			System.out.println(strs[i]);
//		}
		
		
//		String foo = "blue";
//		boolean[] bar = new boolean[2];//boolean[0] = false   boolean[1] = false  默认是false
//		if(bar[0]) {
//			foo = "green";
//		}
//		System.out.println(foo);
		//变化 1 3 5 7
		//输出 1 3 5 7
		
		
		int num = 1;
		while(num < 10) {
			System.out.println(num);
			if(num > 5) {
				break;
			}
			num += 2;
		};
		
	}
}
