package chapter06;

public class Array03 {
	public static void main(String[] args) {
		//使用方式  静态初始化
		//语法  : 数据类型 数组名[]  = {元素值1，元素值2....元素值3};
		//如果知道数组有多少个元素,并且知道数组的具体值,就使用下面的方法
		int a[] = {1,2,3,4};
		//上面的用法就相当于
		int b[] = new int[4];
		b[0] = 1;
		b[1] = 2;
		b[2] = 3;
		b[3] = 4;
		
		
		
		//快速入门案例   
		//在array01 之前的养鸡场中做过
		double[] hens = {1,2,3,4,5,7.7};
		//上面的就等价于
		double[] henss = new double[6];
		henss[0] = 1;
		henss[1] = 2;
		henss[2] = 3;
		henss[3] = 4;
		henss[4] = 5;
		henss[5] = 7.7;
		
		
		for(int i = 0 ;  i < henss.length ; i++) {
			System.out.println(henss[i]);
		}
	}
}
