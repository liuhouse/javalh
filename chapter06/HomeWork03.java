package chapter06;
import java.lang.Math;

public class HomeWork03 {
	public static void main(String[] args) {
		/*
		 * 	需求 随机生成10个整数(1-100)范围内的,保存到数组,并倒序打印以及求平均值,最大值以及最大值的下标,并查找里面是否有8
		 * 
		 * 	思路分析
		 * 	(1):引入随机数的包 生成1-100的随机数
		 * 	(2):定义数组进行接收
		 * 	(3):对数组进行排序，倒序
		 * 	(4)求出平均值,最大值,和最大值的下标,查找里面是不是有8
		 * 	
		 * */
		
		int arr[] = new int[10];
		
		//总数
		int total_num = 0;
		
		//是否有8
		boolean is_eight = false;
		
		
		
		//数组填充
		for(int i = 0 ; i < arr.length ; i++) {
			int num = (int)(Math.random() * 100) + 1;
			arr[i] = num;
		}
		
		//最大值
		int max_num = arr[0];
		//最大值的下标
		int max_index = 0;
		
		//倒序打印,就要进行排序
		//使用冒泡排序法进行排序
		
		//外层表示循环次数    第一次循环的次数等于第一层的长度
		//每一次循环确定一个最小的数,放在最后面
//		int temp = 0;
//		for(int i = 0 ; i < arr.length-1 ; i++) {
//			for(int j = 0 ; j < arr.length - 1 - i ; j++) {
//				//保存前面的数
//				temp = arr[j];
//				//如果前面的数小于后面的数  1 2 进行切换
//				if(arr[j] < arr[j+1]) {
//					arr[j] = arr[j + 1];
//					arr[j + 1] = temp;
//				}
//			}
//		}
		System.out.println("反转之前================================");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print(arr[i] + "\t");
			
		}
		
		
		System.out.println("反转之后================================");
		//倒序的意思就是将数组进行反转
		for(int i = 0; i < arr.length / 2 ; i++) {
			//先保存最后一个
			int temp = arr[arr.length - 1 - i];
			//第一个等于最后一个,最后一个等于第一个
			arr[arr.length - 1 - i] = arr[i];
			arr[i] = temp;
		}
		
		System.out.println("arr的元素是");
		
		for(int i = 0 ; i < arr.length ; i++) {
			//判断是否有8
			if(arr[i] == 8) {
				is_eight = true;
			}
			
			//因为最后一个数有可能等于第一个数,如果判断到最后一个数等于第一个数,那么最大值应该是最后一个数,而不是第一个数
			if(arr[i] >= max_num) {
				max_index = i;
				max_num = arr[i];
			}
			
			total_num += arr[i];
			System.out.print(arr[i] + "\t");
		}
		
		System.out.println("总数为" + total_num);
		System.out.println("平均数为" + total_num / arr.length);
		
		if(is_eight) {
			System.out.println("找到了8");
		}else {
			System.out.println("没有找到8");
		}
		
		
		System.out.println("最大值为" + max_num + "对应的下标为" + max_index);
		
		
	}
}
