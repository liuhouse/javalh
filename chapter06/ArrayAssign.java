package chapter06;

public class ArrayAssign {
	public static void main(String[] args) {
		//数组赋值机制
		//基本数据类型赋值,这个值就是具体的数,而且不会相互影响
		int n1 = 2;
		int n2 = n1;
		n2 = 8;
		//System.out.println(n1);
		//System.out.println(n2);
		
		//数组在默认的情况下是引用传递,赋的值是地址
		//看一个案例,并分析数组赋值的内存图(重点,难点)
		int[] arr1 = {1,2,3};
		
		int[] arr2 = arr1;
		
		arr2[0] = 10;
		
		//两个打印出来的地址是一样的,说明了,数组中的赋值,就是址传递，也就象征着,一个发生了变化,另外一个也会跟着改变
//		System.out.println(arr1);
//		System.out.println(arr2);
		
		for(int i = 0 ; i < arr1.length; i++) {
			System.out.println(arr1[i]);
		}
		
	}
}
