package chapter06;

public class ArrayReverse01 {
	public static void main(String[] args) {
		//定义一个数组
		//int arr[] = {11,22,33,44,55,66};
		//老韩思路
		//规律
		//1:把arr[0] 和 arr[5]进行交换 {66,22,33,44,55,11}
		//2:把arr[1] 和 arr[4]进行交换 {66,55,33,44,22,11}
		//3:把arr[2] 和 arr[3]进行交换 {66,55,44,33,22,11}
		//4:一共要交换3次 = arr.length / 2
		//5每次交换的时候,对应的下标是 arr[i] 和 arr[arr.length-1-i]
		int[] arr = {11,22,33,44,55,66,99};
		//一共循环三次
		for(int i = 0 ; i < arr.length/2;i++) {
			//先保存第五个
			int temp = arr[arr.length-1-i];
			arr[arr.length-1-i] = arr[i];//第五个 = 第一个
			arr[i] = temp;//第一个 = 原本的第五个
		}
		System.out.println("====翻转后的数组===");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.println(arr[i]);
		}
	}
}
