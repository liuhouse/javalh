package chapter06;

public class Array01 {
	public static void main(String[] args) {
		/*
		 * 需求  一个养鸡场里面有6只鸡,他们的体重分别是3kg，5kg,1kg,3.4kg,2kg,50kg,请问这6只鸡的总体重是多少,
		 * 平均体重是多少,请你编写一个程序
		 * 
		 * 思路
		 * 定义6个变量,加起来,总体重   总体重、6  就是平均体重  如果是600个  就要定义600个变量  这样就会非常的麻烦
		 * 
		 * 
		 * 引出新的技术  -> 使用数组解决
		 * */
		
//		double chk1 = 3;
//		double chk2 = 5;
//		double chk3 = 1;
//		double chk4 = 3.4;
//		double chk5 = 2;
//		double chk6 = 50;
//		
//		//总体重
//		double chk_total = chk1 + chk2 + chk3 + chk4 + chk5 + chk6;
//		System.out.println("这6只鸡的总体重是" + chk_total);
//		//平均体重
//		double chk_avg = chk_total / 6;
//		System.out.println("这6只鸡的平均体重是" + chk_avg);
		
		
		
		//我们使用数组的来解决上一个问题 =》 体验
		//定义一个数组
		//老韩解读
		
		
		
		double[] hens = {3,5,1,3.4,2,50,7.8,88.8,1.1,5.6,100,200};
		
		//遍历数组得到数组的所有元素的和,使用for
		//老韩解读
		//1:我们可以通过hens[下标]来访问数组的元素
		//	下标是从0开始变好的比如第一个的元素就是 hens[0]，因为老外习惯的从0开始计数
		//	第二个元素就是hens[1],后面的以此类推
		//2:通过for循环就可以访问数组的各个元素/值
		//3:定义一个totalWeight将各个元素进行累积
		
		//老师提示：可以通过数组名.length 得到数组的大小 也就是长度
		
		
		//定义总量
		double totalWeight = 0;
		for(int i = 0 ; i < hens.length ; i++) {
			System.out.println("第" +(i+1)+"个元素的值为" + hens[i]);
			totalWeight += hens[i];
		}
		System.out.println("鸡的总重量为" + totalWeight);
		System.out.println("鸡的平均重量为" + (totalWeight / hens.length));
		
		
		
	}
}
