package chapter06;

public class HomeWork02_2 {
	public static void main(String[] args) {
		/*需求：已知有一个升序的数组,要求插入一个元素,该数组的顺序依然是升序
		 * 	[10,12,45,90] 添加23后,数组为 [10,12,23,45,90]
		 * 	思路分析
		 * 	核心思想，在数组中查找小于等于插入数的索引,当前索引就是要插入的索引,后面的往后挪
		 * 	(1): 定义数组  
		 * 	(2):定义初始索引
		 * 	(3):循环查找到索引,如果在数组中没有找到,就默认添加到最后一个,这是默认条件
		 * */
		int arr[] = {10,12,45,90};
		int index = -1;
		int insert_num = 100;
		for(int i = 0 ; i< arr.length ; i++) {
			if(arr[i] >= insert_num) {
				index = i;
				//找打了索引位置,就推出循环,因为后面的数肯定更大
				break;
			}
		}
		
		//证明没有找到符合条件的,数组中没有找到比插入数据大的,那么就默认放在最后面
		if(index == -1) {
			index = arr.length;
		}
		
		//进行数组扩容
		int newArr[] = new int[arr.length + 1];
		
		//循环进行数据填充
		for(int i = 0 ; i < newArr.length ; i++) {
			//如果index = 5 的话,就证明最大数在最后面，那么前面的两个数组进行直接赋值
			if(index == arr.length) {//证明是在数组最后面
				if(i < arr.length) {
					newArr[i] = arr[i];
				}else {
					newArr[i] = insert_num;
				}
			}else {
				/*
				 * 	10 12   45 90 
				 *  10 12   23 45 90
				 * */
				//如果小于当前索引,就按原来得走
				//如果等于当前索引，就把当前插入的值给当前索引
				//如果大于当前索引,那么第 i个 = i - 1 个
				if(i < index) {
					newArr[i]= arr[i];
				}else if(i == index) {
					newArr[i] = insert_num;
				}else {
					newArr[i] = arr[i-1];
				}
			}
		}
		
		arr = newArr;
		for(int i = 0 ;i < arr.length ; i++) {
			System.out.print(arr[i] + "\t");
		}
		
	}
}
