package chapter06;
import java.util.Scanner;

public class SeqSearch {
	public static void main(String[] args) {
		/*
		 * 	需求
		 * 	有一个数列：白眉鹰王,金毛狮王,紫衫龙王,青翼蝠王猜数游戏,从键盘中任意输入一个名称
		 * 	判断数列中是否包含此名称 【顺序查找】 要求：如果找到了,就提示找到了,并给出下标值
		 * 
		 * 	思路分析
		 * 	(1):定义一个字符串数组  String[] names = {白眉鹰王,金毛狮王,紫衫龙王,青翼蝠王}
		 * 	(2):实例化Scanner,定义变量  String findName  使用Scanner进行保存
		 * 	(3):循环数组 ， 进行判断
		 * */
		
		String[] names = {"白眉鹰王","金毛狮王","紫衫龙王","青翼蝠王"};
		Scanner myScanner = new Scanner(System.in);
		System.out.println("请输入你要查找的人名：");
		String findName = myScanner.next();
		//定义一个index , 主要目的是为了标识在数组中是不是找到了查找的元素
		int index = -1;
		for(int i = 0 ; i < names.length ; i++) {
			if(names[i].equals(findName)) {
				//如果找到了,就把当前的下标给index
				index = i;
				System.out.println("找到了" + findName + "对应的下标为" + i);
				//如果找到了,就退出循环,不再继续查找
				break;
			}
		}
		
		if(index == -1) {
			System.out.println("没有找到" + findName);
		}
		
	}
}
