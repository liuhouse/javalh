package chapter06;

public class BubbleSort {
	public static void main(String[] args) {
		/*
		 * 总结冒泡排序的特点
			1.我们一共有5个元素
			2.一共进行了4轮排序，可以看成是外层循环
			3.每一轮排序可以确定一个数的位置,比如第1轮排序确定最大数,第二轮排序确定第二大位置，以此类推
			4.当进行比较的时候,如果前面的数大于后面的数,就交换
			5.每次比较数量在减少

		 * 
		 * 
		 * 
		 * 
		 * 	需求 将数组  [24,69,80,57,13]  按照顺序  从小到大进行排序
		 * 
		 * 	化繁为简
		 * */
		
		/*
		 * 开始第一轮的比较
		 * 第一轮比较,目标是把最大数放在最后
		开始数组 [24,69,80,57,13]
			第1次比较 [24,69,80,57,13]
			第2次比较 [24,69,80,57,13]
			第3次比较 [24,69,57,80,13]
			第4次比较 [24,69,57,13,80]
		 * */
		int arr[] = {24,69,80,57,13};
		//每次for循环之前  这个temp都会进行初始化
		int temp = 0;
		for(int j = 0 ; j < 4 ; j++) {
			temp = arr[j];//[99,80]
			if(arr[j] > arr[j+1]) {
				//前面的数就等于后面的数
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
		System.out.println("第一轮比较============");
		for(int j = 0 ; j < 5 ; j++) {
			System.out.print(arr[j] + "\t");
		}
		System.out.println("");
		
		
		
		
		/*
		 * 第二轮比较
		 * 第二轮比较，目标是把第二大数放在倒数第二个
		开始数组 [24,69,57,13,80]
			第1次比较 [24,69,57,13,80]
			第2次比较 [24,57,69,13,80]
			第3次比较 [24,57,13,69,80]
		 * */
		for(int j = 0 ; j < 3 ; j++) {
			//如果前面的数大于后面的数,就进行交换
			temp = arr[j];
			if(arr[j] > arr[j+1]) {
				//前面的数等于后面的数
				arr[j] = arr[j+1];
				//后面的数等于前面的数
				arr[j+1] = temp;
			}
		}
		
		System.out.println("第二轮比较的结果");
		for(int j = 0 ; j < 5 ; j++) {
			System.out.print(arr[j] + "\t");
		}
		System.out.println("");
		
		
		/*
		 * 第三轮比较
		 * 开始数组 [24,57,13,69,80]
			第1次比较[24,57,13,69,80]
			第2次比较[24,13,57,69,80]
		 * 
		 * */
		for(int j = 0 ; j < 2 ; j++) {
			//如果前面的数大于后面的数据就进行交换
			//先保存第一个数
			temp = arr[j];
			if(arr[j] > arr[j+1]) {
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
		
		System.out.println("第三轮比较的结果");
		for(int j = 0 ; j < 5 ; j++) {
			System.out.print(arr[j] + "\t");
		}
		System.out.println("");
		
		
		/*
		 * 第4轮的比较
		 * 开始数组[24,13,57,69,80]
			第1次比较 [13,24,57,69,80]
		 * */
		for(int j = 0 ; j < 1 ; j++) {
			//如果第一个数大于第二个数的话,就进行交换
			//每次循环之前 先保存第一个数
			temp = arr[j];
			if(arr[j] > arr[j+1]) {
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
		
		
		System.out.println("第四轮比较的结果");
		for(int j = 0 ; j < 5 ; j++) {
			System.out.print(arr[j] + "\t");
		}
		System.out.println("");
		
	}
}
