package chapter06;

public class ArrayExercise02 {
	public static void main(String[] args) {
		/*
		 * 需求：
		 * 原则;不管老师怎么解决的,自己先思考,只要能到到正确的结果,那就是对的，掌握自己的,掌握老师的,那么这就是思路
		 * 请求出一个数组int[]的最大值,{4，-1,9,10,23},并得到对应的下标
		 * 
		 * 
		 * 思路分析
		 * (1):先定一个一个数组   int arr[] = {4,-1,9,10,23};
		 * (2):使用for循环遍历数组,每两个进行比较,定义一个整数 【最大值】 max    定义一个整数下标   index    当两个数比较,数据大的时候进行赋值即可
		 * */
		
		int arr[] = {4,-1,9,23,10};
		//定义一个最大数
		int max = arr[0];
		//定义一个索引
		int index = 0;
		for(int i = 1 ;  i < arr.length ; i++) {
			if(max < arr[i]) {
				max = arr[i];
				index = i;
			}
		}
		System.out.println("最大值是" + max + "对应的索引是" + index);
	}
}
