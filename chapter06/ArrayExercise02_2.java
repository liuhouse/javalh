package chapter06;

public class ArrayExercise02_2 {
	public static void main(String[] args) {
		/*
		 * 	需求：请求数组的最大值 int[] 的最大值{4,-1,9,10,23}并得到对应的下标
		 *	思路分析
		 *	(1):定义一个Int数组, int arr[] = {4,-1,9,10,23};
		 *	(2):假定max = arr[0]是最大值  maxIndex = 0 是最大索引  
		 *	(3):从下标1开始遍历arr，如果max<当前元素,说明max不是最大值,我们就对max重新赋值,max=当前元素;maxIndex=当前元素的下标
		 *	(4):当我们遍历这个数组arr之后,max就是最大值,maxIndex就是最大值对应的下标
		 * */
		
		int[] arr = {4,-1,9,23,10};
		int max = arr[0];
		int maxIndex = 0;
		for(int i = 1 ; i < arr.length ; i++) {
			//如果max最大值小于当前元素的话
			if(max < arr[i]) {
				max = arr[i];
				maxIndex = i;
			}
		}
		System.out.println("最大值是" + max + "对应的索引是" + maxIndex);
		
	}
}
