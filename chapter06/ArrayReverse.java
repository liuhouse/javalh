package chapter06;

public class ArrayReverse {
	//数组反转
	public static void main(String[] args) {
		/*
		 * 要求：把数组的元素内容反转 
		 * arr {11,22,33,44,55,66} -> {66,55,44,33,22,11}
		 * 思路分析
		 * (1):先定义一个数组,将第一个元素给最后一个元素,将最后一个元素给第一个
		 *   
		 * */
		int arr[] = {11,22,33,44,55,66};//xx2
		int arr1[] = new int[arr.length];//xx2
		for(int i = 0 ; i< arr.length ; i++) {
			int index = (arr.length - 1) - i;
			arr1[i] = arr[index];
		}
		
		arr = arr1;
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.println(arr[i]);
		}
		
		
		
	}
}
