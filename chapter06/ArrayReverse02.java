package chapter06;

public class ArrayReverse02 {
	public static void main(String[] args) {
		//定义数组
		//int arr[] = {11,22,33,44,55,66};  将数组进行反转
		//使用逆序赋值的方式
		//老韩思路
		//数组的下标是0-5 
		//1先创建一个新的数组 arr2，大小为arr.length
		//2逆序遍历arr，将每个元素的值拷贝到arr2的元素中 【顺序拷贝】
		//3:建议增加一个循环变量,用作顺序拷贝的时候使用
		
		int arr[] = {11,22,33,44,55,66};
					
		int arr2[] = new int[arr.length];
		
		//逆序遍历
		int i = arr.length-1;//5
		//顺序拷贝
		int j = 0;//0
		
		for(;i >= 0;) {
			arr2[j] = arr[i];
			i--;
			j++;
		}
		
		arr = arr2;
		for(int k = 0 ; k < arr2.length ; k++) {
			System.out.println(arr2[k]);
		}
		
		
	}
}
