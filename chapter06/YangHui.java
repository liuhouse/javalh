package chapter06;

public class YangHui {
	public static void main(String[] args) {
		/*
		 * 使用二维数组打印一个10行的杨辉三角
		 * 1
		 * 1 1
		 * 1 2 1
		 * 1 3 3 1
		 * 1 4 6 4 1
		 * 1 5 10 10 5 1
		 * 
		 * 	提示
		 * 	(1):第一行有1个元素,第n行有n个元素
		 * 	(2):每一行的第一个元素和最后一个元素都是1
		 * 	(3):从第三行开始对于一个非第一个元素和最后一个元素的值 arr[i][j] = arr[i-1][j] + arr[i-1][j-1]
		 * 
		 * 	思路分析
		 * 	(1):定义一个长度为10的二维数组,数组中的每一个元素的长度不确定
		 * 	(2):for循环数组,根据行数的规律，开辟空间,空间的大小为当前的行数
		 * 	(3):使用for循环进行赋值操作 ,根据提示(3)的逻辑进行赋值
		 * 	
		 * 
		 * */
		
		//定义一个长度为10的二维数组,数组中每个元素的个数不确定
		int YangHui[][] = new int[10][];
		for(int i = 0 ; i < YangHui.length ; i++) {
			//给每个元素开辟空间
			YangHui[i] = new int[i+1];
			//给元素赋值
			for(int j = 0 ; j < YangHui[i].length ; j++) {
				//如果是第一个元素或者是最后一个元素
				if(j == 0 || j == YangHui[i].length - 1) {
					YangHui[i][j] = 1;
				}else {
					YangHui[i][j] = YangHui[i-1][j] + YangHui[i-1][j-1];
				}
			}
		}
		
		
		System.out.println("Yanghui三角====");
		for(int i = 0 ; i < YangHui.length ; i++) {
			for(int j = 0 ; j < YangHui[i].length ; j++) {
				System.out.print(YangHui[i][j] + " ");
			}
			System.out.println();
		}
		
		
		
	}
}
