package chapter06;
import java.util.Scanner;

public class Array02 {
	public static void main(String[] args) {
		/*
		 * 	需求 
		 * 	循环输入五个成绩,保存到double数组,并输出
		 * 
		 * 	思路分析
		 * 	第一种动态分配方式
		 * 	(1)定义一个double数组   长度为5
		 * 	(2)使用for循环,进行处理
		 *  (3)每循环一次,给数组中保存一个
		 *  (4)使用Scanner进行输入
		 * */
		
		//第一种动态分配的方式
		//double[] arr = new double[5];
		
		//第二种动态分配的方式,先声明数组,再new 分配空间
		//先声明一个数组的名称   此时的arr 是null  在内存中是不占用空间的
		double[] arr;
		//分配内存空间.这个时候就可以存放数据了
		arr = new double[5];
		
		Scanner myScanner = new Scanner(System.in);
		//循环输入数组的值
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.println("请输入第" + (i + 1) + "个数组的值:");
			double arr_input = myScanner.nextDouble();
			arr[i] = arr_input;
		}
		
		//循环输出数组的值
		for(int i = 0 ;  i < arr.length ; i++) {
			System.out.println("数组的第" + (i+1) + "个的元素的值为" + arr[i]);
		}
		
		
		
		
		
		
	}
}
