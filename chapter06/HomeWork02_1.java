package chapter06;

public class HomeWork02_1 {
	public static void main(String[] args) {
		/*需求：已知有一个升序的数组,要求插入一个元素,该数组的顺序依然是升序
		 * 	[10,12,45,90] 添加23后,数组为 [10,12,23,45,90]
		 * 
		 * 	思路分析
		 * 	(1)定义数组arr   定义新数组 newArr 数组的长度为 arr.length 
		 * 	(2)定义要插入的数insert_data,插到最后,使用冒泡排序法进行处理从小到大即可
		 */
		
		int insert_data = 100;
		int arr[] = {10,12,45,90};
		int arrNew[] = new int[arr.length + 1];
		for(int i = 0 ; i < arr.length; i++) {
			arrNew[i] = arr[i];
		}
		
		arrNew[arrNew.length - 1] = insert_data;
		
		//使用冒泡进行排序
		int temp = 0;
		for(int i = 0 ; i < arrNew.length - 1 ; i++) {
			for(int j = 0 ; j < arrNew.length - 1 - i ; j++) {
				temp = arrNew[j];
				if(arrNew[j] > arrNew[j+1]) {
					arrNew[j] = arrNew[j+1];
					arrNew[j+1] = temp;
				}
			}
		}
		
		arr = arrNew;
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
