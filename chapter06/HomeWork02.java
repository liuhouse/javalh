package chapter06;
import java.util.Scanner;

public class HomeWork02 {
	public static void main(String[] args) {
		/*
		 * 	需求：已知有一个升序的数组,要求插入一个元素,该数组的顺序依然是升序
		 * 	[10,12,45,90] 添加23后,数组为 [10,12,23,45,90]
		 * 	思路分析
		 * 	化繁为简
		 * 	(1)定义数组  arr   在定义新的数组 arrNew 新数组的长度为arr.length + 1
		 * 	(2)判断,对数组arr进行循环,从第一个元素开始和插入的数据做对比,查找第一个大于插入数的索引   当前这个索引就是插入值的索引
		 * 	(3)如果index>-1的情况下 循环赋值,将这个索引的值进行替换,当前索引之前的不变，正常赋值,等于当前索引,直接赋值插入的,后面的元素为当前索引-1
		 * 	(4)如果index =-1的情况 插入的值是最后一个值的话,就把前arr对应的所有下标[0-3]给arrNew对应的所有下标[0-3] 第四个下标就是插入的值
		 * 
		 * 	先死后活
		 * 	实例化Scanner对象
		 * 	定义一个int变量来接收Scanner的输入值
		 * 
		 * 	最终还是没有看老师的代码做出来了    思路清除   实现代码的能力也强  成功了  虽然时间长   但是这自己做出来了  很nice
		 * */
		Scanner myScanner = new Scanner(System.in);
		System.out.println("请输入插入的数:");
		int insert_data = myScanner.nextInt();
		int[] arr = {10,12,45,90};
		int arrNew[] = new int[arr.length + 1];
		int insert_index = -1;
		for(int i = 0 ; i < arr.length ; i++) {
			if(arr[i] > insert_data) {
				insert_index = i;
				break;
			}
		}
		
		
		for(int i = 0 ; i < arrNew.length ; i++) {
			if(insert_index == -1) {
				if(i < arr.length) {
					arrNew[i] = arr[i];	
				}else {
					arrNew[i] = insert_data;
				}
			}else {
				if(i < insert_index) {
					arrNew[i] = arr[i];
				}else if(i == insert_index) {
					arrNew[i] = insert_data;
				}else {
					arrNew[i] = arr[i-1];
				}
			}
			
		}
		arr = arrNew;
		
		//System.out.println(arr.length);
		
		System.out.println("最终数组arr的各个元素的值为:");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print(arr[i] + "\t");
		}
		
	}
}
