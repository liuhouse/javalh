package chapter06;
public class ArrayCopy {
	public static void main(String[] args){
		/*
		 * 编写代码,实现数组拷贝(内容复制)
		 * 将int[] arr1 = {10,20,30};拷贝到数组arr2中,要求数据空间是独立的
		 * */
		
		int arr1[] = {10,20,30};
		
		//创建一个新的数组 arr2,开辟新的数据空间,数组的长度为arr1.length
		
		int arr2[] = new int[arr1.length];
		
		//遍历arr1,把每个元素拷贝到arr2对应的元素位置
		for(int i = 0 ; i < arr1.length ; i++) {
			arr2[i] = arr1[i];
		}
		
		System.out.println("arr1的值=============================");
		//遍历数组arr1
		for(int i = 0 ; i < arr1.length ; i++) {
			System.out.print(arr1[i] + " ");
		}
		System.out.println(" ");
		
		//老师修改 arr2,不会对arr1有影响
		arr2[0] = 100;
		System.out.println("arr2的值=============================");
		for(int i = 0 ; i < arr2.length ; i++) {
			System.out.print(arr2[i] + " ");
		}
		
	}
}
