package javalh.chapter18.tankgame4;

import java.util.Vector;

/**
 * @author 刘皓
 * @version 1.0
 * 自己的坦克
 */
public class Hero extends Tank {
    //因为英雄坦克要进行子弹射击,所以现在要在坦克中定义子弹
    Shot shot = null;
    //定义子弹集合,这个子弹集合是属于英雄坦克的
    Vector<Shot> shots = new Vector<>();

    public Hero(int x , int y){
        //使用父类构造器进行初始化
        super(x,y);
    }

    //进行子弹的射击
    public void shotEnemyTank(){

        //发多颗子弹怎么办,控制在我们的面板上,最多只有5颗
        if(shots.size() == 5){
            return;
        }

        //创建shot对象,根据当前的Hero对象的位置和方向创建Shot
        //一切都以英雄坦克的坐标为准
        switch (getDirect()){//得到英雄坦克的方向
            case 0://向上
                shot = new Shot(getX() + 20 , getY() , 0);
                break;
            case 1://向右
                shot = new Shot(getX() + 60 , getY() + 20 , 1);
                break;
            case 2://向下
                shot = new Shot(getX() + 20 , getY() + 60 , 2);
                break;
            case 3://向左
                shot = new Shot(getX() , getY() + 20 , 3);
                break;
        }
        //在发射子弹的时候,可以让发射多个子弹,就要把子弹添加在一个Vector多集合的线程中,然后启动,让子弹跑着
        //然后在面板重绘的时候将这多个子弹遍历的绘制出来即可
        //将子弹添加到集合中
        shots.add(shot);
        //然后正常启动
        //当创建好子弹之后,直接启动Shot线程
        new Thread(shot).start();
    }
}
