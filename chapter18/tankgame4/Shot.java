package javalh.chapter18.tankgame4;

/**
 * @author 刘皓
 * @version 1.0
 * 射击子弹
 * 子弹是可以同时存在多个的,所以子弹是多线程的
 */
public class Shot implements Runnable{
    int x;//子弹的x坐标
    int y;//子弹的y坐标
    int direct = 0;//子弹的方向
    int speed = 10;//子弹的速度
    boolean isLive = true;//子弹是否还存活

    //构造器
    public Shot(int x, int y, int direct) {
        this.x = x;
        this.y = y;
        this.direct = direct;
    }


    //当实例化此子弹类的话,调用start()方法的时候进行射击
    @Override
    public void run() {
        //射击子弹 , 是不停的移动的
        while (true){
            //休眠50毫秒,每50毫秒移动一次
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //根据方向来改变x,y坐标
            //0 - 上 1 - 右 2 - 下 3 - 左
            switch (direct){
                case 0://上
                    y -= speed;
                    break;
                case 1://右
                    x += speed;
                    break;
                case 2://下
                    y += speed;
                    break;
                case 3://左
                    x -= speed;
                    break;
            }
            //测试,这里我们输出x,y的坐标
            System.out.println("子弹 x = " + x + "y = " + y);
            //什么时候子弹结束呢,当子弹移动到面板的边界的时候,就应该销毁(把启动子弹的线程销毁)
            //子弹活着的时候才会进行重绘,只要不符合下面的条件了,就退出
            if(!(x >=0 && x <= 1000 && y >= 0 && y <= 750 && isLive)){
                System.out.println("子弹线程退出");
                isLive = false;
                break;
            }
        }
    }
}
