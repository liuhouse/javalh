package javalh.chapter18.tankgame3;

/**
 * @author 刘皓
 * @version 1.0
 * 自己的坦克
 */
public class Hero extends Tank {
    //因为英雄坦克要进行子弹射击,所以现在要在坦克中定义子弹
    Shot shot = null;

    public Hero(int x , int y){
        //使用父类构造器进行初始化
        super(x,y);
    }

    //进行子弹的射击
    public void shotEnemyTank(){
        //创建shot对象,根据当前的Hero对象的位置和方向创建Shot
        //一切都以英雄坦克的坐标为准
        switch (getDirect()){//得到英雄坦克的方向
            case 0://向上
                shot = new Shot(getX() + 20 , getY() , 0);
                break;
            case 1://向右
                shot = new Shot(getX() + 60 , getY() + 20 , 1);
                break;
            case 2://向下
                shot = new Shot(getX() + 20 , getY() + 60 , 2);
                break;
            case 3://向左
                shot = new Shot(getX() , getY() + 20 , 3);
                break;
        }
        //当创建好子弹之后,直接启动Shot线程
        new Thread(shot).start();
    }
}
