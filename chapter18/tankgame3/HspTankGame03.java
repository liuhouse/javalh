package javalh.chapter18.tankgame3;
import javax.swing.*;

/**
 * @author 刘皓
 * @version 1.0
 *
 *
 * 线程-应用到坦克大战
 * java线程的基本知识,现在我们来实际运用一下
 *
 * 在坦克大战游戏(0.2版本)基础上添加如下功能,当玩家按下j键,就发射一颗子弹
 *
 * 分析如何实现当用户按下J键的时候,我们的坦克就发射一颗子弹
 * 老师思路
 * 1:当发射一颗子弹之后,就相当于是启动一个线程
 * 2:Hero有子弹的对象,当按下J键的时候,我们就启动一个发射行为(线程),让子弹不停的移动,形成一个射击的效果
 * 3:我们MyPanel需要不停的重绘子弹,才能出现该效果
 * 4:当子弹移动到面板的边界的时候,就应该销毁(把启动的子弹的线程销毁)
 *
 *
 */
public class HspTankGame03 extends JFrame {
    //在画框中定义画板
    MyPanel mp = null;

    public static void main(String[] args) {
        new HspTankGame03();
    }


    //使用构造器进行初始化
    public HspTankGame03(){
        //对画板进行初始化，代表一个线程
        mp = new MyPanel();
        //将mp放入到Thread，并启动
        //因为实现了Runnable接口,所以要启动就要放到Thread方法中
        //因为子弹要自己走, 而且面板也要一直刷新,所以就把两个都做成多线程的 ， 自己运行自己的即可
        Thread thread = new Thread(mp);
        thread.start();
        //将画板加载到画框中
        this.add(mp);
        //想要坦克动起来,窗口就必须作为监听者来监听键盘事件对象
        this.addKeyListener(mp);
        //设置画板的大小
        this.setSize(1000,750);
        //设置点击关闭X的时候退出程序
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置画板可视
        this.setVisible(true);
    }

}
