#演示数据库的操作
#创建一个名称为hsp_db01的数据库[图形化和指令]

#使用指令创建数据库
#create database hsp_db01;

#删除数据库的指令 - 这个指令要慎用
#DROP database hsp_db01;

#创建一个使用utf8字符集的hsp_db02的数据库
#create database hsp_db02 character set utf8;



