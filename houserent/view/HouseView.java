package javalh.houserent.view;

import javalh.houserent.domain.House;
import javalh.houserent.service.HouseService;
import javalh.houserent.utils.Utility;


/*
* 1：显示界面
* 2:接收用户输入
* 3:调用HouseService完成对房屋信息的各种操作
* */
public class HouseView {
    private boolean loop = true;//控制显示菜单
    private char key = ' ';//接收用户的选择
    private HouseService houseService = new HouseService(10);//设置数组的大小为10

    //编写mainMenu方法  显示主菜单
    public void mainMenu(){
        do {
            System.out.println("---------------房屋出租系统------------------");
            System.out.println("\t\t 1 新 增 房 源");
            System.out.println("\t\t 2 查 找 房 屋");
            System.out.println("\t\t 3 删 除 房 屋");
            System.out.println("\t\t 4 修 改 房 屋 信 息");
            System.out.println("\t\t 5 房 屋 列 表");
            System.out.println("\t\t 6 退 出");
            System.out.println("请输入你的选择:[1-6]之间");
            //接收用户的选择
            key = Utility.readChar();
            switch (key){
                case '1':
                    addHouse();
                    break;
                case '2':
                    findHouse();
                    break;
                case '3':
                    delHouse();
                    break;
                case '4':
                    updateHouse();
                    break;
                case '5':
                    listHouses();
                    break;
                case '6':
                    exit();
                    break;
                default:
                    System.out.println("您输入错误了 必须(1-6)之间");
            }
        }while(loop == true);
    }



    //查找房屋
    public void findHouse(){
        System.out.println("--------------查找房屋------------------");
        System.out.print("请输入你要查找的id:");
        int i = Utility.readInt();
        House house = houseService.find(i);
        if(house == null){
            System.out.println("-------房屋信息不存在---------");
            return;
        }
        System.out.println(house);
    }


    //退出系统
    public void exit(){
        char c = Utility.readConfirmSelection();
        //确认退出系统
        if(c == 'Y'){
            loop = false;
        }
    }


    //编写delHouse()页面,接收输入的id，调用Service的del方法
    public void delHouse(){
        System.out.println("---------------删除房屋--------------------");
        System.out.print("请选择待删除房屋编号(-1退出)：");
        int delId = Utility.readInt();
        if(delId == -1){
            System.out.println("----------------放弃删除房屋信息-------------");
            return;
        }
        //请注意该方法本身就有循环判断的逻辑,必须输出Y/N
        char choice = Utility.readConfirmSelection();
        //如果输入的是Y,就代表是真的要进行删除操作了
        if(choice == 'Y'){//真的删除
            if(houseService.del(delId)){
                System.out.println("---------删除房屋信息成功---------");
            }else{
                System.out.println("---------房屋编号不存在,删除失败---------");
            }
        }else{
            //如果输入的是N的话,就代表要退出删除了
            System.out.println("--------------------放弃删除房屋----------------");
        }


    }

    //编写addHouse()界面,接收输入,创建House对象,调用add()方法  完成房屋的添加
    public void addHouse(){
        System.out.println("-------------------添加房屋-----------------");
        System.out.print("姓名：");
        String name = Utility.readString(8);
        System.out.print("电话：");
        String phone = Utility.readString(11);
        System.out.print("地址：");
        String address = Utility.readString(30);
        System.out.print("月租：");
        int rent = Utility.readInt();
        System.out.print("状态(未出租/已出租) ：");
        String state = Utility.readString(3);
        //创建一个House对象,用作添加到数组中
        House newHouse = new House(0, name, phone, address, rent, state);
        if(houseService.add(newHouse)){
            System.out.println("------------添加房屋成功----------");
        }else{
            System.out.println("------------添加房屋失败----------");
        }
    }

    //修改房屋信息
    //修改房屋信息先查找房屋信息
    public void updateHouse(){
        System.out.println("-------------------修改房屋信息-----------------");
        System.out.print("请选择待修改房屋编号(-1退出):");
        int findId = Utility.readInt();
        if(findId == -1){
            System.out.println("你放弃了房源信息的修改");
            return;
        }
        House house = houseService.find(findId);
        //如果房源信息没有找到
        if(house == null){
            System.out.println("房源信息不存在,修改失败");
            return;
        }

        //====================第一种处理修改的方式start==================
        /*
        System.out.print("姓名(" + house.getName() +")：");
        String name = Utility.readString(8);
        System.out.print("电话(" + house.getPhone() +")：");
        String phone = Utility.readString(11);
        System.out.print("地址(" + house.getAddress() + ")：");
        String address = Utility.readString(30);
        System.out.print("租金(" + house.getRent() + ")：");
        int rent = Utility.readInt();
        System.out.print("状态(" + house.getState() + ")：");
        String state = Utility.readString(30);
        House house1 = new House(house.getId(), name,  phone ,address , rent , state);
        if(houseService.update(house1)){
            System.out.println("------------修改完成-------------");
        }else{
            System.out.println("------------修改失败--------------");
        }

         */

        //===================第一种处理问题的方式end======================

        //==============第二种处理问题的方式start===============
        System.out.print("姓名(" + house.getName() + ")：");
        String name = Utility.readString(8,"");//如果用户直接回车表示不修改默认信息,默认" "
        if(!"".equals(name)){
            house.setName(name);
        }
        System.out.print("电话(" + house.getPhone() + ")：");
        String phone = Utility.readString(11,"");
        if(!"".equals(phone)){
            house.setPhone(phone);
        }
        System.out.print("地址(" + house.getAddress() + ")：");
        String address = Utility.readString(18,"");
        if(!"".equals(address)){
            house.setAddress(address);
        }
        System.out.print("租金(" + house.getRent() + ")：");
        int rent = Utility.readInt(-1);
        //如果当前的租金不是-1默认值,就代表输入了租金,就设置租金
        if(rent != -1){
            house.setRent(rent);
        }
        System.out.print("状态(" + house.getState() + ")：");
        String state = Utility.readString(3,"");
        if(!"".equals(state)){
            house.setState(state);
        }
        System.out.println("---------------------修改房屋信息成功-------------------");

    }


    //编写listHouses()显示房屋列表
    public void listHouses(){
        System.out.println("-----------------房屋列表-----------------");
        System.out.println("编号\t房主\t\t电话\t\t\t地址\t\t\t月租\t\t状态");
        //房屋的列表信息展示
        //调用服务层的房屋列表完成列表的显示
        House[] houses = houseService.list();
        for(int i = 0 ; i < houses.length ; i++){
            //但是这里默认会有很多的null，是因为给数组没有赋值,所以默认是null，这里使用过关斩将的方式处理
            //如果是null的话,就不显示了
            if(houses[i] == null){
                break;
            }
            System.out.println(houses[i]);
        }
        System.out.println("-----------------房屋列表完成--------------");

    }
}
