package javalh.houserent.utils;

public class TestUtility {
    public static void main(String[] args) {
        //这是一个测试类.使用完毕,就可以删除了
        //要求输入一个字符串,长度最大最大为3
//        String s = Utility.readString(3);
//        System.out.println("s = " + s);

        //要求输入一个字符串,长度最大为10,如果用户直接输入回车,就给一个默认值
        //老师提示,就把该方法当做一个api使用即可

//        System.out.println("请输入一个字符串：");
//        String hspedu = Utility.readString(10, "hspedu");
//        System.out.println("hspedu = " + hspedu);

        //输入一个整数
        System.out.println("请输入一个整数:");
        int i = Utility.readInt();
        System.out.println("i = " + i);


        //这边需要解释一个问题
        //这里老师是直接使用 类.方法() => 因为当一个类是static的时候,就是一个静态方法
        //想要调用hi()，需要实例化
//        new A().hi();
        //因为cry是静态方法,所以可以直接调用
//        A.cry();
    }
}


class A{
    public void hi(){}
    public static void cry(){}
}
