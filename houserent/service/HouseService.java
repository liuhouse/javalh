package javalh.houserent.service;

import javalh.houserent.domain.House;

/*
* HouseService.java <=>类【业务层】也是编写的重点
*   1：响应HouseView的调用
*  2：完成对房屋信息的各种操作（增删改查）c =>create  u => update r => read d => delete
* */
public class HouseService {
    //使用数组保存房屋对象的信息
    private House houses[];
    //记录当前有多少个房屋信息
    private int houseNums = 1;
    //记录当前的id增长到哪个值
    private int idCounter = 1;//记录当前的id增长到哪个值

    //构造器初始化房源信息
    //最多存放多少房源
    public HouseService(int size){
        //new houses
        //当创建HouseService对象,指定数组的大小为size
        houses = new House[size];
        //为了配合测试列表信息,这里初始化一个House对象
        houses[0] = new House(1,"jack","17691107518","西安市雁塔区",2000,"未出租");
    }


    //修改房源
    public boolean update(House house){
        //首先查找出当前修改房源所对应的下标
        for(int i = 0 ; i < houseNums ; i++){
            if(houses[i].getId() == house.getId()){
                houses[i] = house;
                return true;
            }
        }
        return false;
    }


    //find 查找房屋
    public House find(int id){
        //先查找id是不是在数组中存在
        int index = -1;
        for (int i = 0 ; i < houseNums ; i++){
            if(houses[i].getId() == id){
                index = i;
            }
        }
        if(index == -1){
            return null;
        }
        return houses[index];
    }

    //del方法,删除一个房屋信息
    public boolean del(int delId){
        //应当先找到要删除的房屋信息对应的下标
        //老韩强调,一定要搞清楚，下标和房屋的编号不是一回事
        int index = -1;
        //这里的数组的查找范围是,当前数组中不为空的值,也就是房源确实存在的数量   houseNums
        for(int i = 0 ; i < houseNums ; i++){
            //这里就代表确实已经查找到了这个房源id对应的下标信息
            if(delId == houses[i].getId()){
                index = i;
            }
        }

        //代表就没有查找到这个要删除的房源信息对应的下标信息
        //说明delId在数组中不存在
        if(index == -1){
            return false;
        }

        //如果找到了,就把现在有房源的数据统一向前移动,然后将房源的最后一个置空即可

        //思考：这里为什么的长度要是houseNum - 1 呢 ,  举例,如果数组的长度为4,那么数组的下标最大为3， 是不存在下标为4的数据的,
        //下面使用了 i+ 1  所以 这里要-1
        for(int i = 0 ; i < houseNums - 1 ; i++){
            //将后一个数据向前移动
            houses[i] = houses[i + 1];
        }
        //把当前有存在的房屋信息的最后一个置空
        //(1)将最后一个有房源信息的置空
        //(2)既然删除了房源信息,那么就要将房源的数量-1
        houses[--houseNums] = null;
        return true;
    }


    //add方法,添加新对象,返回boolean
    public boolean add(House newHouse){
        //再添加之前要判断当前的数组是不是已经满了,因为现在数组是有最大长度的,所以要记录一下,现在到底添加了多少个房源了
        //如果现在添加房源的数量  == 数组中能存放房源的总数量，则不能再天啊及
        if(houseNums == houses.length){
            System.out.println("数组已满,不能再添加了...");
            return false;
        }
        //把newHouse对象加入到,新增加一个房屋
        houses[houseNums++] = newHouse;
        //每次加完之后给下标+1
        //houseNums++;
        //我们程序员要设计一个自增id的机制,然后更新newHouse的id
        newHouse.setId(++idCounter);
        return true;
    }

    public House[] getHouses() {
        return houses;
    }

    public void setHouses(House[] houses) {
        this.houses = houses;
    }

    //list方法,返回houses数组
    public House[] list(){
        return houses;
    }
}
