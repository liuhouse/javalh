package javalh.houserent;

import javalh.houserent.view.HouseView;

public class HouseRentApp {
    public static void main(String[] args) {
        //在入口文件中调用主菜单的显示
        new HouseView().mainMenu();
        System.out.println("您已经退出了房屋出租系统...");
    }
}
